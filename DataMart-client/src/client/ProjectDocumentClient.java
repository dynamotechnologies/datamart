package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.projectdocument.*;


public class ProjectDocumentClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public ProjectDocumentClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.projectdocument", "ProjectDocument.xsd");
    }


    public void postProjectDocument(String projectid, String projecttype, String docid, String pubflag,
            String wwwlink, String docname, String description, String pdffilesize, String sensitiveflag)
            throws IOException
    {
        Projectdocument projdoc = new Projectdocument();
        projdoc.setProjectid(projectid);
        projdoc.setProjecttype(projecttype);
        projdoc.setDocid(docid);
        projdoc.setPubflag(pubflag);
        projdoc.setWwwlink(wwwlink);
        projdoc.setDocname(docname);
        projdoc.setDescription(description);
        projdoc.setPdffilesize(pdffilesize);
        projdoc.setSensitiveflag(sensitiveflag);
        postProjectDocument(projdoc);
    }


    @Deprecated
    public void postProjectDocument(String projectid, String projecttype, String docid, String pubflag,
            String wwwlink, String docname, String description, String pdffilesize)
            throws IOException
    {
        Projectdocument projdoc = new Projectdocument();
        projdoc.setProjectid(projectid);
        projdoc.setProjecttype(projecttype);
        projdoc.setDocid(docid);
        projdoc.setPubflag(pubflag);
        projdoc.setWwwlink(wwwlink);
        projdoc.setDocname(docname);
        projdoc.setDescription(description);
        projdoc.setPdffilesize(pdffilesize);
        postProjectDocument(projdoc);
    }


    public void postProjectDocument(Projectdocument projdoc)
            throws IOException
    {
        post(objectfactory.createProjectdocument(projdoc), "/projects/" + projdoc.getProjecttype()+"/"+projdoc.getProjectid()+"/docs");
    }


    // TODO: make parameter order consistent
    public Projectdocument getProjectDocument(String projecttype, String projectid, String docid)
            throws IOException
    {
        return get("/projects/" + projecttype + "/" + projectid + "/docs/" + docid, Projectdocument.class);
    }


    // TODO: make parameter order consistent
    public Projectdocuments getProjectDocumentList(String projecttype, String projectid)
            throws IOException
    {
        return getProjectDocumentList(projecttype, projectid, false);
    }

    // TODO: make parameter order consistent
    public Projectdocuments getProjectDocumentList(String projecttype, String projectid, Boolean published)
            throws IOException
    {
        String pubflag = "";
        if (published != null && published)
            pubflag = "?pubflag=1";
        return get("/projects/" + projecttype+"/"+projectid + "/docs/" + pubflag, Projectdocuments.class);
    }
    

    // TODO: make parameter order consistent
    public Projectdocuments getProjectDocumentPagedList(String projecttype, String projectid, Boolean pubflag, Boolean sensitiveflag, Integer start, Integer rows)
            throws IOException
    {
        String query = "/projects/" + projecttype+"/"+projectid + "/docs/?";
        if (pubflag != null) {
            query += "pubflag=" + (pubflag ? 1 : 0) + "&";
        }
        if (sensitiveflag != null) {
            query += "sensitiveflag=" + (sensitiveflag ? 1 : 0) + "&";
        }
        if (start != null) {
            query += "start=" + start + "&";
        }
        if (rows != null) {
            query += "rows=" + rows;
        }
        return get(query, Projectdocuments.class);
    }


    public void putProjectDocument(String projectid, String projecttype, String docid, String pubflag,
            String wwwlink, String docname, String description, String pdffilesize, String sensitiveflag)
            throws IOException
    {
        Projectdocument projdoc = new Projectdocument();
        projdoc.setProjectid(projectid);
        projdoc.setProjecttype(projecttype);
        projdoc.setDocid(docid);
        projdoc.setPubflag(pubflag);
        projdoc.setWwwlink(wwwlink);
        projdoc.setDocname(docname);
        projdoc.setDescription(description);
        projdoc.setPdffilesize(pdffilesize);
        projdoc.setSensitiveflag(sensitiveflag);
        putProjectDocument(projdoc);
    }


    @Deprecated
    public void putProjectDocument(String projectid, String projecttype, String docid, String pubflag,
            String wwwlink, String docname, String description, String pdffilesize)
            throws IOException
    {
        Projectdocument projdoc = new Projectdocument();
        projdoc.setProjectid(projectid);
        projdoc.setProjecttype(projecttype);
        projdoc.setDocid(docid);
        projdoc.setPubflag(pubflag);
        projdoc.setWwwlink(wwwlink);
        projdoc.setDocname(docname);
        projdoc.setDescription(description);
        projdoc.setPdffilesize(pdffilesize);
        putProjectDocument(projdoc);
    }


    public void putProjectDocument(Projectdocument projdoc)
            throws IOException
    {
        put(objectfactory.createProjectdocument(projdoc),
                "/projects/" + projdoc.getProjecttype()+"/"+projdoc.getProjectid()+"/docs/"+projdoc.getDocid());
    }


    // TODO: make parameter order consistent
    public void deleteProjectDocument(String projecttype, String projectid, String docid)
            throws IOException
    {
        delete("/projects/" + projecttype+"/"+projectid + "/docs/" + docid);
    }
}