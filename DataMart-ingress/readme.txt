
Legacy Ingress and Reference Ingress Tools
Deployment and Server Configuration Guide




SERVER CONFIG
~~~~~~~~~~~~~~
1) Decide on a working directory. This is where the ingress tool will run. See SAMPLE-legacyconf.py for examples.
2) In the working directory, create a file called legacyconf.py and add the appropriate config options.
   See SAMPLE-legacyconf.py for samples.
3) Copy the following files to the working directory:
   legacyingress.error
   legacyingress.log
   projectxml.index
   run-datamart-ingress.sh
   legacy-ingress.py
4) Copy the jars in trunk/lib/*.jar to the working directory
4) Configure INGRESS_WORKDIR in run-datamart-ingress.sh
5) Configure a job in cron.hourly to execute run-datamart-ingress.sh



DEPLOYMENT
~~~~~~~~~~~~~~
NOTE: By default the client will connect to the datamart server at http://localhost:7001
      On SvcBkrProd1, the port should be changed to 7003.  Unzip the jar, manually edit client.properties, then re-bundle the jar.
      This should not be necessary on CoreTst1.
      See 'Editing client.properties' below.

1) Using NetBeans, clean and build the client and ingress projects
2) Copy the client jar (trunk/DataMart-client/dist/DataMart-client.jar) to the working directory.
   Overwrite the existing DataMart-client.jar
3) Copy the ingress jar (trunk/DataMart-ingress/dist/DataMart-ingress.jar) to the working directory, then unzip it.
   jar -xvf DataMart-ingress.jar



TEST
~~~~~~
1) Edit projectxml.index to limit the number of files that will be processed in this run
2) sudo ./run-datamart-ingress.sh



CHECK STATUS
~~~~~~~~~~~~~~~~~~
ps aux | grep ingress
cat legacyingress.error
cat legacyingress.log



TODO
~~~~~
Figure out how to build and deploy this tool from the command line.  It should be possible to deploy from a subversion working directory on the target system, eliminating most of the error-prone process described above. The last time I tried this there were issues building the xjc-generated jaxrs files.





Editing client.properties
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cd trunk/DataMart-client/dist
jar -xvf DataMart-client.jar
vim client/client.properties
jar -cf DataMart-client.jar client META-INF us
rm -rf client META-INF us

To verify that you re-assembled the client jar correctly, try this:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
unzip DataMart-client.jar client/client.properties
head -1 client/client.properties
rm -rf client/









~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
From the command line you can invoke the ingress utilities directly like so:

java -cp .:DataMart-client.jar:asm-3.1.jar:jersey-bundle-1.4.jar:jsr311-api-1.1.1.jar ingress.Reference
java -cp .:DataMart-client.jar:asm-3.1.jar:jersey-bundle-1.4.jar:jsr311-api-1.1.1.jar ingress.Legacy
