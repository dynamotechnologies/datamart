package stateless;

import javax.ejb.Stateless;
import beanutil.*;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import us.fed.fs.www.nepa.schema.role.*;

@Stateless(mappedName="RoleManagerRemote")
public class RoleManager implements RoleManagerRemote {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void create(Role resource)
             throws DataMartException
    {
        validate(resource);
        entity.RefRoles entity = createEntity(resource);

        entity.RefRoles badentity = createEntity(entity.getId());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        entity = em.merge(entity);
    }

    @Override
    public Roles getAll()
             throws DataMartException
    {
        // Retrieve all entities from the database
        Query query = em.createQuery("SELECT u FROM RefRoles u");
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Roles responselist = new Roles();
        while (it.hasNext()) {
            responselist.getRole().add(createResource( (entity.RefRoles)it.next() ));
        }
        return responselist;
    }

    @Override
    public void update(Role resource)
             throws DataMartException
    {
        validate(resource);
        entity.RefRoles entity = createEntity(resource);

        if (createEntity(entity.getId())==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        entity = em.merge(entity);
    }

    @Override
    public void delete(int id)
             throws DataMartException
    {
        entity.RefRoles entity = createEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));
    }

    @Override
    public Role get(int id)
             throws DataMartException
    {
        Query query = em.createNamedQuery("RefRoles.findById");
        query.setParameter("id", id);
        try {
            entity.RefRoles entity = (entity.RefRoles)query.getSingleResult();
            return createResource(entity);
        } catch (javax.persistence.NoResultException e) {
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        }
    }

    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     *
     * This method does the mapping from jaxb object to entity object and vice versa
     */
    private Role createResource(entity.RefRoles entity)
            throws DataMartException
    {
        Role resource = new Role();
        resource.setId(entity.getId());
        resource.setName(entity.getRoleName());
        resource.setDescription(entity.getDescription());
        return resource;
    }

    private entity.RefRoles createEntity(Role resource)
            throws DataMartException
    {
        entity.RefRoles entity = new entity.RefRoles();
        entity.setId(resource.getId());
        entity.setRoleName(resource.getName());
        entity.setDescription(resource.getDescription());
        return entity;
    }

    // This version of overloaded method createEntity() accepts a String ID as a parameter, and returns the
    //   associated entity, or null if it doesn't exist
    //
    private entity.RefRoles createEntity(int id)
            throws DataMartException
    {
        return em.find(entity.RefRoles.class, id);
    }

    private void validate(Role resource)
            throws DataMartException
    {
        DataMartBeanUtil.confirmNotNull(resource.getName(), "Name");
    }

}