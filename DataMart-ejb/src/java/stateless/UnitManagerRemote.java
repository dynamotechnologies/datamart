package stateless;

import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.unit.*;
import beanutil.DataMartException;
import us.fed.fs.www.nepa.schema.objection.Objections;
import us.fed.fs.www.nepa.schema.project.Projects;

/*
 *
 * The Manager beans are responsible for:
 *   1) Implementing business logic and
 *   2) Translating front-end resources to back-end database entities.
 *
 * Often the resources will consist of data from numerous database tables. It is the manager's responsibility
 *   to ensure that these front-end resources can be accessed as though they were cut from a single piece of wood.
 * 
 */
@Remote
public interface UnitManagerRemote {
    public void create(Unit resource) throws DataMartException;
    public Unit get(String id) throws DataMartException;
    public Units getRegions() throws DataMartException;
    public Units getSubUnits(String id) throws DataMartException;
    
    public Objections getObjections(String id, String startdate, String enddate) throws DataMartException;
    public Projects getProjects(String id) throws DataMartException;
    public Units getAll() throws DataMartException;
    public Units getAllUnique() throws DataMartException;
    public void update(Unit resource) throws DataMartException;
    public void delete(String id) throws DataMartException;
    public Boolean ping();
}