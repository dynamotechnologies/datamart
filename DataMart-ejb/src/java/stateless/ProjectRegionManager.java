package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.projectlocation.*;

import beanutil.*;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless(mappedName="ProjectRegionManagerRemote")
public class ProjectRegionManager implements ProjectRegionManagerRemote
{

    @PersistenceContext
    private EntityManager em;

    private String PROJECT_TYPE = "nepa";

    @Override
    public void merge(String projectid, String id)
             throws DataMartException
    {
        if (id.length()!=4)
            throw new DataMartException(DataMartException.BAD_FORMAT,
                    "Unit '" + id + "' is not a region. Region codes must be four digits.");
        entity.ProjectRegions region = createEntity(projectid, id);

        // Check for existing entity and grab the internal ID if it exists
        entity.ProjectRegions oldregion = getEntity(projectid, id);
        if (oldregion != null)
            region.setId(oldregion.getId());

        em.merge(region);
    }


    @Override
    public Regions get(String projectid, String id)
            throws DataMartException
    {
        Regions rval = new Regions();
        entity.ProjectRegions region = getEntity(projectid, id);
        if (region==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "NEPA Project " + projectid + " is not associated with region '" + id + "'");

        rval.getRegionid().add(region.getRefUnits().getId());
        return rval;
    }


    @Override
    public void delete(String projectid, String id)
            throws DataMartException
    {
        entity.ProjectRegions region = getEntity(projectid, id);
        if (region==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "NEPA Project " + projectid + " is not associated with region '" + id + "'");

        em.remove(region);
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    private entity.ProjectRegions createEntity(String projectid, String id)
            throws DataMartException
    {
        // Get a reference to the project
        entity.Projects project = DataMartBeanUtil.getProject(em, this.PROJECT_TYPE, projectid);
        if (project==null)
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION,
                    "Project with type '" + this.PROJECT_TYPE + "' and ID '" + projectid + "' does not exist.");

        // Get the reference unit associated with this id
        entity.RefUnits ref = (entity.RefUnits) DataMartBeanUtil.getEntityById(em,
                "RefUnits", id, "Unit Code");

        entity.ProjectRegions region = new entity.ProjectRegions();
        region.setProjects(project);
        region.setRefUnits(ref);
        return region;
    }

    
    private entity.ProjectRegions getEntity(String projectid, String id)
            throws DataMartException
    {
        // Get a reference to the project
        entity.Projects project = DataMartBeanUtil.getProject(em, this.PROJECT_TYPE, projectid);
        if (project==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Project with type '" + this.PROJECT_TYPE + "' and ID '" + projectid + "' does not exist.");

        // Get the reference unit associated with this id
        entity.RefUnits ref = (entity.RefUnits) DataMartBeanUtil.getEntityById(em,
                "RefUnits", id, "Unit Code");
        if (ref==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Reference Unit with id '" + id + "' does not exist.");

        // Query for the relationship table row
        Query q = em.createQuery("SELECT p FROM ProjectRegions p WHERE p.projects = :project AND p.refUnits = :ref");
        q.setParameter("project", project);
        q.setParameter("ref", ref);
        entity.ProjectRegions region = (entity.ProjectRegions) DataMartBeanUtil.getEntityByQueryAllowNull(q,
                "NEPA Project " + projectid + " has multiple region associations with id '" + id + "'");

        return region;
    }
}