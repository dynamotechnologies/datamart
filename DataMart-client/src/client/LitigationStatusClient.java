package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.litigationstatus.*;


public class LitigationStatusClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public LitigationStatusClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.litigationstatus", "LitigationStatus.xsd");
    }

    public void postLitStatus(Integer id, String name)
            throws IOException
    {
        Litigationstatus status = new Litigationstatus();
        status.setId(id);
        status.setName(name);
        postLitStatus(status);
    }


    public void postLitStatus(Litigationstatus status)
            throws IOException
    {
        post(objectfactory.createLitigationstatus(status), "/ref/litigationstatuses");
    }


    public Litigationstatus getLitStatus(Integer id)
            throws IOException
    {
        return get("/ref/litigationstatuses/" + id, Litigationstatus.class);
    }


    public Litigationstatuses getLitStatusList()
            throws IOException
    {
        return get("/ref/litigationstatuses", Litigationstatuses.class);
    }


    public void putLitStatus(Integer id, String name)
            throws IOException
    {
        Litigationstatus status = new Litigationstatus();
        status.setId(id);
        status.setName(name);
        putLitStatus(status);
    }

    public void putLitStatus(Litigationstatus status)
            throws IOException
    {
        put(objectfactory.createLitigationstatus(status), "/ref/litigationstatuses/" + status.getId());
    }


    public void deleteLitStatus(Integer id)
            throws IOException
    {
        delete("/ref/litigationstatuses/" + id);
    }
}