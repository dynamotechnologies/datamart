package test.entity;

import client.AppealClient;
import client.DecisionClient;
import client.ProjectClient;
import client.ProjectDocumentClient;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.appeal.*;


public class AppealTest {
    private AppealClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new AppealClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postAppeal(TestData.appeals.id1, TestData.projects.id1, TestData.projects.type1,
                    TestData.appeals.appellant1, TestData.appeals.outcomeid1, TestData.projectdocuments.docid1,
                    TestData.appeals.dismissalid1, TestData.appeals.responsedate1, TestData.appeals.ruleid1, 
                    TestData.appeals.statusid1, TestData.appeals.adminunitcode1, TestData.decisions.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Appeal appeal = null;
        Appeals appeallist = null;
        Appeals unitappeals = null;
        try {
            appeal = client.getAppeal(TestData.appeals.id1);
            appeallist = client.getAppealList();
            unitappeals = client.getAppealsByUnit(TestData.appeals.adminunitcode1);
        } catch (IOException ex) {
            Logger.getLogger(AppealTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.appeals.id1, appeal.getId());
        Assert.assertEquals(TestData.projects.id1, appeal.getProjectid());
        Assert.assertEquals(TestData.projects.type1, appeal.getProjecttype());
        Assert.assertEquals(TestData.appeals.appellant1, appeal.getAppellant());
        Assert.assertEquals(TestData.appeals.outcomeid1, appeal.getOutcomeid());
        Assert.assertEquals(TestData.projectdocuments.docid1, appeal.getDocid());
        Assert.assertEquals(TestData.appeals.dismissalid1, appeal.getDismissalid());
        Assert.assertEquals(TestData.appeals.responsedate1, appeal.getResponsedate().toString());
        Assert.assertEquals(TestData.appeals.ruleid1, appeal.getRuleid());
        Assert.assertEquals(TestData.appeals.statusid1, appeal.getStatusid());
        Assert.assertEquals(TestData.appeals.adminunitcode1, appeal.getAppadminunit());
        Assert.assertEquals(TestData.decisions.id1, Integer.parseInt(appeal.getDecisionid()));
        Assert.assertTrue(appeallist.getAppeal().get(0).getProjectid().length()>0);
        Assert.assertTrue(appeallist.getAppeal().get(0).getAppellant().length()>0);
        Assert.assertTrue(appeallist.getAppeal().get(0).getOutcomeid().length()>0);
        Assert.assertTrue(appeallist.getAppeal().get(0).getDocid().length()>0);

        Assert.assertTrue(unitappeals.getAppeal().size()>0);
        Boolean containsAppeal = false;
        for (Appeal a : unitappeals.getAppeal()) {
            if (a.getId().equalsIgnoreCase(appeal.getId())) {
                containsAppeal = true;
            }
            Assert.assertTrue(a.getAppadminunit().startsWith(TestData.appeals.adminunitcode1));
        }
        Assert.assertTrue(containsAppeal);

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        Appeal appeal = null;
        try {
            client.putAppeal(TestData.appeals.id1, TestData.projects.id1, TestData.projects.type1,
                    TestData.appeals.appellant2, TestData.appeals.outcomeid2, TestData.projectdocuments.docid1,
                    TestData.appeals.dismissalid1, TestData.appeals.responsedate1, TestData.appeals.ruleid1, 
                    TestData.appeals.statusid1, TestData.appeals.adminunitcode1, TestData.decisions.id1);
            appeal = client.getAppeal(TestData.appeals.id1);
        } catch (Exception ex) {
            Logger.getLogger(AppealTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.appeals.id1, appeal.getId());
        Assert.assertEquals(TestData.projects.id1, appeal.getProjectid());
        Assert.assertEquals(TestData.projects.type1, appeal.getProjecttype());
        Assert.assertEquals(TestData.appeals.appellant2, appeal.getAppellant());
        Assert.assertEquals(TestData.appeals.outcomeid2, appeal.getOutcomeid());
        Assert.assertEquals(TestData.projectdocuments.docid1, appeal.getDocid());
        Assert.assertEquals(TestData.appeals.dismissalid1, appeal.getDismissalid());
        Assert.assertEquals(TestData.appeals.responsedate1, appeal.getResponsedate().toString());
        Assert.assertEquals(TestData.appeals.ruleid1, appeal.getRuleid());
        Assert.assertEquals(TestData.appeals.statusid1, appeal.getStatusid());
        Assert.assertEquals(TestData.appeals.adminunitcode1, appeal.getAppadminunit());
        Assert.assertEquals(TestData.decisions.id1, Integer.parseInt(appeal.getDecisionid()));

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getAppeal(TestData.appeals.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteAppeal(TestData.appeals.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }


    private void post()
    {
        try {
            client.postAppeal(TestData.appeals.id1, TestData.projects.id1, TestData.projects.type1,
                    TestData.appeals.appellant1, TestData.appeals.outcomeid1, TestData.projectdocuments.docid1,
                    TestData.appeals.dismissalid1, TestData.appeals.responsedate1, TestData.appeals.ruleid1,
                    TestData.appeals.statusid1, TestData.appeals.adminunitcode1, TestData.decisions.id1);
        } catch (IOException ex) {
            Logger.getLogger(AppealTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteAppeal(TestData.appeals.id1);
        } catch (IOException ex) {
            Logger.getLogger(AppealTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteAppeal(TestData.appeals.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }

    
    @BeforeClass
    public static void  populateDependencies()
    {
        ProjectDocumentClient pdclient = null;
        ProjectClient projclient = null;
        DecisionClient decisionclient = null;
        try {
            pdclient = new ProjectDocumentClient();
            projclient = new ProjectClient();
            decisionclient = new DecisionClient();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            Assert.fail();
        }

        // Project Documents rely on Projects. Pre-populate a Project.
        ArrayList<String> purposeids1 = new ArrayList<String>();
        purposeids1.add(TestData.projects.purposeid1);
        purposeids1.add(TestData.projects.purposeid2);
        purposeids1.add(TestData.projects.purposeid3);
        try {
            projclient.postProject(TestData.projects.id1, TestData.projects.type1,
                    TestData.projects.name1, TestData.projects.unitcode1, TestData.projects.description1,
                    TestData.projects.lastupdate1, TestData.projects.commentreg1, TestData.projects.wwwlink1,
                    TestData.projects.wwwsummary1, TestData.projects.wwwpub1, TestData.projects.analysistypeid1,
                    TestData.projects.statusid1, TestData.projects.contactname1, TestData.projects.contactphone1,
                    TestData.projects.contactemail1, purposeids1, TestData.projects.expirationdate1, TestData.projects.projectdocumentid1,
                    TestData.projects.sopapub, TestData.projects.sopanew, TestData.projects.sopaheader, TestData.projects.esd, TestData.projects.cenodecision);
        } catch (IOException ex) {
            Logger.getLogger(AppealTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Appeals rely on Project Documents. Pre-populate a Project Document.
        try {
            pdclient.postProjectDocument(TestData.projects.id1, TestData.projects.type1,
                    TestData.projectdocuments.docid1, TestData.projectdocuments.pubflag1, TestData.projectdocuments.wwwlink1,
                    TestData.projectdocuments.docname1, TestData.projectdocuments.description1, TestData.projectdocuments.pdffilesize1,
                    TestData.projectdocuments.sensitiveflag1);
        } catch (IOException ex) {
            Logger.getLogger(AppealTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Appeals rely on Decisions. Pre-populate a Decision.
        try {
            decisionclient.postDecision(TestData.decisions.id1,
                    TestData.projects.id1,
                    TestData.projects.type1,
                    TestData.decisions.name1,
                    TestData.appealstatuses.id1,
                    TestData.decisions.constraint1,
                    TestData.decisions.legalnoticedate1,
                    TestData.decisions.areasize1,
                    TestData.decisions.areaunits1,
                    TestData.decisions.dectype1,
                    TestData.decisions.decdate1);
        } catch (IOException ex) {
            Logger.getLogger(LitigationTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @AfterClass
    public static void  cleanupDependencies()
    {
        ProjectDocumentClient pdclient = null;
        ProjectClient projclient = null;
        DecisionClient decisionclient = null;
        try {
            pdclient = new ProjectDocumentClient();
            projclient = new ProjectClient();
            decisionclient = new DecisionClient();
        } catch (Exception ex) {
            Assert.fail();
        }

        try {
            // Clean up decision
            decisionclient.deleteDecision(TestData.decisions.id1);
        } catch (IOException ex) {
            Logger.getLogger(AppealTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            // Clean up the ProjectDocument
            pdclient.deleteProjectDocument("nepa", TestData.projects.id1, TestData.projectdocuments.docid1);
        } catch (IOException ex) {
            Logger.getLogger(AppealTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            // Clean up the Project
            projclient.deleteProject("nepa", TestData.projects.id1);
        } catch (IOException ex) {
            Logger.getLogger(AppealTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}