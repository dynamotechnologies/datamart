package test.relationship;

import client.MilestoneClient;
import client.ProjectMilestoneClient;
import client.ProjectClient;
import client.ProjectLocationClient;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.projectmilestone.*;


public class ProjectMilestoneTest {
    private ProjectMilestoneClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new ProjectMilestoneClient();
    }

    @Test
    public void testPut()
    {
        put();

        try {
            // Get a list of all entities and send them back. This should delete & recreate them, leaving the data unchanged.
            client.replaceProjectMilestoneList(client.getProjectMilestoneList(TestData.projects.id1));

            // second PUT should overwrite the first
            client.putProjectMilestone( TestData.projects.id1,
                                        TestData.projectmilestones.seqnum1,
                                        TestData.milestones.id1,
                                        TestData.projectmilestones.datestring1,
                                        TestData.projectmilestones.status1,
                                        TestData.projectmilestones.history1);
        } catch (Exception ex) {
            Logger.getLogger(ProjectMilestoneTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        put();

        Projectmilestone milestone = null;
        Projectmilestones milestones1 = null;
        Projectmilestones milestones2 = null;
        try {
            milestone = client.getProjectMilestone(TestData.projects.id1, TestData.projectmilestones.seqnum1);
            milestones1 = client.getProjectMilestoneList(TestData.projects.id1);
            milestones2 = client.getByUnit(TestData.projectlocations.forestid1);
        } catch (Exception ex) {
            Logger.getLogger(ProjectMilestoneTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // make sure the milestone we posted is present in the GET response
        Boolean milestoneWePostedIsPresentInTheGETResponse = false;
        Iterator it = milestones1.getProjectmilestone().iterator();
        while (it.hasNext()) {
            Projectmilestone mstone = (Projectmilestone) it.next();
            if (mstone.getSeqnum().equalsIgnoreCase(TestData.projectmilestones.seqnum1)) {
                milestoneWePostedIsPresentInTheGETResponse = true;
            }
        }
        Assert.assertTrue(milestoneWePostedIsPresentInTheGETResponse);
        Assert.assertEquals(TestData.projects.id1, milestone.getProjectid());
        Assert.assertEquals(TestData.projectmilestones.seqnum1, milestone.getSeqnum());
        Assert.assertEquals(TestData.milestones.id1, milestone.getMilestonetype());
        Assert.assertEquals(TestData.projectmilestones.datestring1, milestone.getDatestring());
        Assert.assertEquals(TestData.projectmilestones.status1, milestone.getStatus());
        Assert.assertEquals(TestData.projectmilestones.history1, milestone.getHistory());

        Assert.assertTrue(milestones2.getProjectmilestone().size()>0);
        Boolean stonefound = false;
        for (Projectmilestone m : milestones2.getProjectmilestone()) {
            if (m.getSeqnum().equalsIgnoreCase(TestData.projectmilestones.seqnum1))
                stonefound = true;
        }
        Assert.assertTrue(stonefound);

        delete();
    }

    @Test
    public void testDelete()
    {
        put();
        delete();
        // Delete the entity

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getProjectMilestoneList(TestData.projects.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteProjectMilestone(TestData.projects.id1, TestData.projectmilestones.seqnum1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }


    private void put()
    {
        try {
            client.putProjectMilestone( TestData.projects.id1,
                                        TestData.projectmilestones.seqnum1,
                                        TestData.milestones.id1,
                                        TestData.projectmilestones.datestring1,
                                        TestData.projectmilestones.status1,
                                        TestData.projectmilestones.history1);
        } catch (Exception ex) {
            Logger.getLogger(ProjectMilestoneTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteProjectMilestone(TestData.projects.id1, TestData.projectmilestones.seqnum1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectMilestoneTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteProjectMilestone(TestData.projects.id1, TestData.projectmilestones.seqnum1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }

    
    @BeforeClass
    public static void  populateDependencies()
    {
        MilestoneClient refmilestoneclient = null;
        ProjectClient projclient = null;
        try {
            refmilestoneclient = new MilestoneClient();
            projclient = new ProjectClient();
        } catch (Exception ex) {
            Logger.getLogger(ProjectMilestoneTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Project Milestones rely on Reference Milestones. Pre-populate a Reference Milestone
        try {
            refmilestoneclient.postMilestone(TestData.milestones.id1, TestData.milestones.name1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectMilestoneTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Project Milestones rely on Projects. Pre-populate a Project.
        ArrayList<String> purposeids1 = new ArrayList<String>();
        purposeids1.add(TestData.projects.purposeid1);
        purposeids1.add(TestData.projects.purposeid2);
        purposeids1.add(TestData.projects.purposeid3);
        try {
            projclient.postProject(TestData.projects.id1, TestData.projects.type1,
                    TestData.projects.name1, TestData.projects.unitcode1, TestData.projects.description1,
                    TestData.projects.lastupdate1, TestData.projects.commentreg1, TestData.projects.wwwlink1,
                    TestData.projects.wwwsummary1, TestData.projects.wwwpub1, TestData.projects.analysistypeid1,
                    TestData.projects.statusid1, TestData.projects.contactname1, TestData.projects.contactphone1,
                    TestData.projects.contactemail1, purposeids1, TestData.projects.expirationdate1, TestData.projects.projectdocumentid1,
                    TestData.projects.sopapub, TestData.projects.sopanew, TestData.projects.sopaheader, TestData.projects.esd, TestData.projects.cenodecision);
        } catch (IOException ex) {
            Logger.getLogger(ProjectMilestoneTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        ArrayList<String> forests = new ArrayList<String>();
        forests.add(TestData.projectlocations.forestid1);
        try {
            new ProjectLocationClient().putProjectLocations( TestData.projects.id1,
                                        TestData.projectlocations.description1,
                                        TestData.projectlocations.legaldesc1,
                                        TestData.projectlocations.latitude1,
                                        TestData.projectlocations.longitude1,
                                        new ArrayList<String>(),
                                        forests,
                                        new ArrayList<String>(),
                                        new ArrayList<String>(),
                                        new ArrayList<String>());
        } catch (Exception ex) {
            Logger.getLogger(ProjectMilestoneTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @AfterClass
    public static void  cleanupDependencies()
    {
        MilestoneClient refmilestoneclient = null;
        ProjectClient projclient = null;
        try {
            refmilestoneclient = new MilestoneClient();
            projclient = new ProjectClient();
        } catch (Exception ex) {
            Assert.fail();
        }

        // Clean up the Project
        try {
            projclient.deleteProject("nepa", TestData.projects.id1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectMilestoneTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Clean up the Reference Milestone
        try {
            refmilestoneclient.deleteMilestone(TestData.milestones.id1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectMilestoneTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }
}