package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.DecisionManagerRemote;
import stateless.DocumentContainerManagerRemote;
import stateless.ObjectionManagerRemote;
import stateless.ProjectManagerRemote;
import us.fed.fs.www.nepa.schema.decision.Decisions;
import us.fed.fs.www.nepa.schema.documentcontainer.Container;
import us.fed.fs.www.nepa.schema.documentcontainer.Containers;
import us.fed.fs.www.nepa.schema.objection.Objections;
import us.fed.fs.www.nepa.schema.project.Project;
import us.fed.fs.www.nepa.schema.project.Sopainfo;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;


@Path ("/projects")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class ProjectResource extends AbstractResource
{
    private DataMartUtil UTIL = null;
    private ProjectManagerRemote projectmanager;
    private DecisionManagerRemote decisionmanager;
    private ObjectionManagerRemote objectionmanager;
    private DocumentContainerManagerRemote containermanager;
    int PROJECT_ID;
    int DECISION_ID;
    int CONTAINER_ID;
    int OBJECTION_ID;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public ProjectResource()
            throws ServletException
    {
        UTIL = DataMartUtil.getInstance();
        if (UTIL==null)
            throw new ServletException("Horror! Could not get a reference to the Datamart Utility singleton! Exiting in shame.");
        
        Properties PROJECT_PROPS = new Properties();
        PROJECT_PROPS.setProperty("schema", "Project.xsd");
        PROJECT_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.project");
        PROJECT_PROPS.setProperty("bean", "ProjectManagerRemote");

        Properties DECISION_PROPS = new Properties();
        DECISION_PROPS.setProperty("schema", "Decision.xsd");
        DECISION_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.decision");
        DECISION_PROPS.setProperty("bean", "DecisionManagerRemote");
        
        
        Properties CONTAINER_PROPS = new Properties();
        CONTAINER_PROPS.setProperty("schema", "DocumentContainer.xsd");
        CONTAINER_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.documentcontainer");
        CONTAINER_PROPS.setProperty("bean", "DocumentContainerManagerRemote");
        
        Properties OBJECTION_PROPS = new Properties();
        OBJECTION_PROPS.setProperty("schema", "Objection.xsd");
        OBJECTION_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.objection");
        OBJECTION_PROPS.setProperty("bean", "ObjectionManagerRemote");
        
        try {
            PROJECT_ID = UTIL.getResourceId(PROJECT_PROPS);
            DECISION_ID = UTIL.getResourceId(DECISION_PROPS);
            CONTAINER_ID = UTIL.getResourceId(CONTAINER_PROPS);
            OBJECTION_ID = UTIL.getResourceId(OBJECTION_PROPS);
            projectmanager = (ProjectManagerRemote) UTIL.getManager(PROJECT_ID);
            decisionmanager = (DecisionManagerRemote) UTIL.getManager(DECISION_ID);
            containermanager = (DocumentContainerManagerRemote) UTIL.getManager(CONTAINER_ID);
            objectionmanager = (ObjectionManagerRemote) UTIL.getManager(OBJECTION_ID);

        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("ProjectResource failed to start. Could not start the bean manager.");
        }
        if (!( projectmanager.ping() && decisionmanager.ping() && containermanager.ping()))
            throw new ServletException("ProjectResource failed to start. Could not ping the bean managers!");
    }


    @POST
    @Path("/{type}")
    public Response post(@PathParam("type") String type, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;
        try {
            Project project = (Project)unmarshal(UTIL, PROJECT_ID, inputxml, Project.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+project.getId());

            // TODO: pull this from the authentication credentials
            project.setAdminapp("pals");

            if (!type.equalsIgnoreCase(project.getType()))
                complainLoudly(new Exception("Type in uri must match type in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // Optional fields must not be null. set to defaults
            // if this changes need to update similar code in the put method
            // and NepaprojectResource::fixProjectDefaults
            if (null==project.getNepainfo().getSopainfo())
                project.getNepainfo().setSopainfo(new Sopainfo());
            if (null==project.getNepainfo().getSopainfo().isPublishflag())
                project.getNepainfo().getSopainfo().setPublishflag(false);
            if (null==project.getNepainfo().getSopainfo().isNewflag())
                project.getNepainfo().getSopainfo().setNewflag(false);
            if (null==project.getNepainfo().getSopainfo().getHeadercategory())
                project.getNepainfo().getSopainfo().setHeadercategory("");

            // All clear. Now do the insert.
            projectmanager.create(project);
            
            createDefaultDocumentContainers(project.getId());
        } catch (URISyntaxException ex) {
            log.error(ex.toString());
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the newly created entity
        return created(uri);
    }

    
    @GET
    @Path("/{type}/{id}")
    public Response get(@PathParam("type") String type, @PathParam("id") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            Project project = projectmanager.get(type, id);

            // Get the query results in a JAXBElement object, marshal and return
            us.fed.fs.www.nepa.schema.project.ObjectFactory objectfactory = new us.fed.fs.www.nepa.schema.project.ObjectFactory();
            JAXBElement<Project> jaxbelement = objectfactory.createProject(project);
            JAXBContext jaxbcontext = UTIL.getContext(PROJECT_ID);
            Marshaller marshaller = jaxbcontext.createMarshaller();
            marshaller.marshal(jaxbelement, sw);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @GET
    @Path("/{type}/{id}/decisions")
    public Response getDecisions(@PathParam("type") String type, @PathParam("id") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            Decisions decisions = decisionmanager.getAllByProject(type, id);



            // Get the query results in a JAXBElement object, marshal and return
            us.fed.fs.www.nepa.schema.decision.ObjectFactory objectfactory = new us.fed.fs.www.nepa.schema.decision.ObjectFactory();
            JAXBElement<Decisions> jaxbelement = objectfactory.createDecisions(decisions);
            JAXBContext jaxbcontext = UTIL.getContext(DECISION_ID);
            Marshaller marshaller = jaxbcontext.createMarshaller();
            marshaller.marshal(jaxbelement, sw);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }

    @GET
    @Path("/{type}/{id}/objections")
    public Response getObjections(@PathParam("type") String type, @PathParam("id") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            Objections objections = objectionmanager.getAllByProject(type, id);

            // Get the query results in a JAXBElement object, marshal and return
            us.fed.fs.www.nepa.schema.objection.ObjectFactory objectfactory = new us.fed.fs.www.nepa.schema.objection.ObjectFactory();
            JAXBElement<Objections> jaxbelement = objectfactory.createObjections(objections);
            JAXBContext jaxbcontext = UTIL.getContext(OBJECTION_ID);
            Marshaller marshaller = jaxbcontext.createMarshaller();
            marshaller.marshal(jaxbelement, sw);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }
    
    @PUT
    @Path("/{type}/{id}")
    public Response put(@PathParam("type") String type, @PathParam("id") String id, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            Project project = (Project) unmarshal(UTIL, PROJECT_ID, inputxml, Project.class);

            // TODO: pull this from the authentication credentials
            project.setAdminapp("pals");

            if (!type.equalsIgnoreCase(project.getType()))
                complainLoudly(new Exception("Type in uri must match type in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);
            if (!id.equalsIgnoreCase(project.getId()))
                complainLoudly(new Exception("Id in uri must match id in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // Optional fields must not be null. set to defaults
            // if this changes need to update similar code in the post method
            // and NepaprojectResource::fixProjectDefaults
            if (null==project.getNepainfo().getSopainfo())
                project.getNepainfo().setSopainfo(new Sopainfo());
            if (null==project.getNepainfo().getSopainfo().isPublishflag())
                project.getNepainfo().getSopainfo().setPublishflag(false);
            if (null==project.getNepainfo().getSopainfo().isNewflag())
                project.getNepainfo().getSopainfo().setNewflag(false);
            if (null==project.getNepainfo().getSopainfo().getHeadercategory())
                project.getNepainfo().getSopainfo().setHeadercategory("");

            // All clear. Now do the update.
            projectmanager.update(project);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"</location>");
    }


    @DELETE
    @Path("/{type}/{id}")
    public Response delete(@PathParam("type") String type, @PathParam("id") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            projectmanager.delete(type, id);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
    
    private void createDefaultDocumentContainers(String id) throws DataMartException{
        
        Containers c = new Containers();
        /**
         * The seemingly arbitrary IDs that are assigned to these containers originated from 
         * the PALS system, which was previously responsible for creating them.
         * They remain in place for the sake of continuity for existing processes and data.
         * 
         */
        
        Container c1 = createContainer(-7, "Assessment", 1, false);
        Container c2 = createContainer(-8, "Forest Plan", 2, false);
        Container c3 = createContainer(-1, "Pre-Scoping", 3, false);
        Container c4 = createContainer(-2, "Scoping", 4, false);
        Container c5 = createContainer(-3, "Analysis", 5, false);
        
        Container objectionC = createContainer(91, "Objections", 1, true);
        Container letterContainer = createContainer(92, "Letters", 1, true);
        Container responseContainer = createContainer(93, "Responses", 2, true);
        objectionC.getContainerOrContainerdoc().add(letterContainer);
        objectionC.getContainerOrContainerdoc().add(responseContainer);
        c5.getContainerOrContainerdoc().add(objectionC);
        
        Container c6 = createContainer(-5, "Decision", 6, false);
        
        Container c7 = createContainer(-4, "Supporting", 7, false);
        Container pubInvolvementC = createContainer(295, "Public Involvement", 1, true);
        Container lettersContainer = createContainer(57, "Original Letters and Submissions", 1, true);
        Container analysisContainer = createContainer(58, "Comment Analysis and Reports", 2, true);
        pubInvolvementC.getContainerOrContainerdoc().add(lettersContainer);
        pubInvolvementC.getContainerOrContainerdoc().add(analysisContainer);
        c7.getContainerOrContainerdoc().add(pubInvolvementC);
        
        Container c8 = createContainer(-6, "Post-Decision", 8, false);
        Container appealsC = createContainer(83, "Appeals", 1, true);
        Container appealsRecContainer = createContainer(84, "Appeals Received", 1, true);
        Container appealsRespContainer = createContainer(87, "Appeal Responses", 2, true);
        appealsC.getContainerOrContainerdoc().add(appealsRecContainer);
        appealsC.getContainerOrContainerdoc().add(appealsRespContainer);
        c8.getContainerOrContainerdoc().add(appealsC);
        
        Container c9 = createContainer(89, "Litigation", 9, true);
        Container litComplaintsC = createContainer(94,"Complaints", 1, true);
        Container litPIC = createContainer(95,"Preliminary Injunctions", 1, true);
        Container litOrdersC = createContainer(96, "Orders", 1, true);
        Container litDecisionsC = createContainer(97, "Decisions", 1, true); 
        c9.getContainerOrContainerdoc().add(litComplaintsC);
        c9.getContainerOrContainerdoc().add(litPIC);
        c9.getContainerOrContainerdoc().add(litOrdersC);
        c9.getContainerOrContainerdoc().add(litDecisionsC);
        
        c.getContainerOrContainerdoc().add(c1);
        c.getContainerOrContainerdoc().add(c2);
        c.getContainerOrContainerdoc().add(c3);
        c.getContainerOrContainerdoc().add(c4);
        c.getContainerOrContainerdoc().add(c5);
        c.getContainerOrContainerdoc().add(c6);
        c.getContainerOrContainerdoc().add(c7);
        c.getContainerOrContainerdoc().add(c8);
        c.getContainerOrContainerdoc().add(c9);
        
        containermanager.create("nepa", id, c);
    }
    
    private Container createContainer(int id, String label, int order, boolean isFixed){
        Container c = new Container();
        c.setContid(id);
        c.setLabel(label);
        c.setOrder(order);
        c.setFixedflag(isFixed);
        return c;
    }
}