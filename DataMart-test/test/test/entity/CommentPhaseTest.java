package test.entity;

import client.CARAProjectClient;
import client.CommentPhaseClient;
import client.ProjectClient;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.datatype.DatatypeConfigurationException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.commentphase.*;


public class CommentPhaseTest {
    private static CommentPhaseClient client;

    @Before
    public void setUp()
            throws IOException, DatatypeConfigurationException
    {
            client = new CommentPhaseClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postCommentPhase(TestData.caraprojects.id1,
                                    TestData.commentphases.id1,
                                    TestData.commentphases.name2,
                                    TestData.commentphases.startdate1,
                                    TestData.commentphases.finishdate1,
                                    TestData.commentphases.lettno1,
                                    TestData.commentphases.lettuniq2,
                                    TestData.commentphases.lettmst2,
                                    TestData.commentphases.lettform1,
                                    TestData.commentphases.lettformplus1,
                                    TestData.commentphases.lettformdupe1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            Assert.assertEquals(TestData.CANT_POST_ALREADY_EXISTS, client.getStatus());
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Commentphase cphase = null;
        Commentphases cphaselist = null;
        try {
            cphase = client.getCommentPhase(TestData.caraprojects.id1, TestData.commentphases.id1);
            cphaselist = client.getCommentPhaseList(TestData.caraprojects.id1);
        } catch (IOException ex) {
            Logger.getLogger(CommentPhaseTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.commentphases.id1, cphase.getPhaseid());
        Assert.assertEquals(TestData.commentphases.name1, cphase.getName());
        Assert.assertEquals(TestData.commentphases.startdate1, cphase.getStartdate().toString());
        Assert.assertEquals(TestData.commentphases.finishdate1, cphase.getFinishdate().toString());
        Assert.assertEquals(TestData.commentphases.lettno1, cphase.getLettno());
        Assert.assertEquals(TestData.commentphases.lettuniq1, cphase.getLettuniq());
        Assert.assertEquals(TestData.commentphases.lettmst1, cphase.getLettmst());
        Assert.assertEquals(TestData.commentphases.lettform1, cphase.getLettform());
        Assert.assertEquals(TestData.commentphases.lettformplus1, cphase.getLettformplus());
        Assert.assertEquals(TestData.commentphases.lettformdupe1, cphase.getLettformdupe());

        Assert.assertTrue(cphaselist.getCommentphase().size()>0);
        Assert.assertTrue(cphaselist.getCommentphase().get(0).getName().length()>0);
        Assert.assertTrue(cphaselist.getCommentphase().get(0).getLettno()>0);
        Assert.assertTrue(cphaselist.getCommentphase().get(0).getLettmst()>0);

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        Commentphase cphase = null;
        try {
            client.putCommentPhase(TestData.caraprojects.id1,
                                    TestData.commentphases.id1,
                                    TestData.commentphases.name2,
                                    TestData.commentphases.startdate1,
                                    TestData.commentphases.finishdate1,
                                    TestData.commentphases.lettno1,
                                    TestData.commentphases.lettuniq2,
                                    TestData.commentphases.lettmst2,
                                    TestData.commentphases.lettform1,
                                    TestData.commentphases.lettformplus1,
                                    TestData.commentphases.lettformdupe1);
            cphase = client.getCommentPhase(TestData.caraprojects.id1, TestData.commentphases.id1);
        } catch (Exception ex) {
            Logger.getLogger(CommentPhaseTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.commentphases.id1, cphase.getPhaseid());
        Assert.assertEquals(TestData.commentphases.name2, cphase.getName());
        Assert.assertEquals(TestData.commentphases.startdate1, cphase.getStartdate().toString());
        Assert.assertEquals(TestData.commentphases.finishdate1, cphase.getFinishdate().toString());
        Assert.assertEquals(TestData.commentphases.lettno1, cphase.getLettno());
        Assert.assertEquals(TestData.commentphases.lettuniq2, cphase.getLettuniq());
        Assert.assertEquals(TestData.commentphases.lettmst2, cphase.getLettmst());
        Assert.assertEquals(TestData.commentphases.lettform1, cphase.getLettform());
        Assert.assertEquals(TestData.commentphases.lettformplus1, cphase.getLettformplus());
        Assert.assertEquals(TestData.commentphases.lettformdupe1, cphase.getLettformdupe());

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getCommentPhase(TestData.caraprojects.id1, TestData.commentphases.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            Assert.assertEquals(TestData.CANT_GET_NOT_FOUND, client.getStatus());
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteCommentPhase(TestData.caraprojects.id1, TestData.commentphases.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            Assert.assertEquals(TestData.CANT_DELETE_DONT_EXIST, client.getStatus());
        }
    }

    private void post()
    {
        try {
            client.postCommentPhase(TestData.caraprojects.id1,
                                    TestData.commentphases.id1,
                                    TestData.commentphases.name1,
                                    TestData.commentphases.startdate1,
                                    TestData.commentphases.finishdate1,
                                    TestData.commentphases.lettno1,
                                    TestData.commentphases.lettuniq1,
                                    TestData.commentphases.lettmst1,
                                    TestData.commentphases.lettform1,
                                    TestData.commentphases.lettformplus1,
                                    TestData.commentphases.lettformdupe1);
        } catch (IOException ex) {
            Logger.getLogger(CommentPhaseTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteCommentPhase(TestData.caraprojects.id1, TestData.commentphases.id1);
        } catch (IOException ex) {
            Logger.getLogger(CommentPhaseTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteCommentPhase(TestData.caraprojects.id1, TestData.commentphases.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }


    @BeforeClass
    public static void  populateDependencies()
    {
        ProjectClient projclient = null;
        CARAProjectClient caraprojclient = null;
        try {
            projclient = new ProjectClient();
            caraprojclient = new CARAProjectClient();
        } catch (Exception ex) {
            Assert.fail();
        }

        // Project Documents rely on Projects. Pre-populate a Project.
        ArrayList<String> purposeids1 = new ArrayList<String>();
        purposeids1.add(TestData.projects.purposeid1);
        purposeids1.add(TestData.projects.purposeid2);
        purposeids1.add(TestData.projects.purposeid3);
        try {
            projclient.postProject(TestData.projects.id1, TestData.projects.type1,
                    TestData.projects.name1, TestData.projects.unitcode1, TestData.projects.description1,
                    TestData.projects.lastupdate1, TestData.projects.commentreg1, TestData.projects.wwwlink1,
                    TestData.projects.wwwsummary1, TestData.projects.wwwpub1, TestData.projects.analysistypeid1,
                    TestData.projects.statusid1, TestData.projects.contactname1, TestData.projects.contactphone1,
                    TestData.projects.contactemail1, purposeids1, TestData.projects.expirationdate1, TestData.projects.projectdocumentid1,
                    TestData.projects.sopapub, TestData.projects.sopanew, TestData.projects.sopaheader, TestData.projects.esd, TestData.projects.cenodecision);
        } catch (IOException ex) {
            Logger.getLogger(ProjectDocumentTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }


        // Link to a CARA ID
        try {
            caraprojclient.linkProject(TestData.projects.id1, TestData.caraprojects.id1);
        } catch (Exception ex) {
            Logger.getLogger(CommentPhaseTest.class.getName()).log(Level.SEVERE, null, ex);
            cleanupDependencies();
            Assert.fail();
        }
    }

    @AfterClass
    public static void cleanupDependencies()
    {
        ProjectClient projclient = null;
        CARAProjectClient caraprojclient = null;
        try {
            projclient = new ProjectClient();
            caraprojclient = new CARAProjectClient();
        } catch (Exception ex) {
            Assert.fail();
        }

        // try to delete the test phase, in case delete() was never run
        try {
            client.deleteCommentPhase(TestData.caraprojects.id1, TestData.commentphases.id1);
        } catch (IOException ex) {
        // don't complain, because this probably just means that delete() was successful
        }

        // Unlink the CARA project
        try {
            caraprojclient.unlinkProject(TestData.caraprojects.id1);
        } catch (Exception ex) {
            Logger.getLogger(CommentPhaseTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Clean up the Project
        try {
            projclient.deleteProject("nepa", TestData.projects.id1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectDocumentTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }
}
