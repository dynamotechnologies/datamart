package ingress.appeals;

import java.util.zip.DataFormatException;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 * This class encapsulates xml ingressed from files validated with the following DTD:
 *
 * <!DOCTYPE appeals [
 * <!ELEMENT appeals (timestamp?, appeal+)>
 * <!ATTLIST appeals xmlns CDATA "http://www.fs.fed.us/pals/xmlns/appeals/">
 * <!ELEMENT timestamp (#PCDATA)>
 * <!ELEMENT appeal (id,projectid?,code,rule,appellant?,outcome,project,response)>
 * <!ELEMENT id (#PCDATA)>
 * <!ELEMENT projectid (#PCDATA)>
 * <!ELEMENT code (#PCDATA)>
 * <!ELEMENT rule (#PCDATA)>
 * <!ELEMENT appellant (#PCDATA)>
 * <!ELEMENT outcome (#PCDATA)>
 * <!ELEMENT project (name,adminunit)>
 * <!ELEMENT name (#PCDATA)>
 * <!ELEMENT adminunit (#PCDATA)>
 * <!ELEMENT response (date,documentid,filesize)>
 * <!ELEMENT date (#PCDATA)>
 * <!ELEMENT documentid (#PCDATA)>
 * <!ELEMENT filesize (#PCDATA)>
 * ]>
 *
 *
 * This class is responsible for encapsulating element `project` as specified above
 */
public class LegacyAppealProject {
    private String name;
    private String adminunit;

    public LegacyAppealProject(Node n)
            throws DataFormatException
    {
        NodeList mynodes = n.getChildNodes();
        for (int i=0; i<mynodes.getLength(); i++)
        {
            if (mynodes.item(i).getNodeName().equals("name"))
                name = mynodes.item(i).getTextContent();
            if (mynodes.item(i).getNodeName().equals("adminunit"))
                adminunit = mynodes.item(i).getTextContent();
        }
        if (!(name.length()>0))
            throw new DataFormatException("name not found.");

        // TODO: find out why the legacy xml does not always populate required element adminunit
//        if (!(adminunit.length()>0))
//            throw new DataFormatException("adminunit not found.");
    }

    public String getName()
    {
        return name;
    }

    public String getAdminUnit()
    {
        return adminunit;
    }
}