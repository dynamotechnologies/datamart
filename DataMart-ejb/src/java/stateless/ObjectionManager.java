package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.objection.*;

import beanutil.*;
import javax.ejb.Stateless;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import static stateless.DecisionManager.createResource;
import us.fed.fs.www.nepa.schema.decision.Decisions;


@Stateless(mappedName="ObjectionManagerRemote")
public class ObjectionManager implements ObjectionManagerRemote
{

    @PersistenceContext
    private EntityManager em;


    @Override
    public void create(Objection resource)
             throws DataMartException
    {
        entity.Objections entity = createEntity(resource);

        // Check for existing entity with this id
        entity.Objections badentity = getEntity(entity.getId());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        em.persist(entity);
    }


    @Override
    public Objection get(String id)
            throws DataMartException
    {
        entity.Objections entity = getEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        return createResource(entity);
    }


    public Objections getAll() throws DataMartException {
        Query query = em.createNamedQuery("Objections.findAll");

        @SuppressWarnings("unchecked")
        Collection<entity.Objections> entitylist = (Collection<entity.Objections>)query.getResultList();

        if (!(entitylist.size()>0))
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        // Loop over the query results and build a list of resources
        Objections objectionslist = new Objections();
        Iterator it = entitylist.iterator();
        while (it.hasNext())
        {
            objectionslist.getObjection().add(createResource((entity.Objections)it.next()));
        }

        return objectionslist;
    }
    public Objections getByUnit(String unitid, String startdate, String enddate)
            throws DataMartException
    {
        // Confirm that this is a valid unit
        entity.RefUnits unit = (entity.RefUnits) DataMartBeanUtil.getEntityById(em,
                "RefUnits", unitid, "Unit");
        if (unit==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);


        // TODO: get startdate and enddate filters working


        // Get objections for this unit
        Query query = em.createQuery("SELECT o FROM Objections o JOIN o.projects p JOIN p.refUnits u WHERE u.id LIKE :id");
        query.setParameter("id", unitid+"%");
        List entitylist =  query.getResultList();

        if (!(entitylist.size()>0))
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        // Loop over the database collection. Each row becomes a new object in the response list
        Iterator it = entitylist.iterator();
        Objections responselist = new Objections();
        while (it.hasNext()) {
            responselist.getObjection().add(ObjectionManager.createResource( (entity.Objections)it.next() ));
        }
        return responselist;
    }

    @Override
    public Objections getAllByProject(String projecttype, String projectid)
            throws DataMartException
    {
        entity.Projects project = DataMartBeanUtil.getProject(em, projecttype, projectid);
        if (project == null) {
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                "Project not found of type=" + projecttype + " and id=" + projectid);
        }
        // Retrieve all entities from the database
        Query query = em.createQuery("SELECT d FROM Objections d WHERE d.projects = :project");
        query.setParameter("project", project);
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Objections responselist = new Objections();
        while (it.hasNext()) {
            responselist.getObjection().add(createResource( (entity.Objections)it.next() ));
        }
        return responselist;
    }


    @Override
    public void update(Objection resource)
             throws DataMartException
    {
        entity.Objections entity = createEntity(resource);

        // Query for the (hopefully) existing entity. Complain if not found.
        entity.Objections existingentity = getEntity(entity.getId());
        if (existingentity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND, "Nonexistent Objection '"+entity.getId()+"' cannot be updated.");

        // Update the entity
        entity = em.merge(entity);
    }


    @Override
    public void delete(String id)
             throws DataMartException
    {
        // Verify that this entity exists
        entity.Objections entity = getEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Could not delete nonexistent Objection '"+id+"'.");

        // Remove the entity itself
        em.remove(entity);
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     *
     * This method does the mapping from entity object to jaxb object
     */
    protected static Objection createResource(entity.Objections entity)
            throws DataMartException
    {
        Objection resource = new Objection();
        resource.setId(entity.getId());
        resource.setObjector(entity.getObjector());

        if (entity.getProjectDocuments() != null)
        {
            resource.setDocid(entity.getProjectDocuments().getDocumentId());
        }
        else
        {
            resource.setDocid("");
        }
        
        resource.setProjectid(entity.getProjects().getPublicId());
        resource.setProjecttype(entity.getProjects().getProjectTypes().getId());
        resource.setResponsedate(
                DataMartBeanUtil.getXMLGregorianFromDate( entity.getResponseDate() ));
        resource.setObjrule(entity.getObjectionRule());
        resource.setFilingstatus(entity.getFilingStatus());
        resource.setStatusdetails(entity.getStatusDetails());
        resource.setOutcomedetails(entity.getOutcomeDetails());
        resource.setPeriodstartdate(
                DataMartBeanUtil.getXMLGregorianFromDate( entity.getPeriodStartDate()));
        resource.setPeriodenddate(
                DataMartBeanUtil.getXMLGregorianFromDate(entity.getPeriodEndDate()));
        resource.setResponseduedate(
                DataMartBeanUtil.getXMLGregorianFromDate(entity.getResponseDueDate()));
        resource.setResponseduedateext(
                DataMartBeanUtil.getXMLGregorianFromDate(entity.getResponseDueDateExt()));
        resource.setOveralloutcome(entity.getOverallOutcome());
        resource.setTitle(entity.getTitle());
        
        return resource;
    }

    /*
     *
     * This method does the mapping from jaxb object to entity object
     */
    private entity.Objections createEntity(Objection resource)
            throws DataMartException
    {
        String projectid = resource.getProjectid(); // Always reference the public ID in error messages

        // Get a reference to the project
        entity.Projects project = DataMartBeanUtil.getProject(em, resource.getProjecttype(), projectid);
        if (project==null)
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION,
                    "Project with type '"+resource.getProjecttype()+"' and ID '"+projectid+"' does not exist.");

        // Get a reference to the project document
        entity.ProjectDocuments projdoc = null;
        if (resource.getDocid() != null && resource.getDocid().length() > 0){
            projdoc = DataMartBeanUtil.getProjectDocument(em, project, resource.getDocid());
        
            if (projdoc==null)
            throw new DataMartException(DataMartException.MISSING_ELEMENT,
                    "Project Document '"+resource.getDocid()+"' does not exist, or is not associated with project '"+projectid+"'.");
        }
        entity.Objections entity = new entity.Objections();
        entity.setId(resource.getId());
        entity.setObjector(resource.getObjector());
        entity.setProjectDocuments(projdoc);
        entity.setProjects(project);
        entity.setResponseDate(
                DataMartBeanUtil.getDateFromXMLGregorian( resource.getResponsedate() ));
        entity.setObjectionRule(resource.getObjrule());
        entity.setFilingStatus(resource.getFilingstatus());
        entity.setStatusDetails(resource.getStatusdetails());
        entity.setOutcomeDetails(resource.getOutcomedetails());
        entity.setPeriodStartDate(
                DataMartBeanUtil.getDateFromXMLGregorian( resource.getPeriodstartdate() ));
        entity.setPeriodEndDate(
                DataMartBeanUtil.getDateFromXMLGregorian( resource.getPeriodenddate() ));
        entity.setResponseDueDate(
                DataMartBeanUtil.getDateFromXMLGregorian( resource.getResponseduedate() ));
        entity.setResponseDueDateExt(
                DataMartBeanUtil.getDateFromXMLGregorian( resource.getResponseduedateext() ));
        
        entity.setOverallOutcome(resource.getOveralloutcome());
        entity.setTitle(resource.getTitle());
        
        return entity;
    }

    // Accepts a String ID as a parameter, and returns the
    //   associated entity, or null if it doesn't exist
    //
    private entity.Objections getEntity(String id)
            throws DataMartException
    {
        Query query = em.createNamedQuery("Objections.findById");
        query.setParameter("id", id);

        entity.Objections objection = (entity.Objections) DataMartBeanUtil.getEntityByQueryAllowNull(query,
                "More than one Objection found with id '"+id+"'.");

        return objection;
    }
}