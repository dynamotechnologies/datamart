package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "MailingListSubscribers")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MailingListSubscribers.findAll", query = "SELECT m FROM MailingListSubscribers m"),
    @NamedQuery(name = "MailingListSubscribers.findById", query = "SELECT m FROM MailingListSubscribers m WHERE m.id = :id"),
    @NamedQuery(name = "MailingListSubscribers.findBySubscriberId", query = "SELECT m FROM MailingListSubscribers m WHERE m.subscriberId = :subscriberId"),
    @NamedQuery(name = "MailingListSubscribers.findByEmail", query = "SELECT m FROM MailingListSubscribers m WHERE m.email = :email"),
    @NamedQuery(name = "MailingListSubscribers.findByLastName", query = "SELECT m FROM MailingListSubscribers m WHERE m.lastName = :lastName"),
    @NamedQuery(name = "MailingListSubscribers.findByFirstName", query = "SELECT m FROM MailingListSubscribers m WHERE m.firstName = :firstName"),
    @NamedQuery(name = "MailingListSubscribers.findByTitle", query = "SELECT m FROM MailingListSubscribers m WHERE m.title = :title"),
    @NamedQuery(name = "MailingListSubscribers.findByOrg", query = "SELECT m FROM MailingListSubscribers m WHERE m.org = :org"),
    @NamedQuery(name = "MailingListSubscribers.findByOrgType", query = "SELECT m FROM MailingListSubscribers m WHERE m.orgType = :orgType"),
    @NamedQuery(name = "MailingListSubscribers.findBySubscriberType", query = "SELECT m FROM MailingListSubscribers m WHERE m.subscriberType = :subscriberType"),
    @NamedQuery(name = "MailingListSubscribers.findByAddressStreet1", query = "SELECT m FROM MailingListSubscribers m WHERE m.addressStreet1 = :addressStreet1"),
    @NamedQuery(name = "MailingListSubscribers.findByAddressStreet2", query = "SELECT m FROM MailingListSubscribers m WHERE m.addressStreet2 = :addressStreet2"),
    @NamedQuery(name = "MailingListSubscribers.findByCity", query = "SELECT m FROM MailingListSubscribers m WHERE m.city = :city"),
    @NamedQuery(name = "MailingListSubscribers.findByState", query = "SELECT m FROM MailingListSubscribers m WHERE m.state = :state"),
    @NamedQuery(name = "MailingListSubscribers.findByZip", query = "SELECT m FROM MailingListSubscribers m WHERE m.zip = :zip"),
    @NamedQuery(name = "MailingListSubscribers.findByProvince", query = "SELECT m FROM MailingListSubscribers m WHERE m.province = :province"),
    @NamedQuery(name = "MailingListSubscribers.findByCountry", query = "SELECT m FROM MailingListSubscribers m WHERE m.country = :country"),
    @NamedQuery(name = "MailingListSubscribers.findByPhone", query = "SELECT m FROM MailingListSubscribers m WHERE m.phone = :phone"),
    @NamedQuery(name = "MailingListSubscribers.findByContactMethod", query = "SELECT m FROM MailingListSubscribers m WHERE m.contactMethod = :contactMethod"),
    @NamedQuery(name = "MailingListSubscribers.findBySubscribedDate", query = "SELECT m FROM MailingListSubscribers m WHERE m.subscribedDate = :subscribedDate")})
public class MailingListSubscribers implements Serializable {
    @Column(name =     "SubscribedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date subscribedDate;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "SubscriberId")
    private int subscriberId;
    @Column(name = "Email")
    private String email;
    @Column(name = "LastName")
    private String lastName;
    @Column(name = "FirstName")
    private String firstName;
    @Column(name = "Title")
    private String title;
    @Column(name = "Org")
    private String org;
    @Column(name = "OrgType")
    private String orgType;
    @Column(name = "SubscriberType")
    private String subscriberType;
    @Column(name = "AddressStreet1")
    private String addressStreet1;
    @Column(name = "AddressStreet2")
    private String addressStreet2;
    @Column(name = "City")
    private String city;
    @Column(name = "State")
    private String state;
    @Column(name = "Zip")
    private String zip;
    @Column(name = "Province")
    private String province;
    @Column(name = "Country")
    private String country;
    @Column(name = "Phone")
    private String phone;
    @Column(name = "ContactMethod")
    private String contactMethod;
    @JoinColumn(name = "ProjectId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Projects projects;

    public MailingListSubscribers() {
    }

    public MailingListSubscribers(Integer id) {
        this.id = id;
    }

    public MailingListSubscribers(Integer id, int subscriberId) {
        this.id = id;
        this.subscriberId = subscriberId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getSubscriberId() {
        return subscriberId;
    }

    public void setSubscriberId(int subscriberId) {
        this.subscriberId = subscriberId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOrg() {
        return org;
    }

    public void setOrg(String org) {
        this.org = org;
    }

    public String getOrgType() {
        return orgType;
    }

    public void setOrgType(String orgType) {
        this.orgType = orgType;
    }

    public String getSubscriberType() {
        return subscriberType;
    }

    public void setSubscriberType(String subscriberType) {
        this.subscriberType = subscriberType;
    }

    public String getAddressStreet1() {
        return addressStreet1;
    }

    public void setAddressStreet1(String addressStreet1) {
        this.addressStreet1 = addressStreet1;
    }

    public String getAddressStreet2() {
        return addressStreet2;
    }

    public void setAddressStreet2(String addressStreet2) {
        this.addressStreet2 = addressStreet2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getContactMethod() {
        return contactMethod;
    }

    public void setContactMethod(String contactMethod) {
        this.contactMethod = contactMethod;
    }

    public Projects getProjects() {
        return projects;
    }

    public void setProjects(Projects projects) {
        this.projects = projects;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MailingListSubscribers)) {
            return false;
        }
        MailingListSubscribers other = (MailingListSubscribers) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.MailingListSubscribers[ id=" + id + " ]";
    }

    public Date getSubscribedDate() {
        return subscribedDate;
    }

    public void setSubscribedDate(Date subscribedDate) {
        this.subscribedDate = subscribedDate;
    }

}
