package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.projectlocation.*;

import beanutil.*;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless(mappedName="ProjectForestManagerRemote")
public class ProjectForestManager implements ProjectForestManagerRemote
{

    @PersistenceContext
    private EntityManager em;

    private String PROJECT_TYPE = "nepa";

    @Override
    public void merge(String projectid, String id)
             throws DataMartException
    {
        if (id.length()!=6)
            throw new DataMartException(DataMartException.BAD_FORMAT,
                    "Unit '" + id + "' is not a forest. Forest codes must be six digits.");
        entity.ProjectForests forest = createEntity(projectid, id);

        // Check for existing entity and grab the internal ID if it exists
        entity.ProjectForests oldforest = getEntity(projectid, id);
        if (oldforest != null)
            forest.setId(oldforest.getId());

        em.merge(forest);
    }


    @Override
    public Forests get(String projectid, String id)
            throws DataMartException
    {
        Forests rval = new Forests();
        entity.ProjectForests forest = getEntity(projectid, id);
        if (forest==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "NEPA Project " + projectid + " is not associated with forest '" + id + "'");

        rval.getForestid().add(forest.getRefUnits().getId());
        return rval;
    }


    @Override
    public void delete(String projectid, String id)
            throws DataMartException
    {
        entity.ProjectForests forest = getEntity(projectid, id);
        if (forest==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "NEPA Project " + projectid + " is not associated with forest '" + id + "'");

        em.remove(forest);
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    private entity.ProjectForests createEntity(String projectid, String id)
            throws DataMartException
    {
        // Get a reference to the project
        entity.Projects project = DataMartBeanUtil.getProject(em, this.PROJECT_TYPE, projectid);
        if (project==null)
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION,
                    "Project with type '" + this.PROJECT_TYPE + "' and ID '" + projectid + "' does not exist.");

        // Get the reference unit associated with this id
        entity.RefUnits ref = (entity.RefUnits) DataMartBeanUtil.getEntityById(em,
                "RefUnits", id, "Unit Code");

        entity.ProjectForests forest = new entity.ProjectForests();
        forest.setProjects(project);
        forest.setRefUnits(ref);
        return forest;
    }


    private entity.ProjectForests getEntity(String projectid, String id)
            throws DataMartException
    {
        // Get a reference to the project
        entity.Projects project = DataMartBeanUtil.getProject(em, this.PROJECT_TYPE, projectid);
        if (project==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Project with type '" + this.PROJECT_TYPE + "' and ID '" + projectid + "' does not exist.");

        // Get the reference unit associated with this id
        entity.RefUnits ref = (entity.RefUnits) DataMartBeanUtil.getEntityById(em,
                "RefUnits", id, "Unit Code");
        if (ref==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Reference Unit with id '" + id + "' does not exist.");

        // Query for the relationship table row
        Query q = em.createQuery("SELECT p FROM ProjectForests p WHERE p.projects = :project AND p.refUnits = :ref");
        q.setParameter("project", project);
        q.setParameter("ref", ref);
        entity.ProjectForests forest = (entity.ProjectForests) DataMartBeanUtil.getEntityByQueryAllowNull(q,
                "NEPA Project " + projectid + " has multiple forest associations with id '" + id + "'");

        return forest;
    }
}