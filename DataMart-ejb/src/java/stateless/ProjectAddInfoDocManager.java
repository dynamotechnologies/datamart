/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateless;

import beanutil.DataMartBeanUtil;
import beanutil.DataMartException;

import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import us.fed.fs.www.nepa.schema.ProjectAddInfoDoc.ProjectAddInfoDoc;
import us.fed.fs.www.nepa.schema.ProjectAddInfoDoc.ProjectAddInfoDocs;
import us.fed.fs.www.nepa.schema.ProjectAdditionalInfo.ProjectAdiitionalInfos;

/**
 *
 * @author gauri
 */

@Stateless(mappedName="ProjectAddInfoDocManagerRemote")
public class ProjectAddInfoDocManager implements ProjectAddInfoDocManagerRemote {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void create(ProjectAddInfoDoc resource) throws DataMartException {
       
        entity.ProjectAddInfoDocs entity = createEntity(resource);

        entity.ProjectAddInfoDocs badentity = retrieveEntity(entity.getAddInfoDocId());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        entity = em.merge(entity);   
    }

    @Override
    public ProjectAddInfoDoc get(String addInfoDocId) throws DataMartException {
   entity.ProjectAddInfoDocs entity = retrieveEntity(new Integer(addInfoDocId));
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return createResource(entity); 
    }

    @Override
    public ProjectAddInfoDocs getAllByProject(String projecttype, String projectid) throws DataMartException {
     // Get Project
        entity.Projects project = DataMartBeanUtil.getProject(em, projecttype, projectid);

        // Retrieve project resource area entities from the database
        Query query = em.createQuery("SELECT a FROM ProjectAdditionalInfo a WHERE a.projectId = :project");
        query.setParameter("project", project);
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        ProjectAddInfoDocs responselist = new ProjectAddInfoDocs();
        entity.ProjectAdditionalInfo projectAdditionalInfo=null;
        while (it.hasNext()) {
            projectAdditionalInfo= (entity.ProjectAdditionalInfo)it.next();
        }
        query = em.createQuery("SELECT a FROM ProjectAddInfoDocs a WHERE a.addInfoId = :addInfoId ORDER BY a.documentOrder");
        query.setParameter("addInfoId", projectAdditionalInfo);
        entitylist =  query.getResultList();
        it = entitylist.iterator();
        while (it.hasNext()) {
            responselist.getProjectAddInfoDoc().add(createResource((entity.ProjectAddInfoDocs)it.next()));
        }
        return responselist;  
    }

    @Override
    public void update(ProjectAddInfoDoc resource) throws DataMartException {
       entity.ProjectAddInfoDocs entity = createEntity(resource);

        entity.ProjectAddInfoDocs existingentity = retrieveEntity(new Integer(resource.getAddInfoDocId()));

        if (existingentity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        entity.setAddInfoDocId(existingentity.getAddInfoDocId());
        entity = em.merge(entity);  
    }

    @Override
    public void delete(String addInfoDocId) throws DataMartException {
        entity.ProjectAddInfoDocs entity = retrieveEntity(new Integer(addInfoDocId));
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));
    }

    @Override
    public Boolean ping() {
    return true;
    }
    
    
    /*
     *
     * This method does the mapping from jaxb object to entity object and vice versa
     */
    private ProjectAddInfoDoc createResource(entity.ProjectAddInfoDocs entity)
            throws DataMartException
    {
        ProjectAddInfoDoc resource = new ProjectAddInfoDoc();
        resource.setAddInfoId(entity.getAddInfoId().getAddInfoId().toString());
        resource.setAddInfoDocId(entity.getAddInfoDocId().toString());
        resource.setDocumentId(entity.getDocumentId().getDocumentId());
        resource.setOrder(entity.getDocumentOrder());
        resource.setDocname(entity.getDocumentId().getLabel());
        resource.setWwwlink(entity.getDocumentId().getLink());
        resource.setPdffilesize(entity.getDocumentId().getPdfSize());
        return resource;
    }

    private entity.ProjectAddInfoDocs createEntity(ProjectAddInfoDoc resource)
            throws DataMartException
    {
        
        Integer addInfoId = new Integer(resource.getAddInfoId()) ;
        entity.ProjectAdditionalInfo addInfo = em.find(entity.ProjectAdditionalInfo.class, addInfoId);
          entity.ProjectDocuments doc = DataMartBeanUtil.getProjectDocument(em, addInfo.getProjectId(), resource.getDocumentId());


        if (addInfo == null) {
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION, "addInfo " + resource.getAddInfoId() + " does not exist");
        }
        entity.ProjectAddInfoDocs entity = new entity.ProjectAddInfoDocs();
     // entity..setProjects(project);
        
        //entity.setAddInfoId(Integer.SIZE);
        entity.setAddInfoId(addInfo);
        entity.setDocumentId(doc);
        entity.setDocumentOrder(resource.getOrder());
        return entity;
    }

    private entity.ProjectAddInfoDocs retrieveEntity(Integer key)
            throws DataMartException
    {
        if(null !=key)
        return em.find(entity.ProjectAddInfoDocs.class, key);
        else
        return null;
    }

   
}