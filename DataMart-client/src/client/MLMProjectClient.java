package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.mlmproject.*;


public class MLMProjectClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public MLMProjectClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.mlmproject", "MLMProject.xsd");
    }


    public void createMlmProject(String mlmId, String projectType, String projectId)
            throws IOException
    {
        Mlmproject project = new Mlmproject();
        project.setMlmid(mlmId);
        project.setProjecttype(projectType);
        project.setProjectid(projectId);
        post(objectfactory.createMlmproject(project), "/mlm/projects");
    }


    public Mlmprojects getAllByUnit(String unitid)
            throws IOException
    {
        return get("/mlm/projects/?unit=" + unitid, Mlmprojects.class);
    }


    public Mlmprojects getAll(String unitid, String forestmatch)
            throws IOException
    {
        String unitparam = "";
        String forestmatchparam = "";

        if (unitid != null) {
            unitparam = "unit=" + unitid;
        }
        if (forestmatch != null) {
            forestmatchparam = "forestmatch=" + forestmatch;
        }

        String paramlist = unitparam + "&" + forestmatchparam;
        if (paramlist.startsWith("&")) {
            paramlist = paramlist.substring(1);
        }
        return get("/mlm/projects/?" + paramlist, Mlmprojects.class);
    }


    public void deleteMlmProject(String mlmid)
            throws IOException
    {
        delete("/mlm/projects/" + mlmid);
    }

}