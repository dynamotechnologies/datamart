package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.application.*;

import beanutil.*;
import java.util.Iterator;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless(mappedName="ApplicationManagerRemote")
public class ApplicationManager implements ApplicationManagerRemote
{

    @PersistenceContext
    private EntityManager em;


    @Override
    public void create(Application resource)
             throws DataMartException
    {
        validate(resource);
        entity.Applications entity = createEntity(resource);

        entity.Applications badentity = createEntity(entity.getId());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        entity = em.merge(entity);
    }

    @Override
    public Applications getAll()
             throws DataMartException
    {
        // Retrieve all entities from the database
        Query query = em.createQuery("SELECT u FROM Applications u");
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Applications responselist = new Applications();
        while (it.hasNext()) {
            responselist.getApplication().add(createResource( (entity.Applications)it.next() ));
        }
        return responselist;
    }

    @Override
    public String getName(String id)
            throws DataMartException
    {
        entity.Applications entity = createEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return entity.getName();
    }

    @Override
    public void update(Application resource)
             throws DataMartException
    {
        validate(resource);
        entity.Applications entity = createEntity(resource);

        if (createEntity(entity.getId())==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        entity = em.merge(entity);
    }

    @Override
    public void delete(String id)
             throws DataMartException
    {
        entity.Applications entity = createEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     *
     * This method does the mapping from jaxb object to entity object and vice versa
     */
    private Application createResource(entity.Applications entity)
            throws DataMartException
    {
        Application resource = new Application();
        resource.setId(entity.getId());
        resource.setName(entity.getName());
        return resource;
    }

    private entity.Applications createEntity(Application resource)
            throws DataMartException
    {
        entity.Applications entity = new entity.Applications();
        entity.setId(resource.getId());
        entity.setName(resource.getName());
        return entity;
    }

    // This version of overloaded method createEntity() accepts a String ID as a parameter, and returns the
    //   associated entity, or null if it doesn't exist
    //
    private entity.Applications createEntity(String id)
            throws DataMartException
    {
        return em.find(entity.Applications.class, id);
    }

    private void validate(Application resource)
            throws DataMartException
    {
        DataMartBeanUtil.confirmNotNull(resource.getName(), "Name");
    }
}