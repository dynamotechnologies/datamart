/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.UserManagerRemote;
import us.fed.fs.www.nepa.schema.user.ObjectFactory;
import us.fed.fs.www.nepa.schema.user.User;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;

/**
 *
 * @author mhsu
 */
@Path ("/users")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class UserResource extends AbstractResource
{
    private UserManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public UserResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "User.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.user");
        MY_PROPS.setProperty("bean", "UserManagerRemote");
        try {
            beanmanager = (UserManagerRemote) init(UserManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            java.io.StringWriter sw = new java.io.StringWriter();
            java.io.PrintWriter pw = new java.io.PrintWriter(sw);
            ex.printStackTrace(pw);
            String trace = sw.toString();
            throw new ServletException("UserResource failed to start. Could not start the bean manager." +
                                        "\n" + trace + "\n" + UserManagerRemote.class.toString());
        }
        if (!beanmanager.ping())
            throw new ServletException("UserResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;
        try {
            User user = (User)unmarshal(inputxml, User.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+user.getShortname());

            // All clear. Now do the insert.
            beanmanager.create(user);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the newly created entity
        return created(uri);
    }


    @GET
    @Path("/{shortname}")
    public Response get(@PathParam("shortname") String shortname, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Retrieve the specified user
            User user = beanmanager.get(shortname);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<User> jaxbelement = objectfactory.createUser(user);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @PUT
    @Path("/{shortname}")
    public Response put(@PathParam("shortname") String shortname, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        User user = null;
        try {
            user = (User)unmarshal(inputxml, User.class);

            if (!shortname.equalsIgnoreCase(user.getShortname()))
                complainLoudly(new Exception("Shortname in uri must match shortname in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.update(user);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"</location>");
    }


    @DELETE
    @Path("/{shortname}")
    public Response delete(@PathParam("shortname") String shortname, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(shortname);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}
