package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "RefLitigationOutcomes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RefLitigationOutcomes.findAll", query = "SELECT r FROM RefLitigationOutcomes r"),
    @NamedQuery(name = "RefLitigationOutcomes.findById", query = "SELECT r FROM RefLitigationOutcomes r WHERE r.id = :id"),
    @NamedQuery(name = "RefLitigationOutcomes.findByName", query = "SELECT r FROM RefLitigationOutcomes r WHERE r.name = :name")})
public class RefLitigationOutcomes implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "Name")
    private String name;
    @OneToMany(mappedBy = "refLitigationOutcomes")
    private Collection<Litigations> litigationsCollection;

    public RefLitigationOutcomes() {
    }

    public RefLitigationOutcomes(Integer id) {
        this.id = id;
    }

    public RefLitigationOutcomes(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Collection<Litigations> getLitigationsCollection() {
        return litigationsCollection;
    }

    public void setLitigationsCollection(Collection<Litigations> litigationsCollection) {
        this.litigationsCollection = litigationsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RefLitigationOutcomes)) {
            return false;
        }
        RefLitigationOutcomes other = (RefLitigationOutcomes) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RefLitigationOutcomes[ id=" + id + " ]";
    }

}
