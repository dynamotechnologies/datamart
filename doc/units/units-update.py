#!/usr/bin/python

import xml.sax
import unitshandler
import pprint
import time
import locale
import sys

def usage():
	print """
	Usage: units-update.py [FILENAME]
	Usage: cat inputfile | units-update.py

	This program takes an XML file and uses it to update Unit records
	It cannot create new Units
	If the file references a nonexistent Unit it will throw an error

	Format is:

		<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
		<units xmlns:ns1="http://www.fs.fed.us/nepa/schema/unitswww">
			<timestamp>20110110</timestamp>
			<unit>
				<code>110000</code>
				<wwwlink>http://www.fs.fed.us/</wwwlink>
				<streetaddress1>1400 Independence Ave., SW</streetaddress1>
				<streetaddress2>Mailstop: 1104</streetaddress2>
				<city>Washington</city>
				<state>DC</state>
				<zip>20250-1104</zip>
				<phone>(202) 205-0895</phone>
				<projectmapflag>0</projectmapflag>
			</unit>
		</units>
	"""

"""
====
MAIN
====
"""

if (len(sys.argv) > 2):
	usage()
	sys.exit()

inputstream = None

if (len(sys.argv) > 1):
	fname = sys.argv[1]
	print "Getting input from file " + fname
	inputstream = open(fname, "r")
else:
	print "Getting input from stdin"
	inputstream = sys.stdin

# 
# TODO: Save copy of old data
#

parser = xml.sax.make_parser()
handler = unitshandler.UnitsHandler()
parser.setContentHandler(handler)
parser.parse(inputstream)

print "Finished at " + time.strftime(locale.nl_langinfo(locale.D_T_FMT), time.gmtime())

