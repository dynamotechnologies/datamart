#!/bin/bash

# APPDIR=/app/DataMart/subscriber-update/
APPDIR=$(pwd)			# TESTING
LOGFILE=$APPDIR/log
AUTOSUBSCRIBE="python $APPDIR/autosubscribe.py"
MAIL=/bin/mail
# MAILLIST="fs-drm-alerts@phaseonecg.com"
MAILLIST="mhsu@localhost"	# TESTING

die() {
  msg=$1
  id=$(id -nu)
  host=$(hostname)
  email="$id at $host said: $msg"
  body=$(tail -7 $LOGFILE)
  errs=$(cat $LOGFILE | awk 'BEGIN { RS=""; FS="==========" } { print $(NF); }' | grep ERROR)
  subject="$host: autosubscribe failure"
  echo -e "${email}\n${body}\n${errs}" | $MAIL -s "$subject" "$MAILLIST"
  echo "Ended with errors" > /dev/stderr
  exit 1
}

touch $LOGFILE
if [ "$?" -ne 0 ]; then
  die "Could not write to logfile, check permissions?"
fi

start_time=$(date)
$AUTOSUBSCRIBE >> $LOGFILE 2>&1
rc=$?
end_time=$(date)
echo -e "Run started at $start_time" >> $LOGFILE
echo -e "Run ended at $end_time" >> $LOGFILE
if [ "$rc" -ne 0 ]; then
  die "Job ended with errors, check logfile"
fi

