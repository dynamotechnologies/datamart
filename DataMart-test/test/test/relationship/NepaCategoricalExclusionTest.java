package test.relationship;

import client.NepaCategoricalExclusionClient;
import client.ProjectClient;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.nepacategoricalexclusion.*;


public class NepaCategoricalExclusionTest {
    private NepaCategoricalExclusionClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new NepaCategoricalExclusionClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should succeed, because there is no PUT
        try {
            client.postNepaCategoricalExclusion(TestData.projects.id1, TestData.nepacategoricalexclusions.id1);
        } catch (IOException ex) {
            Logger.getLogger(NepaCategoricalExclusionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Nepacategoricalexclusion nce = null;
        Nepacategoricalexclusions ncelist = null;
        try {
            nce = client.getNepaCategoricalExclusion(TestData.projects.id1, TestData.nepacategoricalexclusions.id1);
            ncelist = client.getNepaCategoricalExclusionList(TestData.projects.id1);
        } catch (IOException ex) {
            Logger.getLogger(NepaCategoricalExclusionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.nepacategoricalexclusions.id1, nce.getCategoricalexclusionid());
        Assert.assertTrue(ncelist.getNepacategoricalexclusion().get(0).getCategoricalexclusionid()>0);

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getNepaCategoricalExclusion(TestData.projects.id1, TestData.nepacategoricalexclusions.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteNepaCategoricalExclusion(TestData.projects.id1, TestData.nepacategoricalexclusions.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }


    private void post()
    {
        try {
            client.postNepaCategoricalExclusion(TestData.projects.id1, TestData.nepacategoricalexclusions.id1);
        } catch (IOException ex) {
            Logger.getLogger(NepaCategoricalExclusionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteNepaCategoricalExclusion(TestData.projects.id1, TestData.nepacategoricalexclusions.id1);
        } catch (IOException ex) {
            Logger.getLogger(NepaCategoricalExclusionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteNepaCategoricalExclusion(TestData.projects.id1, TestData.nepacategoricalexclusions.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }


    @BeforeClass
    public static void  populateDependencies()
    {
        ProjectClient projclient = null;
        try {
            projclient = new ProjectClient();
        } catch (Exception ex) {
            Assert.fail();
        }

        // NepaCategoricalExclusions rely on Projects. Pre-populate a Project.
        ArrayList<String> purposeids1 = new ArrayList<String>();
        purposeids1.add(TestData.projects.purposeid1);
        purposeids1.add(TestData.projects.purposeid2);
        purposeids1.add(TestData.projects.purposeid3);
        try {
            projclient.postProject(TestData.projects.id1, TestData.projects.type1,
                    TestData.projects.name1, TestData.projects.unitcode1, TestData.projects.description1,
                    TestData.projects.lastupdate1, TestData.projects.commentreg1, TestData.projects.wwwlink1,
                    TestData.projects.wwwsummary1, TestData.projects.wwwpub1, TestData.projects.analysistypeid1,
                    TestData.projects.statusid1, TestData.projects.contactname1, TestData.projects.contactphone1,
                    TestData.projects.contactemail1, purposeids1, TestData.projects.expirationdate1, TestData.projects.projectdocumentid1,
                    TestData.projects.sopapub, TestData.projects.sopanew, TestData.projects.sopaheader, TestData.projects.esd, TestData.projects.cenodecision);
        } catch (IOException ex) {
            Logger.getLogger(NepaCategoricalExclusionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    @AfterClass
    public static void cleanupDependencies()
    {
        ProjectClient projclient = null;
        try {
            projclient = new ProjectClient();
        } catch (Exception ex) {
            Assert.fail();
        }

        // Clean up the Project
        try {
            projclient.deleteProject("nepa", TestData.projects.id1);
        } catch (IOException ex) {
            Logger.getLogger(NepaCategoricalExclusionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }
}