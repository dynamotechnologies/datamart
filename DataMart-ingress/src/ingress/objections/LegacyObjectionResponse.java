package ingress.objections;

import java.util.zip.DataFormatException;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 * This class encapsulates xml ingressed from files validated with the following DTD:
 *
 * <!DOCTYPE objections [
 * <!ELEMENT objections (timestamp?, objection+)>
 * <!ATTLIST objections xmlns CDATA "http://www.fs.fed.us/pals/xmlns/objections/">
 * <!ELEMENT timestamp (#PCDATA)>
 * <!ELEMENT objection (id?,code,rule,objector,project,response)>
 * <!ELEMENT id (#PCDATA)>
 * <!ELEMENT code (#PCDATA)>
 * <!ELEMENT rule (#PCDATA)>
 * <!ELEMENT objector (#PCDATA)>
 * <!ELEMENT project (name,adminunit)>
 * <!ELEMENT name (#PCDATA)>
 * <!ELEMENT adminunit (#PCDATA)>
 * <!ELEMENT response (date,documentid,filesize)>
 * <!ELEMENT date (#PCDATA)>
 * <!ELEMENT documentid (#PCDATA)>
 * <!ELEMENT filesize (#PCDATA)>
 * ]>
 *
 *
 * This class is responsible for encapsulating element `response` as specified above
 */
public class LegacyObjectionResponse {
    private String date;
    private String documentid;
    private String filesize;

    public LegacyObjectionResponse(Node n)
            throws DataFormatException
    {
        NodeList mynodes = n.getChildNodes();
        for (int i=0; i<mynodes.getLength(); i++)
        {
            if (mynodes.item(i).getNodeName().equals("date"))
                date = mynodes.item(i).getTextContent();
            if (mynodes.item(i).getNodeName().equals("documentid"))
                documentid = mynodes.item(i).getTextContent();
            if (mynodes.item(i).getNodeName().equals("filesize"))
                filesize = mynodes.item(i).getTextContent();
        }
        if (!(date.length()>0))
            throw new DataFormatException("date not found.");
        if (!(documentid.length()>0))
            throw new DataFormatException("documentid not found.");
        if (!(filesize.length()>0))
            throw new DataFormatException("filesize not found.");
    }

    public String getDate()
    {
        return date;
    }

    public String getDocumentId()
    {
        return documentid;
    }

    public String getFileSize()
    {
        return filesize;
    }
}