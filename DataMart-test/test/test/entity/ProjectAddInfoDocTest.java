/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.entity;

import client.ProjectAddInfoDocsClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.ProjectAddInfoDoc.ProjectAddInfoDoc;
import us.fed.fs.www.nepa.schema.ProjectAddInfoDoc.ProjectAddInfoDocs;

/**
 *
 * @author gauri
 */

public class ProjectAddInfoDocTest {
    private ProjectAddInfoDocsClient client;
    

    @Before
    public void setUp()
            throws IOException
    {
        client = new ProjectAddInfoDocsClient();
       
    }

    @Test
    public void testPost()
    {
        // This POST should fail, because it violates a constraint. The unit code is bad
        try {
           addAddInfoDocs();
            // If the above statement did not throw an exception then something has gone terribly wrong
            //Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.BAD_PARENTS)
                Assert.fail();
        } catch (Exception ex) {
            Logger.getLogger(ProjectAddInfoDocTest.class.getName()).log(Level.SEVERE, null, ex);
             Assert.fail();
        }

        

        
    }

    @Test
    public void testGet()
    {
     

        ProjectAddInfoDocs project = null;
         
        try {
            project = client.getAddInfoDocs("17735");
        } catch (IOException ex) {
            Logger.getLogger(ProjectAddInfoDocTest.class.getName()).log(Level.SEVERE, null, ex);
             Assert.fail();
        }
            
          
       

       
     

       
    }

    @Test
    public void testPut()
    {
        

        ProjectAddInfoDocs project = null;
        try {
            modifyAddInfoDocs();
            project = client.getAddInfoDocs("17735");
        } catch (Exception ex) {
            Logger.getLogger(ProjectAddInfoDocTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

       
    }

   
    
 


    @Test
    public void testDelete()
    {
        

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            deleteAddInfoDocs();
          
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        } catch (Exception ex) {
            Logger.getLogger(ProjectAddInfoDocTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
      
    }

 
    @After
    public void cleanFailed()
            throws Exception
    {
      
    }


    @BeforeClass
    public static void populateDependencies()
    {
        // No dependencies
    }

    @AfterClass
    public static void cleanupDependencies()
    {
        // No cleanup
    }
    public static void addAddInfoDocs() throws Exception{
	    	
	    	client.ProjectAddInfoDocsClient client = new client.ProjectAddInfoDocsClient();
	    	us.fed.fs.www.nepa.schema.ProjectAddInfoDoc.ProjectAddInfoDoc p = new us.fed.fs.www.nepa.schema.ProjectAddInfoDoc.ProjectAddInfoDoc();
	    	p.setAddInfoId("1");
                p.setDocumentId("FSTST3_1372455");
                p.setOrder(3);
               	client.postAddInfoDocs("17735",p);
	    }
    
	 public static void deleteAddInfoDocs() throws Exception{
	    	
	    	client.ProjectAddInfoDocsClient client = new client.ProjectAddInfoDocsClient();
                client.deleteAddInfoDocs("17735","3");
         }   
         
          public static void modifyAddInfoDocs() throws Exception{
	    	client.ProjectAddInfoDocsClient client = new client.ProjectAddInfoDocsClient();
  	  	us.fed.fs.www.nepa.schema.ProjectAddInfoDoc.ProjectAddInfoDoc p = new us.fed.fs.www.nepa.schema.ProjectAddInfoDoc.ProjectAddInfoDoc();
	        p.setAddInfoDocId("3");
                p.setAddInfoId("1");
                p.setDocumentId("FSTST3_1372455");
                p.setOrder(5);
	    	client.putAddInfoDocs("17735",p);
         }   
}
