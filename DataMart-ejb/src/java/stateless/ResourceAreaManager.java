package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.resourcearea.*;

import beanutil.*;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Iterator;
import java.util.List;

@Stateless(mappedName="ResourceAreaManagerRemote")
public class ResourceAreaManager implements ResourceAreaManagerRemote {

    @PersistenceContext
    private EntityManager em;

    public void create(Resourcearea resource) throws DataMartException {
        entity.ResourceAreas entity = createEntity(resource);

        entity.ResourceAreas badentity = retrieveEntity(resource.getResourceareaid());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS, "Resource area " + resource.getResourceareaid() + " already exists");

        entity = em.merge(entity);
    }

    public Resourcearea get(Integer id) throws DataMartException {
        entity.ResourceAreas entity = retrieveEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return createResource(entity);
    }

    public Resourceareas getListByUnitId(String unitid) throws DataMartException {
        entity.RefUnits unit = em.find(entity.RefUnits.class, unitid);

        Resourceareas result = new Resourceareas();
        Query query = em.createQuery("SELECT a FROM ResourceAreas a WHERE a.refUnits = :unit");
        query.setParameter("unit", unit);
        for (Object i : query.getResultList()) {
            entity.ResourceAreas ra = (entity.ResourceAreas)i;
            result.getResourcearea().add(createResource(ra));
        }
        return result;
    }

    public void update(Resourcearea resource) throws DataMartException {
        entity.ResourceAreas entity = createEntity(resource);

        if (retrieveEntity(entity.getId())==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND, "Resource area " + entity.getId() + " does not exist");

        entity = em.merge(entity);
    }

    public void delete(Integer id) throws DataMartException {
        entity.ResourceAreas entity = retrieveEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        
        // Cascade deletes to ProjectResourceArea
        Query query = em.createQuery("SELECT a FROM ProjectResourceAreas a WHERE a.resourceAreas = :resourcearea");
        query.setParameter("resourcearea", entity);
        for (Object i : query.getResultList()) {
            entity.ProjectResourceAreas pra = (entity.ProjectResourceAreas)i;
            em.remove(em.merge(pra));
        }

        em.remove(em.merge(entity));
    }

    public Boolean ping() {
        return true;
    }

    /*
     * Create XML from EJB
     */
    private Resourcearea createResource(entity.ResourceAreas entity)
            throws DataMartException
    {
        Resourcearea resource = new Resourcearea();
        resource.setResourceareaid(entity.getId());
        resource.setUnitid(entity.getRefUnits().getId());
        resource.setDescription(entity.getDescription());
        return resource;
    }

    /*
     * Create EJB from XML
     */
    private entity.ResourceAreas createEntity(Resourcearea resource)
            throws DataMartException
    {
        entity.RefUnits unit = em.find(entity.RefUnits.class, resource.getUnitid());
        if (unit == null) {
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION, "Unit " + resource.getUnitid() + " does not exist");
        }

        entity.ResourceAreas entity = new entity.ResourceAreas();
        entity.setId(resource.getResourceareaid());
        entity.setRefUnits(unit);
        entity.setDescription(resource.getDescription());
        return entity;
    }

    private entity.ResourceAreas retrieveEntity(Integer resourceareaid)
            throws DataMartException
    {
        return em.find(entity.ResourceAreas.class, resourceareaid);
    }
}
