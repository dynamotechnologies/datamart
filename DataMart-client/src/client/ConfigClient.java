package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.config.*;


public class ConfigClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public ConfigClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.config", "Config.xsd");
    }


    public void postConfig(String key, String value)
            throws IOException
    {
        Config config = new Config();
        config.setKey(key);
        config.setValue(value);
        postConfig(config);
    }


    public void postConfig(Config config)
            throws IOException
    {
        post(objectfactory.createConfig(config), "/ref/configs");
    }


    public Config getConfig(String key)
            throws IOException
    {
        return get("/ref/configs/" + key, Config.class);
    }


    public Configs getConfigList()
            throws IOException
    {
        return get("/ref/configs", Configs.class);
    }


    public void putConfig(String key, String value)
            throws IOException
    {
        Config config = new Config();
        config.setKey(key);
        config.setValue(value);
        putConfig(config);
    }


    public void putConfig(Config config)
            throws IOException
    {
        put(objectfactory.createConfig(config), "/ref/configs/" + config.getKey());
    }


    public void deleteConfig(String key)
            throws IOException
    {
        delete("/ref/configs/" + key);
    }
}