package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.MilestoneManagerRemote;
import us.fed.fs.www.nepa.schema.milestone.Milestone;
import us.fed.fs.www.nepa.schema.milestone.Milestones;
import us.fed.fs.www.nepa.schema.milestone.ObjectFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;


@Path ("/ref/milestones")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class MilestoneResource extends AbstractResource
{
    private MilestoneManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public MilestoneResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "Milestone.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.milestone");
        MY_PROPS.setProperty("bean", "MilestoneManagerRemote");
        try {
            beanmanager = (MilestoneManagerRemote) init(MilestoneManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("MilestoneResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("MilestoneResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;
        try {
            Milestone milestone = (Milestone)unmarshal(inputxml, Milestone.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+milestone.getId());

            // All clear. Now do the insert.
            beanmanager.create(milestone);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the newly created entity
        return created(uri);
    }


    @GET
    public Response get(@Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get a dump of all the Milestones in the datamart
            Milestones milestonelist = beanmanager.getAll();

            // Return 404 if none exist
    //        if (milestonelist.getMilestone().isEmpty())
   //             notFound();

            // Get the query results in a JAXBElement<Milestones> object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Milestones> jaxbelement = objectfactory.createMilestones(milestonelist);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @GET
    @Path("/{id}")
    public Response get(@PathParam("id") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the name of the specified unit
            String name = beanmanager.getName(id);
            Milestone milestone = new Milestone();
            milestone.setId(id);
            milestone.setName(name);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Milestone> jaxbelement = objectfactory.createMilestone(milestone);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @PUT
    @Path("/{id}")
    public Response put(@PathParam("id") String id, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        Milestone milestone = null;
        try {
            milestone = (Milestone) unmarshal(inputxml, Milestone.class);

            if (!id.equalsIgnoreCase(milestone.getId()))
                complainLoudly(new Exception("Id in uri must match id in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.update(milestone);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"/"+milestone.getId()+"</location>");
    }


    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(id);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}
