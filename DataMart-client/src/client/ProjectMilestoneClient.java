package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.projectmilestone.*;


public class ProjectMilestoneClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public ProjectMilestoneClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.projectmilestone", "ProjectMilestone.xsd");
    }


    public Projectmilestone getProjectMilestone(String projectid, String id)
            throws IOException
    {
        return get("/projects/nepa/" + projectid + "/milestones/" + id, Projectmilestone.class);
    }


    public Projectmilestones getProjectMilestoneList(String projectid)
            throws IOException
    {
        return get("/projects/nepa/" + projectid + "/milestones/", Projectmilestones.class);
    }


    public Projectmilestones getByUnit(String unitcode)
            throws IOException
    {
        return get("/units/" + unitcode + "/projectmilestones/", Projectmilestones.class);
    }
    

    public void putProjectMilestone(String projectid, String id, String type, String datestring, String status, String history)
            throws IOException
    {
        Projectmilestone milestone = new Projectmilestone();
        milestone.setProjectid(projectid);
        milestone.setSeqnum(id);
        milestone.setMilestonetype(type);
        milestone.setDatestring(datestring);
        milestone.setStatus(status);
        milestone.setHistory(history);
        putProjectMilestone(milestone);
    }


    public void putProjectMilestone(Projectmilestone milestone)
            throws IOException
    {
        put(objectfactory.createProjectmilestone(milestone), "/projects/nepa/" + milestone.getProjectid() + "/milestones/" + milestone.getSeqnum());
    }

    public void replaceProjectMilestoneList(Projectmilestones milestones)
            throws IOException
    {
        put(objectfactory.createProjectmilestones(milestones), "/projects/nepa/" + milestones.getProjectmilestone().get(0).getProjectid() + "/milestones");
    }


    public void deleteProjectMilestone(String projectid, String id)
            throws IOException
    {
        delete("/projects/nepa/" + projectid + "/milestones/" + id);
    }
}