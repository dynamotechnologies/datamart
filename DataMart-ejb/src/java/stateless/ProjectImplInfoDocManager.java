/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateless;

import beanutil.DataMartBeanUtil;
import beanutil.DataMartException;
import java.util.Iterator;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import us.fed.fs.www.nepa.schema.ProjectAddInfoDoc.ProjectAddInfoDocs;

import us.fed.fs.www.nepa.schema.ProjectImplInfoDoc.ProjectImplInfoDoc;
import us.fed.fs.www.nepa.schema.ProjectImplInfoDoc.ProjectImplInfoDocs;

/**
 *
 * @author gauri
 */
@Stateless(mappedName="ProjectImplInfoDocManagerRemote")
public class ProjectImplInfoDocManager implements ProjectImplInfoDocManagerRemote {
    @PersistenceContext
    private EntityManager em;
    @Override
    public void create(ProjectImplInfoDoc resource) throws DataMartException {
     entity.ProjectImplInfoDocs entity = createEntity(resource);

        entity.ProjectImplInfoDocs badentity = retrieveEntity(entity.getImplInfoDocId());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        entity = em.merge(entity);  
    }

    @Override
    public ProjectImplInfoDoc get(String implInfoDocId) throws DataMartException {
        entity.ProjectImplInfoDocs entity = retrieveEntity(new Integer(implInfoDocId));
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return createResource(entity); 
    }

    @Override
    public ProjectImplInfoDocs getAllByProject(String projecttype, String projectid) throws DataMartException {
       entity.Projects project = DataMartBeanUtil.getProject(em, projecttype, projectid);

        // Retrieve project resource area entities from the database
        Query query = em.createQuery("SELECT a FROM ProjectImplInfo a WHERE a.projectid = :project");
        query.setParameter("project", project);
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        ProjectImplInfoDocs responselist = new ProjectImplInfoDocs();
        entity.ProjectImplInfo projectImplInfo=null;
        while (it.hasNext()) {
            projectImplInfo= (entity.ProjectImplInfo)it.next();
        }
        query = em.createQuery("SELECT a FROM ProjectImplInfoDocs a WHERE a.implInfoId = :implInfoId ORDER BY a.documentOrder");
        query.setParameter("implInfoId", projectImplInfo);
        entitylist =  query.getResultList();
        it = entitylist.iterator();
        while (it.hasNext()) {
            responselist.getProjectImplInfoDoc().add(createResource((entity.ProjectImplInfoDocs)it.next()));
        }
        return responselist; 
    }

    @Override
    public void update(ProjectImplInfoDoc resource) throws DataMartException {
        entity.ProjectImplInfoDocs entity = createEntity(resource);

        entity.ProjectImplInfoDocs existingentity = retrieveEntity(new Integer(resource.getImplInfoDocId()));

        if (existingentity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        entity.setImplInfoDocId(existingentity.getImplInfoDocId());
        entity = em.merge(entity);     
    }

    @Override
    public void delete(String implInfoDocId) throws DataMartException {
        entity.ProjectImplInfoDocs entity = retrieveEntity(new Integer(implInfoDocId));
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));    
    }

    @Override
    public Boolean ping() {
      return true;
    }
        /*
     *
     * This method does the mapping from jaxb object to entity object and vice versa
     */
    private ProjectImplInfoDoc createResource(entity.ProjectImplInfoDocs entity)
            throws DataMartException
    {
        ProjectImplInfoDoc resource = new ProjectImplInfoDoc();
        resource.setImplInfoId(entity.getImplInfoId().getImplId().toString());
        resource.setImplInfoDocId(entity.getImplInfoDocId().toString());
        resource.setDocumentId(entity.getDocumentId().getDocumentId());
        resource.setOrder(entity.getDocumentOrder());
        resource.setDocname(entity.getDocumentId().getLabel());
        resource.setWwwlink(entity.getDocumentId().getLink());
        resource.setPdffilesize(entity.getDocumentId().getPdfSize());
        return resource;
    }

    private entity.ProjectImplInfoDocs createEntity(ProjectImplInfoDoc resource)
            throws DataMartException
    {
        
        Integer implInfoId = new Integer(resource.getImplInfoId()) ;
        entity.ProjectImplInfo implInfo = em.find(entity.ProjectImplInfo.class, implInfoId);
          entity.ProjectDocuments doc = DataMartBeanUtil.getProjectDocument(em, implInfo.getProjectid(), resource.getDocumentId());


        if (implInfo == null) {
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION, "implInfo " + resource.getImplInfoId() + " does not exist");
        }
        entity.ProjectImplInfoDocs entity = new entity.ProjectImplInfoDocs();
     // entity..setProjects(project);
        
        //entity.setAddInfoId(Integer.SIZE);
        entity.setImplInfoId(implInfo);
        entity.setDocumentId(doc);
        entity.setDocumentOrder(resource.getOrder());
        return entity;
    }

    private entity.ProjectImplInfoDocs retrieveEntity(Integer key)
            throws DataMartException
    {
        if(null !=key)
        return em.find(entity.ProjectImplInfoDocs.class, key);
        else
        return null;
    }
}
