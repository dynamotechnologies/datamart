package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.project.*;
import us.fed.fs.www.nepa.schema.projectlisting.*;

import beanutil.*;
import entity.ProjectPurposes;
import entity.RefUnits;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


@Stateless(mappedName="ProjectManagerRemote")
public class ProjectManager implements ProjectManagerRemote
{
    @PersistenceContext
    private EntityManager em;

   
    @Override
    public void create(Project resource)
             throws DataMartException
    {
        entity.Projects entity = createEntity(resource);

        // Check for existing entity with this Type and Public ID
        entity.Projects badentity = getEntity(entity.getProjectTypes().getId(), entity.getPublicId());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS,
                            "Project with type '" + entity.getProjectTypes().getId() + "' and id '" +
                            entity.getPublicId()+"' already exists.");

        em.persist(entity);
    }


    @Override
    public Project get(String type, String id)
            throws DataMartException
    {
        entity.Projects entity = getEntity(type, id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Project with type '"+type+"' and id '"+id+"' does not exist.");

        Project project = createResource(entity);
        attachLinks(entity, project);
        return project;
    }

    public Projectlistings getListingsByUnit(String unitid, Boolean sopaonly)
            throws DataMartException
    {
                // Confirm that this is a valid unit
        entity.RefUnits unit = (entity.RefUnits) DataMartBeanUtil.getEntityById(em,
                "RefUnits", unitid, "Unit");
        if (unit==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        
                // Get projects for this unit
        /*
         * The sql for this join would be pretty straightforward, but it's a bit more convoluted in jpql.
         * Instead the following performs two queries and combines the results.
         */
        String sopaclause = (sopaonly) ? "p.sopaPublish = true and " : "";
        Query query1 = em.createQuery("SELECT p FROM Projects p JOIN p.projectForestsCollection f JOIN f.refUnits u WHERE " + sopaclause + "u.id LIKE :id");
        Query query2 = em.createQuery("SELECT p FROM Projects p JOIN p.projectDistrictsCollection f JOIN f.refUnits u WHERE " + sopaclause + "u.id LIKE :id");
        query1.setParameter("id", unitid+"%");
        query2.setParameter("id", unitid+"%");
        List entitylist1 =  query1.getResultList();
        List entitylist2 =  query2.getResultList();

        if (!(entitylist1.size()>0 || entitylist2.size()>0))
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        
        // Loop over the database collection. Each row becomes a new object in the response list
        Projectlistings responselist = new Projectlistings();
        for (Object e : entitylist1) {
            entity.Projects entity = (entity.Projects)e;
            Projectlisting p = ProjectManager.createListingResource(entity);
            responselist.getProjectlisting().add(p);
        }
        for (Object e : entitylist2) {
            // Remove duplicates from the two queries
            Boolean duped = false;
            for (Object dupe : entitylist1) {
                if ( ((entity.Projects)dupe).getId() == ((entity.Projects)e).getId() )
                    duped = true;
            }
            if (!duped) {
                entity.Projects entity = (entity.Projects)e;
                Projectlisting p = ProjectManager.createListingResource(entity);
                responselist.getProjectlisting().add(p);
            }
        }
        
        return responselist;
    }
    
    public Projectlistings getSpotlightsByUnit(String unitid) 
            throws DataMartException{
        
                // Confirm that this is a valid unit
        entity.RefUnits unit = (entity.RefUnits) DataMartBeanUtil.getEntityById(em,
                "RefUnits", unitid, "Unit");
        if (unit==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        
        
        String id1 = unit.getSpotlightId1();
        String id2 = unit.getSpotlightId2();
        String id3 = unit.getSpotlightId3();
                
        entity.Projects entity1 = null;
        entity.Projects entity2 = null;
        entity.Projects entity3 = null;
        
        if (id1 != null)
            entity1 = getEntity("nepa", id1);
        if (id2 != null)
            entity2 = getEntity("nepa", id2);
        if (id3 != null)
            entity3 = getEntity("nepa", id3);
        
        if (entity1 == null && entity2 == null && entity3 == null)
            Logger.getLogger("No spotlight projects found for unitid: " + unitid);
            //throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        // Loop over the database collection. Each row becomes a new object in the response list
        Projectlistings responselist = new Projectlistings();
        if (entity1 != null){
            Projectlisting p = ProjectManager.createListingResource(entity1);
            responselist.getProjectlisting().add(p);
        }
        if (entity2 != null){
            Projectlisting p = ProjectManager.createListingResource(entity2);
            responselist.getProjectlisting().add(p);
        }
        if (entity3 != null){
            Projectlisting p = ProjectManager.createListingResource(entity3);
            responselist.getProjectlisting().add(p);
        }
        
        return responselist;
    }

    public Projects getByUnit(String unitid, Boolean sopaonly)
            throws DataMartException
    {
        // Confirm that this is a valid unit
        entity.RefUnits unit = (entity.RefUnits) DataMartBeanUtil.getEntityById(em,
                "RefUnits", unitid, "Unit");
        if (unit==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        // Get projects for this unit
        /*
         * The sql for this join would be pretty straightforward, but it's a bit more convoluted in jpql.
         * Instead the following performs two queries and combines the results.
         */
        String sopaclause = (sopaonly) ? "p.sopaPublish = true and " : "";
        Query query1 = em.createQuery("SELECT p FROM Projects p JOIN p.projectForestsCollection f JOIN f.refUnits u WHERE " + sopaclause + "u.id LIKE :id");
        Query query2 = em.createQuery("SELECT p FROM Projects p JOIN p.projectDistrictsCollection f JOIN f.refUnits u WHERE " + sopaclause + "u.id LIKE :id");
        query1.setParameter("id", unitid+"%");
        query2.setParameter("id", unitid+"%");
        List entitylist1 =  query1.getResultList();
        List entitylist2 =  query2.getResultList();

        if (!(entitylist1.size()>0 || entitylist2.size()>0))
            Logger.getLogger("No projects found for unit id: " + unitid);
            //throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        // Loop over the database collection. Each row becomes a new object in the response list
        Projects responselist = new Projects();
        for (Object e : entitylist1) {
            entity.Projects entity = (entity.Projects)e;
            Project p = ProjectManager.createResource(entity);
            attachLinks(entity, p);
            responselist.getProject().add(p);
        }
        for (Object e : entitylist2) {
            // Remove duplicates from the two queries
            Boolean duped = false;
            for (Object dupe : entitylist1) {
                if ( ((entity.Projects)dupe).getId() == ((entity.Projects)e).getId() )
                    duped = true;
            }
            if (!duped) {
                entity.Projects entity = (entity.Projects)e;
                Project p = ProjectManager.createResource((entity.Projects)e);
                attachLinks(entity, p);
                responselist.getProject().add(p);
            }
        }

        // Get nationwide and regionwide entities for the live sopa
        if (sopaonly) {
            Query query3 = em.createQuery("SELECT p FROM Projects p JOIN p.projectDistrictsCollection f JOIN f.refUnits u WHERE p.sopaPublish = true and " +
                "( u.id LIKE '11000000' OR u.id LIKE :regionwide )");
            String regionwide = unitid.substring(0, 4)+"0000";
            query3.setParameter("regionwide", regionwide);
            List entitylist3 = query3.getResultList();
            for (Object e : entitylist3) {
                // Remove duplicates from the two queries
                Boolean duped = false;
                for (Object dupe : entitylist1) {
                    if ( ((entity.Projects)dupe).getId() == ((entity.Projects)e).getId() )
                        duped = true;
                }
                for (Object dupe : entitylist2) {
                    if ( ((entity.Projects)dupe).getId() == ((entity.Projects)e).getId() )
                        duped = true;
                }
                if (!duped) {
                    entity.Projects entity = (entity.Projects)e;
                    Project p = createResource(entity);
                    attachLinks(entity, p);
                    responselist.getProject().add(p);
                }
            }
        }

        return responselist;
    }

    public Projects getActiveByUnit(String unitid, Boolean sopaonly)
            throws DataMartException
    {
        // Confirm that this is a valid unit
        entity.RefUnits unit = (entity.RefUnits) DataMartBeanUtil.getEntityById(em,
                "RefUnits", unitid, "Unit");
        if (unit==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        // Get projects for this unit
        /*
         * The sql for this join would be pretty straightforward, but it's a bit more convoluted in jpql.
         * Instead the following performs two queries and combines the results.
         */
        String sopaclause = (sopaonly) ? "p.sopaPublish = true and " : "";
        String activeflag=" AND ( p.expirationDate is null OR p.expirationDate >= :today )";
        Calendar today = new GregorianCalendar();
        // reset hour, minutes, seconds and millis
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);
        Query query1 = em.createQuery("SELECT p FROM Projects p JOIN p.projectForestsCollection f JOIN f.refUnits u WHERE " + sopaclause + "u.id LIKE :id" + activeflag);
        Query query2 = em.createQuery("SELECT p FROM Projects p JOIN p.projectDistrictsCollection f JOIN f.refUnits u WHERE " + sopaclause + "u.id LIKE :id"  + activeflag);
        query1.setParameter("id", unitid+"%");
        query2.setParameter("id", unitid+"%");
        query1.setParameter("today", today.getTime());
        query2.setParameter("today", today.getTime());
        List entitylist1 =  query1.getResultList();
        List entitylist2 =  query2.getResultList();

        if (!(entitylist1.size()>0 || entitylist2.size()>0))
            Logger.getLogger("No projects found for unit id: " + unitid);
            //throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        // Loop over the database collection. Each row becomes a new object in the response list
        Projects responselist = new Projects();
        for (Object e : entitylist1) {
            entity.Projects entity = (entity.Projects)e;
            Project p = ProjectManager.createResource(entity);
            attachLinks(entity, p);
            responselist.getProject().add(p);
        }
        for (Object e : entitylist2) {
            // Remove duplicates from the two queries
            Boolean duped = false;
            for (Object dupe : entitylist1) {
                if ( ((entity.Projects)dupe).getId() == ((entity.Projects)e).getId() )
                    duped = true;
            }
            if (!duped) {
                entity.Projects entity = (entity.Projects)e;
                Project p = ProjectManager.createResource((entity.Projects)e);
                attachLinks(entity, p);
                responselist.getProject().add(p);
            }
        }

        // Get nationwide and regionwide entities for the live sopa
        if (sopaonly) {
            Query query3 = em.createQuery("SELECT p FROM Projects p JOIN p.projectDistrictsCollection f JOIN f.refUnits u WHERE p.sopaPublish = true and " +
                "( u.id LIKE '11000000' OR u.id LIKE :regionwide )"   + activeflag);
            String regionwide = unitid.substring(0, 4)+"0000";
            query3.setParameter("regionwide", regionwide);
            query3.setParameter("today", today.getTime());
            List entitylist3 = query3.getResultList();
            for (Object e : entitylist3) {
                // Remove duplicates from the two queries
                Boolean duped = false;
                for (Object dupe : entitylist1) {
                    if ( ((entity.Projects)dupe).getId() == ((entity.Projects)e).getId() )
                        duped = true;
                }
                for (Object dupe : entitylist2) {
                    if ( ((entity.Projects)dupe).getId() == ((entity.Projects)e).getId() )
                        duped = true;
                }
                if (!duped) {
                    entity.Projects entity = (entity.Projects)e;
                    Project p = createResource(entity);
                    attachLinks(entity, p);
                    responselist.getProject().add(p);
                }
            }
        }

        return responselist;
    }

    @Override
    public void update(Project resource)
             throws DataMartException
    {
        entity.Projects entity = createEntity(resource);

        // Query for the (hopefully) existing entity. Complain if not found.
        entity.Projects existingentity = getEntity(entity.getProjectTypes().getId(), entity.getPublicId());
        if (existingentity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND, "Nonexistent project cannot be updated.");
        
        //store old values for KML update check
        String oldUrl = existingentity.getWwwLink();
        String oldName = existingentity.getName();
        String oldStatus, oldType;
        try {
            oldStatus = existingentity.getRefStatuses().getName();
        } catch (NullPointerException ex) {
            oldStatus = "";
        }
        try {
            oldType = existingentity.getRefAnalysisTypes().getName();
        } catch (NullPointerException ex) {
            oldType = "";
        }
        Date oldArchiveDate = existingentity.getExpirationDate();
        entity.RefUnits unit = existingentity.getRefUnits();
        String oldUnitId = unit != null ? unit.getId() : "";
        boolean wasPublished = existingentity.getWwwIsPublished();
        //purposes needs to be stored because they get deleted later, might
        //as well put them in a set since we'll be comparing them
        HashSet<String> oldPurposes = new HashSet<>();
        for (ProjectPurposes purpose : existingentity.getProjectPurposesCollection()) {
            oldPurposes.add(purpose.getRefPurposes().getName());
        }
        // Set the database id (not the public id). This is necessary because the ID
        //   is not passed to us by the resource.
        entity.setId(existingentity.getId());
        // Also do this for the location information, because this is also not passed to us by the resource
        entity.setLatitude(existingentity.getLatitude());
        entity.setLongitude(existingentity.getLongitude());
        entity.setLocationDesc(existingentity.getLocationDesc());
        entity.setLocationLegalDesc(existingentity.getLocationLegalDesc());

        // Remove the old ProjectPurposes. They will be replaced with the new ones.
        Collection<entity.ProjectPurposes> purposes = existingentity.getProjectPurposesCollection();
        Iterator it = purposes.iterator();
        while (it.hasNext()) {
            em.remove(it.next());
        }

        // Update the entity
        entity = em.merge(entity);
        
        //now that the update has been successful, update the KML if neccessary
        String updatedUrl = entity.getWwwLink();
        String updatedName = entity.getName();
        String updatedStatus = entity.getRefStatuses().getName();
        String updatedType = entity.getRefAnalysisTypes().getName();
        Date updatedDate = entity.getExpirationDate();
        unit = entity.getRefUnits();
        String updatedUnitId = unit != null ? unit.getId() : "";
        Collection<ProjectPurposes> updatedPurposes = entity.getProjectPurposesCollection();
        boolean samePurposes = true;
        if (oldPurposes.size() != updatedPurposes.size()) {
            samePurposes = false;
        }
        if (samePurposes) {
            for (ProjectPurposes updatedPurpose : updatedPurposes) {
                if (!oldPurposes.contains(updatedPurpose.getRefPurposes().getName())) {
                    samePurposes = false;
                    break;
                }
            }
        }
        boolean isPublished = entity.getWwwIsPublished();
        if (wasPublished && !isPublished) {
            try {
                //need to delete the project and unit KML when unpublished
                ServiceHelper.deleteKML(entity.getPublicId(), oldUnitId);
                //it's not likely to be in the new unit's kml, but no harm
                ServiceHelper.deleteKML(entity.getPublicId(), updatedUnitId);
            } catch (ServiceException ex) {
                Logger.getLogger(ProjectManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (isPublished) {
            //only update KMLs on published projects
            boolean updateSent = false;
            boolean informationChanged = (
                (!compareStrings(updatedUrl, oldUrl) || !compareStrings(updatedName, oldName) ||
                !compareStrings(updatedStatus, oldStatus) || !compareStrings(updatedType, oldType) ||
                !samePurposes) && existingentity.getLatitude() != null && existingentity.getLongitude() != null
            );
            boolean firstPublish = !wasPublished && isPublished;
            String unitId = entity.getRefUnits() == null ?
                            "" : entity.getRefUnits().getId();
            String unitUrl = "";
            if (unitId != null && !unitId.equals("") && unitId.length() > 6){
                List<RefUnits> result = em.createNamedQuery("RefUnits.findById")
                        .setParameter("id", unitId.substring(0, 6))
                        .getResultList();
                if (result.size() > 0) {
                    unitUrl = result.get(0).getWwwLink();
                }
            }
            if (informationChanged || firstPublish) {
                //if the latitude and longitude are null and indicate that the project KML
                //needs to be deleted, the ProjectLocationManager will handle it.
                try {
                    ServiceHelper.updateKML(entity, unitUrl);
                } catch (ServiceException ex) {
                    Logger.getLogger(ProjectManager.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            boolean archiveUpdated = false;
            boolean dateUpdated = updatedDate == null ? oldArchiveDate != null :
                                                        !updatedDate.equals(oldArchiveDate);
            boolean firstArchiveDate = firstPublish && updatedDate != null;
            if (dateUpdated || firstArchiveDate) {
                try {
                    //if updatedDate isn't null (true) updateArchive sends the add/update
                    //flag. If it is null (false), it sends the remove flag
                    ServiceHelper.updateArchive(entity, updatedDate != null);
                    archiveUpdated = true;
                } catch (ServiceException ex) {
                    Logger.getLogger(ProjectManager.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            //purely change based, don't have to check for first publish here
            if (!compareStrings(oldUnitId, updatedUnitId)) {
                try {
                    if (!archiveUpdated){
                        ServiceHelper.updateArchive(entity, updatedDate != null);
                    }
                
                    //If the unit changes, have to remove it from the old unit, but not
                    //delete the project KML
                    ServiceHelper.deleteKML(entity.getPublicId(), oldUnitId, true);
                    
                    //sometimes unit will be the only change, and so the update
                    //signal needs to be sent immediatley after the delete
                    ServiceHelper.updateKML(entity, unitUrl);
                
                } catch (ServiceException ex) {
                    Logger.getLogger(ProjectManager.class.getName()).log(Level.SEVERE, null, ex);
                }  
            }
        }
    }

    
    @Override
    public void delete(String type, String id)
             throws DataMartException
    {
        // TODO: fail if documents exist for this project
        // TODO: fail if this project is linked by the CARA linkProject
        // TODO: fail if this project is linked by MLM create


        // Verify that this entity exists
        entity.Projects entity = getEntity(type, id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Could not delete nonexistent resource with type '"+type+"' and id '"+id+"'.");

        // Don't delete if decisions still exist for this project
        Query query = em.createQuery("SELECT d FROM Decisions d WHERE d.projects = :project");
        query.setParameter("project", entity);
        if (query.getResultList().size()>0)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Could not delete project with type '"+type+"' and id '"+id+"'. You must delete the associated Decisions first.");

        // Remove locations information for this project if it exists
        entity = DataMartBeanUtil.removeProjectLocationRelationships(em, entity);

        // Remove the associated ProjectPurposes
        Collection<entity.ProjectPurposes> purposes = entity.getProjectPurposesCollection();
        Iterator it = purposes.iterator();
        while (it.hasNext()) {
            em.remove(it.next());
        }
        //get the Unit ID before removing the entity
        String unitId = entity.getRefUnits().getId();
        // Remove the entity itself
        em.remove(entity);
        
        //once the entity is removed, remove the KML file
        try {
            ServiceHelper.deleteKML(id, unitId);
        } catch (ServiceException ex) {
            Logger.getLogger(ProjectManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    @Override
    public Projectsearchresults search(String name,
            String palsid,
            String [] unitlist,
            Integer [] statuslist,
            Boolean maponly,
            Boolean pubflag,
            Boolean archiveflag,
            Integer start,
            Integer rows)
            throws DataMartException
    {
        Projectsearchresults results = new Projectsearchresults();
        String matchName = "%";
        String matchPalsid = "%";
        Integer matchStart = 0;
        Integer matchRows = 100;    // Documented default value

        if (name != null) {
            matchName = "%" + name.toLowerCase() + "%"; // case-insensitive
        }

        if (palsid != null) {
            matchPalsid = palsid + "%";
        }

        if (start != null) {
            matchStart = start;
            if (matchStart < 0) {
                matchStart = 0;
            }
        }

        if (rows != null) {
            matchRows = rows;
            if (matchRows < 0) {
                matchRows = 0;
            }
        }
                
        /*
         * Construct the query
         */
        

        // AND ((p.refUnits.id LIKE :unit1) OR (p.refUnits.id LIKE :unit2))
        //
        // ...later on assign host variables
        // query.setParameter("unit1", "1103%");
        // query.setParameter("unit2", "1104%");
        String unitclause = "";
        if (unitlist != null && unitlist.length > 0) {
            for (int i = 0; i < unitlist.length; i++) {
                if (i == 0) {
                    unitclause += "AND (";
                } else {
                    unitclause += "OR ";
                }
                String unitparam = ":unit" + Integer.toString(i);
                unitclause += "(p.refUnits.id LIKE " + unitparam + ")";
            }
            unitclause += ") ";
        }

        // AND (p.refStatuses.id IN (0, 1, 2, 3, 4, 5))
        String statusclause = "";
        if (statuslist != null && statuslist.length > 0) {
            statusclause = "AND (p.refStatuses.id IN (" + joinIntegerArray(statuslist, ",") + "))";
        }

        String mapclause = "";
        if (maponly != null) {
            if (maponly) {
                mapclause = " AND p.latitude is not null AND p.longitude is not null ";
            } else {
                mapclause = " AND p.latitude is null AND p.longitude is null ";
            }
        }

        String pubclause = "";
        if (pubflag != null) {
            if (pubflag) {
                pubclause = " AND p.wwwIsPublished = TRUE ";
            } else {
                pubclause = " AND p.wwwIsPublished = FALSE ";
            }
        }

        String archiveclause = "";
        if (archiveflag != null) {
            if (archiveflag) {
                // return only expired projects
                archiveclause = " AND p.expirationDate < :today ";
            } else {
                // return only unexpired projects
                archiveclause = " AND ( p.expirationDate is null OR p.expirationDate >= :today ) ";
            }
        }

        String fullQuery = "SELECT p FROM Projects p WHERE (" +
                "(p.projectTypes.id = 'nepa') AND " +
                "(lower(p.name) like :name) AND " +
                "(p.publicId like :palsid) " +
                unitclause +
                statusclause +
                mapclause +
                pubclause +
                archiveclause +
                ") ORDER BY p.name, p.publicId, p.id";
        
        Query query = em.createQuery(fullQuery);

        /*
         * Assign parameters
         */
        query.setParameter("name", matchName);
        query.setParameter("palsid", matchPalsid);

        // set filter date/time to midnight this morning
        if (archiveclause.length()>0) {
            Calendar today = new GregorianCalendar();
            // reset hour, minutes, seconds and millis
            today.set(Calendar.HOUR_OF_DAY, 0);
            today.set(Calendar.MINUTE, 0);
            today.set(Calendar.SECOND, 0);
            today.set(Calendar.MILLISECOND, 0);
            query.setParameter("today", today.getTime());
        }

        if (unitlist != null && unitlist.length > 0) {
            for (int i = 0; i < unitlist.length; i++) {
                String unitparam = "unit" + Integer.toString(i);
                query.setParameter(unitparam, unitlist[i] + "%");
            }
        }

        query.setFirstResult(matchStart);
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Projects responselist = new Projects();
        while (it.hasNext()) {
            entity.Projects entity = (entity.Projects)it.next();
            Project p = createResource(entity);
            attachLinks(entity, p);
            responselist.getProject().add(p);
            if (responselist.getProject().size() >= matchRows) {
                break;
            }
        }

        Projectsearchresultmetadata metadata = new Projectsearchresultmetadata();
        metadata.setRequest("Request string here");
        metadata.setRowcount(responselist.getProject().size());
        metadata.setTotalcount(entitylist.size() + matchStart); // wrong if start > size of full set
        results.setResultmetadata(metadata);
        results.setProjects(responselist);

        return results;
    }

    @Override
    public Boolean ping()
    {
        return true;
    }
    
    
    /*
     *
     * This method does the mapping from entity object to jaxb object
     */
    protected static Project createResource(entity.Projects entity)
            throws DataMartException
    {
        Project resource = new Project();
        Nepainfo nepainfo = new Nepainfo();
        Sopainfo sopainfo = new Sopainfo();
        us.fed.fs.www.nepa.schema.project.Purposeids purposeids = new us.fed.fs.www.nepa.schema.project.Purposeids();

        String isPublished = (entity.getWwwIsPublished()) ? "1" : "0";
        
        resource.setType(entity.getProjectTypes().getId());
        resource.setId(entity.getPublicId());
        resource.setName(entity.getName());
        resource.setUnitcode(entity.getRefUnits().getId());
        resource.setAdminapp(entity.getApplications().getId());
        resource.setDescription(entity.getDescription());
        if (entity.getLastUpdate() != null) {
            resource.setLastupdate( DataMartBeanUtil.getXMLGregorianFromDateTime( entity.getLastUpdate() ));
        }
        resource.setCommentreg(entity.getRefCommentRegulations().getId().toString());
        resource.setWwwlink(entity.getWwwLink());
        resource.setAddinfopubflag(DataMartBeanUtil.getStringValFromBool(entity.getAddInfoPubFlag()));
        resource.setImplinfopubflag(DataMartBeanUtil.getStringValFromBool(entity.getImplPubFlag()));
        resource.setMeetingpubflag(DataMartBeanUtil.getStringValFromBool(entity.getMeetingPubFlag()));
        resource.setWwwsummary(entity.getWwwSummary());
        resource.setWwwpub(isPublished);
        resource.setExpirationdate( DataMartBeanUtil.getXMLGregorianFromDate( entity.getExpirationDate() ));

        // Set the NEPA info
        nepainfo.setProjectdocumentid(entity.getProjectDocumentId());
        nepainfo.setAnalysistypeid(entity.getRefAnalysisTypes().getId());
        nepainfo.setStatusid(entity.getRefStatuses().getId().toString());
        if (entity.getContactName() != null) {
            nepainfo.setContactname(entity.getContactName());
        }
        if (entity.getContactPhone() != null) {
            nepainfo.setContactphone(entity.getContactPhone());
        }
        if (entity.getContactEmail() != null) {
            nepainfo.setContactemail(entity.getContactEmail());
        }
        if (entity.getContactShortName() != null) {
            nepainfo.setContactshortname(entity.getContactShortName());
        }
        if (entity.getPrimaryProjectManager() != null) {
            nepainfo.setPrimaryprojectmanager(entity.getPrimaryProjectManager());
        }
        if (entity.getSecondaryProjectManager() != null) {
            nepainfo.setSecondaryprojectmanager(entity.getSecondaryProjectManager());
        }
        if (entity.getDataEntryPerson() != null) {
            nepainfo.setDataentryperson(entity.getDataEntryPerson());
        }
        nepainfo.setEsd(entity.getEsd());
        nepainfo.setCenodecision(entity.getCenodecision());
        if (entity.getDecisionDecided() != null){
            nepainfo.setDecisiondecided(DataMartBeanUtil.getXMLGregorianFromDateTime( entity.getDecisionDecided()));
        }
        // Set the SOPA info
        sopainfo.setPublishflag(entity.getSopaPublish());
        sopainfo.setNewflag(entity.getSopaNew());
        sopainfo.setHeadercategory(entity.getSopaHeaderCat());

        // Put the Purpose ID's into a Purposeids object
        for (Iterator it = entity.getProjectPurposesCollection().iterator(); it.hasNext();)
        {
            entity.ProjectPurposes pp = (entity.ProjectPurposes)it.next();
            purposeids.getPurposeid().add(pp.getRefPurposes().getId());
        }
        nepainfo.setPurposeids(purposeids);
        nepainfo.setSopainfo(sopainfo);
        resource.setNepainfo(nepainfo);

        return resource;
    }

    /*
     *
     * This method does the mapping from entity object to jaxb object
     */
    protected static Projectlisting createListingResource(entity.Projects entity)
            throws DataMartException
    {
        Projectlisting resource = new Projectlisting();
        
        resource.setArchivedate(DataMartBeanUtil.getXMLGregorianFromDate( entity.getExpirationDate() ));
        resource.setDescription(entity.getDescription());
        resource.setId(entity.getPublicId());
        
        if (entity.getWwwIsPublished())
            resource.setIspublished("true");
        else
            resource.setIspublished("false");
        
        resource.setName(entity.getName());
        resource.setType(entity.getProjectTypes().getId());
        us.fed.fs.www.nepa.schema.projectlisting.Purposeids purposeids 
                    = new us.fed.fs.www.nepa.schema.projectlisting.Purposeids();

        for (Iterator it = entity.getProjectPurposesCollection().iterator(); it.hasNext();)
        {
            entity.ProjectPurposes pp = (entity.ProjectPurposes)it.next();
            //pp.getRefPurposes().getId()
            purposeids.getPurposeid().add(pp.getRefPurposes().getId().toString()); 
        }
        
        resource.setPurposeids(purposeids);
        
        resource.setStatusid(entity.getRefStatuses().getId().toString());
        
        resource.setUnitid(""+entity.getRefUnits().getId());
        resource.setUrl(entity.getWwwLink() );
        return resource;
        
        
    }

    /*
     *
     * This method does the mapping from jaxb object to entity object
     */
    private entity.Projects createEntity(Project resource)
            throws DataMartException
    {
        // These are the objects we're going to return. Technically just the entity
        //  is returned, but the subordinate objects are embedded via the jaxb setter methods
        entity.Projects entity = new entity.Projects();
        Nepainfo nepainfo = resource.getNepainfo();

        // The following fields are optional in the schema, but must be set by the resource class.
        //   We don't need to verify the presence of required fields here, because they've already
        //   been validated by the JAXB unmarshaller.
        DataMartBeanUtil.confirmNotNull(resource.getType(), "Type");
        DataMartBeanUtil.confirmNotNull(resource.getAdminapp(), "AdminApp");

        // Verify formatting of integer/date/etc fields, and convert them to their proper form (String/int/etc)
        int commentregid = DataMartBeanUtil.getIntFromString(resource.getCommentreg(), "commentreg");
        Boolean isPub = DataMartBeanUtil.getBoolFromString(resource.getWwwpub(), "wwwpub");
        int statusid = DataMartBeanUtil.getIntFromString(nepainfo.getStatusid(), "statusid");
        
        
         Boolean implPubFlag = DataMartBeanUtil.getBoolFromString(resource.getImplinfopubflag(), "implPubFlag");
         Boolean addInfoPubFlag = DataMartBeanUtil.getBoolFromString(resource.getAddinfopubflag(), "addInfoPubFlag");
         Boolean meetingPubFlag = DataMartBeanUtil.getBoolFromString(resource.getMeetingpubflag(), "meetingPubFlag");
        /*
         * 
         * It sucks running all these queries, but it's probably better than returning five pages of
         *    incomprehensible database output when one of your constraints fails. And it's certainly better
         *    than inserting nulls. If JPA has some other way to handle FK checking, that might be the way to go.
         */
        // Make sure all foreign keys are valid, and retrieve a jpa representation of the associated row
        entity.ProjectTypes type = (entity.ProjectTypes) DataMartBeanUtil.getEntityById(em,
                "ProjectTypes", resource.getType(), "Type");
        entity.Applications application = (entity.Applications) DataMartBeanUtil.getEntityById(em,
                "Applications", resource.getAdminapp(), "Admin Application");
        entity.RefUnits unit = (entity.RefUnits) DataMartBeanUtil.getEntityById(em,
                "RefUnits", resource.getUnitcode(), "Unit Code");
        entity.RefAnalysisTypes analysistype = (entity.RefAnalysisTypes) DataMartBeanUtil.getEntityById(em,
                "RefAnalysisTypes", resource.getNepainfo().getAnalysistypeid(), "Analysis Type");
        entity.RefStatuses status = (entity.RefStatuses) DataMartBeanUtil.getEntityById(em,
                "RefStatuses", statusid, "Project Status");
        entity.RefCommentRegulations commentreg = (entity.RefCommentRegulations) DataMartBeanUtil.getEntityById(em,
                "RefCommentRegulations", commentregid, "Comment Regulation");

        entity.setProjectTypes(type);
        entity.setPublicId(resource.getId());
        entity.setName(resource.getName());
        entity.setRefUnits(unit);
        entity.setApplications(application);
        entity.setDescription(resource.getDescription());
        if (resource.getLastupdate() != null) {
            entity.setLastUpdate(DataMartBeanUtil.getDateFromXMLGregorian(resource.getLastupdate()));
        }
        entity.setRefCommentRegulations(commentreg);
        entity.setWwwLink(resource.getWwwlink());
        entity.setWwwSummary(resource.getWwwsummary());
        entity.setWwwIsPublished(isPub);
        entity.setRefAnalysisTypes(analysistype);
        entity.setRefStatuses(status);
        entity.setSopaPublish(resource.getNepainfo().getSopainfo().isPublishflag());
        //if( null !=resource.getNepainfo().getSopainfo().isNewflag())
        entity.setSopaNew(resource.getNepainfo().getSopainfo().isNewflag());
        entity.setSopaHeaderCat(resource.getNepainfo().getSopainfo().getHeadercategory());
        entity.setAddInfoPubFlag(addInfoPubFlag);
        entity.setImplPubFlag(implPubFlag);
        entity.setMeetingPubFlag(meetingPubFlag);
        entity.setExpirationDate(DataMartBeanUtil.getDateFromXMLGregorian(resource.getExpirationdate()));

        entity.setProjectDocumentId(nepainfo.getProjectdocumentid());
        if (nepainfo.getContactname() != null) {
            entity.setContactName(nepainfo.getContactname());
        }
        if (nepainfo.getContactphone() != null) {
             entity.setContactPhone(nepainfo.getContactphone());
        }
        if (nepainfo.getContactemail() != null) {
            entity.setContactEmail(nepainfo.getContactemail());
        }
        if (nepainfo.getPrimaryprojectmanager() != null){
            entity.setPrimaryProjectManager(nepainfo.getPrimaryprojectmanager());
        }
        if (nepainfo.getSecondaryprojectmanager() != null){
            entity.setSecondaryProjectManager(nepainfo.getSecondaryprojectmanager());
        }
        if (nepainfo.getDataentryperson()!= null){
            entity.setDataEntryPerson(nepainfo.getDataentryperson());
        }
        if (nepainfo.isEsd() != null) {
            entity.setEsd(nepainfo.isEsd());
        }
        if (nepainfo.isCenodecision() != null) {
            entity.setCenodecision(nepainfo.isCenodecision());
        }
        if (nepainfo.getDecisiondecided() != null) {
            entity.setDecisionDecided(DataMartBeanUtil.getDateFromXMLGregorian(nepainfo.getDecisiondecided()));
        }
        // The following block populates a jointable. It reads the purposes
        //   specified in the resource and adds each one to the jointable, along with a
        //   reference to this entity.
        Collection<entity.ProjectPurposes> joinpurposes = new ArrayList<entity.ProjectPurposes>();
        Iterator it = nepainfo.getPurposeids().getPurposeid().iterator();
        // Loop over the purposes specified in this resource
        while (it.hasNext()) {
            // Get the id passed to us in the resource; use it to create a new entity object
            entity.RefPurposes refp = (entity.RefPurposes) DataMartBeanUtil.getEntityById(em,
                    "RefPurposes", it.next(), "Project Purpose");
            // Create a new jointable object to store the result of the join
            entity.ProjectPurposes joinpurp = new entity.ProjectPurposes();
            // Populate the jointable object...
            joinpurp.setProjects(entity);
            joinpurp.setRefPurposes(refp);
            // ...and add it to the response list
            joinpurposes.add(joinpurp);
        }
        // Finally, tell the entity object to use this response list
        entity.setProjectPurposesCollection(joinpurposes);
        
        return entity;
    }

    
    // Accepts two String ID parameters, and returns the associated entity,
    //   or null if it doesn't exist
    private entity.Projects getEntity(String type, String id)
            throws DataMartException
    {
        // TODO: make sure the type exists.
        Query query = em.createQuery("SELECT p FROM Projects p WHERE p.projectTypes = :type and p.publicId = :publicid");
        query.setParameter("type", new entity.ProjectTypes(type));
        query.setParameter("publicid", id);

        return (entity.Projects) DataMartBeanUtil.getEntityByQueryAllowNull(query,
                "More than one project found with this type and Public ID");
    }

    private String joinIntegerArray(Integer [] list, String seperator) {
        String result = "";
        for (int i = 0; i < list.length; i++) {
            if (i != 0) {
                result += seperator;
            }
            result += Integer.toString(list[i]);
        }
        return result;
    }

    /*
     * Find links related to the given project entity, attach them to the resource
     */
    private void attachLinks(entity.Projects project, Project resource)
            throws DataMartException
    {
        attachCaraLinks(project, resource);
        attachMlmProjectLinks(project, resource);
        attachKmlLinks(project, resource);
        
        //Ensure that Links element is present in output
        us.fed.fs.www.nepa.schema.project.Links links = resource.getLinks();
        if (links == null) {
            links = new us.fed.fs.www.nepa.schema.project.Links();
            resource.setLinks(links);
        }
    }
    
    /*
     * Create a <link> tag for any MLM ID linked to this project (should be no more than 1)
     */
    private void attachMlmProjectLinks(entity.Projects project, Project resource)
            throws DataMartException
    {
        String prefix = DataMartBeanUtil.getConfig(em, ConfigManager.DATAMART_URL_PREFIX);

        if (project == null) { return; }

        Query query = em.createQuery("SELECT p FROM MLMProjects p WHERE p.projects = :project");
        query.setParameter("project", project);

        us.fed.fs.www.nepa.schema.project.Link link = null;
        entity.MLMProjects mlmproj = (entity.MLMProjects)DataMartBeanUtil.getEntityByQueryAllowNull(
                query, "Found multiple MLMProjects for project "+resource.getType()+"/"+resource.getId());
        if (mlmproj != null) {
            link = new us.fed.fs.www.nepa.schema.project.Link();
            link.setHref(prefix + "/mlm/projects/" + mlmproj.getId());
            link.setRel("mlmproject");

            us.fed.fs.www.nepa.schema.project.Links links = resource.getLinks();
            if (links == null) {
                links = new us.fed.fs.www.nepa.schema.project.Links();
            }

            links.getLink().add(link);
            resource.setLinks(links);
        }
    }


    /*
     * Create a <link> tag for any CARA ID linked to this project (should be no more than 1)
     * Instead of linking to the CARAProject relationship record, this links to the list of
     * CARA Comment phases (potentially empty)
     */
    private void attachCaraLinks(entity.Projects project, Project resource)
            throws DataMartException
    {
        String prefix = DataMartBeanUtil.getConfig(em, ConfigManager.DATAMART_URL_PREFIX);

        if (project == null) { return; }

        Query query = em.createQuery("SELECT p FROM CARAProjects p WHERE p.projects = :project");
        query.setParameter("project", project);

        us.fed.fs.www.nepa.schema.project.Link caralink = null;
        us.fed.fs.www.nepa.schema.project.Link phaseslink = null;
        entity.CARAProjects caraproj = (entity.CARAProjects)DataMartBeanUtil.getEntityByQueryAllowNull(
                query, "Found multiple CARAProjects for project "+resource.getType()+"/"+resource.getId());
        if (caraproj != null) {
            // Add link to CARAProject record
            caralink = new us.fed.fs.www.nepa.schema.project.Link();
            caralink.setHref(prefix + "/caraprojects/" + caraproj.getId());
            caralink.setRel("caraproject");

            // Add link to list of phases
            phaseslink = new us.fed.fs.www.nepa.schema.project.Link();
            phaseslink.setHref(prefix + "/caraprojects/" + caraproj.getId() + "/comment/phases");
            phaseslink.setRel("caraphaselist");

            us.fed.fs.www.nepa.schema.project.Links links = resource.getLinks();
            if (links == null) {
                links = new us.fed.fs.www.nepa.schema.project.Links();
            }

            links.getLink().add(caralink);
            links.getLink().add(phaseslink);
            resource.setLinks(links);
        }
    }

    private void attachKmlLinks(entity.Projects project, Project resource)
            throws DataMartException
    {
        String prefix = DataMartBeanUtil.getConfig(em, ConfigManager.EDGESERVER_URL_PREFIX);

        if (project == null) { return; }

        // Retrieve entities from the database
        Query query = em.createQuery("SELECT k FROM ProjectKMLs k WHERE k.projects = :project");
        query.setParameter("project", project);
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Links responselist = new Links();
        while (it.hasNext()) {
            Link link = new Link();
            entity.ProjectKMLs projectkml = (entity.ProjectKMLs)it.next();
            String date =new SimpleDateFormat("yyMMddhh").format(new Date());
            link.setHref(prefix + "/projects/" + projectkml.getId()+"?refresh="+date);
            link.setRel("projectkml");
            link.setTitle(projectkml.getLabel());
            link.setType(projectkml.getMapType());
            responselist.getLink().add(link);
        }
        if (entitylist.size() > 0) {
            Links links = resource.getLinks();
            if (links == null) {
                links = new Links();
                resource.setLinks(links);
            }
            links.getLink().addAll(responselist.getLink());
        }
    }
    
    private boolean compareStrings(String a, String b) {
        return a == null ? b == null : a.equals(b);
    }
}