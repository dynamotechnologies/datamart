package test.entity;

import client.ProjectClient;
import client.ProjectDocumentClient;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.projectdocument.*;


public class ProjectDocumentTest {
    private ProjectDocumentClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new ProjectDocumentClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postProjectDocument(TestData.projects.id1, TestData.projects.type1, TestData.projectdocuments.docid1,
                    TestData.projectdocuments.pubflag1, TestData.projectdocuments.wwwlink1, TestData.projectdocuments.docname1,
                    TestData.projectdocuments.description1, TestData.projectdocuments.pdffilesize1, TestData.projectdocuments.sensitiveflag1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Projectdocument projectdocument = null;
        Projectdocuments projectdocumentlist = null;
        try {
            projectdocument = client.getProjectDocument("nepa", TestData.projects.id1, TestData.projectdocuments.docid1);
   // TODO: Test getAll()
   //
   // TODO: look through all tests/clients and compare them to the interfaces. Confirm that all clients implement
   //       all interface methods, and that all of the tests do in fact test all client methods.
   //
            //         projectdocumentlist = client.getProjectDocumentList(TestData.projectdocuments.projectid1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectDocumentTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.projects.id1, projectdocument.getProjectid());
        Assert.assertEquals(TestData.projectdocuments.docid1, projectdocument.getDocid());
        Assert.assertEquals(TestData.projectdocuments.pubflag1, projectdocument.getPubflag());
        Assert.assertEquals(TestData.projectdocuments.wwwlink1, projectdocument.getWwwlink());
        Assert.assertEquals(TestData.projectdocuments.docname1, projectdocument.getDocname());
        Assert.assertEquals(TestData.projectdocuments.description1, projectdocument.getDescription());
        Assert.assertEquals(TestData.projectdocuments.pdffilesize1, projectdocument.getPdffilesize());
        Assert.assertEquals(TestData.projectdocuments.sensitiveflag1, projectdocument.getSensitiveflag());

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        Projectdocument projectdocument = null;
        try {
            client.putProjectDocument(TestData.projects.id1, TestData.projects.type1, TestData.projectdocuments.docid1,
                    TestData.projectdocuments.pubflag2, TestData.projectdocuments.wwwlink2, TestData.projectdocuments.docname2,
                    TestData.projectdocuments.description2, TestData.projectdocuments.pdffilesize2, TestData.projectdocuments.sensitiveflag2);
            projectdocument = client.getProjectDocument("nepa", TestData.projects.id1, TestData.projectdocuments.docid1);
        } catch (Exception ex) {
            Logger.getLogger(ProjectDocumentTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.projects.id1, projectdocument.getProjectid());
        Assert.assertEquals(TestData.projectdocuments.docid1, projectdocument.getDocid());
        Assert.assertEquals(TestData.projectdocuments.pubflag2, projectdocument.getPubflag());
        Assert.assertEquals(TestData.projectdocuments.wwwlink2, projectdocument.getWwwlink());
        Assert.assertEquals(TestData.projectdocuments.docname2, projectdocument.getDocname());
        Assert.assertEquals(TestData.projectdocuments.description2, projectdocument.getDescription());
        Assert.assertEquals(TestData.projectdocuments.pdffilesize2, projectdocument.getPdffilesize());
        Assert.assertEquals(TestData.projectdocuments.sensitiveflag2, projectdocument.getSensitiveflag());

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getProjectDocument("nepa", TestData.projects.id1, TestData.projectdocuments.docid1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteProjectDocument("nepa", TestData.projects.id1, TestData.projectdocuments.docid1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }

    @Test
    public void testGetPagedList()
    {
        post();

        Projectdocument projectdocument = null;
        Projectdocuments projectdocumentlist = null;

        // Get all docs
        try {
            projectdocumentlist = client.getProjectDocumentPagedList(TestData.projects.type1, TestData.projects.id1, null, null, null, null);
        } catch (IOException ex) {
            Logger.getLogger(ProjectDocumentTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(4, projectdocumentlist.getProjectdocument().size());
        Assert.assertEquals(TestData.projectdocuments.docid1, projectdocumentlist.getProjectdocument().get(0).getDocid());
        Assert.assertEquals(TestData.projectdocuments.docid2, projectdocumentlist.getProjectdocument().get(1).getDocid());
        Assert.assertEquals(TestData.projectdocuments.docid3, projectdocumentlist.getProjectdocument().get(2).getDocid());
        Assert.assertEquals(TestData.projectdocuments.docid4, projectdocumentlist.getProjectdocument().get(3).getDocid());

        // Get paged docs
        // NOTE ordering is by docid
        try {
            projectdocumentlist = client.getProjectDocumentPagedList(TestData.projects.type1, TestData.projects.id1, null, null, 2, 2);
        } catch (IOException ex) {
            Logger.getLogger(ProjectDocumentTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(2, projectdocumentlist.getProjectdocument().size());
        Assert.assertEquals(TestData.projectdocuments.docid3, projectdocumentlist.getProjectdocument().get(0).getDocid());
        Assert.assertEquals(TestData.projectdocuments.docid4, projectdocumentlist.getProjectdocument().get(1).getDocid());

        // Get published docs
        try {
            projectdocumentlist = client.getProjectDocumentPagedList(TestData.projects.type1, TestData.projects.id1, true, null, null, null);
        } catch (IOException ex) {
            Logger.getLogger(ProjectDocumentTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(2, projectdocumentlist.getProjectdocument().size());
        Assert.assertEquals(TestData.projectdocuments.docid1, projectdocumentlist.getProjectdocument().get(0).getDocid());
        Assert.assertEquals(TestData.projectdocuments.docid2, projectdocumentlist.getProjectdocument().get(1).getDocid());

        // Get sensitive docs
        try {
            projectdocumentlist = client.getProjectDocumentPagedList(TestData.projects.type1, TestData.projects.id1, null, true, null, null);
        } catch (IOException ex) {
            Logger.getLogger(ProjectDocumentTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(2, projectdocumentlist.getProjectdocument().size());
        Assert.assertEquals(TestData.projectdocuments.docid1, projectdocumentlist.getProjectdocument().get(0).getDocid());
        Assert.assertEquals(TestData.projectdocuments.docid3, projectdocumentlist.getProjectdocument().get(1).getDocid());

        delete();
    }

    private void post()
    {
        delete();
        try {
            // docid1: published, sensitive
            client.postProjectDocument(TestData.projects.id1, TestData.projects.type1, TestData.projectdocuments.docid1,
                    TestData.projectdocuments.pubflag1, TestData.projectdocuments.wwwlink1, TestData.projectdocuments.docname1,
                    TestData.projectdocuments.description1, TestData.projectdocuments.pdffilesize1, TestData.projectdocuments.sensitiveflag1);
            // docid2: published, nonsensitive
            client.postProjectDocument(TestData.projects.id1, TestData.projects.type1, TestData.projectdocuments.docid2,
                    TestData.projectdocuments.pubflag2, TestData.projectdocuments.wwwlink2, TestData.projectdocuments.docname2,
                    TestData.projectdocuments.description2, TestData.projectdocuments.pdffilesize2, TestData.projectdocuments.sensitiveflag2);
            // docid3: unpublished, sensitive
            client.postProjectDocument(TestData.projects.id1, TestData.projects.type1, TestData.projectdocuments.docid3,
                    TestData.projectdocuments.pubflag3, TestData.projectdocuments.wwwlink3, TestData.projectdocuments.docname3,
                    TestData.projectdocuments.description3, TestData.projectdocuments.pdffilesize3, TestData.projectdocuments.sensitiveflag3);
            // docid4: unpublished, nonsensitive
            client.postProjectDocument(TestData.projects.id1, TestData.projects.type1, TestData.projectdocuments.docid4,
                    TestData.projectdocuments.pubflag4, TestData.projectdocuments.wwwlink4, TestData.projectdocuments.docname4,
                    TestData.projectdocuments.description4, TestData.projectdocuments.pdffilesize4, TestData.projectdocuments.sensitiveflag4);
        } catch (IOException ex) {
            Logger.getLogger(ProjectDocumentTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }
    
    /*
     * Cleanup
     * Missing documents are considered benign
     */
    private void delete()
    {
        ArrayList<String> docs = new ArrayList<String>();
        docs.add(TestData.projectdocuments.docid1);
        docs.add(TestData.projectdocuments.docid2);
        docs.add(TestData.projectdocuments.docid3);
        docs.add(TestData.projectdocuments.docid4);

        for (String doc : docs) {
            try {
                client.deleteProjectDocument("nepa", TestData.projects.id1, doc);
            } catch (IOException ex) {
                if (client.getStatus() != TestData.CANT_GET_NOT_FOUND) {
                    Logger.getLogger(ProjectDocumentTest.class.getName()).log(Level.SEVERE, null, ex);
                    Assert.fail();
                }
            }
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        ArrayList<String> docs = new ArrayList<String>();
        docs.add(TestData.projectdocuments.docid1);
        docs.add(TestData.projectdocuments.docid2);
        docs.add(TestData.projectdocuments.docid3);
        docs.add(TestData.projectdocuments.docid4);

        int bad = 0;
        for (String doc : docs) {
            try {
                client.deleteProjectDocument("nepa", TestData.projects.id1, doc);
                bad++; // wait to throw the exception so we can get any others that were missed
            } catch (IOException ex) {
                // woohoo! the test cleaned up after itself
            }
        }
        if (bad > 0)
            throw new Exception("A test failed to clean up after itself!");
    }


    @BeforeClass
    public static void  populateDependencies()
    {
        ProjectClient projclient = null;
        try {
            projclient = new ProjectClient();
        } catch (Exception ex) {
            Assert.fail();
        }

        // Project Documents rely on Projects. Pre-populate a Project.
        ArrayList<String> purposeids1 = new ArrayList<String>();
        purposeids1.add(TestData.projects.purposeid1);
        purposeids1.add(TestData.projects.purposeid2);
        purposeids1.add(TestData.projects.purposeid3);
        try {
            projclient.postProject(TestData.projects.id1, TestData.projects.type1,
                    TestData.projects.name1, TestData.projects.unitcode1, TestData.projects.description1,
                    TestData.projects.lastupdate1, TestData.projects.commentreg1, TestData.projects.wwwlink1,
                    TestData.projects.wwwsummary1, TestData.projects.wwwpub1, TestData.projects.analysistypeid1,
                    TestData.projects.statusid1, TestData.projects.contactname1, TestData.projects.contactphone1,
                    TestData.projects.contactemail1, purposeids1, TestData.projects.expirationdate1, TestData.projects.projectdocumentid1,
                    TestData.projects.sopapub, TestData.projects.sopanew, TestData.projects.sopaheader, TestData.projects.esd, TestData.projects.cenodecision);
        } catch (IOException ex) {
            if (projclient.getStatus() != TestData.CANT_POST_ALREADY_EXISTS) {
                Logger.getLogger(ProjectDocumentTest.class.getName()).log(Level.SEVERE, null, ex);
                Assert.fail();
            }
        }
        // If project already exists, try updating with a PUT
        try {
            projclient.putProject(TestData.projects.id1, TestData.projects.type1,
                TestData.projects.name1, TestData.projects.unitcode1, TestData.projects.description1,
                TestData.projects.lastupdate1, TestData.projects.commentreg1, TestData.projects.wwwlink1,
                TestData.projects.wwwsummary1, TestData.projects.wwwpub1, TestData.projects.analysistypeid1,
                TestData.projects.statusid1, TestData.projects.contactname1, TestData.projects.contactphone1,
                TestData.projects.contactemail1, purposeids1, TestData.projects.expirationdate1, TestData.projects.projectdocumentid1,
                TestData.projects.sopapub, TestData.projects.sopanew, TestData.projects.sopaheader, TestData.projects.esd, TestData.projects.cenodecision);
        } catch (IOException ex) {
            Logger.getLogger(ProjectDocumentTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    @AfterClass
    public static void cleanupDependencies()
    {
        ProjectClient projclient = null;
        try {
            projclient = new ProjectClient();
        } catch (Exception ex) {
            Assert.fail();
        }
        
        // Clean up the Project
        try {
            projclient.deleteProject(TestData.projects.type1, TestData.projects.id1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectDocumentTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }
}