package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.documentcontainer.*;

import beanutil.*;
import entity.CARAPhaseDocs;
import entity.CARAProjects;
import entity.CommentPhases;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.annotation.Resource;
import javax.ejb.EJBContext;


/*
 * For this class, I've tried to adhere to the following terminology:
 *
 * container tree - the complete hierarchy of Containers and Containerdocs for
 *                  a given project, in particular the XML resource form for
 *                  the hierarchy.  The root of the tree is always a single
 *                  "Containers" node
 *
 * subtree - a container tree, or a branch of the tree, including both Containers
 *           and Containerdocs
 *
 * Container - a "folder" node, the corresponding entity consists of a row in the
 *             ProjectDocumentContainers table.  Each row has a reference to its
 *             parent container (ContainerId) and an integer ID provided by PALS
 *             (PalsContId) which must be project-unique.  However, this type
 *             is not used for the root of the tree (see below: Containers)
 *
 * Containers - a special resource type used to indicate the root of the
 *              container tree.  Unlike Container resources, the root
 *              of the tree does not have a corresponding row in the
 *              ProjectDocumentContainers table, and is indicated as the parent
 *              of other Containers and Containerdocs with a NULL ContainerId.
 *              (This unfortunate naming convention also means that the
 *              Containers type is NOT a list of Container resources)
 *
 * Containerdoc - a "document" node.  This corresponds to an entry in the
 *                ProjectDocuments table because I didn't feel like breaking
 *                out the Containerdoc attributes into another table that would
 *                be 1:1 with ProjectDocuments anyway
 *
 * Containerdocs - a resource type consisting of a list of Containerdocs
 *
 * Template - a version of the container tree that only has Container nodes,
 *            and no Containerdocs.  This is because a large number of use cases
 *            only require access to folder nodes
 *
 * The first version of this API required all updates to provide complete container
 * trees, but this became a problem when the number of documents in each tree
 * was respecified to have no limit, and project document counts reached the tens
 * of thousands from CARA comment periods.
 *
 * Unlike complete updates to the container tree, updating the Template includes
 * an implicit promise to not move documents out of their parent Containers.  The
 * corresponding requirement in the request is that no Container having documents
 * may be removed (although Containers that contain only empty Containers are OK
 * to delete)
 */

@Stateless(mappedName="DocumentContainerManagerRemote")
public class DocumentContainerManager implements DocumentContainerManagerRemote {

    @PersistenceContext
    private EntityManager em;

    @Resource
    private EJBContext context;

    /*
     * Create a persistent container tree (containers, docs) from a complete tree specification
     *
     * The actual "Containers" object is just a root node for any other
     * Container or Containerdoc objects, so there's actually nothing to
     * persist for it.
     *
     * Instead, each of the contained objects will have to be persisted
     * individually.
     *
     * This means:
     *
     *   - walking the specified tree of containers and containerdocs
     *
     *   - for every container, immediately create and persist a
     *     ProjectDocumentContainers object, then add the child objects
     *
     *   - for every containerdoc, update the corresponding ProjectDocuments
     *     row to point to the parent container
     */
    @Override
    public void create(String projecttype, String projectid, Containers resource) throws DataMartException
    {
        List<java.io.Serializable> contents = resource.getContainerOrContainerdoc();
        entity.Projects project = DataMartBeanUtil.getProject(em, projecttype, projectid);
        if (project == null) {
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND, "Project '"+projectid+"' with type '"+projecttype+"' does not exist.");
        }else{
            createSubtree(project, null, contents);
        }
    }

    /*
     * Retrieve the complete container tree (containers, docs) for a project
     *
     * Return a tree of Container and Containerdoc resources within a
     * Containers root
     *
     * If pubflag is specified, only containerdocs with a matching pubflag value
     * will be returned
     * If pubflag is null, all containerdocs are returned
     */
    @Override
    public Containers get(String projecttype, String projectid, Boolean pubflag, Boolean ignorecaraflag, Boolean sortdesc,String viewId) throws DataMartException
    {
        entity.Projects project = DataMartBeanUtil.getProject(em, projecttype, projectid);
        if (project == null) {
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND, "Project '"+projectid+"' with type '"+projecttype+"' does not exist.");
        }
        Containers result = new Containers();

        // Get the container tree for this project, starting at the root (parent container = null),
        // with documents having a publish status matching pubflag,
        // and store it under the result container resource
        getSubtree(project, null, false, pubflag, ignorecaraflag, result.getContainerOrContainerdoc(),sortdesc,viewId);

        return result;
    }

    /*
     * Update a complete container tree (containers, docs)
     *
     * Note that an update request just replaces the entire container tree
     * so this is functionally equivalent to a delete followed by a create
     *
     * The non-destructive function for updating just the container nodes (without
     * losing any contained documents) is updateContainerTemplate
     */
    @Override
    public void update(String projecttype, String projectid, Containers resource) throws DataMartException
    {
        // Delete old container tree
        delete(projecttype, projectid);
        // Create new container tree
        create(projecttype, projectid, resource);
    }

    /*
     * Delete a container tree
     *
     * Note that this also deletes "container docs" by resetting the
     * ContainerId on any project documents attached to this project
     *
     * There is an ON DELETE SET NULL in the database definition for ProjectDocuments,
     * but unfortunately JPA does not support this correctly, so we will have to
     * remove the document associations by ourselves
     */
    @Override
    public void delete(String projecttype, String projectid) throws DataMartException
    {
        entity.Projects project = DataMartBeanUtil.getProject(em, projecttype, projectid);

        // Remove all ContainerDoc references
        Query query = em.createQuery("SELECT p FROM ProjectDocuments p WHERE p.projects = :projectid");
        query.setParameter("projectid", project);
        List doclist = query.getResultList();
        Iterator i = doclist.iterator();
        while (i.hasNext())
        {
            entity.ProjectDocuments doc = (entity.ProjectDocuments)i.next();
            doc.setProjectDocumentContainers(null);
            doc.setOrderTag(0);
            em.merge(doc);
        }

        // Delete all Containers
        query = em.createQuery("SELECT p FROM ProjectDocumentContainers p WHERE p.projects = :project AND p.projectDocumentContainers IS NULL");
        query.setParameter("project", project);

        List containerlist = query.getResultList();
        i = containerlist.iterator();
        while (i.hasNext())
        {
            entity.ProjectDocumentContainers cont = (entity.ProjectDocumentContainers)i.next();
            em.remove(em.merge(cont));
        }
    }

    @Override
    public Boolean ping()
    {
        return true;
    }

    /*
     * Retrieve a list of container docs from a specific container
     *
     * The container is specified by contid.  Results are paged by start/rows.
     * This addresses the issue of how to retrieve container documents from a
     * project that can potentially have unlimited documents.
     */
    @Override
    public Containerdocs getContainerDocsFromContainer(String projectType, String projectId, Integer contid, Integer start, Integer rows, Integer phaseId, Integer viewId)
            throws DataMartException
    {
        // get Project
        entity.Projects project = DataMartBeanUtil.getProject(em, projectType, projectId);

        Query query = null;

        if (contid == null) {
            // get list of documents in root container
            String sql = "SELECT d from ProjectDocuments d WHERE d.projects = :project AND d.projectDocumentContainers IS NULL ORDER BY d.orderTag, d.documentId";
            query = em.createQuery(sql);
            query.setParameter("project", project);
        } else {
            // get list of documents in non-root container
            // get Container
            entity.ProjectDocumentContainers container = getContainerEnt(project, contid);

            // get list of documents in container
            String sql = "SELECT d from ProjectDocuments d WHERE d.projectDocumentContainers = :container ORDER BY d.orderTag, d.documentId";
            if(null != viewId)
            sql = "SELECT d from ProjectDocuments d, ProjectViewDocs p WHERE  p.viewId.viewId=:viewId AND p.docId.documentId = d.documentId AND d.projectDocumentContainers = :container ORDER BY d.orderTag, d.documentId";
            if(null != phaseId)
            sql = "SELECT d from ProjectDocuments d WHERE d.projectDocumentContainers = :container and d.phaseId= :phaseid ORDER BY d.orderTag, d.documentId";

            query = em.createQuery(sql);
            query.setParameter("container", container);
            if(null != viewId)
            query.setParameter("viewId", viewId);
            if(null != phaseId)
            query.setParameter("phaseid", phaseId);
        }

        if (start != null) {
            query.setFirstResult(start);
        }
        if (rows != null) {
            query.setMaxResults(rows);
        }
        @SuppressWarnings("unchecked")
        List<entity.ProjectDocuments> resultlist = (List<entity.ProjectDocuments>)query.getResultList();

        Containerdocs result = new Containerdocs();
        for (entity.ProjectDocuments pdoc : resultlist) {
            Containerdoc cdoc = createContainerdocRsrc(pdoc);
            result.getContainerdoc().add(cdoc);
        }
        return result;
    }

    /*
     * Update a list of container docs for a specific project
     *
     * Process a list of containerdocs with attribute updates,
     * possibly including parentcontid (the contid of the new parent container)
     *
     * Containerdocs may be incomplete--ignore any missing attributes
     */
    @Override
    public void updateContainerDocs(String projectType, String projectId, Containerdocs doclist, boolean fixedflag, boolean forceParentUpdate)
            throws DataMartException
    {
        // get Project
        entity.Projects project = DataMartBeanUtil.getProject(em, projectType, projectId);

        // get container-only tree, convert to hashmap of contids to containers
        Containers containerRoot = getContainerTemplate(projectType, projectId);
        HashMap<Integer, Container> contidmap = new HashMap<Integer, Container>();
        Container cont = new Container();
        // (convert container root into a generic container)
        cont.getContainerOrContainerdoc().addAll(containerRoot.getContainerOrContainerdoc());
        makeContainerMap(cont, contidmap);

        // update each document
        for (Containerdoc doc : doclist.getContainerdoc()) {

            // if parentcontid is specified, get the parent container entity
            // if parentcontid is null, the parent container is the root, which
            // is represented by a null container as well
            entity.ProjectDocumentContainers parentContainer = null;
            if (doc.getParentcontid() != null) {
                Integer contid = doc.getParentcontid();
                parentContainer = getContainerEnt(project, contid);
            }

            //if fixedflag is set, only update container docs in a fixedflag folder
            //this is put in place to prevent updates to non-fixedflag documents
            if (fixedflag){
                if (!parentContainer.getFixedFlag()){
                    continue;
                }
            }
            // update the document attributes
            updateDocEnt(project, parentContainer, doc, forceParentUpdate);
        }
        //project.setLastUpdate(new java.util.Date() );
        //em.merge(project);

    }

    /*
     * Retrieve the tree of containers for a project, but omit all containerdocs
     *
     * There is no limit on the number of containers for a project, but it is
     * generally expected that this number will be small (<100)
     */
    public Containers getContainerTemplate(String projectType, String projectId)
            throws DataMartException
    {
        entity.Projects project = DataMartBeanUtil.getProject(em, projectType, projectId);
        if (project == null) {
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND, "Project '"+projectId+"' with type '"+projectType+"' does not exist.");
        }
        Containers result = new Containers();

        // Get the container tree for this project, starting at the root (parent container = null),
        // and store it under the result container resource
        getSubtree(project, null, true, null, null, result.getContainerOrContainerdoc(),null,null);

        return result;
    }
    /*
     * Retrieve the tree of containers for a project, but omit all containerdocs
     *
     * There is no limit on the number of containers for a project, but it is
     * generally expected that this number will be small (<100)
     */
    public Containers getDownloaderContainerTemplate(String projectType, String projectId,String viewId)
            throws DataMartException
    {
        entity.Projects project = DataMartBeanUtil.getProject(em, projectType, projectId);
        if (project == null) {
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND, "Project '"+projectId+"' with type '"+projectType+"' does not exist.");
        }
        Containers result = new Containers();

        // Get the container tree for this project, starting at the root (parent container = null),
        // and store it under the result container resource
        if(null != viewId)
        getDownloaderSubtree(project, null, false, null, null, result.getContainerOrContainerdoc(),null,viewId);    
        else    
        getDownloaderSubtree(project, null, true, null, null, result.getContainerOrContainerdoc(),null,viewId);
        List<java.io.Serializable> containersAndContainerDocs = result.getContainerOrContainerdoc();
	                for (Object o : containersAndContainerDocs) {
	                 if (isContainer(o) && ((Container)o).getContid().intValue()<0 ){
                             removeContainerDocs((Container)o);
                         }
                        }
                         
        return result;
    }

    /*
     * Update the tree of containers for a specific project, given a container
     * tree with no containerdocs
     *
     * Containerdocs should remain in their original containers
     *
     * New containers will be created, and missing containers will be deleted.
     * However, deleted containers may not contain containerdocs.
     * (Trees of empty containers may be deleted without incident)
     */
    public void updateContainerTemplate(String projectType, String projectId, Containers newContainerRoot)
            throws DataMartException
    {
        try {
            entity.Projects project = DataMartBeanUtil.getProject(em, projectType, projectId);

            // Make a map of all old (existing) container nodes, indexed by PALS container ID
            Containers oldContainerRoot = getContainerTemplate(projectType, projectId);
            Container oldcontainer = new Container();
            oldcontainer.getContainerOrContainerdoc().addAll(oldContainerRoot.getContainerOrContainerdoc());
            HashMap<Integer, Container> oldContainersMap = new HashMap<Integer, Container>();
            makeContainerMap(oldcontainer, oldContainersMap);

            // Make a map of all moved (processed) container nodes, which is initially empty
            HashMap<Integer, Container> movedContainersMap = new HashMap<Integer, Container>();

            // Walk the new container tree, updating nodes as necessary
            // As each old node is processed, move it from the "old" list to the "moved" list
            for (Object c : newContainerRoot.getContainerOrContainerdoc()) {
                if (c instanceof Container) {
                    Container newcontainer = (Container)c;
                    remapContainerTree(project, null, newcontainer, oldContainersMap, movedContainersMap); // recursive
                } else {
                    throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION,
                    "Container template update may only have container nodes, " +
                    "bad request for project " +
                    project.getProjectTypes().getId() + " / " + project.getPublicId());
                }
            }

            // After walking the tree, any nodes still in the "old" list should be
            // deleted.  If any of these container nodes still has contained documents,
            // then it cannot be deleted.  Roll back.
            Collection<Container> oldContainerList = (Collection<Container>)(oldContainersMap.values());
            for (Container c : oldContainerList) {
                Containerdocs d = getContainerDocsFromContainer(projectType, projectId, c.getContid(), 0, 1,null,null);
                if (d.getContainerdoc().size() > 0) {
                    throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION, "Container " +
                    c.getContid() + " in project " + projectType + "/" +
                    projectId + " has documents, cannot be deleted");
                }
            }
            // Containers are empty, go ahead and delete them
            for (Container c : oldContainerList) {
                try {
                    deleteContainerEnt(project, c.getContid());
                } catch (DataMartException e) {
                    // Containers will cascade-delete, so we might have requests
                    // to delete containers that have just been removed
                    // Allow errors for missing containers, otherwise rethrow
                    if (e.getCode() != DataMartException.RESOURCE_NOT_FOUND) {
                        throw e;
                    }
                }
            }
            //Apply timestamp to project
            project.setLastUpdate(new java.util.Date());
            em.persist(project);
        } catch (DataMartException e) {
            context.setRollbackOnly();
            throw e;
        }
    }

    /*
     * =========================================================================
     * Private methods start here
     * =========================================================================
     */

    /*
     * Walk the new container tree, and at each node, find the corresponding old node
     * (based on PALS container ID) and remap it into the new tree
     *
     * As each old node is processed, move it from the "old" map to the "moved" map
     *
     * If a matching old node cannot be found, create a new node
     */
    private void remapContainerTree(entity.Projects project, entity.ProjectDocumentContainers parent, Container newnode, HashMap<Integer, Container> oldmap, HashMap<Integer, Container> movedmap)
        throws DataMartException
    {
        Integer contid = newnode.getContid();
        /*
         * "movedmap" is the collection of nodes that have already been remapped
         *
         * If the newnode contid already exists in the "moved" map, then the new
         * tree has duplicate contid values and is invalid.  Roll back.
         */
        if (movedmap.containsKey(contid)) {
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION,
                "Cannot update container template for " +
                project.getProjectTypes().getId() + " / " + project.getPublicId() +
                ", bad request contains duplicate contid value " + contid);
        }
        /*
         * "oldmap" is the collection of nodes that have not been remapped yet
         *
         * If a node with a matching contid exists in the "old" map,
         * remap it in the Datamart with any attribute changes.  Then move
         * that node to the "moved" map
         */
        entity.ProjectDocumentContainers pdc = null;
        if (oldmap.containsKey(contid)) {
            pdc = updateContainerEnt(project, parent, newnode);
            movedmap.put(contid, oldmap.get(contid));
            oldmap.remove(contid);
        } else {
            /* else create a new Container node. */
            pdc = createContainerEnt(project, parent, newnode);
            movedmap.put(contid, newnode);
        }

        /* remap all child containers */
        for (Object childnode : newnode.getContainerOrContainerdoc()) {
            if (childnode instanceof Container) {
                Container newcontainer = (Container)childnode;
                remapContainerTree(project, pdc, (Container)childnode, oldmap, movedmap);
            } else {
                throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION,
                "Cannot update container template for " +
                project.getProjectTypes().getId() + " / " + project.getPublicId() +
                ", bad request contains non-container nodes");
            }
        }
    }

    /*
     * Store a list of containers and containerdocs into a single parent container node
     *
     * This function calls itself recursively on any subcontainers, and so creates
     * a persistent tree of containers and containerdocs, one node at a time
     */
    private void createSubtree(
            entity.Projects project,
            entity.ProjectDocumentContainers parentcontainer,
            List<java.io.Serializable> containercontents)
            throws DataMartException
    {
        Iterator<java.io.Serializable> li = containercontents.listIterator();
        while (li.hasNext()) {
            Object item = li.next();
            if (item.getClass() == Container.class)
            {
                entity.ProjectDocumentContainers newparent = createContainerEnt(project, parentcontainer, (Container)item);
                createSubtree(project, newparent, ((Container)item).getContainerOrContainerdoc());
            } else if (item.getClass() == Containerdoc.class) {
                updateDocEnt(project, parentcontainer, (Containerdoc)item, false);
            } else {
                throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION, "Unknown class '" + item.getClass().getName() + "'.");
            }
        }
    }

    /*
     * Retrieve a list of the contents of a single parent container
     *
     * This is essentially the inverse operation of createSubtree.  The function
     * retrieves the contents of a single container, and recursively calls
     * itself for each subcontainer.  The final list then contains complete
     * trees of containers and (optionally) containerdocs for the given project
     *
     * If containersOnly is true, then omit all containerdocs
     *
     * If pubflag is not null, only fetch documents with matching pubflags
     */
    private void getSubtree(entity.Projects project, entity.ProjectDocumentContainers parentcontainer, Boolean containersOnly, Boolean pubflag, Boolean ignoreCaraflag, List<java.io.Serializable> destination, Boolean sortdesc, String viewId)
            throws DataMartException
    {

        // Get list of containers
        Query query;
        if (parentcontainer == null)
        {
            if(null != sortdesc && sortdesc.booleanValue())
            query = em.createQuery("SELECT p FROM ProjectDocumentContainers p WHERE p.projects = :project AND p.projectDocumentContainers IS NULL ORDER BY p.orderTag desc, p.label");
            else
            query = em.createQuery("SELECT p FROM ProjectDocumentContainers p WHERE p.projects = :project AND p.projectDocumentContainers IS NULL ORDER BY p.orderTag, p.label");
            query.setParameter("project", project);
        } else {
            query = em.createQuery("SELECT p FROM ProjectDocumentContainers p WHERE p.projects = :project AND p.projectDocumentContainers = :container ORDER BY p.orderTag, p.label");
            query.setParameter("project", project);
            query.setParameter("container", parentcontainer);
        }

        List containerlist = query.getResultList();
        Iterator i = containerlist.iterator();
        while (i.hasNext())
        {
            entity.ProjectDocumentContainers entity = (entity.ProjectDocumentContainers)i.next();
            Container subcontainer = createContainerRsrc(entity);
            getSubtree(project, entity, containersOnly, pubflag, ignoreCaraflag, subcontainer.getContainerOrContainerdoc(),sortdesc,viewId);
            boolean isAdd = true;
            if(null != viewId){
            int count = getContainerCount(subcontainer,new ArrayList());
            if(count ==0)
            isAdd=false;
            }
            if(isAdd)
            destination.add(subcontainer);
        }

        //This flag allows the request to ignore all Documents within project folder 57.
        //Folder 57 is the CARA letters folder, which can grow especially large (200K+ docs)
        //It was implemented especially for the PRM application, which cannot and does
        //not need to present an interface for these documents.
        if (ignoreCaraflag != null && ignoreCaraflag == true){
            if (parentcontainer != null){
                if (parentcontainer.getPalsContId()!= null &&
                        parentcontainer.getPalsContId().intValue()==57){
                    containersOnly = true;
                }
            }
        }

        // Get list of documents
        if (containersOnly == false) {
            String sql = "SELECT p FROM ProjectDocuments p ";
            if(null != viewId){
            sql += "  , ProjectViewDocs d ";
            }
            sql += " WHERE p.projects = :project";
            if(null != viewId){
            sql += " AND d.viewId.viewId=:viewId AND d.docId.documentId = p.documentId ";
            }
            if (pubflag != null) {
                sql += " AND p.isPublished = :pubflag";
            }
            if (parentcontainer != null) {
                sql = sql + " AND p.projectDocumentContainers = :container";
            } else {
                sql = sql + " AND p.projectDocumentContainers IS NULL";
            }
            sql = sql + " ORDER BY p.orderTag, p.documentId";

            query = em.createQuery(sql);
            query.setParameter("project", project);
            if(null != viewId){
            query.setParameter("viewId", new Integer(viewId));
            }
            if (pubflag != null) {
                query.setParameter("pubflag", pubflag);
            }
            if (parentcontainer != null) {
                query.setParameter("container", parentcontainer);
            }

            List doclist = query.getResultList();

            i = doclist.iterator();
            while (i.hasNext())
            {
                entity.ProjectDocuments entity = (entity.ProjectDocuments)i.next();
                Containerdoc doc = createContainerdocRsrc(entity);
                //destination.add(doc);
                addInOrder(doc, destination);
            }
        }
    }
    /*
     * Retrieve a list of the contents of a single parent container
     *
     * This is essentially the inverse operation of createSubtree.  The function
     * retrieves the contents of a single container, and recursively calls
     * itself for each subcontainer.  The final list then contains complete
     * trees of containers and (optionally) containerdocs for the given project
     *
     * If containersOnly is true, then omit all containerdocs
     *
     * If pubflag is not null, only fetch documents with matching pubflags
     */
    private void getDownloaderSubtree(entity.Projects project, entity.ProjectDocumentContainers parentcontainer, Boolean containersOnly, Boolean pubflag, Boolean ignoreCaraflag, List<java.io.Serializable> destination, Boolean sortdesc, String viewId)
            throws DataMartException
    {
              // Get list of containers
        Query query;
        if (parentcontainer == null)
        {
            if(null != sortdesc && sortdesc.booleanValue())
            query = em.createQuery("SELECT p FROM ProjectDocumentContainers p WHERE p.projects = :project AND p.projectDocumentContainers IS NULL ORDER BY p.orderTag desc, p.label");
            else
            query = em.createQuery("SELECT p FROM ProjectDocumentContainers p WHERE p.projects = :project AND p.projectDocumentContainers IS NULL ORDER BY p.orderTag, p.label");
            query.setParameter("project", project);
        } else {
            query = em.createQuery("SELECT p FROM ProjectDocumentContainers p WHERE p.projects = :project AND p.projectDocumentContainers = :container ORDER BY p.orderTag, p.label");
            query.setParameter("project", project);
            query.setParameter("container", parentcontainer);
        }

        List containerlist = query.getResultList();
        Iterator i = containerlist.iterator();
        while (i.hasNext())
        {
            entity.ProjectDocumentContainers entity = (entity.ProjectDocumentContainers)i.next();
            Container subcontainer = createContainerRsrc(entity);
            getDownloaderSubtree(project, entity, containersOnly, pubflag, ignoreCaraflag, subcontainer.getContainerOrContainerdoc(),sortdesc,viewId);
             if(subcontainer.getContid().intValue()==57){

                     query = em.createQuery("SELECT p FROM CommentPhases p WHERE p.cARAProjects.projects = :project");
                     query.setParameter("project", project);
                     @SuppressWarnings("unchecked")
                     List<entity.CommentPhases> commentPhases = (List<entity.CommentPhases>)query.getResultList();
                     if(null !=commentPhases && commentPhases.size()>0){
                        Iterator it = commentPhases.iterator();
                        while (it.hasNext()) {
                            CommentPhases phase =(CommentPhases)it.next();
                            query = em.createQuery("SELECT COUNT(c) FROM ProjectDocuments c WHERE c.projects = :project AND c.projectDocumentContainers =:container and c.phaseId =:phaseid");
                            query.setParameter("project", project);
                            query.setParameter("container", entity);
                            query.setParameter("phaseid", new Integer(phase.getPhaseId()));
                            Long phasedoccount = (Long)query.getSingleResult();

                            query = em.createQuery("SELECT COUNT(c) FROM ProjectDocuments c WHERE c.projects = :project AND c.projectDocumentContainers =:container and c.phaseId =:phaseid AND c.isSensitive=true");
                            query.setParameter("project", project);
                            query.setParameter("container", entity);
                            query.setParameter("phaseid", new Integer(phase.getPhaseId()));
                            Long phasesensitiveCount = (Long)query.getSingleResult();

                            query = em.createQuery("SELECT SUM(SUBSTRING(c.pdfSize,1, LENGTH(c.pdfSize)-2))  FROM ProjectDocuments c WHERE c.projects = :project AND c.projectDocumentContainers =:container and c.phaseId =:phaseid");
                            query.setParameter("project", project);
                            query.setParameter("container", entity);
                            query.setParameter("phaseid", new Integer(phase.getPhaseId()));
                            Double phasetotalsize = null != query.getSingleResult()?(Double)query.getSingleResult():new Double(0);

                            subcontainer = createDownloaderContainerRsrc(entity,phasesensitiveCount.intValue()>0?Boolean.TRUE:Boolean.FALSE,Boolean.TRUE,phasedoccount.intValue(),phasetotalsize.doubleValue()+"kb");
                            if(commentPhases.size()>1){
                            subcontainer.setLabel(subcontainer.getLabel()+" - "+phase.getName());
                            subcontainer.setPhaseid(phase.getPhaseId());
                            }
                            boolean isAdd = true;    
                            if(null != viewId )   
                            {
                              int count = getContainerCount(subcontainer,new ArrayList());
                              if(count ==0)
                              isAdd=false;       
                            }
                            if(isAdd)
                            destination.add(subcontainer);
                            
                         }
                    }
                    else
                     {
                        String sql = "SELECT COUNT(d) from ProjectDocuments d WHERE d.projects = :project AND d.projectDocumentContainers =:container AND d.isSensitive=true";
                        query = em.createQuery(sql);
                        query.setParameter("project", project);
                        query.setParameter("container", entity);
                        Long sensitiveCount = (Long)query.getSingleResult();

                        query = em.createQuery("SELECT COUNT(c) FROM ProjectDocuments c WHERE c.projects = :project AND c.projectDocumentContainers =:container");
                        query.setParameter("project", project);
                        query.setParameter("container", entity);
                        Long doccount = (Long)query.getSingleResult();

                        query = em.createQuery("SELECT SUM(SUBSTRING(c.pdfSize,1, LENGTH(c.pdfSize)-2))   FROM ProjectDocuments c WHERE c.projects = :project AND c.projectDocumentContainers =:container");
                        query.setParameter("project", project);
                        query.setParameter("container", entity);
                        Double filesize = null != query.getSingleResult()?(Double)query.getSingleResult():new Double(0);
                        subcontainer = createDownloaderContainerRsrc(entity,sensitiveCount.intValue()>0?Boolean.TRUE:Boolean.FALSE,Boolean.TRUE,doccount.intValue(),filesize.doubleValue()+"kb");
                        boolean isAdd = true;    
                        if(null != viewId )   
                        {
                          int count = getContainerCount(subcontainer,new ArrayList());
                          if(count ==0)
                          isAdd=false;       
                        }
                        if(isAdd)
                        destination.add(subcontainer);
                     }

             }
             else
             { 
                boolean isAdd = true;    
                if(null != viewId )   
                {
                   int count = getContainerCount(subcontainer,new ArrayList());
                   if(count ==0)
                   isAdd=false;       
                }
                if(isAdd)
                destination.add(subcontainer);
             }
        }

        //This flag allows the request to ignore all Documents within project folder 57.
        //Folder 57 is the CARA letters folder, which can grow especially large (200K+ docs)
        //It was implemented especially for the PRM application, which cannot and does
        //not need to present an interface for these documents.
        if (ignoreCaraflag != null && ignoreCaraflag == true){
            if (parentcontainer != null){
                if (parentcontainer.getPalsContId()!= null &&
                        parentcontainer.getPalsContId().intValue()==57){
                    containersOnly = true;
                }
            }
        }

        // Get list of documents
        if (containersOnly == false) {
            String sql = "SELECT p FROM ProjectDocuments p ";
            if(null != viewId){
            sql += "  , ProjectViewDocs d ";
            }
            sql += " WHERE p.projects = :project";
            if(null != viewId){
            sql += " AND d.viewId.viewId=:viewId AND d.docId.documentId = p.documentId ";
            }
            if (pubflag != null) {
                sql += " AND p.isPublished = :pubflag";
            }
            if (parentcontainer != null) {
                sql = sql + " AND p.projectDocumentContainers = :container";
            } else {
                sql = sql + " AND p.projectDocumentContainers IS NULL";
            }
            sql = sql + " ORDER BY p.orderTag, p.documentId";

            query = em.createQuery(sql);
            query.setParameter("project", project);
            if(null != viewId){
            query.setParameter("viewId", new Integer(viewId));
            }
            if (pubflag != null) {
                query.setParameter("pubflag", pubflag);
            }
            if (parentcontainer != null) {
                query.setParameter("container", parentcontainer);
            }

            List doclist = query.getResultList();

            i = doclist.iterator();
            while (i.hasNext())
            {
                entity.ProjectDocuments entity = (entity.ProjectDocuments)i.next();
                Containerdoc doc = createContainerdocRsrc(entity);
                //destination.add(doc);
                addInOrder(doc, destination);
            }
        }
    }

    //Place documents in specified order within the destination object.
    private void addInOrder(Containerdoc doc, List<java.io.Serializable> destination)
    {
        if (doc.getOrder() != null){
            int order = doc.getOrder();
            for (int i = 0; i < destination.size(); i++){
                Object o = destination.get(i);
                int currentOrder = -1;
                if (o instanceof Container){
                    Container c = (Container) o;
                    currentOrder = c.getOrder();
                }else{
                    Containerdoc d = (Containerdoc) o;
                    currentOrder = d.getOrder();
                }
                if (order < currentOrder){
                    destination.add(i, doc);
                    return;
                }
            }
        }
        //If entire list is iterated and Containerdoc has not been inserted, add to end
        destination.add(doc);

    }

    /*
     * Create a container entity, return entity reference
     *
     * "parent" is a reference to the parent container entity, set to null if
     * the parent is the root container
     */
    private entity.ProjectDocumentContainers createContainerEnt(
            entity.Projects project,
            entity.ProjectDocumentContainers parent,
            Container containerRsrc)
            throws DataMartException
    {
        entity.ProjectDocumentContainers e = new entity.ProjectDocumentContainers();
        e.setProjects(project);
        e.setLabel(containerRsrc.getLabel());
        e.setOrderTag(containerRsrc.getOrder());
        e.setPalsContId(containerRsrc.getContid());
        e.setProjectDocumentContainers(parent);
        if (containerRsrc.isFixedflag() != null){
            e.setFixedFlag(containerRsrc.isFixedflag());
        }else{
            e.setFixedFlag(false);
        }
        e = em.merge(e);

        return e;
    }

    /*
     * Get a container entity, based on contid (assigned by PALS)
     *
     * There is no database record for the "root" container, so this function
     * will not work at all for the container root.
     *
     * Throw exception if contid can't be found
     * Throw exception if multiple contids found--contids should be unique
     */
    private entity.ProjectDocumentContainers getContainerEnt(entity.Projects project, Integer contid)
        throws DataMartException
    {
        Query query = null;
        if (contid == null) {
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION, "getContainerEnt() needs non-null container ID");
        }

        String sql = "SELECT c from ProjectDocumentContainers c where c.projects = :project AND c.palsContId = :contid";
        query = em.createQuery(sql);
        query.setParameter("project", project);
        query.setParameter("contid", contid);

        @SuppressWarnings("unchecked")
        List<entity.ProjectDocumentContainers> resultlist = query.getResultList();
        if (resultlist.isEmpty()) {
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION, "Cannot find container "+ contid +
                " in project " + project.getProjectTypes().getId() + " / " +
                project.getPublicId());
        } else if (resultlist.size() > 1) {
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION, "Found multiple containers with ID "+ contid +
                " in project " + project.getProjectTypes().getId() + " / " +
                project.getPublicId());
        }

        return resultlist.get(0);
    }

    /*
     * Update a container entity
     *
     * The target container is identified by project / contid (from the container resource)
     * "parent" is a reference to the parent container entity
     * "newContainer" is a container resource with the new container attributes
     * Null or missing attributes are ignored.  Non-null values are applied to the
     * target container entity
     *
     * Return a reference to the updated entity
     */
    private entity.ProjectDocumentContainers updateContainerEnt(entity.Projects project, entity.ProjectDocumentContainers parent, Container newContainer)
            throws DataMartException
    {
        Integer contid = newContainer.getContid();
        if (contid == null) {
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION,
                    "Cannot update container with null contid for project " +
                    project.getProjectTypes().getId() + " / " + project.getPublicId());
        }
        entity.ProjectDocumentContainers e = getContainerEnt(project, contid);

        if (newContainer.getLabel() != null) {
            e.setLabel(newContainer.getLabel());
        }
        if (newContainer.getOrder() != null) {
            e.setOrderTag(newContainer.getOrder());
        }
        if (newContainer.isFixedflag() != null){
            e.setFixedFlag(newContainer.isFixedflag());
        }
        e.setProjectDocumentContainers(parent);
        e = em.merge(e);

        return e;
    }

    /*
     * Delete a container entity
     *
     * The target container is identified by project / contid
     *
     * This function does not check to see if the container is empty.
     * The current default behavior when deleting a non-empty container is to
     * re-home the contents to the container root.
     */
    private void deleteContainerEnt(entity.Projects project, Integer contid) throws DataMartException
    {
        // Get reference to Container
        Query query = em.createQuery("SELECT c FROM ProjectDocumentContainers c WHERE c.projects = :project AND c.palsContId = :contid");
        query.setParameter("project", project);
        query.setParameter("contid", contid);

        @SuppressWarnings("unchecked")
        List<entity.ProjectDocumentContainers> resultlist = (List<entity.ProjectDocumentContainers>)query.getResultList();
        if (resultlist.isEmpty()) {
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND, "Container " +
                contid + " in project " + project.getProjectTypes().getId() + "/" +
                project.getPublicId() + " is missing, cannot be deleted");
        }
        if (resultlist.size() > 1) {
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION, "Container " +
                contid + " in project " + project.getProjectTypes().getId() + "/" +
                project.getPublicId() + " is not unique, cannot be deleted");
        }
        entity.ProjectDocumentContainers container = resultlist.get(0);
        if (container.getProjectDocumentsCollection().size() > 0) {
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION, "Container " +
                contid + " in project " + project.getProjectTypes().getId() + "/" +
                project.getPublicId() + " has documents, cannot be deleted");
        }
        // Delete Container
        em.remove(em.merge(container));
    }

    /*
     * Update a document entity
     *
     * "docRsrc" contains non-null attributes used to update the document, and
     * any null attributes are simply ignored
     * "parentContainer" contains the new container for the document.  A null
     * parentContainer will reassign the document to the container tree root
     *
     * Note that Containerdoc resources map to ProjectDocument entities, so this
     * actually  updates ProjectDocuments
     */
    private void updateDocEnt(
            entity.Projects project,
            entity.ProjectDocumentContainers parentContainer,
            Containerdoc docRsrc,
            boolean forceParentUpdate
    )
            throws DataMartException
    {
        String docid = docRsrc.getDocid();
        entity.ProjectDocuments projectdoc = DataMartBeanUtil.getProjectDocument(em, project, docid);

        if (projectdoc == null) {
            /* Project-document does not exist */
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND, "Cannot find document "+ docid +
                    " in project with type '" + project.getProjectTypes().getId() + "' and id '" +
                    project.getPublicId() + "'." );
        }

        // Only update attributes that have been provided with non-null values
        if (parentContainer != null || forceParentUpdate) {
            projectdoc.setProjectDocumentContainers(parentContainer);
        }
        if (docRsrc.getOrder() != null) {
            projectdoc.setOrderTag(docRsrc.getOrder());
        }
        if (docRsrc.isPubflag() != null) {
        //If document going from unpublished to published, set DatePublished timestamp
            if (docRsrc.isPubflag() && !projectdoc.getIsPublished() && projectdoc.getDatePublished() == null){
                projectdoc.setDatePublished(new java.util.Date());
            }
            projectdoc.setIsPublished(docRsrc.isPubflag());

        }


        projectdoc = em.merge(projectdoc);
    }

    /*
     * Convert a ProjectDocumentContainers entity into a Container resource
     */
    private Container createContainerRsrc(
            entity.ProjectDocumentContainers entity)
            throws DataMartException
    {
        Container resource = new Container();
        resource.setLabel(entity.getLabel());
        resource.setOrder(entity.getOrderTag());
        resource.setContid(entity.getPalsContId());
        resource.setFixedflag(entity.getFixedFlag());

        return resource;
    }
 /*
     * Convert a ProjectDocumentContainers entity into a Container resource for downloader
     */
    private Container createDownloaderContainerRsrc(
            entity.ProjectDocumentContainers entity, Boolean sensitive, Boolean selectable, int docsCount, String fileSize)
            throws DataMartException
    {
        Container resource = new Container();
        resource.setLabel(entity.getLabel());
        resource.setOrder(entity.getOrderTag());
        resource.setContid(entity.getPalsContId());
        resource.setFixedflag(entity.getFixedFlag());
        resource.setContid(entity.getPalsContId());
        resource.setFixedflag(entity.getFixedFlag());
        resource.setSensitive(sensitive);
        resource.setSelectable(selectable);
        resource.setDoccount(new Integer(docsCount));
        resource.setFilesizetotal(fileSize);
        return resource;
    }
    /*
     * Convert a ProjectDocument entity into a Containerdoc resource
     */
    private Containerdoc createContainerdocRsrc(
            entity.ProjectDocuments entity)
            throws DataMartException
    {
        Containerdoc resource = new Containerdoc();
        resource.setDocid(entity.getDocumentId());
        resource.setOrder(entity.getOrderTag());
        return resource;
    }

    /*
     * Convert a container tree resource into a map of contids (integers) to containers
     *
     * This gives us a quick way to track which container nodes have been processed
     * during template updates
     */
    private void makeContainerMap(Container container, HashMap<Integer, Container> map)
            throws DataMartException
    {
        for (Object obj : container.getContainerOrContainerdoc()) {
            if (obj instanceof Container) {
                Container cont = (Container)obj;
                map.put(cont.getContid(), cont);
                makeContainerMap(cont, map);    // depth-first traversal
            }
        }
    }

    @Override
    public void updatePhaseIds() throws DataMartException {
     Query query = null;


        String sql = "SELECT c.id from ProjectDocumentContainers c where  c.palsContId = :contid";
        query = em.createQuery(sql);
        query.setParameter("contid", new Integer(57));

        @SuppressWarnings("unchecked")
        List resultlist = query.getResultList();
        if (!resultlist.isEmpty()) {
           Iterator i = resultlist.iterator();
            while (i.hasNext())
            {
                Object id=i.next();
                sql = "SELECT c from ProjectDocumentContainers c where  c.id = :id";
                query = em.createQuery(sql);
                query.setParameter("id", id);
                List<entity.ProjectDocumentContainers> contList = query.getResultList();
                if(!contList.isEmpty()){
                entity.ProjectDocumentContainers entity = (entity.ProjectDocumentContainers)contList.get(0);

                Collection <entity.ProjectDocuments> docs= entity.getProjectDocumentsCollection();
                for (Iterator it = docs.iterator(); it.hasNext();)
                {
                 entity.ProjectDocuments doc =(entity.ProjectDocuments)it.next();
                 if( null ==doc.getPhaseId())
                 {
                     try {
                         Hashtable result = ServiceHelper.getFileMetaData(doc.getDocumentId(),false);
                         Hashtable moreMeta =(Hashtable)result.get("moreMetadata");
                         String phaseid = String.valueOf (moreMeta.get("phaseid"));
                         if(null !=phaseid && !phaseid.equals("null")){
                         doc.setPhaseId(new Integer(phaseid));
                         em.merge(doc);
                         }
                     } catch (ServiceException ex) {
                         Logger.getLogger(DocumentContainerManager.class.getName()).log(Level.SEVERE, null, ex);
                     }
                 }
                }
             }
            }
        }



    }
     boolean removeContainerDocs(us.fed.fs.www.nepa.schema.documentcontainer.Container container)
	        {

			List<java.io.Serializable> containersAndContainerDocs = container.getContainerOrContainerdoc();
	                ArrayList removeList = new ArrayList();
                        boolean docExist = false;
                        for (Object o : containersAndContainerDocs) {	
                         if (isContainer(o)) {
	                  removeContainerDocs((us.fed.fs.www.nepa.schema.documentcontainer.Container)o);
	                 }
	                 if (isDoc(o)) {
                             docExist= true;
                             removeList.add(o);	                  
	                 }
	                }
	                containersAndContainerDocs.removeAll(removeList);
                        return docExist;
	        }
    int getContainerCount(us.fed.fs.www.nepa.schema.documentcontainer.Container container,List docsList)
	        {

			List<java.io.Serializable> containersAndContainerDocs = container.getContainerOrContainerdoc();
	                for (Object o : containersAndContainerDocs) {
	                 if (isContainer(o)) {
	                  getContainerCount((us.fed.fs.www.nepa.schema.documentcontainer.Container)o,docsList);
	                 }
	                 if (isDoc(o)) {
	                  docsList.add(o);
	                 }
	                }
	                return docsList.size();
	        }
        Boolean isContainer(Object o) {
		return (o.getClass() == us.fed.fs.www.nepa.schema.documentcontainer.Container.class);
	}

	Boolean isDoc(Object o) {

		return (o.getClass() == us.fed.fs.www.nepa.schema.documentcontainer.Containerdoc.class);
	}

}
