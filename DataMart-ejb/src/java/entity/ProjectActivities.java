package entity;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "ProjectActivities")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProjectActivities.findAll", query = "SELECT p FROM ProjectActivities p"),
    @NamedQuery(name = "ProjectActivities.findById", query = "SELECT p FROM ProjectActivities p WHERE p.id = :id")})
public class ProjectActivities implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @JoinColumn(name = "ProjectId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Projects projects;
    @JoinColumn(name = "ActivityId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private RefActivities refActivities;

    public ProjectActivities() {
    }

    public ProjectActivities(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Projects getProjects() {
        return projects;
    }

    public void setProjects(Projects projects) {
        this.projects = projects;
    }

    public RefActivities getRefActivities() {
        return refActivities;
    }

    public void setRefActivities(RefActivities refActivities) {
        this.refActivities = refActivities;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProjectActivities)) {
            return false;
        }
        ProjectActivities other = (ProjectActivities) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ProjectActivities[ id=" + id + " ]";
    }

}
