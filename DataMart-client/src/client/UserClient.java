package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.user.*;



public class UserClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public UserClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.user", "User.xsd");
    }


    /*
     * TODO: implement EntityTag logic for conditional requests. Do this for all clients.
     *
     * TODO: Figure out how to communicate the various flavors of failure back to the
     *      application. For example 404 should probably be handled differently than 503. Do this for all clients.
     */
    public void postUser(String shortname, String firstname, String lastname, String phone, String title, String email)
            throws IOException
    {
        User user = new User();
        user.setShortname(shortname);
        user.setFirstname(firstname);
        user.setLastname(lastname);
        user.setPhone(phone);
        user.setTitle(title);
        user.setEmail(email);
        postUser(user);
    }


    public void postUser(User user)
            throws IOException
    {
        post(objectfactory.createUser(user), "/users");
    }


    public User getUser(String shortname)
            throws IOException
    {
        return get("/users/" + shortname, User.class);
    }


    public Users getUserList()
            throws IOException
    {
        return get("/users", Users.class);
    }


    public void putUser(String shortname, String firstname, String lastname, String phone, String title, String email)
            throws IOException
    {
        User user = new User();
        user.setShortname(shortname);
        user.setFirstname(firstname);
        user.setLastname(lastname);
        user.setPhone(phone);
        user.setTitle(title);
        user.setEmail(email);
        putUser(user);
    }


    public void putUser(User user)
            throws IOException
    {
        put(objectfactory.createUser(user), "/users/" + user.getShortname());
    }


    public void deleteUser(String shortname)
            throws IOException
    {
        delete("/users/" + shortname);
    }

    public Usersearchresults findUsers(String shortname, String firstname, String lastname)
            throws IOException
    {
        String url = "/utils/findUser?";
        if (shortname.length() > 0) { url = url + "shortname=" + shortname; }
        if (firstname.length() > 0) { url = url + "firstname=" + firstname; }
        if (lastname.length() > 0) { url = url + "lastname=" + lastname; }
        return get(url, Usersearchresults.class);
    }
}