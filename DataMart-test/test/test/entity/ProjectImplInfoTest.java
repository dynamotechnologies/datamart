/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.entity;

import client.ProjectAdditionalInfoClient;
import client.ProjectImplInfoClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.ProjectImplInfo.ProjectImplInfo;

/**
 *
 * @author gauri
 */

public class ProjectImplInfoTest {
    private ProjectImplInfoClient client;
    

    @Before
    public void setUp()
            throws IOException
    {
        client = new ProjectImplInfoClient();
       
    }

    @Test
    public void testPost()
    {
        // This POST should fail, because it violates a constraint. The unit code is bad
        try {
           addProjectImplInfo();
            // If the above statement did not throw an exception then something has gone terribly wrong
            //Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.BAD_PARENTS)
                Assert.fail();
        } catch (Exception ex) {
            Logger.getLogger(ProjectImplInfoTest.class.getName()).log(Level.SEVERE, null, ex);
             Assert.fail();
        }

        

        
    }

    @Test
    public void testGet()
    {
     

        ProjectImplInfo project = null;
         
        try {
            project = client.getProjectImplInfo("43167");
        } catch (IOException ex) {
            Logger.getLogger(ProjectImplInfoTest.class.getName()).log(Level.SEVERE, null, ex);
             Assert.fail();
        }
            
          
       

       
     

       
    }

    @Test
    public void testPut()
    {
        

        ProjectImplInfo project = null;
        try {
            modifyProjectImplInfo();
            project = client.getProjectImplInfo("17735");
        } catch (Exception ex) {
            Logger.getLogger(ProjectImplInfoTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

       
    }

   
    
 


    @Test
    public void testDelete()
    {
        

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            deleteProjectImplInfo();
          
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        } catch (Exception ex) {
            Logger.getLogger(ProjectImplInfoTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
      
    }

 
    @After
    public void cleanFailed()
            throws Exception
    {
      
    }


    @BeforeClass
    public static void populateDependencies()
    {
        // No dependencies
    }

    @AfterClass
    public static void cleanupDependencies()
    {
        // No cleanup
    }
     public static void addProjectImplInfo() throws Exception{
	    	
	    	client.ProjectImplInfoClient  client = new client.ProjectImplInfoClient();
	    	us.fed.fs.www.nepa.schema.ProjectImplInfo.ProjectImplInfo p = new us.fed.fs.www.nepa.schema.ProjectImplInfo.ProjectImplInfo();
	    	p.setImplInfoId("2");
                p.setDescription("test2");
                p.setLastUpdateBy("oshroff");
                p.setLasteUpdateDate("2016-07-07");
                p.setProjectId("39356");
	    	client.postProjectImplInfo(p);
	    }
    
	 public static void deleteProjectImplInfo() throws Exception{
	    	
	    	client.ProjectImplInfoClient  client = new client.ProjectImplInfoClient();
                client.deleteProjectImplInfo("39356","2");
         }   
         
          public static void modifyProjectImplInfo() throws Exception{
	       client.ProjectImplInfoClient  client = new client.ProjectImplInfoClient();
	    	us.fed.fs.www.nepa.schema.ProjectImplInfo.ProjectImplInfo p = new us.fed.fs.www.nepa.schema.ProjectImplInfo.ProjectImplInfo();
	    	p.setImplInfoId("1");
                p.setDescription("test3");
                p.setLastUpdateBy("oshroff");
                p.setProjectId("17735");
                p.setLasteUpdateDate("2016-07-08");
	    	client.putProjectImplInfo(p);
         } 
}
