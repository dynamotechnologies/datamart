package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.status.*;

import beanutil.*;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import java.util.Iterator;


@Stateless(mappedName="StatusManagerRemote")
public class StatusManager implements StatusManagerRemote {
    
    @PersistenceContext
    private EntityManager em;


    public void create(Status resource) throws DataMartException
    {
        validate(resource);
        entity.RefStatuses entity = createEntity(resource);

        entity.RefStatuses badstatus = createEntity(resource.getId());
        if (badstatus != null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        entity = em.merge(entity);
    }

    public String getName(String id) throws DataMartException
    {
        entity.RefStatuses entity = createEntity(id);
        if (entity == null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return entity.getName();
    }

    public void update(Status resource) throws DataMartException
    {
        validate(resource);
        entity.RefStatuses entity = createEntity(resource);

        if (createEntity(resource.getId())==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        entity = em.merge(entity);
    }

    public void delete(String id) throws DataMartException
    {
        entity.RefStatuses entity = createEntity(id);
        if (entity == null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));
    }

    public Statuses getAll() throws DataMartException
    {
        // Retrieve all entities from the database
        Query query = em.createQuery("SELECT s FROM RefStatuses s");
        List statuslist =  query.getResultList();
        Iterator it = statuslist.iterator();
        // Loop over the database collection. Each row becomes a new object in the response list
        Statuses responselist = new Statuses();
        while (it.hasNext()) {
            responselist.getStatus().add(createResource( (entity.RefStatuses)it.next() ));
        }
        return responselist;
    }

    public Boolean ping()
    {
        return true;
    }

    /*
     *
     * This method does the mapping from jaxb Status to entity.Status and vice versa
     */
    private Status createResource(entity.RefStatuses entity)
            throws DataMartException
    {
        Status resource = new Status();
        resource.setId(entity.getId().toString());
        resource.setName(entity.getName());
        return resource;
    }

    private entity.RefStatuses createEntity(Status resource)
            throws DataMartException
    {
        int id;
        try {
            id = Integer.parseInt(resource.getId());
        } catch (NumberFormatException ex) {
            throw new DataMartException(DataMartException.BAD_FORMAT, "Id must be an integer");
        }
        entity.RefStatuses entity = new entity.RefStatuses();
        entity.setId(id);
        entity.setName(resource.getName());
        return entity;
    }

    // This version of overloaded method createEntity() accepts a String ID as a parameter, and returns the
    //   associated entity, or null if it doesn't exist
    private entity.RefStatuses createEntity(String id)
            throws DataMartException
    {
        int intId;
        try {
            intId = Integer.parseInt(id);
        } catch (NumberFormatException ex) {
            throw new DataMartException(DataMartException.BAD_FORMAT, "Id must be an integer");
        }
        return em.find(entity.RefStatuses.class, intId);
    }

    private void validate(Status resource)
            throws DataMartException
    {
        DataMartBeanUtil.confirmNotNull(resource.getName(), "Name");
    }
}
