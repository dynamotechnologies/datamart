/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package stateless;
import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.role.*;
import beanutil.DataMartException;

/**
 *
 * @author mhsu
 */
@Remote
public interface RoleManagerRemote {
    public void create(Role resource) throws DataMartException;
    public void update(Role resource) throws DataMartException;
    public void delete(int id) throws DataMartException;
    public Role get(int id) throws DataMartException;
    public Roles getAll() throws DataMartException;
    public Boolean ping();
}