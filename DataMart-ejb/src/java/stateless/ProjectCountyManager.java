package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import entity.ProjectCounties;
import us.fed.fs.www.nepa.schema.projectlocation.*;

import beanutil.*;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless(mappedName="ProjectCountyManagerRemote")
public class ProjectCountyManager implements ProjectCountyManagerRemote
{

    @PersistenceContext
    private EntityManager em;

    private String PROJECT_TYPE = "nepa";

    @Override
    public void merge(String projectid, String id)
             throws DataMartException
    {
        entity.ProjectCounties county = createEntity(projectid, id);

        // Check for existing entity and grab the internal ID if it exists
        entity.ProjectCounties oldcty = getEntity(projectid, id);
        if (oldcty!=null)
            county.setId(oldcty.getId());

        em.merge(county);
    }


    @Override
    public Counties get(String projectid, String id)
            throws DataMartException
    {
        Counties rval = new Counties();
        entity.ProjectCounties county = getEntity(projectid, id);
        if (county==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "NEPA Project " + projectid + " is not associated with county '" + id + "'");

        rval.getCountyid().add(county.getRefCounties().getId());
        return rval;
    }


    @Override
    public void delete(String projectid, String id)
            throws DataMartException
    {
        entity.ProjectCounties county = getEntity(projectid, id);
        if (county==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "NEPA Project " + projectid + " is not associated with county '" + id + "'");

        em.remove(county);
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    private entity.ProjectCounties createEntity(String projectid, String id)
            throws DataMartException
    {
        // Get a reference to the project
        entity.Projects project = DataMartBeanUtil.getProject(em, this.PROJECT_TYPE, projectid);
        if (project==null)
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION,
                    "Project with type '" + this.PROJECT_TYPE + "' and ID '" + projectid + "' does not exist.");

        // Get the reference county associated with this id
        entity.RefCounties ref = (entity.RefCounties) DataMartBeanUtil.getEntityById(em,
                "RefCounties", id, "County Code");

        entity.ProjectCounties county = new entity.ProjectCounties();
        county.setProjects(project);
        county.setRefCounties(ref);
        return county;
    }


    private entity.ProjectCounties getEntity(String projectid, String id)
            throws DataMartException
    {
        // Get a reference to the project
        entity.Projects project = DataMartBeanUtil.getProject(em, this.PROJECT_TYPE, projectid);
        if (project==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Project with type '" + this.PROJECT_TYPE + "' and ID '" + projectid + "' does not exist.");

        // Get the reference county associated with this id
        entity.RefCounties ref = (entity.RefCounties) DataMartBeanUtil.getEntityById(em,
                "RefCounties", id, "County Code");
        if (ref==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Reference County with id '" + id + "' does not exist.");

        // Query for the relationship table row
        Query q = em.createQuery("SELECT p FROM ProjectCounties p WHERE p.projects = :project AND p.refCounties = :ref");
        q.setParameter("project", project);
        q.setParameter("ref", ref);
        entity.ProjectCounties county = (entity.ProjectCounties) DataMartBeanUtil.getEntityByQueryAllowNull(q,
                "NEPA Project " + projectid + " has multiple county associations with id '" + id + "'");

        return county;
    }
}