/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.entity;

import client.ProjectMeetingNoticeDocsClient;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.ProjectMeetingNoticeDoc.ProjectMeetingNoticeDoc;
import us.fed.fs.www.nepa.schema.ProjectMeetingNoticeDoc.ProjectMeetingNoticeDocs;


/**
 *
 * @author gauri
 */

public class ProjectMeetingNoticeDocTest {
    private ProjectMeetingNoticeDocsClient client;
    

    @Before
    public void setUp()
            throws IOException
    {
        client = new ProjectMeetingNoticeDocsClient();
       
    }

    @Test
    public void testPost()
    {
        // This POST should fail, because it violates a constraint. The unit code is bad
        try {
           addProjectMeetingNoticeDocs();
            // If the above statement did not throw an exception then something has gone terribly wrong
            //Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.BAD_PARENTS)
                Assert.fail();
        } catch (Exception ex) {
            Logger.getLogger(ProjectMeetingNoticeDocTest.class.getName()).log(Level.SEVERE, null, ex);
             Assert.fail();
        }

        

        
    }

    @Test
    public void testGet()
    {
     

        ProjectMeetingNoticeDocs project = null;
         
        try {
            project = client.getProjectMeetingNoticeDocs("17735","1");
        } catch (IOException ex) {
            Logger.getLogger(ProjectMeetingNoticeDocTest.class.getName()).log(Level.SEVERE, null, ex);
             Assert.fail();
        }
            
          
       

       
     

       
    }

    @Test
    public void testPut()
    {
        

        ProjectMeetingNoticeDocs project = null;
        try {
            modifyProjectMeetingNoticeDocs();
            project = client.getProjectMeetingNoticeDocs("17735","1");
        } catch (Exception ex) {
            Logger.getLogger(ProjectMeetingNoticeDocTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

       
    }

   
    
 


    @Test
    public void testDelete()
    {
        

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            deleteProjectMeetingNoticeDocs();
          
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        } catch (Exception ex) {
            Logger.getLogger(ProjectMeetingNoticeDocTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
      
    }

 
    @After
    public void cleanFailed()
            throws Exception
    {
      
    }


    @BeforeClass
    public static void populateDependencies()
    {
        // No dependencies
    }

    @AfterClass
    public static void cleanupDependencies()
    {
        // No cleanup
    }
        public static void addProjectMeetingNoticeDocs() throws Exception{
	    	
	    	client.ProjectMeetingNoticeDocsClient client = new client.ProjectMeetingNoticeDocsClient();
	    	us.fed.fs.www.nepa.schema.ProjectMeetingNoticeDoc.ProjectMeetingNoticeDoc p = new us.fed.fs.www.nepa.schema.ProjectMeetingNoticeDoc.ProjectMeetingNoticeDoc();
//	    	p.setMeetingId("1");
//                p.setDocumentId("FSTST3_1385255");
//                p.setOrder(6);
                p.setMeetingId("43");
                p.setDocumentId("FSPLT1_012022");
                p.setOrder(1);
               	client.postProjectMeetingNoticeDocs("17735",p);
	    }
    
	 public static void deleteProjectMeetingNoticeDocs() throws Exception{
	    	
	    	client.ProjectMeetingNoticeDocsClient client = new client.ProjectMeetingNoticeDocsClient();
                client.deleteProjectMeetingNoticeDocs("17735","1","6");
         }   
         
          public static void modifyProjectMeetingNoticeDocs() throws Exception{
	    	client.ProjectMeetingNoticeDocsClient client = new client.ProjectMeetingNoticeDocsClient();
  	  	us.fed.fs.www.nepa.schema.ProjectMeetingNoticeDoc.ProjectMeetingNoticeDoc p = new us.fed.fs.www.nepa.schema.ProjectMeetingNoticeDoc.ProjectMeetingNoticeDoc();
	        p.setMeetingDocId("6");
                p.setMeetingId("1");
                p.setDocumentId("FSTST3_1385255");
                p.setOrder(5);
	    	client.putProjectMeetingNoticeDocs("17735",p);
         }  
}