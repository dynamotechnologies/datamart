
-- BEGIN: general.1
-- New fields for Objections table

ALTER TABLE Objections ADD COLUMN FilingStatus varchar(20);	-- Objection Filing Status (TBD,N/A,Open,Closed)
ALTER TABLE Objections ADD COLUMN StatusDetails varchar(255);	-- Objection Status Details (New,Closed,Under Review,Will Not Review)
ALTER TABLE Objections ADD COLUMN OutcomeDetails varchar(255);	-- Objection Outcome Details (Adequately addressed, further instructions provided)
ALTER TABLE Objections ADD COLUMN PeriodStartDate datetime;	-- Objection Period Start Date
ALTER TABLE Objections ADD COLUMN PeriodEndDate datetime;	-- Objection Period Start Date
ALTER TABLE Objections ADD COLUMN ResponseDueDate datetime;	-- Response Due Date
ALTER TABLE Objections ADD COLUMN ResponseDueDateExt datetime;	-- Response Due Date (Extended)
ALTER TABLE Objections ADD COLUMN OverallOutcome varchar(255);	-- Overall Outcome (Resolved during resolution meeting, Responsible official provided further instructions, Responsible official may sign decision)
ALTER TABLE Objections ADD COLUMN Title varchar(255);		-- Objection Title


Update Objections set FilingStatus = 'TBD';
Update Objections set StatusDetails = 'New';
Update Objections set OutcomeDetails = 'Further Instructions Provided';
Update Objections set PeriodStartDate = '2014-05-23 00:00:00';
Update Objections set PeriodEndDate = '2014-06-23 00:00:00';
Update Objections set ResponseDueDate = '2014-07-23 00:00:00';
Update Objections set ResponseDueDateExt = '2014-08-23 00:00:00';
Update Objections set OverallOutcome = 'Responsible official may sign decision';
Update Objections set Title = 'John Q. Objector';
-- END: general.1

-- BEGIN: prmro.1
ALTER TABLE UserSession ADD COLUMN ProjectAccessLevel VARCHAR(20) NOT NULL;
UPDATE UserSession SET ProjectAccessLevel = "write"; 
-- END: prmro.1
