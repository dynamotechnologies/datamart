package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "RefCategoricalExclusions")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RefCategoricalExclusions.findAll", query = "SELECT r FROM RefCategoricalExclusions r"),
    @NamedQuery(name = "RefCategoricalExclusions.findById", query = "SELECT r FROM RefCategoricalExclusions r WHERE r.id = :id"),
    @NamedQuery(name = "RefCategoricalExclusions.findByName", query = "SELECT r FROM RefCategoricalExclusions r WHERE r.name = :name"),
    @NamedQuery(name = "RefCategoricalExclusions.findByDescription", query = "SELECT r FROM RefCategoricalExclusions r WHERE r.description = :description")})
public class RefCategoricalExclusions implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "Name")
    private String name;
    @Column(name = "Description")
    private String description;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refCategoricalExclusions")
    private Collection<NepaCategoricalExclusions> nepaCategoricalExclusionsCollection;

    public RefCategoricalExclusions() {
    }

    public RefCategoricalExclusions(Integer id) {
        this.id = id;
    }

    public RefCategoricalExclusions(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public Collection<NepaCategoricalExclusions> getNepaCategoricalExclusionsCollection() {
        return nepaCategoricalExclusionsCollection;
    }

    public void setNepaCategoricalExclusionsCollection(Collection<NepaCategoricalExclusions> nepaCategoricalExclusionsCollection) {
        this.nepaCategoricalExclusionsCollection = nepaCategoricalExclusionsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RefCategoricalExclusions)) {
            return false;
        }
        RefCategoricalExclusions other = (RefCategoricalExclusions) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RefCategoricalExclusions[ id=" + id + " ]";
    }

}
