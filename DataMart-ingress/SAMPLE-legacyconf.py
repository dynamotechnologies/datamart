# See readme for instructions


# 
# PILOT
# 
# server_name='PILOT'
# work_dir='/app/DataMart-ingress-pilot'
# log_file=work_dir+'/legacyingress.log'
# lock_file=work_dir+'/legacyingress.lock'
# error_file=work_dir+'/legacyingress.error'
# index_file=work_dir+'/projectxml.index'
# java_home='/usr/lib/jvm/jdk1.6.0_24'
# # java_options='-Xmx1024m -XX:MaxPermSize=1024m'
# # Apparently 1024m is not enough for a 100mb ingress file!
# java_options='-Xmx2048m -XX:MaxPermSize=2048m'
# notifications_enabled=True
# notify_addresses='fs-drm-alerts@phaseonecg.com'
# test_mode=False
# 
# projectfiles_dir='/var/www/html/nepaftp/projects/'
# appeals_file='/var/www/html/nepaftp/appeals.xml'
# objections_file='/var/www/html/nepaftp/objections.xml'



# 
# TST
#
# server_name='TST'
# work_dir='/app/DataMart-ingress'
# log_file=work_dir+'/legacyingress.log'
# lock_file=work_dir+'/legacyingress.lock'
# error_file=work_dir+'/legacyingress.error'
# index_file=work_dir+'/projectxml.index'
# java_home='/usr/lib/jvm/jdk1.6.0_24'
# # Why does this need more memory than it does on pilot?
# java_options='-Xmx2048m -XX:MaxPermSize=2048m -XX:-UseGCOverheadLimit'
# notifications_enabled=True
# notify_addresses='fs-drm-alerts@phaseonecg.com'
# test_mode=True
# 
# projectfiles_dir='/var/www/html/nepaftp/projects/'
# appeals_file='/var/www/html/nepaftp/appeals.xml'
# objections_file='/var/www/html/nepaftp/objections.xml'



# 
# James' Workstation
# 
# server_name='James\' workstation'
# work_dir='/opt/DataMart-ingress'
# log_file=work_dir+'/legacyingress.log'
# lock_file=work_dir+'/legacyingress.lock'
# error_file=work_dir+'/legacyingress.error'
# index_file=work_dir+'/projectxml.index'
# java_home='/usr/lib/jvm/java-6-sun-1.6.0.24'
# java_options='-Xmx2048m -XX:MaxPermSize=2048m -XX:-UseGCOverheadLimit'
# notifications_enabled=False
# notify_addresses='james@makaisoftware.com'
# test_mode=True
# 
# projectfiles_dir='/opt/DataMart-ingress/files/'
# appeals_file='/opt/DataMart-ingress/files/appeals.xml'
# objections_file='/opt/DataMart-ingress/files/objections.xml'
