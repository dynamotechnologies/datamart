CREATE TABLE ContainerTemplates (
	Id integer NOT NULL AUTO_INCREMENT,
	UnitId varchar(255) NOT NULL,
	Name varchar(255) NOT NULL,
 	CONSTRAINT PkContainerTemplate_Id PRIMARY KEY (Id),
	CONSTRAINT FkContainerTemplate_UnitId FOREIGN KEY(UnitId) REFERENCES RefUnits(Id),
	CONSTRAINT UnContainerTemplate_UnitId_Name UNIQUE(UnitId(100), Name(200))
) ENGINE=INNODB;

CREATE TABLE ContainerTemplateItems (
	Id integer NOT NULL AUTO_INCREMENT,
	TemplateId integer NOT NULL,
	ContainerId integer NULL,
	Label varchar(255) NOT NULL,
	OrderTag integer NOT NULL,
	FixedFlag tinyint(1) NOT NULL,
 	CONSTRAINT PkContainerTemplateItems_Id PRIMARY KEY (Id),
	CONSTRAINT FkContainerTemplateItems_TemplateId FOREIGN KEY(TemplateId) REFERENCES ContainerTemplates(Id),
	CONSTRAINT FkContainerTemplateItems_ContainerId FOREIGN KEY(ContainerId) REFERENCES ContainerTemplateItems(Id) ON DELETE CASCADE,
	INDEX KeyTemplateId(TemplateId),
	INDEX KeyContainerId(ContainerId)
) ENGINE=INNODB;

ALTER TABLE ContainerTemplateItems ADD COLUMN (PalsContainerId integer DEFAULT NULL);
ALTER TABLE ContainerTemplateItems ADD UNIQUE UnProjectDocumentContainers_ProjectId_PalsContId(TemplateId, PalsContainerId);
ALTER TABLE ContainerTemplates ADD COLUMN(Created DATETIME NOT NULL);

--avoid some sorts when querying the list of templates
ALTER TABLE ContainerTemplates ADD Index KeyCreated(Created);
ALTER TABLE ContainerTemplates ADD Index KeyUnitId_Created(UnitId, Created);

--potentially useful query for updating test sites that have template created without the date
--not needed for the production upgrade (though is harmless)
--UPDATE ContainerTemplates SET Created = now() WHERE Created IS NULL;
