package ingress.detailprojects;

import ingress.LegacyUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.zip.DataFormatException;
import javax.xml.datatype.XMLGregorianCalendar;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 *
<!DOCTYPE unitprojectsdetail [
<!ELEMENT unitprojectsdetail (timestamp, unit*)>
<!ELEMENT timestamp (#PCDATA)>
<!ELEMENT unit (code,extendeddetails,project+)>
<!ELEMENT code (#PCDATA)>
<!ELEMENT extendeddetails (#PCDATA)>
<!ELEMENT project (id,projectdocumentid,expirationdate?,name,status,type,url?,appealslink,objectionslink,description,projectsummary,contact,catexclusion*,specauth+,milestone*,purpose,adminunit,activity+,commreg?,esd?,cenodecision?,location?,decision*,pubflag,updatedate,sopa,primaryProjectManager?,secondaryProjectManager?,dataEntryPerson?,goal*,resourcearea*)>
<!ELEMENT id (#PCDATA)>
<!ELEMENT projectdocumentid (#PCDATA)>
<!ELEMENT expirationdate (#PCDATA)>
<!ELEMENT name (#PCDATA)>
<!ELEMENT status (#PCDATA)>
<!ELEMENT type (#PCDATA)>
<!ELEMENT url (#PCDATA)>
<!ELEMENT appealslink (#PCDATA)>
<!ELEMENT objectionslink (#PCDATA)>
<!ELEMENT description (#PCDATA)>
<!ELEMENT projectsummary (#PCDATA)>
<!ELEMENT contact (contactShortName,contactname, contactphone, contactemail)>
<!ELEMENT contactShortName (#PCDATA)>
<!ELEMENT contactname (#PCDATA)>
<!ELEMENT contactphone (#PCDATA)>
<!ELEMENT contactemail (#PCDATA)>
<!ELEMENT catexclusion (#PCDATA)>
<!ELEMENT specauth (#PCDATA)>
<!ELEMENT milestone (msseq,mstype,msdate,msstatus,mshistoryflag)>
<!ELEMENT msseq (#PCDATA)>
<!ELEMENT mstype (#PCDATA)>
<!ELEMENT msdate (#PCDATA)>
<!ELEMENT msstatus (#PCDATA)>
<!ELEMENT mshistoryflag (#PCDATA)>
<!ELEMENT purpose (purposename+)>
<!ELEMENT purposename (#PCDATA)>
<!ELEMENT adminunit (#PCDATA)>
<!ELEMENT activity (#PCDATA)>
<!ELEMENT commreg (#PCDATA)>
<!ELEMENT location (locdesc,locfor+,locdst+,locstate+,loccount+,loclegal?,loclat?,loclong?)>
<!ELEMENT locdesc (#PCDATA)>
<!ELEMENT locfor (#PCDATA)>
<!ELEMENT locdst (#PCDATA)>
<!ELEMENT locstate (#PCDATA)>
<!ELEMENT loccount (#PCDATA)>
<!ELEMENT loclegal (#PCDATA)>
<!ELEMENT loclat (#PCDATA)>
<!ELEMENT loclong (#PCDATA)>
<!ELEMENT decision (decname,decisionId, dectype,decdate,decnotice?,decconstraint,decapprule,decmaker+,decappstat?,decapp*,declit*,decareasize?,decareaunits?)>
<!ELEMENT decname (#PCDATA)>
<!ELEMENT decisionId (#PCDATA)>
<!ELEMENT dectype (#PCDATA)>
<!ELEMENT decdate (#PCDATA)>
<!ELEMENT decnotice (#PCDATA)>
<!ELEMENT decconstraint (#PCDATA)>
<!ELEMENT decapprule (#PCDATA)>
<!ELEMENT decmaker (decmakerId,decmakername,decmakertitle,decmakesigneddate)>
<!ELEMENT decmakerId (#PCDATA)>
<!ELEMENT decmakername (#PCDATA)>
<!ELEMENT decmakertitle (#PCDATA)>
<!ELEMENT decmakesigneddate (#PCDATA)>
<!ELEMENT decappstat (#PCDATA)>
<!ELEMENT decapp (appealid,appealadminunitcode,rule,appellant?,outcome?,response?)>
<!ELEMENT appealid (#PCDATA)>
<!ELEMENT appealadminunitcode (#PCDATA)>
<!ELEMENT rule (#PCDATA)>
<!ELEMENT appellant (#PCDATA)>
<!ELEMENT outcome (#PCDATA)>
<!ELEMENT response (date,documentid?,appealresponsefilesize?)>
<!ELEMENT date (#PCDATA)>
<!ELEMENT documentid (#PCDATA)>
<!ELEMENT appealresponsefilesize (#PCDATA)>
<!ELEMENT declit (litid,litname,litstat,litoutcome?,litclosed?)>
<!ELEMENT litid (#PCDATA)>
<!ELEMENT litname (#PCDATA)>
<!ELEMENT litstat (#PCDATA)>
<!ELEMENT litoutcome (#PCDATA)>
<!ELEMENT litclosed (#PCDATA)>
<!ELEMENT pubflag (#PCDATA)>
<!ELEMENT updatedate (#PCDATA)>
<!ELEMENT sopa (sopaFlag,isNewProjectFlag?,sopaHeaderUnit?)>
<!ELEMENT sopaFlag (#PCDATA)>
<!ELEMENT isNewProjectFlag (#PCDATA)>
<!ELEMENT sopaHeaderUnit (#PCDATA)>
<!ELEMENT primaryProjectManager (#PCDATA)>
<!ELEMENT secondaryProjectManager (#PCDATA)>
<!ELEMENT dataEntryPerson (#PCDATA)>
<!ELEMENT goal (goalid,goalorder?)>
<!ELEMENT goalid (#PCDATA)>
<!ELEMENT goalorder (#PCDATA)>
<!ELEMENT resourcearea (resourceareaid,resourceareaorder?)>
<!ELEMENT resourceareaid (#PCDATA)>
<!ELEMENT resourceareaorder (#PCDATA)>
<!ELEMENT decareasize (#PCDATA)>
<!ELEMENT decareaunits (#PCDATA)>
<!ATTLIST unitprojects xmlns CDATA 'http://www.fs.fed.us/pals/xmlns/unitprojectsdetail/'>
]>
 *
 * This class is responsible for encapsulating element `project` as specified above
 */
public class LegacyProject
{
    private String id = "";
    private Integer projectdocumentid;
    private XMLGregorianCalendar expirationdate;
    private String name = "";
    private String status = "";
    private String analysistype = "";
    private String url = ""; // optional
    private String appealslink = "";
    private String objectionslink = "";
    private String description = "";
    private String projectsummary = "";
    private LegacyContact contact = new LegacyContact();
    private ArrayList<LegacySection> sections = new ArrayList<LegacySection>(); // zero or many
    private ArrayList<Integer> catexclusions = new ArrayList<Integer>(); // zero or many
    private ArrayList<String> specauths = new ArrayList<String>(); // one or many
    private ArrayList<LegacyMilestone> milestones = new ArrayList<LegacyMilestone>(); // zero or many
    private ArrayList<String> purposes = new ArrayList<String>(); // one purpose; one or many purposenames
    private String adminunit = "";
    private ArrayList<String> activities = new ArrayList<String>(); // one or many
    private String commentregulation = ""; // optional
    private String esd = ""; // optional
    private String cenodecision = ""; // optional
    private LegacyLocation location = new LegacyLocation(); // optional
    private ArrayList<LegacyDecision> decisions = new ArrayList<LegacyDecision>(); // zero or many
    private String pubflag = "";

    public void setAddinfoflag(String addinfoflag) {
        this.addinfoflag = addinfoflag;
    }

    public void setImplinfoflag(String implinfoflag) {
        this.implinfoflag = implinfoflag;
    }

    public void setMeetingflag(String meetingflag) {
        this.meetingflag = meetingflag;
    }
    private XMLGregorianCalendar updatedate;
    private LegacySopa sopa = new LegacySopa();
    String primaryProjectManager = ""; // optional
    String secondaryProjectManager = ""; // optional
    String dataEntryPerson = ""; // optional

    public String getAddinfoflag() {
        return addinfoflag;
    }

    public String getImplinfoflag() {
        return implinfoflag;
    }

    public String getMeetingflag() {
        return meetingflag;
    }
    private ArrayList<LegacyGoal> goals = new ArrayList<LegacyGoal>(); // zero or many
    private ArrayList<LegacyResourceArea> resourceareas = new ArrayList<LegacyResourceArea>(); // zero or many
    private String addinfoflag = "";
    private String implinfoflag = "";
    private String meetingflag = "";
    
    public LegacyProject()
    {
        // allow instantiation without nodes
    }

    public LegacyProject(Node n)
            throws DataFormatException
    {
        NodeList subnodes = n.getChildNodes();
        
        for (int i=0; i<subnodes.getLength(); i++)
        {
            if (subnodes.item(i).getNodeName().equals("id"))
                id = subnodes.item(i).getTextContent();
            if (subnodes.item(i).getNodeName().equals("projectdocumentid"))
                projectdocumentid = Integer.parseInt(subnodes.item(i).getTextContent());
            if (subnodes.item(i).getNodeName().equals("expirationdate"))
                setExpirationDate(subnodes.item(i).getTextContent());
            if (subnodes.item(i).getNodeName().equals("name"))
                name = subnodes.item(i).getTextContent();
            if (subnodes.item(i).getNodeName().equals("status"))
                status = subnodes.item(i).getTextContent();
            if (subnodes.item(i).getNodeName().equals("type"))
                analysistype = LegacyUtil.getAnalysisTypeIdFromPalsId(Integer.parseInt(subnodes.item(i).getTextContent()));
            if (subnodes.item(i).getNodeName().equals("url"))
                url = subnodes.item(i).getTextContent();
            if (subnodes.item(i).getNodeName().equals("appealslink"))
                appealslink = subnodes.item(i).getTextContent();
            if (subnodes.item(i).getNodeName().equals("objectionslink"))
                objectionslink = subnodes.item(i).getTextContent();
            if (subnodes.item(i).getNodeName().equals("description"))
                description = subnodes.item(i).getTextContent();
            if (subnodes.item(i).getNodeName().equals("projectsummary"))
                projectsummary = subnodes.item(i).getTextContent();
            if (subnodes.item(i).getNodeName().equals("contact"))
                setContact(subnodes.item(i));
            if (subnodes.item(i).getNodeName().equals("section"))
                addSection(subnodes.item(i));
            if (subnodes.item(i).getNodeName().equals("catexclusion"))
                addCatExclusion(subnodes.item(i));
            if (subnodes.item(i).getNodeName().equals("specauth"))
                addSpecAuth(subnodes.item(i));
            if (subnodes.item(i).getNodeName().equals("milestone"))
                addMilestone(subnodes.item(i));
            if (subnodes.item(i).getNodeName().equals("purpose"))
                addPurposes(subnodes.item(i));
            if (subnodes.item(i).getNodeName().equals("adminunit"))
                adminunit = subnodes.item(i).getTextContent();
            if (subnodes.item(i).getNodeName().equals("activity"))
                addActivity(subnodes.item(i));
            if (subnodes.item(i).getNodeName().equals("commreg"))
                commentregulation = subnodes.item(i).getTextContent();
            if (subnodes.item(i).getNodeName().equals("esd"))
                esd = subnodes.item(i).getTextContent();
            if (subnodes.item(i).getNodeName().equals("cenodecision"))
                cenodecision = subnodes.item(i).getTextContent();
            if (subnodes.item(i).getNodeName().equals("location"))
                setLocation(subnodes.item(i));
            if (subnodes.item(i).getNodeName().equals("decision"))
                addDecision(subnodes.item(i));                          // DECISION UPDATED
            if (subnodes.item(i).getNodeName().equals("pubflag"))
                pubflag = subnodes.item(i).getTextContent();
            if (subnodes.item(i).getNodeName().equals("addinfoflag"))
            	addinfoflag = subnodes.item(i).getTextContent();
            if (subnodes.item(i).getNodeName().equals("implinfoflag"))
            	implinfoflag = subnodes.item(i).getTextContent();
            if (subnodes.item(i).getNodeName().equals("meetingflag"))
            	meetingflag = subnodes.item(i).getTextContent();
            if (subnodes.item(i).getNodeName().equals("updatedate"))
                setLastUpdateDate(subnodes.item(i).getTextContent());
            if (subnodes.item(i).getNodeName().equals("sopa"))
                setSopa(subnodes.item(i));
            if (subnodes.item(i).getNodeName().equals("primaryProjectManager"))
                primaryProjectManager = subnodes.item(i).getTextContent();
            if (subnodes.item(i).getNodeName().equals("secondaryProjectManager"))
                secondaryProjectManager = subnodes.item(i).getTextContent();
            if (subnodes.item(i).getNodeName().equals("dataEntryPerson"))
                dataEntryPerson = subnodes.item(i).getTextContent();
            if (subnodes.item(i).getNodeName().equals("goal"))
                addGoal(subnodes.item(i));
            if (subnodes.item(i).getNodeName().equals("resourcearea"))
                addResourceArea(subnodes.item(i));


        }
        if (!(id.length()>0))
            throw new DataFormatException("id not found.");
        if (projectdocumentid==null)
            throw new DataFormatException("projectdocumentid not found.");
        if (!(name.length()>0))
            throw new DataFormatException("name not found.");
        if (!(status.length()>0))
            throw new DataFormatException("status not found.");
        if (!(analysistype.length()>0))
            throw new DataFormatException("type not found.");
        if (!(appealslink.length()>0))
            throw new DataFormatException("appealslink not found.");
        if (!(objectionslink.length()>0))
            throw new DataFormatException("objectionslink not found.");
        if (!(description.length()>0))
            throw new DataFormatException("description not found.");
        if (!(projectsummary.length()>0))
            throw new DataFormatException("projectsummary not found.");
        if (contact==null)
            throw new DataFormatException("contact not found.");
        if (!(specauths.size()>0))
            throw new DataFormatException("specauth not found.");
        if (!(purposes.size()>0))
            throw new DataFormatException("purposes not found.");
        if (!(adminunit.length()>0))
            throw new DataFormatException("adminunit not found.");
        if (!(activities.size()>0))
            throw new DataFormatException("activities not found.");
        if (!(pubflag.length()>0))
            throw new DataFormatException("pubflag not found.");
        if (updatedate==null || !(updatedate.toString().length()>0))
            throw new DataFormatException("updatedate not found.");
        if (sopa==null)
            throw new DataFormatException("sopa not found.");
    }

    private void setExpirationDate(String d)
            throws DataFormatException
    {
        Date date = LegacyUtil.getDateFromString(d, "Expiration Date");
        expirationdate = LegacyUtil.getXMLGregorianFromDateTime(date);
    }

    private void setLastUpdateDate(String d)
            throws DataFormatException
    {
        Date date = LegacyUtil.getDateFromString(d, "Last Update Date");
        updatedate = LegacyUtil.getXMLGregorianFromDateTime(date);
    }

    private void addSection(Node n)
            throws DataFormatException
    {
        sections.add(new LegacySection(n));
    }

    private void addCatExclusion(Node n)
            throws DataFormatException
    {
        Integer ce = Integer.parseInt(n.getTextContent());
        catexclusions.add(ce);
    }

    private void addSpecAuth(Node n)
            throws DataFormatException
    {
        specauths.add(n.getTextContent());
    }

    private void addMilestone(Node n)
            throws DataFormatException
    {
        milestones.add(new LegacyMilestone(n));
    }

    private void addActivity(Node n)
            throws DataFormatException
    {
        String a = LegacyUtil.getActivityIdFromPalsId(
                Integer.parseInt( n.getTextContent() ));
        activities.add(a);
    }

    private void addDecision(Node n)
            throws DataFormatException
    {
        decisions.add(new LegacyDecision(n));
    }

    private void addGoal(Node n)
            throws DataFormatException
    {
        goals.add(new LegacyGoal(n));
    }

    private void addResourceArea(Node n)
            throws DataFormatException
    {
        resourceareas.add(new LegacyResourceArea(n));
    }

    private void setContact(Node n)
            throws DataFormatException
    {
        contact = new LegacyContact(n);
    }

    private void setSopa(Node n)
            throws DataFormatException
    {
        sopa = new LegacySopa(n);
    }

    private void addPurposes(Node n)
            throws DataFormatException
    {
        NodeList subnodes = n.getChildNodes();

        for (int i=0; i<subnodes.getLength(); i++)
        {
            if (subnodes.item(i).getNodeName().equals("purposename")) {
                String p = LegacyUtil.getPurposeIdFromPalsId(
                        Integer.parseInt( subnodes.item(i).getTextContent() ));
                purposes.add(p);
            }
        }
    }
    
    private void setLocation(Node n)
            throws DataFormatException
    {
        location = new LegacyLocation(n);
    }

    public String getId()
    {
        return id;
    }

    public Integer getProjectDocumentId()
    {
        return projectdocumentid;
    }

    public XMLGregorianCalendar getExpirationDate()
    {
        return expirationdate;
    }

    public String getName()
    {
        return name;
    }

    public String getUrl()
    {
        return url;
    }

    public String getStatus()
    {
        return status;
    }

    public String getAnalysisType()
    {
        return analysistype;
    }

    public String getAppealsLink()
    {
        return appealslink;
    }

    public String getObjectionsLink()
    {
        return objectionslink;
    }

    public String getDescription()
    {
        return description;
    }

    public String getProjectSummary()
    {
        return projectsummary;
    }

    public LegacyContact getContact()
    {
        return contact;
    }

    public String getPrimaryProjectManager()
    {
        return this.primaryProjectManager;
    }

    public String getSecondaryProjectManager()
    {
        return this.secondaryProjectManager;
    }

    public String getDataEntryPerson()
    {
        return this.dataEntryPerson;
    }
    
    public ArrayList<LegacySection> getSections()
    {
        return sections;
    }

    public ArrayList<Integer> getCatExclusions()
    {
        return catexclusions;
    }

    public ArrayList<String> getSpecAuths()
    {
        return specauths;
    }

    public ArrayList<LegacyMilestone> getMilestones()
    {
        return milestones;
    }

    public ArrayList<String> getPurposes()
    {
        return purposes;
    }

    public String getAdminUnit()
    {
        return adminunit;
    }

    public ArrayList<String> getActivities()
    {
        return activities;
    }

    public String getCommentRegulation()
    {
        return commentregulation;
    }

    public String getEsd()
    {
        return esd;
    }
    
    public String getCenodecision()
    {
        return cenodecision;
    }
    
    public LegacyLocation getLocation()
    {
        return location;
    }

    public ArrayList<LegacyDecision> getDecisions()
    {
        return decisions;
    }

    public ArrayList<LegacyGoal> getGoals()
    {
        return goals;
    }

    public ArrayList<LegacyResourceArea> getResourceAreas()
    {
        return resourceareas;
    }

    public String getPubFlag()
    {
        return pubflag;
    }

    public XMLGregorianCalendar getUpdateDate()
    {
        return updatedate;
    }

    public LegacySopa getSopa()
    {
        return sopa;
    }
}