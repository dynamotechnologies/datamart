package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.caraproject.*;


public class CARAProjectClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public CARAProjectClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.caraproject", "CaraProject.xsd");
    }


    public void linkProject(String palsid, int caraid)
            throws IOException
    {
        Caralinkproject linkrequest = new Caralinkproject();
        linkrequest.setPalsid(palsid);
        linkrequest.setCaraid(caraid);
        post(objectfactory.createCaralinkproject(linkrequest), "/utils/cara/linkProject");
    }


    public void unlinkProject(int caraid)
            throws IOException
    {
        Caraunlinkproject unlinkrequest = new Caraunlinkproject();
        unlinkrequest.setCaraid(caraid);
        post(objectfactory.createCaraunlinkproject(unlinkrequest), "/utils/cara/unlinkProject");
    }

    public Caraproject getProject(int caraid)
            throws IOException
    {
        return get("/caraprojects/" + Integer.toString(caraid), Caraproject.class);
    }

    public void putProject(Caraproject caraproj)
            throws IOException
    {
        put(objectfactory.createCaraproject(caraproj),
                "/caraprojects/" + Integer.toString(caraproj.getCaraid()));

    }

    // Note these use PALS ID
    public void activateRR(String palsid)
            throws IOException
    {
        Caraproject caraproj = new Caraproject();   // throwaway object
        caraproj.setCaraid(0);
        caraproj.setPalsid("");
        caraproj.setReadingroomactive(false);
        post(objectfactory.createCaraproject(caraproj), "/utils/cara/projectbypalsid/" + palsid + "/readingroom/setactive");
    }

    // Note these use PALS ID
    public void deactivateRR(String palsid)
            throws IOException
    {
        Caraproject caraproj = new Caraproject();   // throwaway object
        caraproj.setCaraid(0);
        caraproj.setPalsid("");
        caraproj.setReadingroomactive(false);
        post(objectfactory.createCaraproject(caraproj), "/utils/cara/projectbypalsid/" + palsid + "/readingroom/setinactive");
    }
}