package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.decisiontype.*;

import beanutil.*;
import java.util.Iterator;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless(mappedName="DecisionTypeManagerRemote")
public class DecisionTypeManager implements DecisionTypeManagerRemote
{

    @PersistenceContext
    private EntityManager em;


    @Override
    public void create(Decisiontype resource)
             throws DataMartException
    {
        validate(resource);
        entity.RefDecisionTypes entity = createEntity(resource);

        entity.RefDecisionTypes badentity = createEntity(resource.getId());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        /* Note: JDBC will barf profusely if palsid already exists in table */
        entity = em.merge(entity);
    }

    @Override
    public Decisiontypes getAll()
             throws DataMartException
    {
        // Retrieve all entities from the database
        Query query = em.createQuery("SELECT u FROM RefDecisionTypes u");
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Decisiontypes responselist = new Decisiontypes();
        while (it.hasNext()) {
            responselist.getDecisiontype().add(createResource( (entity.RefDecisionTypes)it.next() ));
        }
        return responselist;
    }
    
    @Override
    public Decisiontype get(String id)
            throws DataMartException
    {
        entity.RefDecisionTypes entity = createEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return createResource(entity);
    }

    @Override
    public String getDescription(String id)
            throws DataMartException
    {
        entity.RefDecisionTypes entity = createEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return entity.getDescription();
    }

    @Override
    public void update(Decisiontype resource)
             throws DataMartException
    {
        validate(resource);
        entity.RefDecisionTypes entity = createEntity(resource);

        if (createEntity(resource.getId())==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        
        /* Note: JDBC will barf profusely if palsid already exists in table */
        entity = em.merge(entity);
    }

    @Override
    public void delete(String id)
             throws DataMartException
    {
        entity.RefDecisionTypes entity = createEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     *
     * This method does the mapping from jaxb object to entity object and vice versa
     */
    private Decisiontype createResource(entity.RefDecisionTypes entity)
            throws DataMartException
    {
        Decisiontype resource = new Decisiontype();
        resource.setId(entity.getId());
        resource.setDescription(entity.getDescription());
        resource.setPalsid(entity.getPalsId());
        return resource;
    }

    private entity.RefDecisionTypes createEntity(Decisiontype resource)
            throws DataMartException
    {
        entity.RefDecisionTypes entity = new entity.RefDecisionTypes();
        entity.setId(resource.getId());
        entity.setDescription(resource.getDescription());
        entity.setPalsId(resource.getPalsid());
        return entity;
    }

    // This version of overloaded method createEntity() accepts a String ID as a parameter, and returns the
    //   associated entity, or null if it doesn't exist
    private entity.RefDecisionTypes createEntity(String id)
            throws DataMartException
    {
        return em.find(entity.RefDecisionTypes.class, id);
    }

    private void validate(Decisiontype resource)
            throws DataMartException
    {
        DataMartBeanUtil.confirmNotNull(resource.getDescription(), "Description");
    }
}
