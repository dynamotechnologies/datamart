package ingress.detailprojects;

import java.util.ArrayList;
import java.util.zip.DataFormatException;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 *
 * <!DOCTYPE unitprojectsdetail [
 * <!ELEMENT unitprojectsdetail (timestamp, unit*)>
 * <!ELEMENT timestamp (#PCDATA)>
 * <!ELEMENT unit (code,extendeddetails,project+)>
 * <!ELEMENT code (#PCDATA)>
 * <!ELEMENT extendeddetails (#PCDATA)>
 * <!ELEMENT project (id,projectdocumentid,expirationdate?,name,status,type,url?,appealslink,objectionslink,description,projectsummary,contact,catexclusion*,specauth+,milestone*,purpose,adminunit,activity+,commreg?,esd?,cenodecision?,location?,decision*,pubflag,updatedate,sopa,primaryProjectManager?,secondaryProjectManager?,dataEntryPerson?,goal*,resourcearea*)>
 * <!ELEMENT id (#PCDATA)>
 * <!ELEMENT projectdocumentid (#PCDATA)>
 * <!ELEMENT expirationdate (#PCDATA)>
 * <!ELEMENT name (#PCDATA)>
 * <!ELEMENT status (#PCDATA)>
 * <!ELEMENT type (#PCDATA)>
 * <!ELEMENT url (#PCDATA)>
 * <!ELEMENT appealslink (#PCDATA)>
 * <!ELEMENT objectionslink (#PCDATA)>
 * <!ELEMENT description (#PCDATA)>
 * <!ELEMENT projectsummary (#PCDATA)>
 * <!ELEMENT contact (contactShortName,contactname, contactphone, contactemail)>
 * <!ELEMENT contactShortName (#PCDATA)>
 * <!ELEMENT contactname (#PCDATA)>
 * <!ELEMENT contactphone (#PCDATA)>
 * <!ELEMENT contactemail (#PCDATA)>
 * <!ELEMENT catexclusion (#PCDATA)>
 * <!ELEMENT specauth (#PCDATA)>
 * <!ELEMENT milestone (msseq,mstype,msdate,msstatus,mshistoryflag)>
 * <!ELEMENT msseq (#PCDATA)>
 * <!ELEMENT mstype (#PCDATA)>
 * <!ELEMENT msdate (#PCDATA)>
 * <!ELEMENT msstatus (#PCDATA)>
 * <!ELEMENT mshistoryflag (#PCDATA)>
 * <!ELEMENT purpose (purposename+)>
 * <!ELEMENT purposename (#PCDATA)>
 * <!ELEMENT adminunit (#PCDATA)>
 * <!ELEMENT activity (#PCDATA)>
 * <!ELEMENT commreg (#PCDATA)>
 * <!ELEMENT location (locdesc,locfor+,locdst+,locstate+,loccount+,loclegal?,loclat?,loclong?)>
 * <!ELEMENT locdesc (#PCDATA)>
 * <!ELEMENT locfor (#PCDATA)>
 * <!ELEMENT locdst (#PCDATA)>
 * <!ELEMENT locstate (#PCDATA)>
 * <!ELEMENT loccount (#PCDATA)>
 * <!ELEMENT loclegal (#PCDATA)>
 * <!ELEMENT loclat (#PCDATA)>
 * <!ELEMENT loclong (#PCDATA)>
 * <!ELEMENT decision (decname,decisionId,decnotice?,decconstraint,decapprule,decappstat?,decapp*,declit*,decareasize?,decareaunits?)>
 * <!ELEMENT decname (#PCDATA)>
 * <!ELEMENT decisionId (#PCDATA)>
 * <!ELEMENT decnotice (#PCDATA)>
 * <!ELEMENT decconstraint (#PCDATA)>
 * <!ELEMENT decapprule (#PCDATA)>
 * <!ELEMENT decappstat (#PCDATA)>
 * <!ELEMENT decapp (appealid,appealadminunitcode,rule,appellant?,outcome?,response?)>
 * <!ELEMENT appealid (#PCDATA)>
 * <!ELEMENT appealadminunitcode (#PCDATA)>
 * <!ELEMENT rule (#PCDATA)>
 * <!ELEMENT appellant (#PCDATA)>
 * <!ELEMENT outcome (#PCDATA)>
 * <!ELEMENT response (date,documentid?,appealresponsefilesize?)>
 * <!ELEMENT date (#PCDATA)>
 * <!ELEMENT documentid (#PCDATA)>
 * <!ELEMENT appealresponsefilesize (#PCDATA)>
 * <!ELEMENT declit (litid,litname,litstat,litoutcome?,litclosed?)>
 * <!ELEMENT litid (#PCDATA)>
 * <!ELEMENT litname (#PCDATA)>
 * <!ELEMENT litstat (#PCDATA)>
 * <!ELEMENT litoutcome (#PCDATA)>
 * <!ELEMENT litclosed (#PCDATA)>
 * <!ELEMENT pubflag (#PCDATA)>
 * <!ELEMENT updatedate (#PCDATA)>
 * <!ELEMENT sopa (sopaFlag,isNewProjectFlag?,sopaHeaderUnit?)>
 * <!ELEMENT sopaFlag (#PCDATA)>
 * <!ELEMENT isNewProjectFlag (#PCDATA)>
 * <!ELEMENT sopaHeaderUnit (#PCDATA)>
 * <!ELEMENT primaryProjectManager (#PCDATA)>
 * <!ELEMENT secondaryProjectManager (#PCDATA)>
 * <!ELEMENT dataEntryPerson (#PCDATA)>
 * <!ELEMENT goal (goalid,goalorder?)>
 * <!ELEMENT goalid (#PCDATA)>
 * <!ELEMENT goalorder (#PCDATA)>
 * <!ELEMENT resourcearea (resourceareaid,resourceareaorder?)>
 * <!ELEMENT resourceareaid (#PCDATA)>
 * <!ELEMENT resourceareaorder (#PCDATA)>
 * <!ELEMENT decareasize (#PCDATA)>
 * <!ELEMENT decareaunits (#PCDATA)>
 * <!ATTLIST unitprojects xmlns CDATA 'http://www.fs.fed.us/pals/xmlns/unitprojectsdetail/'>
 * ]>
 *
 * This class is responsible for encapsulating element `section` as specified above
 */
public class LegacySection {
    private Integer sectionid; // Required, but shown as optional in DTD
    private String sectionname = "";
    private Integer order;
    private String sectiontext = ""; // Optional
    private ArrayList<LegacyDocument> documents = new ArrayList<LegacyDocument>(); // zero, one or many
    ArrayList<LegacySection> sections = new ArrayList<LegacySection>(); // zero, one or many

    public LegacySection()
    {
        // allow instantiation without nodes
    }

    public LegacySection(Node n)
            throws DataFormatException
    {
        NodeList mynodes = n.getChildNodes();
        for (int i=0; i<mynodes.getLength(); i++)
        {
            if (mynodes.item(i).getNodeName().equals("sectionid"))
                sectionid = Integer.parseInt(mynodes.item(i).getTextContent());
            if (mynodes.item(i).getNodeName().equals("sectionname"))
                sectionname = mynodes.item(i).getTextContent();
            if (mynodes.item(i).getNodeName().equals("order"))
                order = Integer.parseInt(mynodes.item(i).getTextContent());
            if (mynodes.item(i).getNodeName().equals("sectiontext"))
                sectiontext = mynodes.item(i).getTextContent();
            if (mynodes.item(i).getNodeName().equals("document"))
                addDocument(mynodes.item(i));
            if (mynodes.item(i).getNodeName().equals("section"))
                addSection(mynodes.item(i));
        }
        /*
         * TODO: sectionid should be required
         */
        if (!(sectionname.length()>0))
            throw new DataFormatException("sectionname not found.");
        if (order==null)
            throw new DataFormatException("order not found.");
    }

    private void addDocument(Node n)
            throws DataFormatException
    {
        documents.add(new LegacyDocument(n));
    }

    private void addSection(Node n)
            throws DataFormatException
    {
        sections.add(new LegacySection(n));
    }

    public Integer getId()
    {
        return sectionid;
    }

    public String getName()
    {
        return sectionname;
    }

    public Integer getOrder()
    {
        return order;
    }

    public String getText()
    {
        return sectiontext;
    }

    public ArrayList<LegacySection> getSections()
    {
        return sections;
    }

    public ArrayList<LegacyDocument> getDocuments()
    {
        return documents;
    }
}