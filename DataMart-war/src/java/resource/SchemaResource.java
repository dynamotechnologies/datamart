package resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.validation.Schema;
import schema.DataMartSchemaGetter;
import javax.ws.rs.core.Response.ResponseBuilder;

/*
 *
 * This Resource exposes the schemas, so integrating clients can use them for validation.
 */
@Path ("/schema")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
public class SchemaResource {

    @GET
    @Path("/{name}")
    public Response get(@PathParam("name") String name, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            Schema schema = DataMartSchemaGetter.getSchema(name);
            String s = DataMartSchemaGetter.getSchemaAsString(name);
            return Response.ok(s).build();
        } catch (Exception ex) {
            //throw new WebApplicationException(Response.Status.NOT_FOUND);

            ResponseBuilder builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
            builder.type("text/html");
            builder.entity(ex.getMessage());
            throw new WebApplicationException(ex, builder.build());
        }
    }
}