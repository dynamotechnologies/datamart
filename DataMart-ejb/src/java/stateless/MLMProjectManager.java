package stateless;
import us.fed.fs.www.nepa.schema.mlmproject.*;
import beanutil.*;
import javax.ejb.Stateless;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.jboss.logging.Logger;

@Stateless(mappedName="MLMProjectManagerRemote")
public class MLMProjectManager implements MLMProjectManagerRemote {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void create(Mlmproject resource)
             throws DataMartException
    {
        String mlmid = resource.getMlmid();
        String projid = resource.getProjectid();
        String projtype = resource.getProjecttype();
        String logmsg = "Linking MLM project (" + mlmid + ", " + projid + ")";
        Logger logger = Logger.getLogger(this.getClass());
        logger.info(logmsg);

        entity.MLMProjects badentity = getEntity(mlmid);
        if (badentity != null)
        {
            throw new DataMartException(DataMartException.ALREADY_EXISTS, "MLM project ID " + mlmid + " already in use, cannot be relinked");
        }
        entity.MLMProjects badentity2 = getEntityByProject(resource.getProjecttype(), resource.getProjectid());
        if (badentity2 != null)
        {
            throw new DataMartException(DataMartException.ALREADY_EXISTS, "Project type/ID " + projtype + "/" + projid + " already linked to MLM ID " + badentity2.getId());
        }

        entity.MLMProjects entity = createEntity(mlmid, projtype, projid);
        entity = em.merge(entity);
    }


    @Override
    public Mlmproject get(String mlmid)
            throws DataMartException
    {
        System.out.println("Saravana@NSGi : MLMProjectManager.get()" + mlmid);
        entity.MLMProjects entity = getEntity(mlmid);
        if (entity == null) {
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        } else {
            return createResource(entity);
        }
    }


    @Override
    public Mlmprojects getAll()
            throws DataMartException
    {
        // Retrieve all entities from the database
        Query query = em.createQuery("SELECT p FROM MLMProjects p");
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Mlmprojects responselist = new Mlmprojects();
        while (it.hasNext()) {
            responselist.getMlmproject().add(createResource((entity.MLMProjects)it.next()));
        }
        return responselist;
    }


    @Override
    public Mlmprojects getAllByUnit(String unitid)
            throws DataMartException
    {
        if (! unitid.matches("\\d+")) {
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION, "Bad unit ID format");
        }
        // Retrieve all entities from the database
        Query query = em.createQuery("SELECT m FROM MLMProjects m JOIN m.projects p JOIN p.refUnits u WHERE u.id = :unitid");
        query.setParameter("unitid", unitid);
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Mlmprojects responselist = new Mlmprojects();
        while (it.hasNext()) {
            responselist.getMlmproject().add(createResource((entity.MLMProjects)it.next()));
        }
        return responselist;
    }


    @Override
    public Mlmprojects getAllByForestmatch(String forestid)
            throws DataMartException
    {
        if (! forestid.matches("\\d{6}")) {
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION, "Bad forest ID format");
        }
        // Retrieve all entities from the database
        Query query = em.createQuery("SELECT m FROM MLMProjects m JOIN m.projects p JOIN p.refUnits u WHERE u.id LIKE :forestid");

        query.setParameter("forestid", forestid + "%");
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Mlmprojects responselist = new Mlmprojects();
        while (it.hasNext()) {
            responselist.getMlmproject().add(createResource((entity.MLMProjects)it.next()));
        }
        return responselist;
    }


    @Override
    public void delete(String mlmid)
             throws DataMartException
    {
        String logmsg = "Unlinking MLM project (" + mlmid + ")";
        Logger logger = Logger.getLogger(this.getClass());
        logger.info(logmsg);

        entity.MLMProjects entity = getEntity(mlmid);
        if (entity == null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(entity);
    }


    @Override
    public Boolean ping()
    {
        return true;
    }

    private entity.MLMProjects createEntity(String mlmId, String projectType, String projectId)
            throws DataMartException
    {
        entity.MLMProjects entity = new entity.MLMProjects();
        entity.setId(mlmId);
        entity.Projects project = DataMartBeanUtil.getProject(em, projectType, projectId);
        if (project == null) {
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION, "Project not found"
                    + " for type=" + projectType + ", id=" +projectId);
        }
        entity.setProjects(project);
        return entity;
    }

    /*
     * Find MLMProject record by MLM ID
     * Returns null if not found
     */
    private entity.MLMProjects getEntity(String mlmid)
            throws DataMartException
    {
        return em.find(entity.MLMProjects.class, mlmid);
    }

    private Mlmproject createResource(entity.MLMProjects entity)
    {
        Mlmproject resource = new Mlmproject();
        resource.setMlmid(entity.getId());
        resource.setProjecttype(entity.getProjects().getProjectTypes().getId());
        resource.setProjectid(entity.getProjects().getPublicId());
        return resource;
    }

    private entity.MLMProjects getEntityByProject(String projType, String projId)
            throws DataMartException
    {
        entity.Projects p = DataMartBeanUtil.getProject(em, projType, projId);
        Query query = em.createQuery("SELECT m FROM MLMProjects m WHERE m.projects = :project");
        query.setParameter("project", p);
        return (entity.MLMProjects)DataMartBeanUtil.getEntityByQueryAllowNull(query,
                "Multiple MLM IDs found for project " + projType + "/" + projId);
    }
}
