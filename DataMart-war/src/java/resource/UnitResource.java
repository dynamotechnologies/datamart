package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.UnitManagerRemote;
import us.fed.fs.www.nepa.schema.unit.ObjectFactory;
import us.fed.fs.www.nepa.schema.unit.Unit;
import us.fed.fs.www.nepa.schema.unit.Units;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;


@Path ("/ref/units")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class UnitResource extends AbstractResource
{
    private UnitManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public UnitResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "Unit.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.unit");
        MY_PROPS.setProperty("bean", "UnitManagerRemote");
        try {
            beanmanager = (UnitManagerRemote) init(UnitManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("UnitResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("UnitResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;
        Unit unit;
        try {
            unit = (Unit)unmarshal(inputxml, Unit.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+unit.getCode());

            // All clear. Now do the insert.
            beanmanager.create(unit);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the newly created entity
        return created(uri);
    }

    
    @GET
    public Response get(@Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get a dump of all the entities in the datamart
            Units unitlist = beanmanager.getAll();

            // Return 404 if none exist
            if (unitlist.getUnit().isEmpty())
                notFound();

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Units> jaxbelement = objectfactory.createUnits(unitlist);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @GET
    @Path("/{code}")
    public Response get(@PathParam("code") String code, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            System.out.println("GetByCode:" + code);
            if(code.equals("unique")){
                // Get a dump of all the entities in the datamart
                Units unitlist = beanmanager.getAllUnique();

                // Return 404 if none exist
                if (unitlist.getUnit().isEmpty())
                    notFound();

                // Get the query results in a JAXBElement object, marshal and return
                ObjectFactory objectfactory = new ObjectFactory();
                JAXBElement<Units> jaxbelement = objectfactory.createUnits(unitlist);
                sw = marshal(jaxbelement);
            }else{
                // Get the name of the specified entity
                Unit unit = beanmanager.get(code);

                // Get the query results in a JAXBElement object, marshal and return
                ObjectFactory objectfactory = new ObjectFactory();
                JAXBElement<Unit> jaxbelement = objectfactory.createUnit(unit);
                sw = marshal(jaxbelement);
            }
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @PUT
    @Path("/{code}")
    public Response put(@PathParam("code") String code, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        Unit unit = null;
        try {
            unit = (Unit) unmarshal(inputxml, Unit.class);

            if (!code.equalsIgnoreCase(unit.getCode()))
                complainLoudly(new Exception("Code in uri must match code in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.update(unit);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"/"+unit.getCode()+"</location>");
    }

    
    @DELETE
    @Path("/{code}")
    public Response delete(@PathParam("code") String code, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(code);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}