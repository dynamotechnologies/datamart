package client;

import java.io.IOException;
import java.util.List;
import us.fed.fs.www.nepa.schema.documentcontainer.*;


public class DocumentContainerClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public DocumentContainerClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.documentcontainer", "DocumentContainer.xsd");
    }


    // The docsAndContainers parameter may consist of any order or number of Container and
    //   ContainerDoc objects.
    public void postDocumentContainer(String projectType, String projectid, List<java.io.Serializable> docsAndContainers)
            throws IOException
    {
        Containers containers = new Containers();
        containers.getContainerOrContainerdoc().addAll(docsAndContainers);
        post(objectfactory.createContainers(containers), "/projects/" + projectType + "/" + projectid + "/containers");
    }


    public Containers getContainerTree(String projectType, String projectID, Boolean published)
            throws IOException
    {
        String pubflag = "";
        if (published != null && published)
            pubflag = "?pubflag=1";
        return get("/projects/" + projectType + "/" + projectID + "/containers/" + pubflag, Containers.class);
    }
    public Containers getContainerTree(String projectType, String projectID, Boolean published, Boolean sortdesc)
            throws IOException
    {
        boolean queryParam = false;
        StringBuffer queryBuffer = new StringBuffer();
        if (published != null && published){
            queryParam = true;
            queryBuffer.append("?pubflag=1");
        }    
        if(sortdesc != null && sortdesc)
        {
         if(queryParam)
         queryBuffer.append("&sortdesc=true");
         else
         queryBuffer.append("?sortdesc=true");    
        }
        return get("/projects/" + projectType + "/" + projectID + "/containers/" + queryBuffer.toString(), Containers.class);
    }
    //
    // Container tree home is /[apiroot]/projects/[projectType]/[projectID]/containers
    //
    public Containers getContainerTree(String projectType, String projectID)
            throws IOException
    {
        return get("/projects/" + projectType + "/" + projectID + "/containers/", Containers.class);
    }


    //
    // The docsAndContainers parameter may consist of any order or number of Documents and
    //   Container objects, so List<Object> is about as precise as we can get.
    //
    public void putContainerTree(String projectType, String projectid, List<java.io.Serializable> docsAndContainers)
            throws IOException
    {
        // Create new container tree
        Containers containers = new Containers();
        containers.getContainerOrContainerdoc().addAll(docsAndContainers);
        put(objectfactory.createContainers(containers),
                "/projects/" + projectType + "/" + projectid + "/containers");
    }

    
    public void deleteContainerTree(String projectType, String projectid)
            throws IOException
    {
        delete("/projects/" + projectType + "/" + projectid + "/containers");
    }

    /* Just retrieve containers but no documents */
    public Containers getContainerTemplate(String projectType, String projectid)
            throws IOException
    {
        return get("/projects/" + projectType + "/" + projectid + "/containers/template", Containers.class);
    }

    /* Retrieve list of documents from a single container */
    public Containerdocs getContainerDocs(String projectType, String projectid, Integer contid, Integer start, Integer rows)
            throws IOException
    {
        String query = "/projects/" + projectType + "/" + projectid + "/containers/containerdocs?";
        if (contid != null) {
            query = query + "contid=" + contid + "&";
        }
        if (start != null) {
            query = query + "start=" + start + "&";
        }
        if (rows != null) {
            query = query + "rows=" + rows + "&";
        }
        return get(query, Containerdocs.class);
    }

    /* Update container template */
    /* containertree may only contain Container resources */
    public void putContainerTemplate(String projectType, String projectid, List<java.io.Serializable> containertree)
            throws IOException
    {
        Containers containers = new Containers();
        containers.getContainerOrContainerdoc().addAll(containertree);
        put(objectfactory.createContainers(containers),
                "/projects/" + projectType + "/" + projectid + "/containers/template");
    }

    /* Update container attributes for a list of containerdocs */
    public void postContainerdocs(String projectType, String projectid, Containerdocs doclist)
            throws IOException
    {
        post(objectfactory.createContainerdocs(doclist),
                "/projects/" + projectType + "/" + projectid + "/containers/updatecontainerdocs");
    }
}