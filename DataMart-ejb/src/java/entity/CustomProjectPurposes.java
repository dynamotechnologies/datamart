/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sam
 */
@Entity
@Table(name = "CustomProjectPurposes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CustomProjectPurposes.findAll", query = "SELECT c FROM CustomProjectPurposes c"),
    @NamedQuery(name = "CustomProjectPurposes.findById", query = "SELECT c FROM CustomProjectPurposes c WHERE c.id = :id")})
public class CustomProjectPurposes implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @JoinColumn(name = "PurposeId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private RefPurposes purposeId;
    @JoinColumn(name = "CustomProjectId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private CustomProjects customProjectId;

    public CustomProjectPurposes() {
    }

    public CustomProjectPurposes(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public RefPurposes getPurposeId() {
        return purposeId;
    }

    public void setPurposeId(RefPurposes purposeId) {
        this.purposeId = purposeId;
    }

    public CustomProjects getCustomProjectId() {
        return customProjectId;
    }

    public void setCustomProjectId(CustomProjects customProjectId) {
        this.customProjectId = customProjectId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CustomProjectPurposes)) {
            return false;
        }
        CustomProjectPurposes other = (CustomProjectPurposes) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CustomProjectPurposes[ id=" + id + " ]";
    }
    
}
