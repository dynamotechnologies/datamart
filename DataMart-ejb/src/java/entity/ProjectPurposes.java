package entity;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "ProjectPurposes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProjectPurposes.findAll", query = "SELECT p FROM ProjectPurposes p"),
    @NamedQuery(name = "ProjectPurposes.findById", query = "SELECT p FROM ProjectPurposes p WHERE p.id = :id")})
public class ProjectPurposes implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @JoinColumn(name = "PurposeId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private RefPurposes refPurposes;
    @JoinColumn(name = "ProjectId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Projects projects;

    public ProjectPurposes() {
    }

    public ProjectPurposes(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public RefPurposes getRefPurposes() {
        return refPurposes;
    }

    public void setRefPurposes(RefPurposes refPurposes) {
        this.refPurposes = refPurposes;
    }

    public Projects getProjects() {
        return projects;
    }

    public void setProjects(Projects projects) {
        this.projects = projects;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProjectPurposes)) {
            return false;
        }
        ProjectPurposes other = (ProjectPurposes) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ProjectPurposes[ id=" + id + " ]";
    }

}
