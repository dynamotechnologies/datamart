package stateless;

import javax.ejb.Stateless;
import beanutil.*;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import us.fed.fs.www.nepa.schema.activity.*;


/**
 *
 * @author mhsu
 */
@Stateless(mappedName="ActivityManagerRemote")
public class ActivityManager implements ActivityManagerRemote {

    @PersistenceContext
    private EntityManager em;
 
    @Override
    public void create(Activity resource)
             throws DataMartException
    {
        validate(resource);
        entity.RefActivities entity = createEntity(resource);

        entity.RefActivities badentity = createEntity(entity.getId());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        entity = em.merge(entity);
    }

    @Override
    public Activities getAll()
             throws DataMartException
    {
        // Retrieve all entities from the database
        Query query = em.createQuery("SELECT u FROM RefActivities u");
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Activities responselist = new Activities();
        while (it.hasNext()) {
            responselist.getActivity().add(createResource( (entity.RefActivities)it.next() ));
        }
        return responselist;
    }

    @Override
    public String getName(String id)
            throws DataMartException
    {
        entity.RefActivities entity = createEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return entity.getName();
    }

    @Override
    public void update(Activity resource)
             throws DataMartException
    {
        validate(resource);
        entity.RefActivities entity = createEntity(resource);

        if (createEntity(entity.getId())==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        entity = em.merge(entity);
    }

    @Override
    public void delete(String id)
             throws DataMartException
    {
        entity.RefActivities entity = createEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     *
     * This method does the mapping from jaxb object to entity object and vice versa
     */
    private Activity createResource(entity.RefActivities entity)
            throws DataMartException
    {
        Activity resource = new Activity();
        resource.setId(entity.getId());
        resource.setName(entity.getName());
        return resource;
    }

    private entity.RefActivities createEntity(Activity resource)
            throws DataMartException
    {
        entity.RefActivities entity = new entity.RefActivities();
        entity.setId(resource.getId());
        entity.setName(resource.getName());
        return entity;
    }

    // This version of overloaded method createEntity() accepts a String ID as a parameter, and returns the
    //   associated entity, or null if it doesn't exist
    //
    private entity.RefActivities createEntity(String id)
            throws DataMartException
    {
        return em.find(entity.RefActivities.class, id);
    }

    private void validate(Activity resource)
            throws DataMartException
    {
        DataMartBeanUtil.confirmNotNull(resource.getName(), "Name");
    }

}
