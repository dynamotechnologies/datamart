package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.ProjectDocumentManagerRemote;
import us.fed.fs.www.nepa.schema.projectdocument.ObjectFactory;
import us.fed.fs.www.nepa.schema.projectdocument.Projectdocuments;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.StringWriter;


@Path ("/projectdocs/{type}/{docid}")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class SpecialProjectDocument extends AbstractResource
{
    private ProjectDocumentManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public SpecialProjectDocument()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "ProjectDocument.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.projectdocument");
        MY_PROPS.setProperty("bean", "ProjectDocumentManagerRemote");
        try {
            beanmanager = (ProjectDocumentManagerRemote) init(ProjectDocumentManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("ProjectDocumentResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("ProjectDocumentResource failed to start. Could not ping the bean manager!");
    }



    /*
     * Get all project-documents for a document id
     */
    @GET
    public Response get(@PathParam("type") String type, @PathParam("docid") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            Projectdocuments projdocs = beanmanager.getByDocument(type, id);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Projectdocuments> jaxbelement = objectfactory.createProjectdocuments(projdocs);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }
    
 
}