package stateless;

import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.projectresourcearea.*;
import beanutil.DataMartException;

@Remote
public interface ProjectResourceAreaManagerRemote {
/*
 *
 * The Manager beans are responsible for:
 *   1) Implementing business logic and
 *   2) Translating front-end resources to back-end database entities.
 *
 * Often the resources will consist of data from numerous database tables. It is the manager's responsibility
 *   to ensure that these front-end resources can be accessed as though they were cut from a single piece of wood.
 *
 */
    public void create(Projectresourcearea resource) throws DataMartException;
    public Projectresourcearea get(String projecttype, String projectid, Integer areaid) throws DataMartException;
    public Projectresourceareas getAllByProject(String projecttype, String projectid) throws DataMartException;
    public void update(Projectresourcearea resource) throws DataMartException;
    public void delete(String projecttype, String projectid, Integer areaid) throws DataMartException;
    public Boolean ping();
}
