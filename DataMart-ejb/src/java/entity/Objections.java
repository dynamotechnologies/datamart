/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sam
 */
@Entity
@Table(name = "Objections")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Objections.findAll", query = "SELECT o FROM Objections o"),
    @NamedQuery(name = "Objections.findById", query = "SELECT o FROM Objections o WHERE o.id = :id"),
    @NamedQuery(name = "Objections.findByObjector", query = "SELECT o FROM Objections o WHERE o.objector = :objector"),
    @NamedQuery(name = "Objections.findByResponseDate", query = "SELECT o FROM Objections o WHERE o.responseDate = :responseDate"),
    @NamedQuery(name = "Objections.findByObjectionRule", query = "SELECT o FROM Objections o WHERE o.objectionRule = :objectionRule"),
    @NamedQuery(name = "Objections.findByFilingStatus", query = "SELECT o FROM Objections o WHERE o.filingStatus = :filingStatus"),
    @NamedQuery(name = "Objections.findByStatusDetails", query = "SELECT o FROM Objections o WHERE o.statusDetails = :statusDetails"),
    @NamedQuery(name = "Objections.findByOutcomeDetails", query = "SELECT o FROM Objections o WHERE o.outcomeDetails = :outcomeDetails"),
    @NamedQuery(name = "Objections.findByPeriodStartDate", query = "SELECT o FROM Objections o WHERE o.periodStartDate = :periodStartDate"),
    @NamedQuery(name = "Objections.findByPeriodEndDate", query = "SELECT o FROM Objections o WHERE o.periodEndDate = :periodEndDate"),
    @NamedQuery(name = "Objections.findByResponseDueDate", query = "SELECT o FROM Objections o WHERE o.responseDueDate = :responseDueDate"),
    @NamedQuery(name = "Objections.findByResponseDueDateExt", query = "SELECT o FROM Objections o WHERE o.responseDueDateExt = :responseDueDateExt"),
    @NamedQuery(name = "Objections.findByOverallOutcome", query = "SELECT o FROM Objections o WHERE o.overallOutcome = :overallOutcome"),
    @NamedQuery(name = "Objections.findByTitle", query = "SELECT o FROM Objections o WHERE o.title = :title")})
public class Objections implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private String id;
    @Basic(optional = false)
    @Column(name = "Objector")
    private String objector;
    @Basic(optional = false)
    @Column(name = "ResponseDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date responseDate;
    @Column(name = "ObjectionRule")
    private String objectionRule;
    @Column(name = "FilingStatus")
    private String filingStatus;
    @Column(name = "StatusDetails")
    private String statusDetails;
    @Column(name = "OutcomeDetails")
    private String outcomeDetails;
    @Column(name = "PeriodStartDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date periodStartDate;
    @Column(name = "PeriodEndDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date periodEndDate;
    @Column(name = "ResponseDueDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date responseDueDate;
    @Column(name = "ResponseDueDateExt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date responseDueDateExt;
    @Column(name = "OverallOutcome")
    private String overallOutcome;
    @Column(name = "Title")
    private String title;
     @JoinColumn(name = "ProjectId", referencedColumnName = "Id")
     @ManyToOne(optional = false)
    private Projects projects;
     @JoinColumn(name = "ProjectDocumentId", referencedColumnName = "Id")
     @ManyToOne
    private ProjectDocuments projectDocuments;


    public Objections() {
    }

    public Objections(String id) {
        this.id = id;
    }

    public Objections(String id, String objector, Date responseDate) {
        this.id = id;
        this.objector = objector;
        this.responseDate = responseDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getObjector() {
        return objector;
    }

    public void setObjector(String objector) {
        this.objector = objector;
    }

    public Date getResponseDate() {
        return responseDate;
    }

    public void setResponseDate(Date responseDate) {
        this.responseDate = responseDate;
    }

    public String getObjectionRule() {
        return objectionRule;
    }

    public void setObjectionRule(String objectionRule) {
        this.objectionRule = objectionRule;
    }

    public String getFilingStatus() {
        return filingStatus;
    }

    public void setFilingStatus(String filingStatus) {
        this.filingStatus = filingStatus;
    }

    public String getStatusDetails() {
        return statusDetails;
    }

    public void setStatusDetails(String statusDetails) {
        this.statusDetails = statusDetails;
    }

    public String getOutcomeDetails() {
        return outcomeDetails;
    }

    public void setOutcomeDetails(String outcomeDetails) {
        this.outcomeDetails = outcomeDetails;
    }

    public Date getPeriodStartDate() {
        return periodStartDate;
    }

    public void setPeriodStartDate(Date periodStartDate) {
        this.periodStartDate = periodStartDate;
    }

    public Date getPeriodEndDate() {
        return periodEndDate;
    }

    public void setPeriodEndDate(Date periodEndDate) {
        this.periodEndDate = periodEndDate;
    }

    public Date getResponseDueDate() {
        return responseDueDate;
    }

    public void setResponseDueDate(Date responseDueDate) {
        this.responseDueDate = responseDueDate;
    }

    public Date getResponseDueDateExt() {
        return responseDueDateExt;
    }

    public void setResponseDueDateExt(Date responseDueDateExt) {
        this.responseDueDateExt = responseDueDateExt;
    }

    public String getOverallOutcome() {
        return overallOutcome;
    }

    public void setOverallOutcome(String overallOutcome) {
        this.overallOutcome = overallOutcome;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    
    public Projects getProjects() {
        return projects;
    }
    
    public void setProjects(Projects p){
        projects = p;
    }


    public ProjectDocuments getProjectDocuments() {
        return projectDocuments;
    }
    
    public void setProjectDocuments(ProjectDocuments projectDocuments){
        this.projectDocuments = projectDocuments;
    }

    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Objections)) {
            return false;
        }
        Objections other = (Objections) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Objections[ id=" + id + " ]";
    }
    
}
