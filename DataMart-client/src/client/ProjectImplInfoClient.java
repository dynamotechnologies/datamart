/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import us.fed.fs.www.nepa.schema.ProjectImplInfo.*;

/**
 *
 * @author gauri
 */

public class ProjectImplInfoClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public ProjectImplInfoClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.ProjectImplInfo", "ProjectImplInfo.xsd");
    }


    public void postProjectImplInfo(Integer id, String projectid, String description,String updatedBy, String updatedDate)
            throws IOException
    {
        ProjectImplInfo projectImplInfo = new ProjectImplInfo();
        projectImplInfo.setImplInfoId(id.toString());
        projectImplInfo.setDescription(description);
        projectImplInfo.setProjectId(projectid);
        projectImplInfo.setLastUpdateBy(updatedBy);
        projectImplInfo.setLasteUpdateDate(updatedDate);
        postProjectImplInfo(projectImplInfo);
    }


    public void postProjectImplInfo(ProjectImplInfo projectImplInfo)
            throws IOException
    {
        post(objectfactory.createProjectImplInfo(projectImplInfo), "/projects/nepa/"+projectImplInfo.getProjectId()+"/ProjectImplInfo/");
    }


    public ProjectImplInfo getProjectImplInfo(String projectId)
            throws IOException
    {
        ProjectImplInfos infos=   (ProjectImplInfos)     get("/projects/nepa/"+projectId+"/ProjectImplInfo/", ProjectImplInfos.class);
        if(null !=infos && infos.getProjectImplInfo().size()>0 )
        return infos.getProjectImplInfo().get(0);
        else
        return new ProjectImplInfo(); 
    }



    public void putProjectImplInfo(Integer id, String projectid, String description,String updatedBy, String updatedDate)
            throws IOException
    {
        ProjectImplInfo projectImplInfo = new ProjectImplInfo();
        projectImplInfo.setImplInfoId(id.toString());
        projectImplInfo.setDescription(description);
        projectImplInfo.setProjectId(projectid);
        projectImplInfo.setLastUpdateBy(updatedBy);
        projectImplInfo.setLasteUpdateDate(updatedDate);
        putProjectImplInfo(projectImplInfo);
    }


    public void putProjectImplInfo(ProjectImplInfo projectImplInfo)
            throws IOException
    {
        put(objectfactory.createProjectImplInfo(projectImplInfo), "/projects/nepa/"+projectImplInfo.getProjectId()+"/ProjectImplInfo/" + projectImplInfo.getImplInfoId());
    }


    public void deleteProjectImplInfo(String projectId, String implId)
            throws IOException
    {
        delete("/projects/nepa/"+projectId+"/ProjectImplInfo/"+implId);
    }
    
     
           
}