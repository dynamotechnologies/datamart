package test.entity;

import client.ObjectionClient;
import client.ProjectClient;
import client.ProjectDocumentClient;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.objection.*;
import us.fed.fs.www.nepa.schema.project.Project;


public class ObjectionTest {
    private ObjectionClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new ObjectionClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postObjection(TestData.objections.id1, TestData.projects.id1, TestData.projects.type1,
                    TestData.objections.objector1, TestData.projectdocuments.docid1, TestData.objections.responsedate1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Objection objection = null;
        Objections objectionlist = null;
        Objections unitobjections = null;
        try {
            objection = client.getObjection(TestData.objections.id1);
            objectionlist = client.getObjectionList();
            unitobjections = client.getObjectionsByUnit(TestData.projects.unitcode1); // unit must match admin unit in populateDependencies->postProject()
        } catch (IOException ex) {
            Logger.getLogger(ObjectionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.objections.id1, objection.getId());
        Assert.assertEquals(TestData.projects.id1, objection.getProjectid());
        Assert.assertEquals(TestData.projects.type1, objection.getProjecttype());
        Assert.assertEquals(TestData.objections.objector1, objection.getObjector());
        Assert.assertEquals(TestData.projectdocuments.docid1, objection.getDocid());
        Assert.assertEquals(TestData.objections.responsedate1, objection.getResponsedate().toString());
        Assert.assertTrue(objectionlist.getObjection().get(0).getProjectid().length()>0);
        Assert.assertTrue(objectionlist.getObjection().get(0).getObjector().length()>0);
        Assert.assertTrue(objectionlist.getObjection().get(0).getDocid().length()>0);

        Assert.assertTrue(unitobjections.getObjection().size()>0);
        Boolean isObjectionable = false;
        for (Objection o : unitobjections.getObjection()) {
            if (o.getId().equalsIgnoreCase(objection.getId())) {
                isObjectionable = true;
            }
            try {
                Project project = new ProjectClient().getProject(TestData.projects.type1, o.getProjectid());
                Assert.assertTrue(project.getUnitcode().startsWith(TestData.projects.unitcode1));
            } catch (IOException ex) {
                Logger.getLogger(ObjectionTest.class.getName()).log(Level.SEVERE, null, ex);
                Assert.fail();
            }
        }
        Assert.assertTrue(isObjectionable);

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        Objection objection = null;
        try {
            client.putObjection(TestData.objections.id1, TestData.projects.id1, TestData.projects.type1,
                    TestData.objections.objector2, TestData.projectdocuments.docid2, TestData.objections.responsedate2);
            objection = client.getObjection(TestData.objections.id1);
        } catch (Exception ex) {
            Logger.getLogger(ObjectionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.objections.id1, objection.getId());
        Assert.assertEquals(TestData.projects.id1, objection.getProjectid());
        Assert.assertEquals(TestData.projects.type1, objection.getProjecttype());
        Assert.assertEquals(TestData.objections.objector2, objection.getObjector());
        Assert.assertEquals(TestData.projectdocuments.docid2, objection.getDocid());
        Assert.assertEquals(TestData.objections.responsedate2, objection.getResponsedate().toString());

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getObjection(TestData.objections.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteObjection(TestData.objections.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }

    private void post()
    {
        try {
            client.postObjection(TestData.objections.id1, TestData.projects.id1, TestData.projects.type1,
                    TestData.objections.objector1, TestData.projectdocuments.docid1, TestData.objections.responsedate1);
        } catch (IOException ex) {
            Logger.getLogger(ObjectionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteObjection(TestData.objections.id1);
        } catch (IOException ex) {
            Logger.getLogger(ObjectionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteObjection(TestData.objections.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }


    @BeforeClass
    public static void populateDependencies()
    {
        ProjectDocumentClient pdclient = null;
        ProjectClient projclient = null;
        try {
            pdclient = new ProjectDocumentClient();
            projclient = new ProjectClient();
        } catch (Exception ex) {
            Assert.fail();
        }

        // Project Documents rely on Projects. Pre-populate a Project.
        ArrayList<String> purposeids1 = new ArrayList<String>();
        purposeids1.add(TestData.projects.purposeid1);
        purposeids1.add(TestData.projects.purposeid2);
        purposeids1.add(TestData.projects.purposeid3);
        try {
            projclient.postProject(TestData.projects.id1, TestData.projects.type1,
                    TestData.projects.name1, TestData.projects.unitcode1, TestData.projects.description1,
                    TestData.projects.lastupdate1, TestData.projects.commentreg1, TestData.projects.wwwlink1,
                    TestData.projects.wwwsummary1, TestData.projects.wwwpub1, TestData.projects.analysistypeid1,
                    TestData.projects.statusid1, TestData.projects.contactname1, TestData.projects.contactphone1,
                    TestData.projects.contactemail1, purposeids1, TestData.projects.expirationdate1, TestData.projects.projectdocumentid1,
                    TestData.projects.sopapub, TestData.projects.sopanew, TestData.projects.sopaheader, TestData.projects.esd, TestData.projects.cenodecision);
        } catch (IOException ex) {
            Logger.getLogger(ObjectionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Objections rely on Project Documents. Pre-populate some Project Documents.
        try {
            pdclient.postProjectDocument(TestData.projects.id1, TestData.projects.type1,
                    TestData.projectdocuments.docid1, TestData.projectdocuments.pubflag1, TestData.projectdocuments.wwwlink1,
                    TestData.projectdocuments.docname1, TestData.projectdocuments.description1, TestData.projectdocuments.pdffilesize1,
                    TestData.projectdocuments.sensitiveflag1);
            pdclient.postProjectDocument(TestData.projects.id1, TestData.projects.type1,
                    TestData.projectdocuments.docid2, TestData.projectdocuments.pubflag1, TestData.projectdocuments.wwwlink1,
                    TestData.projectdocuments.docname1, TestData.projectdocuments.description1, TestData.projectdocuments.pdffilesize1,
                    TestData.projectdocuments.sensitiveflag1);
        } catch (IOException ex) {
            Logger.getLogger(ObjectionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    @AfterClass
    public static void cleanupDependencies()
    {
        ProjectDocumentClient pdclient = null;
        ProjectClient projclient = null;
        try {
            pdclient = new ProjectDocumentClient();
            projclient = new ProjectClient();
        } catch (Exception ex) {
            Assert.fail();
        }

        // Clean up the ProjectDocuments
        try {
            pdclient.deleteProjectDocument("nepa", TestData.projects.id1, TestData.projectdocuments.docid1);
            pdclient.deleteProjectDocument("nepa", TestData.projects.id1, TestData.projectdocuments.docid2);
        } catch (IOException ex) {
            Logger.getLogger(ObjectionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Clean up the Project
        try {
            projclient.deleteProject("nepa", TestData.projects.id1);
        } catch (IOException ex) {
            Logger.getLogger(ObjectionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }
}