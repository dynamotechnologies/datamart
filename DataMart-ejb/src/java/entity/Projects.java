package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "Projects")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Projects.findAll", query = "SELECT p FROM Projects p"),
    @NamedQuery(name = "Projects.findById", query = "SELECT p FROM Projects p WHERE p.id = :id"),
    @NamedQuery(name = "Projects.findByPublicId", query = "SELECT p FROM Projects p WHERE p.publicId = :publicId"),
    @NamedQuery(name = "Projects.findByProjectDocumentId", query = "SELECT p FROM Projects p WHERE p.projectDocumentId = :projectDocumentId"),
    @NamedQuery(name = "Projects.findByName", query = "SELECT p FROM Projects p WHERE p.name = :name"),
    @NamedQuery(name = "Projects.findByDescription", query = "SELECT p FROM Projects p WHERE p.description = :description"),
    @NamedQuery(name = "Projects.findByLastUpdate", query = "SELECT p FROM Projects p WHERE p.lastUpdate = :lastUpdate"),
    @NamedQuery(name = "Projects.findByContactName", query = "SELECT p FROM Projects p WHERE p.contactName = :contactName"),
    @NamedQuery(name = "Projects.findByContactPhone", query = "SELECT p FROM Projects p WHERE p.contactPhone = :contactPhone"),
    @NamedQuery(name = "Projects.findByContactEmail", query = "SELECT p FROM Projects p WHERE p.contactEmail = :contactEmail"),
    @NamedQuery(name = "Projects.findByWwwIsPublished", query = "SELECT p FROM Projects p WHERE p.wwwIsPublished = :wwwIsPublished"),
    @NamedQuery(name = "Projects.findByWwwSummary", query = "SELECT p FROM Projects p WHERE p.wwwSummary = :wwwSummary"),
    @NamedQuery(name = "Projects.findByWwwLink", query = "SELECT p FROM Projects p WHERE p.wwwLink = :wwwLink"),
    @NamedQuery(name = "Projects.findByExpirationDate", query = "SELECT p FROM Projects p WHERE p.expirationDate = :expirationDate"),
    @NamedQuery(name = "Projects.findByLocationDesc", query = "SELECT p FROM Projects p WHERE p.locationDesc = :locationDesc"),
    @NamedQuery(name = "Projects.findByLocationLegalDesc", query = "SELECT p FROM Projects p WHERE p.locationLegalDesc = :locationLegalDesc"),
    @NamedQuery(name = "Projects.findByLatitude", query = "SELECT p FROM Projects p WHERE p.latitude = :latitude"),
    @NamedQuery(name = "Projects.findByLongitude", query = "SELECT p FROM Projects p WHERE p.longitude = :longitude"),
    @NamedQuery(name = "Projects.findBySopaPublish", query = "SELECT p FROM Projects p WHERE p.sopaPublish = :sopaPublish"),
    @NamedQuery(name = "Projects.findBySopaNew", query = "SELECT p FROM Projects p WHERE p.sopaNew = :sopaNew"),
    @NamedQuery(name = "Projects.findBySopaHeaderCat", query = "SELECT p FROM Projects p WHERE p.sopaHeaderCat = :sopaHeaderCat"),
    @NamedQuery(name = "Projects.findByAddInfoPubFlag", query = "SELECT p FROM Projects p WHERE p.addInfoPubFlag = :addInfoPubFlag"),
    @NamedQuery(name = "Projects.findByImplPubFlag", query = "SELECT p FROM Projects p WHERE p.implPubFlag = :implPubFlag"),
    @NamedQuery(name = "Projects.findByMeetingPubFlag", query = "SELECT p FROM Projects p WHERE p.meetingPubFlag = :meetingPubFlag")})

public class Projects implements Serializable {

    @OneToMany(mappedBy = "projectId")
    private Collection<ProjectViews> projectViewsCollection;

    @Column(name = "addInfoPubFlag")
    private Boolean addInfoPubFlag;
    @Column(name = "implPubFlag")
    private Boolean implPubFlag;
    @Column(name = "meetingPubFlag")
    private Boolean meetingPubFlag;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projectId")
    private Collection<ProjectMeetingNotices> projectMeetingNoticesCollection;

   
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projectid")
    private Collection<ProjectImplInfo> projectImplInfoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projectId")
    private Collection<ProjectAdditionalInfo> projectAdditionalInfoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projectId")
    private Collection<UserSession> userSessionCollection;

    @Column(name =     "LastUpdate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;
    @Column(name =     "ExpirationDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expirationDate;
    @Basic(optional = false)
    @Column(name = "Cenodecision")
    private boolean cenodecision;
    @Basic(optional = false)
    @Column(name =     "DecisionDecided")
    @Temporal(TemporalType.TIMESTAMP)
    private Date decisionDecided;
    @Column(name = "Esd")
    private boolean esd;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Column(name = "PublicId")
    private String publicId;
    @Column(name = "ProjectDocumentId")
    private Integer projectDocumentId;
    @Basic(optional = false)
    @Column(name = "Name")
    private String name;
    @Column(name = "Description")
    private String description;
    @Column(name = "ContactName")
    private String contactName;
    @Column(name = "ContactPhone")
    private String contactPhone;
    @Column(name = "ContactEmail")
    private String contactEmail;
    @Column(name = "ContactShortName")
    private String contactShortName;
    @Column(name = "PrimaryProjectManager")
    private String primaryProjectManager;
    @Column(name = "SecondaryProjectManager")
    private String secondaryProjectManager;
    @Column(name = "DataEntryPerson")
    private String dataEntryPerson;
    @Basic(optional = false)
    @Column(name = "wwwIsPublished")
    private boolean wwwIsPublished;
    @Column(name = "wwwSummary")
    private String wwwSummary;
    @Column(name = "wwwLink")
    private String wwwLink;
    @Column(name = "LocationDesc")
    private String locationDesc;
    @Column(name = "LocationLegalDesc")
    private String locationLegalDesc;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Latitude")
    private Double latitude;
    @Column(name = "Longitude")
    private Double longitude;
    @Column(name = "SopaPublish")
    private Boolean sopaPublish;
    @Column(name = "SopaNew")
    private Boolean sopaNew;
    @Column(name = "SopaHeaderCat")
    private String sopaHeaderCat;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projects")
    private Collection<ProjectKMLs> projectKMLsCollection;
    @JoinColumn(name = "UnitId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private RefUnits refUnits;
    @JoinColumn(name = "StatusId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private RefStatuses refStatuses;
    @JoinColumn(name = "ProjectTypeId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private ProjectTypes projectTypes;
    @JoinColumn(name = "CommentRegulationId", referencedColumnName = "Id")
    @ManyToOne
    private RefCommentRegulations refCommentRegulations;
    @JoinColumn(name = "ApplicationId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Applications applications;
    @JoinColumn(name = "AnalysisTypeId", referencedColumnName = "Id")
    @ManyToOne
    private RefAnalysisTypes refAnalysisTypes;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projects")
    private Collection<Decisions> decisionsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projects")
    private Collection<ProjectDocumentContainers> projectDocumentContainersCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projects")
    private Collection<ProjectActivities> projectActivitiesCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projects")
    private Collection<ProjectRegions> projectRegionsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projects")
    private Collection<ProjectResourceAreas> projectResourceAreasCollection;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "projects")
    private CARAProjects cARAProjects;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projects")
    private Collection<ProjectGoals> projectGoalsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projects")
    private Collection<ProjectStates> projectStatesCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projects")
    private Collection<ProjectSpecialAuthorities> projectSpecialAuthoritiesCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projects")
    private Collection<NepaCategoricalExclusions> nepaCategoricalExclusionsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projects")
    private Collection<Appeals> appealsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projects")
    private Collection<ProjectPurposes> projectPurposesCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projects")
    private Collection<Objections> objectionsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projects")
    private Collection<ProjectMilestones> projectMilestonesCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projects")
    private Collection<ProjectDocuments> projectDocumentsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projects")
    private Collection<ProjectForests> projectForestsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projects")
    private Collection<ProjectCounties> projectCountiesCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projects")
    private Collection<ProjectDistricts> projectDistrictsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projects")
    private Collection<MailingListSubscribers> mailingListSubscribersCollection;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "projects")
    private MLMProjects mLMProjects;

    public Projects() {
    }

    public Projects(Integer id) {
        this.id = id;
    }

    public Projects(Integer id, String name, boolean wwwIsPublished,boolean addInfoPubFlag, boolean implPubFlag, boolean meetingPubFlag) {
        this.id = id;
        this.name = name;
        this.wwwIsPublished = wwwIsPublished;
        this.addInfoPubFlag = addInfoPubFlag;
        this.implPubFlag = implPubFlag;
        this.meetingPubFlag = meetingPubFlag;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    public Integer getProjectDocumentId() {
        return projectDocumentId;
    }

    public void setProjectDocumentId(Integer projectDocumentId) {
        this.projectDocumentId = projectDocumentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }
    public boolean getAddInfoPubFlag() {
        return addInfoPubFlag;
    }

    public void setAddInfoPubFlag(boolean addInfoPubFlag) {
        this.addInfoPubFlag = addInfoPubFlag;
    }

    public boolean getImplPubFlag() {
        return implPubFlag;
    }

    public void setImplPubFlag(boolean implPubFlag) {
        this.implPubFlag = implPubFlag;
    }

    public boolean getMeetingPubFlag() {
        return meetingPubFlag;
    }

    public void setMeetingPubFlag(boolean meetingPubFlag) {
        this.meetingPubFlag = meetingPubFlag;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }
    
    public String getContactShortName() {
        return contactShortName;
    }

    public void setContactShortName(String contactShortName) {
        this.contactShortName = contactShortName;
    }

    public String getPrimaryProjectManager() {
        return primaryProjectManager;
    }

    public void setPrimaryProjectManager(String primaryProjectManager) {
        this.primaryProjectManager = primaryProjectManager;
    }

    public String getSecondaryProjectManager() {
        return secondaryProjectManager;
    }

    public void setSecondaryProjectManager(String secondaryProjectManager) {
        this.secondaryProjectManager = secondaryProjectManager;
    }

    public String getDataEntryPerson() {
        return dataEntryPerson;
    }

    public void setDataEntryPerson(String dataEntryPerson) {
        this.dataEntryPerson = dataEntryPerson;
    }
    
    public boolean getWwwIsPublished() {
        return wwwIsPublished;
    }

    public void setWwwIsPublished(boolean wwwIsPublished) {
        this.wwwIsPublished = wwwIsPublished;
    }

    public String getWwwSummary() {
        return wwwSummary;
    }

    public void setWwwSummary(String wwwSummary) {
        this.wwwSummary = wwwSummary;
    }

    public String getWwwLink() {
        return wwwLink;
    }

    public void setWwwLink(String wwwLink) {
        this.wwwLink = wwwLink;
    }

    public String getLocationDesc() {
        return locationDesc;
    }

    public void setLocationDesc(String locationDesc) {
        this.locationDesc = locationDesc;
    }

    public String getLocationLegalDesc() {
        return locationLegalDesc;
    }

    public void setLocationLegalDesc(String locationLegalDesc) {
        this.locationLegalDesc = locationLegalDesc;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Boolean getSopaPublish() {
        return sopaPublish;
    }

    public void setSopaPublish(Boolean sopaPublish) {
        this.sopaPublish = sopaPublish;
    }

    public Boolean getSopaNew() {
        return sopaNew;
    }

    public void setSopaNew(Boolean sopaNew) {
        this.sopaNew = sopaNew;
    }

    public String getSopaHeaderCat() {
        return sopaHeaderCat;
    }

    public void setSopaHeaderCat(String sopaHeaderCat) {
        this.sopaHeaderCat = sopaHeaderCat;
    }

    @XmlTransient
    public Collection<ProjectKMLs> getProjectKMLsCollection() {
        return projectKMLsCollection;
    }

    public void setProjectKMLsCollection(Collection<ProjectKMLs> projectKMLsCollection) {
        this.projectKMLsCollection = projectKMLsCollection;
    }

    public RefUnits getRefUnits() {
        return refUnits;
    }

    public void setRefUnits(RefUnits refUnits) {
        this.refUnits = refUnits;
    }

    public RefStatuses getRefStatuses() {
        return refStatuses;
    }

    public void setRefStatuses(RefStatuses refStatuses) {
        this.refStatuses = refStatuses;
    }

    public ProjectTypes getProjectTypes() {
        return projectTypes;
    }

    public void setProjectTypes(ProjectTypes projectTypes) {
        this.projectTypes = projectTypes;
    }

    public RefCommentRegulations getRefCommentRegulations() {
        return refCommentRegulations;
    }

    public void setRefCommentRegulations(RefCommentRegulations refCommentRegulations) {
        this.refCommentRegulations = refCommentRegulations;
    }

    public Applications getApplications() {
        return applications;
    }

    public void setApplications(Applications applications) {
        this.applications = applications;
    }

    public RefAnalysisTypes getRefAnalysisTypes() {
        return refAnalysisTypes;
    }

    public void setRefAnalysisTypes(RefAnalysisTypes refAnalysisTypes) {
        this.refAnalysisTypes = refAnalysisTypes;
    }

    @XmlTransient
    public Collection<Decisions> getDecisionsCollection() {
        return decisionsCollection;
    }

    public void setDecisionsCollection(Collection<Decisions> decisionsCollection) {
        this.decisionsCollection = decisionsCollection;
    }

    @XmlTransient
    public Collection<ProjectDocumentContainers> getProjectDocumentContainersCollection() {
        return projectDocumentContainersCollection;
    }

    public void setProjectDocumentContainersCollection(Collection<ProjectDocumentContainers> projectDocumentContainersCollection) {
        this.projectDocumentContainersCollection = projectDocumentContainersCollection;
    }

    @XmlTransient
    public Collection<ProjectActivities> getProjectActivitiesCollection() {
        return projectActivitiesCollection;
    }

    public void setProjectActivitiesCollection(Collection<ProjectActivities> projectActivitiesCollection) {
        this.projectActivitiesCollection = projectActivitiesCollection;
    }

    @XmlTransient
    public Collection<ProjectRegions> getProjectRegionsCollection() {
        return projectRegionsCollection;
    }

    public void setProjectRegionsCollection(Collection<ProjectRegions> projectRegionsCollection) {
        this.projectRegionsCollection = projectRegionsCollection;
    }

    @XmlTransient
    public Collection<ProjectResourceAreas> getProjectResourceAreasCollection() {
        return projectResourceAreasCollection;
    }

    public void setProjectResourceAreasCollection(Collection<ProjectResourceAreas> projectResourceAreasCollection) {
        this.projectResourceAreasCollection = projectResourceAreasCollection;
    }

    public CARAProjects getCARAProjects() {
        return cARAProjects;
    }

    public void setCARAProjects(CARAProjects cARAProjects) {
        this.cARAProjects = cARAProjects;
    }

    @XmlTransient
    public Collection<ProjectGoals> getProjectGoalsCollection() {
        return projectGoalsCollection;
    }

    public void setProjectGoalsCollection(Collection<ProjectGoals> projectGoalsCollection) {
        this.projectGoalsCollection = projectGoalsCollection;
    }

    @XmlTransient
    public Collection<ProjectStates> getProjectStatesCollection() {
        return projectStatesCollection;
    }

    public void setProjectStatesCollection(Collection<ProjectStates> projectStatesCollection) {
        this.projectStatesCollection = projectStatesCollection;
    }

    @XmlTransient
    public Collection<ProjectSpecialAuthorities> getProjectSpecialAuthoritiesCollection() {
        return projectSpecialAuthoritiesCollection;
    }

    public void setProjectSpecialAuthoritiesCollection(Collection<ProjectSpecialAuthorities> projectSpecialAuthoritiesCollection) {
        this.projectSpecialAuthoritiesCollection = projectSpecialAuthoritiesCollection;
    }

    @XmlTransient
    public Collection<NepaCategoricalExclusions> getNepaCategoricalExclusionsCollection() {
        return nepaCategoricalExclusionsCollection;
    }

    public void setNepaCategoricalExclusionsCollection(Collection<NepaCategoricalExclusions> nepaCategoricalExclusionsCollection) {
        this.nepaCategoricalExclusionsCollection = nepaCategoricalExclusionsCollection;
    }

    @XmlTransient
    public Collection<Appeals> getAppealsCollection() {
        return appealsCollection;
    }

    public void setAppealsCollection(Collection<Appeals> appealsCollection) {
        this.appealsCollection = appealsCollection;
    }

    @XmlTransient
    public Collection<ProjectPurposes> getProjectPurposesCollection() {
        return projectPurposesCollection;
    }

    public void setProjectPurposesCollection(Collection<ProjectPurposes> projectPurposesCollection) {
        this.projectPurposesCollection = projectPurposesCollection;
    }

    @XmlTransient
    public Collection<Objections> getObjectionsCollection() {
        return objectionsCollection;
    }

    public void setObjectionsCollection(Collection<Objections> objectionsCollection) {
        this.objectionsCollection = objectionsCollection;
    }

    @XmlTransient
    public Collection<ProjectMilestones> getProjectMilestonesCollection() {
        return projectMilestonesCollection;
    }

    public void setProjectMilestonesCollection(Collection<ProjectMilestones> projectMilestonesCollection) {
        this.projectMilestonesCollection = projectMilestonesCollection;
    }

    @XmlTransient
    public Collection<ProjectDocuments> getProjectDocumentsCollection() {
        return projectDocumentsCollection;
    }

    public void setProjectDocumentsCollection(Collection<ProjectDocuments> projectDocumentsCollection) {
        this.projectDocumentsCollection = projectDocumentsCollection;
    }

    @XmlTransient
    public Collection<ProjectForests> getProjectForestsCollection() {
        return projectForestsCollection;
    }

    public void setProjectForestsCollection(Collection<ProjectForests> projectForestsCollection) {
        this.projectForestsCollection = projectForestsCollection;
    }

    @XmlTransient
    public Collection<ProjectCounties> getProjectCountiesCollection() {
        return projectCountiesCollection;
    }

    public void setProjectCountiesCollection(Collection<ProjectCounties> projectCountiesCollection) {
        this.projectCountiesCollection = projectCountiesCollection;
    }

    @XmlTransient
    public Collection<ProjectDistricts> getProjectDistrictsCollection() {
        return projectDistrictsCollection;
    }

    public void setProjectDistrictsCollection(Collection<ProjectDistricts> projectDistrictsCollection) {
        this.projectDistrictsCollection = projectDistrictsCollection;
    }

    @XmlTransient
    public Collection<MailingListSubscribers> getMailingListSubscribersCollection() {
        return mailingListSubscribersCollection;
    }

    public void setMailingListSubscribersCollection(Collection<MailingListSubscribers> mailingListSubscribersCollection) {
        this.mailingListSubscribersCollection = mailingListSubscribersCollection;
    }

    public MLMProjects getMLMProjects() {
        return mLMProjects;
    }

    public void setMLMProjects(MLMProjects mLMProjects) {
        this.mLMProjects = mLMProjects;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Projects)) {
            return false;
        }
        Projects other = (Projects) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Projects[ id=" + id + " ]";
    }

    public boolean getEsd() {
        return esd;
    }

    public void setEsd(boolean esd) {
        this.esd = esd;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public boolean getCenodecision() {
        return cenodecision;
    }

    public void setCenodecision(boolean cenodecision) {
        this.cenodecision = cenodecision;
    }
    
    public Date getDecisionDecided() {
        return decisionDecided;
    }

    public void setDecisionDecided(Date decisionDecided) {
        this.decisionDecided = decisionDecided;
    }

    @XmlTransient
    public Collection<UserSession> getUserSessionCollection() {
        return userSessionCollection;
    }

    public void setUserSessionCollection(Collection<UserSession> userSessionCollection) {
        this.userSessionCollection = userSessionCollection;
    }

    @XmlTransient
    public Collection<ProjectAdditionalInfo> getProjectAdditionalInfoCollection() {
        return projectAdditionalInfoCollection;
    }

    public void setProjectAdditionalInfoCollection(Collection<ProjectAdditionalInfo> projectAdditionalInfoCollection) {
        this.projectAdditionalInfoCollection = projectAdditionalInfoCollection;
    }

    @XmlTransient
    public Collection<ProjectImplInfo> getProjectImplInfoCollection() {
        return projectImplInfoCollection;
    }

    public void setProjectImplInfoCollection(Collection<ProjectImplInfo> projectImplInfoCollection) {
        this.projectImplInfoCollection = projectImplInfoCollection;
    }

   

    public void setAddInfoPubFlag(Boolean addInfoPubFlag) {
        this.addInfoPubFlag = addInfoPubFlag;
    }

   

    public void setImplPubFlag(Boolean implPubFlag) {
        this.implPubFlag = implPubFlag;
    }

   

    public void setMeetingPubFlag(Boolean meetingPubFlag) {
        this.meetingPubFlag = meetingPubFlag;
    }

   

    @XmlTransient
    public Collection<ProjectMeetingNotices> getProjectMeetingNoticesCollection() {
        return projectMeetingNoticesCollection;
    }

    public void setProjectMeetingNoticesCollection(Collection<ProjectMeetingNotices> projectMeetingNoticesCollection) {
        this.projectMeetingNoticesCollection = projectMeetingNoticesCollection;
    }

    @XmlTransient
    public Collection<ProjectViews> getProjectViewsCollection() {
        return projectViewsCollection;
    }

    public void setProjectViewsCollection(Collection<ProjectViews> projectViewsCollection) {
        this.projectViewsCollection = projectViewsCollection;
    }

}
