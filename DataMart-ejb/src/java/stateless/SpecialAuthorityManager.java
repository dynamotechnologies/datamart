package stateless;

import javax.ejb.Stateless;
import beanutil.*;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import us.fed.fs.www.nepa.schema.specialauthority.*;

/**
 *
 * @author mhsu
 */
@Stateless(mappedName="SpecialAuthorityManagerRemote")
public class SpecialAuthorityManager implements SpecialAuthorityManagerRemote {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void create(Specialauthority resource)
             throws DataMartException
    {
        validate(resource);
        entity.RefSpecialAuthorities entity = createEntity(resource);

        entity.RefSpecialAuthorities badentity = createEntity(entity.getId());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        entity = em.merge(entity);
    }

    @Override
    public Specialauthorities getAll()
             throws DataMartException
    {
        // Retrieve all entities from the database
        Query query = em.createQuery("SELECT u FROM RefSpecialAuthorities u");
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Specialauthorities responselist = new Specialauthorities();
        while (it.hasNext()) {
            responselist.getSpecialauthority().add(createResource( (entity.RefSpecialAuthorities)it.next() ));
        }
        return responselist;
    }

    @Override
    public Specialauthority get(String id)
            throws DataMartException
    {
        Query query = em.createNamedQuery("RefSpecialAuthorities.findById");
        query.setParameter("id", id);
        try {
            entity.RefSpecialAuthorities entity = (entity.RefSpecialAuthorities)query.getSingleResult();
            return createResource(entity);
        } catch (javax.persistence.NoResultException e) {
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        }
    }

    @Override
    public void update(Specialauthority resource)
             throws DataMartException
    {
        validate(resource);
        entity.RefSpecialAuthorities entity = createEntity(resource);

        if (createEntity(entity.getId())==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        entity = em.merge(entity);
    }

    @Override
    public void delete(String id)
             throws DataMartException
    {
        entity.RefSpecialAuthorities entity = createEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     *
     * This method does the mapping from jaxb object to entity object and vice versa
     */
    private Specialauthority createResource(entity.RefSpecialAuthorities entity)
            throws DataMartException
    {
        Specialauthority resource = new Specialauthority();
        resource.setId(entity.getId());
        resource.setName(entity.getName());
        resource.setDescription(entity.getDescription());
        return resource;
    }

    private entity.RefSpecialAuthorities createEntity(Specialauthority resource)
            throws DataMartException
    {
        entity.RefSpecialAuthorities entity = new entity.RefSpecialAuthorities();
        entity.setId(resource.getId());
        entity.setName(resource.getName());
        entity.setDescription(resource.getDescription());
        return entity;
    }

    // This version of overloaded method createEntity() accepts a String ID as a parameter, and returns the
    //   associated entity, or null if it doesn't exist
    //
    private entity.RefSpecialAuthorities createEntity(String id)
            throws DataMartException
    {
        return em.find(entity.RefSpecialAuthorities.class, id);
    }

    private void validate(Specialauthority resource)
            throws DataMartException
    {
        DataMartBeanUtil.confirmNotNull(resource.getName(), "Name");
        DataMartBeanUtil.confirmNotNull(resource.getName(), "Description");
    }

}
