package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.CommentRegulationManagerRemote;
import us.fed.fs.www.nepa.schema.commentregulation.Commentregulation;
import us.fed.fs.www.nepa.schema.commentregulation.Commentregulations;
import us.fed.fs.www.nepa.schema.commentregulation.ObjectFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;


@Path ("/ref/commentregulations")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class CommentRegulationResource extends AbstractResource
{
    private CommentRegulationManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public CommentRegulationResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "CommentRegulation.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.commentregulation");
        MY_PROPS.setProperty("bean", "CommentRegulationManagerRemote");
        try {
            beanmanager = (CommentRegulationManagerRemote) init(CommentRegulationManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("CommentRegulationResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("CommentRegulationResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;
        try {
            Commentregulation reg = (Commentregulation)unmarshal(inputxml, Commentregulation.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+reg.getId());

            // All clear. Now do the insert.
            beanmanager.create(reg);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        // Return a link to the newly created entity
        return created(uri);
    }


    @GET
    public Response get(@Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get a dump of all the entities in the datamart
            Commentregulations reglist = beanmanager.getAll();

            // Return 404 if none exist
            if (reglist.getCommentregulation().isEmpty())
                notFound();

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Commentregulations> jaxbelement = objectfactory.createCommentregulations(reglist);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }

    @GET
    @Path("/{id}")
    public Response get(@PathParam("id") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the name of the specified entity
            String name = beanmanager.getName(id);
            Commentregulation reg = new Commentregulation();
            reg.setId(id);
            reg.setName(name);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Commentregulation> jaxbelement = objectfactory.createCommentregulation(reg);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @PUT
    @Path("/{id}")
    public Response put(@PathParam("id") String id, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        Commentregulation reg = null;
        try {
            reg = (Commentregulation) unmarshal(inputxml, Commentregulation.class);

            if (!id.equalsIgnoreCase(reg.getId()))
                complainLoudly(new Exception("Id in uri must match id in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.update(reg);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"/"+reg.getId()+"</location>");
    }


    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(id);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}