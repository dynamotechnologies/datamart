import xml.sax.handler
import pprint
import xml.dom.minidom
import sys, os
fname = os.path.join(os.path.dirname(__file__), "/var/www/common/dmUtil")
sys.path.insert(0, fname)
import dmUtil
from xml.sax.saxutils import escape
from urllib2 import HTTPError
 
# TODO
# 1.  Snapshot old Units data before running
# 2.  Actually log/handle errors, user should notice them
# 3.  Genericize merge code?

class UnitsHandler(xml.sax.handler.ContentHandler):

  def __init__(self):
    # do global inits here
    self.mapping = {}
    self.DRY_RUN = 1

    # Comment out after configuration
    raise Exception("unitshandler.py not configured, please edit file")

    # self.UNITS_URL = 'http://localhost:7001/api/1_0/ref/units/' # CORETST,DEV
    # self.UNITS_URL = 'http://localhost:7003/api/1_0/ref/units/' # PILOT
    self.inunits = 0
    self.intimestamp = 0
    self.inunit = 0
    dmUtil.initOpener()
    self.InputUnitXmlToDatamartMapping = {
      # Matt's name		Schema name
      "code":			"code",
      "name":			"name",
      "extendeddetails":	"extendeddetails",
      "projectmap":		"projectmap",
      "address1":		"address1",
      "address2":		"address2",
      "city":			"city",
      "state":			"state",
      "zip":			"zip",
      "phonenumber":		"phonenumber",
      "wwwlink":		"wwwlink",
      "commentemail":		"commentemail",
      "newspaper":		"newspaper",
      "newspaperurl":		"newspaperurl",
      "boundaryurl":		"boundaryurl",
      "active":			"active"
    }
    self.InputUnitFields = self.InputUnitXmlToDatamartMapping.keys()
    self.DatamartFields = self.InputUnitXmlToDatamartMapping.values()
 
  def startElement(self, name, attributes):
    if name == "units":
      # print "In units"
      self.inunits = 1
    elif name == "timestamp":
      if (self.inunits):
        # print "In timestamp"
        self.buffer = ""
        self.intimestamp = 1
      else:
        print "Missing units tag?"
    elif name == "unit":
      if (self.inunits):
        # print "In unit"
        self.buffer = ""
        self.inunit = 1
        self.mapping.clear()
      else:
        print "Missing units tag?"
    elif name in self.InputUnitFields:
      if (self.inunit):
        # print "Got attribute " + name
        self.buffer = ""
      else:
        print "Missing unit tag?"
    else:
      print "Unrecognized element " + name
 
  def characters(self, data):
    if ((self.inunit) or (self.intimestamp)):
      self.buffer += data 
 
  def endElement(self, name):
    self.buffer = self.buffer.strip()
    if name == "timestamp":
      print "Timestamp: " + self.buffer
      self.intimestamp = 0
    if name == "units":
      if (self.inunits):
        # all done, quit here
        print "All done"
      else:
        print "Mismatched closing units tag?"
    elif name == "unit":
      if (self.inunit):
        print "from source:"
        pprint.pprint(self.mapping)
        # fetch unit from Datamart
        if ("code" not in self.mapping):
          print "Bad unit record, no code given"
          return
        else:
          newUnit = self.getUnit(self.mapping['code'])
          print "from db:"
          pprint.pprint(newUnit)
          # if unit not found, POST instead of PUT
          unitFound = (len(newUnit) > 0)
          # update unit structure
          self.mergeUnits(self.mapping, newUnit)
          print "merged unit:"
          pprint.pprint(newUnit)
          # store unit in Datamart
          if (unitFound):
            self.putUnit(newUnit)
          else:
            self.postUnit(newUnit)
          # print "leaving " + name
        self.inunit = 0
      else:
        print "Mismatched closing unit tag?"
    else:
      self.mapping[name] = self.buffer

  # Retrieve Unit as dictionary
  def getUnit(self, code):
    newUnit = {}
    try:
      unitxml = dmUtil.get(self.UNITS_URL + code)
      dom = xml.dom.minidom.parseString(unitxml)
      for key in self.DatamartFields:
        nodes = dom.getElementsByTagName(key)
        if (len(nodes) == 1):
          newUnit[key] = self.getText(nodes[0].childNodes)
        elif (len(nodes) > 1):
          print "Too many nodes for key " + key
    except HTTPError, e:
      # If error is a 404, just return an empty dictionary
      if (e.code != 404):
        raise
    return newUnit

  # Store Unit from dictionary
  def putUnit(self, unitDic):
    xml = '<unit xmlns="http://www.fs.fed.us/nepa/schema">\n'
    for key in unitDic.keys():
      xml += '<'+key+'>'+escape(unitDic[key])+'</'+key+'>\n'
    xml += '</unit>\n'
    print "putUnit xml:"
    print xml
    code = unitDic["code"]
    if (self.DRY_RUN):
      print "Dry run, no changes applied"
    else:
      dmUtil.put(self.UNITS_URL + code, xml)

  # Store Unit from dictionary
  def postUnit(self, unitDic):
    xml = '<unit xmlns="http://www.fs.fed.us/nepa/schema">\n'
    for key in unitDic.keys():
      xml += '<'+key+'>'+escape(unitDic[key])+'</'+key+'>\n'
    xml += '</unit>\n'
    print "postUnit xml:"
    print xml
    code = unitDic["code"]
    if (self.DRY_RUN):
      print "Dry run, no changes applied"
    else:
      dmUtil.post(self.UNITS_URL, xml)

  def getText(self, nodelist):
    rc = []
    for node in nodelist:
        if node.nodeType == node.TEXT_NODE:
            rc.append(node.data)
    return ''.join(rc)

  # Merge Unit data from input dictionary into Datamart dictionary
  def mergeUnits(self, inputDic, datamartDic):
    for key in inputDic.keys():
      datamartDic[self.InputUnitXmlToDatamartMapping[key]] = inputDic[key]

