package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.projectspecialauthority.*;

import beanutil.*;
import javax.ejb.Stateless;
import java.util.Collection;
import java.util.Iterator;
import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Stateless(mappedName="ProjectSpecialAuthorityManagerRemote")
public class ProjectSpecialAuthorityManager implements ProjectSpecialAuthorityManagerRemote
{

    @PersistenceContext
    private EntityManager em;

    private String PROJECT_TYPE = "nepa";


    @Override
    public void merge(Projectspecialauthority resource)
             throws DataMartException
    {
        entity.ProjectSpecialAuthorities entity = createEntity(resource);

        // Check for existing entity and grab the internal ID if it exists
        entity.ProjectSpecialAuthorities oldauth = getEntity(resource.getProjectid(), entity.getRefSpecialAuthorities().getId());
        if (oldauth!=null)
            entity.setId(oldauth.getId());

        entity = em.merge(entity);
    }


    @Override
    public Projectspecialauthority get(String projectid, String id)
            throws DataMartException
    {
        entity.ProjectSpecialAuthorities entity = getEntity(projectid, id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        return createResource(entity);
    }


    @Override
    public Projectspecialauthorities getByProject(String projectid) throws DataMartException {
        // Get a reference to the project
        entity.Projects project = DataMartBeanUtil.getProject(em, this.PROJECT_TYPE, projectid);
        if (project==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Project with type '" + this.PROJECT_TYPE + "' and ID '" + projectid + "' does not exist.");

        Query query = em.createQuery("SELECT a FROM ProjectSpecialAuthorities a WHERE a.projects = :project");
        query.setParameter("project", project);

        @SuppressWarnings("unchecked")
        Collection<entity.ProjectSpecialAuthorities> entitylist = (Collection<entity.ProjectSpecialAuthorities>)query.getResultList();

        // Loop over the query results and build a list of resources
        Projectspecialauthorities authoritylist = new Projectspecialauthorities();
        Iterator it = entitylist.iterator();
        while (it.hasNext())
        {
            authoritylist.getProjectspecialauthority().add(createResource((entity.ProjectSpecialAuthorities)it.next()));
        }

        return authoritylist;
    }


    @Override
    public void delete(String projectid, String id)
             throws DataMartException
    {
        // Verify that this entity exists
        entity.ProjectSpecialAuthorities entity = getEntity(projectid, id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Could not delete nonexistent Project Special Authority '"+id+"'.");

        // Remove the entity itself
        em.remove(entity);
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     *
     * This method does the mapping from entity object to jaxb object
     */
    protected static Projectspecialauthority createResource(entity.ProjectSpecialAuthorities entity)
            throws DataMartException
    {
        Projectspecialauthority resource = new Projectspecialauthority();
        resource.setProjectid(entity.getProjects().getPublicId());
        resource.setSpecialauthorityid(entity.getRefSpecialAuthorities().getId());
        return resource;
    }


    /*
     *
     * This method does the mapping from jaxb object to entity object
     */
    private entity.ProjectSpecialAuthorities createEntity(Projectspecialauthority resource)
            throws DataMartException
    {
        String projectid = resource.getProjectid(); // Always reference the public ID in error messages

        // Get a reference to the project
        entity.Projects project = DataMartBeanUtil.getProject(em, "nepa", projectid);
        if (project==null)
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION,
                    "Project with type 'nepa' and ID '"+projectid+"' does not exist.");

        // Make sure the foreign keys are valid, and retrieve a jpa representation of the associated rows
        entity.RefSpecialAuthorities specialauthority = (entity.RefSpecialAuthorities) DataMartBeanUtil.getEntityById(em,
                "RefSpecialAuthorities", resource.getSpecialauthorityid(), "Special Authority ID");

        entity.ProjectSpecialAuthorities entity = new entity.ProjectSpecialAuthorities();
        entity.setProjects(project);
        entity.setRefSpecialAuthorities(specialauthority);
        
        return entity;
    }


    private entity.ProjectSpecialAuthorities getEntity(String projectid, String id)
            throws DataMartException
    {
        // Get the project from the public project id
        entity.Projects project = DataMartBeanUtil.getProject(em, this.PROJECT_TYPE, projectid);

        entity.RefSpecialAuthorities refspecialauth = (entity.RefSpecialAuthorities) DataMartBeanUtil.getEntityById(em,
                "RefSpecialAuthorities", id, "Special Authority");

        Query query = em.createQuery("SELECT p FROM ProjectSpecialAuthorities p WHERE p.projects = :project AND p.refSpecialAuthorities = :specialauth");
        query.setParameter("project", project);
        query.setParameter("specialauth", refspecialauth);

        entity.ProjectSpecialAuthorities specialauthority = (entity.ProjectSpecialAuthorities) DataMartBeanUtil.getEntityByQueryAllowNull(query,
                "More than one Special Authority exists with id '" + id + "'");
        return specialauthority;
    }
}