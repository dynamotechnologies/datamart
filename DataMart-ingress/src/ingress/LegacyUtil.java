package ingress;

import client.DecisionTypeClient;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.HashMap;
import java.util.zip.DataFormatException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import us.fed.fs.www.nepa.schema.decisiontype.Decisiontype;
import us.fed.fs.www.nepa.schema.decisiontype.Decisiontypes;

public class LegacyUtil {
    final static String DATETIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    final static String DATE_FORMAT = "yyyy-MM-dd";
    static Boolean TEST_MODE = false;
    static HashMap<Integer, String> DecisionTypeMap = null;

    /*
     * Data retrieval
     *
     */


    public static Properties getIngressProps()
            throws IOException
    {
        Properties props = new Properties();
        InputStream propstream = Legacy.class.getResourceAsStream("ingress.properties");
        props.load(propstream);
        return props;
    }


    public static Document getDocFromUrl(String path)
            throws IOException
    {
        URL url = new URL(path);
        URLConnection urlconn = url.openConnection();
        InputStream is = urlconn.getInputStream();
        return getDocFromStream(is);
    }


    public static Document getDocFromStream(InputStream is)
    {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = factory.newDocumentBuilder();
            return db.parse(is);
        } catch (SAXException ex) {
            Logger.getLogger(LegacyUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(LegacyUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(LegacyUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }


    /*
     *
     * Data transformation, conversion, extraction
     *
     */



    /*
     * The legacy xml doesn't contain a document ID for www exp docs, so we need to
     *   extract it from the url. The url's look like this:
     *
     *   <documenturl>http://cache.ecosystem-management.org/69340_FSPLT2_021680.pdf</documenturl>
     */
    public static String getDocIdFromUrl(String url)
            throws DataFormatException
    {
        if (TEST_MODE) {
            if (url.endsWith(".pdf") && url.contains("FSTST"))
            {
                return url.substring(url.lastIndexOf("FSTST"), url.length()-4);
            }
        }
        if (!(url.endsWith(".pdf") && url.contains("FSPLT")))
            throw new DataFormatException("url " + url + " is in unexpected format.");

        return url.substring(url.lastIndexOf("FSPLT"), url.length()-4);
    }


    /*
     * Look up the ID of the outcome based on the name in the xml.
     * The outcome names in the xml have multiple spaces in between some of the words, so
     *     we need to remove all whitespace before performing the comparison
     */
    public static String getOutcomeIdFromName(String name)
            throws DataFormatException
    {
        String n = name.replaceAll(" ", "");

        if (n.equalsIgnoreCase("NotTimely"))
                return "1";
        if (n.equalsIgnoreCase("ReliefCannotBeGranted"))
            return "2";
        if (n.equalsIgnoreCase("AppealPendingOtherAuthority"))
            return "3";
        if (n.equalsIgnoreCase("DecisionNotSubjecttoAppeal"))
            return "4";
        if (n.equalsIgnoreCase("NoCommentsFiled"))
            return "5";
        if (n.equalsIgnoreCase("FSWithdrewDecision"))
            return "6";
        if (n.equalsIgnoreCase("AppellantWithdrewAppeal"))
            return "7";
        if (n.equalsIgnoreCase("InsufficientInfo"))
            return "8";
        if (n.equalsIgnoreCase("Affirmed"))
            return "9";
        if (n.equalsIgnoreCase("AffirmedwithInstructions"))
            return "10";
        if (n.equalsIgnoreCase("ReversedinWhole"))
            return "11";
        if (n.equalsIgnoreCase("ReversedinPart"))
            return "12";
        if (n.equalsIgnoreCase("NoAppealDecisionIssued"))
            return "13";
        if (n.equalsIgnoreCase("None"))
            return "14";
        if (n.equalsIgnoreCase("Dismissed"))
            return "15";

        throw new DataFormatException("Appeal Outcome '" + name + "' not found.");
    }


    public static Boolean isDismissalReason(String name)
    {
        String n = name.replaceAll(" ", "");
        return (n.equalsIgnoreCase("Unknown") ||
                n.equalsIgnoreCase("NotTimely") ||
                n.equalsIgnoreCase("ReliefCannotBeGranted") ||
                n.equalsIgnoreCase("AppealPendingOtherAuthority") ||
                n.equalsIgnoreCase("DecisionNotSubjecttoAppeal") ||
                n.equalsIgnoreCase("NoCommentsFiled") ||
                n.equalsIgnoreCase("FSWithdrewDecision") ||
                n.equalsIgnoreCase("AppellantWithdrewAppeal") ||
                n.equalsIgnoreCase("InsufficientInfo") ||
                n.equalsIgnoreCase("Dismissed")) ? true : false;
    }


    public static String getDismissalIdFromOutcomeId(String outcomeid)
    {
        if (outcomeid.equalsIgnoreCase("15"))
            return "9";
        else
            return outcomeid;
    }


    public static XMLGregorianCalendar getXMLGregorianFromDateTime(Date date)
            throws DataFormatException
    {
        GregorianCalendar gCal = new GregorianCalendar();
        gCal.setTime(date);
        XMLGregorianCalendar xgCal = null;
        try {
            xgCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCal);
        } catch (DatatypeConfigurationException e) {
            throw new DataFormatException("Error converting date '" + date.toString() + "' to an XMLGregorianDate.");
        }
        return xgCal;
    }


    public static Date getDateFromString(String oldval, String fieldName)
            throws DataFormatException
    {
        String ov = fixDateString(oldval);
        String fmt = (hasTimePart(ov)) ? DATETIME_FORMAT : DATE_FORMAT;
        Date newval;
        try {
            DateFormat df = new SimpleDateFormat(fmt);
            newval = df.parse(ov);
        } catch (ParseException ex) {
            throw new DataFormatException("Could not convert date '" + oldval + "' in field '" + fieldName + "'.");
        }
        return newval;
    }


    /*
     * XML timestamp looks like this: 2011-02-01 7:07:1 PM
     * Should look like this: 2011-02-01
     *
     * Actually we should probably keep the time part, but we have no use for this at
     * the moment and it'll require converting AM/PM to 24 hour time
     */
    public static String cleanXmlTimeStampString(String oldval)
            throws DataFormatException
    {
        if ( !(oldval.contains("AM") || oldval.contains("PM")) )
            throw new DataFormatException("Could not clean XML timestamp '" + oldval + "'. It should contain 'AM' or 'PM'.");

        String[] arr = oldval.split(" ");
        if (arr.length!=3)
            throw new DataFormatException("Could not clean XML timestamp '" + oldval + "'. It should contain two spaces.");

        String[] datearr = arr[0].split("-");
        if (datearr.length!=3 || arr[0].length()>10)
            throw new DataFormatException("Could not clean XML timestamp '" + oldval + "'. The date part should be 10 characters and contain two dashes.");

        return arr[0];
    }


    public static String getStringFromXMLGregorian(XMLGregorianCalendar xgc, Boolean dateOnly)
    {
        String fmt = (dateOnly) ? DATE_FORMAT : DATETIME_FORMAT;
        return new SimpleDateFormat(fmt).format(xgc.toGregorianCalendar().getTime());
    }


    public static String getAnalysisTypeIdFromPalsId(Integer palsid)
            throws DataFormatException
    {
        switch (palsid)
        {
            case 1: return "EIS";
            case 2: return "EA";
            case 3: return "CE";
            default: throw new DataFormatException("Unknown analysis type '" + palsid.toString() + "'.");
        }
    }


    public static String getPurposeIdFromPalsId(Integer palsid)
            throws DataFormatException
    {
        switch (palsid)
        {
            case 1: return "RO";
            case 2: return "PN";
            case 3: return "RW";
            case 4: return "HR";
            case 5: return "RU";
            case 6: return "WF";
            case 7: return "RG";
            case 8: return "TM";
            case 9: return "VM";
            case 10: return "HF";
            case 11: return "WM";
            case 12: return "MG";
            case 13: return "LM";
            case 14: return "LW";
            case 15: return "SU";
            case 16: return "FC";
            case 17: return "RD";
            case 18: return "FR";
            default: throw new DataFormatException("Unknown purpose '" + palsid.toString() + "'.");
        }
    }


    public static String getActivityIdFromPalsId(Integer palsid)
            throws DataFormatException
    {
        switch (palsid)
        {
            case 1: return "RC";
            case 2: return "DC";
            case 3: return "OC";
            case 4: return "CP";
            case 5: return "MP";
            case 6: return "MT";
            case 7: return "DS";
            case 8: return "WD";
            case 9: return "GA";
            case 10: return "TR";
            case 11: return "SC";
            case 12: return "HR";
            case 13: return "SA";
            case 14: return "RA";
            case 15: return "HI";
            case 16: return "PE";
            case 17: return "GP";
            case 18: return "GR";
            case 19: return "SI";
            case 20: return "TS";
            case 21: return "SS";
            case 22: return "NC";
            case 23: return "RV";
            case 24: return "FV";
            case 25: return "NW";
            case 26: return "FN";
            case 27: return "WC";
            case 28: return "ML";
            case 29: return "EC";
            case 30: return "MO";
            case 31: return "AL";
            case 32: return "BL";
            case 33: return "LP";
            case 34: return "PJ";
            case 35: return "LA";
            case 36: return "FI";
            case 37: return "MF";
            case 38: return "RI";
            case 39: return "RD";
            case 40: return "DR";
            case 41: return "RE";
            case 42: return "SL";
            case 43: return "GT";
            case 44: return "WI";
            case 45: return "OL";
            case 46: return "NG";
            case 47: return "BM";
            case 48: return "HP";
            case 49: return "ET";
            default: throw new DataFormatException("Unknown activity '" + palsid.toString() + "'.");
        }
    }


    public static String getStateIdFromPalsId(Integer palsid)
            throws DataFormatException
    {
        switch (palsid)
        {
            case 1: return "AL";
            case 2: return "AK";
            case 4: return "AZ";
            case 5: return "AR";
            case 6: return "CA";
            case 8: return "CO";
            case 9: return "CT";
            case 10: return "DE";
            case 11: return "DC";
            case 12: return "FL";
            case 13: return "GA";
            case 15: return "HI";
            case 16: return "ID";
            case 17: return "IL";
            case 18: return "IN";
            case 19: return "IA";
            case 20: return "KS";
            case 21: return "KY";
            case 22: return "LA";
            case 23: return "ME";
            case 24: return "MD";
            case 25: return "MA";
            case 26: return "MI";
            case 27: return "MN";
            case 28: return "MS";
            case 29: return "MO";
            case 30: return "MT";
            case 31: return "NE";
            case 32: return "NV";
            case 33: return "NH";
            case 34: return "NJ";
            case 35: return "NM";
            case 36: return "NY";
            case 37: return "NC";
            case 38: return "ND";
            case 39: return "OH";
            case 40: return "OK";
            case 41: return "OR";
            case 42: return "PA";
            case 44: return "RI";
            case 45: return "SC";
            case 46: return "SD";
            case 47: return "TN";
            case 48: return "TX";
            case 49: return "UT";
            case 50: return "VT";
            case 51: return "VA";
            case 53: return "WA";
            case 54: return "WV";
            case 55: return "WI";
            case 56: return "WY";
            case 60: return "AS";
            case 64: return "FM";
            case 66: return "GU";
            case 68: return "MH";
            case 69: return "MP";
            case 70: return "PW";
            case 72: return "PR";
            case 74: return "UM";
            case 78: return "VI";
            case 99: return "US";
            default: throw new DataFormatException("Unknown state '" + palsid.toString() + "'.");
        }
    }

    public static String getAppealRuleIdFromPalsId(Integer palsid)
            throws DataFormatException
    {
        switch (palsid)
        {
            case 1: return "None";
            case 2: return "215";
            case 3: return "217";
            case 4: return "218";
            case 5: return "251";
            case 6: return "214";   // added 8/1/2013 for emergency fix
            default: throw new DataFormatException("Unknown Appeal Rule '" + palsid.toString() + "'.");
        }
    }

    public static String getDecisionTypeIdFromPalsId(Integer palsid)
            throws DataFormatException
    {
        if (DecisionTypeMap == null) {
            // Initialize mapping from PALS code to Decision Type ID
            DecisionTypeMap = new HashMap<Integer, String>();
            DecisionTypeClient client = null;
            try {
                client = new DecisionTypeClient();
                Decisiontypes dectypelist = client.getDecisionTypeList();
                for (Decisiontype d : dectypelist.getDecisiontype()) {
                    DecisionTypeMap.put(d.getPalsid(), d.getId());
                }
            } catch (IOException e) {
                throw new DataFormatException("Cannot initialize DecisionType mapping, error initializing DecisionTypeClient: "+e.getMessage());
            }
        }
        
        return (String)DecisionTypeMap.get(palsid);
    }

    

    /*
     *
     * Private methods
     *
     */


    private static String fixDateString(String oldval)
            throws DataFormatException
    {
        String ov = removeGarbage(oldval);
        String fmt = (hasTimePart(ov)) ? DATETIME_FORMAT : DATE_FORMAT;
        
        Date olddate;
        try {
            DateFormat df = new SimpleDateFormat(fmt);
            olddate = df.parse(ov);
        } catch (ParseException ex) {
            throw new DataFormatException("Could not parse date '" + ov + "' using date format " +
                    fmt + ". Original xml looked like this: '" + oldval + "'.");
        }

        return new SimpleDateFormat(fmt).format(olddate);
    }


    private static String removeGarbage(String oldval)
            throws DataFormatException
    {
        String ov = oldval;
        ov = ov.replaceAll("'T'00:00:00", "");
        ov = ov.replaceAll("'T'", "T");
        ov = ov.replaceAll("/", "-");
        ov = fixMissingZeros(ov);
        ov = fixInvertedDate(ov);
        return ov;
    }


    /*
     * Some dates are submitted like this: 06-10-2009
     * We need to make them look like this: 2009-06-10
     */
    private static String fixInvertedDate(String oldval)
            throws DataFormatException
    {

        String datepart = oldval;
        String rest = "";
        if (oldval.length()>10) {
            datepart = oldval.substring(0, 10);
            rest = oldval.substring(10, oldval.length());
        }

        String[] arr = datepart.split("-");
        if (arr.length!=3)
            throw new DataFormatException("Could not invert date '" + oldval + "'.");

        // If the date is indeed inverted, flip it.
        if (arr[2].length()==4 && arr[0].length()==2)
            datepart = arr[2] + "-" + arr[0] + "-" + arr[1];
        
        return datepart + rest;
    }


    /*
     * Leading zeros are sometimes dropped from the timestamps in the xml. Here are some examples:
     * 2010-09-30T4:07:45 (Should be 04:07:45)
     * 2010-09-29T11:19:7
     *
     * This method adds leading zeros where it's appropriate to do so.
     */
    private static String fixMissingZeros(String oldval)
            throws DataFormatException
    {
        if (!hasTimePart(oldval))
            return oldval;

        // Split the date/timestamp into an array {date,time}
        String[] tokens = oldval.split("T");
        if (tokens.length!=2)
            throw new DataFormatException("Could not fix missing zeros in date '" + oldval + "'. There should be two dashes in the date part.");

        String[] timepart = tokens[1].split(":");
        if (timepart.length!=3)
            throw new DataFormatException("Could not fix missing zeros in date '" + oldval + "'. There should be two colons in the time part.");

        // Loop over the time part
        for (int i=0; i<3; i++)
            if (timepart[i].length()==1)
                timepart[i] = "0"+timepart[i];

        return tokens[0]+"T"+timepart[0]+":"+timepart[1]+":"+timepart[2];
    }

    
    private static Boolean hasTimePart(String s)
    {
        return (s.contains(" ") || s.contains("T")) ? true : false;
    }

    public static void setTestMode(Boolean b)
    {
        TEST_MODE = b;
    }
}
