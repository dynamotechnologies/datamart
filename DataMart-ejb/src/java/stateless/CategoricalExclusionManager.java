package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.categoricalexclusion.*;

import beanutil.*;
import java.util.Iterator;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;


@Stateless(mappedName="CategoricalExclusionManagerRemote")
public class CategoricalExclusionManager implements CategoricalExclusionManagerRemote
{
    @PersistenceContext
    private EntityManager em;


    @Override
    public void create(Categoricalexclusion resource)
             throws DataMartException
    {
        entity.RefCategoricalExclusions entity = createEntity(resource);

        // Check for existing entity with this ID
        entity.RefCategoricalExclusions badentity = getEntity(entity.getId());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS,
                    "Categorical Exclusion with id '"+entity.getId()+"' already exists.");

        em.persist(entity);
    }


    @Override
    public Categoricalexclusion get(Integer id)
            throws DataMartException
    {
        entity.RefCategoricalExclusions entity = getEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Categorical Exclusion with id '"+id+"' does not exist.");
        
        return createResource(entity);
    }


    public Categoricalexclusions getAll()
            throws DataMartException
    {
        // Retrieve all entities from the database
        Query query = em.createQuery("SELECT e FROM RefCategoricalExclusions e");
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Categoricalexclusions responselist = new Categoricalexclusions();
        while (it.hasNext()) {
            responselist.getCategoricalexclusion().add(createResource( (entity.RefCategoricalExclusions)it.next() ));
        }
        return responselist;
    }


    @Override
    public void update(Categoricalexclusion resource)
             throws DataMartException
    {
        entity.RefCategoricalExclusions entity = createEntity(resource);

        // Query for the (hopefully) existing entity. Complain if not found.
        entity.RefCategoricalExclusions existingentity = getEntity(entity.getId());
        if (existingentity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND, "Nonexistent Categorical Exclusion cannot be updated.");

        // Set the database id. This is necessary because the ID
        //   is not passed to us by the resource.
        entity.setId(existingentity.getId());

        // Update the entity
        entity = em.merge(entity);
    }

    @Override
    public void delete(Integer id)
             throws DataMartException
    {
        // Verify that this entity exists
        entity.RefCategoricalExclusions entity = getEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Could not delete nonexistent resource with id '"+id+"'.");

        em.remove(entity);
    }

    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     *
     * This method does the mapping from entity object to jaxb object
     */
    protected static Categoricalexclusion createResource(entity.RefCategoricalExclusions entity)
            throws DataMartException
    {
        Categoricalexclusion resource = new Categoricalexclusion();
        resource.setId(entity.getId());
        resource.setName(entity.getName());
        resource.setDescription(entity.getDescription());
        return resource;
    }

    /*
     *
     * This method does the mapping from jaxb object to entity object
     */
    private entity.RefCategoricalExclusions createEntity(Categoricalexclusion resource)
            throws DataMartException
    {
        entity.RefCategoricalExclusions entity = new entity.RefCategoricalExclusions();
        entity.setId(resource.getId());
        entity.setName(resource.getName());
        entity.setDescription(resource.getDescription());
        return entity;
    }


    // Accepts ID parameter, and returns the associated entity,
    //   or null if it doesn't exist
    private entity.RefCategoricalExclusions getEntity(Integer id)
            throws DataMartException
    {
        return em.find(entity.RefCategoricalExclusions.class, id);
    }
}