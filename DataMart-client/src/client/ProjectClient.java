package client;

import java.io.IOException;
import java.util.ArrayList;
import javax.xml.datatype.XMLGregorianCalendar;
import us.fed.fs.www.nepa.schema.project.*;


public class ProjectClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public ProjectClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.project", "Project.xsd");
    }


public void postProject(String id,
        String type,
        String name,
        String unitcode,
        String description,
        String lastupdate,
        String commentreg,
        String wwwlink,
        String wwwsummary,
        String wwwpub,
        String analysistypeid,
        String statusid,
        String contactname,
        String contactphone,
        String contactemail,
        ArrayList<String> purposeidlist,
        String expirationdate,
        Integer projectdocumentid,
        Boolean sopapub,
        Boolean sopanew,
        String sopaheader,
        Boolean esd,
        Boolean cenodecision)
            throws IOException
    {
        Project project = new Project();
        project.setType(type);
        project.setId(id);
        project.setName(name);
        project.setUnitcode(unitcode);
        project.setDescription(description);
        project.setLastupdate(dateTimeStrToXML(lastupdate));
        project.setCommentreg(commentreg);
        project.setWwwlink(wwwlink);
        project.setWwwsummary(wwwsummary);
        project.setWwwpub(wwwpub);
        project.setAddinfopubflag("0");
        project.setMeetingpubflag("0");
        project.setImplinfopubflag("0");
        project.setExpirationdate(dateStrToXML(expirationdate));

        Nepainfo nepainfo = new Nepainfo();
        nepainfo.setAnalysistypeid(analysistypeid);
        nepainfo.setStatusid(statusid);
        nepainfo.setContactname(contactname);
        nepainfo.setContactphone(contactphone);
        nepainfo.setContactemail(contactemail);
        nepainfo.setProjectdocumentid(projectdocumentid);
        nepainfo.setEsd(esd);
        nepainfo.setCenodecision(cenodecision);
        

        Sopainfo sopainfo = new Sopainfo();
        sopainfo.setPublishflag(sopapub);
        sopainfo.setNewflag(sopanew);
        sopainfo.setHeadercategory(sopaheader);

        Purposeids purposeids = new Purposeids();
        purposeids.getPurposeid().addAll(purposeidlist);
        nepainfo.setPurposeids(purposeids);
        nepainfo.setSopainfo(sopainfo);
        project.setNepainfo(nepainfo);

        postProject(project);
    }


    @Deprecated
    public void postProject(String id,
        String type,
        String name,
        String unitcode,
        String description,
        String lastupdate,
        String commentreg,
        String wwwlink,
        String wwwsummary,
        String wwwpub,
        String analysistypeid,
        String statusid,
        String contactname,
        String contactphone,
        String contactemail,
        ArrayList<String> purposeidlist,
        String expirationdate,
        Integer projectdocumentid)
            throws IOException
    {
        postProject(id, type, name, unitcode, description, lastupdate, commentreg, wwwlink, wwwsummary,
                wwwpub, analysistypeid, statusid, contactname, contactphone, contactemail, purposeidlist,
                expirationdate, projectdocumentid, false, false, "", false, false);
    }


    @Deprecated
    public void postProject(String id,
        String type,
        String name,
        String unitcode,
        String description,
        String lastupdate,
        String commentreg,
        String wwwlink,
        String wwwsummary,
        String wwwpub,
        String analysistypeid,
        String statusid,
        String contactname,
        String contactphone,
        String contactemail,
        ArrayList<String> purposeidlist)
            throws IOException
    {
        postProject(id,
                type,
                name,
                unitcode,
                description,
                lastupdate,
                commentreg,
                wwwlink,
                wwwsummary,
                wwwpub,
                analysistypeid,
                statusid,
                contactname,
                contactphone,
                contactemail,
                purposeidlist,
                "1970-1-1",     // expiration date
                null            // projectdocumentid
                );
    }


    public void postProject(Project project)
            throws IOException
    {
        post(objectfactory.createProject(project), "/projects/" + project.getType());
    }


    // TODO: make parameter order consistent
    public Project getProject(String type, String id)
            throws IOException
    {
        return get("/projects/" + type + "/" + id, Project.class);
    }


    public Projects getProjectsByUnit(String id)
            throws IOException
    {
        return get("/units/" + id + "/projects", Projects.class);
    }


    public void putProject(String id,
        String type,
        String name,
        String unitcode,
        String description,
        String lastupdate,
        String commentreg,
        String wwwlink,
        String wwwsummary,
        String wwwpub,
        String analysistypeid,
        String statusid,
        String contactname,
        String contactphone,
        String contactemail,
        ArrayList<String> purposeidlist,
        String expirationdate,
        Integer projectdocumentid,
        Boolean sopapub,
        Boolean sopanew,
        String sopaheader,
        Boolean esd,
        Boolean cenodecision)
            throws IOException
    {
        Project project = new Project();
        project.setType(type);
        project.setId(id);
        project.setName(name);
        project.setUnitcode(unitcode);
        project.setDescription(description);
        project.setLastupdate(dateTimeStrToXML(lastupdate));
        project.setCommentreg(commentreg);
        project.setWwwlink(wwwlink);
        project.setWwwsummary(wwwsummary);
        project.setWwwpub(wwwpub);

        XMLGregorianCalendar exdate = null;
        if (expirationdate != null) {
            exdate = dateStrToXML(expirationdate);
        }
        project.setExpirationdate(exdate);

        Nepainfo nepainfo = new Nepainfo();
        nepainfo.setProjectdocumentid(projectdocumentid);
        nepainfo.setAnalysistypeid(analysistypeid);
        nepainfo.setStatusid(statusid);
        nepainfo.setContactname(contactname);
        nepainfo.setContactphone(contactphone);
        nepainfo.setContactemail(contactemail);
        nepainfo.setEsd(esd);
        nepainfo.setCenodecision(cenodecision);

        Sopainfo sopainfo = new Sopainfo();
        sopainfo.setPublishflag(sopapub);
        sopainfo.setNewflag(sopanew);
        sopainfo.setHeadercategory(sopaheader);

        Purposeids purposeids = new Purposeids();
        purposeids.getPurposeid().addAll(purposeidlist);
        nepainfo.setPurposeids(purposeids);
        nepainfo.setSopainfo(sopainfo);
        project.setNepainfo(nepainfo);

        putProject(project);
    }


    @Deprecated
    public void putProject(String id,
        String type,
        String name,
        String unitcode,
        String description,
        String lastupdate,
        String commentreg,
        String wwwlink,
        String wwwsummary,
        String wwwpub,
        String analysistypeid,
        String statusid,
        String contactname,
        String contactphone,
        String contactemail,
        ArrayList<String> purposeidlist,
        String expirationdate,
        Integer projectdocumentid)
            throws IOException
    {
        putProject(id, type, name, unitcode, description, lastupdate, commentreg, wwwlink, wwwsummary,
                wwwpub, analysistypeid, statusid, contactname, contactphone, contactemail, purposeidlist,
                expirationdate, projectdocumentid, false, false, "", false, false);
    }

    @Deprecated
    public void putProject(String id,
        String type,
        String name,
        String unitcode,
        String description,
        String lastupdate,
        String commentreg,
        String wwwlink,
        String wwwsummary,
        String wwwpub,
        String analysistypeid,
        String statusid,
        String contactname,
        String contactphone,
        String contactemail,
        ArrayList<String> purposeidlist)
            throws IOException
    {
        putProject(id,
                type,
                name,
                unitcode,
                description,
                lastupdate,
                commentreg,
                wwwlink,
                wwwsummary,
                wwwpub,
                analysistypeid,
                statusid,
                contactname,
                contactphone,
                contactemail,
                purposeidlist,
                "1970-1-1",     // expiration date
                null            // projectdocumentid
                );
    }


    public void putProject(Project project)
            throws IOException
    {
        put(objectfactory.createProject(project), "/projects/" + project.getType() + "/" + project.getId());
    }


    // TODO: make parameter order consistent
    public void deleteProject(String type, String id)
            throws IOException
    {
        delete("/projects/" + type + "/" + id);
    }

    public Projectsearchresults findNEPAProject(String name, String id, String unitlist, String statuslist)
            throws IOException
    {
        String url = "/utils/cara/findNEPAProject?";
        if (name.length() > 0) { url = url + "&name=" + name; }
        if (id.length() > 0) { url = url + "&id=" + id; }
        if (unitlist.length() > 0) { url = url + "&unit=" + unitlist; }
        if (statuslist.length() > 0) { url = url + "&status=" + statuslist; }
        return get(url, Projectsearchresults.class);
    }

    public Projectsearchresults getMapProjects(String pubflag, String archiveflag, String rows, String start)
            throws IOException
    {
        String url = "/utils/mapprojects?";
        if (pubflag.length() > 0) { url = url + "&pubflag=" + pubflag; }
        if (archiveflag.length() > 0) { url = url + "&archiveflag=" + archiveflag; }
        if (rows.length() > 0) { url = url + "&rows=" + rows; }
        if (start.length() > 0) { url = url + "&start=" + start; }
        return get(url, Projectsearchresults.class);
    }
}