package stateless;

import javax.ejb.Stateless;
import beanutil.*;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import us.fed.fs.www.nepa.schema.usersession.*;


/**
 *
 * @author mhsu
 */
@Stateless(mappedName="UserSessionManagerRemote")
public class UserSessionManager implements UserSessionManagerRemote {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Usersession create(Usersession resource)
             throws DataMartException
    {
        validate(resource);
        entity.UserSession entity = createEntity(resource);

        entity.UserSession badentity = getEntity(entity.getToken());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        entity.setCreated(new java.util.Date() );
        entity.setLastActivity(new java.util.Date() );
        
        entity = em.merge(entity);
        return createResource(entity);
    }

    @Override
    public Usersessions getAll()
             throws DataMartException
    {
        // Retrieve all entities from the database
        Query query = em.createQuery("SELECT u FROM UserSession u");
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Usersessions responselist = new Usersessions();
        while (it.hasNext()) {
            responselist.getUsersession().add(createResource( (entity.UserSession)it.next() ));
        }
        return responselist;
    }

    @Override
    public Usersession get(String token)
            throws DataMartException
    {
        entity.UserSession entity = getEntity(token);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return createResource(entity);
    }

    @Override
    public void update(Usersession resource)
             throws DataMartException
    {
        validate(resource);
        
        entity.UserSession existingEntity = getEntity(resource.getToken());
        if (existingEntity == null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        
        //All update does is to reset activity timestampe
        existingEntity.setLastActivity(new java.util.Date() );
        existingEntity = em.merge(existingEntity);
    }

    @Override
    public void delete(String token)
             throws DataMartException
    {
        entity.UserSession entity = getEntity(token);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     * This method does the mapping from jaxb object to entity object and vice versa
     */
    private Usersession createResource(entity.UserSession entity)
            throws DataMartException
    {
        Usersession resource = new Usersession();
        resource.setToken(entity.getToken());
        resource.setUserid(entity.getUserId().getId());
        resource.setProjectid(entity.getProjectId().getPublicId());
        resource.setProjectaccesslevel(entity.getProjectAccessLevel());
        if (entity.getCreated() != null) {
            resource.setCreated(DataMartBeanUtil.getXMLGregorianFromDateTime( entity.getCreated() ));
        }
        if (entity.getLastActivity() != null) {
            resource.setLastactivity(DataMartBeanUtil.getXMLGregorianFromDateTime( entity.getLastActivity()));
        }

        return resource;
    }

    private entity.UserSession createEntity(Usersession resource)
            throws DataMartException
    {
        entity.UserSession userSession = new entity.UserSession();

        String token = resource.getToken();
        if (token == null || token.equals(""))
        {
            token = UUID.randomUUID().toString();
        }

        String access = resource.getProjectaccesslevel();
        if (access == null || access.equals(""))
        {
            access = "write";
        }

        if (!(access.equals("read") || access.equals("write")))
        {
            throw new DataMartException(DataMartException.UNKNOWN_ERROR, "Incorrect project access level provided : " + access);
        }

        userSession.setToken(token);
        
        entity.Users theUser= (entity.Users) DataMartBeanUtil.getEntityById(em, 
                "Users", resource.getUserid(), "User");
        
        entity.Projects project = DataMartBeanUtil.getProject(em, "nepa", resource.getProjectid());

                //unit = (entity.RefUnits) DataMartBeanUtil.getEntityById(em,
                //"RefUnits", resource.getUnitcode(), "Unit Code");
        userSession.setUserId(theUser);
        userSession.setProjectId(project);
        userSession.setProjectAccessLevel(access);
        
    //    if (resource.getCreated() != null) {
    //        userSession.setCreated(DataMartBeanUtil.getDateFromXMLGregorian(resource.getCreated()));
    //    }
        if (resource.getLastactivity() != null) {
            userSession.setLastActivity(DataMartBeanUtil.getDateFromXMLGregorian(resource.getLastactivity()));
        }else{
            userSession.setLastActivity(new java.util.Date() );
        }
        
        return userSession;
    }

    // Accepts a String ID as a parameter, and returns the
    //   associated entity, or null if it doesn't exist
    //
    private entity.UserSession getEntity(String token)
            throws DataMartException
    {
        return em.find(entity.UserSession.class, token);
    }

    private void validate(Usersession resource)
            throws DataMartException
    {
        // No enforceable validations known at this time
        // DataMartBeanUtil.confirmNotNull(resource.getLastname(), "Last Name");
    }

}