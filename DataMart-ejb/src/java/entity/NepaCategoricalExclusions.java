package entity;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "NepaCategoricalExclusions")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NepaCategoricalExclusions.findAll", query = "SELECT n FROM NepaCategoricalExclusions n"),
    @NamedQuery(name = "NepaCategoricalExclusions.findById", query = "SELECT n FROM NepaCategoricalExclusions n WHERE n.id = :id")})
public class NepaCategoricalExclusions implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @JoinColumn(name = "ProjectId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Projects projects;
    @JoinColumn(name = "CategoricalExclusionId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private RefCategoricalExclusions refCategoricalExclusions;

    public NepaCategoricalExclusions() {
    }

    public NepaCategoricalExclusions(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Projects getProjects() {
        return projects;
    }

    public void setProjects(Projects projects) {
        this.projects = projects;
    }

    public RefCategoricalExclusions getRefCategoricalExclusions() {
        return refCategoricalExclusions;
    }

    public void setRefCategoricalExclusions(RefCategoricalExclusions refCategoricalExclusions) {
        this.refCategoricalExclusions = refCategoricalExclusions;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NepaCategoricalExclusions)) {
            return false;
        }
        NepaCategoricalExclusions other = (NepaCategoricalExclusions) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.NepaCategoricalExclusions[ id=" + id + " ]";
    }

}
