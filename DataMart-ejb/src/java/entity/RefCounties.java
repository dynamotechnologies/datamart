package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "RefCounties")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RefCounties.findAll", query = "SELECT r FROM RefCounties r"),
    @NamedQuery(name = "RefCounties.findById", query = "SELECT r FROM RefCounties r WHERE r.id = :id"),
    @NamedQuery(name = "RefCounties.findByName", query = "SELECT r FROM RefCounties r WHERE r.name = :name")})
public class RefCounties implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private String id;
    @Basic(optional = false)
    @Column(name = "Name")
    private String name;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refCounties")
    private Collection<ProjectCounties> projectCountiesCollection;

    public RefCounties() {
    }

    public RefCounties(String id) {
        this.id = id;
    }

    public RefCounties(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Collection<ProjectCounties> getProjectCountiesCollection() {
        return projectCountiesCollection;
    }

    public void setProjectCountiesCollection(Collection<ProjectCounties> projectCountiesCollection) {
        this.projectCountiesCollection = projectCountiesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RefCounties)) {
            return false;
        }
        RefCounties other = (RefCounties) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RefCounties[ id=" + id + " ]";
    }

}
