package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.goal.*;


public class GoalClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public GoalClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.goal", "Goal.xsd");
    }


    public void postGoal(Integer id, String description)
            throws IOException
    {
        Goal goal = new Goal();
        goal.setGoalid(id);
        goal.setDescription(description);
        postGoal(goal);
    }


    public void postGoal(Goal goal)
            throws IOException
    {
        post(objectfactory.createGoal(goal), "/goals");
    }


    public Goal getGoal(Integer id)
            throws IOException
    {
        return get("/goals/" + id, Goal.class);
    }

/* These list APIs don't exist yet
    public Goals getGoalsByProject(String projecttype, String projectid)
            throws IOException
    {
        return get("/goals/project/"+projecttype+"/"+projectid, Goals.class);
    }
    
    public Goals getGoalsByUnit(String unitid)
            throws IOException
    {
        return get("/goals/unit/"+unitid, Goals.class);
    }
*/

    public void putGoal(Integer id, String description)
            throws IOException
    {
        Goal goal = new Goal();
        goal.setGoalid(id);
        goal.setDescription(description);
        putGoal(goal);
    }


    public void putGoal(Goal goal)
            throws IOException
    {
        put(objectfactory.createGoal(goal), "/goals/" + goal.getGoalid());
    }


    public void deleteGoal(Integer id)
            throws IOException
    {
        delete("/goals/" + id);
    }

}
