package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.documentsensitivityrationale.*;

import beanutil.*;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Iterator;
import java.util.List;
import javax.persistence.Query;

@Stateless(mappedName="DocumentSensitivityRationaleManagerRemote")
public class DocumentSensitivityRationaleManager implements DocumentSensitivityRationaleManagerRemote
{
    @PersistenceContext
    private EntityManager em;


    @Override
    public void create(Documentsensitivityrationale resource)
             throws DataMartException
    {
        entity.RefDocumentSensitivityRationales entity = createEntity(resource);

        // Check for existing entity with this ID
        if (getEntity(entity.getId()) != null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS,
                    "DocumentSensitivityRationale with id '"+entity.getId()+"' already exists.");

        em.persist(entity);
    }


    @Override
    public void update(Documentsensitivityrationale resource)
             throws DataMartException
    {
        entity.RefDocumentSensitivityRationales entity = createEntity(resource);

        // Query for the (hopefully) existing entity. Complain if not found.
        if (getEntity(entity.getId())==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND, "Nonexistent DocumentSensitivityRationale cannot be updated.");

        // Update the entity
        entity = em.merge(entity);
    }


    @Override
    public Documentsensitivityrationale get(Integer id)
            throws DataMartException
    {
        entity.RefDocumentSensitivityRationales entity = getEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "DocumentSensitivityRationale with id '"+id+"' does not exist.");

        return createResource(entity);
    }


    @Override
    public Documentsensitivityrationales getAll()
            throws DataMartException
    {
        // Retrieve all entities from the database
        Query query = em.createQuery("SELECT r FROM RefDocumentSensitivityRationales r");
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Documentsensitivityrationales responselist = new Documentsensitivityrationales();
        while (it.hasNext()) {
            responselist.getDocumentsensitivityrationale().add(createResource( (entity.RefDocumentSensitivityRationales)it.next() ));
        }
        return responselist;

    }


    @Override
    public void delete(Integer id)
             throws DataMartException
    {
        // Verify that this entity exists
        entity.RefDocumentSensitivityRationales entity = getEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Could not delete nonexistent DocumentSensitivityRationale with id '"+id+"'.");

        em.remove(entity);
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     *
     * This method does the mapping from entity object to jaxb object
     */
    protected static Documentsensitivityrationale createResource(entity.RefDocumentSensitivityRationales entity)
            throws DataMartException
    {
        Documentsensitivityrationale resource = new Documentsensitivityrationale();
        resource.setId(entity.getId());
        resource.setSensitivityid(entity.getSensitivityId());
        resource.setDescription(entity.getDescription());
        resource.setDisplayorder(entity.getDisplayOrder());
        return resource;
    }


    /*
     *
     * This method does the mapping from jaxb object to entity object
     */
    private entity.RefDocumentSensitivityRationales createEntity(Documentsensitivityrationale resource)
            throws DataMartException
    {
        entity.RefDocumentSensitivityRationales entity = new entity.RefDocumentSensitivityRationales();
        entity.setId(resource.getId());
        entity.setSensitivityId(resource.getSensitivityid());
        entity.setDescription(resource.getDescription());
        entity.setDisplayOrder(resource.getDisplayorder());
        return entity;
    }


    // Accepts ID parameter, and returns the associated entity,
    //   or null if it doesn't exist
    private entity.RefDocumentSensitivityRationales getEntity(Integer id)
            throws DataMartException
    {
        return em.find(entity.RefDocumentSensitivityRationales.class, id);
    }
}