/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateless;

import beanutil.DataMartBeanUtil;
import beanutil.DataMartException;
import java.sql.Date;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import us.fed.fs.www.nepa.schema.ProjectView.ProjectView;
import us.fed.fs.www.nepa.schema.ProjectView.ProjectViews;
import us.fed.fs.www.nepa.schema.user.Users;

/**
 *
 * @author gauri
 */
@Stateless(mappedName="ProjectViewManagerRemote")
public class ProjectViewManager implements ProjectViewManagerRemote {
     @PersistenceContext
     private EntityManager em;

    @Override
    public void create(ProjectView resource) throws DataMartException {
        entity.ProjectViews entity = createEntity(resource);

        

        entity = em.merge(entity); 
    }

    @Override
    public ProjectView get(Integer viewId) throws DataMartException {
       entity.ProjectViews entity = createEntity(viewId);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return createResource(entity);  
    }

    @Override
    public ProjectViews getAllByProject(String projecttype, String projectid) throws DataMartException {
        // Get Project
        entity.Projects project = DataMartBeanUtil.getProject(em, projecttype, projectid);

        // Retrieve project resource area entities from the database
        Query query = em.createQuery("SELECT a FROM ProjectViews a WHERE a.projectId = :project");
        query.setParameter("project", project);
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        ProjectViews responselist = new ProjectViews();
        while (it.hasNext()) {
            responselist.getProjectView().add(createResource((entity.ProjectViews)it.next()));
        }
       
        return responselist;
    }
    @Override 
    public ProjectViews getAllByProjectByUser(String projecttype, String projectid, String userId) throws DataMartException {
        // Get Project
        entity.Projects project = DataMartBeanUtil.getProject(em, projecttype, projectid);

        // Retrieve project resource area entities from the database
        Query query = em.createQuery("SELECT a FROM ProjectViews a WHERE a.projectId = :project and a.userId.id=:userid");
        query.setParameter("project", project);
        query.setParameter("userid", userId);
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        ProjectViews responselist = new ProjectViews();
        while (it.hasNext()) {
            responselist.getProjectView().add(createResource((entity.ProjectViews)it.next()));
        }
       
        return responselist;
    }
    @Override
    public void update(ProjectView resource) throws DataMartException {
       entity.ProjectViews entity = createEntity(resource);

        entity.ProjectViews existingentity = createEntity(resource.getId());

        if (existingentity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        entity.setViewId(existingentity.getViewId());
        entity = em.merge(entity); 
    }

    @Override
    public void delete(Integer id) throws DataMartException {
        entity.ProjectViews entity = createEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity)); 
    }

    @Override
    public Boolean ping() {
 return true;
    }
        /*
     *
     * This method does the mapping from jaxb object to entity object and vice versa
     */
    private ProjectView createResource(entity.ProjectViews entity)
            throws DataMartException
    {
        ProjectView resource = new ProjectView();
        resource.setId(entity.getViewId());
        resource.setProjectId(new Integer(entity.getProjectId().getPublicId()));
        resource.setName(entity.getViewName());
        resource.setUserId(entity.getUserId().getId());
        Calendar cal = Calendar.getInstance();
        cal.setTime(entity.getCreatedDate());
        String formatedDate = fill('0',2,""+(cal.get(Calendar.MONTH) + 1) )+ "/" + cal.get(Calendar.DATE) + "/" +cal.get(Calendar.YEAR);
        resource.setCreateDate(formatedDate);      

        return resource;
    }

    private entity.ProjectViews createEntity(ProjectView resource)
            throws DataMartException
    {
        entity.ProjectViews entity = new entity.ProjectViews();
        entity.Projects project = DataMartBeanUtil.getProject(em, "nepa", ""+resource.getProjectId());
        Query query = em.createQuery("SELECT u FROM Users u where u.id=:id");
        query.setParameter("id", resource.getUserId());
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        entity.Users user = new entity.Users();
        while (it.hasNext()) {
            user =(entity.Users)it.next();
        }
        entity.setUserId(user);
        entity.setViewName(resource.getName());
        entity.setCreatedDate(Date.valueOf(resource.getCreateDate()));
        entity.setProjectId(project);        
        return entity;
    }

    private entity.ProjectViews createEntity(Integer key)
            throws DataMartException
    {
        return em.find(entity.ProjectViews.class, key);
    }
     public static final String fill( char fillChar,
                                     int fieldLength, String value) 
    {
        StringBuffer buffer = new StringBuffer();
        int length = value.length();
      
        for (int i = 0; i < (fieldLength - length); i++)
        {
            buffer.append(fillChar);
        }
        buffer.append(value);
        return buffer.toString();
    }
}

