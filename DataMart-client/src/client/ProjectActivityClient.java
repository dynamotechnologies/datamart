package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.projectactivity.*;


public class ProjectActivityClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public ProjectActivityClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.projectactivity", "ProjectActivity.xsd");
    }


    public Projectactivity getProjectActivity(String projectid, String id)
            throws IOException
    {
        return get("/projects/nepa/" + projectid + "/activities/" + id, Projectactivity.class);
    }


    public Projectactivities getProjectActivityList(String projectid)
            throws IOException
    {
        return get("/projects/nepa/" + projectid + "/activities/", Projectactivities.class);
    }


    public void putProjectActivity(String projectid, String id)
            throws IOException
    {
        Projectactivity projectactivity = new Projectactivity();
        projectactivity.setProjectid(projectid);
        projectactivity.setActivityid(id);
        put(objectfactory.createProjectactivity(projectactivity),
                "/projects/nepa/" + projectid + "/activities/" + id);
    }


    public void replaceProjectActivityList(Projectactivities activities)
            throws IOException
    {
        put(objectfactory.createProjectactivities(activities),
                "/projects/nepa/" + activities.getProjectactivity().get(0).getProjectid() + "/activities");
    }
    

    public void deleteProjectActivity(String projectid, String id)
            throws IOException
    {
        delete("/projects/nepa/" + projectid + "/activities/" + id);
    }
}