package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.documentsensitivityrationale.*;


public class DocumentSensitivityRationaleClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public DocumentSensitivityRationaleClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.documentsensitivityrationale", "DocumentSensitivityRationale.xsd");
    }


    public void postDocumentSensitivityRationale(int id, int sensitivity, String desc, int order)
            throws IOException
    {
        Documentsensitivityrationale rationale = new Documentsensitivityrationale();
        rationale.setId(id);
        rationale.setSensitivityid(sensitivity);
        rationale.setDescription(desc);
        rationale.setDisplayorder(order);
        post(objectfactory.createDocumentsensitivityrationale(rationale), "/ref/documentsensitivityrationales");
    }


    public void postDocumentSensitivityRationale(Documentsensitivityrationale rationale)
            throws IOException
    {
        post(objectfactory.createDocumentsensitivityrationale(rationale), "/ref/documentsensitivityrationales");
    }


    public Documentsensitivityrationale getDocumentSensitivityRationale(String id)
            throws IOException
    {
        return get("/ref/documentsensitivityrationales/" + id, Documentsensitivityrationale.class);
    }


    public Documentsensitivityrationales getDocumentSensitivityRationaleList()
            throws IOException
    {
        return get("/ref/documentsensitivityrationales", Documentsensitivityrationales.class);
    }


    public void putDocumentSensitivityRationale(Documentsensitivityrationale rationale)
            throws IOException
    {
        put(objectfactory.createDocumentsensitivityrationale(rationale), "/ref/documentsensitivityrationales/" + rationale.getId());
    }


    public void deleteDocumentSensitivityRationale(String id)
            throws IOException
    {
        delete("/ref/documentsensitivityrationales/" + id);
    }
}