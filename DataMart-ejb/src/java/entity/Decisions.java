package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "Decisions")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Decisions.findAll", query = "SELECT d FROM Decisions d"),
    @NamedQuery(name = "Decisions.findById", query = "SELECT d FROM Decisions d WHERE d.id = :id"),
    @NamedQuery(name = "Decisions.findByName", query = "SELECT d FROM Decisions d WHERE d.name = :name"),
    @NamedQuery(name = "Decisions.findByAppealStatus", query = "SELECT d FROM Decisions d WHERE d.appealStatus = :appealStatus"),
    @NamedQuery(name = "Decisions.findByDecConstraint", query = "SELECT d FROM Decisions d WHERE d.decConstraint = :decConstraint"),
    @NamedQuery(name = "Decisions.findByLegalNoticeDate", query = "SELECT d FROM Decisions d WHERE d.legalNoticeDate = :legalNoticeDate"),
    @NamedQuery(name = "Decisions.findByAreaSize", query = "SELECT d FROM Decisions d WHERE d.areaSize = :areaSize"),
    @NamedQuery(name = "Decisions.findByAreaUnits", query = "SELECT d FROM Decisions d WHERE d.areaUnits = :areaUnits")})
public class Decisions implements Serializable {
    @Column(name =     "LegalNoticeDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date legalNoticeDate;
    @Column(name =     "DecisionDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date decisionDate;
    @JoinColumn(name = "DecisionType", referencedColumnName = "Id")
    @ManyToOne
    private RefDecisionTypes refDecisionTypes;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Column(name = "Name")
    private String name;
    @Column(name = "AppealStatus")
    private String appealStatus;
    @Column(name = "DecConstraint")
    private String decConstraint;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "AreaSize")
    private Double areaSize;
    @Column(name = "AreaUnits")
    private String areaUnits;
    @JoinColumn(name = "ProjectId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Projects projects;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "decisions")
    private Collection<Litigations> litigationsCollection;
    @OneToMany(mappedBy = "decisions")
    private Collection<DecisionAppealRules> decisionAppealRulesCollection;
    @OneToMany(mappedBy = "decisions")
    private Collection<Appeals> appealsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "decisions")
    private Collection<DecisionMakers> decisionMakersCollection;

    public Decisions() {
    }

    public Decisions(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAppealStatus() {
        return appealStatus;
    }

    public void setAppealStatus(String appealStatus) {
        this.appealStatus = appealStatus;
    }

    public String getDecConstraint() {
        return decConstraint;
    }

    public void setDecConstraint(String decConstraint) {
        this.decConstraint = decConstraint;
    }

    public Double getAreaSize() {
        return areaSize;
    }

    public void setAreaSize(Double areaSize) {
        this.areaSize = areaSize;
    }

    public String getAreaUnits() {
        return areaUnits;
    }

    public void setAreaUnits(String areaUnits) {
        this.areaUnits = areaUnits;
    }

    public Projects getProjects() {
        return projects;
    }

    public void setProjects(Projects projects) {
        this.projects = projects;
    }

    @XmlTransient
    public Collection<Litigations> getLitigationsCollection() {
        return litigationsCollection;
    }

    public void setLitigationsCollection(Collection<Litigations> litigationsCollection) {
        this.litigationsCollection = litigationsCollection;
    }

    @XmlTransient
    public Collection<DecisionAppealRules> getDecisionAppealRulesCollection() {
        return decisionAppealRulesCollection;
    }

    public void setDecisionAppealRulesCollection(Collection<DecisionAppealRules> decisionAppealRulesCollection) {
        this.decisionAppealRulesCollection = decisionAppealRulesCollection;
    }

    @XmlTransient
    public Collection<Appeals> getAppealsCollection() {
        return appealsCollection;
    }

    public void setAppealsCollection(Collection<Appeals> appealsCollection) {
        this.appealsCollection = appealsCollection;
    }

    @XmlTransient
    public Collection<DecisionMakers> getDecisionMakersCollection() {
        return decisionMakersCollection;
    }

    public void setDecisionMakersCollection(Collection<DecisionMakers> decisionMakersCollection) {
        this.decisionMakersCollection = decisionMakersCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Decisions)) {
            return false;
        }
        Decisions other = (Decisions) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Decisions[ id=" + id + " ]";
    }

    public RefDecisionTypes getRefDecisionTypes() {
        return refDecisionTypes;
    }

    public void setRefDecisionTypes(RefDecisionTypes refDecisionTypes) {
        this.refDecisionTypes = refDecisionTypes;
    }

    public Date getLegalNoticeDate() {
        return legalNoticeDate;
    }

    public void setLegalNoticeDate(Date legalNoticeDate) {
        this.legalNoticeDate = legalNoticeDate;
    }

    public Date getDecisionDate() {
        return decisionDate;
    }

    public void setDecisionDate(Date decisionDate) {
        this.decisionDate = decisionDate;
    }

}
