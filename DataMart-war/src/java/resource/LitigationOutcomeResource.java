package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.LitigationOutcomeManagerRemote;
import us.fed.fs.www.nepa.schema.litigationoutcome.Litigationoutcome;
import us.fed.fs.www.nepa.schema.litigationoutcome.Litigationoutcomes;
import us.fed.fs.www.nepa.schema.litigationoutcome.ObjectFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;


@Path ("/ref/litigationoutcomes")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class LitigationOutcomeResource extends AbstractResource
{
    private LitigationOutcomeManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public LitigationOutcomeResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "LitigationOutcome.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.litigationoutcome");
        MY_PROPS.setProperty("bean", "LitigationOutcomeManagerRemote");
        try {
            beanmanager = (LitigationOutcomeManagerRemote) init(LitigationOutcomeManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("LitigationOutcomeResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("LitigationOutcomeResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;
        Litigationoutcome outcome;
        try {
            outcome = (Litigationoutcome)unmarshal(inputxml, Litigationoutcome.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+outcome.getId());

            // All clear. Now do the insert.
            beanmanager.create(outcome);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the newly created entity
        return created(uri);
    }


    @GET
    public Response get(@Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get a dump of all the entities in the datamart
            Litigationoutcomes outcomelist = beanmanager.getAll();

            // Return 404 if none exist
            if (outcomelist.getLitigationoutcome().isEmpty())
                notFound();

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Litigationoutcomes> jaxbelement = objectfactory.createLitigationoutcomes(outcomelist);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @GET
    @Path("/{id}")
    public Response get(@PathParam("id") int id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the name of the specified entity
            Litigationoutcome outcome = beanmanager.get(id);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Litigationoutcome> jaxbelement = objectfactory.createLitigationoutcome(outcome);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @PUT
    @Path("/{id}")
    public Response put(@PathParam("id") int id, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        Litigationoutcome outcome = null;
        try {
            outcome = (Litigationoutcome) unmarshal(inputxml, Litigationoutcome.class);

            if (id != outcome.getId())
                complainLoudly(new Exception("Id in uri must match id in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.update(outcome);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"/"+outcome.getId()+"</location>");
    }


    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") int id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(id);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}