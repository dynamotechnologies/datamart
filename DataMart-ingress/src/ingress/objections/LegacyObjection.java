package ingress.objections;

import java.util.zip.DataFormatException;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 * This class encapsulates xml ingressed from files validated with the following DTD:
 *
 * <!DOCTYPE objections [
 * <!ELEMENT objections (timestamp?, objection+)>
 * <!ATTLIST objections xmlns CDATA "http://www.fs.fed.us/pals/xmlns/objections/">
 * <!ELEMENT timestamp (#PCDATA)>
 * <!ELEMENT objection (id?,code,rule,objector,project,response)>
 * <!ELEMENT id (#PCDATA)>
 * <!ELEMENT code (#PCDATA)>
 * <!ELEMENT rule (#PCDATA)>
 * <!ELEMENT objector (#PCDATA)>
 * <!ELEMENT project (name,adminunit)>
 * <!ELEMENT name (#PCDATA)>
 * <!ELEMENT adminunit (#PCDATA)>
 * <!ELEMENT response (date,documentid,filesize)>
 * <!ELEMENT date (#PCDATA)>
 * <!ELEMENT documentid (#PCDATA)>
 * <!ELEMENT filesize (#PCDATA)>
 * ]>
 *
 *
 * This class is responsible for encapsulating element `objection` as specified above
 */
public class LegacyObjection {
    private String id = "";
    private String projectid = ""; // Optional
    private String code;
    private String rule;
    private String objector;
    private LegacyObjectionProject project;
    private LegacyObjectionResponse response;

    public LegacyObjection(Node n)
            throws DataFormatException
    {
        NodeList mynodes = n.getChildNodes();
        for (int i=0; i<mynodes.getLength(); i++)
        {
            if (mynodes.item(i).getNodeName().equals("id"))
                id = mynodes.item(i).getTextContent();
            if (mynodes.item(i).getNodeName().equals("projectid"))
                projectid = mynodes.item(i).getTextContent();
            if (mynodes.item(i).getNodeName().equals("code"))
                code = mynodes.item(i).getTextContent();
            if (mynodes.item(i).getNodeName().equals("rule"))
                rule = mynodes.item(i).getTextContent();
            if (mynodes.item(i).getNodeName().equals("objector"))
                objector = mynodes.item(i).getTextContent();
            if (mynodes.item(i).getNodeName().equals("project"))
                project = new LegacyObjectionProject(mynodes.item(i));
            if (mynodes.item(i).getNodeName().equals("response"))
                response = new LegacyObjectionResponse(mynodes.item(i));
        }
        if (!(id.length()>0))
            throw new DataFormatException("id not found.");
        if (!(code.length()>0))
            throw new DataFormatException("code not found.");
        if (!(rule.length()>0))
            throw new DataFormatException("rule not found.");
        if (!(objector.length()>0))
            throw new DataFormatException("objector not found.");
        if (project==null)
            throw new DataFormatException("project not found.");
        if (response==null)
            throw new DataFormatException("response not found.");
    }

    public String getId()
    {
        return id;
    }

    public String getProjectId()
    {
        return projectid;
    }

    public String getCode()
    {
        return code;
    }

    public String getRule()
    {
        return rule;
    }

    public String getObjector()
    {
        return objector;
    }

    public LegacyObjectionProject getProject()
    {
        return project;
    }

    public LegacyObjectionResponse getResponse()
    {
        return response;
    }
}