alter table RefUnits add column SpotlightId1 varchar(255);
alter table RefUnits add column SpotlightId2 varchar(255);
alter table RefUnits add column SpotlightId3 varchar(255);


CREATE TABLE CustomProjects (
  Id int(11) NOT NULL,	
  Name varchar(255) NOT NULL,
  Type varchar(255),
  URL varchar(255),
  IsPublished tinyint(1),
  ArchiveDate DATETIME DEFAULT NULL,
  Description varchar(4000),
  StatusId int(11),
  UnitId int(11),
  CreatedBy varchar(255),
  CreatedDate DATETIME DEFAULT NULL,
  CONSTRAINT PkCustomProjects_Id PRIMARY KEY (Id)
) ENGINE=INNODB;

-- proj_purp_list
-- joins table CustomProjects with table RefPurposes
CREATE TABLE CustomProjectPurposes (
  Id int(11) NOT NULL AUTO_INCREMENT,
  CustomProjectId int(11) NOT NULL,
  PurposeId varchar(2) NOT NULL,
  CONSTRAINT PkCustomProjectPurposes_Id PRIMARY KEY (Id),
  CONSTRAINT FkCustomProjectPurposes_CustomProjectId FOREIGN KEY (CustomProjectId) REFERENCES CustomProjects (Id),
  CONSTRAINT FkCustomProjectPurposes_PurposeId FOREIGN KEY (PurposeId) REFERENCES RefPurposes (Id)
) ENGINE=INNODB;

-- Categorical Exclusion No Decision (cenodecision) flag for Projects
ALTER TABLE Projects ADD COLUMN Cenodecision tinyint(1) NOT NULL DEFAULT 0;

-- Emergency Situation Determination (ESD) flag for Projects
ALTER TABLE Projects ADD COLUMN Esd tinyint(1) NOT NULL DEFAULT 0;

-- Publication Timestamp for Documents
ALTER TABLE ProjectDocuments ADD COLUMN DatePublished datetime;
