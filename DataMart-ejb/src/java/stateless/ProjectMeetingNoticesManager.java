/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.


import beanutil.*;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import us.fed.fs.www.nepa.schema.ProjectMeetingNotices.Meetingnotice;
import us.fed.fs.www.nepa.schema.ProjectMeetingNotices.Meetingnotices;

@Stateless(mappedName="ProjectMeetingNoticesManagerRemote")


public class ProjectMeetingNoticesManager implements ProjectMeetingNoticesManagerRemote {
    @PersistenceContext
    private EntityManager em;
    @Override
    public void create(Meetingnotice resource) throws DataMartException {
        entity.ProjectMeetingNotices entity = createEntity(resource);

        entity.ProjectMeetingNotices badentity = retrieveEntity("nepa", resource.getDocumentId(), new Integer(resource.getMeetingId()));
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        entity = em.merge(entity);
    }

    @Override
    public Meetingnotice get(String projecttype, String projectid, Integer meetingId) throws DataMartException {
         entity.ProjectMeetingNotices entity = retrieveEntity(projecttype, projectid, meetingId);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return createResource(entity);
    }

    @Override
    public Meetingnotices getAllByProject(String projecttype, String projectid) throws DataMartException {
    entity.Projects project = DataMartBeanUtil.getProject(em, projecttype, projectid);

        // Retrieve project resource area entities from the database
        
        Query query = em.createQuery("SELECT a FROM ProjectMeetingNotices a WHERE a.projectId = :project ORDER BY  a.meetingOrder");
        query.setParameter("project", project);
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Meetingnotices responselist = new Meetingnotices();
        while (it.hasNext()) {
            responselist.getMeetingnotice().add(createResource((entity.ProjectMeetingNotices )it.next()));
        }
        return responselist;    }

    @Override
    public void update(Meetingnotice resource) throws DataMartException {
   entity.ProjectMeetingNotices entity = createEntity(resource);

        entity.ProjectMeetingNotices existingentity = retrieveEntity("nepa", resource.getDocumentId(), new Integer(resource.getMeetingId()));

        if (existingentity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        entity.setMeetingId(existingentity.getMeetingId());
        entity = em.merge(entity);    }

    @Override
    public void delete(String projecttype, String projectid, Integer meetingId) throws DataMartException {
          entity.ProjectMeetingNotices entity = retrieveEntity(projecttype, projectid, meetingId);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));
    }

    @Override
    public Boolean ping() {
     return true;
    }
   /*
     * Create XML from EJB
     */
    private Meetingnotice createResource(entity.ProjectMeetingNotices entity)
            throws DataMartException
    {
        Meetingnotice resource = new Meetingnotice();
    resource.setCity			(entity.getCity()) ;
    resource.setDirections		(entity.getDirections		()) ;
    resource.setDocumentId		(entity.getProjectId().getPublicId()) ;
   
        Calendar cal = Calendar.getInstance();
        cal.setTime(entity.getLastUpdatedDate());
        String formatedDate = fill('0',2,""+(cal.get(Calendar.MONTH) + 1) )+ "/" + cal.get(Calendar.DATE) + "/" +cal.get(Calendar.YEAR);
        resource.setLastUpdateDate(formatedDate);		
       
       resource.setLastUpdatedBy		(entity.getLastUpdatedBy	()	) ;  
       cal.setTime(entity.getMeetingDate());
       formatedDate =fill('0',2,""+(cal.get(Calendar.MONTH) + 1) )  + "/" +cal.get(Calendar.DATE)  + "/" +cal.get(Calendar.YEAR);
       resource.setMeetingDate(formatedDate);
    
    resource.setMeetingDescription	(entity.getMeetingDescription	()) ;
    resource.setMeetingEndTime		(entity.getMeetingEndTime	()	) ;
    resource.setMeetingId		(entity.getMeetingId().toString()) ;
    resource.setMeetingOrder		(entity.getMeetingOrder().toString()) ;
    resource.setMeetingPubFlag		(entity.getMeetingPubFlag	()	) ;
    resource.setMeetingStartTime	(entity.getMeetingStartTime	()) ;
    resource.setMeetingTitle		(entity.getMeetingTitle		()) ;
    resource.setState			(entity.getState		()	) ;
    resource.setStreet1			(entity.getStreet1		()	) ;
    resource.setStreet2			(entity.getStreet2		()	) ;
    resource.setZip			(entity.getZip			()) ;
        return resource;
    }

    /*
     * Create EJB from XML
     */
    private entity.ProjectMeetingNotices createEntity(Meetingnotice resource)
            throws DataMartException
    {
        
        String projectid = resource.getDocumentId();
        entity.Projects project = DataMartBeanUtil.getProject(em, "nepa", projectid);
        if (project == null) {
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION, "Project " + "nepa" + "/" + projectid + " does not exist");
        }

       

        entity.ProjectMeetingNotices entity = new entity.ProjectMeetingNotices();
        entity.setProjectId(project);
           entity.setCity			(resource.getCity()) ;
    entity.setDirections		(resource.getDirections		()) ;
    entity.setLastUpdatedDate(Date.valueOf(resource.getLastUpdateDate())) ;
    entity.setLastUpdatedBy		(resource.getLastUpdatedBy	()	) ;
    entity.setMeetingDate		(Date.valueOf(resource.getMeetingDate())) ;
    entity.setMeetingDescription	(resource.getMeetingDescription	()) ;
    entity.setMeetingEndTime		(resource.getMeetingEndTime	()	) ;
    entity.setMeetingId			(new Integer(resource.getMeetingId())) ;
    entity.setMeetingOrder		(new Integer(resource.getMeetingOrder())) ;
    entity.setMeetingPubFlag		(resource.getMeetingPubFlag	()	) ;
    entity.setMeetingStartTime		(resource.getMeetingStartTime	()) ;
    entity.setMeetingTitle		(resource.getMeetingTitle		()) ;
    entity.setState			(resource.getState		()	) ;
    entity.setStreet1			(resource.getStreet1		()	) ;
    entity.setStreet2			(resource.getStreet2		()	) ;
    entity.setZip			(resource.getZip			()) ;

        
        return entity;
    }

    private entity.ProjectMeetingNotices retrieveEntity(String projecttype, String projectid, Integer meetingId)
            throws DataMartException
    {
        // Get Project
        //entity.Projects project = DataMartBeanUtil.getProject(em, projecttype, projectid);
        return em.find(entity.ProjectMeetingNotices.class, meetingId);

        // Retrieve project resource area entities from the database
//        Query query = em.createQuery("SELECT a FROM ProjectMeetingNotices a WHERE a.projectId = :project AND a.meetingId = :meetingnotice");
//        query.setParameter("project", project);
//        query.setParameter("meetingnotice", meetingNotice.getMeetingId());

        //Object rec = DataMartBeanUtil.getEntityByQueryAllowNull(query, "Multiple rows found!");
        //return (entity.ProjectMeetingNotices)rec;
    }
    public static final String fill( char fillChar,
                                     int fieldLength, String value) 
    {
        StringBuffer buffer = new StringBuffer();
        int length = value.length();
      
        for (int i = 0; i < (fieldLength - length); i++)
        {
            buffer.append(fillChar);
        }
        buffer.append(value);
        return buffer.toString();
    }

}
