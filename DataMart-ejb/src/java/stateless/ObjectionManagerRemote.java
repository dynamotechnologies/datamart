package stateless;

import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.objection.*;
import beanutil.DataMartException;
import us.fed.fs.www.nepa.schema.decision.Decisions;

/*
 *
 * The Manager beans are responsible for:
 *   1) Implementing business logic and
 *   2) Translating front-end resources to back-end database entities.
 *
 * Often the resources will consist of data from numerous database tables. It is the manager's responsibility
 *   to ensure that these front-end resources can be accessed as though they were cut from a single piece of wood.
 *
 */
@Remote
public interface ObjectionManagerRemote {
    public void create(Objection resource) throws DataMartException;
    public Objection get(String id) throws DataMartException;
    public Objections getAll() throws DataMartException;
    public Objections getByUnit(String unitid, String startdate, String enddate) throws DataMartException;
    public Objections getAllByProject(String projecttype, String projectid) throws DataMartException;
    public void update(Objection resource) throws DataMartException;
    public void delete(String id) throws DataMartException;
    public Boolean ping();
}