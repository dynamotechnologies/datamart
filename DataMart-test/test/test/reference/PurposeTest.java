package test.reference;

import client.PurposeClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.purpose.*;


public class PurposeTest {
    private PurposeClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new PurposeClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postPurpose(TestData.purposes.id1, TestData.purposes.name2);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Purpose purpose = null;
        Purposes purposelist = null;
        try {
            purpose = client.getPurpose(TestData.purposes.id1);
            purposelist = client.getPurposeList();
        } catch (IOException ex) {
            Logger.getLogger(PurposeTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.purposes.id1, purpose.getId());
        Assert.assertEquals(TestData.purposes.name1, purpose.getName());
        Assert.assertTrue(purposelist.getPurpose().get(0).getId().length()>0);
        Assert.assertTrue(purposelist.getPurpose().get(0).getName().length()>0);

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        Purpose purpose = null;
        try {
            client.putPurpose(TestData.purposes.id1, TestData.purposes.name2);
            purpose = client.getPurpose(TestData.purposes.id1);
        } catch (Exception ex) {
            Logger.getLogger(PurposeTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.purposes.name2, purpose.getName());

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getPurpose(TestData.purposes.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deletePurpose(TestData.purposes.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }

    private void post()
    {
        try {
            client.postPurpose(TestData.purposes.id1, TestData.purposes.name1);
        } catch (IOException ex) {
            Logger.getLogger(PurposeTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deletePurpose(TestData.purposes.id1);
        } catch (IOException ex) {
            Logger.getLogger(PurposeTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deletePurpose(TestData.purposes.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }
}