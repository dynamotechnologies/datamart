package test.reference;

import client.StateClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.state.*;


public class StateTest {

    private StateClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new StateClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postState(TestData.states.id1, TestData.states.name2);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        State state = null;
        States statelist = null;
        try {
            state = client.getState(TestData.states.id1);
            statelist = client.getStateList();
        } catch (IOException ex) {
            Logger.getLogger(StateTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.states.id1, state.getId());
        Assert.assertEquals(TestData.states.name1, state.getName());
        Assert.assertTrue(statelist.getState().get(0).getName().length()>0);
        Assert.assertTrue(statelist.getState().get(0).getId().length()>0);

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        State state = null;
        try {
            client.putState(TestData.states.id1, TestData.states.name2);
            state = client.getState(TestData.states.id1);
        } catch (Exception ex) {
            Logger.getLogger(StateTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.states.name2, state.getName());

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getState(TestData.states.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteState(TestData.states.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }

    private void post()
    {
        try {
            client.postState(TestData.states.id1, TestData.states.name1);
        } catch (IOException ex) {
            Logger.getLogger(StateTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteState(TestData.states.id1);
        } catch (IOException ex) {
            Logger.getLogger(StateTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteState(TestData.states.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }
}