package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.*;
import us.fed.fs.www.nepa.schema.appeal.Appeals;
import us.fed.fs.www.nepa.schema.customproject.Customprojects;
import us.fed.fs.www.nepa.schema.decision.Decisions;
import us.fed.fs.www.nepa.schema.objection.Objections;
import us.fed.fs.www.nepa.schema.project.Projects;
import us.fed.fs.www.nepa.schema.projectlisting.Projectlistings;
import us.fed.fs.www.nepa.schema.projectlocation.Locationslist;
import us.fed.fs.www.nepa.schema.projectmilestone.Projectmilestones;
import us.fed.fs.www.nepa.schema.unit.Units;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Properties;

@Path("/units")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class SpecialUnit extends AbstractResource
        // TODO: don't extend AbstractResource if you're going to circumvent init().
        //       Redefine the abstract resource to something more useful, or create another abstract class that is more accommodating
        // TODO: reevaluate whether this url heirarchy is really the way to go. The implementation would be much simpler
        //       were it possible to return objects of type appeal from its own base url, i.e., /appeals/.../
        //       This is because JAX-RS looks for the top-level path annotation, e.g., @Path("/appeals") to be defined at the class level.
        //       Only one class can listen on the "/appeals" path. Only one class can listen on the "/units" path. This means if we have
        //       Appeal objects being returned from /units/{code}/appeals and Unit objects being returned from /units/, then the Unit resource
        //       class must access two different kinds of manager beans: UnitManagerRemote and AppealManagerRemote. It would be simpler if
        //       each resource was responsible for returning objects only of its own type. Using this logic you would get appeals for a particular
        //       unit by calling GET /appeals?unit=110305. I'm not sure if this would have serious adverse effects on the overall API.
{
    private DataMartUtil UTIL = null;
    private UnitManagerRemote unitmanager;
    private AppealManagerRemote appealmanager;
    private ObjectionManagerRemote objectionmanager;
    private ProjectManagerRemote projectmanager;
    private ProjectLocationManagerRemote locationmanager;
    private DecisionManagerRemote decisionmanager;
    private ProjectMilestoneManagerRemote msmanager;
    private CustomProjectManagerRemote customprojectmanager;

    int UNIT_ID;
    int APPEAL_ID;
    int OBJECTION_ID;
    int PROJECT_ID;
    int CUSTOM_PROJECT_ID;
    int PROJECT_LISTING_ID;
    int LOCATION_ID;
    int DECISION_ID;
    int MS_ID;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public SpecialUnit()
            throws ServletException
    {
        UTIL = DataMartUtil.getInstance();
        if (UTIL==null)
            throw new ServletException("Horror! Could not get a reference to the Datamart Utility singleton! Exiting in shame.");

        Properties UNIT_PROPS = new Properties();
        UNIT_PROPS.setProperty("schema", "Unit.xsd");
        UNIT_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.unit");
        UNIT_PROPS.setProperty("bean", "UnitManagerRemote");
        
        Properties APPEAL_PROPS = new Properties();
        APPEAL_PROPS.setProperty("schema", "Appeal.xsd");
        APPEAL_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.appeal");
        APPEAL_PROPS.setProperty("bean", "AppealManagerRemote");

        Properties OBJECTION_PROPS = new Properties();
        OBJECTION_PROPS.setProperty("schema", "Objection.xsd");
        OBJECTION_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.objection");
        OBJECTION_PROPS.setProperty("bean", "ObjectionManagerRemote");

        Properties PROJECT_PROPS = new Properties();
        PROJECT_PROPS.setProperty("schema", "Project.xsd");
        PROJECT_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.project");
        PROJECT_PROPS.setProperty("bean", "ProjectManagerRemote");

        Properties CUSTOM_PROJECT_PROPS = new Properties();
        CUSTOM_PROJECT_PROPS.setProperty("schema", "CustomProject.xsd");
        CUSTOM_PROJECT_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.customproject");
        CUSTOM_PROJECT_PROPS.setProperty("bean", "CustomProjectManagerRemote");

        Properties PROJECT_LISTING_PROPS = new Properties();
        PROJECT_LISTING_PROPS.setProperty("schema", "ProjectListing.xsd");
        PROJECT_LISTING_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.projectlisting");
        PROJECT_LISTING_PROPS.setProperty("bean", "ProjectListingManagerRemote");
        
        Properties LOCATION_PROPS = new Properties();
        LOCATION_PROPS.setProperty("schema", "ProjectLocation.xsd");
        LOCATION_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.projectlocation");
        LOCATION_PROPS.setProperty("bean", "ProjectLocationManagerRemote");

        Properties DECISION_PROPS = new Properties();
        DECISION_PROPS.setProperty("schema", "Decision.xsd");
        DECISION_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.decision");
        DECISION_PROPS.setProperty("bean", "DecisionManagerRemote");

        Properties MS_PROPS = new Properties();
        MS_PROPS.setProperty("schema", "ProjectMilestone.xsd");
        MS_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.projectmilestone");
        MS_PROPS.setProperty("bean", "ProjectMilestoneManagerRemote");
        try {
            UNIT_ID = UTIL.getResourceId(UNIT_PROPS);
            APPEAL_ID = UTIL.getResourceId(APPEAL_PROPS);
            OBJECTION_ID = UTIL.getResourceId(OBJECTION_PROPS);
            PROJECT_ID = UTIL.getResourceId(PROJECT_PROPS);
            LOCATION_ID = UTIL.getResourceId(LOCATION_PROPS);
            DECISION_ID = UTIL.getResourceId(DECISION_PROPS);
            MS_ID = UTIL.getResourceId(MS_PROPS);
            CUSTOM_PROJECT_ID = UTIL.getResourceId(CUSTOM_PROJECT_PROPS);
            PROJECT_LISTING_ID = UTIL.getResourceId(PROJECT_LISTING_PROPS);

            unitmanager = (UnitManagerRemote) UTIL.getManager(UNIT_ID);
            appealmanager = (AppealManagerRemote) UTIL.getManager(APPEAL_ID);
            objectionmanager = (ObjectionManagerRemote) UTIL.getManager(OBJECTION_ID);
            projectmanager = (ProjectManagerRemote) UTIL.getManager(PROJECT_ID);
            locationmanager = (ProjectLocationManagerRemote) UTIL.getManager(LOCATION_ID);
            decisionmanager = (DecisionManagerRemote) UTIL.getManager(DECISION_ID);
            msmanager = (ProjectMilestoneManagerRemote) UTIL.getManager(MS_ID);
            customprojectmanager = (CustomProjectManagerRemote) UTIL.getManager(CUSTOM_PROJECT_ID);
            
        } catch (IOException ex) {
            ex.printStackTrace();
            log.error(ex.toString());
            throw new ServletException("SpecialUnit failed to start. Could not start the bean managers.");
       }
        if (!(unitmanager.ping() && appealmanager.ping() && objectionmanager.ping() && projectmanager.ping()))
            throw new ServletException("SpecialUnit failed to start. Could not ping the bean managers!");
    }


    @GET
    @Path("/regions")
    public Response getRegions(@Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter response = new StringWriter();
        try {
            // Get the name of the specified entity
            Units unitlist = unitmanager.getRegions();

            // Get the query results in a JAXBElement object, marshal and return
            us.fed.fs.www.nepa.schema.unit.ObjectFactory objectfactory = new us.fed.fs.www.nepa.schema.unit.ObjectFactory();
            JAXBElement<Units> jaxbelement = objectfactory.createUnits(unitlist);
            JAXBContext jaxbcontext = UTIL.getContext(UNIT_ID);
            Marshaller marshaller = jaxbcontext.createMarshaller();
            marshaller.marshal(jaxbelement, response);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(response);
    }


    @GET
    @Path("/{unitcode}/subunits")
    public Response getSubunits(@PathParam("unitcode") String unitcode, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter response = new StringWriter();
        try {
            // Get the name of the specified entity
            Units unitlist = unitmanager.getSubUnits(unitcode);

            // Get the query results in a JAXBElement object, marshal and return
            us.fed.fs.www.nepa.schema.unit.ObjectFactory objectfactory = new us.fed.fs.www.nepa.schema.unit.ObjectFactory();
            JAXBElement<Units> jaxbelement = objectfactory.createUnits(unitlist);
            JAXBContext jaxbcontext = UTIL.getContext(UNIT_ID);
            Marshaller marshaller = jaxbcontext.createMarshaller();
            marshaller.marshal(jaxbelement, response);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(response);
    }


    @GET
    @Path("/{unitcode}/appeals")
    public Response getAppealsByUnit(@PathParam("unitcode") String unitcode, @QueryParam("startdate") String startdate,
            @QueryParam("enddate") String enddate, @QueryParam("statusfilter") String statusfilter, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter response = new StringWriter();
        try {
            // Get the name of the specified entity
            Appeals appeallist = appealmanager.getByUnit(unitcode, startdate, enddate, statusfilter);

            // Get the query results in a JAXBElement object, marshal and return
            us.fed.fs.www.nepa.schema.appeal.ObjectFactory objectfactory = new us.fed.fs.www.nepa.schema.appeal.ObjectFactory();
            JAXBElement<Appeals> jaxbelement = objectfactory.createAppeals(appeallist);
            JAXBContext jaxbcontext = UTIL.getContext(APPEAL_ID);
            Marshaller marshaller = jaxbcontext.createMarshaller();
            marshaller.marshal(jaxbelement, response);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(response);
    }


    @GET
    @Path("/{unitcode}/objections")
    public Response getObjectionsByUnit(@PathParam("unitcode") String unitcode, @QueryParam("startdate") String startdate,
            @QueryParam("enddate") String enddate, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter response = new StringWriter();
        try {
            // Get the name of the specified entity
            Objections objectionlist = objectionmanager.getByUnit(unitcode, startdate, enddate);

            // Get the query results in a JAXBElement object, marshal and return
            us.fed.fs.www.nepa.schema.objection.ObjectFactory objectfactory = new us.fed.fs.www.nepa.schema.objection.ObjectFactory();
            JAXBElement<Objections> jaxbelement = objectfactory.createObjections(objectionlist);
            JAXBContext jaxbcontext = UTIL.getContext(OBJECTION_ID);
            Marshaller marshaller = jaxbcontext.createMarshaller();
            marshaller.marshal(jaxbelement, response);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(response);
    }


    @GET
    @Path("/{unitcode}/projects")
    public Response getProjectsByUnit(@PathParam("unitcode") String unitcode,
            @QueryParam("sopaonly") Boolean sopaonly,
            @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        if (sopaonly==null)
            sopaonly=false;

        StringWriter response = new StringWriter();
        try {
            // Get the name of the specified entity
            Projects projectlist = projectmanager.getByUnit(unitcode, sopaonly);

            // Get the query results in a JAXBElement object, marshal and return
            us.fed.fs.www.nepa.schema.project.ObjectFactory objectfactory = new us.fed.fs.www.nepa.schema.project.ObjectFactory();
            JAXBElement<Projects> jaxbelement = objectfactory.createProjects(projectlist);
            JAXBContext jaxbcontext = UTIL.getContext(PROJECT_ID);
            Marshaller marshaller = jaxbcontext.createMarshaller();
            marshaller.marshal(jaxbelement, response);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(response);
    }
    
    @GET
    @Path("/{unitcode}/activeprojects")
    public Response getActiveProjectsByUnit(@PathParam("unitcode") String unitcode,
            @QueryParam("sopaonly") Boolean sopaonly,
            @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        if (sopaonly==null)
            sopaonly=false;

        StringWriter response = new StringWriter();
        try {
            // Get the name of the specified entity
            Projects projectlist = projectmanager.getActiveByUnit(unitcode, sopaonly);

            // Get the query results in a JAXBElement object, marshal and return
            us.fed.fs.www.nepa.schema.project.ObjectFactory objectfactory = new us.fed.fs.www.nepa.schema.project.ObjectFactory();
            JAXBElement<Projects> jaxbelement = objectfactory.createProjects(projectlist);
            JAXBContext jaxbcontext = UTIL.getContext(PROJECT_ID);
            Marshaller marshaller = jaxbcontext.createMarshaller();
            marshaller.marshal(jaxbelement, response);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(response);
    }
    
    @GET
    @Path("/{unitcode}/customprojects")
    public Response getCustomProjectsByUnit(@PathParam("unitcode") String unitcode,
            @QueryParam("sopaonly") Boolean sopaonly,
            @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        if (sopaonly==null)
            sopaonly=false;

        StringWriter response = new StringWriter();
        try {
            // Get the name of the specified entity
            Customprojects projectlist = customprojectmanager.getAllForUnit(unitcode);

            // Get the query results in a JAXBElement object, marshal and return
            us.fed.fs.www.nepa.schema.customproject.ObjectFactory objectfactory = new us.fed.fs.www.nepa.schema.customproject.ObjectFactory();
            JAXBElement<Customprojects> jaxbelement = objectfactory.createCustomprojects(projectlist);
            JAXBContext jaxbcontext = UTIL.getContext(CUSTOM_PROJECT_ID);
            Marshaller marshaller = jaxbcontext.createMarshaller();
            marshaller.marshal(jaxbelement, response);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(response);
    }
    
    @GET
    @Path("/{unitcode}/projectlistings")
    public Response getProjectListingsByUnit(@PathParam("unitcode") String unitcode,
            @QueryParam("sopaonly") Boolean sopaonly,
            @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        if (sopaonly==null)
            sopaonly=false;

        StringWriter response = new StringWriter();
        try {
            // Get the name of the specified entity
            Projectlistings projectlist = projectmanager.getListingsByUnit(unitcode, sopaonly);
            
            // Get the name of the specified entity
            Projectlistings customprojectlist = customprojectmanager.getListingsByUnit(unitcode);

            projectlist.getProjectlisting().addAll(customprojectlist.getProjectlisting());
            
            // Get the query results in a JAXBElement object, marshal and return
            us.fed.fs.www.nepa.schema.projectlisting.ObjectFactory objectfactory = new us.fed.fs.www.nepa.schema.projectlisting.ObjectFactory();
            JAXBElement<Projectlistings> jaxbelement = objectfactory.createProjectlistings(projectlist);
            JAXBContext jaxbcontext = UTIL.getContext(PROJECT_LISTING_ID);
            Marshaller marshaller = jaxbcontext.createMarshaller();
            marshaller.marshal(jaxbelement, response);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex) {
            ex.printStackTrace();
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(response);
    }
    
    
    @GET
    @Path("/{unitcode}/projectspotlights")
    public Response getProjectSpotlightsByUnit(@PathParam("unitcode") String unitcode,
            @Context UriInfo uriInfo)
            throws WebApplicationException
    {

        StringWriter response = new StringWriter();
        try {
            // Get the name of the specified entity
            Projectlistings projectlist = projectmanager.getSpotlightsByUnit(unitcode);
            // Get the name of the specified entity
       //  NOTE: CustomProjects are not included in Spotlights at this time!
            
            // Get the query results in a JAXBElement object, marshal and return
            us.fed.fs.www.nepa.schema.projectlisting.ObjectFactory objectfactory = new us.fed.fs.www.nepa.schema.projectlisting.ObjectFactory();
            JAXBElement<Projectlistings> jaxbelement = objectfactory.createProjectlistings(projectlist);
            JAXBContext jaxbcontext = UTIL.getContext(PROJECT_LISTING_ID);
            Marshaller marshaller = jaxbcontext.createMarshaller();
            marshaller.marshal(jaxbelement, response);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex) {
            ex.printStackTrace();
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(response);
    }
    
    
    @GET
    @Path("/{unitcode}/locations")
    public Response getLocationsByUnit(@PathParam("unitcode") String unitcode,
            @QueryParam("sopaonly") Boolean sopaonly,
            @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        if (sopaonly==null)
            sopaonly=false;

        StringWriter response = new StringWriter();
        try {
            // Get the name of the specified entity
            Locationslist locationslist = locationmanager.getByUnit(unitcode, sopaonly);

            // Get the query results in a JAXBElement object, marshal and return
            us.fed.fs.www.nepa.schema.projectlocation.ObjectFactory objectfactory = new us.fed.fs.www.nepa.schema.projectlocation.ObjectFactory();
            JAXBElement<Locationslist> jaxbelement = objectfactory.createLocationslist(locationslist);
            JAXBContext jaxbcontext = UTIL.getContext(LOCATION_ID);
            Marshaller marshaller = jaxbcontext.createMarshaller();
            marshaller.marshal(jaxbelement, response);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(response);
    }


    @GET
    @Path("/{unitcode}/decisions")
    public Response getDecisionsByUnit(@PathParam("unitcode") String unitcode,
            @QueryParam("sopaonly") Boolean sopaonly,
            @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        if (sopaonly==null)
            sopaonly=false;

        StringWriter response = new StringWriter();
        try {
            // Get the name of the specified entity
            Decisions decisionlist = decisionmanager.getByUnit(unitcode, sopaonly);

            // Get the query results in a JAXBElement object, marshal and return
            us.fed.fs.www.nepa.schema.decision.ObjectFactory objectfactory = new us.fed.fs.www.nepa.schema.decision.ObjectFactory();
            JAXBElement<Decisions> jaxbelement = objectfactory.createDecisions(decisionlist);
            JAXBContext jaxbcontext = UTIL.getContext(DECISION_ID);
            Marshaller marshaller = jaxbcontext.createMarshaller();
            marshaller.marshal(jaxbelement, response);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(response);
    }


    @GET
    @Path("/{unitcode}/projectmilestones")
    public Response getProjctMilestonesByUnit(@PathParam("unitcode") String unitcode,
            @QueryParam("sopaonly") Boolean sopaonly,
            @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        if (sopaonly==null)
            sopaonly=false;

        StringWriter response = new StringWriter();
        try {
            // Get the name of the specified entity
            Projectmilestones mslist = msmanager.getByUnit(unitcode, sopaonly);

            // Get the query results in a JAXBElement object, marshal and return
            us.fed.fs.www.nepa.schema.projectmilestone.ObjectFactory objectfactory = new us.fed.fs.www.nepa.schema.projectmilestone.ObjectFactory();
            JAXBElement<Projectmilestones> jaxbelement = objectfactory.createProjectmilestones(mslist);
            JAXBContext jaxbcontext = UTIL.getContext(MS_ID);
            Marshaller marshaller = jaxbcontext.createMarshaller();
            marshaller.marshal(jaxbelement, response);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(response);
    }
}