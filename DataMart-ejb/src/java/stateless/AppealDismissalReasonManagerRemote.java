package stateless;

import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.appealdismissalreason.*;
import beanutil.DataMartException;

/*
 *
 * The Manager beans are responsible for:
 *   1) Implementing business logic and
 *   2) Translating front-end resources to back-end database entities.
 *
 * Often the resources will consist of data from numerous database tables. It is the manager's responsibility
 *   to ensure that these front-end resources can be accessed as though they were cut from a single piece of wood.
 *
 */
@Remote
public interface AppealDismissalReasonManagerRemote {
    public void create(Appealdismissalreason resource) throws DataMartException;
    public String getName(String id) throws DataMartException;
    public void update(Appealdismissalreason resource) throws DataMartException;
    public void delete(String id) throws DataMartException;
    public Appealdismissalreasons getAll() throws DataMartException;
    public Boolean ping();
}