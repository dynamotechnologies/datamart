package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.projecttype.*;


public class ProjectTypeClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public ProjectTypeClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.projecttype", "ProjectType.xsd");
    }


    public void postProjectType(String id, String name)
            throws IOException
    {
        Projecttype projecttype = new Projecttype();
        projecttype.setId(id);
        projecttype.setName(name);
        postProjectType(projecttype);
    }


    public void postProjectType(Projecttype projecttype)
            throws IOException
    {
        post(objectfactory.createProjecttype(projecttype), "/ref/projecttypes");
    }


    public Projecttype getProjectType(String id)
            throws IOException
    {
        return get("/ref/projecttypes/" + id, Projecttype.class);
    }


    public Projecttypes getProjectTypeList()
            throws IOException
    {
        return get("/ref/projecttypes", Projecttypes.class);
    }


    public void putProjectType(String id, String name)
            throws IOException
    {
        Projecttype projecttype = new Projecttype();
        projecttype.setId(id);
        projecttype.setName(name);
        putProjectType(projecttype);
    }


    public void putProjectType(Projecttype projecttype)
            throws IOException
    {
        put(objectfactory.createProjecttype(projecttype), "/ref/projecttypes/" + projecttype.getId());
    }


    public void deleteProjectType(String id)
            throws IOException
    {
        delete("/ref/projecttypes/" + id);
    }
}