package test.relationship;

import client.ProjectRegionClient;
import client.ProjectClient;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.projectlocation.*;


public class ProjectRegionTest {
    private ProjectRegionClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new ProjectRegionClient();
    }


    @Test
    public void testPut()
    {
        put();

        // second PUT should overwrite the first
        try {
            client.putProjectRegion(TestData.projects.id1, TestData.projectlocations.regionid1);
            client.putProjectRegion(TestData.projects.id1, TestData.projectlocations.regionid2);
        } catch (Exception ex) {
            Logger.getLogger(ProjectRegionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        put();

        Regions region1 = null;
        Regions region2 = null;
        try {
            region1 = client.getProjectRegion(TestData.projects.id1, TestData.projectlocations.regionid1);
            region2 = client.getProjectRegion(TestData.projects.id1, TestData.projectlocations.regionid2);
        } catch (Exception ex) {
            Logger.getLogger(ProjectRegionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // make sure each GET response is exactly one region, and that it matches the one we PUTted.
        Assert.assertEquals(1, region1.getRegionid().size());
        Assert.assertEquals(1, region2.getRegionid().size());
        Assert.assertEquals(TestData.projectlocations.regionid1, region1.getRegionid().get(0));
        Assert.assertEquals(TestData.projectlocations.regionid2, region2.getRegionid().get(0));

        delete();
    }

    @Test
    public void testDelete()
    {
        put();
        delete();

        try {
            client.deleteProjectRegion(TestData.projects.id1, TestData.projectlocations.regionid1);
            // second delete should have thrown an exception, because the region no longer exists
            Assert.fail();
        } catch (Exception ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
        try {
            client.deleteProjectRegion(TestData.projects.id1, TestData.projectlocations.regionid2);
            // second delete should have thrown an exception, because the region no longer exists
            Assert.fail();
        } catch (Exception ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }


    private void put()
    {
        try {
            client.putProjectRegion(TestData.projects.id1, TestData.projectlocations.regionid1);
            client.putProjectRegion(TestData.projects.id1, TestData.projectlocations.regionid2);
        } catch (Exception ex) {
            Logger.getLogger(ProjectRegionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteProjectRegion(TestData.projects.id1, TestData.projectlocations.regionid1);
            client.deleteProjectRegion(TestData.projects.id1, TestData.projectlocations.regionid2);
        } catch (Exception ex) {
            Logger.getLogger(ProjectRegionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteProjectRegion(TestData.projects.id1, TestData.projectlocations.regionid1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
        try {
            client.deleteProjectRegion(TestData.projects.id1, TestData.projectlocations.regionid2);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }

    
    @BeforeClass
    public static void  populateDependencies()
    {
        ProjectClient projclient = null;
        try {
            projclient = new ProjectClient();
        } catch (Exception ex) {
            Logger.getLogger(ProjectRegionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Project Regions rely on Projects. Pre-populate a Project.
        ArrayList<String> purposeids1 = new ArrayList<String>();
        purposeids1.add(TestData.projects.purposeid1);
        purposeids1.add(TestData.projects.purposeid2);
        purposeids1.add(TestData.projects.purposeid3);
        try {
            projclient.postProject(TestData.projects.id1, TestData.projects.type1,
                    TestData.projects.name1, TestData.projects.unitcode1, TestData.projects.description1,
                    TestData.projects.lastupdate1, TestData.projects.commentreg1, TestData.projects.wwwlink1,
                    TestData.projects.wwwsummary1, TestData.projects.wwwpub1, TestData.projects.analysistypeid1,
                    TestData.projects.statusid1, TestData.projects.contactname1, TestData.projects.contactphone1,
                    TestData.projects.contactemail1, purposeids1, TestData.projects.expirationdate1, TestData.projects.projectdocumentid1,
                    TestData.projects.sopapub, TestData.projects.sopanew, TestData.projects.sopaheader, TestData.projects.esd, TestData.projects.cenodecision);
        } catch (IOException ex) {
            Logger.getLogger(ProjectRegionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @AfterClass
    public static void  cleanupDependencies()
    {
        ProjectClient projclient = null;
        try {
            projclient = new ProjectClient();
        } catch (Exception ex) {
            Assert.fail();
        }

        // Clean up the Project
        try {
            projclient.deleteProject("nepa", TestData.projects.id1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectRegionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }
}