package test.entity;

import client.DecisionClient;
import client.DecisionMakerClient;
import client.ProjectClient;
import client.ProjectForestClient;
import java.io.IOException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.*;
import test.TestData;
import us.fed.fs.www.nepa.schema.decisionmaker.Decisionmaker;

public class DecisionMakerTest {

    private DecisionMakerClient client;
    private SimpleDateFormat dateformatter;

    @Before
    public void setUp()
            throws IOException
    {
         client = new DecisionMakerClient();
         dateformatter = new SimpleDateFormat("yyyy-MM-dd");
    }

    @Test
    public void testPost()
    {
        post1();

        // This second POST should fail, because the entity already exists
        try {
            client.postDecisionMaker(TestData.decisionmakers.id1,
                    TestData.decisions.id1,
                    TestData.decisionmakers.name2,
                    TestData.decisionmakers.signeddate2,
                    TestData.decisionmakers.title2
                    );
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post1();

        Decisionmaker decisionmaker = null;
        try {
            decisionmaker = client.getDecisionMaker(TestData.decisions.id1, TestData.decisionmakers.id1);
        } catch (IOException ex) {
            Logger.getLogger(DecisionMakerTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.decisionmakers.id1, decisionmaker.getId());
        Assert.assertEquals(TestData.decisions.id1, decisionmaker.getDecisionid());
        Assert.assertEquals(TestData.decisionmakers.name1, decisionmaker.getName());
        
        Date testdate = dateformatter.parse(TestData.decisionmakers.signeddate1, new ParsePosition(0));
        Assert.assertTrue(testdate.equals(dateformatter.parse(decisionmaker.getSigneddate().toString(), new ParsePosition(0))));
        
        Assert.assertEquals(TestData.decisionmakers.title1, decisionmaker.getTitle());

        delete();
    }

    @Test
    public void testPut()
    {
        post1();

        Decisionmaker decisionmaker = null;
        
        try {
            client.putDecisionMaker(TestData.decisionmakers.id1,
                    TestData.decisions.id1,
                    TestData.decisionmakers.name2,
                    TestData.decisionmakers.signeddate2,
                    TestData.decisionmakers.title2
                    );
            decisionmaker = client.getDecisionMaker(TestData.decisions.id1, TestData.decisionmakers.id1);
        } catch (IOException ex) {
            Logger.getLogger(DecisionMakerTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.decisionmakers.id1, decisionmaker.getId());
        Assert.assertEquals(TestData.decisions.id1, decisionmaker.getDecisionid());
        Assert.assertEquals(TestData.decisionmakers.name2, decisionmaker.getName());
        
        Date testdate = dateformatter.parse(TestData.decisionmakers.signeddate2, new ParsePosition(0));
        Assert.assertTrue(testdate.equals(dateformatter.parse(decisionmaker.getSigneddate().toString(), new ParsePosition(0))));
        
        Assert.assertEquals(TestData.decisionmakers.title2, decisionmaker.getTitle());

        delete();
    }

    @Test
    public void testDelete()
    {
        post1();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getDecisionMaker(TestData.decisions.id1, TestData.decisionmakers.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND) {
                System.out.println("Delete should have failed, returned status " + client.getStatus() + " instead");
                Assert.fail();
            }
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteDecisionMaker(TestData.decisions.id1, TestData.decisionmakers.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }

    private void post1()
    {
        try {
            client.postDecisionMaker(TestData.decisionmakers.id1,
                    TestData.decisions.id1,
                    TestData.decisionmakers.name1,
                    TestData.decisionmakers.signeddate1,
                    TestData.decisionmakers.title1
                    );

        } catch (IOException ex) {
            Logger.getLogger(DecisionMakerTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }
    

    private void delete()
    {
        try {
            client.deleteDecisionMaker(TestData.decisions.id1, TestData.decisionmakers.id1);
        } catch (IOException ex) {
            Logger.getLogger(DecisionMakerTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteDecisionMaker(TestData.decisions.id1, TestData.decisionmakers.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }


    @BeforeClass
    public static void  populateDependencies()
    {
        ProjectClient projclient = null;
        ProjectForestClient projforestclient = null;
        try {
            projclient = new ProjectClient();
            projforestclient = new ProjectForestClient();
        } catch (Exception ex) {
            Assert.fail();
        }

        // Decisions rely on Projects. Pre-populate a Project.
        ArrayList<String> purposeids1 = new ArrayList<String>();
        purposeids1.add(TestData.projects.purposeid1);
        purposeids1.add(TestData.projects.purposeid2);
        purposeids1.add(TestData.projects.purposeid3);
        try {
            projclient.postProject(TestData.projects.id1, TestData.projects.type1,
                    TestData.projects.name1, TestData.projects.unitcode1, TestData.projects.description1,
                    TestData.projects.lastupdate1, TestData.projects.commentreg1, TestData.projects.wwwlink1,
                    TestData.projects.wwwsummary1, TestData.projects.wwwpub1, TestData.projects.analysistypeid1,
                    TestData.projects.statusid1, TestData.projects.contactname1, TestData.projects.contactphone1,
                    TestData.projects.contactemail1, purposeids1, TestData.projects.expirationdate1, TestData.projects.projectdocumentid1,
                    TestData.projects.sopapub, TestData.projects.sopanew, TestData.projects.sopaheader, TestData.projects.esd, TestData.projects.cenodecision);
            projforestclient.putProjectForest(TestData.projects.id1, TestData.projects.unitcode1);
        } catch (IOException ex) {
            Logger.getLogger(DecisionMakerTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        
        // Create a Decision
        try {
            DecisionClient decclient = new DecisionClient();
            decclient.postDecision(TestData.decisions.id1,
                    TestData.projects.id1,
                    TestData.projects.type1,
                    TestData.decisions.name1,
                    TestData.appealstatuses.id1,
                    TestData.decisions.constraint1,
                    TestData.decisions.legalnoticedate1,
                    null,
                    null,
                    TestData.decisions.dectype1,
                    TestData.decisions.decdate1
                    );

        } catch (IOException ex) {
            Logger.getLogger(DecisionMakerTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @AfterClass
    public static void  cleanupDependencies()
    {
        // Remove the Decision
        try {
            DecisionClient decclient = new DecisionClient();
            decclient.deleteDecision(TestData.decisions.id1);
        } catch (IOException ex) {
            Logger.getLogger(DecisionMakerTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
                
        ProjectForestClient projforestclient = null;
        ProjectClient projclient = null;
        try {
            projforestclient = new ProjectForestClient();
            projclient = new ProjectClient();
        } catch (Exception ex) {
            Assert.fail();
        }

        // Clean up the Project
        try {
            projforestclient.deleteProjectForest(TestData.projects.id1, TestData.projects.unitcode1);
            projclient.deleteProject("nepa", TestData.projects.id1);
        } catch (IOException ex) {
            Logger.getLogger(DecisionMakerTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }
}
