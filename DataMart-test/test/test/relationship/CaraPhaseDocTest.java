package test.relationship;

import client.ProjectClient;
import client.ProjectDocumentClient;
import client.CARAProjectClient;
import client.CommentPhaseClient;
import client.CARAPhaseDocClient;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.projectdocument.*;


public class CaraPhaseDocTest {
    private CARAPhaseDocClient client;
    private ProjectClient projclient;
    private ProjectDocumentClient projdocclient;

    @Before
    public void setUp()
            throws IOException
    {
         client = new CARAPhaseDocClient();
         projclient = new ProjectClient();
         projdocclient = new ProjectDocumentClient();
    }


    @Test
    public void testLink()
    {
        link();

        // Try to relink same document
        try {
            // second link request should fail
            client.linkDoc(TestData.caraprojects.id1, TestData.commentphases.id1, TestData.projectdocuments.docid1);
            Assert.fail();
        } catch (Exception ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
            {
                Assert.fail();
            }
        }

        // Project document should have link to a CARA phase
        try {
            Projectdocument d = projdocclient.getProjectDocument(
                    TestData.projects.type1,
                    TestData.projects.id1,
                    TestData.projectdocuments.docid1);
            if (d.getLinks() == null) {
                Assert.fail("Project document linked to a CARA phase has no <link> tags!");
            }
            boolean foundPhase = false;
            for (us.fed.fs.www.nepa.schema.projectdocument.Link link : d.getLinks().getLink()) {
                if (link.getRel().matches("caraphase")) {
 //                   System.out.print("Link: " + link.getHref() + "\n");
                    foundPhase = true;
                }
            }
            if (foundPhase == false) {
                Assert.fail("Missing phase link in project document!");
            }
        } catch (IOException ex) {
            Logger.getLogger(CaraPhaseDocTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        unlink();
    }

    @Test
    public void testUnlink()
    {
        link();
        unlink();

        // Attempt to unlink same document
        try {
            // second unlink request should fail
            client.unlinkDoc(TestData.caraprojects.id1, TestData.commentphases.id1, TestData.projectdocuments.docid1);
            Assert.fail();
        } catch (Exception ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
            {
                Assert.fail();
            }
        }

        // Make sure the project document no longer has a CARA phase <link> tag
        try {
            Projectdocument d = projdocclient.getProjectDocument(
                    TestData.projects.type1,
                    TestData.projects.id1,
                    TestData.projectdocuments.docid1);
            if (d.getLinks() != null) {
                Assert.fail("Unlinked project document has unexpected <link> tags!");
            }
        } catch (IOException ex) {
            Logger.getLogger(MlmProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void link()
    {
        try {
            client.linkDoc(TestData.caraprojects.id1, TestData.commentphases.id1, TestData.projectdocuments.docid1);
        } catch (Exception ex) {
            Logger.getLogger(CaraPhaseDocTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void unlink()
    {
        try {
            client.unlinkDoc(TestData.caraprojects.id1, TestData.commentphases.id1, TestData.projectdocuments.docid1);
        } catch (Exception ex) {
            Logger.getLogger(CaraPhaseDocTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.unlinkDoc(TestData.caraprojects.id1, TestData.commentphases.id1, TestData.projectdocuments.docid1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }


    @BeforeClass
    public static void  populateDependencies()
    {
        ProjectClient projclient = null;
        ProjectDocumentClient projdocclient = null;
        CARAProjectClient caraprojclient = null;
        CommentPhaseClient commentphaseclient = null;
        try {
            projclient = new ProjectClient();
            projdocclient = new ProjectDocumentClient();
            caraprojclient = new CARAProjectClient();
            commentphaseclient = new CommentPhaseClient();
        } catch (Exception ex) {
            Logger.getLogger(CaraPhaseDocTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Create a Project.
        ArrayList<String> purposeids1 = new ArrayList<String>();
        purposeids1.add(TestData.projects.purposeid1);
        purposeids1.add(TestData.projects.purposeid2);
        purposeids1.add(TestData.projects.purposeid3);
        try {
            projclient.postProject(TestData.projects.id1, TestData.projects.type1,
                    TestData.projects.name1, TestData.projects.unitcode1, TestData.projects.description1,
                    TestData.projects.lastupdate1, TestData.projects.commentreg1, TestData.projects.wwwlink1,
                    TestData.projects.wwwsummary1, TestData.projects.wwwpub1, TestData.projects.analysistypeid1,
                    TestData.projects.statusid1, TestData.projects.contactname1, TestData.projects.contactphone1,
                    TestData.projects.contactemail1, purposeids1, TestData.projects.expirationdate1, TestData.projects.projectdocumentid1,
                    TestData.projects.sopapub, TestData.projects.sopanew, TestData.projects.sopaheader, TestData.projects.esd, TestData.projects.cenodecision);
        } catch (IOException ex) {
            Logger.getLogger(CaraPhaseDocTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Add documents
        try {
            projdocclient.postProjectDocument(TestData.projects.id1, TestData.projects.type1,
                TestData.projectdocuments.docid1, TestData.projectdocuments.pubflag1,
                TestData.projectdocuments.wwwlink1, TestData.projectdocuments.docname1,
                TestData.projectdocuments.description1, TestData.projectdocuments.pdffilesize1,
                TestData.projectdocuments.sensitiveflag1);
        } catch (IOException ex) {
            Logger.getLogger(CaraPhaseDocTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Link to a CARA ID
        try {
            caraprojclient.linkProject(TestData.projects.id1, TestData.caraprojects.id1);
        } catch (Exception ex) {
            Logger.getLogger(CaraPhaseDocTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Create a Phase
        try {
            commentphaseclient.postCommentPhase(TestData.caraprojects.id1,
                                    TestData.commentphases.id1,
                                    TestData.commentphases.name2,
                                    TestData.commentphases.startdate1,
                                    TestData.commentphases.finishdate1,
                                    TestData.commentphases.lettno1,
                                    TestData.commentphases.lettuniq2,
                                    TestData.commentphases.lettmst2,
                                    TestData.commentphases.lettform1,
                                    TestData.commentphases.lettformplus1,
                                    TestData.commentphases.lettformdupe1);
        } catch (Exception ex) {
            Logger.getLogger(CaraPhaseDocTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        try {
            Projectdocument d = projdocclient.getProjectDocument(
                    TestData.projects.type1,
                    TestData.projects.id1,
                    TestData.projectdocuments.docid1);
            if (d.getLinks() != null) {
                Assert.fail("Non-linked project document has unexpected <link> tags!");
            }
        } catch (IOException ex) {
            Logger.getLogger(MlmProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @AfterClass
    public static void  cleanupDependencies()
    {
        ProjectClient projclient = null;
        ProjectDocumentClient projdocclient = null;
        CARAProjectClient caraprojclient = null;
        CommentPhaseClient commentphaseclient = null;
        CARAPhaseDocClient caraphasedocclient = null;
        try {
            projclient = new ProjectClient();
            projdocclient = new ProjectDocumentClient();
            caraprojclient = new CARAProjectClient();
            commentphaseclient = new CommentPhaseClient();
            caraphasedocclient = new CARAPhaseDocClient();
        } catch (Exception ex) {
            Logger.getLogger(CaraPhaseDocTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Unlink CARA documents (if linked)
        try {
            caraphasedocclient.unlinkDoc(TestData.caraprojects.id1, TestData.commentphases.id1, TestData.projectdocuments.docid1);
        } catch (IOException ex) {
            if (caraphasedocclient.getStatus() != 404) {
                Logger.getLogger(CaraPhaseDocTest.class.getName()).log(Level.SEVERE, null, ex);
                Assert.fail();
            }
        }

        // Drop phase
        try {
            commentphaseclient.deleteCommentPhase(TestData.caraprojects.id1, TestData.commentphases.id1);
        } catch (IOException ex) {
            Logger.getLogger(CaraPhaseDocTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Unlink the CARA project
        try {
            caraprojclient.unlinkProject(TestData.caraprojects.id1);
        } catch (Exception ex) {
            Logger.getLogger(CaraPhaseDocTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Remove project documents
        try {
            projdocclient.deleteProjectDocument(TestData.projects.type1, TestData.projects.id1,
                    TestData.projectdocuments.docid1);
        } catch (IOException ex) {
            Logger.getLogger(CaraPhaseDocTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Clean up the Project
        try {
            projclient.deleteProject("nepa", TestData.projects.id1);
        } catch (IOException ex) {
            Logger.getLogger(CaraPhaseDocTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }
}