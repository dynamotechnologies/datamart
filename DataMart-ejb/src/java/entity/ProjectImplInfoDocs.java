/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author gauri
 */
@Entity
@Table(name = "ProjectImplInfoDocs")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProjectImplInfoDocs.findAll", query = "SELECT p FROM ProjectImplInfoDocs p"),
    @NamedQuery(name = "ProjectImplInfoDocs.findByDocumentOrder", query = "SELECT p FROM ProjectImplInfoDocs p WHERE p.documentOrder = :documentOrder"),
    @NamedQuery(name = "ProjectImplInfoDocs.findByImplInfoDocId", query = "SELECT p FROM ProjectImplInfoDocs p WHERE p.implInfoDocId = :implInfoDocId")})
public class ProjectImplInfoDocs implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "documentOrder")
    private Integer documentOrder;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ImplInfoDocId")
    private Integer implInfoDocId;
    @JoinColumn(name = "ImplInfoId", referencedColumnName = "implId")
    @ManyToOne(optional = false)
    private ProjectImplInfo implInfoId;
    @JoinColumn(name = "documentId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private ProjectDocuments documentId;

    public ProjectImplInfoDocs() {
    }

    public ProjectImplInfoDocs(Integer implInfoDocId) {
        this.implInfoDocId = implInfoDocId;
    }

    public Integer getDocumentOrder() {
        return documentOrder;
    }

    public void setDocumentOrder(Integer documentOrder) {
        this.documentOrder = documentOrder;
    }

    public Integer getImplInfoDocId() {
        return implInfoDocId;
    }

    public void setImplInfoDocId(Integer implInfoDocId) {
        this.implInfoDocId = implInfoDocId;
    }

    public ProjectImplInfo getImplInfoId() {
        return implInfoId;
    }

    public void setImplInfoId(ProjectImplInfo implInfoId) {
        this.implInfoId = implInfoId;
    }

    public ProjectDocuments getDocumentId() {
        return documentId;
    }

    public void setDocumentId(ProjectDocuments documentId) {
        this.documentId = documentId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (implInfoDocId != null ? implInfoDocId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProjectImplInfoDocs)) {
            return false;
        }
        ProjectImplInfoDocs other = (ProjectImplInfoDocs) object;
        if ((this.implInfoDocId == null && other.implInfoDocId != null) || (this.implInfoDocId != null && !this.implInfoDocId.equals(other.implInfoDocId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ProjectImplInfoDocs[ implInfoDocId=" + implInfoDocId + " ]";
    }
    
}
