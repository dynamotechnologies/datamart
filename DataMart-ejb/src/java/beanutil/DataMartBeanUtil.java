package beanutil;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import java.util.GregorianCalendar;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

public class DataMartBeanUtil {
    // TODO: put this in a config file
    final static String DATETIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    final static String DATE_FORMAT = "yyyy-MM-dd";


    public static int getIntFromString(String oldval, String fieldName)
            throws DataMartException
    {
        int newval;
        try {
            newval = Integer.parseInt(oldval);
        } catch (NumberFormatException ex) {
            throw new DataMartException(DataMartException.BAD_FORMAT, fieldName+" must be an integer.");
        }
        return newval;
    }


    public static Boolean getBoolFromString(String oldval, String fieldName)
            throws DataMartException
    {
        Boolean newval = false;
        if (oldval.equalsIgnoreCase("1"))
        {
            newval = true;
        } else if (!oldval.equalsIgnoreCase("0"))
        {
            throw new DataMartException(DataMartException.BAD_FORMAT, fieldName+" must be 0 or 1.");
        }
        return newval;
    }

    public static String getStringFromBool(Boolean oldval)
            throws DataMartException
    {
        return (oldval) ? "1" : "0";
    }

    public static String getStringValFromBool(Boolean oldval)
            throws DataMartException
    {
        return (oldval) ? "true" : "false";
    }
    
    public static XMLGregorianCalendar getXMLGregorianFromDate(Date date)
            throws DataMartException
    {
        if (date==null)
            return null;
        XMLGregorianCalendar xgCal = getXMLGregorianFromDateTime(date);
            // Mask time and timezone from XML date
            xgCal.setTime(
                DatatypeConstants.FIELD_UNDEFINED,
                DatatypeConstants.FIELD_UNDEFINED,
                DatatypeConstants.FIELD_UNDEFINED);
        return xgCal;
    }

    public static XMLGregorianCalendar getXMLGregorianFromDateTime(Date date)
            throws DataMartException
    {
        if (date==null)
            return null;
        GregorianCalendar gCal = new GregorianCalendar();
        gCal.setTime(date);
        XMLGregorianCalendar xgCal = null;
        try {
            xgCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCal);
            xgCal.setTimezone(
                    DatatypeConstants.FIELD_UNDEFINED);
            xgCal.setMillisecond(
                    DatatypeConstants.FIELD_UNDEFINED);
        } catch (DatatypeConfigurationException e) {
            throw new DataMartException(DataMartException.BAD_FORMAT,
                    "Error converting date '" + date.toString() + "' to an XMLGregorianDate.");
        }
        return xgCal;
    }


    public static Date getDateFromXMLGregorian(XMLGregorianCalendar xgc) {
        if (xgc==null)
            return null;
        return xgc.toGregorianCalendar().getTime();
    }


    public static void confirmNotNull(String value, String name)
            throws DataMartException
    {
        if (value==null)
            throw new DataMartException(DataMartException.MISSING_ELEMENT, name + " is required.");

        if (!(value.length()>0))
            throw new DataMartException(DataMartException.MISSING_ELEMENT, name + " is required.");
    }

    /*
     * Use the manager to fetch a single entity that has the given id as its
     * primary key value.
     * 
     * This function MUST return a single entity.  If it cannot, it throws a
     * DataMartException of type CONSTRAINT_VIOLATION
     * 
     * Parameter `id` is of type `Object`. It can accept int, Integer, String, whatever.
     *    This method could be overloaded for better type-checking, but then the id parameter would need
     *    to be converted or cast before the call to Query.setParameter. That would be redundant, because all id's
     *    should be converted to the proper format before calling this method.
     *
     * Note: Convert your id's to the proper format before calling this method! In other words,
     *    do not send me a String if the jpa entity object has the ID defined as an int.
     */
    public static Object getEntityById(EntityManager mgr, String jpaname, Object id, String friendlyname)
            throws DataMartException
    {
        //
        // TODO: use jee 1.6 CriteriaBuilder for type-safe query building rather than named queries?
        //
        Query query = mgr.createNamedQuery(jpaname+".findById");
        query.setParameter("id", id);
        String noneFoundMsg = friendlyname+" '"+id+"' does not exist.";
        String multipleFoundMsg = "Multiple rows found for field '"+friendlyname+"' with id='"+id+"'.";
        
        return getEntityByQuery(query, noneFoundMsg, multipleFoundMsg);
    }


    /*
     *
     * Runs the query and returns a single object. If some non-singular number of rows
     *    is returned, an exception will be thrown containing noneFoundMsg or multipleFoundMsg.
     */
    public static Object getEntityByQuery(Query q, String noneFoundMsg, String multipleFoundMsg)
            throws DataMartException
    {
        Object response = getEntityByQueryAllowNull(q, multipleFoundMsg);
        if (response==null)
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION, noneFoundMsg);

        return response;
    }
    

    /*
     *
     * Runs the query and returns a single object, or NULL if none are found. If multiple rows
     *    are returned, an exception will be thrown containing multipleFoundMsg.
     */
    public static Object getEntityByQueryAllowNull(Query q, String multipleFoundMsg)
            throws DataMartException
    {
        Object response = null;
        try {
            response = q.getSingleResult();
        } catch (NoResultException ex) {
            // The response is already null. I have nothing to do.
        } catch (NonUniqueResultException ex) {
            throw new DataMartException(DataMartException.UNKNOWN_ERROR, multipleFoundMsg);
        }
        return response;
    }


    /*
     * TODO: Consider making this an abstract class which can be implemented by the managers.
     *       This would at least preclude the passing around of EntityManagers, etc as method parameters.
     */

    // Accepts two String parameters type and ID. Returns the id of the associated Project entity,
    //   or null if it doesn't exist
    public static entity.Projects getProject(EntityManager mgr, String type, String projectid)
            throws DataMartException
    {
        Query query = mgr.createQuery("SELECT p FROM Projects p WHERE p.projectTypes = :type and p.publicId = :publicid");
        query.setParameter("type", new entity.ProjectTypes(type));
        query.setParameter("publicid", projectid);

        entity.Projects project = (entity.Projects) getEntityByQueryAllowNull(query,
                "More than one project found with type '"+type+"' and id '"+projectid+"'");

        return (project==null) ? null : project;
    }


    public static entity.Projects getCaraProject(EntityManager mgr, int caraProjectId)
            throws DataMartException
    {
        try {
            entity.CARAProjects proj = (entity.CARAProjects) getEntityById(mgr, "CARAProjects", caraProjectId, "CARA Project");
            return proj.getProjects();
        } catch (javax.persistence.NoResultException e) {
            return null;
        }
    }


    public static entity.ProjectDocuments getProjectDocument(EntityManager mgr, entity.Projects project, String docid)
            throws DataMartException
    {
        // TODO: Use named query
        Query query = mgr.createQuery("SELECT p FROM ProjectDocuments p WHERE p.projects = :projects and p.documentId = :documentId");
        query.setParameter("projects", project);
        query.setParameter("documentId", docid);

        entity.ProjectDocuments projdoc = (entity.ProjectDocuments) getEntityByQueryAllowNull(query,
                "More than one ProjectDocuments found with project id '"+project.getId()+"' and doc id '"+docid+"'.");

        return (projdoc==null) ? null : projdoc;
    }

    /*
     * Get CARA Comment Phase by CARA ID
     */
    public static entity.CommentPhases getCommentPhase(EntityManager mgr, int caraid, int phaseid)
            throws DataMartException
    {
        Query query = mgr.createQuery("SELECT p FROM CommentPhases p WHERE p.cARAProjects.id = :caraid AND p.phaseId = :phaseid");
        query.setParameter("caraid", caraid);
        query.setParameter("phaseid", phaseid);

        entity.CommentPhases phase = (entity.CommentPhases) getEntityByQueryAllowNull(query,
                "More than one phase found with CARA ID '"+caraid+"' and phase ID '"+phaseid+"'");

        return (phase==null) ? null : phase;
    }

    public static entity.Projects removeProjectLocationRelationships(EntityManager em, entity.Projects p)
    {
        // Remove regions
        removeProjectLocationRelationships(em, p, "ProjectRegions");
        removeProjectLocationRelationships(em, p, "ProjectForests");
        removeProjectLocationRelationships(em, p, "ProjectDistricts");
        removeProjectLocationRelationships(em, p, "ProjectStates");
        removeProjectLocationRelationships(em, p, "ProjectCounties");
        return p;
    }

    private static void removeProjectLocationRelationships(EntityManager em, entity.Projects p, String entitybean)
    {
        Query q = em.createQuery("DELETE FROM " + entitybean + " p WHERE p.projects = :project");
        q.setParameter("project", p);
        q.executeUpdate();
    }

    public static String getConfig(EntityManager mgr, String key)
        throws DataMartException
    {
        Query query = mgr.createQuery("SELECT c FROM RefConfigs c WHERE c.id = :key");
        query.setParameter("key", key);

        entity.RefConfigs config = (entity.RefConfigs) getEntityByQueryAllowNull(query,
                "Error reading config '" + key + "'");

        if (config == null) {
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND, "Missing config value for " + key);
        } else {
            return config.getValue();
        }
    }

    public static Date getDateFromString(String oldval)
            throws DataMartException
    {
        String ov = fixDateString(oldval);
        String fmt = (hasTimePart(ov)) ? DATETIME_FORMAT : DATE_FORMAT;
        Date newval;
        try {
            DateFormat df = new SimpleDateFormat(fmt);
            newval = df.parse(ov);
        } catch (ParseException ex) {
            throw new DataMartException("Could not convert date '" + oldval + "'");
        }
        return newval;
    }

    /*
     * Private methods for getDateFromString
     */
    private static String fixDateString(String oldval)
            throws DataMartException
    {
        String ov = removeGarbage(oldval);
        String fmt = (hasTimePart(ov)) ? DATETIME_FORMAT : DATE_FORMAT;
        
        Date olddate;
        try {
            DateFormat df = new SimpleDateFormat(fmt);
            olddate = df.parse(ov);
        } catch (ParseException ex) {
            throw new DataMartException("Could not parse date '" + ov + "' using date format " +
                    fmt + ". Original xml looked like this: '" + oldval + "'.");
        }

        return new SimpleDateFormat(fmt).format(olddate);
    }


    private static String removeGarbage(String oldval)
            throws DataMartException
    {
        String ov = oldval;
        ov = ov.replaceAll("'T'00:00:00", "");
        ov = ov.replaceAll("'T'", "T");
        ov = ov.replaceAll("/", "-");
        ov = fixMissingZeros(ov);
        ov = fixInvertedDate(ov);
        return ov;
    }


    /*
     * Some dates are submitted like this: 06-10-2009
     * We need to make them look like this: 2009-06-10
     */
    private static String fixInvertedDate(String oldval)
            throws DataMartException
    {

        String datepart = oldval;
        String rest = "";
        if (oldval.length()>10) {
            datepart = oldval.substring(0, 10);
            rest = oldval.substring(10, oldval.length());
        }

        String[] arr = datepart.split("-");
        if (arr.length!=3)
            throw new DataMartException("Could not invert date '" + oldval + "'.");

        // If the date is indeed inverted, flip it.
        if (arr[2].length()==4 && arr[0].length()<=2)
            datepart = arr[2] + "-" + arr[0] + "-" + arr[1];
        
        return datepart + rest;
    }


    /*
     * Leading zeros are sometimes dropped from the timestamps in the xml. Here are some examples:
     * 2010-09-30T4:07:45 (Should be 04:07:45)
     * 2010-09-29T11:19:7
     *
     * This method adds leading zeros where it's appropriate to do so.
     */
    private static String fixMissingZeros(String oldval)
            throws DataMartException
    {
        if (!hasTimePart(oldval))
            return oldval;

        // Split the date/timestamp into an array {date,time}
        String[] tokens = oldval.split("T");
        if (tokens.length!=2)
            throw new DataMartException("Could not fix missing zeros in date '" + oldval + "'. There should be two dashes in the date part.");

        String[] timepart = tokens[1].split(":");
        if (timepart.length!=3)
            throw new DataMartException("Could not fix missing zeros in date '" + oldval + "'. There should be two colons in the time part.");

        // Loop over the time part
        for (int i=0; i<3; i++)
            if (timepart[i].length()==1)
                timepart[i] = "0"+timepart[i];

        return tokens[0]+"T"+timepart[0]+":"+timepart[1]+":"+timepart[2];
    }

    
    private static Boolean hasTimePart(String s)
    {
        return (s.contains(" ") || s.contains("T")) ? true : false;
    }    
}