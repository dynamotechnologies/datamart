/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.entity;

import client.ProjectImplInfoClient;
import client.ProjectMeetingNoticesClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.ProjectMeetingNotices.Meetingnotice;
import us.fed.fs.www.nepa.schema.ProjectMeetingNotices.Meetingnotices;

/**
 *
 * @author gauri
 */

public class ProjectMeetingNoticesTest {
    private ProjectMeetingNoticesClient client;
    

    @Before
    public void setUp()
            throws IOException
    {
        client = new ProjectMeetingNoticesClient();
       
    }

    @Test
    public void testPost()
    {
        // This POST should fail, because it violates a constraint. The unit code is bad
        try {
           addProjectMeetingNotices();
            // If the above statement did not throw an exception then something has gone terribly wrong
            //Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.BAD_PARENTS)
                Assert.fail();
        } catch (Exception ex) {
            Logger.getLogger(ProjectMeetingNoticesTest.class.getName()).log(Level.SEVERE, null, ex);
             Assert.fail();
        }

        

        
    }

    @Test
    public void testGet()
    {
     

        Meetingnotice project = null;
         
        try {
            project = client.getProjectMeetingNotices("40090","128");
        } catch (IOException ex) {
            Logger.getLogger(ProjectMeetingNoticesTest.class.getName()).log(Level.SEVERE, null, ex);
             Assert.fail();
        }
            
          
       

       
     

       
    }

    @Test
    public void testPut()
    {
        

        Meetingnotice project = null;
        try {
            modifyProjectMeetingNotices();
            project = client.getProjectMeetingNotices("40090","128");
        } catch (Exception ex) {
            Logger.getLogger(ProjectMeetingNoticesTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

       
    }

   
    
 


    @Test
    public void testDelete()
    {
        

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            deleteProjectMeetingNotices();
          
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        } catch (Exception ex) {
            Logger.getLogger(ProjectMeetingNoticesTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
      
    }

 
    @After
    public void cleanFailed()
            throws Exception
    {
      
    }


    @BeforeClass
    public static void populateDependencies()
    {
        // No dependencies
    }

    @AfterClass
    public static void cleanupDependencies()
    {
        // No cleanup
    }
    
     public static void addProjectMeetingNotices() throws Exception{
	    	
	    	client.ProjectMeetingNoticesClient  client = new client.ProjectMeetingNoticesClient();
	    	Meetingnotice p = new Meetingnotice();
//	    	    p.setCity			("Ashburn");    
//                    p.setDirections		("test");    
//                    p.setDocumentId		("17735");    
//                    p.setLastUpdateDate		("2016-07-10");    
//                    p.setLastUpdatedBy		("oshroff");    
//                    p.setMeetingDate		("2016-07-10");    
//                    p.setMeetingDescription	("test1");    
//                    p.setMeetingEndTime		("10:00 A.M");    
//                    p.setMeetingId		("10");    
//                    p.setMeetingOrder		("1");    
//                    p.setMeetingPubFlag		("1");    
//                    p.setMeetingStartTime	("09:00 A.M");    
//                    p.setMeetingTitle		("test3");    
//                    p.setState			("VA");    
//                    p.setStreet1		("test4");    
//                    p.setStreet2		("test5");    
//                    p.setZip			("20148"); 
                    p.setCity			("");    
                    p.setDirections		("test datamart3");    
                    p.setDocumentId		("40090");    
                    p.setLastUpdateDate		("2016-07-12");    
                    p.setLastUpdatedBy		("oshroff");    
                    p.setMeetingDate		("2016-07-12");    
                    p.setMeetingDescription	("test datamart3");    
                    p.setMeetingEndTime		("12:20");    
                    p.setMeetingId		("128");    
                    p.setMeetingOrder		("6");    
                    p.setMeetingPubFlag		("0");    
                    p.setMeetingStartTime	("10.30");    
                    p.setMeetingTitle		("test datamart3");    
                    p.setState			("AK");    
                    p.setStreet1		("ddd");    
                    p.setStreet2		("dd");    
                    p.setZip			(""); 

	    	client.postProjectMeetingNotices(p);
	    }
    
	 public static void deleteProjectMeetingNotices() throws Exception{
	    	
	    	client.ProjectMeetingNoticesClient  client = new client.ProjectMeetingNoticesClient();
                client.deleteProjectMeetingNotices("40090","128");
         }   
         
          public static void modifyProjectMeetingNotices() throws Exception{
	       client.ProjectMeetingNoticesClient  client = new client.ProjectMeetingNoticesClient();
	    	Meetingnotice p = new Meetingnotice();
                    p.setCity			("Ashburn");    
                    p.setDirections		("test");    
                    p.setDocumentId		("40090");    
                    p.setLastUpdateDate		("2016-07-10");    
                    p.setLastUpdatedBy		("oshroff");    
                    p.setMeetingDate		("2016-07-10");    
                    p.setMeetingDescription	("test1");    
                    p.setMeetingEndTime		("11:00 A.M");    
                    p.setMeetingId		("128");    
                    p.setMeetingOrder		("5");    
                    p.setMeetingPubFlag		("1");    
                    p.setMeetingStartTime	("10:00 A.M");    
                    p.setMeetingTitle		("test3");    
                    p.setState			("VA");    
                    p.setStreet1		("test4");    
                    p.setStreet2		("test5");    
                    p.setZip("20148");
	    	client.putProjectMeetingNotices(p);
         } 
}