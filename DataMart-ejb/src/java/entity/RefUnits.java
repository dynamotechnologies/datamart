package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "RefUnits")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RefUnits.findAll", query = "SELECT r FROM RefUnits r"),
    @NamedQuery(name = "RefUnits.findById", query = "SELECT r FROM RefUnits r WHERE r.id = :id"),
    @NamedQuery(name = "RefUnits.findByName", query = "SELECT r FROM RefUnits r WHERE r.name = :name"),
    @NamedQuery(name = "RefUnits.findByExtendedDetails", query = "SELECT r FROM RefUnits r WHERE r.extendedDetails = :extendedDetails"),
    @NamedQuery(name = "RefUnits.findByProjectMap", query = "SELECT r FROM RefUnits r WHERE r.projectMap = :projectMap"),
    @NamedQuery(name = "RefUnits.findByAddress1", query = "SELECT r FROM RefUnits r WHERE r.address1 = :address1"),
    @NamedQuery(name = "RefUnits.findByAddress2", query = "SELECT r FROM RefUnits r WHERE r.address2 = :address2"),
    @NamedQuery(name = "RefUnits.findByCity", query = "SELECT r FROM RefUnits r WHERE r.city = :city"),
    @NamedQuery(name = "RefUnits.findByState", query = "SELECT r FROM RefUnits r WHERE r.state = :state"),
    @NamedQuery(name = "RefUnits.findByZip", query = "SELECT r FROM RefUnits r WHERE r.zip = :zip"),
    @NamedQuery(name = "RefUnits.findByPhone", query = "SELECT r FROM RefUnits r WHERE r.phone = :phone"),
    @NamedQuery(name = "RefUnits.findByWwwLink", query = "SELECT r FROM RefUnits r WHERE r.wwwLink = :wwwLink"),
    @NamedQuery(name = "RefUnits.findByCommentEmail", query = "SELECT r FROM RefUnits r WHERE r.commentEmail = :commentEmail"),
    @NamedQuery(name = "RefUnits.findByNewspaper", query = "SELECT r FROM RefUnits r WHERE r.newspaper = :newspaper"),
    @NamedQuery(name = "RefUnits.findByNewspaperURL", query = "SELECT r FROM RefUnits r WHERE r.newspaperURL = :newspaperURL"),
    @NamedQuery(name = "RefUnits.findByBoundaryURL", query = "SELECT r FROM RefUnits r WHERE r.boundaryURL = :boundaryURL"),
    @NamedQuery(name = "RefUnits.findByActive", query = "SELECT r FROM RefUnits r WHERE r.active = :active")})
public class RefUnits implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "unitId")
    private Collection<ContainerTemplates> containerTemplatesCollection;
    @Column(name = "SpotlightId1")
    private String spotlightId1;
    @Column(name = "SpotlightId2")
    private String spotlightId2;
    @Column(name = "SpotlightId3")
    private String spotlightId3;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private String id;
    @Basic(optional = false)
    @Column(name = "Name")
    private String name;
    @Basic(optional = false)
    @Column(name = "ExtendedDetails")
    private boolean extendedDetails;
    @Basic(optional = false)
    @Column(name = "ProjectMap")
    private boolean projectMap;
    @Column(name = "Address1")
    private String address1;
    @Column(name = "Address2")
    private String address2;
    @Column(name = "City")
    private String city;
    @Column(name = "State")
    private String state;
    @Column(name = "Zip")
    private String zip;
    @Column(name = "Phone")
    private String phone;
    @Column(name = "wwwLink")
    private String wwwLink;
    @Column(name = "CommentEmail")
    private String commentEmail;
    @Column(name = "Newspaper")
    private String newspaper;
    @Column(name = "NewspaperURL")
    private String newspaperURL;
    @Column(name = "BoundaryURL")
    private String boundaryURL;
    @Basic(optional = false)
    @Column(name = "Active")
    private boolean active;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refUnits")
    private Collection<ResourceAreas> resourceAreasCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refUnits")
    private Collection<Projects> projectsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refUnits")
    private Collection<ProjectRegions> projectRegionsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refUnits")
    private Collection<UnitKMLs> unitKMLsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refUnits")
    private Collection<UnitGoals> unitGoalsCollection;
    @OneToMany(mappedBy = "refUnits")
    private Collection<Appeals> appealsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refUnits")
    private Collection<ProjectForests> projectForestsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refUnits")
    private Collection<ProjectDistricts> projectDistrictsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refUnits")
    private Collection<UnitRoles> unitRolesCollection;

    public RefUnits() {
    }

    public RefUnits(String id) {
        this.id = id;
    }

    public RefUnits(String id, String name, boolean extendedDetails, boolean projectMap, boolean active) {
        this.id = id;
        this.name = name;
        this.extendedDetails = extendedDetails;
        this.projectMap = projectMap;
        this.active = active;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getExtendedDetails() {
        return extendedDetails;
    }

    public void setExtendedDetails(boolean extendedDetails) {
        this.extendedDetails = extendedDetails;
    }

    public boolean getProjectMap() {
        return projectMap;
    }

    public void setProjectMap(boolean projectMap) {
        this.projectMap = projectMap;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWwwLink() {
        return wwwLink;
    }

    public void setWwwLink(String wwwLink) {
        this.wwwLink = wwwLink;
    }

    public String getCommentEmail() {
        return commentEmail;
    }

    public void setCommentEmail(String commentEmail) {
        this.commentEmail = commentEmail;
    }

    public String getNewspaper() {
        return newspaper;
    }

    public void setNewspaper(String newspaper) {
        this.newspaper = newspaper;
    }

    public String getNewspaperURL() {
        return newspaperURL;
    }

    public void setNewspaperURL(String newspaperURL) {
        this.newspaperURL = newspaperURL;
    }

    public String getBoundaryURL() {
        return boundaryURL;
    }

    public void setBoundaryURL(String boundaryURL) {
        this.boundaryURL = boundaryURL;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @XmlTransient
    public Collection<ResourceAreas> getResourceAreasCollection() {
        return resourceAreasCollection;
    }

    public void setResourceAreasCollection(Collection<ResourceAreas> resourceAreasCollection) {
        this.resourceAreasCollection = resourceAreasCollection;
    }

    @XmlTransient
    public Collection<Projects> getProjectsCollection() {
        return projectsCollection;
    }

    public void setProjectsCollection(Collection<Projects> projectsCollection) {
        this.projectsCollection = projectsCollection;
    }

    @XmlTransient
    public Collection<ProjectRegions> getProjectRegionsCollection() {
        return projectRegionsCollection;
    }

    public void setProjectRegionsCollection(Collection<ProjectRegions> projectRegionsCollection) {
        this.projectRegionsCollection = projectRegionsCollection;
    }

    @XmlTransient
    public Collection<UnitKMLs> getUnitKMLsCollection() {
        return unitKMLsCollection;
    }

    public void setUnitKMLsCollection(Collection<UnitKMLs> unitKMLsCollection) {
        this.unitKMLsCollection = unitKMLsCollection;
    }

    @XmlTransient
    public Collection<UnitGoals> getUnitGoalsCollection() {
        return unitGoalsCollection;
    }

    public void setUnitGoalsCollection(Collection<UnitGoals> unitGoalsCollection) {
        this.unitGoalsCollection = unitGoalsCollection;
    }

    @XmlTransient
    public Collection<Appeals> getAppealsCollection() {
        return appealsCollection;
    }

    public void setAppealsCollection(Collection<Appeals> appealsCollection) {
        this.appealsCollection = appealsCollection;
    }

    @XmlTransient
    public Collection<ProjectForests> getProjectForestsCollection() {
        return projectForestsCollection;
    }

    public void setProjectForestsCollection(Collection<ProjectForests> projectForestsCollection) {
        this.projectForestsCollection = projectForestsCollection;
    }

    @XmlTransient
    public Collection<ProjectDistricts> getProjectDistrictsCollection() {
        return projectDistrictsCollection;
    }

    public void setProjectDistrictsCollection(Collection<ProjectDistricts> projectDistrictsCollection) {
        this.projectDistrictsCollection = projectDistrictsCollection;
    }

    @XmlTransient
    public Collection<UnitRoles> getUnitRolesCollection() {
        return unitRolesCollection;
    }

    public void setUnitRolesCollection(Collection<UnitRoles> unitRolesCollection) {
        this.unitRolesCollection = unitRolesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RefUnits)) {
            return false;
        }
        RefUnits other = (RefUnits) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RefUnits[ id=" + id + " ]";
    }

    public String getSpotlightId1() {
        return spotlightId1;
    }

    public void setSpotlightId1(String spotlightId1) {
        this.spotlightId1 = spotlightId1;
    }

    public String getSpotlightId2() {
        return spotlightId2;
    }

    public void setSpotlightId2(String spotlightId2) {
        this.spotlightId2 = spotlightId2;
    }

    public String getSpotlightId3() {
        return spotlightId3;
    }

    public void setSpotlightId3(String spotlightId3) {
        this.spotlightId3 = spotlightId3;
    }

    @XmlTransient
    public Collection<ContainerTemplates> getContainerTemplatesCollection() {
        return containerTemplatesCollection;
    }

    public void setContainerTemplatesCollection(Collection<ContainerTemplates> containerTemplatesCollection) {
        this.containerTemplatesCollection = containerTemplatesCollection;
    }

}
