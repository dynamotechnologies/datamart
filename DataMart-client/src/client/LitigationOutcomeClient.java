package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.litigationoutcome.*;


public class LitigationOutcomeClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public LitigationOutcomeClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.litigationoutcome", "LitigationOutcome.xsd");
    }

    public void postLitOutcome(Integer id, String name)
            throws IOException
    {
        Litigationoutcome outcome = new Litigationoutcome();
        outcome.setId(id);
        outcome.setName(name);
        postLitOutcome(outcome);
    }


    public void postLitOutcome(Litigationoutcome outcome)
            throws IOException
    {
        post(objectfactory.createLitigationoutcome(outcome), "/ref/litigationoutcomes");
    }


    public Litigationoutcome getLitOutcome(Integer id)
            throws IOException
    {
        return get("/ref/litigationoutcomes/" + id, Litigationoutcome.class);
    }


    public Litigationoutcomes getLitOutcomeList()
            throws IOException
    {
        return get("/ref/litigationoutcomes", Litigationoutcomes.class);
    }


    public void putLitOutcome(Integer id, String name)
            throws IOException
    {
        Litigationoutcome outcome = new Litigationoutcome();
        outcome.setId(id);
        outcome.setName(name);
        putLitOutcome(outcome);
    }

    public void putLitOutcome(Litigationoutcome outcome)
            throws IOException
    {
        put(objectfactory.createLitigationoutcome(outcome), "/ref/litigationoutcomes/" + outcome.getId());
    }


    public void deleteLitOutcome(Integer id)
            throws IOException
    {
        delete("/ref/litigationoutcomes/" + id);
    }
}