package stateless;

import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.commentphase.*;
import beanutil.DataMartException;

@Remote
public interface CommentPhaseManagerRemote {
    public void create(Commentphase resource) throws DataMartException;
    public Commentphase get(int caraProjectId, Integer id) throws DataMartException;
    public void update(Commentphase resource) throws DataMartException;
    public void delete(int caraProjectId, Integer id) throws DataMartException;
    public Commentphases getAll(int caraProjectId) throws DataMartException;
    public Boolean ping();
}