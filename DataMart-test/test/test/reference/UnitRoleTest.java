package test.reference;

import client.UnitRoleClient;
import client.UserClient;
import client.RoleClient;
import client.UnitClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.AfterClass;
import org.junit.Test;
import test.TestData;


public class UnitRoleTest {

    private UnitRoleClient client;


    @Before
    public void setUp()
            throws IOException
    {
        client = new UnitRoleClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postUnitRole(TestData.unitroles.id1, TestData.users.shortname1, TestData.units.code1, TestData.roles.id1);
            // If the above rolement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        us.fed.fs.www.nepa.schema.unitrole.Unitrole unitrole = null;
        try {
            unitrole = client.getUnitRole(TestData.unitroles.id1);
        } catch (IOException ex) {
            Logger.getLogger(UnitRoleTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.unitroles.id1, unitrole.getId().intValue());
        Assert.assertEquals(TestData.roles.id1, unitrole.getRole());
        Assert.assertEquals(TestData.users.shortname1, unitrole.getShortname());
        Assert.assertEquals(TestData.units.code1, unitrole.getUnit());

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        us.fed.fs.www.nepa.schema.unitrole.Unitrole unitrole = null;
        try {
            client.putUnitRole(TestData.unitroles.id1, TestData.users.shortname1, TestData.units.code1, TestData.roles.id2);
            unitrole = client.getUnitRole(TestData.unitroles.id1);
        } catch (Exception ex) {
            Logger.getLogger(UnitRoleTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.roles.id2, unitrole.getRole());

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getUnitRole(TestData.unitroles.id1);
            // If the above rolement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteUnitRole(TestData.unitroles.id1);
            // If the above rolement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }

    private void post()
    {
        try {
            client.postUnitRole(TestData.unitroles.id1, TestData.users.shortname1, TestData.units.code1, TestData.roles.id1);
        } catch (IOException ex) {
            Logger.getLogger(RoleTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteUnitRole(TestData.unitroles.id1);
        } catch (IOException ex) {
            Logger.getLogger(RoleTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteUnitRole(TestData.unitroles.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }
    

    @BeforeClass
    public static void populateDependencies()
    {
        UserClient userclient;
        RoleClient roleclient;
        UnitClient unitclient;

        try {
            userclient = new UserClient();
            roleclient = new RoleClient();
            unitclient = new UnitClient();

            // create unit
            unitclient.postUnit(TestData.units.code1, TestData.units.name1);
            // create roles
            roleclient.postRole(TestData.roles.id1, TestData.roles.name1, TestData.roles.description1);
            roleclient.postRole(TestData.roles.id2, TestData.roles.name2, TestData.roles.description2);
            // create user
            userclient.postUser(TestData.users.shortname1, TestData.users.firstname1, TestData.users.lastname1, TestData.users.phone1, TestData.users.title1, TestData.users.email1);
        } catch (Exception ex) {
            Logger.getLogger(UnitRoleTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    @AfterClass
    public static void cleanupDependencies()
    {
        UserClient userclient;
        RoleClient roleclient;
        UnitClient unitclient;

        try {
            userclient = new UserClient();
            roleclient = new RoleClient();
            unitclient = new UnitClient();
            // delete user
            userclient.deleteUser(TestData.users.shortname1);
            // delete roles
            roleclient.deleteRole(TestData.roles.id1);
            roleclient.deleteRole(TestData.roles.id2);
            // delete unit
            unitclient.deleteUnit(TestData.units.code1);
        } catch (Exception ex) {
            Logger.getLogger(UnitRoleTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }
}