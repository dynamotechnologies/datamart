package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "RefPurposes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RefPurposes.findAll", query = "SELECT r FROM RefPurposes r"),
    @NamedQuery(name = "RefPurposes.findById", query = "SELECT r FROM RefPurposes r WHERE r.id = :id"),
    @NamedQuery(name = "RefPurposes.findByName", query = "SELECT r FROM RefPurposes r WHERE r.name = :name")})
public class RefPurposes implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "purposeId")
    private Collection<CustomProjectPurposes> customProjectPurposesCollection;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private String id;
    @Basic(optional = false)
    @Column(name = "Name")
    private String name;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refPurposes")
    private Collection<ProjectPurposes> projectPurposesCollection;

    public RefPurposes() {
    }

    public RefPurposes(String id) {
        this.id = id;
    }

    public RefPurposes(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Collection<ProjectPurposes> getProjectPurposesCollection() {
        return projectPurposesCollection;
    }

    public void setProjectPurposesCollection(Collection<ProjectPurposes> projectPurposesCollection) {
        this.projectPurposesCollection = projectPurposesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RefPurposes)) {
            return false;
        }
        RefPurposes other = (RefPurposes) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RefPurposes[ id=" + id + " ]";
    }

    @XmlTransient
    public Collection<CustomProjectPurposes> getCustomProjectPurposesCollection() {
        return customProjectPurposesCollection;
    }

    public void setCustomProjectPurposesCollection(Collection<CustomProjectPurposes> customProjectPurposesCollection) {
        this.customProjectPurposesCollection = customProjectPurposesCollection;
    }

}
