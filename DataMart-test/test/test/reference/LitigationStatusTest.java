package test.reference;

import client.LitigationStatusClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.litigationstatus.*;


public class LitigationStatusTest {
    private LitigationStatusClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new LitigationStatusClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postLitStatus(TestData.litstatuses.id1, TestData.litstatuses.name2);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Litigationstatus status = null;
        Litigationstatuses statuslist = null;
        try {
            status = client.getLitStatus(TestData.litstatuses.id1);
            statuslist = client.getLitStatusList();
        } catch (IOException ex) {
            Logger.getLogger(LitigationStatusTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.litstatuses.id1, status.getId());
        Assert.assertEquals(TestData.litstatuses.name1, status.getName());
        Assert.assertTrue(statuslist.getLitigationstatus().get(0).getId()>0);
        Assert.assertTrue(statuslist.getLitigationstatus().get(0).getName().length()>0);

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        Litigationstatus status = null;
        try {
            client.putLitStatus(TestData.litstatuses.id1, TestData.litstatuses.name2);
            status = client.getLitStatus(TestData.litstatuses.id1);
        } catch (Exception ex) {
            Logger.getLogger(LitigationStatusTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.litstatuses.name2, status.getName());

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getLitStatus(TestData.litstatuses.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteLitStatus(TestData.litstatuses.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }

    private void post()
    {
        try {
            client.postLitStatus(TestData.litstatuses.id1, TestData.litstatuses.name1);
        } catch (IOException ex) {
            Logger.getLogger(LitigationStatusTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteLitStatus(TestData.litstatuses.id1);
        } catch (IOException ex) {
            Logger.getLogger(LitigationStatusTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteLitStatus(TestData.litstatuses.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }
}