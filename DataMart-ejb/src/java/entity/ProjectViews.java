/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author gauri
 */
@Entity
@Table(name = "ProjectViews")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProjectViews.findAll", query = "SELECT p FROM ProjectViews p"),
    @NamedQuery(name = "ProjectViews.findByViewId", query = "SELECT p FROM ProjectViews p WHERE p.viewId = :viewId"),
    @NamedQuery(name = "ProjectViews.findByViewName", query = "SELECT p FROM ProjectViews p WHERE p.viewName = :viewName"),
    @NamedQuery(name = "ProjectViews.findByCreatedDate", query = "SELECT p FROM ProjectViews p WHERE p.createdDate = :createdDate")})
public class ProjectViews implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ViewId")
    private Integer viewId;
    @Size(max = 1000)
    @Column(name = "ViewName")
    private String viewName;
    @Column(name = "CreatedDate")
    @Temporal(TemporalType.DATE)
    private Date createdDate;
    @JoinColumn(name = "UserId", referencedColumnName = "Id")
    @ManyToOne
    private Users userId;
    @JoinColumn(name = "ProjectId", referencedColumnName = "Id")
    @ManyToOne
    private Projects projectId;
    @OneToMany(mappedBy = "viewId")
    private Collection<ProjectViewDocs> projectViewDocsCollection;

    public ProjectViews() {
    }

    public ProjectViews(Integer viewId) {
        this.viewId = viewId;
    }

    public Integer getViewId() {
        return viewId;
    }

    public void setViewId(Integer viewId) {
        this.viewId = viewId;
    }

    public String getViewName() {
        return viewName;
    }

    public void setViewName(String viewName) {
        this.viewName = viewName;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Users getUserId() {
        return userId;
    }

    public void setUserId(Users userId) {
        this.userId = userId;
    }

    public Projects getProjectId() {
        return projectId;
    }

    public void setProjectId(Projects projectId) {
        this.projectId = projectId;
    }

    @XmlTransient
    public Collection<ProjectViewDocs> getProjectViewDocsCollection() {
        return projectViewDocsCollection;
    }

    public void setProjectViewDocsCollection(Collection<ProjectViewDocs> projectViewDocsCollection) {
        this.projectViewDocsCollection = projectViewDocsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (viewId != null ? viewId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProjectViews)) {
            return false;
        }
        ProjectViews other = (ProjectViews) object;
        if ((this.viewId == null && other.viewId != null) || (this.viewId != null && !this.viewId.equals(other.viewId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ProjectViews[ viewId=" + viewId + " ]";
    }
    
}
