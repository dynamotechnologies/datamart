package entity;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "ProjectMilestones")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProjectMilestones.findAll", query = "SELECT p FROM ProjectMilestones p"),
    @NamedQuery(name = "ProjectMilestones.findById", query = "SELECT p FROM ProjectMilestones p WHERE p.id = :id"),
    @NamedQuery(name = "ProjectMilestones.findByMilestoneDate", query = "SELECT p FROM ProjectMilestones p WHERE p.milestoneDate = :milestoneDate"),
    @NamedQuery(name = "ProjectMilestones.findByMilestoneStatus", query = "SELECT p FROM ProjectMilestones p WHERE p.milestoneStatus = :milestoneStatus"),
    @NamedQuery(name = "ProjectMilestones.findByHistory", query = "SELECT p FROM ProjectMilestones p WHERE p.history = :history")})
public class ProjectMilestones implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Column(name = "MilestoneDate")
    private String milestoneDate;
    @Basic(optional = false)
    @Column(name = "MilestoneStatus")
    private char milestoneStatus;
    @Basic(optional = false)
    @Column(name = "History")
    private char history;
    @JoinColumn(name = "ProjectId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Projects projects;
    @JoinColumn(name = "MilestoneId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private RefMilestones refMilestones;

    public ProjectMilestones() {
    }

    public ProjectMilestones(Integer id) {
        this.id = id;
    }

    public ProjectMilestones(Integer id, char milestoneStatus, char history) {
        this.id = id;
        this.milestoneStatus = milestoneStatus;
        this.history = history;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMilestoneDate() {
        return milestoneDate;
    }

    public void setMilestoneDate(String milestoneDate) {
        this.milestoneDate = milestoneDate;
    }

    public char getMilestoneStatus() {
        return milestoneStatus;
    }

    public void setMilestoneStatus(char milestoneStatus) {
        this.milestoneStatus = milestoneStatus;
    }

    public char getHistory() {
        return history;
    }

    public void setHistory(char history) {
        this.history = history;
    }

    public Projects getProjects() {
        return projects;
    }

    public void setProjects(Projects projects) {
        this.projects = projects;
    }

    public RefMilestones getRefMilestones() {
        return refMilestones;
    }

    public void setRefMilestones(RefMilestones refMilestones) {
        this.refMilestones = refMilestones;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProjectMilestones)) {
            return false;
        }
        ProjectMilestones other = (ProjectMilestones) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ProjectMilestones[ id=" + id + " ]";
    }

}
