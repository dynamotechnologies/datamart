/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.io.IOException;

import us.fed.fs.www.nepa.schema.ProjectAddInfoDoc.*;

/**
 *
 * @author gauri
 */

public class ProjectAddInfoDocsClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public ProjectAddInfoDocsClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.ProjectAddInfoDoc", "ProjectAddInfoDoc.xsd");
    }


   


    public void postAddInfoDocs(String projectId, ProjectAddInfoDoc projectAddInfoDoc)
            throws IOException
    {
        post(objectfactory.createProjectAddInfoDoc(projectAddInfoDoc), "/projects/nepa/"+projectId+"/projectAdditionainfo/docs");
    }


    public ProjectAddInfoDocs getAddInfoDocs(String projectId)
            throws IOException
    {
        return get("/projects/nepa/"+projectId+"/projectAdditionainfo/docs/" , ProjectAddInfoDocs.class);
    }



    


    public void putAddInfoDocs(String projectId,ProjectAddInfoDoc projectAddInfoDoc)
            throws IOException
    {
        put(objectfactory.createProjectAddInfoDoc(projectAddInfoDoc), "/projects/nepa/"+projectId+"/projectAdditionainfo/docs/" + projectAddInfoDoc.getAddInfoDocId());
    }


    public void deleteAddInfoDocs(String projectId,String docId)
            throws IOException
    {
        delete("/projects/nepa/"+projectId+"/projectAdditionainfo/docs/" + docId);
    }
    
    
          
}