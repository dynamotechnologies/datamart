package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.ProjectSpecialAuthorityManagerRemote;
import us.fed.fs.www.nepa.schema.projectspecialauthority.ObjectFactory;
import us.fed.fs.www.nepa.schema.projectspecialauthority.Projectspecialauthorities;
import us.fed.fs.www.nepa.schema.projectspecialauthority.Projectspecialauthority;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;


@Path ("/projects/nepa/{projectid}/specialauthorities")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class ProjectSpecialAuthorityResource extends AbstractResource
{
    private ProjectSpecialAuthorityManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public ProjectSpecialAuthorityResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "ProjectSpecialAuthority.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.projectspecialauthority");
        MY_PROPS.setProperty("bean", "ProjectSpecialAuthorityManagerRemote");
        try {
            beanmanager = (ProjectSpecialAuthorityManagerRemote) init(ProjectSpecialAuthorityManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("ProjectSpecialAuthorityResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("ProjectSpecialAuthorityResource failed to start. Could not ping the bean manager!");
    }


    @PUT
    @Path("/{id}")
    public Response put(@PathParam("projectid") String projectid,
                        @PathParam("id") String id,
                        InputStream inputxml,
                        @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        Projectspecialauthority specialauth = null;
        try {
            specialauth = (Projectspecialauthority) unmarshal(inputxml, Projectspecialauthority.class);

            // project id may be excluded from the xml...
            if (specialauth.getProjectid()==null || specialauth.getProjectid().length()==0)
                specialauth.setProjectid(projectid);

            // ...but if specified it must match the url pattern
            if (!projectid.equalsIgnoreCase(specialauth.getProjectid()))
                complainLoudly(new Exception("Project Id in uri must match Project Id in xml."),
                        Response.Status.UNSUPPORTED_MEDIA_TYPE);

            if (!id.equalsIgnoreCase(specialauth.getSpecialauthorityid()))
                complainLoudly(new Exception("Id in uri must match id in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.merge(specialauth);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"/"+specialauth.getSpecialauthorityid()+"</location>");
    }


    @GET
    public Response get(@PathParam("projectid") String projectid, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get a dump of all the entities in the datamart
            Projectspecialauthorities specialauthlist = beanmanager.getByProject(projectid);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Projectspecialauthorities> jaxbelement = objectfactory.createProjectspecialauthorities(specialauthlist);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @GET
    @Path("/{id}")
    public Response get(@PathParam("projectid") String projectid, @PathParam("id") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the name of the specified entity
            Projectspecialauthority specialauth = beanmanager.get(projectid, id);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Projectspecialauthority> jaxbelement = objectfactory.createProjectspecialauthority(specialauth);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("projectid") String projectid, @PathParam("id") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(projectid, id);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}