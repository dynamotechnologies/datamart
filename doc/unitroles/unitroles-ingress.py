#!/usr/bin/python

import sys, os
import urllib
import urllib2
from urllib2 import HTTPError
fname = os.path.join(os.path.dirname(__file__), "/var/www/common/dmUtil")
sys.path.insert(0, fname)
import dmUtil

def usage():
	print """
	Usage: unitroles-ingress.py [FILENAME]
	Usage: cat inputfile | unitroles-ingress.py

	This program takes a line of vbar-delimited input and inserts
	the values into the UnitRoles table

	Format is: AG_ID|USER_NAME|GROUP_VALUE|ROLE_ID|ROLE_NAME

	The AG_ID is an integer
	The USER_NAME is a shortname
	The GROUP_VALUE is a unit ID
	The ROLE_ID is an integer that references the Roles reference table

	We don't need role name
	"""

#
#       Make sure input looks like the expected CSV file
#
def validateFile(inputstream):
        line1 = inputstream.readline().strip()
        if (line1 != 'QUERY RESULT||||'):
                print 'Input file looks wrong, check 1st line'
                sys.exit(1)
        line2 = inputstream.readline().strip()
        if (line2 != 'AG_ID|USER_NAME|GROUP_VALUE|ROLE_ID|ROLE_NAME'):
                print 'Input file looks wrong, check 2nd line'
                sys.exit(1)


#
#	Generate the XML for POST and PUT requests
#
def buildXML(id,shortname,unit,role):
	return '<?xml version="1.0" encoding="UTF-8"?><unitrole xmlns="http://www.fs.fed.us/nepa/schema"><id>'+id+'</id><shortname>'+shortname+'</shortname><unit>'+unit+'</unit><role>'+role+'</role></unitrole>'


"""
====
MAIN
====
"""

if (len(sys.argv) > 2):
	usage()
	sys.exit()

inputstream = None

if (len(sys.argv) > 1):
	fname = sys.argv[1]
	print "Getting input from file " + fname
	inputstream = open(fname, "r")
else:
	print "Getting input from stdin"
	inputstream = sys.stdin

validateFile(inputstream)
dmUtil.initOpener()
UNITROLE_URL = 'http://localhost:7001/api/1_0/unitroles'

for line in inputstream:
	# print line
	line = line.strip()
	splitvals = line.split('|',5)
	splitvals.extend([''] * 5)
	[AG_ID,USER_NAME,GROUP_VALUE,ROLE_ID,ROLE_NAME,etc] = splitvals[0:6]
	print "AG_ID = " + AG_ID
	print "name = " + USER_NAME
	print "unit = " + GROUP_VALUE
	print "role ID = " + ROLE_ID
	print "role name (not used) = " + ROLE_NAME
	xml = buildXML(AG_ID, USER_NAME, GROUP_VALUE, ROLE_ID)
	try:
		print "MERGE output is: " + dmUtil.merge(UNITROLE_URL, AG_ID, xml)
	except HTTPError, e:
                # Write bad line to error file with e.code
		print >> sys.stderr, line + "|ERROR|" + str(e.code)
		# raise e

