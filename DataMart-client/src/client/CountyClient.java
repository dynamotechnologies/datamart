package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.county.*;


public class CountyClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public CountyClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.county", "County.xsd");
    }


    public void postCounty(String id, String name)
            throws IOException
    {
        County county = new County();
        county.setId(id);
        county.setName(name);
        postCounty(county);
    }

    public void postCounty(County county)
            throws IOException
    {
        post(objectfactory.createCounty(county), "/ref/counties");
    }


    public County getCounty(String id)
            throws IOException
    {
        return get("/ref/counties/" + id, County.class);
    }


    public Counties getCountyList()
            throws IOException
    {
        return get("/ref/counties", Counties.class);
    }


    public void putCounty(String id, String name)
            throws IOException
    {
        County county = new County();
        county.setId(id);
        county.setName(name);
        putCounty(county);
    }


    public void putCounty(County county)
            throws IOException
    {
        put(objectfactory.createCounty(county), "/ref/counties/" + county.getId());
    }


    public void deleteCounty(String id)
            throws IOException
    {
        delete("/ref/counties/" + id);
    }
}