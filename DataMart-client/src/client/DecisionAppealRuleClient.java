package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.decisionappealrule.*;


public class DecisionAppealRuleClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public DecisionAppealRuleClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.decisionappealrule", "DecisionAppealRule.xsd");
    }

    public void postDecisionAppealRule(int decisionid, String appealrule)
                throws IOException
    {
        Decisionappealrule dar = new Decisionappealrule();
        dar.setDecisionid(decisionid);
        dar.setAppealrule(appealrule);
        postDecisionAppealRule(dar);
    }


    public void postDecisionAppealRule(Decisionappealrule dar)
            throws IOException
    {
        post(objectfactory.createDecisionappealrule(dar), "/decisions/" + dar.getDecisionid() + "/decisionappealrules");
    }


    public Decisionappealrule getDecisionAppealRule(int decisionid, String appealrule)
            throws IOException
    {
        return get("/decisions/" + decisionid + "/decisionappealrules/" + appealrule, Decisionappealrule.class);
    }


    public Decisionappealrules getDecisionAppealRuleList(int decisionid)
            throws IOException
    {
        return get("/decisions/" + decisionid + "/decisionappealrules", Decisionappealrules.class);
    }


    public void deleteDecisionAppealRule(int decisionid, String appealrule)
            throws IOException
    {
        delete("/decisions/" + decisionid + "/decisionappealrules/" + appealrule);
    }
}