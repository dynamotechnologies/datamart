package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.UserSessionManagerRemote;
import us.fed.fs.www.nepa.schema.usersession.ObjectFactory;
import us.fed.fs.www.nepa.schema.usersession.Usersession;
import us.fed.fs.www.nepa.schema.usersession.Usersessions;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import javax.annotation.security.*;
import us.fed.fs.www.nepa.schema.usersession.*;


@Path ("/usersessions")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class UserSessionResource extends AbstractResource
{
    private UserSessionManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public UserSessionResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "UserSession.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.usersession");
        MY_PROPS.setProperty("bean", "UserSessionManagerRemote");
        try {
            beanmanager = (UserSessionManagerRemote) init(UserSessionManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("UserSessionResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("UserSessionResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        URI uri = null;
        Usersession sess;
        try {
            sess = (Usersession)unmarshal(inputxml, Usersession.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+sess.getToken());

            // All clear. Now do the insert.
            sess = beanmanager.create(sess);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Usersession> jaxbelement = objectfactory.createUsersession(sess);
            sw = marshal(jaxbelement);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }

        // Return a link to the newly created entity
        // also return the xml resource of the created item, which may not be
        // identical to the xml resource passed in since we create/default some data.
        return created(uri, sw);
    }


    @GET
    public Response get(@Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get a dump of all the entities in the datamart
            Usersessions sessionlist = beanmanager.getAll();

            // Return 404 if none exist
            if (sessionlist.getUsersession().isEmpty())
                notFound();

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Usersessions> jaxbelement = objectfactory.createUsersessions(sessionlist);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @GET
    @Path("/{code}")
    public Response get(@PathParam("code") String code, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the name of the specified entity
            Usersession sess = beanmanager.get(code);

            //this updates last activity to now.  If we are getting an individual session
            //that means the session is still active.
            beanmanager.update(sess);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Usersession> jaxbelement = objectfactory.createUsersession(sess);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @PUT
    @Path("/{code}")
    public Response put(@PathParam("code") String code, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        Usersession sess = null;
        try {
            sess = (Usersession) unmarshal(inputxml, Usersession.class);

            if (!code.equalsIgnoreCase(sess.getToken()))
                complainLoudly(new Exception("Code in uri must match code in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.update(sess);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"/"+sess.getToken()+"</location>");
    }


    @DELETE
    @Path("/{code}")
    public Response delete(@PathParam("code") String code, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(code);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}
