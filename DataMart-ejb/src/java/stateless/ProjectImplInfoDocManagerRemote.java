/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateless;

import beanutil.DataMartException;
import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.ProjectImplInfoDoc.*;

/**
 *
 * @author gauri
 */
@Remote
public interface ProjectImplInfoDocManagerRemote {
     public void create(ProjectImplInfoDoc resource) throws DataMartException;
    public ProjectImplInfoDoc get(String addInfoDocId) throws DataMartException;
      public ProjectImplInfoDocs getAllByProject(String projecttype, String projectid) throws DataMartException;
  
    public void update(ProjectImplInfoDoc resource) throws DataMartException;
    public void delete(String addInfoDocId) throws DataMartException;
    public Boolean ping(); 
}
