package entity;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "RefConfigs")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RefConfigs.findAll", query = "SELECT r FROM RefConfigs r"),
    @NamedQuery(name = "RefConfigs.findById", query = "SELECT r FROM RefConfigs r WHERE r.id = :id"),
    @NamedQuery(name = "RefConfigs.findByValue", query = "SELECT r FROM RefConfigs r WHERE r.value = :value")})
public class RefConfigs implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private String id;
    @Basic(optional = false)
    @Column(name = "Value")
    private String value;

    public RefConfigs() {
    }

    public RefConfigs(String id) {
        this.id = id;
    }

    public RefConfigs(String id, String value) {
        this.id = id;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RefConfigs)) {
            return false;
        }
        RefConfigs other = (RefConfigs) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RefConfigs[ id=" + id + " ]";
    }

}
