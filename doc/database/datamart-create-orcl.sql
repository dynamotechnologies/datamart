CREATE TABLE RefDocSensitivityRationales (
  Id int NOT NULL,
  SensitivityId int NOT NULL,
  Description varchar(255) NOT NULL,
  DisplayOrder int NOT NULL,
  CONSTRAINT PkRefDocSensRationales_Id PRIMARY KEY (Id)
);

CREATE TABLE RefAnalysisApplicables (	-- Added for CARA
  Id int NOT NULL,
  AnalysisTypeId varchar(255) NOT NULL,	-- matches Projects.AnalysisTypeId
  ApplicableRegId int NOT NULL,	-- matches Projects.CommentRegulationId
  CommentPeriodDays varchar(50),
  ObjectionPeriodDays varchar(50),
  Active int NOT NULL,
  CONSTRAINT PkRefAnalysisApplicables_Id PRIMARY KEY (Id)
);

CREATE TABLE RefRoles (			-- Added for CARA
  Id int NOT NULL,
  RoleName varchar(50) NOT NULL,
  Description varchar(255) NOT NULL,
  CONSTRAINT PkRefRoles_Id PRIMARY KEY (Id)
);

CREATE TABLE Users (			-- Added for CARA
  Id varchar(50) NOT NULL,		-- PALS shortname
  FirstName varchar(100) NOT NULL,
  LastName varchar(100) NOT NULL,
  PhoneNumber varchar(100) NOT NULL,
  Title varchar(50) NOT NULL,
  Email varchar(255) NOT NULL,
  CONSTRAINT PkUsers_Id PRIMARY KEY (Id)
);

CREATE INDEX IxUsers_Names ON
  Users (FirstName, LastName);

CREATE TABLE RefStates (
  Id varchar(8) NOT NULL,
  Name varchar(50) NOT NULL,
  CONSTRAINT PkRefStates_Id PRIMARY KEY (Id)
);

CREATE TABLE RefCounties (
  Id varchar(8) NOT NULL,
  Name varchar(50) NOT NULL,
  CONSTRAINT PkRefCounties_Id PRIMARY KEY (Id)
);

CREATE TABLE RefMilestones (
  Id varchar(8) NOT NULL,
  Name varchar(255) NOT NULL,
  CONSTRAINT PkRefMilestones_Id PRIMARY KEY (Id)
);

CREATE TABLE RefSpecialAuthorities (
  Id varchar(8) NOT NULL,
  Name varchar(255) NOT NULL,
  Description varchar(1000),
  CONSTRAINT PkRefSpecialAuthorities_Id PRIMARY KEY (Id)
);

CREATE TABLE RefActivities (
  Id varchar(8) NOT NULL,
  Name varchar(50) NOT NULL,
  CONSTRAINT PkRefActivities_Id PRIMARY KEY (Id)
);

CREATE TABLE RefUnits (
  Id varchar(255) NOT NULL,
  Name varchar(255) NOT NULL,
  ExtendedDetails int NOT NULL,
  ProjectMap int NOT NULL,
  Address1 varchar(255),
  Address2 varchar(255),
  City varchar(255),
  State varchar(255),
  Zip varchar(20),
  Phone varchar(20),
  wwwLink varchar(255),
  CommentEmail varchar(255),
  Newspaper varchar(255),
  NewspaperURL varchar(255),
  BoundaryURL varchar(255),
  Active int NOT NULL,
  CONSTRAINT PkRefUnits_Id PRIMARY KEY (Id)
);

CREATE TABLE RefStatuses (
  Id int NOT NULL,
  Name varchar(255) NOT NULL,
  CONSTRAINT PkRefStatuses_Id PRIMARY KEY (Id)
);

CREATE TABLE RefPurposes (
  Id varchar(2) NOT NULL,
  Name varchar(255) NOT NULL,
  CONSTRAINT PkRefPurposes_Id PRIMARY KEY (Id)
);

CREATE TABLE RefAnalysisTypes (
  Id varchar(255) NOT NULL,
  Name varchar(255) NOT NULL,
  CONSTRAINT PkRefAnalysisTypes_Id PRIMARY KEY (Id)
);

CREATE TABLE RefAppealRules (
  Id varchar(8) NOT NULL,
  Rule varchar(30) NOT NULL,
  Description varchar(500) NOT NULL,
  CONSTRAINT PkRefAppealRules_Id PRIMARY KEY (Id)
);

CREATE TABLE RefAppealStatuses (
  Id int NOT NULL,
  Name varchar(255) NOT NULL,
  CONSTRAINT PkRefAppealStatuses_Id PRIMARY KEY (Id)
);

CREATE TABLE RefAppealOutcomes (
  Id int NOT NULL,
  Outcome varchar(255) NOT NULL,
  CONSTRAINT PkRefAppealOutcomes_Id PRIMARY KEY (Id)
);

CREATE TABLE RefAppealDismissalReasons (
  Id int NOT NULL,
  Name varchar(255) NOT NULL,
  CONSTRAINT PkRefAppealDismissalReasons_Id PRIMARY KEY (Id)
);

CREATE TABLE RefCommentRegulations (
  Id int NOT NULL,
  Name varchar(255) NOT NULL,
  CONSTRAINT PkRefCommentRegulations_Id PRIMARY KEY (Id)
);

CREATE TABLE RefCategoricalExclusions (
  Id int NOT NULL,
  Name varchar(255) NOT NULL,
  Description varchar(1024),
  CONSTRAINT PkRefCategoricalExclusions_Id PRIMARY KEY (Id)
);

CREATE TABLE RefLitigationOutcomes (
  Id int NOT NULL,
  Name varchar(255) NOT NULL,
  CONSTRAINT PkRefLitigationOutcomes_Id PRIMARY KEY (Id)
);

CREATE TABLE RefLitigationStatuses (
  Id int NOT NULL,
  Name varchar(255) NOT NULL,
  CONSTRAINT PkRefLitigationStatuses_Id PRIMARY KEY (Id)
);

-- Unit Roles aka Access Group
CREATE TABLE UnitRoles (		-- Added for CARA
  Id int NOT NULL,			-- Assigned by PALS
  ShortName varchar(50) NOT NULL,	-- FK to Users
  UnitId varchar(255) NOT NULL,		-- FK to RefUnits
  RoleId int NOT NULL,			-- FK to RefRoles
  CONSTRAINT PkUnitRoles_Id PRIMARY KEY (Id),
  CONSTRAINT FkUnitRoles_ShortName FOREIGN KEY (ShortName) REFERENCES Users(Id),
  CONSTRAINT FkUnitRoles_UnitId FOREIGN KEY (UnitId) REFERENCES RefUnits (Id),
  CONSTRAINT FkUnitRoles_RoleId FOREIGN KEY (RoleId) REFERENCES RefRoles (Id)
);


-- admin_projtype
CREATE TABLE ProjectTypes (
  Id varchar(10) NOT NULL,
  Type varchar(255) NOT NULL,
  CONSTRAINT PkProjectTypes_Id PRIMARY KEY (Id)
);


-- admin_app
CREATE TABLE Applications (
  Id varchar(10) NOT NULL,
  Name varchar(255) NOT NULL,
  CONSTRAINT PkApplications_Id PRIMARY KEY (Id)
);

-- proj_info
CREATE TABLE Projects (
  Id int NOT NULL,
  ProjectTypeId varchar(10) NOT NULL,
  PublicId varchar(255) DEFAULT NULL, -- PALSID
  ApplicationId varchar(10) NOT NULL,
  UnitId varchar(255) NOT NULL,
  ProjectDocumentId int,	-- PALS alternate project ID
  AnalysisTypeId varchar(255) DEFAULT NULL,
  StatusId int NOT NULL,
  CommentRegulationId int DEFAULT NULL,
  Name varchar(255) NOT NULL,
  Description varchar(255) DEFAULT NULL,
  LastUpdate TIMESTAMP DEFAULT NULL,
  ContactName varchar(255) DEFAULT NULL,
  ContactPhone varchar(255) DEFAULT NULL,
  ContactEmail varchar(255) DEFAULT NULL,
  wwwIsPublished int NOT NULL,
  wwwSummary varchar(4000) DEFAULT NULL,
  wwwLink varchar(255) DEFAULT NULL,
  ExpirationDate TIMESTAMP DEFAULT NULL,
  LocationDesc varchar(255) DEFAULT NULL,
  LocationLegalDesc varchar(255) DEFAULT NULL,
  Latitude BINARY_DOUBLE DEFAULT NULL,
  Longitude BINARY_DOUBLE DEFAULT NULL,
  SopaPublish int DEFAULT 0,
  SopaNew int DEFAULT 0,
  SopaHeaderCat varchar(255),
  CONSTRAINT PkProjects_Id PRIMARY KEY (Id),
  CONSTRAINT FkProjects_ProjectTypeId FOREIGN KEY (ProjectTypeId) REFERENCES ProjectTypes (Id),
  CONSTRAINT FkProjects_ApplicationId FOREIGN KEY (ApplicationId) REFERENCES Applications (Id),
  CONSTRAINT FkProjects_UnitId FOREIGN KEY (UnitId) REFERENCES RefUnits (Id),
  CONSTRAINT FkProjects_AnalysisTypeId FOREIGN KEY (AnalysisTypeId) REFERENCES RefAnalysisTypes (Id),
  CONSTRAINT FkProjects_StatusId FOREIGN KEY (StatusId) REFERENCES RefStatuses (Id),
  CONSTRAINT FkProjects_CommentRegulationId FOREIGN KEY (CommentRegulationId) REFERENCES RefCommentRegulations (Id),
  CONSTRAINT UnProjects_PubId_ProjTypeId UNIQUE (PublicId, ProjectTypeId)
);


-- doc_node
CREATE TABLE ProjectDocumentContainers (
  Id int NOT NULL,
  ProjectId int NOT NULL,
  ContainerId int DEFAULT NULL,	-- FK to parent container
  Label varchar(255) NOT NULL,
  OrderTag int NOT NULL,
  PalsContId int DEFAULT NULL,	-- Public PALS container ID property
  CONSTRAINT PkProjectDocumentContainers_Id PRIMARY KEY (Id),
  CONSTRAINT FkProjDocCont_ProjId FOREIGN KEY (ProjectId) REFERENCES Projects (Id),
  CONSTRAINT FkProjDocCont_ContainerId FOREIGN KEY (ContainerId) REFERENCES ProjectDocumentContainers (Id) ON DELETE CASCADE
);


-- proj_docs
-- Joins table Projects with imaginary table Documents, which may be added later
CREATE TABLE ProjectDocuments (
  Id int NOT NULL,
  ProjectId int NOT NULL,
  ContainerId int DEFAULT NULL,
  DocumentId varchar(255) NOT NULL,
  Label varchar(255) NOT NULL,
  PdfSize varchar(255) NOT NULL,
  isPublished int NOT NULL,
  Description varchar(1000) DEFAULT NULL,
  Link varchar(255) DEFAULT NULL,
  OrderTag int DEFAULT NULL,
  isSensitive int DEFAULT 0 NOT NULL,
  CONSTRAINT PkProjectDocuments_Id PRIMARY KEY (Id),
  CONSTRAINT FkProjectDocuments_ProjId FOREIGN KEY (ProjectId) REFERENCES Projects (Id),
  CONSTRAINT FkProjectDocuments_ContId FOREIGN KEY (ContainerId) REFERENCES ProjectDocumentContainers (Id) ON DELETE SET NULL,
  CONSTRAINT UnProjDocs_ProjId_DocId UNIQUE (ProjectId, DocumentId)
);


-- proj_purp_list
-- joins table Projects with table RefPurposes
CREATE TABLE ProjectPurposes (
  Id int NOT NULL,
  ProjectId int NOT NULL,
  PurposeId varchar(2) NOT NULL,
  CONSTRAINT PkProjectPurposes_Id PRIMARY KEY (Id),
  CONSTRAINT FkProjectPurposes_ProjId FOREIGN KEY (ProjectId) REFERENCES Projects (Id),
  CONSTRAINT FkProjectPurposes_PurposeId FOREIGN KEY (PurposeId) REFERENCES RefPurposes (Id)
);


-- obj_info
-- FkObjections_ProjectDocumentId should probably reference table Documents. Since that doesnt exist well use ProjectDocuments instead
CREATE TABLE Objections (
  Id varchar(255) NOT NULL,
  ProjectDocumentId int DEFAULT NULL,
  ProjectId int NOT NULL,
  Objector varchar(255) NOT NULL,
  ResponseDate TIMESTAMP NOT NULL,
  CONSTRAINT PkObjections_Id PRIMARY KEY (Id),
  CONSTRAINT FkObjections_ProjectDocumentId FOREIGN KEY (ProjectDocumentId) REFERENCES ProjectDocuments (Id),
  CONSTRAINT FkObjections_ProjId FOREIGN KEY (ProjectId) REFERENCES Projects (Id)
);


CREATE TABLE Decisions (
  Id int NOT NULL,
  ProjectId int NOT NULL,      -- FK to Projects
  Name varchar(255),
  AppealStatus varchar(255),
  DecConstraint varchar(255),
  LegalNoticeDate TIMESTAMP DEFAULT NULL,
  AreaSize BINARY_DOUBLE,		-- PALS 3.3
  AreaUnits varchar(255),	-- PALS 3.3
  CONSTRAINT PkDecisions_Id PRIMARY KEY (Id),
  CONSTRAINT FkDecisions_ProjId FOREIGN KEY (ProjectId) REFERENCES Projects(Id)
);


CREATE TABLE Litigations (
  Id int NOT NULL,
  DecisionId int NOT NULL,
  CaseName varchar(255),
  Status int,
  Outcome int,
  ClosedDate TIMESTAMP default NULL,
  CONSTRAINT PKLitigations_Id PRIMARY KEY (Id),
  CONSTRAINT FKLitigations_DecisionId FOREIGN KEY (DecisionId) REFERENCES Decisions(Id),
  CONSTRAINT FKLitigations_Status FOREIGN KEY (Status) REFERENCES RefLitigationStatuses(Id),
  CONSTRAINT FKLitigations_Outcome FOREIGN KEY (Outcome) REFERENCES RefLitigationOutcomes(Id)
);


CREATE TABLE DecisionAppealRules (
  Id int NOT NULL,
  DecisionId int,
  AppealRule varchar(8),
  CONSTRAINT PKDecAppRules_Id PRIMARY KEY (Id),
  CONSTRAINT FKDecAppealRules_DecId FOREIGN KEY (DecisionId) REFERENCES Decisions(Id),
  CONSTRAINT FKDecAppRules_AppRule FOREIGN KEY (AppealRule) REFERENCES RefAppealRules(Id),
  CONSTRAINT UnDecAppRules_DecId_AppRule UNIQUE (DecisionId, AppealRule)
);


CREATE TABLE NepaCategoricalExclusions (
  Id int NOT NULL,
  ProjectId int NOT NULL,
  CategoricalExclusionId int NOT NULL,
  CONSTRAINT PKNepaCatExcl_Id PRIMARY KEY (Id),
  CONSTRAINT FkNepaCatExcl_ProjId FOREIGN KEY (ProjectId) REFERENCES Projects(Id),
  CONSTRAINT FKNepaCatExcl_CateExclId FOREIGN KEY (CategoricalExclusionId) REFERENCES RefCategoricalExclusions(Id),
  CONSTRAINT UnNepaCatExcl_ProjId_CatExclId UNIQUE (ProjectId, CategoricalExclusionId)
);


-- app_info
-- FkAppeals_ProjectDocumentId should probably reference table Documents. Since that doesnt exist well use ProjectDocuments instead
CREATE TABLE Appeals (
  Id varchar(255) NOT NULL,
  AppealStatusId int NOT NULL,
  OutcomeId int NOT NULL,
  ProjectDocumentId int DEFAULT NULL,
  ProjectId int NOT NULL,
  RuleId varchar(8) DEFAULT NULL,
  DismissalReasonId int DEFAULT NULL,
  Appellant varchar(255) NOT NULL,
  ResponseDate TIMESTAMP NOT NULL,
  AdminUnit varchar(255),
  DecisionId int,
  CONSTRAINT PkAppeals_Id PRIMARY KEY (Id),
  CONSTRAINT FkAppeals_AppealStatusId FOREIGN KEY (AppealStatusId) REFERENCES RefAppealStatuses (Id),
  CONSTRAINT FkAppeals_OutcomeId FOREIGN KEY (OutcomeId) REFERENCES RefAppealOutcomes (Id),
  CONSTRAINT FkAppeals_ProjectDocumentId FOREIGN KEY (ProjectDocumentId) REFERENCES ProjectDocuments (Id),
  CONSTRAINT FkAppeals_ProjId FOREIGN KEY (ProjectId) REFERENCES Projects (Id),
  CONSTRAINT FkAppeals_RuleId FOREIGN KEY (RuleId) REFERENCES RefAppealRules (Id),
  CONSTRAINT FkAppeals_DismissalReasonId FOREIGN KEY (DismissalReasonId) REFERENCES RefAppealDismissalReasons (Id),
  CONSTRAINT FkAppeals_AdminUnit FOREIGN KEY (AdminUnit) REFERENCES RefUnits(Id),
  CONSTRAINT FkAppeals_DecisionId FOREIGN KEY (DecisionId) REFERENCES Decisions(Id)
);

-- proj_special_authorities
-- joins table Projects with table RefSpecialAuthorities
CREATE TABLE ProjectSpecialAuthorities (
  Id int NOT NULL,
  ProjectId int NOT NULL,
  SpecialAuthorityId varchar(8) NOT NULL,
  CONSTRAINT PkProjSpecAth_Id PRIMARY KEY (Id),
  CONSTRAINT FkProjSpecAth_ProjId FOREIGN KEY (ProjectId) REFERENCES Projects (Id),
  CONSTRAINT FkProjSpecAth_SpecAthId FOREIGN KEY (SpecialAuthorityId) REFERENCES RefSpecialAuthorities(Id),
  CONSTRAINT UnProjSpecAth_PrId_SpcAthId UNIQUE (ProjectId, SpecialAuthorityId)
);

-- proj_activities
-- joins table Projects with table RefActivities
CREATE TABLE ProjectActivities (
  Id int NOT NULL,
  ProjectId int NOT NULL,
  ActivityId varchar(8) NOT NULL,
  CONSTRAINT PkProjAct_Id PRIMARY KEY (Id),
  CONSTRAINT FkProjAct_ProjId FOREIGN KEY (ProjectId) REFERENCES Projects (Id),
  CONSTRAINT FkProjAct_ActId FOREIGN KEY (ActivityId) REFERENCES RefActivities(Id),
  CONSTRAINT UnProjAct_ProjId_ActId UNIQUE (ProjectId, ActivityId)
);

-- proj_regions
-- joins table Projects with table RefUnits
CREATE TABLE ProjectRegions (
  Id int NOT NULL,
  ProjectId int NOT NULL,
  RegionId varchar(255) NOT NULL,
  CONSTRAINT PkProjRegions_Id PRIMARY KEY (Id),
  CONSTRAINT FkProjRegions_ProjId FOREIGN KEY (ProjectId) REFERENCES Projects (Id),
  CONSTRAINT FkProjRegions_RegionId FOREIGN KEY (RegionId) REFERENCES RefUnits(Id),
  CONSTRAINT UnProjRegions_ProjId_RegionId UNIQUE (ProjectId, RegionId)
);

-- proj_forests
-- joins table Projects with table RefUnits
CREATE TABLE ProjectForests (
  Id int NOT NULL,
  ProjectId int NOT NULL,
  ForestId varchar(255) NOT NULL,
  CONSTRAINT PkProjFor_Id PRIMARY KEY (Id),
  CONSTRAINT FkProjFor_ProjId FOREIGN KEY (ProjectId) REFERENCES Projects (Id),
  CONSTRAINT FkProjFor_ForId FOREIGN KEY (ForestId) REFERENCES RefUnits(Id),
  CONSTRAINT UnProjFor_ProjId_ForId UNIQUE (ProjectId, ForestId)
);

-- proj_districts
-- joins table Projects with table RefUnits
CREATE TABLE ProjectDistricts (
  Id int NOT NULL,
  ProjectId int NOT NULL,
  DistrictId varchar(255) NOT NULL,
  CONSTRAINT PkProjectDist_Id PRIMARY KEY (Id),
  CONSTRAINT FkProjectDist_ProjId FOREIGN KEY (ProjectId) REFERENCES Projects (Id),
  CONSTRAINT FkProjectDist_DistId FOREIGN KEY (DistrictId) REFERENCES RefUnits(Id),
  CONSTRAINT UnProjectDist_ProjId_DistId UNIQUE (ProjectId, DistrictId)
);

-- proj_states
-- joins table Projects with table RefStates
CREATE TABLE ProjectStates (
  Id int NOT NULL,
  ProjectId int NOT NULL,
  StateId varchar(8) NOT NULL,
  CONSTRAINT PkProjectStates_Id PRIMARY KEY (Id),
  CONSTRAINT FkProjectStates_ProjId FOREIGN KEY (ProjectId) REFERENCES Projects (Id),
  CONSTRAINT FkProjectStates_StateId FOREIGN KEY (StateId) REFERENCES RefStates(Id),
  CONSTRAINT UnProjectStates_ProjId_StateId UNIQUE (ProjectId, StateId)
);

-- proj_counties
-- joins table Projects with table RefCounties
CREATE TABLE ProjectCounties (
  Id int NOT NULL,
  ProjectId int NOT NULL,
  CountyId varchar(8) NOT NULL,
  CONSTRAINT PkProjCount_Id PRIMARY KEY (Id),
  CONSTRAINT FkProjCount_ProjId FOREIGN KEY (ProjectId) REFERENCES Projects (Id),
  CONSTRAINT FkProjCount_CountyId FOREIGN KEY (CountyId) REFERENCES RefCounties(Id),
  CONSTRAINT UnProjCount_ProjId_CountyId UNIQUE (ProjectId, CountyId)
);

-- proj_milestones
-- joins table Projects with table RefMilestones
CREATE TABLE ProjectMilestones (
  Id int NOT NULL,			-- Milestone sequence number
  ProjectId int NOT NULL,
  MilestoneId varchar(8) NOT NULL,	-- Milestone type
  MilestoneDate varchar(50),		-- Freeform text, either M/Y or M/D/Y
  MilestoneStatus char(1) NOT NULL,	-- Estimated/Not estimated flag (0/1)
  History char(1) NOT NULL,		-- History flag (0/1)
  CONSTRAINT PkProjMs_Id PRIMARY KEY (Id),
  CONSTRAINT FkProjMs_ProjId FOREIGN KEY (ProjectId) REFERENCES Projects (Id),
  CONSTRAINT FkProjMs_MsId FOREIGN KEY (MilestoneId) REFERENCES RefMilestones(Id)
);

-- mailing list subscribers
CREATE TABLE MailingListSubscribers (	-- Added for CARA
  Id int NOT NULL,
  ProjectId int NOT NULL,		-- FK to Projects
  SubscriberId int NOT NULL,	-- CARA Subscriber ID
  Email varchar(250),
  LastName varchar(100),
  FirstName varchar(100),
  Title varchar(50),
  Org varchar(100),
  OrgType varchar(100),
  SubscriberType varchar(25),
  AddressStreet1 varchar(200),
  AddressStreet2 varchar(200),
  City varchar(100),
  State varchar(2),
  Zip varchar(10),
  Province varchar(100),
  Country varchar(100),
  Phone varchar(20),
  ContactMethod varchar(5),
  SubscribedDate TIMESTAMP,
  CONSTRAINT PkMailListSubs_Id PRIMARY KEY (Id),
  CONSTRAINT FkMailListSubs_ProjId FOREIGN KEY (ProjectId) REFERENCES Projects(Id),
  CONSTRAINT UnMailListSubs_ProjId_SubsId UNIQUE (ProjectId, SubscriberId)
);

-- CARA projects
CREATE TABLE CARAProjects (		-- Added for CARA
  Id int NOT NULL,			-- CARA ID
  ProjectId int NOT NULL,		-- FK to Projects
  ReadingRoomActive int DEFAULT 0 NOT NULL,
  CONSTRAINT PkCARAProjects_Id PRIMARY KEY (Id),
  CONSTRAINT FkCARAProjects_ProjId FOREIGN KEY (ProjectId) REFERENCES Projects(Id),
  CONSTRAINT UnCARAProjects_ProjId UNIQUE (ProjectId)
);

-- comment phase metadata
CREATE TABLE CommentPhases (		-- Added for CARA
  Id int NOT NULL,
  CaraId int NOT NULL,		-- FK to CARAProjects
  PhaseId int NOT NULL,		-- Assigned by CARA
  Name varchar(40),
  StartDate TIMESTAMP DEFAULT NULL,
  FinishDate TIMESTAMP DEFAULT NULL,
  LetterCount int DEFAULT 0,
  LetterUniqueCount int DEFAULT 0,
  LetterMastersCount int DEFAULT 0,
  LetterFormCount int DEFAULT 0,
  LetterFormPlusCount int DEFAULT 0,
  LetterFormDupeCount int DEFAULT 0,
  CONSTRAINT PkCommentPhases_Id PRIMARY KEY (Id),
  CONSTRAINT FkCommentPhases_CaraId FOREIGN KEY (CaraId) REFERENCES CARAProjects(Id),
  CONSTRAINT UnCommentPhases_CaraId_PhaseId UNIQUE (CaraId, PhaseId)
);

-- CARA phase documents
CREATE TABLE CARAPhaseDocs (		-- Added for CARA
  Id int NOT NULL,
  CommentPhaseId int NOT NULL,	-- FK to CommentPhases
  ProjectDocumentId int NOT NULL,	-- FK to ProjectDocuments
  CONSTRAINT PkCARAPhaseDocs_Id PRIMARY KEY (Id),
  CONSTRAINT FkCARAPhaseDocs_CommentPhaseId FOREIGN KEY (CommentPhaseId) REFERENCES CommentPhases(Id),
  CONSTRAINT FkCARAPhaseDocs_ProjDocId FOREIGN KEY (ProjectDocumentId) REFERENCES ProjectDocuments(Id)
);

-- MLM projects
CREATE TABLE MLMProjects (		-- Added for CARA
  Id varchar(50) NOT NULL,		-- GovDelivery MLM ID
  ProjectId int NOT NULL,		-- FK to Projects
  CONSTRAINT PkMLMProjects_Id PRIMARY KEY (Id),
  CONSTRAINT FkMLMProjects_ProjId FOREIGN KEY (ProjectId) REFERENCES Projects(Id),
  CONSTRAINT UnMLMProjects_ProjId UNIQUE (ProjectId)
);

CREATE TABLE RefConfigs (
  Id varchar(50) NOT NULL,
  Value varchar(255) NOT NULL,
  CONSTRAINT PkRefConfigs_Id PRIMARY KEY (Id)
);

CREATE TABLE ProjectKMLs (
  Id varchar(255) NOT NULL,	-- Filename
  ProjectId int NOT NULL,
  Label varchar(255),
  MapType varchar(50),
  CONSTRAINT PkProjectKMLs_Id PRIMARY KEY (Id),
  CONSTRAINT FkProjectKMLs_ProjId FOREIGN KEY (ProjectId) REFERENCES Projects (Id)
);

CREATE TABLE UnitKMLs (
  Id varchar(255) NOT NULL,	-- Filename
  UnitId varchar(255) NOT NULL,
  Label varchar(255),
  MapType varchar(50),
  CONSTRAINT PkUnitKMLs_Id PRIMARY KEY (Id),
  CONSTRAINT FkUnitKMLs_UnitId FOREIGN KEY (UnitId) REFERENCES RefUnits (Id)
);

CREATE TABLE Goals ( -- Added for PALS 3.3
  Id int NOT NULL,	-- goalid
  Description varchar(1024),
  CONSTRAINT PkGoals_Id PRIMARY KEY (Id)
);

CREATE TABLE ResourceAreas ( -- Added for PALS 3.3
  Id int NOT NULL,	-- resourceareaid
  UnitId varchar(255) NOT NULL,	-- forest unit
  Description varchar(1024),
  CONSTRAINT PkResourceAreas_Id PRIMARY KEY (Id),
  CONSTRAINT FkResourceAreas_UnitId FOREIGN KEY (UnitId) REFERENCES RefUnits (Id)
);

CREATE TABLE ProjectGoals ( -- Added for PALS 3.3
  Id int NOT NULL,
  ProjectId int NOT NULL,
  GoalId int NOT NULL,
  OrderTag int,	-- display order
  CONSTRAINT PkProjectGoals_Id PRIMARY KEY (Id),
  CONSTRAINT FkProjectGoals_ProjId FOREIGN KEY (ProjectId) REFERENCES Projects (Id),
  CONSTRAINT FkProjectGoals_GoalId FOREIGN KEY (GoalId) REFERENCES Goals (Id),
  CONSTRAINT UnProjectGoals_ProjId_GoalId UNIQUE (ProjectId, GoalId)
);

CREATE TABLE UnitGoals ( -- Added for PALS 3.3
  Id int NOT NULL,
  UnitId varchar(255) NOT NULL,	-- forest unit
  GoalId int NOT NULL,
  OrderTag int,	-- display order
  CONSTRAINT PkUnitGoals_Id PRIMARY KEY (Id),
  CONSTRAINT FkUnitGoals_UnitId FOREIGN KEY (UnitId) REFERENCES RefUnits (Id),
  CONSTRAINT FkUnitGoals_GoalId FOREIGN KEY (GoalId) REFERENCES Goals (Id),
  CONSTRAINT UnUnitGoals_UnitId_GoalId UNIQUE (UnitId, GoalId)
);

CREATE TABLE ProjectResourceAreas ( -- Added for PALS 3.3
  Id int NOT NULL,
  ProjectId int NOT NULL,
  ResourceAreaId int NOT NULL,
  OrderTag int,	-- display order
  CONSTRAINT PkPrjResAreas_Id PRIMARY KEY (Id),
  CONSTRAINT FkPrjResAreas_ProjId FOREIGN KEY (ProjectId) REFERENCES Projects (Id),
  CONSTRAINT FkPrjResAreas_ResAreaId FOREIGN KEY (ResourceAreaId) REFERENCES ResourceAreas (Id),
  CONSTRAINT UnPrjRsArs_PrjId_RsArId UNIQUE (ProjectId, ResourceAreaId)
);


CREATE SEQUENCE SEQ_GEN_IDENTITY;

SELECT OBJECT_NAME from USER_OBJECTS WHERE OBJECT_TYPE = 'TABLE';
