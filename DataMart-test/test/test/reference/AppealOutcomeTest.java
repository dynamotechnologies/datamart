package test.reference;

import client.AppealOutcomeClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.appealoutcome.*;


public class AppealOutcomeTest {
    private AppealOutcomeClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new AppealOutcomeClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postAppealOutcome(TestData.appealoutcomes.id1, TestData.appealoutcomes.name2);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Appealoutcome appealoutcome = null;
        Appealoutcomes appealoutcomelist = null;
        try {
            appealoutcome = client.getAppealOutcome(TestData.appealoutcomes.id1);
            appealoutcomelist = client.getAppealOutcomeList();
        } catch (IOException ex) {
            Logger.getLogger(AppealOutcomeTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.appealoutcomes.id1, appealoutcome.getId());
        Assert.assertEquals(TestData.appealoutcomes.name1, appealoutcome.getName());
        Assert.assertTrue(appealoutcomelist.getAppealoutcome().get(0).getId().length()>0);
        Assert.assertTrue(appealoutcomelist.getAppealoutcome().get(0).getName().length()>0);

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        Appealoutcome appealoutcome = null;
        try {
            client.putAppealOutcome(TestData.appealoutcomes.id1, TestData.appealoutcomes.name2);
            appealoutcome = client.getAppealOutcome(TestData.appealoutcomes.id1);
        } catch (Exception ex) {
            Logger.getLogger(AppealOutcomeTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.appealoutcomes.name2, appealoutcome.getName());

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getAppealOutcome(TestData.appealoutcomes.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteAppealOutcome(TestData.appealoutcomes.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }

    private void post()
    {
        try {
            client.postAppealOutcome(TestData.appealoutcomes.id1, TestData.appealoutcomes.name1);
        } catch (IOException ex) {
            Logger.getLogger(AppealOutcomeTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteAppealOutcome(TestData.appealoutcomes.id1);
        } catch (IOException ex) {
            Logger.getLogger(AppealOutcomeTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteAppealOutcome(TestData.appealoutcomes.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }
}