package entity;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "ProjectCounties")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProjectCounties.findAll", query = "SELECT p FROM ProjectCounties p"),
    @NamedQuery(name = "ProjectCounties.findById", query = "SELECT p FROM ProjectCounties p WHERE p.id = :id")})
public class ProjectCounties implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @JoinColumn(name = "ProjectId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Projects projects;
    @JoinColumn(name = "CountyId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private RefCounties refCounties;

    public ProjectCounties() {
    }

    public ProjectCounties(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Projects getProjects() {
        return projects;
    }

    public void setProjects(Projects projects) {
        this.projects = projects;
    }

    public RefCounties getRefCounties() {
        return refCounties;
    }

    public void setRefCounties(RefCounties refCounties) {
        this.refCounties = refCounties;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProjectCounties)) {
            return false;
        }
        ProjectCounties other = (ProjectCounties) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ProjectCounties[ id=" + id + " ]";
    }

}
