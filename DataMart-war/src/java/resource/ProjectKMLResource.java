package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.ProjectKMLManagerRemote;
import us.fed.fs.www.nepa.schema.projectkml.ObjectFactory;
import us.fed.fs.www.nepa.schema.projectkml.Projectkml;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;


@Path ("/projectkmls")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class ProjectKMLResource extends AbstractResource
{
    private ProjectKMLManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public ProjectKMLResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "ProjectKML.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.projectkml");
        MY_PROPS.setProperty("bean", "ProjectKMLManagerRemote");
        try {
            beanmanager = (ProjectKMLManagerRemote) init(ProjectKMLManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("ProjectKMLResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("ProjectKMLResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;
        try {
            Projectkml Projectkml = (Projectkml)unmarshal(inputxml, Projectkml.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+Projectkml.getFilename());

            // All clear. Now do the insert.
            beanmanager.create(Projectkml);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the newly created entity
        return created(uri);
    }


    @GET
    @Path("/{filename}")
    /*
     * Get single unit kml resource
     */
    public Response get(@PathParam("filename") String filename, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            Projectkml Projectkml = beanmanager.get(filename);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Projectkml> jaxbelement = objectfactory.createProjectkml(Projectkml);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @PUT
    @Path("/{filename}")
    public Response put(@PathParam("filename") String filename, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        Projectkml Projectkml = null;
        try {
            Projectkml = (Projectkml) unmarshal(inputxml, Projectkml.class);

            if (!filename.equalsIgnoreCase(Projectkml.getFilename()))
                complainLoudly(new Exception("Filename in uri must match filename in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.update(Projectkml);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"/"+Projectkml.getFilename()+"</location>");
    }


    @DELETE
    @Path("/{filename}")
    public Response delete(@PathParam("filename") String filename, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(filename);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}

// TODO: restrict access to the admin application