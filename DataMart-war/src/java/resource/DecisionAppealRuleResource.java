package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.DecisionAppealRuleManagerRemote;
import us.fed.fs.www.nepa.schema.decisionappealrule.Decisionappealrule;
import us.fed.fs.www.nepa.schema.decisionappealrule.Decisionappealrules;
import us.fed.fs.www.nepa.schema.decisionappealrule.ObjectFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;


@Path ("/decisions/{decisionid}/decisionappealrules")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class DecisionAppealRuleResource extends AbstractResource
{
    private DecisionAppealRuleManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public DecisionAppealRuleResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "DecisionAppealRule.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.decisionappealrule");
        MY_PROPS.setProperty("bean", "DecisionAppealRuleManagerRemote");
        try {
            beanmanager = (DecisionAppealRuleManagerRemote) init(DecisionAppealRuleManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("DecisionAppealRuleResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("DecisionAppealRuleResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(@PathParam("decisionid") int decisionid, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;
        Decisionappealrule dar;
        try {
            dar = (Decisionappealrule)unmarshal(inputxml, Decisionappealrule.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+dar.getAppealrule());

            // All clear. Now do the insert.
            beanmanager.create(dar);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the newly created entity
        return created(uri);
    }


    @GET
    @Path("/{id}")
    public Response get(@PathParam("decisionid") int decisionid, @PathParam("id") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the name of the specified entity
            Decisionappealrule dar = beanmanager.get(decisionid, id);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Decisionappealrule> jaxbelement = objectfactory.createDecisionappealrule(dar);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @GET
    public Response getList(@PathParam("decisionid") int decisionid, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the name of the specified entity
            Decisionappealrules darlist = beanmanager.getList(decisionid);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Decisionappealrules> jaxbelement = objectfactory.createDecisionappealrules(darlist);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("decisionid") int decisionid, @PathParam("id") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(decisionid, id);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}