package stateless;

import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.containertemplate.*;
import beanutil.DataMartException;
import java.util.List;

/*
 *
 * The Manager beans are responsible for:
 *   1) Implementing business logic and
 *   2) Translating front-end resources to back-end database entities.
 *
 * Often the resources will consist of data from numerous database tables. It is the manager's responsibility
 *   to ensure that these front-end resources can be accessed as though they were cut from a single piece of wood.
 *
 */
@Remote
public interface ContainerTemplateManagerRemote {
    public int create(Containertemplate resource) throws DataMartException;
    public Containertemplate get(Integer templateid) throws DataMartException;

    //various ways to get the template headers based on what's provided.
    public Containertemplateheaders getHeaders() throws DataMartException;
    public Containertemplateheaders getHeaders(Integer start, Integer count) throws DataMartException;
    public Containertemplateheaders getHeaders(String unitid) throws DataMartException;
    public Containertemplateheaders getHeaders(String unitid, Integer start, Integer count) throws DataMartException;
    public Containertemplateheaders getHeaders(List<String> unitids) throws DataMartException;
    public Containertemplateheaders getHeaders(List<String> unitids, Integer start, Integer count) throws DataMartException;
    
//    public void update(Containertemplate resource) throws DataMartException;
    public void delete(Integer templateid) throws DataMartException;
    public Boolean ping();
}