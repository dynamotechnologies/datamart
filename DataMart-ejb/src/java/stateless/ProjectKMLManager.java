package stateless;

import javax.ejb.Stateless;
import beanutil.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import us.fed.fs.www.nepa.schema.projectkml.*;

@Stateless(mappedName="ProjectKMLManagerRemote")
public class ProjectKMLManager implements ProjectKMLManagerRemote {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void create(Projectkml resource)
             throws DataMartException
    {
        validate(resource);
        entity.ProjectKMLs entity = createEntity(resource);

        entity.ProjectKMLs badentity = createEntity(entity.getId());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        entity = em.merge(entity);
    }

    @Override
    public Projectkml get(String key)
             throws DataMartException
    {
        entity.ProjectKMLs entity = createEntity(key);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return createResource(entity);
    }

    @Override
    public void update(Projectkml resource)
             throws DataMartException
    {
        validate(resource);
        entity.ProjectKMLs entity = createEntity(resource);

        if (createEntity(entity.getId())==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        entity = em.merge(entity);
    }

    @Override
    public void delete(String key)
             throws DataMartException
    {
        entity.ProjectKMLs entity = createEntity(key);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     *
     * This method does the mapping from jaxb object to entity object and vice versa
     */
    private Projectkml createResource(entity.ProjectKMLs entity)
            throws DataMartException
    {
        Projectkml resource = new Projectkml();
        resource.setFilename(entity.getId());
        resource.setProjecttype(entity.getProjects().getProjectTypes().getId());
        resource.setProjectid(entity.getProjects().getPublicId());
        resource.setLabel(entity.getLabel());
        resource.setMaptype(entity.getMapType());
        return resource;
    }

    private entity.ProjectKMLs createEntity(Projectkml resource)
            throws DataMartException
    {
        entity.ProjectKMLs entity = new entity.ProjectKMLs();
        entity.setId(resource.getFilename());
        entity.Projects project = DataMartBeanUtil.getProject(em, resource.getProjecttype(), resource.getProjectid());
        entity.setProjects(project);
        entity.setLabel(resource.getLabel());
        entity.setMapType(resource.getMaptype());
        return entity;
    }

    private entity.ProjectKMLs createEntity(String key)
            throws DataMartException
    {
        return em.find(entity.ProjectKMLs.class, key);
    }

    private void validate(Projectkml resource)
            throws DataMartException
    {
        // Ensure these are not empty strings
        DataMartBeanUtil.confirmNotNull(resource.getFilename(), "filename");
        DataMartBeanUtil.confirmNotNull(resource.getProjecttype(), "projecttype");
        DataMartBeanUtil.confirmNotNull(resource.getProjectid(), "projectid");
    }

}