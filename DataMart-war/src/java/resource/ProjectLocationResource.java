package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.ProjectLocationManagerRemote;
import us.fed.fs.www.nepa.schema.projectlocation.*;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;


@Path ("/projects/nepa/{projectid}/locations")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class ProjectLocationResource extends AbstractResource
{
    private ProjectLocationManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public ProjectLocationResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "ProjectLocation.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.projectlocation");
        MY_PROPS.setProperty("bean", "ProjectLocationManagerRemote");
        try {
            beanmanager = (ProjectLocationManagerRemote) init(ProjectLocationManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("ProjectLocationResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("ProjectLocationResource failed to start. Could not ping the bean manager!");
    }


    @PUT
    public Response put(@PathParam("projectid") String projectid, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        Locations locations = null;
        try {
            locations = (Locations) unmarshal(inputxml, Locations.class);

            // avoid null pointer exception for optional lists
            if (locations.getRegions()==null)
                locations.setRegions(new Regions());
            if (locations.getForests()==null)
                locations.setForests(new Forests());
            if (locations.getDistricts()==null)
                locations.setDistricts(new Districts());
            if (locations.getStates()==null)
                locations.setStates(new States());
            if (locations.getCounties()==null)
                locations.setCounties(new Counties());

            if (!projectid.equalsIgnoreCase(locations.getProjectid()))
                complainLoudly(new Exception("Project Id in uri must match Project Id in xml."),
                        Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.merge(locations);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"</location>");
    }


    @GET
    public Response get(@PathParam("projectid") String projectid, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get a dump of all the entities in the datamart
            Locations locations = beanmanager.get(projectid);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Locations> jaxbelement = objectfactory.createLocations(locations);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }
}