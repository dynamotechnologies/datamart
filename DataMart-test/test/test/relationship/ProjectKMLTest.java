package test.relationship;


import client.ProjectKMLClient;
import client.ProjectClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.projectkml.*;
import us.fed.fs.www.nepa.schema.project.*;

public class ProjectKMLTest {
    private ProjectKMLClient client;
    private ProjectClient projectclient;

    @Before
    public void setUp()
            throws IOException
    {
        client = new ProjectKMLClient();
        projectclient = new ProjectClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postProjectKML(TestData.projectkmls.filename1, TestData.projectkmls.label1, TestData.projectkmls.maptype1, TestData.projects.id1, TestData.projects.type1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Projectkml projectkml = null;
        try {
            projectkml = client.getProjectKML(TestData.projectkmls.filename1);

        } catch (IOException ex) {
            Logger.getLogger(ProjectKMLTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.projectkmls.filename1,    projectkml.getFilename());
        Assert.assertEquals(TestData.projectkmls.label1,       projectkml.getLabel());
        Assert.assertEquals(TestData.projectkmls.maptype1,     projectkml.getMaptype());
        Assert.assertEquals(TestData.projects.id1,             projectkml.getProjectid());
        Assert.assertEquals(TestData.projects.type1,           projectkml.getProjecttype());

        // List-context check of links in the Project entity
        try {
            Project p = projectclient.getProject(TestData.projects.type1, TestData.projects.id1);
            if (p.getLinks() == null) {
                Assert.fail("Project with linked KML has no <link> tags!");
            }
            boolean foundKMLlink = false;
            for (Link link : p.getLinks().getLink()) {
                if (link.getRel().matches("projectkml")) {
//                    System.out.print("Link: " + link.getHref() + "\n");
                    if (link.getHref().matches(".*/" + TestData.projectkmls.filename1 + "$")) {
                        Assert.assertEquals(TestData.projectkmls.label1, link.getTitle());
                        Assert.assertEquals(TestData.projectkmls.maptype1, link.getType());
                        foundKMLlink = true;
                    }
                }
            }
            if (foundKMLlink == false) {
                Assert.fail("Couldn't find matching KML link in Project record!");
            }
        } catch (IOException ex) {
            Logger.getLogger(UnitKMLTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        Projectkml projectkml = null;
        try {
            client.putProjectKML(TestData.projectkmls.filename1, TestData.projectkmls.label2, TestData.projectkmls.maptype2, TestData.projects.id1, TestData.projects.type1);
            projectkml = client.getProjectKML(TestData.projectkmls.filename1);
        } catch (Exception ex) {
            Logger.getLogger(ProjectKMLTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.projectkmls.filename1,    projectkml.getFilename());
        Assert.assertEquals(TestData.projectkmls.label2,       projectkml.getLabel());
        Assert.assertEquals(TestData.projectkmls.maptype2,     projectkml.getMaptype());
        Assert.assertEquals(TestData.projects.id1,             projectkml.getProjectid());
        Assert.assertEquals(TestData.projects.type1,           projectkml.getProjecttype());

        // TODO: Add a list-context check of links in the Unit entity

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();
        // Delete the entity

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getProjectKML(TestData.projectkmls.filename1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteProjectKML(TestData.projectkmls.filename1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }


    private void post()
    {
        try {
            client.postProjectKML(TestData.projectkmls.filename1, TestData.projectkmls.label1, TestData.projectkmls.maptype1, TestData.projects.id1, TestData.projects.type1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectKMLTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteProjectKML(TestData.projectkmls.filename1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectKMLTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteProjectKML(TestData.projectkmls.filename1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }


    @BeforeClass
    public static void populateDependencies()
    {
        ProjectClient pc = null;
        try {
            pc = new ProjectClient();
        } catch (Exception ex) {
            Assert.fail();
        }

        // Create the Project
        // Add the test Project
        Project project = new Project();
        project.setType(TestData.projects.type1);
        project.setId(TestData.projects.id1);
        project.setName(TestData.projects.name1);
        project.setUnitcode(TestData.projects.unitcode1);
        // Adminapp is set by Project resource class
        project.setDescription(TestData.projects.description1);
        project.setCommentreg(TestData.projects.commentreg1);
        project.setWwwsummary(TestData.projects.wwwsummary1);
        project.setWwwpub(TestData.projects.wwwpub1);

        Nepainfo nepainfo = new Nepainfo();
        nepainfo.setProjectdocumentid(TestData.projects.projectdocumentid1);
        nepainfo.setAnalysistypeid(TestData.projects.analysistypeid1);
        nepainfo.setStatusid(TestData.projects.statusid1);

        Purposeids purposeids = new Purposeids();
        purposeids.getPurposeid().add(TestData.projects.purposeid1);

        nepainfo.setPurposeids(purposeids);
        project.setNepainfo(nepainfo);

        try {
            pc.postProject(project);
        } catch (IOException ex) {
            Logger.getLogger(ProjectKMLTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    @AfterClass
    public static void cleanupDependencies()
    {
        ProjectClient pc = null;
        try {
            pc = new ProjectClient();
        } catch (Exception ex) {
            Assert.fail();
        }

        // Remove the test Project
        try {
            pc.deleteProject("nepa", TestData.projects.id1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectKMLTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }
}