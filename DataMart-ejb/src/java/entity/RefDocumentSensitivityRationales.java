package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "RefDocumentSensitivityRationales")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RefDocumentSensitivityRationales.findAll", query = "SELECT r FROM RefDocumentSensitivityRationales r"),
    @NamedQuery(name = "RefDocumentSensitivityRationales.findById", query = "SELECT r FROM RefDocumentSensitivityRationales r WHERE r.id = :id"),
    @NamedQuery(name = "RefDocumentSensitivityRationales.findBySensitivityId", query = "SELECT r FROM RefDocumentSensitivityRationales r WHERE r.sensitivityId = :sensitivityId"),
    @NamedQuery(name = "RefDocumentSensitivityRationales.findByDescription", query = "SELECT r FROM RefDocumentSensitivityRationales r WHERE r.description = :description"),
    @NamedQuery(name = "RefDocumentSensitivityRationales.findByDisplayOrder", query = "SELECT r FROM RefDocumentSensitivityRationales r WHERE r.displayOrder = :displayOrder")})
public class RefDocumentSensitivityRationales implements Serializable {
    @OneToMany(mappedBy = "sensitivityRationale")
    private Collection<ProjectDocuments> projectDocumentsCollection;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "SensitivityId")
    private int sensitivityId;
    @Basic(optional = false)
    @Column(name = "Description")
    private String description;
    @Basic(optional = false)
    @Column(name = "DisplayOrder")
    private int displayOrder;

    public RefDocumentSensitivityRationales() {
    }

    public RefDocumentSensitivityRationales(Integer id) {
        this.id = id;
    }

    public RefDocumentSensitivityRationales(Integer id, int sensitivityId, String description, int displayOrder) {
        this.id = id;
        this.sensitivityId = sensitivityId;
        this.description = description;
        this.displayOrder = displayOrder;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getSensitivityId() {
        return sensitivityId;
    }

    public void setSensitivityId(int sensitivityId) {
        this.sensitivityId = sensitivityId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RefDocumentSensitivityRationales)) {
            return false;
        }
        RefDocumentSensitivityRationales other = (RefDocumentSensitivityRationales) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RefDocumentSensitivityRationales[ id=" + id + " ]";
    }

    @XmlTransient
    public Collection<ProjectDocuments> getProjectDocumentsCollection() {
        return projectDocumentsCollection;
    }

    public void setProjectDocumentsCollection(Collection<ProjectDocuments> projectDocumentsCollection) {
        this.projectDocumentsCollection = projectDocumentsCollection;
    }

}
