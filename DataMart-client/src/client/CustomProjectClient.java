package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.customproject.ObjectFactory;

public class CustomProjectClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public CustomProjectClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.customproject", "CustomProject.xsd");
    }

        
    public void deleteCustomProject(String id)
            throws IOException
    {
        delete("/ref/units/" + id);
    }
    
}
