package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.ProjectImplInfoManagerRemote;
import us.fed.fs.www.nepa.schema.ProjectImplInfo.ObjectFactory;
import us.fed.fs.www.nepa.schema.ProjectImplInfo.ProjectImplInfo;
import us.fed.fs.www.nepa.schema.ProjectImplInfo.ProjectImplInfos;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;

@Path ("/projects/{projtype}/{projid}/ProjectImplInfo")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class ProjectImplInfoResource extends AbstractResource
{
    private ProjectImplInfoManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public ProjectImplInfoResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "ProjectImplInfo.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.ProjectImplInfo");
        MY_PROPS.setProperty("bean", "ProjectImplInfoManagerRemote");
        try {
            beanmanager = (ProjectImplInfoManagerRemote) init(ProjectImplInfoManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("ProjectImplInfoResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("ProjectImplInfoResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;
        try {
            
            us.fed.fs.www.nepa.schema.ProjectImplInfo.ProjectImplInfo ProjectImplInfo= (us.fed.fs.www.nepa.schema.ProjectImplInfo.ProjectImplInfo)unmarshal(inputxml, us.fed.fs.www.nepa.schema.ProjectImplInfo.ProjectImplInfo.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+ProjectImplInfo.getImplInfoId());

            // All clear. Now do the insert.
            beanmanager.create(ProjectImplInfo);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the newly created entity
        return created(uri);
    }
 @GET
    @Path("/{implInfoId}")
    public Response get(@PathParam("projtype") String projtype, @PathParam("projid") String projid,  @PathParam("implInfoId") Integer implInfoId, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            ProjectImplInfo projectImplInfo = beanmanager.get(implInfoId.toString());

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<ProjectImplInfo> jaxbelement = objectfactory.createProjectImplInfo(projectImplInfo);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }

   
  /*
     *  Get list of all project implInfo under this project
     */
    @GET
    public Response getAllByProject(@PathParam("projtype") String projtype, @PathParam("projid") String projid, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the name of the specified unit
            ProjectImplInfos implInfo = beanmanager.getAllByProject(projtype, projid);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<ProjectImplInfos> jaxbelement = objectfactory.createProjectImplInfos(implInfo);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }

    @PUT
    @Path("/{implInfoId}")
    public Response put(@PathParam("projid") String projid, @PathParam("implInfoId") Integer implInfoId, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        ProjectImplInfo ProjectImplInfo = null;
        try {
            ProjectImplInfo = (us.fed.fs.www.nepa.schema.ProjectImplInfo.ProjectImplInfo) unmarshal(inputxml, us.fed.fs.www.nepa.schema.ProjectImplInfo.ProjectImplInfo.class);

            if (projid.compareTo(ProjectImplInfo.getProjectId()) != 0)
                complainLoudly(new Exception("Project ID in uri must match ID in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            if (implInfoId.compareTo(new Integer(ProjectImplInfo.getImplInfoId())) != 0)
                complainLoudly(new Exception("Resource goal ID in uri must match ID in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

           beanmanager.update(ProjectImplInfo);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("");
    }


    @DELETE
    @Path("/{implInfoId}")
    public Response delete(@PathParam("implInfoId") String filename, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(filename);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}

// TODO: restrict access to the admin application