package test.reference;

import client.MilestoneClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.milestone.*;


public class MilestoneTest {

    private MilestoneClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new MilestoneClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postMilestone(TestData.milestones.id1, TestData.milestones.name2);
            // If the above milestonement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Milestone milestone = null;
        Milestones milestonelist = null;
        try {
            milestone = client.getMilestone(TestData.milestones.id1);
            milestonelist = client.getMilestoneList();
        } catch (IOException ex) {
            Logger.getLogger(MilestoneTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.milestones.id1, milestone.getId());
        Assert.assertEquals(TestData.milestones.name1, milestone.getName());
        Assert.assertTrue(milestonelist.getMilestone().get(0).getName().length()>0);
        Assert.assertTrue(milestonelist.getMilestone().get(0).getId().length()>0);

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        Milestone milestone = null;
        try {
            client.putMilestone(TestData.milestones.id1, TestData.milestones.name2);
            milestone = client.getMilestone(TestData.milestones.id1);
        } catch (Exception ex) {
            Logger.getLogger(MilestoneTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.milestones.name2, milestone.getName());

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getMilestone(TestData.milestones.id1);
            // If the above milestonement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteMilestone(TestData.milestones.id1);
            // If the above milestonement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }

    private void post()
    {
        try {
            client.postMilestone(TestData.milestones.id1, TestData.milestones.name1);
        } catch (IOException ex) {
            Logger.getLogger(MilestoneTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteMilestone(TestData.milestones.id1);
        } catch (IOException ex) {
            Logger.getLogger(MilestoneTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteMilestone(TestData.milestones.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }
}