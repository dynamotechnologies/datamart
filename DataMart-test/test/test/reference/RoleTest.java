package test.reference;

import client.RoleClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.role.*;


public class RoleTest {

    private RoleClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new RoleClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postRole(TestData.roles.id1, TestData.roles.name2, TestData.roles.description1);
            // If the above rolement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Role role = null;
        Roles rolelist = null;
        try {
            role = client.getRole(TestData.roles.id1);
            rolelist = client.getRoleList();
        } catch (IOException ex) {
            Logger.getLogger(RoleTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.roles.id1, role.getId());
        Assert.assertEquals(TestData.roles.name1, role.getName());
        Assert.assertEquals(TestData.roles.description1, role.getDescription());
        Assert.assertTrue(rolelist.getRole().get(0).getName().length()>0);
        Assert.assertTrue(rolelist.getRole().get(0).getDescription().length()>0);

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        Role role = null;
        try {
            client.putRole(TestData.roles.id1, TestData.roles.name2, TestData.roles.description2);
            role = client.getRole(TestData.roles.id1);
        } catch (Exception ex) {
            Logger.getLogger(RoleTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.roles.name2, role.getName());
        Assert.assertEquals(TestData.roles.description2, role.getDescription());

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getRole(TestData.roles.id1);
            // If the above rolement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteRole(TestData.roles.id1);
            // If the above rolement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }

    private void post()
    {
        try {
            client.postRole(TestData.roles.id1, TestData.roles.name1, TestData.roles.description1);
        } catch (IOException ex) {
            Logger.getLogger(RoleTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteRole(TestData.roles.id1);
        } catch (IOException ex) {
            Logger.getLogger(RoleTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteRole(TestData.roles.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }
}