package stateless;

import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.categoricalexclusion.*;
import beanutil.DataMartException;

/*
 *
 * The Manager beans are responsible for:
 *   1) Implementing business logic and
 *   2) Translating front-end resources to back-end database entities.
 *
 * Often the resources will consist of data from numerous database tables. It is the manager's responsibility
 *   to ensure that these front-end resources can be accessed as though they were cut from a single piece of wood.
 *
 */
@Remote
public interface CategoricalExclusionManagerRemote {
    public void create(Categoricalexclusion resource) throws DataMartException;
    public Categoricalexclusion get(Integer id) throws DataMartException;
    public Categoricalexclusions getAll() throws DataMartException;
    public void update(Categoricalexclusion resource) throws DataMartException;
    public void delete(Integer id) throws DataMartException;
    public Boolean ping();
}