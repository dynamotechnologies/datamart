package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.AppealStatusManagerRemote;
import us.fed.fs.www.nepa.schema.appealstatus.Appealstatus;
import us.fed.fs.www.nepa.schema.appealstatus.Appealstatuses;
import us.fed.fs.www.nepa.schema.appealstatus.ObjectFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;


@Path ("/ref/appealstatuses")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class AppealStatusResource extends AbstractResource
{
    private AppealStatusManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public AppealStatusResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "AppealStatus.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.appealstatus");
        MY_PROPS.setProperty("bean", "AppealStatusManagerRemote");
        try {
            beanmanager = (AppealStatusManagerRemote) init(AppealStatusManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("AppealStatusResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("AppealStatusResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;
        try {
            Appealstatus status = (Appealstatus)unmarshal(inputxml, Appealstatus.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+status.getId());

            // All clear. Now do the insert.
            beanmanager.create(status);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        // Return a link to the newly created entity
        return created(uri);
    }


    @GET
    public Response get(@Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get a dump of all the entities in the datamart
            Appealstatuses statuslist = beanmanager.getAll();

            // Return 404 if none exist
            if (statuslist.getAppealstatus().isEmpty())
                notFound();

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Appealstatuses> jaxbelement = objectfactory.createAppealstatuses(statuslist);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }

    @GET
    @Path("/{id}")
    public Response get(@PathParam("id") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the name of the specified entity
            String name = beanmanager.getName(id);
            Appealstatus status = new Appealstatus();
            status.setId(id);
            status.setName(name);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Appealstatus> jaxbelement = objectfactory.createAppealstatus(status);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @PUT
    @Path("/{id}")
    public Response put(@PathParam("id") String id, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        Appealstatus status = null;
        try {
            status = (Appealstatus) unmarshal(inputxml, Appealstatus.class);

            if (!id.equalsIgnoreCase(status.getId()))
                complainLoudly(new Exception("Id in uri must match id in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.update(status);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"/"+status.getId()+"</location>");
    }


    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(id);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}