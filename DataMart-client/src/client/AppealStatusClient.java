package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.appealstatus.*;


public class AppealStatusClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public AppealStatusClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.appealstatus", "AppealStatus.xsd");
    }


    public void postAppealStatus(String id, String name)
            throws IOException
    {
        Appealstatus status = new Appealstatus();
        status.setId(id);
        status.setName(name);
        postAppealStatus(status);
    }

    public void postAppealStatus(Appealstatus status)
            throws IOException
    {
        post(objectfactory.createAppealstatus(status), "/ref/appealstatuses");
    }


    public Appealstatus getAppealStatus(String id)
            throws IOException
    {
        return get("/ref/appealstatuses/" + id, Appealstatus.class);
    }


    public Appealstatuses getAppealStatusList()
            throws IOException
    {
        return get("/ref/appealstatuses", Appealstatuses.class);
    }


    public void putAppealStatus(String id, String name)
            throws IOException
    {
        Appealstatus status = new Appealstatus();
        status.setId(id);
        status.setName(name);
        putAppealStatus(status);
    }


    public void putAppealStatus(Appealstatus status)
            throws IOException
    {
        put(objectfactory.createAppealstatus(status), "/ref/appealstatuses/" + status.getId());
    }


    public void deleteAppealStatus(String id)
            throws IOException
    {
        delete("/ref/appealstatuses/" + id);
    }
}