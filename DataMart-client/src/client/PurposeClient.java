package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.purpose.*;


public class PurposeClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public PurposeClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.purpose", "Purpose.xsd");
    }


    public void postPurpose(String id, String name)
            throws IOException
    {
        Purpose purpose = new Purpose();
        purpose.setId(id);
        purpose.setName(name);
        postPurpose(purpose);
    }


    public void postPurpose(Purpose purpose)
            throws IOException
    {
        post(objectfactory.createPurpose(purpose), "/ref/purposes");
    }


    public Purpose getPurpose(String id)
            throws IOException
    {
        return get("/ref/purposes/" + id, Purpose.class);
    }


    public Purposes getPurposeList()
            throws IOException
    {
        return get("/ref/purposes", Purposes.class);
    }


    public void putPurpose(String id, String name)
            throws IOException
    {
        Purpose purpose = new Purpose();
        purpose.setId(id);
        purpose.setName(name);
        putPurpose(purpose);
    }


    public void putPurpose(Purpose purpose)
            throws IOException
    {
        put(objectfactory.createPurpose(purpose), "/ref/purposes/" + purpose.getId());
    }


    public void deletePurpose(String id)
            throws IOException
    {
        delete("/ref/purposes/" + id);
    }
}