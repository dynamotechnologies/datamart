package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.appealdismissalreason.*;


public class AppealDismissalReasonClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public AppealDismissalReasonClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.appealdismissalreason", "AppealDismissalReason.xsd");
    }


    public void postAppealDismissalReason(String id, String name)
            throws IOException
    {
        Appealdismissalreason adr = new Appealdismissalreason();
        adr.setId(id);
        adr.setName(name);
        postAppealDismissalReason(adr);
    }


    public void postAppealDismissalReason(Appealdismissalreason adr)
            throws IOException
    {
        post(objectfactory.createAppealdismissalreason(adr), "/ref/appealdismissalreasons");
    }


    public Appealdismissalreason getAppealDismissalReason(String id)
            throws IOException
    {
        return get("/ref/appealdismissalreasons/" + id, Appealdismissalreason.class);
    }


    public Appealdismissalreasons getAppealDismissalReasonList()
            throws IOException
    {
        return get("/ref/appealdismissalreasons", Appealdismissalreasons.class);
    }


    public void putAppealDismissalReason(String id, String name)
            throws IOException
    {
        Appealdismissalreason adr = new Appealdismissalreason();
        adr.setId(id);
        adr.setName(name);
        putAppealDismissalReason(adr);
    }


    public void putAppealDismissalReason(Appealdismissalreason adr)
            throws IOException
    {
        put(objectfactory.createAppealdismissalreason(adr), "/ref/appealdismissalreasons/" + adr.getId());
    }


    public void deleteAppealDismissalReason(String id)
            throws IOException
    {
        delete("/ref/appealdismissalreasons/" + id);
    }
}