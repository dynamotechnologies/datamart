/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author gauri
 */
@Entity
@Table(name = "ProjectViewDocs")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProjectViewDocs.findAll", query = "SELECT p FROM ProjectViewDocs p"),
    @NamedQuery(name = "ProjectViewDocs.findByProjectViewDocsId", query = "SELECT p FROM ProjectViewDocs p WHERE p.projectViewDocsId = :projectViewDocsId")})
public class ProjectViewDocs implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ProjectViewDocsId")
    private Integer projectViewDocsId;
    @JoinColumn(name = "DocId", referencedColumnName = "Id")
    @ManyToOne
    private ProjectDocuments docId;
    @JoinColumn(name = "ViewId", referencedColumnName = "ViewId")
    @ManyToOne
    private ProjectViews viewId;

    public ProjectViewDocs() {
    }

    public ProjectViewDocs(Integer projectViewDocsId) {
        this.projectViewDocsId = projectViewDocsId;
    }

    public Integer getProjectViewDocsId() {
        return projectViewDocsId;
    }

    public void setProjectViewDocsId(Integer projectViewDocsId) {
        this.projectViewDocsId = projectViewDocsId;
    }

    public ProjectDocuments getDocId() {
        return docId;
    }

    public void setDocId(ProjectDocuments docId) {
        this.docId = docId;
    }

    public ProjectViews getViewId() {
        return viewId;
    }

    public void setViewId(ProjectViews viewId) {
        this.viewId = viewId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (projectViewDocsId != null ? projectViewDocsId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProjectViewDocs)) {
            return false;
        }
        ProjectViewDocs other = (ProjectViewDocs) object;
        if ((this.projectViewDocsId == null && other.projectViewDocsId != null) || (this.projectViewDocsId != null && !this.projectViewDocsId.equals(other.projectViewDocsId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ProjectViewDocs[ projectViewDocsId=" + projectViewDocsId + " ]";
    }
    
}
