package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.PurposeManagerRemote;
import us.fed.fs.www.nepa.schema.purpose.ObjectFactory;
import us.fed.fs.www.nepa.schema.purpose.Purpose;
import us.fed.fs.www.nepa.schema.purpose.Purposes;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;


@Path ("/ref/purposes")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class PurposeResource extends AbstractResource
{
    private PurposeManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public PurposeResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "Purpose.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.purpose");
        MY_PROPS.setProperty("bean", "PurposeManagerRemote");
        try {
            beanmanager = (PurposeManagerRemote) init(PurposeManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("PurposeResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("PurposeResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;
        try {
            Purpose purpose = (Purpose)unmarshal(inputxml, Purpose.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+purpose.getId());

            // All clear. Now do the insert.
            beanmanager.create(purpose);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        // Return a link to the newly created entity
        return created(uri);
    }


    @GET
    public Response get(@Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get a dump of all the entities in the datamart
            Purposes purposelist = beanmanager.getAll();

            // Return 404 if none exist
            if (purposelist.getPurpose().isEmpty())
                notFound();

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Purposes> jaxbelement = objectfactory.createPurposes(purposelist);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }

    @GET
    @Path("/{id}")
    public Response get(@PathParam("id") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the name of the specified entity
            String name = beanmanager.getName(id);
            Purpose purpose = new Purpose();
            purpose.setId(id);
            purpose.setName(name);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Purpose> jaxbelement = objectfactory.createPurpose(purpose);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @PUT
    @Path("/{id}")
    public Response put(@PathParam("id") String id, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        Purpose purpose = null;
        try {
            purpose = (Purpose) unmarshal(inputxml, Purpose.class);

            if (!id.equalsIgnoreCase(purpose.getId()))
                complainLoudly(new Exception("Id in uri must match id in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.update(purpose);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"/"+purpose.getId()+"</location>");
    }


    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(id);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}