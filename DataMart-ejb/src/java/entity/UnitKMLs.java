package entity;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "UnitKMLs")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UnitKMLs.findAll", query = "SELECT u FROM UnitKMLs u"),
    @NamedQuery(name = "UnitKMLs.findById", query = "SELECT u FROM UnitKMLs u WHERE u.id = :id"),
    @NamedQuery(name = "UnitKMLs.findByLabel", query = "SELECT u FROM UnitKMLs u WHERE u.label = :label"),
    @NamedQuery(name = "UnitKMLs.findByMapType", query = "SELECT u FROM UnitKMLs u WHERE u.mapType = :mapType")})
public class UnitKMLs implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private String id;
    @Column(name = "Label")
    private String label;
    @Column(name = "MapType")
    private String mapType;
    @JoinColumn(name = "UnitId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private RefUnits refUnits;

    public UnitKMLs() {
    }

    public UnitKMLs(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getMapType() {
        return mapType;
    }

    public void setMapType(String mapType) {
        this.mapType = mapType;
    }

    public RefUnits getRefUnits() {
        return refUnits;
    }

    public void setRefUnits(RefUnits refUnits) {
        this.refUnits = refUnits;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UnitKMLs)) {
            return false;
        }
        UnitKMLs other = (UnitKMLs) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.UnitKMLs[ id=" + id + " ]";
    }

}
