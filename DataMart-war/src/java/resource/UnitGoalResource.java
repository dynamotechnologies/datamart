package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.UnitGoalManagerRemote;
import us.fed.fs.www.nepa.schema.unitgoal.ObjectFactory;
import us.fed.fs.www.nepa.schema.unitgoal.Unitgoal;
import us.fed.fs.www.nepa.schema.unitgoal.Unitgoals;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;

@Path ("/ref/units/{unitid}/goals")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class UnitGoalResource extends AbstractResource
{
    private UnitGoalManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public UnitGoalResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "UnitGoal.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.unitgoal");
        MY_PROPS.setProperty("bean", "UnitGoalManagerRemote");
        try {
            beanmanager = (UnitGoalManagerRemote) init(UnitGoalManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("UnitGoalResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("UnitGoalResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(@PathParam("unitid") String unitid, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;

        try {
            Unitgoal goal = (Unitgoal)unmarshal(inputxml, Unitgoal.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+goal.getGoalid());

            if (unitid.compareTo(goal.getUnitid()) != 0)
                complainLoudly(new Exception("Unit ID in uri must match ID in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the insert.
            beanmanager.create(goal);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the newly created entity
        return created(uri);
    }


    @GET
    @Path("/{goalid}")
    public Response get(@PathParam("unitid") String unitid, @PathParam("goalid") Integer goalid, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            Unitgoal goal = beanmanager.get(unitid, goalid);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Unitgoal> jaxbelement = objectfactory.createUnitgoal(goal);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    /*
     *  Get list of all project goals under this project
     */
    @GET
    public Response getAllByUnit(@PathParam("unitid") String unitid, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the name of the specified unit
            Unitgoals goals = beanmanager.getAllByUnit(unitid);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Unitgoals> jaxbelement = objectfactory.createUnitgoals(goals);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }

    @PUT
    @Path("/{goalid}")
    public Response put(@PathParam("unitid") String unitid, @PathParam("goalid") Integer goalid, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        Unitgoal unitgoal = null;
        try {
            unitgoal = (Unitgoal) unmarshal(inputxml, Unitgoal.class);

            if (unitid.compareTo(unitgoal.getUnitid()) != 0)
                complainLoudly(new Exception("Unit ID in uri must match ID in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            if (goalid != unitgoal.getGoalid())
                complainLoudly(new Exception("Resource goal ID in uri must match ID in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.update(unitgoal);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"</location>");
    }


    @DELETE
    @Path("/{goalid}")
    public Response delete(@PathParam("unitid") String unitid, @PathParam("goalid") Integer goalid, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(unitid, goalid);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}
