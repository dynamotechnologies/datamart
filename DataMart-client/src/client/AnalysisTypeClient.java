package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.analysistype.*;


public class AnalysisTypeClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public AnalysisTypeClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.analysistype", "AnalysisType.xsd");
    }


    public void postAnalysisType(String id, String name)
            throws IOException
    {
        Analysistype analysistype = new Analysistype();
        analysistype.setId(id);
        analysistype.setName(name);
        postAnalysisType(analysistype);
    }


    public void postAnalysisType(Analysistype analysistype)
            throws IOException
    {
        post(objectfactory.createAnalysistype(analysistype), "/ref/analysistypes");
    }


    public Analysistype getAnalysisType(String id)
            throws IOException
    {
        return get("/ref/analysistypes/" + id, Analysistype.class);
    }


    public Analysistypes getAnalysisTypeList()
            throws IOException
    {
        return get("/ref/analysistypes", Analysistypes.class);
    }


    public void putAnalysisType(String id, String name)
            throws IOException
    {
        Analysistype analysistype = new Analysistype();
        analysistype.setId(id);
        analysistype.setName(name);
        putAnalysisType(analysistype);
    }


    public void putAnalysisType(Analysistype analysistype)
            throws IOException
    {
        put(objectfactory.createAnalysistype(analysistype), "/ref/analysistypes/" + analysistype.getId());
    }
    

    public void deleteAnalysisType(String id)
            throws IOException
    {
        delete("/ref/analysistypes/" + id);
    }
}