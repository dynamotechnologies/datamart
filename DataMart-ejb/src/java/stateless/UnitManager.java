package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.unit.*;
import us.fed.fs.www.nepa.schema.objection.Objections;
import us.fed.fs.www.nepa.schema.project.Projects;

import beanutil.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless(mappedName="UnitManagerRemote")
public class UnitManager implements UnitManagerRemote
{

    @PersistenceContext
    private EntityManager em;
    

    // TODO: catch org.eclipse.persistence.exceptions.DatabaseException and report failure gracefully
    //  currently this will be thrown if, for example, the data size exceeds the database field size.
    //  It currently throws http 409, which is misleading. Do this for all manager beans.
    @Override
    public void create(Unit resource)
             throws DataMartException
    {
        validate(resource);
        entity.RefUnits entity = createEntity(resource);

        entity.RefUnits badentity = createEntity(entity.getId());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);
        
        entity = em.merge(entity);
    }

    @Override
    public Units getAll()
             throws DataMartException
    {
        // Retrieve all entities from the database
        // TODO: use named query here. Look through all manager beans for createQuery() and similar methods. Create a named query for
        //       all queries. Also don't query anything from this level. Look at ProjectManager.java and use
        //       the getEntity() method as an example for how this should work.
        //
        Query query = em.createQuery("SELECT u FROM RefUnits u");
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Units responselist = new Units();
        while (it.hasNext()) {
            entity.RefUnits entity = (entity.RefUnits)it.next();
            Unit resource = createResource(entity);
            addLinks(entity, resource);
            responselist.getUnit().add(resource);
        }
        return responselist;
    }

    @Override
    public Units getAllUnique()
             throws DataMartException
    {
        // Retrieve all entities from the database
        // TODO: use named query here. Look through all manager beans for createQuery() and similar methods. Create a named query for
        //       all queries. Also don't query anything from this level. Look at ProjectManager.java and use
        //       the getEntity() method as an example for how this should work.
        //
        Query query = em.createQuery("SELECT u FROM RefUnits u  where u.id not like :code00");
        query.setParameter("code00", "%00");
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Units responselist = new Units();
        while (it.hasNext()) {
            entity.RefUnits entity = (entity.RefUnits)it.next();
            Unit resource = createResource(entity);
            addLinks(entity, resource);
            responselist.getUnit().add(resource);
        }
        return responselist;
    }
    
    @Override
    public Unit get(String id)
            throws DataMartException
    {
        //TODO need to check this
        if(id.equals("refresh")){
            em.getEntityManagerFactory().getCache().evict(Unit.class);
               id="000000";
        }
            entity.RefUnits entity = createEntity(id);
            if (entity==null)
                throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

            Unit response = createResource(entity);
            addLinks(entity, response);
            return response;
    }

    @Override
    public void update(Unit resource)
             throws DataMartException
    {
        validate(resource);
        entity.RefUnits entity = createEntity(resource);
        
        entity.RefUnits existingEntity = createEntity(entity.getId());
        
        if (existingEntity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        
        if (entity.getPhone() == null){
            entity.setPhone(existingEntity.getPhone());
        }
        if (entity.getWwwLink() == null){
            entity.setWwwLink(existingEntity.getWwwLink());
        }
        
        entity = em.merge(entity);
    }

    @Override
    public void delete(String id)
             throws DataMartException
    {
        entity.RefUnits entity = createEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));
    }


    /*
     * SPECIAL REQUESTS
     */


    @Override
    public Units getRegions()
            throws DataMartException
    {
        // Get all units with codes that are four digits long
        Query query = em.createQuery("SELECT r FROM RefUnits r WHERE LENGTH(r.id) = 4");
        List entitylist =  query.getResultList();

        if (!(entitylist.size()>0))
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        // Loop over the database collection. Each row becomes a new object in the response list
        Iterator it = entitylist.iterator();
        Units responselist = new Units();
        while (it.hasNext()) {
            responselist.getUnit().add(createResource( (entity.RefUnits)it.next() ));
        }
        return responselist;
    }


    @Override
    public Units getSubUnits(String id)
            throws DataMartException
    {
        // Confirm that this is a valid unit
        entity.RefUnits entity = createEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        // Get all units with codes starting with this unit's code (subunits)
        Query query = em.createQuery("SELECT r FROM RefUnits r WHERE SUBSTRING(r.id, 1, :chunklen) = :id");
        query.setParameter("id", id);
        query.setParameter("chunklen", id.length());
        List entitylist =  query.getResultList();

        if (!(entitylist.size()>0))
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        // Loop over the database collection. Each row becomes a new object in the response list
        Iterator it = entitylist.iterator();
        Units responselist = new Units();
        while (it.hasNext()) {
            responselist.getUnit().add(createResource( (entity.RefUnits)it.next() ));
        }
        return responselist;
    }

    
    @Override
    public Objections getObjections(String id, String startdate, String enddate)
            throws DataMartException
    {
        // Confirm that this is a valid unit
        entity.RefUnits entity = createEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        
        // TODO: filter by startdate, enddate


        // Get objections for this unit
        Query query = em.createQuery("SELECT o FROM Objections o JOIN o.projects p WHERE p.refUnits LIKE ':id'");
        query.setParameter("id", id);
        List entitylist =  query.getResultList();

        if (!(entitylist.size()>0))
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        // Loop over the database collection. Each row becomes a new object in the response list
        Iterator it = entitylist.iterator();
        Objections responselist = new Objections();
        while (it.hasNext()) {
            responselist.getObjection().add(ObjectionManager.createResource( (entity.Objections)it.next() ));
        }
        return responselist;
    }


    @Override
    public Projects getProjects(String id)
            throws DataMartException
    {
        // Confirm that this is a valid unit
        entity.RefUnits entity = createEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        // Get projects for this unit
        Query query = em.createQuery("SELECT p FROM Projects p WHERE p.refUnits LIKE ':id'");
        query.setParameter("id", id);
        List entitylist =  query.getResultList();

        if (!(entitylist.size()>0))
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        // Loop over the database collection. Each row becomes a new object in the response list
        Iterator it = entitylist.iterator();
        Projects responselist = new Projects();
        while (it.hasNext()) {
            responselist.getProject().add(ProjectManager.createResource( (entity.Projects)it.next() ));
        }
        return responselist;
    }
    

    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     *
     * This method does the mapping from entity object to jaxb object
     */
    // TODO: It has been decided that all createResource methods now need to be protected
    //    static methods. This allows for JPQL JOINs and special requests. Go through all
    //    manager beans and update these methods. The same thing should probably also be considered
    //    for createEntity.
    protected static Unit createResource(entity.RefUnits entity)
            throws DataMartException
    {
        Unit resource = new Unit();
        resource.setCode(entity.getId());
        resource.setName(entity.getName());
        resource.setExtendeddetails(true);  //Deprecated field - always true
        resource.setProjectmap(entity.getProjectMap());
        resource.setAddress1(entity.getAddress1());
        resource.setAddress2(entity.getAddress2());
        resource.setCity(entity.getCity());
        resource.setState(entity.getState());
        resource.setZip(entity.getZip());
        resource.setPhonenumber(entity.getPhone());
        resource.setWwwlink(entity.getWwwLink());
        resource.setCommentemail(entity.getCommentEmail());
        resource.setNewspaper(entity.getNewspaper());
        resource.setNewspaperurl(entity.getNewspaperURL());
        resource.setBoundaryurl(entity.getBoundaryURL());
        resource.setActive(entity.getActive());
        resource.setSpotlightid1(entity.getSpotlightId1());
        resource.setSpotlightid2(entity.getSpotlightId2());
        resource.setSpotlightid3(entity.getSpotlightId3());
        return resource;
    }

    /*
     *
     * This method does the mapping from jaxb object to entity object
     */
    private entity.RefUnits createEntity(Unit resource)
            throws DataMartException
    {
        entity.RefUnits entity = new entity.RefUnits();
        entity.setId(resource.getCode());
        entity.setName(resource.getName());
        if (resource.isProjectmap() != null)
            entity.setProjectMap(resource.isProjectmap());
        else
            entity.setProjectMap(false);
        entity.setAddress1(resource.getAddress1());
        entity.setAddress2(resource.getAddress2());
        entity.setCity(resource.getCity());
        entity.setState(resource.getState());
        entity.setZip(resource.getZip());
        entity.setPhone(resource.getPhonenumber());
        entity.setWwwLink(resource.getWwwlink());
        entity.setCommentEmail(resource.getCommentemail());
        entity.setNewspaper(resource.getNewspaper());
        entity.setNewspaperURL(resource.getNewspaperurl());
        entity.setBoundaryURL(resource.getBoundaryurl());
        entity.setSpotlightId1(resource.getSpotlightid1());
        entity.setSpotlightId2(resource.getSpotlightid2());
        entity.setSpotlightId3(resource.getSpotlightid3());
        
        if (resource.isActive() == null) {
            entity.setActive(true);
        } else {
            entity.setActive(resource.isActive());
        }
        entity.setExtendedDetails(true);//Deprecated field - All Units have extended details.
        return entity;
    }

    // This version of overloaded method createEntity() accepts a String ID as a parameter, and returns the
    //   associated entity, or null if it doesn't exist
    //
    // TODO: should this method be called getEntity(id) ?
    private entity.RefUnits createEntity(String id)
            throws DataMartException
    {
        return em.find(entity.RefUnits.class, id);
    }

    //
    // TODO: Validation should be performed in the createEntity() method. Use ProjectManager
    //       as an example. Do this for all manager beans.
    //
    // TODO: Obtain business rules for validation and implement them with friendly error messages on POST/PUT
    //
    private void validate(Unit resource)
            throws DataMartException
    {
        DataMartBeanUtil.confirmNotNull(resource.getName(), "Name");
    }

    private void addLinks(entity.RefUnits unit, Unit resource)
            throws DataMartException
    {
        attachKMLLinks(unit, resource);
    }
    
    private void attachKMLLinks(entity.RefUnits unit, Unit resource)
        throws DataMartException
    {
        String prefix = DataMartBeanUtil.getConfig(em, ConfigManager.EDGESERVER_URL_PREFIX);

        if (unit == null) { return; }

        // Retrieve UnitKMLs entities from the database
        Query query = em.createQuery("SELECT k FROM UnitKMLs k WHERE k.refUnits = :unit");
        query.setParameter("unit", unit);
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Links responselist = new Links();
        while (it.hasNext()) {
            Link link = new Link();
            entity.UnitKMLs unitkml = (entity.UnitKMLs)it.next();
            String date =new SimpleDateFormat("yyMMdd").format(new Date());
            if(unitkml.getId().indexOf("kml") != -1)
            date =new SimpleDateFormat("yyMMddhh").format(new Date());
            link.setHref(prefix + "/units/" + unitkml.getId()+"?refresh="+date);
            link.setRel("unitkml");
            link.setTitle(unitkml.getLabel());
            link.setType(unitkml.getMapType());
            responselist.getLink().add(link);
        }
        if (entitylist.size() > 0) {
            Links links = resource.getLinks();
            if (links == null) {
                links = new Links();
                resource.setLinks(links);
            }
            links.getLink().addAll(responselist.getLink());
        }
    }
}