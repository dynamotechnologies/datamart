/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package stateless;
import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.county.*;
import beanutil.DataMartException;

/**
 *
 * @author mhsu
 */
@Remote
public interface CountyManagerRemote {
    public void create(County resource) throws DataMartException;
    public String getName(String id) throws DataMartException;
    public void update(County resource) throws DataMartException;
    public void delete(String id) throws DataMartException;
    public Counties getAll() throws DataMartException;
    public Boolean ping();
}