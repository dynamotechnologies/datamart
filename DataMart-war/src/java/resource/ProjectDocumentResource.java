package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.CARAPhaseDocManagerRemote;
import stateless.ProjectDocumentManagerRemote;
import us.fed.fs.www.nepa.schema.projectdocument.ObjectFactory;
import us.fed.fs.www.nepa.schema.projectdocument.Projectdocument;
import us.fed.fs.www.nepa.schema.projectdocument.Projectdocuments;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;


@Path ("/projects/{type}/{projectid}/docs")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class ProjectDocumentResource extends AbstractResource
{
    private CARAPhaseDocManagerRemote caraphasedocbeanmanager;
    private ProjectDocumentManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public ProjectDocumentResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "ProjectDocument.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.projectdocument");
        MY_PROPS.setProperty("bean", "ProjectDocumentManagerRemote");
        try {
            beanmanager = (ProjectDocumentManagerRemote) init(ProjectDocumentManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("ProjectDocumentResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("ProjectDocumentResource failed to start. Could not ping the bean manager!");
    
        UTIL = DataMartUtil.getInstance();
        if (UTIL==null)
            throw new ServletException("Horror! Could not get a reference to the Datamart Utility singleton! Exiting in shame.");

        Properties CARAPHASEDOC_PROPS = new Properties();
        CARAPHASEDOC_PROPS.setProperty("schema", "CaraDocument.xsd");
        CARAPHASEDOC_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.caradocument");
        CARAPHASEDOC_PROPS.setProperty("bean", "CARAPhaseDocManagerRemote");
        
       
        try {
            int CARAPHASEDOC_ID = UTIL.getResourceId(CARAPHASEDOC_PROPS);
            caraphasedocbeanmanager = (CARAPhaseDocManagerRemote)UTIL.getManager(CARAPHASEDOC_ID);
        } catch (IOException ex) {
            log.error(ex.toString());
        }
        
     
    
    
    
    }


    @POST
    public Response post(@PathParam("type") String type, @PathParam("projectid") String projectid, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;
        try {
            Projectdocument projdoc = (Projectdocument)unmarshal(inputxml, Projectdocument.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+projdoc.getDocid());

            if (!type.equalsIgnoreCase(projdoc.getProjecttype()))
                complainLoudly(new Exception("Type in uri must match type in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            if (!projectid.equalsIgnoreCase(projdoc.getProjectid()))
                complainLoudly(new Exception("Project Id in uri must match project id in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the insert.
            beanmanager.create(projdoc);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        // Return a link to the newly created entity
        return created(uri);
    }


    /*
     * Get list of project-documents
     *
     * Optional query parameters:
     *      pubflag (only retrieve documents with a matching boolean (1/0) pubflag value)
     *      sensitiveflag (only retrieve documents with a matching (1/0) sensitiveflag value)
     *      rows (integer maximum number of rows to retrieve)
     *      start (integer zero-based index of first row to retrieve)
     */
    @GET
    public Response get(@PathParam("type") String type,
                @PathParam("projectid") String projectid,
                @QueryParam("pubflag") Integer pubflagval,
                @QueryParam("sensitiveflag") Integer sensitiveflagval,
                @QueryParam("start") Integer startval,
                @QueryParam("rows") Integer rowsval,
                @QueryParam("ignorecaraflag") Integer ignorecaraflagval,
                //@QueryParam("phaseid") Integer phaseid,
                @QueryParam("viewId") Integer viewId,
                @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        Projectdocuments projdocs;

        try {
            Boolean pubflag = null;
            Boolean sensitiveflag = null;
            Boolean ignorecaraflag = null;
            if (pubflagval != null) {
                if ((pubflagval == 0) || (pubflagval == 1)) {
                    pubflag = (pubflagval == 1);
                } else {
                  complainLoudly(new Exception("Bad pubflag value"), Response.Status.BAD_REQUEST);
                }
            }
            if (sensitiveflagval != null) {
                if ((sensitiveflagval == 0) || (sensitiveflagval == 1)) {
                    sensitiveflag = (sensitiveflagval == 1);
                } else {
                  complainLoudly(new Exception("Bad sensitiveflag value"), Response.Status.BAD_REQUEST);
                }
            }
            if (ignorecaraflagval != null) {
                if ((ignorecaraflagval == 0) || (ignorecaraflagval == 1)) {
                    ignorecaraflag = (ignorecaraflagval == 1);
                } else {
                  complainLoudly(new Exception("Bad ignorecaraflag value"), Response.Status.BAD_REQUEST);
                }
            }

            projdocs = beanmanager.getAll(type, projectid, pubflag, sensitiveflag, ignorecaraflag, startval, rowsval,viewId);
            /*Projectdocuments doclist = new Projectdocuments();
            if(phaseid !=null && null !=projdocs)
            {
             List caraphaseDocs= caraphasedocbeanmanager.getAll(phaseid);
             if(null !=projdocs.getProjectdocument()){
             Iterator it = projdocs.getProjectdocument().iterator();
             while(it.hasNext())
             {
              Projectdocument doc =(Projectdocument) it.next();
              if(caraphaseDocs.contains(doc.getDocid()))
               doclist.getProjectdocument().add(doc);
             }
             projdocs=doclist;
             }
            }*/
            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Projectdocuments> jaxbelement = objectfactory.createProjectdocuments(projdocs);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    /*
     * Get single project-document
     */
    @GET
    @Path("{id}")
    public Response get(@PathParam("type") String type, @PathParam("projectid") String projectid, @PathParam("id") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            Projectdocument projdoc = beanmanager.get(type, projectid, id);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Projectdocument> jaxbelement = objectfactory.createProjectdocument(projdoc);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }

    @PUT
    @Path("{id}")
    public Response put(@PathParam("type") String type, @PathParam("projectid") String projectid, @PathParam("id") String id, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            Projectdocument projdoc = (Projectdocument) unmarshal(inputxml, Projectdocument.class);

            if (!type.equalsIgnoreCase(projdoc.getProjecttype()))
                complainLoudly(new Exception("Project Document type in uri must match project document type in xml."),
                        Response.Status.UNSUPPORTED_MEDIA_TYPE);

            if (!projectid.equalsIgnoreCase(projdoc.getProjectid()))
                complainLoudly(new Exception("Project Id in uri must match project id in xml."),
                        Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.update(projdoc);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"</location>");
    }


    @DELETE
    @Path("{id}")
    public Response delete(@PathParam("type") String type, @PathParam("projectid") String projectid, @PathParam("id") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(type, projectid, id);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}