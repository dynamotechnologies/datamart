#!/bin/sh

# This script breaks up a large ProjectXml into smaller portions.
# It is only meant to be used when processing a file too large for the ingresser.
# Usage: ./split-projectxml.sh ProjectXml_12345.xml
# Result: creates out-files/ folder, splits ProjectXml into fragments by <unit>
# 
# To run the ingresser against the generated files, temporarily update the value of projectfiles_dir in legacyconf.py to the out-files/ folder and the value of projectxml.index to 1. 



#Create output folder
mkdir out-files

#Copy target ProjectXml to output folder
cp $1 out-files/$1

cd out-files

#Split the ProjectXml into segments of each <unit> element
csplit -f "ProjectXml_" -b "%04d.tmp" $1 /\<unit\>/ {*}

#the first fragment is the prefix to apply to each segment
mv ProjectXml_0000.tmp prefix.fragment

#Apply the prefix and closing XML tag to each segment
for file in *.tmp; do
 cat prefix.fragment "$file" > tmp.txt
 mv tmp.txt "$file"
 echo '</unitprojectsdetail>' >> "$file"
done

#Rename all segment files from .tmp to .xml
for file in *.tmp; do
 mv "$file" "${file%.tmp}.xml"
done

#remove the source file and prefix file
rm $1
rm prefix.fragment

echo "Don't forget to edit the last file created to remove the duplicate </unitprojectsdetail> tags"
