package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "RefDecisionTypes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RefDecisionTypes.findAll", query = "SELECT r FROM RefDecisionTypes r"),
    @NamedQuery(name = "RefDecisionTypes.findById", query = "SELECT r FROM RefDecisionTypes r WHERE r.id = :id"),
    @NamedQuery(name = "RefDecisionTypes.findByDescription", query = "SELECT r FROM RefDecisionTypes r WHERE r.description = :description"),
    @NamedQuery(name = "RefDecisionTypes.findByPalsId", query = "SELECT r FROM RefDecisionTypes r WHERE r.palsId = :palsId")})
public class RefDecisionTypes implements Serializable {
    @OneToMany(mappedBy = "refDecisionTypes")
    private Collection<Decisions> decisionsCollection;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private String id;
    @Basic(optional = false)
    @Column(name = "Description")
    private String description;
    @Basic(optional = false)
    @Column(name = "PalsId")
    private int palsId;

    public RefDecisionTypes() {
    }

    public RefDecisionTypes(String id) {
        this.id = id;
    }

    public RefDecisionTypes(String id, String description, int palsId) {
        this.id = id;
        this.description = description;
        this.palsId = palsId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPalsId() {
        return palsId;
    }

    public void setPalsId(int palsId) {
        this.palsId = palsId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RefDecisionTypes)) {
            return false;
        }
        RefDecisionTypes other = (RefDecisionTypes) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RefDecisionTypes[ id=" + id + " ]";
    }

    @XmlTransient
    public Collection<Decisions> getDecisionsCollection() {
        return decisionsCollection;
    }

    public void setDecisionsCollection(Collection<Decisions> decisionsCollection) {
        this.decisionsCollection = decisionsCollection;
    }

}
