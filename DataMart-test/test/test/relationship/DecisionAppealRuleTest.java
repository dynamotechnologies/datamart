package test.relationship;

import client.DecisionAppealRuleClient;
import client.DecisionClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import test.TestData;
import test.entity.DecisionTest;
import us.fed.fs.www.nepa.schema.decisionappealrule.*;


public class DecisionAppealRuleTest {
    private DecisionAppealRuleClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new DecisionAppealRuleClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should succeed, because there is no PUT
        try {
            client.postDecisionAppealRule(TestData.decisions.id1, TestData.decisionappealrules.id1);
        } catch (IOException ex) {
            Logger.getLogger(DecisionAppealRuleTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Decisionappealrule dar = null;
        Decisionappealrules darlist = null;
        try {
            dar = client.getDecisionAppealRule(TestData.decisions.id1, TestData.decisionappealrules.id1);
            darlist = client.getDecisionAppealRuleList(TestData.decisions.id1);
        } catch (IOException ex) {
            Logger.getLogger(DecisionAppealRuleTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.decisionappealrules.id1, dar.getAppealrule());
        Assert.assertTrue(darlist.getDecisionappealrule().get(0).getAppealrule().length()>0);

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getDecisionAppealRule(TestData.decisions.id1, TestData.decisionappealrules.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteDecisionAppealRule(TestData.decisions.id1, TestData.decisionappealrules.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }


    private void post()
    {
        try {
            client.postDecisionAppealRule(TestData.decisions.id1, TestData.decisionappealrules.id1);

        } catch (IOException ex) {
            Logger.getLogger(DecisionAppealRuleTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteDecisionAppealRule(TestData.decisions.id1, TestData.decisionappealrules.id1);
        } catch (IOException ex) {
            Logger.getLogger(DecisionAppealRuleTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteDecisionAppealRule(TestData.decisions.id1, TestData.decisionappealrules.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }


    @BeforeClass
    public static void  populateDependencies()
    {
        DecisionClient decisionclient = null;
        try {
            decisionclient = new DecisionClient();
        } catch (Exception ex) {
            Assert.fail();
        }

        // DecisionAppealRules rely on Decisions. Pre-populate a Decision.
        try {
            // First, pre-populate Decision dependencies
            DecisionTest.populateDependencies();

            decisionclient.postDecision(TestData.decisions.id1,
                    TestData.projects.id1,
                    TestData.projects.type1,
                    TestData.decisions.name1,
                    TestData.appealstatuses.id1,
                    TestData.decisions.constraint1,
                    TestData.decisions.legalnoticedate1,
                    TestData.decisions.areasize1,
                    TestData.decisions.areaunits1,
                    TestData.decisions.dectype1,
                    TestData.decisions.decdate1);
        } catch (IOException ex) {
            Logger.getLogger(DecisionAppealRuleTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @AfterClass
    public static void  cleanupDependencies()
    {
        DecisionClient decisionclient = null;
        try {
            decisionclient = new DecisionClient();
        } catch (Exception ex) {
            Assert.fail();
        }

        // Clean up the Project
        try {
            decisionclient.deleteDecision(TestData.decisions.id1);

            // Finally, clean up Decision dependencies
            DecisionTest.cleanupDependencies();
        } catch (IOException ex) {
            Logger.getLogger(DecisionAppealRuleTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }
}