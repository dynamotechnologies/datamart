/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateless;

import beanutil.DataMartException;
import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.ProjectView.*;


/**
 *
 * @author gauri
 */
@Remote
public interface ProjectViewManagerRemote {
     public void create(ProjectView resource) throws DataMartException;
     public ProjectView get(Integer viewId) throws DataMartException; 
     public ProjectViews getAllByProjectByUser(String projecttype, String projectid, String userId) throws DataMartException;
     public ProjectViews getAllByProject(String projecttype, String projectid) throws DataMartException;
     public void update(ProjectView resource) throws DataMartException;
     public void delete(Integer id) throws DataMartException;
     public Boolean ping();
}
