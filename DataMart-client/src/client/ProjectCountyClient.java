package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.projectlocation.*;


public class ProjectCountyClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public ProjectCountyClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.projectlocation", "ProjectLocation.xsd");
    }


    public Counties getProjectCounty(String projectid, String id)
            throws IOException
    {
        return get("/projects/nepa/" + projectid + "/locations/counties/" + id, Counties.class);
    }


    public void putProjectCounty(String projectid, String id)
            throws IOException
    {
        Counties county = new Counties();
        county.getCountyid().add(id);
        put(objectfactory.createCounties(county), "/projects/nepa/" + projectid + "/locations/counties/" + id);
    }


    public void deleteProjectCounty(String projectid, String id)
            throws IOException
    {
        delete("/projects/nepa/" + projectid + "/locations/counties/" + id);
    }
}