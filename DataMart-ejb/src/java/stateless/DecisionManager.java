package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.decision.*;

import beanutil.*;
import java.util.Iterator;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


@Stateless(mappedName="DecisionManagerRemote")
public class DecisionManager implements DecisionManagerRemote
{
    @PersistenceContext
    private EntityManager em;


    @Override
    public void create(Decision resource)
             throws DataMartException
    {
        if (resource.getDectype() == null) {
            throw new DataMartException(DataMartException.MISSING_ELEMENT,
                "Decision with id '"+resource.getId()+"' missing dectype.");
        }
        if (resource.getDecdate() == null) {
            throw new DataMartException(DataMartException.MISSING_ELEMENT,
                "Decision with id '"+resource.getId()+"' missing decdate.");
        }
        entity.Decisions entity = createEntity(resource);

        // Check for existing entity with this ID
        entity.Decisions badentity = getEntity(entity.getId());
        if (badentity != null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS,
                    "Decision with id '"+entity.getId()+"' already exists.");

        em.persist(entity);
    }


    @Override
    public Decision get(Integer id)
            throws DataMartException
    {
        entity.Decisions entity = getEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Decision with id '"+id+"' does not exist.");

        return createResource(entity);
    }


    @Override
    public Decisions getAllByProject(String projecttype, String projectid)
            throws DataMartException
    {
        entity.Projects project = DataMartBeanUtil.getProject(em, projecttype, projectid);
        if (project == null) {
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                "Project not found of type=" + projecttype + " and id=" + projectid);
        }
        // Retrieve all entities from the database
        Query query = em.createQuery("SELECT d FROM Decisions d WHERE d.projects = :project");
        query.setParameter("project", project);
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Decisions responselist = new Decisions();
        while (it.hasNext()) {
            responselist.getDecision().add(createResource( (entity.Decisions)it.next() ));
        }
        return responselist;
    }


    @Override
    public Decisions getByUnit(String unitid, Boolean sopaonly)
            throws DataMartException
    {
        // Confirm that this is a valid unit
        entity.RefUnits unit = (entity.RefUnits) DataMartBeanUtil.getEntityById(em,
                "RefUnits", unitid, "Unit");
        if (unit==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);


        /*
         *
         * TODO: make this less sucky. The complexity of this query alone might be justification to
         * combine project-forests and project-districts into a single table.
         * 
         */

        /*
         * The goal here is to get a list of all decisions associated with a project that is associated with the specified unit.
         * The unit association may be in either the project-forests or project-districts locations tables.
         * In the future it may also be necessary to check the regions table.
         */
        String sopaclause = (sopaonly) ? "p.sopaPublish = true and " : "";
        Query query1 = em.createQuery("SELECT d FROM Decisions d JOIN d.projects p JOIN p.projectForestsCollection f JOIN f.refUnits u WHERE " + sopaclause + "u.id LIKE :id");
        Query query2 = em.createQuery("SELECT d FROM Decisions d JOIN d.projects p JOIN p.projectDistrictsCollection f JOIN f.refUnits u WHERE " + sopaclause + "u.id LIKE :id");
        query1.setParameter("id", unitid+"%");
        query2.setParameter("id", unitid+"%");
        List entitylist1 =  query1.getResultList();
        List entitylist2 =  query2.getResultList();

        if (!(entitylist1.size()>0 || entitylist2.size()>0))
                Logger.getLogger("No decisions found for unit id: " + unitid);
            //throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND); //Due to throwing lot of exception; have one line.

        // Loop over the database collection. Each row becomes a new object in the response list
        Decisions responselist = new Decisions();
        for (Object e : entitylist1) {
            Decision d = DecisionManager.createResource((entity.Decisions)e);
            responselist.getDecision().add(d);
        }
        for (Object e : entitylist2) {
            Decision d = DecisionManager.createResource((entity.Decisions)e);
            // Remove duplicates from the two queries
            Boolean duped = false;
            for (Object dupe : entitylist1) {
                if ( ((entity.Decisions)dupe).getId() == ((entity.Decisions)e).getId() )
                    duped = true;
            }
            if (!duped) {
                responselist.getDecision().add(d);
            }
        }

        // Get nationwide and regionwide entities for the live sopa
        if (sopaonly) {
            Query query3 = em.createQuery("SELECT d FROM Decisions d JOIN d.projects p JOIN p.projectDistrictsCollection f JOIN f.refUnits u WHERE p.sopaPublish = true and " +
                "( u.id LIKE '11000000' OR u.id LIKE :regionwide )");
            String regionwide = unitid.substring(0, 4)+"0000";
            query3.setParameter("regionwide", regionwide);
            List entitylist3 = query3.getResultList();
            for (Object e : entitylist3) {
                // Remove duplicates from the two queries
                Boolean duped = false;
                for (Object dupe : entitylist1) {
                    if ( ((entity.Decisions)dupe).getId() == ((entity.Decisions)e).getId() )
                        duped = true;
                }
                for (Object dupe : entitylist2) {
                    if ( ((entity.Decisions)dupe).getId() == ((entity.Decisions)e).getId() )
                        duped = true;
                }
                if (!duped) {
                    responselist.getDecision().add(createResource((entity.Decisions)e));
                }
            }
        }

        return responselist;
    }


    @Override
    public void update(Decision resource)
             throws DataMartException
    {
        if (resource.getDectype() == null) {
            throw new DataMartException(DataMartException.MISSING_ELEMENT,
                "Decision with id '"+resource.getId()+"' missing dectype.");
        }
        if (resource.getDecdate() == null) {
            throw new DataMartException(DataMartException.MISSING_ELEMENT,
                "Decision with id '"+resource.getId()+"' missing decdate.");
        }
        entity.Decisions entity = createEntity(resource);

        // Query for the (hopefully) existing entity. Complain if not found.
        if (getEntity(entity.getId())==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND, "Nonexistent Decision cannot be updated.");

        // Update the entity
        entity = em.merge(entity);
    }

    @Override
    public void delete(Integer id)
             throws DataMartException
    {
        // Verify that this entity exists
        entity.Decisions entity = getEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Could not delete nonexistent Decision with id '"+id+"'.");

        em.remove(entity);
    }

    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     *
     * This method does the mapping from entity object to jaxb object
     */
    protected static Decision createResource(entity.Decisions entity)
            throws DataMartException
    {
        Decision resource = new Decision();
        resource.setId(entity.getId());
        resource.setProjectid(entity.getProjects().getPublicId());
        resource.setProjecttype(entity.getProjects().getProjectTypes().getId());
        resource.setName(entity.getName());
        resource.setAppealstatus(entity.getAppealStatus());
        resource.setConstraint(entity.getDecConstraint());
        if (entity.getLegalNoticeDate()!=null)
            resource.setLegalnoticedate(DataMartBeanUtil.getXMLGregorianFromDateTime(entity.getLegalNoticeDate()));
        if (entity.getAreaSize() != null)
            resource.setAreasize(entity.getAreaSize());
        if (entity.getAreaUnits() != null)
            resource.setAreaunits(entity.getAreaUnits());
        if (entity.getDecisionDate()!=null)
            resource.setDecdate(DataMartBeanUtil.getXMLGregorianFromDateTime(entity.getDecisionDate()));
        if (entity.getRefDecisionTypes()!=null)
            resource.setDectype(entity.getRefDecisionTypes().getId());
        return resource;
    }

    /*
     *
     * This method does the mapping from jaxb object to entity object
     */
    private entity.Decisions createEntity(Decision resource)
            throws DataMartException
    {
        String pid = resource.getProjectid();
        String ptype = resource.getProjecttype();
        entity.Decisions entity = new entity.Decisions();
        entity.setId(resource.getId());
        entity.Projects project = DataMartBeanUtil.getProject(em, ptype, pid);
        if (project==null)
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION, "Project with type '" +
                    ptype + "' and id '" + pid + "' does not exist.");
        entity.setProjects(project);
        entity.setName(resource.getName());
        entity.setAppealStatus(resource.getAppealstatus());
        entity.setDecConstraint(resource.getConstraint());
        if (resource.getLegalnoticedate()!=null)
            entity.setLegalNoticeDate(DataMartBeanUtil.getDateFromXMLGregorian(resource.getLegalnoticedate()));
        if (resource.getAreasize() != null)
            entity.setAreaSize(resource.getAreasize());
        if (resource.getAreaunits() != null)
            entity.setAreaUnits(resource.getAreaunits());
        if (resource.getDecdate()!=null)
            entity.setDecisionDate(DataMartBeanUtil.getDateFromXMLGregorian(resource.getDecdate()));
        if (resource.getDectype() != null) {
            entity.RefDecisionTypes dectype = (entity.RefDecisionTypes)DataMartBeanUtil.getEntityById(em, "RefDecisionTypes", resource.getDectype(), "DecisionType");
            entity.setRefDecisionTypes(dectype);
        }

        return entity;
    }


    // Accepts ID parameter, and returns the associated entity,
    //   or null if it doesn't exist
    private entity.Decisions getEntity(Integer id)
            throws DataMartException
    {
        return em.find(entity.Decisions.class, id);
    }
}