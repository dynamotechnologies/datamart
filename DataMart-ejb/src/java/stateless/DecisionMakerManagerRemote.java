package stateless;

import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.decisionmaker.*;
import beanutil.DataMartException;

@Remote
public interface DecisionMakerManagerRemote {
/*
 *
 * The Manager beans are responsible for:
 *   1) Implementing business logic and
 *   2) Translating front-end resources to back-end database entities.
 *
 * Often the resources will consist of data from numerous database tables. It is the manager's responsibility
 *   to ensure that these front-end resources can be accessed as though they were cut from a single piece of wood.
 *
 */

    public void create(Decisionmaker resource) throws DataMartException;
    public void update(Decisionmaker resource) throws DataMartException;
    public Decisionmaker get(int decisionid, int id) throws DataMartException;
    public Decisionmakers getList(int decisionid) throws DataMartException;
    public void delete(int decisionid, int id) throws DataMartException;
    public Boolean ping();
}
