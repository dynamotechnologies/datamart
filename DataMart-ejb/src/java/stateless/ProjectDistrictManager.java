package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.projectlocation.*;

import beanutil.*;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless(mappedName="ProjectDistrictManagerRemote")
public class ProjectDistrictManager implements ProjectDistrictManagerRemote
{

    @PersistenceContext
    private EntityManager em;

    private String PROJECT_TYPE = "nepa";

    @Override
    public void merge(String projectid, String id)
             throws DataMartException
    {
        if (id.length()!=8)
            throw new DataMartException(DataMartException.BAD_FORMAT,
                    "Unit '" + id + "' is not a district. District codes must be eight digits.");
        entity.ProjectDistricts district = createEntity(projectid, id);

        // Check for existing entity and grab the internal ID if it exists
        entity.ProjectDistricts olddistrict = getEntity(projectid, id);
        if (olddistrict != null)
            district.setId(olddistrict.getId());
        
        em.merge(district);
    }


    @Override
    public Districts get(String projectid, String id)
            throws DataMartException
    {
        Districts rval = new Districts();
        entity.ProjectDistricts district = getEntity(projectid, id);
        if (district==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "NEPA Project " + projectid + " is not associated with district '" + id + "'");

        rval.getDistrictid().add(district.getRefUnits().getId());
        return rval;
    }


    @Override
    public void delete(String projectid, String id)
            throws DataMartException
    {
        entity.ProjectDistricts district = getEntity(projectid, id);
        if (district==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "NEPA Project " + projectid + " is not associated with district '" + id + "'");

        em.remove(district);
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    private entity.ProjectDistricts createEntity(String projectid, String id)
            throws DataMartException
    {
        // Get a reference to the project
        entity.Projects project = DataMartBeanUtil.getProject(em, this.PROJECT_TYPE, projectid);
        if (project==null)
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION,
                    "Project with type '" + this.PROJECT_TYPE + "' and ID '" + projectid + "' does not exist.");

        // Get the reference unit associated with this id
        entity.RefUnits ref = (entity.RefUnits) DataMartBeanUtil.getEntityById(em,
                "RefUnits", id, "Unit Code");

        entity.ProjectDistricts district = new entity.ProjectDistricts();
        district.setProjects(project);
        district.setRefUnits(ref);
        return district;
    }


    private entity.ProjectDistricts getEntity(String projectid, String id)
            throws DataMartException
    {
        // Get a reference to the project
        entity.Projects project = DataMartBeanUtil.getProject(em, this.PROJECT_TYPE, projectid);
        if (project==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Project with type '" + this.PROJECT_TYPE + "' and ID '" + projectid + "' does not exist.");

        // Get the reference unit associated with this id
        entity.RefUnits ref = (entity.RefUnits) DataMartBeanUtil.getEntityById(em,
                "RefUnits", id, "Unit Code");
        if (ref==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Reference Unit with id '" + id + "' does not exist.");

        // Query for the relationship table row
        Query q = em.createQuery("SELECT p FROM ProjectDistricts p WHERE p.projects = :project AND p.refUnits = :ref");
        q.setParameter("project", project);
        q.setParameter("ref", ref);
        entity.ProjectDistricts district = (entity.ProjectDistricts) DataMartBeanUtil.getEntityByQueryAllowNull(q,
                "NEPA Project " + projectid + " has multiple district associations with id '" + id + "'");

        return district;
    }
}