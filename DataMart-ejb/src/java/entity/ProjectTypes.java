package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "ProjectTypes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProjectTypes.findAll", query = "SELECT p FROM ProjectTypes p"),
    @NamedQuery(name = "ProjectTypes.findById", query = "SELECT p FROM ProjectTypes p WHERE p.id = :id"),
    @NamedQuery(name = "ProjectTypes.findByType", query = "SELECT p FROM ProjectTypes p WHERE p.type = :type")})
public class ProjectTypes implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private String id;
    @Basic(optional = false)
    @Column(name = "Type")
    private String type;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projectTypes")
    private Collection<Projects> projectsCollection;

    public ProjectTypes() {
    }

    public ProjectTypes(String id) {
        this.id = id;
    }

    public ProjectTypes(String id, String type) {
        this.id = id;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @XmlTransient
    public Collection<Projects> getProjectsCollection() {
        return projectsCollection;
    }

    public void setProjectsCollection(Collection<Projects> projectsCollection) {
        this.projectsCollection = projectsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProjectTypes)) {
            return false;
        }
        ProjectTypes other = (ProjectTypes) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ProjectTypes[ id=" + id + " ]";
    }

}
