package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.CARAPhaseDocManagerRemote;
import stateless.DocumentContainerManagerRemote;
import us.fed.fs.www.nepa.schema.documentcontainer.Containerdocs;
import us.fed.fs.www.nepa.schema.documentcontainer.Containers;
import us.fed.fs.www.nepa.schema.documentcontainer.ObjectFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;


@Path ("/projects/{type}/{projectid}/containers")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class DocumentContainerResource extends AbstractResource {
    private CARAPhaseDocManagerRemote caraphasedocbeanmanager;
    private DocumentContainerManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public DocumentContainerResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "DocumentContainer.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.documentcontainer");
        MY_PROPS.setProperty("bean", "DocumentContainerManagerRemote");
        try {
            beanmanager = (DocumentContainerManagerRemote) init(DocumentContainerManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("DocumentContainerResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("DocumentContainerResource failed to start. Could not ping the bean manager!");
        
        UTIL = DataMartUtil.getInstance();
        if (UTIL==null)
            throw new ServletException("Horror! Could not get a reference to the Datamart Utility singleton! Exiting in shame.");

        Properties CARAPHASEDOC_PROPS = new Properties();
        CARAPHASEDOC_PROPS.setProperty("schema", "CaraDocument.xsd");
        CARAPHASEDOC_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.caradocument");
        CARAPHASEDOC_PROPS.setProperty("bean", "CARAPhaseDocManagerRemote");
        
       
        try {
            int CARAPHASEDOC_ID = UTIL.getResourceId(CARAPHASEDOC_PROPS);
            caraphasedocbeanmanager = (CARAPhaseDocManagerRemote)UTIL.getManager(CARAPHASEDOC_ID);
        } catch (IOException ex) {
            log.error(ex.toString());
        }
    }


    @POST
    public Response post(@PathParam("type") String type, @PathParam("projectid") String projectid, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;
        try {
            Containers containerTree = (Containers)unmarshal(inputxml, Containers.class);
            uri = new URI(uriInfo.getAbsolutePath().toString());

            // All clear. Now do the insert.
            beanmanager.update(type, projectid, containerTree);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        // Return a link to the newly created entity
        return created(uri);
    }


    @GET
    public Response get(@PathParam("type") String type, 
                        @PathParam("projectid") String projectid, 
                        @QueryParam("pubflag") Integer pubflagval, 
                        @QueryParam("ignorecaraflag") Integer ignorecaraflagval,
                        @QueryParam("sortdesc") String sortdesc,
                        @QueryParam("viewId") String viewId,
                        @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            Boolean pubflag = null;
            if (pubflagval != null) {
                if ((pubflagval == 0) || (pubflagval == 1)) {
                    pubflag = (pubflagval == 1);
                } else {
                  complainLoudly(new Exception("Bad pubflag value"), Response.Status.BAD_REQUEST);
                }
            }
            Boolean ignorecaraflag = null;
            if (ignorecaraflagval != null) {
                if ((ignorecaraflagval == 0) || (ignorecaraflagval == 1)) {
                    ignorecaraflag = (ignorecaraflagval == 1);
                } else {
                  complainLoudly(new Exception("Bad ignorecaraflag value"), Response.Status.BAD_REQUEST);
                }
            }
            Containers container = beanmanager.get(type, projectid, pubflag, ignorecaraflag,Boolean.valueOf(false),viewId);
            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Containers> jaxbelement = objectfactory.createContainers(container);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }

    @PUT
    public Response put(@PathParam("type") String type, @PathParam("projectid") String projectid, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            Containers containers = (Containers) unmarshal(inputxml, Containers.class);

            // All clear. Now do the update.
            beanmanager.update(type, projectid, containers);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"</location>");
    }


    @DELETE
    public Response delete(@PathParam("type") String type, @PathParam("projectid") String projectid, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(type, projectid);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }

    @GET
    @Path("/containerdocs")
    public Response getContainerDocs(@PathParam("type") String type, @PathParam("projectid") String projectid, @QueryParam("contid") Integer contid, @QueryParam("start") Integer start, @QueryParam("rows") Integer rows, @QueryParam("phaseid") Integer phaseid, @QueryParam("viewId") Integer viewId, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            Containerdocs containerdocs = beanmanager.getContainerDocsFromContainer(type, projectid, contid, start, rows,phaseid,viewId);
            /*Containerdocs doclist = new Containerdocs();
            if(phaseid !=null && null != containerdocs)
            {
             List caraphaseDocs= caraphasedocbeanmanager.getAll(phaseid);
             if(null !=containerdocs.getContainerdoc() ){
             Iterator it = containerdocs.getContainerdoc().iterator();
             while(it.hasNext())
             {
              Containerdoc doc =(Containerdoc) it.next();
              //System.out.println(doc.getDocid());
              if(caraphaseDocs.contains(doc.getDocid()))
               doclist.getContainerdoc().add(doc);
             }
             containerdocs=doclist;
             }
            }*/
            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Containerdocs> jaxbelement = objectfactory.createContainerdocs(containerdocs);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }

    @POST
    @Path("/updatecontainerdocs")
    public Response updateContainerDocs(
        @PathParam("type") String type,
        @PathParam("projectid") String projectid,
        @QueryParam("externalflag") Integer externalFlag,
        @QueryParam("forceparent") boolean forceParentUpdate,
        InputStream inputxml,
        @Context UriInfo uriInfo
    )
            throws WebApplicationException
    {
        try {
            Containerdocs doclist = (Containerdocs) unmarshal(inputxml, Containerdocs.class);
            boolean isOnlyAppliedToFixed = false;
            if (externalFlag != null){
                if (externalFlag.compareTo(1) == 0){
                    isOnlyAppliedToFixed = true;
                }
            }
            // All clear. Now do the update.
            beanmanager.updateContainerDocs(type, projectid, doclist, isOnlyAppliedToFixed, forceParentUpdate);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        StringWriter sw = new StringWriter();
        return ok(sw);
    }

    @GET
    @Path("/updatephaseids")
    public Response updatephaseids(
        @PathParam("type") String type,
        @PathParam("projectid") String projectid,
        @Context UriInfo uriInfo
    )
            throws WebApplicationException
    {
        try {
          
            // All clear. Now do the update.
            beanmanager.updatePhaseIds();
            } catch (DataMartException ex) {
            handleException(ex);
        }
        StringWriter sw = new StringWriter();
        return ok(sw);
    }
    
    @GET
    @Path("/template")
    public Response getContainerTemplate(@PathParam("type") String type, @PathParam("projectid") String projectid, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            Containers container = beanmanager.getContainerTemplate(type, projectid);
            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Containers> jaxbelement = objectfactory.createContainers(container);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }
    @GET
    @Path("/downloader/template")
    public Response getDownloaderContainerTemplate(@PathParam("type") String type, @PathParam("projectid") String projectid,@QueryParam("viewid") String viewId, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
           
            Containers container = beanmanager.getDownloaderContainerTemplate(type, projectid,viewId);
            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Containers> jaxbelement = objectfactory.createContainers(container);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }
    @PUT
    @Path("/template")
    public Response updateContainerTemplate(@PathParam("type") String type, @PathParam("projectid") String projectid, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            Containers containers = (Containers) unmarshal(inputxml, Containers.class);

            // All clear. Now do the update.
            beanmanager.updateContainerTemplate(type, projectid, containers);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"</location>");
    }
}
