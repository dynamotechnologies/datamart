package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "ProjectDocuments")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProjectDocuments.findAll", query = "SELECT p FROM ProjectDocuments p"),
    @NamedQuery(name = "ProjectDocuments.findById", query = "SELECT p FROM ProjectDocuments p WHERE p.id = :id"),
    @NamedQuery(name = "ProjectDocuments.findByDocumentId", query = "SELECT p FROM ProjectDocuments p WHERE p.documentId = :documentId"),
    @NamedQuery(name = "ProjectDocuments.findByLabel", query = "SELECT p FROM ProjectDocuments p WHERE p.label = :label"),
    @NamedQuery(name = "ProjectDocuments.findByPdfSize", query = "SELECT p FROM ProjectDocuments p WHERE p.pdfSize = :pdfSize"),
    @NamedQuery(name = "ProjectDocuments.findByIsPublished", query = "SELECT p FROM ProjectDocuments p WHERE p.isPublished = :isPublished"),
    @NamedQuery(name = "ProjectDocuments.findByDescription", query = "SELECT p FROM ProjectDocuments p WHERE p.description = :description"),
    @NamedQuery(name = "ProjectDocuments.findByLink", query = "SELECT p FROM ProjectDocuments p WHERE p.link = :link"),
    @NamedQuery(name = "ProjectDocuments.findByOrderTag", query = "SELECT p FROM ProjectDocuments p WHERE p.orderTag = :orderTag"),
    @NamedQuery(name = "ProjectDocuments.findByIsSensitive", query = "SELECT p FROM ProjectDocuments p WHERE p.isSensitive = :isSensitive"),
    @NamedQuery(name = "ProjectDocuments.findByPhaseId", query = "SELECT p FROM ProjectDocuments p WHERE p.phaseId = :phaseId")})
public class ProjectDocuments implements Serializable {

    @OneToMany(mappedBy = "docId")
    private Collection<ProjectViewDocs> projectViewDocsCollection;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "documentId")
    private Collection<ProjectImplInfoDocs> projectImplInfoDocsCollection;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "documentId")
    private Collection<ProjectMeetingNoticeDocs> projectMeetingNoticeDocsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "documentId")
    private Collection<ProjectAddInfoDocs> projectAddInfoDocsCollection;
    @Column(name = "DatePublished")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datePublished;
    
    @Column(name = "DateAdded")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateAdded;  
    @Column(name = "AddedBy")
    private String addedBy;
    
    @Column(name = "DocumentDate")
    private String documentDate;
    @Column(name = "Author")
    private String author;
    @Column(name = "Addressee")
    private String addressee;
    @Column(name = "DateLabel")
    private String dateLabel;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "DocumentId")
    private String documentId;
    @Basic(optional = false)
    @Column(name = "Label")
    private String label;
    @Basic(optional = false)
    @Column(name = "PdfSize")
    private String pdfSize;
    @Basic(optional = false)
    @Column(name = "isPublished")
    private boolean isPublished;
    @Column(name = "Description")
    private String description;
    @Column(name = "Link")
    private String link;
    @Column(name = "OrderTag")
    private Integer orderTag;
    @Basic(optional = false)
    @Column(name = "isSensitive")
    private boolean isSensitive;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projectDocuments")
    private Collection<CARAPhaseDocs> cARAPhaseDocsCollection;
    @OneToMany(mappedBy = "projectDocuments")
    private Collection<Appeals> appealsCollection;
    @OneToMany(mappedBy = "projectDocuments")
    private Collection<Objections> objectionsCollection;
    @JoinColumn(name = "ProjectId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Projects projects;
    @JoinColumn(name = "SensitivityRationale", referencedColumnName = "Id")
    @ManyToOne(fetch = FetchType.EAGER)
    private RefDocumentSensitivityRationales sensitivityRationale;
    @JoinColumn(name = "ContainerId", referencedColumnName = "Id")
    @ManyToOne
    private ProjectDocumentContainers projectDocumentContainers;
    //private String docorder;
    //private String containerorder;
    //private String containerid;
    @Column(name = "PhaseId")
    private Integer phaseId;

    public ProjectDocuments() {
    }

    public ProjectDocuments(Integer id) {
        this.id = id;
    }

    public ProjectDocuments(Integer id, String documentId, String label, String pdfSize, boolean isPublished, boolean isSensitive) {
        this.id = id;
        this.documentId = documentId;
        this.label = label;
        this.pdfSize = pdfSize;
        this.isPublished = isPublished;
        this.isSensitive = isSensitive;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getPdfSize() {
        return pdfSize;
    }

    public void setPdfSize(String pdfSize) {
        this.pdfSize = pdfSize;
    }

    public boolean getIsPublished() {
        return isPublished;
    }

    public void setIsPublished(boolean isPublished) {
        this.isPublished = isPublished;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Integer getOrderTag() {
        return orderTag;
    }

    public void setOrderTag(Integer orderTag) {
        this.orderTag = orderTag;
    }

    public boolean getIsSensitive() {
        return isSensitive;
    }

    public void setIsSensitive(boolean isSensitive) {
        this.isSensitive = isSensitive;
    }
    
    public String getDocumentDate() {
        return documentDate;
    }

    public void setDocumentDate(String documentDate) {
        this.documentDate = documentDate;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAddressee() {
        return addressee;
    }

    public void setAddressee(String addressee) {
        this.addressee = addressee;
    }

    public String getDateLabel() {
        return dateLabel;
    }

    public void setDateLabel(String dateLabel) {
        this.dateLabel = dateLabel;
    }
    public Integer getPhaseId() {
        return phaseId;
    }

    public void setPhaseId(Integer phaseId) {
        this.phaseId = phaseId;
    }
    @XmlTransient
    public Collection<CARAPhaseDocs> getCARAPhaseDocsCollection() {
        return cARAPhaseDocsCollection;
    }

    public void setCARAPhaseDocsCollection(Collection<CARAPhaseDocs> cARAPhaseDocsCollection) {
        this.cARAPhaseDocsCollection = cARAPhaseDocsCollection;
    }

    @XmlTransient
    public Collection<Appeals> getAppealsCollection() {
        return appealsCollection;
    }

    public void setAppealsCollection(Collection<Appeals> appealsCollection) {
        this.appealsCollection = appealsCollection;
    }

    @XmlTransient
    public Collection<Objections> getObjectionsCollection() {
        return objectionsCollection;
    }

    public void setObjectionsCollection(Collection<Objections> objectionsCollection) {
        this.objectionsCollection = objectionsCollection;
    }

    public Projects getProjects() {
        return projects;
    }

    public void setProjects(Projects projects) {
        this.projects = projects;
    }

    public ProjectDocumentContainers getProjectDocumentContainers() {
        return projectDocumentContainers;
    }

    public void setProjectDocumentContainers(ProjectDocumentContainers projectDocumentContainers) {
        this.projectDocumentContainers = projectDocumentContainers;
    }
    
    public RefDocumentSensitivityRationales getSensitivityRationale() {
        return sensitivityRationale;
    }

    public void setSensitivityRationale(RefDocumentSensitivityRationales sensitivityRationale) {
        this.sensitivityRationale = sensitivityRationale;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProjectDocuments)) {
            return false;
        }
        ProjectDocuments other = (ProjectDocuments) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ProjectDocuments[ id=" + id + " ]";
    }

    public Date getDatePublished() {
        return datePublished;
    }

    public void setDatePublished(Date datePublished) {
        this.datePublished = datePublished;
    }
    
    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    public String getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(String addedBy) {
        this.addedBy = addedBy;
    }

    @XmlTransient
    public Collection<ProjectMeetingNoticeDocs> getProjectMeetingNoticeDocsCollection() {
        return projectMeetingNoticeDocsCollection;
    }

    public void setProjectMeetingNoticeDocsCollection(Collection<ProjectMeetingNoticeDocs> projectMeetingNoticeDocsCollection) {
        this.projectMeetingNoticeDocsCollection = projectMeetingNoticeDocsCollection;
    }

    @XmlTransient
    public Collection<ProjectAddInfoDocs> getProjectAddInfoDocsCollection() {
        return projectAddInfoDocsCollection;
    }

    public void setProjectAddInfoDocsCollection(Collection<ProjectAddInfoDocs> projectAddInfoDocsCollection) {
        this.projectAddInfoDocsCollection = projectAddInfoDocsCollection;
    }

    @XmlTransient
    public Collection<ProjectImplInfoDocs> getProjectImplInfoDocsCollection() {
        return projectImplInfoDocsCollection;
    }

    public void setProjectImplInfoDocsCollection(Collection<ProjectImplInfoDocs> projectImplInfoDocsCollection) {
        this.projectImplInfoDocsCollection = projectImplInfoDocsCollection;
    }

    @XmlTransient
    public Collection<ProjectViewDocs> getProjectViewDocsCollection() {
        return projectViewDocsCollection;
    }

    public void setProjectViewDocsCollection(Collection<ProjectViewDocs> projectViewDocsCollection) {
        this.projectViewDocsCollection = projectViewDocsCollection;
    }

}
