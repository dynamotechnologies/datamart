package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.projecttype.*;

import beanutil.*;
import java.util.Iterator;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless(mappedName="ProjectTypeManagerRemote")
public class ProjectTypeManager implements ProjectTypeManagerRemote
{

    @PersistenceContext
    private EntityManager em;


    @Override
    public void create(Projecttype resource)
             throws DataMartException
    {
        validate(resource);
        entity.ProjectTypes entity = createEntity(resource);

        entity.ProjectTypes badentity = createEntity(entity.getId());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        entity = em.merge(entity);
    }

    @Override
    public Projecttypes getAll()
             throws DataMartException
    {
        // Retrieve all entities from the database
        Query query = em.createQuery("SELECT u FROM ProjectTypes u");
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Projecttypes responselist = new Projecttypes();
        while (it.hasNext()) {
            responselist.getProjecttype().add(createResource( (entity.ProjectTypes)it.next() ));
        }
        return responselist;
    }

    @Override
    public String getName(String id)
            throws DataMartException
    {
        entity.ProjectTypes entity = createEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return entity.getType();
    }

    @Override
    public void update(Projecttype resource)
             throws DataMartException
    {
        validate(resource);
        entity.ProjectTypes entity = createEntity(resource);

        if (createEntity(entity.getId())==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        entity = em.merge(entity);
    }

    @Override
    public void delete(String id)
             throws DataMartException
    {
        entity.ProjectTypes entity = createEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     *
     * This method does the mapping from jaxb object to entity object and vice versa
     */
    private Projecttype createResource(entity.ProjectTypes entity)
            throws DataMartException
    {
        Projecttype resource = new Projecttype();
        resource.setId(entity.getId());
        resource.setName(entity.getType());
        return resource;
    }

    private entity.ProjectTypes createEntity(Projecttype resource)
            throws DataMartException
    {
        entity.ProjectTypes entity = new entity.ProjectTypes();
        entity.setId(resource.getId());
        entity.setType(resource.getName());
        return entity;
    }

    // This version of overloaded method createEntity() accepts a String ID as a parameter, and returns the
    //   associated entity, or null if it doesn't exist
    //
    private entity.ProjectTypes createEntity(String id)
            throws DataMartException
    {
        return em.find(entity.ProjectTypes.class, id);
    }

    private void validate(Projecttype resource)
            throws DataMartException
    {
        DataMartBeanUtil.confirmNotNull(resource.getName(), "Name");
    }
}