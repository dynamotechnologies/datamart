/*
 * ProjectDocumentFilter.java
 * Originally this was called PubWebDocFilter, but changed to reflect its role in
 * applying various filters to ProjectDocumetns in general.
 * 8/7/2015
 *
 */
package resource;

import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

/**
 *
 * @author Sam Collins
 */
public class ProjectDocumentFilter implements Filter {

    private static final boolean debug = false;
    // The filter configuration object we are associated with.  If
    // this value is null, this filter instance is not currently
    // configured.
    private FilterConfig filterConfig = null;

    public ProjectDocumentFilter() {
    }

    private void doBeforeProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {
        if (debug) {
            log("PublicWebDocFilter:DoBeforeProcessing");
        }
    }

    private void doAfterProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {
        if (debug) {
            log("PublicWebDocFilter:DoAfterProcessing");
        }
    }

    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {

        if (debug) {
            log("PublicWebDocFilter:doFilter()");
        }

        doBeforeProcessing(request, response);

        Throwable problem = null;
        try {
            FilteredRequest filteredRequest = new FilteredRequest(request);

            String method = filteredRequest.getMethod();
            String pathInfo = filteredRequest.getPathInfo();
            String remoteUser = filteredRequest.getRemoteUser();

            // Fix previously in place to prevent errant template updates from the PALS system
            //if (pathInfo.indexOf("template") >0){
             //   if (remoteUser !=null && remoteUser.equalsIgnoreCase("pals")){
              //      if (method.equalsIgnoreCase("PUT") || method.equalsIgnoreCase("POST")){
                 //       throw new javax.servlet.ServletException(method + " path unavailable: /template");
                  //  }
               // }
            //}


            chain.doFilter(filteredRequest, response);
        } catch (Throwable t) {
            // If an exception is thrown somewhere down the filter chain,
            // we still want to execute our after processing, and then
            // rethrow the problem after that.
            problem = t;
            t.printStackTrace();
        }

        doAfterProcessing(request, response);

        // If there was a problem, we want to rethrow it if it is
        // a known type, otherwise log it.
        if (problem != null) {
            if (problem instanceof ServletException) {
                throw (ServletException) problem;
            }
            if (problem instanceof IOException) {
                throw (IOException) problem;
            }
            sendProcessingError(problem, response);
        }
    }

    /**
     * Return the filter configuration object for this filter.
     */
    public FilterConfig getFilterConfig() {
        return (this.filterConfig);
    }

    /**
     * Set the filter configuration object for this filter.
     *
     * @param filterConfig The filter configuration object
     */
    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    /**
     * Destroy method for this filter
     */
    public void destroy() {
    }

    /**
     * Init method for this filter
     */
    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
        if (filterConfig != null) {
            if (debug) {
                log("PublicWebDocFilter:Initializing filter");
            }
        }
    }

    /**
     * Return a String representation of this object.
     */
    @Override
    public String toString() {
        if (filterConfig == null) {
            return ("PublicWebDocFilter()");
        }
        StringBuffer sb = new StringBuffer("PublicWebDocFilter(");
        sb.append(filterConfig);
        sb.append(")");
        return (sb.toString());
    }

    private void sendProcessingError(Throwable t, ServletResponse response) {
        String stackTrace = getStackTrace(t);

        if (stackTrace != null && !stackTrace.equals("")) {
            try {
                response.setContentType("text/html");
                PrintStream ps = new PrintStream(response.getOutputStream());
                PrintWriter pw = new PrintWriter(ps);
                pw.print("<html>\n<head>\n<title>Error</title>\n</head>\n<body>\n"); //NOI18N

                // PENDING! Localize this for next official release
                pw.print("<h1>The resource did not process correctly</h1>\n<pre>\n");
                pw.print(stackTrace);
                pw.print("</pre></body>\n</html>"); //NOI18N
                pw.close();
                ps.close();
                response.getOutputStream().close();
            } catch (Exception ex) {
            }
        } else {
            try {
                PrintStream ps = new PrintStream(response.getOutputStream());
                t.printStackTrace(ps);
                ps.close();
                response.getOutputStream().close();
            } catch (Exception ex) {
            }
        }
    }

    public static String getStackTrace(Throwable t) {
        String stackTrace = null;
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            t.printStackTrace(pw);
            pw.close();
            sw.close();
            stackTrace = sw.getBuffer().toString();
        } catch (Exception ex) {
        }
        return stackTrace;
    }

    public void log(String msg) {
        filterConfig.getServletContext().log(msg);
    }

    /**
     * Wrapper class to fix issue in requests from the Portal (user pubweb).
     * Requests for documents should always be limited to published, non-sensitive documents.
     *
     */
    static class FilteredRequest extends HttpServletRequestWrapper {

        private String queryString = null;

        public FilteredRequest(ServletRequest request) {
            super((HttpServletRequest)request);

            String method = super.getMethod();
            String pathInfo = super.getPathInfo();
            String remoteUser = super.getRemoteUser();

            if (remoteUser == null) {
              //TODO
              //no login, will need to eventually make sure that this ensure the request isn't
              //fulfilled; for now, just saying the queryString is the same
              queryString = super.getQueryString();
              return;
            }
            //The pubweb user should only be able to access non-sensitive, published documents
            if (remoteUser.equalsIgnoreCase("pubweb")
                    && pathInfo.indexOf("/docs") >0){
                queryString = "sensitiveflag=0&pubflag=1";
            }
            //The PRM user should only access non-CARA documents, as the large number of CARA documents overloads PRM
            else if (remoteUser.equalsIgnoreCase("prm")
                    && pathInfo.indexOf("/docs") > 0){
                queryString = "ignorecaraflag=1";
            }
            /*
            //The PALS user should only be allowed to update externally managed documents
            else if (remoteUser.equalsIgnoreCase("pals")
                    && pathInfo.indexOf("updatecontainerdocs")>0){
                queryString = "externalflag=1";
            }
            * */
            //
            else {
                queryString = super.getQueryString();
            }

        }

        @Override
        public String getQueryString(){
            return queryString;
        }

    }
}
