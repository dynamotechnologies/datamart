package stateless;

import javax.ejb.Stateless;
import beanutil.*;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import us.fed.fs.www.nepa.schema.unitkml.*;

@Stateless(mappedName="UnitKMLManagerRemote")
public class UnitKMLManager implements UnitKMLManagerRemote {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void create(Unitkml resource)
             throws DataMartException
    {
        validate(resource);
        entity.UnitKMLs entity = createEntity(resource);

        entity.UnitKMLs badentity = createEntity(entity.getId());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        entity = em.merge(entity);
    }

    @Override
    public Unitkml get(String key)
             throws DataMartException
    {
        entity.UnitKMLs entity = createEntity(key);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return createResource(entity);
    }

    @Override
    public void update(Unitkml resource)
             throws DataMartException
    {
        validate(resource);
        entity.UnitKMLs entity = createEntity(resource);

        if (createEntity(entity.getId())==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        entity = em.merge(entity);
    }

    @Override
    public void delete(String key)
             throws DataMartException
    {
        entity.UnitKMLs entity = createEntity(key);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     *
     * This method does the mapping from jaxb object to entity object and vice versa
     */
    private Unitkml createResource(entity.UnitKMLs entity)
            throws DataMartException
    {
        Unitkml resource = new Unitkml();
        resource.setFilename(entity.getId());
        resource.setUnitid(entity.getRefUnits().getId());
        resource.setLabel(entity.getLabel());
        resource.setMaptype(entity.getMapType());
        return resource;
    }

    private entity.UnitKMLs createEntity(Unitkml resource)
            throws DataMartException
    {
        entity.UnitKMLs entity = new entity.UnitKMLs();
        entity.setId(resource.getFilename());
        entity.setRefUnits((entity.RefUnits)DataMartBeanUtil.getEntityById(em, "RefUnits", resource.getUnitid(), "Unit"));
        entity.setLabel(resource.getLabel());
        entity.setMapType(resource.getMaptype());
        return entity;
    }

    private entity.UnitKMLs createEntity(String key)
            throws DataMartException
    {
        return em.find(entity.UnitKMLs.class, key);
    }

    private void validate(Unitkml resource)
            throws DataMartException
    {
        // Ensure these are not empty strings
        DataMartBeanUtil.confirmNotNull(resource.getFilename(), "filename");
        DataMartBeanUtil.confirmNotNull(resource.getUnitid(), "unittid");
    }

}