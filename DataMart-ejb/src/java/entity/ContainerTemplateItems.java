/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Kevin Sours
 */
@Entity
@Table(name = "ContainerTemplateItems")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContainerTemplateItems.findAll", query = "SELECT c FROM ContainerTemplateItems c"),
    @NamedQuery(name = "ContainerTemplateItems.findById", query = "SELECT c FROM ContainerTemplateItems c WHERE c.id = :id"),
    @NamedQuery(name = "ContainerTemplateItems.findByLabel", query = "SELECT c FROM ContainerTemplateItems c WHERE c.label = :label"),
    @NamedQuery(name = "ContainerTemplateItems.findByOrderTag", query = "SELECT c FROM ContainerTemplateItems c WHERE c.orderTag = :orderTag"),
    @NamedQuery(name = "ContainerTemplateItems.findByFixedFlag", query = "SELECT c FROM ContainerTemplateItems c WHERE c.fixedFlag = :fixedFlag")})
public class ContainerTemplateItems implements Serializable {
    @Column(name = "PalsContainerId")
    private Integer palsContainerId;
    @Basic(optional = false)
    @Column(name = "FixedFlag")
    private boolean fixedFlag;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "Label")
    private String label;
    @Basic(optional = false)
    @Column(name = "OrderTag")
    private int orderTag;
    @JoinColumn(name = "TemplateId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private ContainerTemplates templateId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "containerId")
    private Collection<ContainerTemplateItems> containerTemplateItemsCollection;
    @JoinColumn(name = "ContainerId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private ContainerTemplateItems containerId;

    public ContainerTemplateItems() {
    }

    public ContainerTemplateItems(Integer id) {
        this.id = id;
    }

    public ContainerTemplateItems(Integer id, String label, int orderTag, boolean fixedFlag) {
        this.id = id;
        this.label = label;
        this.orderTag = orderTag;
        this.fixedFlag = fixedFlag;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getOrderTag() {
        return orderTag;
    }

    public void setOrderTag(int orderTag) {
        this.orderTag = orderTag;
    }


    public ContainerTemplates getTemplateId() {
        return templateId;
    }

    public void setTemplateId(ContainerTemplates templateId) {
        this.templateId = templateId;
    }

    @XmlTransient
    public Collection<ContainerTemplateItems> getContainerTemplateItemsCollection() {
        return containerTemplateItemsCollection;
    }

    public void setContainerTemplateItemsCollection(Collection<ContainerTemplateItems> containerTemplateItemsCollection) {
        this.containerTemplateItemsCollection = containerTemplateItemsCollection;
    }

    public ContainerTemplateItems getContainerId() {
        return containerId;
    }

    public void setContainerId(ContainerTemplateItems containerId) {
        this.containerId = containerId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContainerTemplateItems)) {
            return false;
        }
        ContainerTemplateItems other = (ContainerTemplateItems) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "beanutil.ContainerTemplateItems[ id=" + id + " ]";
    }

    public boolean getFixedFlag() {
        return fixedFlag;
    }

    public void setFixedFlag(boolean fixedFlag) {
        this.fixedFlag = fixedFlag;
    }

    public Integer getPalsContainerId() {
        return palsContainerId;
    }

    public void setPalsContainerId(Integer palsContainerId) {
        this.palsContainerId = palsContainerId;
    }

}
