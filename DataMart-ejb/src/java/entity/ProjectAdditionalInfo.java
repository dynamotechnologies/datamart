/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author gauri
 */
@Entity
@Table(name = "ProjectAdditionalInfo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProjectAdditionalInfo.findAll", query = "SELECT p FROM ProjectAdditionalInfo p"),
    @NamedQuery(name = "ProjectAdditionalInfo.findByAddInfoId", query = "SELECT p FROM ProjectAdditionalInfo p WHERE p.addInfoId = :addInfoId"),
    @NamedQuery(name = "ProjectAdditionalInfo.findByDescription", query = "SELECT p FROM ProjectAdditionalInfo p WHERE p.description = :description"),
    @NamedQuery(name = "ProjectAdditionalInfo.findByLastUpdateBy", query = "SELECT p FROM ProjectAdditionalInfo p WHERE p.lastUpdateBy = :lastUpdateBy"),
    @NamedQuery(name = "ProjectAdditionalInfo.findByLasteUpdateDate", query = "SELECT p FROM ProjectAdditionalInfo p WHERE p.lasteUpdateDate = :lasteUpdateDate")})
public class ProjectAdditionalInfo implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "addInfoId")
    private Collection<ProjectAddInfoDocs> projectAddInfoDocsCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "AddInfoId")
    private Integer addInfoId;
    @Size(max = 4000)
    @Column(name = "description")
    private String description;
    @Size(max = 255)
    @Column(name = "LastUpdateBy")
    private String lastUpdateBy;
    @Column(name = "LasteUpdateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lasteUpdateDate;
    @JoinColumn(name = "ProjectId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Projects projectId;

    public ProjectAdditionalInfo() {
    }

    public ProjectAdditionalInfo(Integer addInfoId) {
        this.addInfoId = addInfoId;
    }

    public Integer getAddInfoId() {
        return addInfoId;
    }

    public void setAddInfoId(Integer addInfoId) {
        this.addInfoId = addInfoId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLastUpdateBy() {
        return lastUpdateBy;
    }

    public void setLastUpdateBy(String lastUpdateBy) {
        this.lastUpdateBy = lastUpdateBy;
    }

    public Date getLasteUpdateDate() {
        return lasteUpdateDate;
    }

    public void setLasteUpdateDate(Date lasteUpdateDate) {
        this.lasteUpdateDate = lasteUpdateDate;
    }

    public Projects getProjectId() {
        return projectId;
    }

    public void setProjectId(Projects projectId) {
        this.projectId = projectId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (addInfoId != null ? addInfoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProjectAdditionalInfo)) {
            return false;
        }
        ProjectAdditionalInfo other = (ProjectAdditionalInfo) object;
        if ((this.addInfoId == null && other.addInfoId != null) || (this.addInfoId != null && !this.addInfoId.equals(other.addInfoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ProjectAdditionalInfo[ addInfoId=" + addInfoId + " ]";
    }

    @XmlTransient
    public Collection<ProjectAddInfoDocs> getProjectAddInfoDocsCollection() {
        return projectAddInfoDocsCollection;
    }

    public void setProjectAddInfoDocsCollection(Collection<ProjectAddInfoDocs> projectAddInfoDocsCollection) {
        this.projectAddInfoDocsCollection = projectAddInfoDocsCollection;
    }
    
}
