package ingress.objections;

import java.util.zip.DataFormatException;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 * This class encapsulates xml ingressed from files validated with the following DTD:
 *
 * <!DOCTYPE objections [
 * <!ELEMENT objections (timestamp?, objection+)>
 * <!ATTLIST objections xmlns CDATA "http://www.fs.fed.us/pals/xmlns/objections/">
 * <!ELEMENT timestamp (#PCDATA)>
 * <!ELEMENT objection (id?,code,rule,objector,project,response)>
 * <!ELEMENT id (#PCDATA)>
 * <!ELEMENT code (#PCDATA)>
 * <!ELEMENT rule (#PCDATA)>
 * <!ELEMENT objector (#PCDATA)>
 * <!ELEMENT project (name,adminunit)>
 * <!ELEMENT name (#PCDATA)>
 * <!ELEMENT adminunit (#PCDATA)>
 * <!ELEMENT response (date,documentid,filesize)>
 * <!ELEMENT date (#PCDATA)>
 * <!ELEMENT documentid (#PCDATA)>
 * <!ELEMENT filesize (#PCDATA)>
 * ]>
 *
 *
 * This class is responsible for encapsulating element `project` as specified above
 */
public class LegacyObjectionProject {
    private String name;
    private String adminunit;

    public LegacyObjectionProject(Node n)
            throws DataFormatException
    {
        NodeList mynodes = n.getChildNodes();
        for (int i=0; i<mynodes.getLength(); i++)
        {
            if (mynodes.item(i).getNodeName().equals("name"))
                name = mynodes.item(i).getTextContent();
            if (mynodes.item(i).getNodeName().equals("adminunit"))
                adminunit = mynodes.item(i).getTextContent();
        }
        if (!(name.length()>0))
            throw new DataFormatException("name not found.");
        if (!(adminunit.length()>0))
            throw new DataFormatException("adminunit not found.");
    }

    public String getName()
    {
        return name;
    }

    public String getAdminUnit()
    {
        return adminunit;
    }
}