package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.DecisionMakerManagerRemote;
import us.fed.fs.www.nepa.schema.decisionmaker.Decisionmaker;
import us.fed.fs.www.nepa.schema.decisionmaker.Decisionmakers;
import us.fed.fs.www.nepa.schema.decisionmaker.ObjectFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;


@Path ("/decisions/{decisionid}/decisionmakers")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class DecisionMakerResource extends AbstractResource
{
    private DecisionMakerManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public DecisionMakerResource()
            throws ServletException
    {   
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "DecisionMaker.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.decisionmaker");
        MY_PROPS.setProperty("bean", "DecisionMakerManagerRemote");
        try {
            beanmanager = (DecisionMakerManagerRemote) init(DecisionMakerManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("DecisionMakerResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("DecisionMakerResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(@PathParam("decisionid") int decisionid, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;
        Decisionmaker dar;
        try {
            dar = (Decisionmaker)unmarshal(inputxml, Decisionmaker.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+dar.getId());

            // All clear. Now do the insert.
            beanmanager.create(dar);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the newly created entity
        return created(uri);
    }

    
    @PUT
    @Path("/{id}")
    public Response put(@PathParam("id") int id, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        Decisionmaker dm = null;
        try {
            dm = (Decisionmaker)unmarshal(inputxml, Decisionmaker.class);

            if (id!=dm.getId())
                complainLoudly(new Exception("Id in uri must match id in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.update(dm);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"</location>");
    }
    
    
    @GET
    @Path("/{id}")
    public Response get(@PathParam("decisionid") int decisionid, @PathParam("id") int id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the name of the specified entity
            Decisionmaker dar = beanmanager.get(decisionid, id);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Decisionmaker> jaxbelement = objectfactory.createDecisionmaker(dar);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @GET
    public Response getList(@PathParam("decisionid") int decisionid, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the name of the specified entity
            Decisionmakers darlist = beanmanager.getList(decisionid);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Decisionmakers> jaxbelement = objectfactory.createDecisionmakers(darlist);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("decisionid") int decisionid, @PathParam("id") int id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(decisionid, id);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}