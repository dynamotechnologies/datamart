package test.reference;

import client.ProjectTypeClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.projecttype.*;


public class ProjectTypeTest {
    private ProjectTypeClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new ProjectTypeClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postProjectType(TestData.projecttypes.id1, TestData.projecttypes.name2);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Projecttype projecttype = null;
        Projecttypes projecttypelist = null;
        try {
            projecttype = client.getProjectType(TestData.projecttypes.id1);
            projecttypelist = client.getProjectTypeList();
        } catch (IOException ex) {
            Logger.getLogger(ProjectTypeTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.projecttypes.id1, projecttype.getId());
        Assert.assertEquals(TestData.projecttypes.name1, projecttype.getName());
        Assert.assertTrue(projecttypelist.getProjecttype().get(0).getId().length()>0);
        Assert.assertTrue(projecttypelist.getProjecttype().get(0).getName().length()>0);

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        Projecttype projecttype = null;
        try {
            client.putProjectType(TestData.projecttypes.id1, TestData.projecttypes.name2);
            projecttype = client.getProjectType(TestData.projecttypes.id1);
        } catch (Exception ex) {
            Logger.getLogger(ProjectTypeTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.projecttypes.name2, projecttype.getName());

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getProjectType(TestData.projecttypes.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteProjectType(TestData.projecttypes.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }

    private void post()
    {
        try {
            client.postProjectType(TestData.projecttypes.id1, TestData.projecttypes.name1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectTypeTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteProjectType(TestData.projecttypes.id1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectTypeTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteProjectType(TestData.projecttypes.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }
}