package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "RefStates")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RefStates.findAll", query = "SELECT r FROM RefStates r"),
    @NamedQuery(name = "RefStates.findById", query = "SELECT r FROM RefStates r WHERE r.id = :id"),
    @NamedQuery(name = "RefStates.findByName", query = "SELECT r FROM RefStates r WHERE r.name = :name")})
public class RefStates implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private String id;
    @Basic(optional = false)
    @Column(name = "Name")
    private String name;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refStates")
    private Collection<ProjectStates> projectStatesCollection;

    public RefStates() {
    }

    public RefStates(String id) {
        this.id = id;
    }

    public RefStates(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Collection<ProjectStates> getProjectStatesCollection() {
        return projectStatesCollection;
    }

    public void setProjectStatesCollection(Collection<ProjectStates> projectStatesCollection) {
        this.projectStatesCollection = projectStatesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RefStates)) {
            return false;
        }
        RefStates other = (RefStates) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RefStates[ id=" + id + " ]";
    }

}
