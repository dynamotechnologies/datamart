package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.application.*;


public class ApplicationClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public ApplicationClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.application", "Application.xsd");
    }


    public void postApplication(String id, String name)
            throws IOException
    {
        Application application = new Application();
        application.setId(id);
        application.setName(name);
        postApplication(application);
    }


    public void postApplication(Application application)
            throws IOException
    {
        post(objectfactory.createApplication(application), "/ref/applications");
    }


    public Application getApplication(String id)
            throws IOException
    {
        return get("/ref/applications/" + id, Application.class);
    }


    public Applications getApplicationList()
            throws IOException
    {
        return get("/ref/applications", Applications.class);
    }


    public void putApplication(String id, String name)
            throws IOException
    {
        Application application = new Application();
        application.setId(id);
        application.setName(name);
        putApplication(application);
    }


    public void putApplication(Application application)
            throws IOException
    {
        put(objectfactory.createApplication(application), "/ref/applications/" + application.getId());
    }


    public void deleteApplication(String id)
            throws IOException
    {
        delete("/ref/applications/" + id);
    }
}