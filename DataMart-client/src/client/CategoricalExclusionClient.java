package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.categoricalexclusion.*;


public class CategoricalExclusionClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public CategoricalExclusionClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.categoricalexclusion", "CategoricalExclusion.xsd");
    }

    public void postCatExclusion(Integer id, String name, String description)
            throws IOException
    {
        Categoricalexclusion ce = new Categoricalexclusion();
        ce.setId(id);
        ce.setName(name);
        ce.setDescription(description);
        postCatExclusion(ce);
    }


    public void postCatExclusion(Categoricalexclusion ce)
            throws IOException
    {
        post(objectfactory.createCategoricalexclusion(ce), "/ref/categoricalexclusions");
    }


    public Categoricalexclusion getCatExclusion(Integer id)
            throws IOException
    {
        return get("/ref/categoricalexclusions/" + id, Categoricalexclusion.class);
    }


    public Categoricalexclusions getCatExclusionList()
            throws IOException
    {
        return get("/ref/categoricalexclusions", Categoricalexclusions.class);
    }


    public void putCatExclusion(Integer id, String name, String description)
            throws IOException
    {
        Categoricalexclusion ce = new Categoricalexclusion();
        ce.setId(id);
        ce.setName(name);
        ce.setDescription(description);
        putCatExclusion(ce);
    }

    public void putCatExclusion(Categoricalexclusion ce)
            throws IOException
    {
        put(objectfactory.createCategoricalexclusion(ce), "/ref/categoricalexclusions/" + ce.getId());
    }


    public void deleteCatExclusion(Integer id)
            throws IOException
    {
        delete("/ref/categoricalexclusions/" + id);
    }
}