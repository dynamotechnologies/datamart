package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.ProjectGoalManagerRemote;
import us.fed.fs.www.nepa.schema.projectgoal.ObjectFactory;
import us.fed.fs.www.nepa.schema.projectgoal.Projectgoal;
import us.fed.fs.www.nepa.schema.projectgoal.Projectgoals;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;

@Path ("/projects/{projtype}/{projid}/goals")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class ProjectGoalResource extends AbstractResource
{
    private ProjectGoalManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public ProjectGoalResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "ProjectGoal.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.projectgoal");
        MY_PROPS.setProperty("bean", "ProjectGoalManagerRemote");
        try {
            beanmanager = (ProjectGoalManagerRemote) init(ProjectGoalManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("ProjectGoalResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("ProjectGoalResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(@PathParam("projtype") String projtype, @PathParam("projid") String projid, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;

        try {
            Projectgoal goal = (Projectgoal)unmarshal(inputxml, Projectgoal.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+goal.getGoalid());

            if (projtype.compareTo(goal.getProjecttype()) != 0)
                complainLoudly(new Exception("Project type in uri must match type in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            if (projid.compareTo(goal.getProjectid()) != 0)
                complainLoudly(new Exception("Project ID in uri must match ID in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the insert.
            beanmanager.create(goal);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the newly created entity
        return created(uri);
    }


    @GET
    @Path("/{goalid}")
    public Response get(@PathParam("projtype") String projtype, @PathParam("projid") String projid, @PathParam("goalid") Integer goalid, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            Projectgoal goal = beanmanager.get(projtype, projid, goalid);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Projectgoal> jaxbelement = objectfactory.createProjectgoal(goal);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    /*
     *  Get list of all project goals under this project
     */
    @GET
    public Response getAllByProject(@PathParam("projtype") String projtype, @PathParam("projid") String projid, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the name of the specified unit
            Projectgoals goals = beanmanager.getAllByProject(projtype, projid);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Projectgoals> jaxbelement = objectfactory.createProjectgoals(goals);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }

    @PUT
    @Path("/{goalid}")
    public Response put(@PathParam("projtype") String projtype, @PathParam("projid") String projid, @PathParam("goalid") Integer goalid, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        Projectgoal projectgoal = null;
        try {
            projectgoal = (Projectgoal) unmarshal(inputxml, Projectgoal.class);

            if (projtype.compareTo(projectgoal.getProjecttype()) != 0)
                complainLoudly(new Exception("Project type in uri must match type in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            if (projid.compareTo(projectgoal.getProjectid()) != 0)
                complainLoudly(new Exception("Project ID in uri must match ID in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            if (goalid != projectgoal.getGoalid())
                complainLoudly(new Exception("Resource goal ID in uri must match ID in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.update(projectgoal);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"</location>");
    }


    @DELETE
    @Path("/{goalid}")
    public Response delete(@PathParam("projtype") String projtype, @PathParam("projid") String projid, @PathParam("goalid") Integer goalid, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(projtype, projid, goalid);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}
