package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "RefSpecialAuthorities")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RefSpecialAuthorities.findAll", query = "SELECT r FROM RefSpecialAuthorities r"),
    @NamedQuery(name = "RefSpecialAuthorities.findById", query = "SELECT r FROM RefSpecialAuthorities r WHERE r.id = :id"),
    @NamedQuery(name = "RefSpecialAuthorities.findByName", query = "SELECT r FROM RefSpecialAuthorities r WHERE r.name = :name"),
    @NamedQuery(name = "RefSpecialAuthorities.findByDescription", query = "SELECT r FROM RefSpecialAuthorities r WHERE r.description = :description")})
public class RefSpecialAuthorities implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private String id;
    @Basic(optional = false)
    @Column(name = "Name")
    private String name;
    @Column(name = "Description")
    private String description;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refSpecialAuthorities")
    private Collection<ProjectSpecialAuthorities> projectSpecialAuthoritiesCollection;

    public RefSpecialAuthorities() {
    }

    public RefSpecialAuthorities(String id) {
        this.id = id;
    }

    public RefSpecialAuthorities(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public Collection<ProjectSpecialAuthorities> getProjectSpecialAuthoritiesCollection() {
        return projectSpecialAuthoritiesCollection;
    }

    public void setProjectSpecialAuthoritiesCollection(Collection<ProjectSpecialAuthorities> projectSpecialAuthoritiesCollection) {
        this.projectSpecialAuthoritiesCollection = projectSpecialAuthoritiesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RefSpecialAuthorities)) {
            return false;
        }
        RefSpecialAuthorities other = (RefSpecialAuthorities) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RefSpecialAuthorities[ id=" + id + " ]";
    }

}
