package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.projectgoal.*;

import beanutil.*;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Iterator;
import java.util.List;

@Stateless(mappedName="ProjectGoalManagerRemote")

public class ProjectGoalManager implements ProjectGoalManagerRemote {

    @PersistenceContext
    private EntityManager em;

    public void create(Projectgoal resource) throws DataMartException {
        entity.ProjectGoals entity = createEntity(resource);

        entity.ProjectGoals badentity = retrieveEntity(resource.getProjecttype(), resource.getProjectid(), resource.getGoalid());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        entity = em.merge(entity);
    }

    public Projectgoal get(String projecttype, String projectid, Integer goalid)
            throws DataMartException {
        entity.ProjectGoals entity = retrieveEntity(projecttype, projectid, goalid);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return createResource(entity);
    }

    public Projectgoals getAllByProject(String projecttype, String projectid)
            throws DataMartException {
        // Get Project
        entity.Projects project = DataMartBeanUtil.getProject(em, projecttype, projectid);

        // Retrieve project resource area entities from the database
        Query query = em.createQuery("SELECT a FROM ProjectGoals a WHERE a.projects = :project ORDER BY a.orderTag, a.id");
        query.setParameter("project", project);
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Projectgoals responselist = new Projectgoals();
        while (it.hasNext()) {
            responselist.getProjectgoal().add(createResource((entity.ProjectGoals)it.next()));
        }
        return responselist;
    }

    public void update(Projectgoal resource) throws DataMartException {
        entity.ProjectGoals entity = createEntity(resource);

        entity.ProjectGoals existingentity = retrieveEntity(resource.getProjecttype(), resource.getProjectid(), resource.getGoalid());

        if (existingentity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        entity.setId(existingentity.getId());
        entity = em.merge(entity);
    }

    public void delete(String projecttype, String projectid, Integer goalid) throws DataMartException {
        entity.ProjectGoals entity = retrieveEntity(projecttype, projectid, goalid);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));
    }

    public Boolean ping() {
        return true;
    }

    /*
     * Create XML from EJB
     */
    private Projectgoal createResource(entity.ProjectGoals entity)
            throws DataMartException
    {
        Projectgoal resource = new Projectgoal();
        resource.setProjectid(entity.getProjects().getPublicId());
        resource.setProjecttype(entity.getProjects().getProjectTypes().getId());
        resource.setGoalid(entity.getGoals().getId());
        resource.setOrder(entity.getOrderTag());
        return resource;
    }

    /*
     * Create EJB from XML
     */
    private entity.ProjectGoals createEntity(Projectgoal resource)
            throws DataMartException
    {
        String projecttype = resource.getProjecttype();
        String projectid = resource.getProjectid();
        entity.Projects project = DataMartBeanUtil.getProject(em, projecttype, projectid);
        if (project == null) {
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION, "Project " + projecttype + "/" + projectid + " does not exist");
        }

        Integer goalid = resource.getGoalid();
        entity.Goals goal = em.find(entity.Goals.class, goalid);
        if (goal == null) {
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION, "Goal " + goalid + " does not exist");
        }

        entity.ProjectGoals entity = new entity.ProjectGoals();
        entity.setProjects(project);
        entity.setGoals(goal);
        entity.setOrderTag(resource.getOrder());
        return entity;
    }

    private entity.ProjectGoals retrieveEntity(String projecttype, String projectid, Integer goalid)
            throws DataMartException
    {
        // Get Project
        entity.Projects project = DataMartBeanUtil.getProject(em, projecttype, projectid);
        entity.Goals goal = em.find(entity.Goals.class, goalid);

        // Retrieve project resource area entities from the database
        Query query = em.createQuery("SELECT a FROM ProjectGoals a WHERE a.projects = :project AND a.goals = :goal");
        query.setParameter("project", project);
        query.setParameter("goal", goal);

        Object rec = DataMartBeanUtil.getEntityByQueryAllowNull(query, "Multiple rows found!");
        return (entity.ProjectGoals)rec;
    }
}
