/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateless;

import beanutil.DataMartException;

import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.ProjectAddInfoDoc.*;

/**
 *
 * @author gauri
 */


@Remote
public interface ProjectAddInfoDocManagerRemote {
    public void create(ProjectAddInfoDoc resource) throws DataMartException;
    public ProjectAddInfoDoc get(String addInfoDocId) throws DataMartException;
      public ProjectAddInfoDocs getAllByProject(String projecttype, String projectid) throws DataMartException;
  
    public void update(ProjectAddInfoDoc resource) throws DataMartException;
    public void delete(String addInfoDocId) throws DataMartException;
    public Boolean ping();

 }