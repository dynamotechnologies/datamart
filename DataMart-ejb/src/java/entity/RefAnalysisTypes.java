package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "RefAnalysisTypes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RefAnalysisTypes.findAll", query = "SELECT r FROM RefAnalysisTypes r"),
    @NamedQuery(name = "RefAnalysisTypes.findById", query = "SELECT r FROM RefAnalysisTypes r WHERE r.id = :id"),
    @NamedQuery(name = "RefAnalysisTypes.findByName", query = "SELECT r FROM RefAnalysisTypes r WHERE r.name = :name")})
public class RefAnalysisTypes implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private String id;
    @Basic(optional = false)
    @Column(name = "Name")
    private String name;
    @OneToMany(mappedBy = "refAnalysisTypes")
    private Collection<Projects> projectsCollection;

    public RefAnalysisTypes() {
    }

    public RefAnalysisTypes(String id) {
        this.id = id;
    }

    public RefAnalysisTypes(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Collection<Projects> getProjectsCollection() {
        return projectsCollection;
    }

    public void setProjectsCollection(Collection<Projects> projectsCollection) {
        this.projectsCollection = projectsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RefAnalysisTypes)) {
            return false;
        }
        RefAnalysisTypes other = (RefAnalysisTypes) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RefAnalysisTypes[ id=" + id + " ]";
    }

}
