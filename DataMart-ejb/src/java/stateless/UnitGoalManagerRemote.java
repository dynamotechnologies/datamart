package stateless;

import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.unitgoal.*;
import beanutil.DataMartException;

@Remote
public interface UnitGoalManagerRemote {
/*
 *
 * The Manager beans are responsible for:
 *   1) Implementing business logic and
 *   2) Translating front-end resources to back-end database entities.
 *
 * Often the resources will consist of data from numerous database tables. It is the manager's responsibility
 *   to ensure that these front-end resources can be accessed as though they were cut from a single piece of wood.
 *
 */
    public void create(Unitgoal resource) throws DataMartException;
    public Unitgoal get(String unitid, Integer goalid) throws DataMartException;
    public Unitgoals getAllByUnit(String unitid) throws DataMartException;
    public void update(Unitgoal resource) throws DataMartException;
    public void delete(String unitid, Integer goalid) throws DataMartException;
    public Boolean ping();
}