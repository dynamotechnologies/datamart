/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.entity;

import client.ProjectViewDocsClient;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.ProjectViewDoc.*;


/**
 *
 * @author gauri
 */

public class ProjectViewDocTest {
    private ProjectViewDocsClient client;
    

    @Before
    public void setUp()
            throws IOException
    {
        client = new ProjectViewDocsClient();
       
    }

    @Test
    public void testPost()
    {
        // This POST should fail, because it violates a constraint. The unit code is bad
        try {
           addProjectViewDocs();
            // If the above statement did not throw an exception then something has gone terribly wrong
            //Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.BAD_PARENTS)
                Assert.fail();
        } catch (Exception ex) {
            Logger.getLogger(ProjectViewDocTest.class.getName()).log(Level.SEVERE, null, ex);
             Assert.fail();
        }

        

        
    }

    @Test
    public void testGet()
    {
     

        ProjectViewDocs docs = null;
         
        try {
            docs = client.getProjectViewDocs("40090","1");
        } catch (IOException ex) {
            Logger.getLogger(ProjectViewDocTest.class.getName()).log(Level.SEVERE, null, ex);
             Assert.fail();
        }
            
          
       

       
     

       
    }

    @Test
    public void testPut()
    {
        

        ProjectViewDocs docs = null;
        try {
            modifyProjectViewDocs();
            docs = client.getProjectViewDocs("40090","1");
        } catch (Exception ex) {
            Logger.getLogger(ProjectViewDocTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

       
    }

   
    
 


    @Test
    public void testDelete()
    {
        

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            deleteProjectViewDocs();
          
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        } catch (Exception ex) {
            Logger.getLogger(ProjectViewDocTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
      
    }

 
    @After
    public void cleanFailed()
            throws Exception
    {
      
    }


    @BeforeClass
    public static void populateDependencies()
    {
        // No dependencies
    }

    @AfterClass
    public static void cleanupDependencies()
    {
        // No cleanup
    }
        public static void addProjectViewDocs() throws Exception{
	    	
	    	client.ProjectViewDocsClient client = new client.ProjectViewDocsClient();
	    	ProjectViewDoc p = new ProjectViewDoc();
//	     p.setViewId("7");
//                p.setDocumentId("FSPLT3_1460285");                
 //              	client.postProjectViewDocs("43042",p);
                p.setViewId("2");
                p.setDocumentId("FSPLT2_287333");                
               	client.postProjectViewDocs("40090",p);
	    }
    
	 public static void deleteProjectViewDocs() throws Exception{
	    	
	    	client.ProjectViewDocsClient client = new client.ProjectViewDocsClient();
                client.deleteProjectViewDocs("17735","2","1");
         }   
         
          public static void modifyProjectViewDocs() throws Exception{
	    	client.ProjectViewDocsClient client = new client.ProjectViewDocsClient();
  	  	ProjectViewDoc p = new ProjectViewDoc();
                p.setViewId("2");
                p.setDocumentId("FSPLT2_287333");               
	    	client.putProjectViewDocs("40090",p);
         }  
}