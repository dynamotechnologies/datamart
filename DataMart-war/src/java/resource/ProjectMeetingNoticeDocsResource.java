/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.ProjectMeetingNoticeDocsManagerRemote;
import us.fed.fs.www.nepa.schema.ProjectMeetingNoticeDoc.ObjectFactory;
import us.fed.fs.www.nepa.schema.ProjectMeetingNoticeDoc.ProjectMeetingNoticeDoc;
import us.fed.fs.www.nepa.schema.ProjectMeetingNoticeDoc.ProjectMeetingNoticeDocs;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;



@Path ("/projects/{projtype}/{projid}/meetingnotices/{meetingId}/docs")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class ProjectMeetingNoticeDocsResource extends AbstractResource
{
    private ProjectMeetingNoticeDocsManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public ProjectMeetingNoticeDocsResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "ProjectMeetingNoticeDoc.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.ProjectMeetingNoticeDoc");
        MY_PROPS.setProperty("bean", "ProjectMeetingNoticeDocsManagerRemote");
        try {
            beanmanager = (ProjectMeetingNoticeDocsManagerRemote) init(ProjectMeetingNoticeDocsManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("ProjectMeetingNoticeDocsResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("ProjectMeetingNoticeDocsResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(@PathParam("projtype") String projtype, @PathParam("projid") String projid, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;

        try {
            ProjectMeetingNoticeDoc meetingnoticeDoc = (ProjectMeetingNoticeDoc)unmarshal(inputxml, ProjectMeetingNoticeDoc.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+meetingnoticeDoc.getMeetingDocId());

           
        
            // All clear. Now do the insert.
            beanmanager.create(meetingnoticeDoc);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the newly created entity
        return created(uri);
    }


    @GET
    @Path("/{meetingDocId}")
    public Response get(@PathParam("projtype") String projtype, @PathParam("projid") String projid, @PathParam("meetingId") Integer meetingId, @PathParam("meetingDocId") Integer meetingDocId, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            ProjectMeetingNoticeDoc meetingnoticeDoc = beanmanager.get(projtype, projid, meetingId,meetingDocId);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<ProjectMeetingNoticeDoc> jaxbelement = objectfactory.createProjectMeetingNoticeDoc(meetingnoticeDoc);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    /*
     *  Get list of all project Meetingnotices under this project
     */
    @GET
    public Response getAllByProject(@PathParam("projtype") String projtype, @PathParam("projid") String projid,@PathParam("meetingId") Integer meetingId,  @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the name of the specified unit
            ProjectMeetingNoticeDocs meetingnoticeDocs = beanmanager.getAllByProject(projtype, projid,meetingId);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<ProjectMeetingNoticeDocs> jaxbelement = objectfactory.createProjectMeetingNoticeDocs(meetingnoticeDocs);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }

    @PUT
    @Path("/{meetingDocId}")
    public Response put(@PathParam("projtype") String projtype, @PathParam("projid") String projid, @PathParam("meetingId") Integer meetingId,@PathParam("meetingDocId") Integer meetingDocId, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        ProjectMeetingNoticeDoc meetingnotice = null;
        try {
            meetingnotice = (ProjectMeetingNoticeDoc) unmarshal(inputxml, ProjectMeetingNoticeDoc.class);

          
           
            if (!meetingId.toString().equals(meetingnotice.getMeetingId()))
                complainLoudly(new Exception("Meetingnotice ID in uri must match ID in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);
           if (!meetingDocId.toString().equals(meetingnotice.getMeetingDocId()))
                complainLoudly(new Exception("Meetingnotice Doc ID in uri must match ID in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.update(meetingnotice);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"</location>");
    }


    @DELETE
    @Path("/{meetingDocId}")
    public Response delete(@PathParam("projtype") String projtype, @PathParam("projid") String projid, @PathParam("meetingId") Integer meetingId,@PathParam("meetingDocId") Integer meetingDocId,  @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(projtype, projid, meetingId,meetingDocId);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}
