package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.appealoutcome.*;


public class AppealOutcomeClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public AppealOutcomeClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.appealoutcome", "AppealOutcome.xsd");
    }


    public void postAppealOutcome(String id, String name)
            throws IOException
    {
        Appealoutcome outcome = new Appealoutcome();
        outcome.setId(id);
        outcome.setName(name);
        postAppealOutcome(outcome);
    }


    public void postAppealOutcome(Appealoutcome outcome)
            throws IOException
    {
        post(objectfactory.createAppealoutcome(outcome), "/ref/appealoutcomes");
    }


    public Appealoutcome getAppealOutcome(String id)
            throws IOException
    {
        return get("/ref/appealoutcomes/" + id, Appealoutcome.class);
    }


    public Appealoutcomes getAppealOutcomeList()
            throws IOException
    {
        return get("/ref/appealoutcomes", Appealoutcomes.class);
    }


    public void putAppealOutcome(String id, String name)
            throws IOException
    {
        Appealoutcome outcome = new Appealoutcome();
        outcome.setId(id);
        outcome.setName(name);
        putAppealOutcome(outcome);
    }


    public void putAppealOutcome(Appealoutcome outcome)
            throws IOException
    {
        put(objectfactory.createAppealoutcome(outcome), "/ref/appealoutcomes/" + outcome.getId());
    }


    public void deleteAppealOutcome(String id)
            throws IOException
    {
        delete("/ref/appealoutcomes/" + id);
    }
}