package test.reference;

import client.AnalysisTypeClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.analysistype.*;


public class AnalysisTypeTest {
    private AnalysisTypeClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new AnalysisTypeClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postAnalysisType(TestData.analysistypes.id1, TestData.analysistypes.name2);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Analysistype analysistype = null;
        Analysistypes analysistypelist = null;
        try {
            analysistype = client.getAnalysisType(TestData.analysistypes.id1);
            analysistypelist = client.getAnalysisTypeList();
        } catch (IOException ex) {
            Logger.getLogger(AnalysisTypeTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.analysistypes.id1, analysistype.getId());
        Assert.assertEquals(TestData.analysistypes.name1, analysistype.getName());
        Assert.assertTrue(analysistypelist.getAnalysistype().get(0).getId().length()>0);
        Assert.assertTrue(analysistypelist.getAnalysistype().get(0).getName().length()>0);

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        Analysistype analysistype = null;
        try {
            client.putAnalysisType(TestData.analysistypes.id1, TestData.analysistypes.name2);
            analysistype = client.getAnalysisType(TestData.analysistypes.id1);
        } catch (Exception ex) {
            Logger.getLogger(AnalysisTypeTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.analysistypes.name2, analysistype.getName());

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getAnalysisType(TestData.analysistypes.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteAnalysisType(TestData.analysistypes.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }

    private void post()
    {
        try {
            client.postAnalysisType(TestData.analysistypes.id1, TestData.analysistypes.name1);
        } catch (IOException ex) {
            Logger.getLogger(AnalysisTypeTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteAnalysisType(TestData.analysistypes.id1);
        } catch (IOException ex) {
            Logger.getLogger(AnalysisTypeTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteAnalysisType(TestData.analysistypes.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }
}