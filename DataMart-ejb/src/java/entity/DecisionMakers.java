package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "DecisionMakers")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DecisionMakers.findAll", query = "SELECT d FROM DecisionMakers d"),
    @NamedQuery(name = "DecisionMakers.findById", query = "SELECT d FROM DecisionMakers d WHERE d.id = :id"),
    @NamedQuery(name = "DecisionMakers.findByName", query = "SELECT d FROM DecisionMakers d WHERE d.name = :name"),
    @NamedQuery(name = "DecisionMakers.findByTitle", query = "SELECT d FROM DecisionMakers d WHERE d.title = :title"),
    @NamedQuery(name = "DecisionMakers.findByDecMakeSignedDate", query = "SELECT d FROM DecisionMakers d WHERE d.decMakeSignedDate = :decMakeSignedDate")})
public class DecisionMakers implements Serializable {
    @Column(name =     "DecMakeSignedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date decMakeSignedDate;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Column(name = "Name")
    private String name;
    @Column(name = "Title")
    private String title;
    @JoinColumn(name = "DecisionId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Decisions decisions;

    public DecisionMakers() {
    }

    public DecisionMakers(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Decisions getDecisions() {
        return decisions;
    }

    public void setDecisions(Decisions decisions) {
        this.decisions = decisions;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DecisionMakers)) {
            return false;
        }
        DecisionMakers other = (DecisionMakers) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DecisionMakers[ id=" + id + " ]";
    }

    public Date getDecMakeSignedDate() {
        return decMakeSignedDate;
    }

    public void setDecMakeSignedDate(Date decMakeSignedDate) {
        this.decMakeSignedDate = decMakeSignedDate;
    }

}
