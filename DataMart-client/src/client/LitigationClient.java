package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.litigation.*;


public class LitigationClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public LitigationClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.litigation", "Litigation.xsd");
    }

    public void postLitigation(Integer id,
            int decisionid,
            String casename,
            int status,
            int outcome,
            String closed)
                throws IOException
    {
        Litigation l = new Litigation();
        l.setId(id);
        l.setDecisionid(decisionid);
        l.setCasename(casename);
        l.setStatus(status);
        l.setOutcome(outcome);
        l.setClosed(dateStrToXML(closed));
        postLitigation(l);
    }


    public void postLitigation(Litigation l)
            throws IOException
    {
        post(objectfactory.createLitigation(l), "/litigations");
    }


    public Litigation getLitigation(Integer id)
            throws IOException
    {
        return get("/litigations/" + id, Litigation.class);
    }


    public Litigations getLitigationList(int decisionid)
            throws IOException
    {
        return get("/decisions/" + decisionid + "/litigations", Litigations.class);
    }


    public void putLitigation(Integer id,
            int decisionid,
            String casename,
            int status,
            int outcome,
            String closed)
                throws IOException
    {
        Litigation l = new Litigation();
        l.setId(id);
        l.setDecisionid(decisionid);
        l.setCasename(casename);
        l.setStatus(status);
        l.setOutcome(outcome);
        l.setClosed(dateStrToXML(closed));
        putLitigation(l);
    }

    public void putLitigation(Litigation l)
            throws IOException
    {
        put(objectfactory.createLitigation(l), "/litigations/" + l.getId());
    }


    public void deleteLitigation(Integer id)
            throws IOException
    {
        delete("/litigations/" + id);
    }
}