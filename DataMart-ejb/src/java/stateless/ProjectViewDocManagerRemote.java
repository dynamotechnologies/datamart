/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateless;

import beanutil.DataMartException;
import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.ProjectViewDoc.*;

/**
 *
 * @author gauri
 */
@Remote
public interface ProjectViewDocManagerRemote {
     public void create(ProjectViewDoc resource) throws DataMartException;
     public ProjectViewDoc get(Integer id) throws DataMartException;
     public ProjectViewDocs getAllByView(Integer viewId) throws DataMartException;
     public void update(ProjectViewDoc resource) throws DataMartException;
     public void delete(Integer id) throws DataMartException;
     public Boolean ping();
}
