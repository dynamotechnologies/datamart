package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.appealdismissalreason.*;

import beanutil.*;
import java.util.Iterator;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless(mappedName="AppealDismissalReasonManagerRemote")
public class AppealDismissalReasonManager implements AppealDismissalReasonManagerRemote
{

    @PersistenceContext
    private EntityManager em;


    @Override
    public void create(Appealdismissalreason resource)
             throws DataMartException
    {
        validate(resource);
        entity.RefAppealDismissalReasons entity = createEntity(resource);

        entity.RefAppealDismissalReasons badentity = createEntity(resource.getId());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        entity = em.merge(entity);
    }

    @Override
    public Appealdismissalreasons getAll()
             throws DataMartException
    {
        // Retrieve all entities from the database
        Query query = em.createQuery("SELECT u FROM RefAppealDismissalReasons u");
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Appealdismissalreasons responselist = new Appealdismissalreasons();
        while (it.hasNext()) {
            responselist.getAppealdismissalreason().add(createResource( (entity.RefAppealDismissalReasons)it.next() ));
        }
        return responselist;
    }

    @Override
    public String getName(String id)
            throws DataMartException
    {
        entity.RefAppealDismissalReasons entity = createEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return entity.getName();
    }

    @Override
    public void update(Appealdismissalreason resource)
             throws DataMartException
    {
        validate(resource);
        entity.RefAppealDismissalReasons entity = createEntity(resource);

        if (createEntity(resource.getId())==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        entity = em.merge(entity);
    }

    @Override
    public void delete(String id)
             throws DataMartException
    {
        entity.RefAppealDismissalReasons entity = createEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     *
     * This method does the mapping from jaxb object to entity object and vice versa
     */
    private Appealdismissalreason createResource(entity.RefAppealDismissalReasons entity)
            throws DataMartException
    {
        Appealdismissalreason resource = new Appealdismissalreason();
        resource.setId(entity.getId().toString());
        resource.setName(entity.getName());
        return resource;
    }

    private entity.RefAppealDismissalReasons createEntity(Appealdismissalreason resource)
            throws DataMartException
    {
        int id;
        try {
            id = Integer.parseInt(resource.getId());
        } catch (NumberFormatException ex) {
            throw new DataMartException(DataMartException.BAD_FORMAT, "Id must be an integer");
        }
        entity.RefAppealDismissalReasons entity = new entity.RefAppealDismissalReasons();
        entity.setId(id);
        entity.setName(resource.getName());
        return entity;
    }

    // This version of overloaded method createEntity() accepts a String ID as a parameter, and returns the
    //   associated entity, or null if it doesn't exist
    private entity.RefAppealDismissalReasons createEntity(String id)
            throws DataMartException
    {
        int intId;
        try {
            intId = Integer.parseInt(id);
        } catch (NumberFormatException ex) {
            throw new DataMartException(DataMartException.BAD_FORMAT, "Id must be an integer");
        }
        return em.find(entity.RefAppealDismissalReasons.class, intId);
    }

    private void validate(Appealdismissalreason resource)
            throws DataMartException
    {
        DataMartBeanUtil.confirmNotNull(resource.getName(), "Name");
    }
}
