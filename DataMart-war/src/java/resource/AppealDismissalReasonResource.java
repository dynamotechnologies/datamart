package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.AppealDismissalReasonManagerRemote;
import us.fed.fs.www.nepa.schema.appealdismissalreason.Appealdismissalreason;
import us.fed.fs.www.nepa.schema.appealdismissalreason.Appealdismissalreasons;
import us.fed.fs.www.nepa.schema.appealdismissalreason.ObjectFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;


@Path ("/ref/appealdismissalreasons")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class AppealDismissalReasonResource extends AbstractResource
{
    private AppealDismissalReasonManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public AppealDismissalReasonResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "AppealDismissalReason.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.appealdismissalreason");
        MY_PROPS.setProperty("bean", "AppealDismissalReasonManagerRemote");
        try {
            beanmanager = (AppealDismissalReasonManagerRemote) init(AppealDismissalReasonManagerRemote.class);
        } catch (IOException ex) {
            log.error("AppealDismissalReasonResource failed to start. Could not start the bean manager.", ex);
            throw new ServletException("AppealDismissalReasonResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("AppealDismissalReasonResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;
        try {
            Appealdismissalreason adr = (Appealdismissalreason)unmarshal(inputxml, Appealdismissalreason.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+adr.getId());

            // All clear. Now do the insert.
            beanmanager.create(adr);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        // Return a link to the newly created entity
        return created(uri);
    }


    @GET
    public Response get(@Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get a dump of all the entities in the datamart
            Appealdismissalreasons adrlist = beanmanager.getAll();

            // Return 404 if none exist
            if (adrlist.getAppealdismissalreason().isEmpty())
                notFound();

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Appealdismissalreasons> jaxbelement = objectfactory.createAppealdismissalreasons(adrlist);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }

    @GET
    @Path("/{id}")
    public Response get(@PathParam("id") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the name of the specified entity
            String name = beanmanager.getName(id);
            Appealdismissalreason adr = new Appealdismissalreason();
            adr.setId(id);
            adr.setName(name);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Appealdismissalreason> jaxbelement = objectfactory.createAppealdismissalreason(adr);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @PUT
    @Path("/{id}")
    public Response put(@PathParam("id") String id, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        Appealdismissalreason adr = null;
        try {
            adr = (Appealdismissalreason) unmarshal(inputxml, Appealdismissalreason.class);

            if (!id.equalsIgnoreCase(adr.getId()))
                complainLoudly(new Exception("Id in uri must match id in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.update(adr);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"/"+adr.getId()+"</location>");
    }


    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(id);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}