package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.NepaCategoricalExclusionManagerRemote;
import us.fed.fs.www.nepa.schema.nepacategoricalexclusion.Nepacategoricalexclusion;
import us.fed.fs.www.nepa.schema.nepacategoricalexclusion.Nepacategoricalexclusions;
import us.fed.fs.www.nepa.schema.nepacategoricalexclusion.ObjectFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;


@Path ("/projects/nepa/{projectid}/nepacategoricalexclusions")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class NepaCategoricalExclusionResource extends AbstractResource
{
    private NepaCategoricalExclusionManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public NepaCategoricalExclusionResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "NepaCategoricalExclusion.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.nepacategoricalexclusion");
        MY_PROPS.setProperty("bean", "NepaCategoricalExclusionManagerRemote");
        try {
            beanmanager = (NepaCategoricalExclusionManagerRemote) init(NepaCategoricalExclusionManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("NepaCategoricalExclusionResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("NepaCategoricalExclusionResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(@PathParam("projectid") String projectid, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;
        Nepacategoricalexclusion nce;
        try {
            nce = (Nepacategoricalexclusion)unmarshal(inputxml, Nepacategoricalexclusion.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+nce.getCategoricalexclusionid());

            // All clear. Now do the insert.
            beanmanager.create(nce);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the newly created entity
        return created(uri);
    }


    @GET
    @Path("/{id}")
    public Response get(@PathParam("projectid") String projectid, @PathParam("id") int id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the name of the specified entity
            Nepacategoricalexclusion nce = beanmanager.get(projectid, id);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Nepacategoricalexclusion> jaxbelement = objectfactory.createNepacategoricalexclusion(nce);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @GET
    public Response getList(@PathParam("projectid") String projectid, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the name of the specified entity
            Nepacategoricalexclusions ncelist = beanmanager.getList(projectid);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Nepacategoricalexclusions> jaxbelement = objectfactory.createNepacategoricalexclusions(ncelist);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("projectid") String projectid, @PathParam("id") int id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(projectid, id);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}