package ingress.detailprojects;

import ingress.LegacyUtil;
import java.util.ArrayList;
import java.util.zip.DataFormatException;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 *
 * <!DOCTYPE unitprojectsdetail [
 * <!ELEMENT unitprojectsdetail (timestamp, unit*)>
 * <!ELEMENT timestamp (#PCDATA)>
 * <!ELEMENT unit (code,extendeddetails,project+)>
 * <!ELEMENT code (#PCDATA)>
 * <!ELEMENT extendeddetails (#PCDATA)>
 * <!ELEMENT project (id,projectdocumentid,expirationdate?,name,status,type,url?,appealslink,objectionslink,description,projectsummary,contact,catexclusion*,specauth+,milestone*,purpose,adminunit,activity+,commreg?,esd?,cenodecision?,location?,decision*,pubflag,updatedate,sopa,primaryProjectManager?,secondaryProjectManager?,dataEntryPerson?,goal*,resourcearea*)>
 * <!ELEMENT id (#PCDATA)>
 * <!ELEMENT projectdocumentid (#PCDATA)>
 * <!ELEMENT expirationdate (#PCDATA)>
 * <!ELEMENT name (#PCDATA)>
 * <!ELEMENT status (#PCDATA)>
 * <!ELEMENT type (#PCDATA)>
 * <!ELEMENT url (#PCDATA)>
 * <!ELEMENT appealslink (#PCDATA)>
 * <!ELEMENT objectionslink (#PCDATA)>
 * <!ELEMENT description (#PCDATA)>
 * <!ELEMENT projectsummary (#PCDATA)>
 * <!ELEMENT contact (contactShortName,contactname, contactphone, contactemail)>
 * <!ELEMENT contactShortName (#PCDATA)>
 * <!ELEMENT contactname (#PCDATA)>
 * <!ELEMENT contactphone (#PCDATA)>
 * <!ELEMENT contactemail (#PCDATA)>
 * <!ELEMENT catexclusion (#PCDATA)>
 * <!ELEMENT specauth (#PCDATA)>
 * <!ELEMENT milestone (msseq,mstype,msdate,msstatus,mshistoryflag)>
 * <!ELEMENT msseq (#PCDATA)>
 * <!ELEMENT mstype (#PCDATA)>
 * <!ELEMENT msdate (#PCDATA)>
 * <!ELEMENT msstatus (#PCDATA)>
 * <!ELEMENT mshistoryflag (#PCDATA)>
 * <!ELEMENT purpose (purposename+)>
 * <!ELEMENT purposename (#PCDATA)>
 * <!ELEMENT adminunit (#PCDATA)>
 * <!ELEMENT activity (#PCDATA)>
 * <!ELEMENT commreg (#PCDATA)>
 * <!ELEMENT location (locdesc,locfor+,locdst+,locstate+,loccount+,loclegal?,loclat?,loclong?)>
 * <!ELEMENT locdesc (#PCDATA)>
 * <!ELEMENT locfor (#PCDATA)>
 * <!ELEMENT locdst (#PCDATA)>
 * <!ELEMENT locstate (#PCDATA)>
 * <!ELEMENT loccount (#PCDATA)>
 * <!ELEMENT loclegal (#PCDATA)>
 * <!ELEMENT loclat (#PCDATA)>
 * <!ELEMENT loclong (#PCDATA)>
 * <!ELEMENT decision (decname,decisionId,decnotice?,decconstraint,decapprule,decappstat?,decapp*,declit*,decareasize?,decareaunits?)>
 * <!ELEMENT decname (#PCDATA)>
 * <!ELEMENT decisionId (#PCDATA)>
 * <!ELEMENT decnotice (#PCDATA)>
 * <!ELEMENT decconstraint (#PCDATA)>
 * <!ELEMENT decapprule (#PCDATA)>
 * <!ELEMENT decappstat (#PCDATA)>
 * <!ELEMENT decapp (appealid,appealadminunitcode,rule,appellant?,outcome?,response?)>
 * <!ELEMENT appealid (#PCDATA)>
 * <!ELEMENT appealadminunitcode (#PCDATA)>
 * <!ELEMENT rule (#PCDATA)>
 * <!ELEMENT appellant (#PCDATA)>
 * <!ELEMENT outcome (#PCDATA)>
 * <!ELEMENT response (date,documentid?,appealresponsefilesize?)>
 * <!ELEMENT date (#PCDATA)>
 * <!ELEMENT documentid (#PCDATA)>
 * <!ELEMENT appealresponsefilesize (#PCDATA)>
 * <!ELEMENT declit (litid,litname,litstat,litoutcome?,litclosed?)>
 * <!ELEMENT litid (#PCDATA)>
 * <!ELEMENT litname (#PCDATA)>
 * <!ELEMENT litstat (#PCDATA)>
 * <!ELEMENT litoutcome (#PCDATA)>
 * <!ELEMENT litclosed (#PCDATA)>
 * <!ELEMENT pubflag (#PCDATA)>
 * <!ELEMENT updatedate (#PCDATA)>
 * <!ELEMENT sopa (sopaFlag,isNewProjectFlag?,sopaHeaderUnit?)>
 * <!ELEMENT sopaFlag (#PCDATA)>
 * <!ELEMENT isNewProjectFlag (#PCDATA)>
 * <!ELEMENT sopaHeaderUnit (#PCDATA)>
 * <!ELEMENT primaryProjectManager (#PCDATA)>
 * <!ELEMENT secondaryProjectManager (#PCDATA)>
 * <!ELEMENT dataEntryPerson (#PCDATA)>
 * <!ELEMENT goal (goalid,goalorder?)>
 * <!ELEMENT goalid (#PCDATA)>
 * <!ELEMENT goalorder (#PCDATA)>
 * <!ELEMENT resourcearea (resourceareaid,resourceareaorder?)>
 * <!ELEMENT resourceareaid (#PCDATA)>
 * <!ELEMENT resourceareaorder (#PCDATA)>
 * <!ELEMENT decareasize (#PCDATA)>
 * <!ELEMENT decareaunits (#PCDATA)>
 * <!ATTLIST unitprojects xmlns CDATA 'http://www.fs.fed.us/pals/xmlns/unitprojectsdetail/'>
 * ]>
 *
 * This class is responsible for encapsulating element `location` as specified above
 */
public class LegacyLocation {
    private String description = "";
    private ArrayList<String> forests = new ArrayList<String>(); // one or many
    private ArrayList<String> districts = new ArrayList<String>(); // one or many
    private ArrayList<String> states = new ArrayList<String>(); // one or many
    private ArrayList<String> counties = new ArrayList<String>(); // one or many
    private String legal = ""; // optional
    private Double latitude = Double.NaN; // optional
    private Double longitude = Double.NaN; // optional

    public LegacyLocation()
    {
        // allow instantiation without nodes
    }

    public LegacyLocation(Node n)
            throws DataFormatException
    {
        NodeList mynodes = n.getChildNodes();
        for (int i=0; i<mynodes.getLength(); i++)
        {
            if (mynodes.item(i).getNodeName().equals("locdesc"))
                description = mynodes.item(i).getTextContent();
            if (mynodes.item(i).getNodeName().equals("locfor"))
                addForest(mynodes.item(i));
            if (mynodes.item(i).getNodeName().equals("locdst"))
                addDistrict(mynodes.item(i));
            if (mynodes.item(i).getNodeName().equals("locstate"))
                addState(mynodes.item(i));
            if (mynodes.item(i).getNodeName().equals("loccount"))
                addCounty(mynodes.item(i));
            if (mynodes.item(i).getNodeName().equals("loclegal"))
                legal = mynodes.item(i).getTextContent();
            if (mynodes.item(i).getNodeName().equals("loclat"))
                setLatitude(mynodes.item(i));
            if (mynodes.item(i).getNodeName().equals("loclong"))
                setLongitude(mynodes.item(i));
        }
        if (!(description.length()>0))
            throw new DataFormatException("locdesc not found.");
        if (!(forests.size()>0))
            throw new DataFormatException("locfor not found.");
        if (!(districts.size()>0))
            throw new DataFormatException("locdst not found.");
        if (!(states.size()>0))
            throw new DataFormatException("locstate not found.");
        if (!(counties.size()>0))
            throw new DataFormatException("loccount not found.");
    }

    private void setLatitude(Node n)
    {
        latitude = Double.parseDouble(n.getTextContent());
    }

    private void setLongitude(Node n)
    {
        longitude = Double.parseDouble(n.getTextContent());
    }

    private void addForest(Node n)
            throws DataFormatException
    {
        forests.add(n.getTextContent());
    }

    private void addDistrict(Node n)
            throws DataFormatException
    {
        districts.add(n.getTextContent());
    }

    private void addState(Node n)
            throws DataFormatException
    {
        String s = LegacyUtil.getStateIdFromPalsId(
                Integer.parseInt( n.getTextContent() ));
        states.add(s);
    }

    private void addCounty(Node n)
            throws DataFormatException
    {
        counties.add(n.getTextContent());
    }

    public String getDescription()
    {
        return description;
    }

    public ArrayList<String> getForests()
    {
        return forests;
    }

    public ArrayList<String> getDistricts()
    {
        return districts;
    }

    public ArrayList<String> getStates()
    {
        return states;
    }

    public ArrayList<String> getCounties()
    {
        return counties;
    }

    public String getLegal()
    {
        return legal;
    }

    public Double getLatitude()
    {
        return latitude;
    }

    public Double getLongitude()
    {
        return longitude;
    }
}
