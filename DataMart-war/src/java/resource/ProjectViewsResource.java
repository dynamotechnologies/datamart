/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resource;





/**
 *
 * @author gauri
 */


import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.ProjectViewManagerRemote;
import us.fed.fs.www.nepa.schema.ProjectView.*;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;


@Path ("/projects/{projtype}/{projid}/views")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class ProjectViewsResource extends AbstractResource
{
    private ProjectViewManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public ProjectViewsResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "ProjectView.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.ProjectView");
        MY_PROPS.setProperty("bean", "ProjectViewManagerRemote");
        try {
            beanmanager = (ProjectViewManagerRemote) init(ProjectViewManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("ProjectViewsResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("ProjectViewsResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(@PathParam("projtype") String projtype, @PathParam("projid") String projid, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;

        try {
            ProjectView projView = (ProjectView)unmarshal(inputxml, ProjectView.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+projView.getId());

           
            if (projid.compareTo(""+projView.getProjectId()) != 0)
                complainLoudly(new Exception("Project ID in uri must match ID in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the insert.
            beanmanager.create(projView);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the newly created entity
        return created(uri);
    }


    @GET
    @Path("/{viewId}")
    public Response get(@PathParam("projtype") String projtype, @PathParam("projid") String projid, @PathParam("viewId") Integer viewId, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            ProjectView projectView = beanmanager.get(viewId);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<ProjectView> jaxbelement = objectfactory.createProjectView(projectView);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    /*
     *  Get list of all project Meetingnotices under this project
     */
    @GET
    public Response getAllByProject(@PathParam("projtype") String projtype, @PathParam("projid") String projid, @QueryParam("userId") String userId,@Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            ProjectViews projectViews = null;
            if(null != userId)
            projectViews = beanmanager.getAllByProjectByUser(projtype, projid,userId);
            else
            projectViews = beanmanager.getAllByProject(projtype, projid);  

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();  
            JAXBElement<ProjectViews> jaxbelement = objectfactory.createProjectViews(projectViews);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }

    @PUT
    @Path("/{viewId}")
    public Response put(@PathParam("projtype") String projtype, @PathParam("projid") String projid, @PathParam("viewId") Integer viewId, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        ProjectView projectView = null;
        try {
            projectView = (ProjectView) unmarshal(inputxml, ProjectView.class);

          
            if (projid.compareTo(""+projectView.getProjectId()) != 0)
                complainLoudly(new Exception("Project ID in uri must match ID in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            if (!viewId.toString().equals(projectView.getId()))
                complainLoudly(new Exception("View ID in uri must match ID in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.update(projectView);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"</location>");
    }


    @DELETE
    @Path("/{viewId}")
    public Response delete(@PathParam("projtype") String projtype, @PathParam("projid") String projid, @PathParam("viewId") Integer viewId, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(viewId);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}
