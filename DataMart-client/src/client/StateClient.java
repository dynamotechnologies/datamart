package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.state.*;


public class StateClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public StateClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.state", "State.xsd");
    }


    public void postState(String id, String name)
            throws IOException
    {
        State state = new State();
        state.setId(id);
        state.setName(name);
        postState(state);
    }


    public void postState(State state)
            throws IOException
    {
        post(objectfactory.createState(state), "/ref/states");
    }


    public State getState(String id)
            throws IOException
    {
        return get("/ref/states/" + id, State.class);
    }


    public States getStateList()
            throws IOException
    {
        return get("/ref/states", States.class);
    }


    public void putState(String id, String name)
            throws IOException
    {
        State state = new State();
        state.setId(id);
        state.setName(name);
        putState(state);
    }


    public void putState(State state)
            throws IOException
    {
        put(objectfactory.createState(state), "/ref/states/" + state.getId());
    }


    public void deleteState(String id)
            throws IOException
    {
        delete("/ref/states/" + id);
    }
}