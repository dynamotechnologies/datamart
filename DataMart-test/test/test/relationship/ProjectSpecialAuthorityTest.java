package test.relationship;

import client.SpecialAuthorityClient;
import client.ProjectSpecialAuthorityClient;
import client.ProjectClient;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.projectspecialauthority.*;


public class ProjectSpecialAuthorityTest {
    private ProjectSpecialAuthorityClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new ProjectSpecialAuthorityClient();
    }

    @Test
    public void testPut()
    {
        put();

        // second PUT should overwrite the first
        try {
            client.putProjectSpecialAuthority(TestData.projects.id1, TestData.specialauthorities.id1);
        } catch (Exception ex) {
            Logger.getLogger(ProjectSpecialAuthorityTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        put();

        Projectspecialauthority authority = null;
        Projectspecialauthorities authorities = null;
        try {
            authority = client.getProjectSpecialAuthority(TestData.projects.id1, TestData.specialauthorities.id1);
            authorities = client.getProjectSpecialAuthorityList(TestData.projects.id1);
        } catch (Exception ex) {
            Logger.getLogger(ProjectSpecialAuthorityTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // make sure the specialauthority we posted is present in the GET response
        Boolean specialAuthorityWePostedIsPresentInTheGETResponse = false;
        Iterator it = authorities.getProjectspecialauthority().iterator();
        while (it.hasNext()) {
            Projectspecialauthority auth = (Projectspecialauthority) it.next();
            if (auth.getSpecialauthorityid().equalsIgnoreCase(TestData.specialauthorities.id1)) {
                specialAuthorityWePostedIsPresentInTheGETResponse = true;
            }
        }
        Assert.assertTrue(specialAuthorityWePostedIsPresentInTheGETResponse);
        Assert.assertEquals(TestData.projects.id1, authority.getProjectid());
        Assert.assertEquals(TestData.specialauthorities.id1, authority.getSpecialauthorityid());

        delete();
    }

    @Test
    public void testDelete()
    {
        put();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getProjectSpecialAuthority(TestData.projects.id1, TestData.specialauthorities.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteProjectSpecialAuthority(TestData.projects.id1, TestData.specialauthorities.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }


    private void put()
    {
        try {
            client.putProjectSpecialAuthority(TestData.projects.id1, TestData.specialauthorities.id1);
        } catch (Exception ex) {
            Logger.getLogger(ProjectSpecialAuthorityTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteProjectSpecialAuthority(TestData.projects.id1, TestData.specialauthorities.id1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectSpecialAuthorityTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteProjectSpecialAuthority(TestData.projects.id1, TestData.specialauthorities.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }

    
    @BeforeClass
    public static void  populateDependencies()
    {
        SpecialAuthorityClient refspecialauthorityclient = null;
        ProjectClient projclient = null;
        try {
            refspecialauthorityclient = new SpecialAuthorityClient();
            projclient = new ProjectClient();
        } catch (Exception ex) {
            Logger.getLogger(ProjectSpecialAuthorityTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Project Special Authorities rely on Reference Special Authorities. Pre-populate a Reference Special Authority
        try {
            refspecialauthorityclient.postSpecialAuthority( TestData.specialauthorities.id1,
                                                            TestData.specialauthorities.name1,
                                                            TestData.specialauthorities.description1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectSpecialAuthorityTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Project Special Authorities rely on Projects. Pre-populate a Project.
        ArrayList<String> purposeids1 = new ArrayList<String>();
        purposeids1.add(TestData.projects.purposeid1);
        purposeids1.add(TestData.projects.purposeid2);
        purposeids1.add(TestData.projects.purposeid3);
        try {
            projclient.postProject(TestData.projects.id1, TestData.projects.type1,
                    TestData.projects.name1, TestData.projects.unitcode1, TestData.projects.description1,
                    TestData.projects.lastupdate1, TestData.projects.commentreg1, TestData.projects.wwwlink1,
                    TestData.projects.wwwsummary1, TestData.projects.wwwpub1, TestData.projects.analysistypeid1,
                    TestData.projects.statusid1, TestData.projects.contactname1, TestData.projects.contactphone1,
                    TestData.projects.contactemail1, purposeids1, TestData.projects.expirationdate1, TestData.projects.projectdocumentid1,
                    TestData.projects.sopapub, TestData.projects.sopanew, TestData.projects.sopaheader, TestData.projects.esd, TestData.projects.cenodecision);
        } catch (IOException ex) {
            Logger.getLogger(ProjectSpecialAuthorityTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @AfterClass
    public static void  cleanupDependencies()
    {
        SpecialAuthorityClient refspecialauthorityclient = null;
        ProjectClient projclient = null;
        try {
            refspecialauthorityclient = new SpecialAuthorityClient();
            projclient = new ProjectClient();
        } catch (Exception ex) {
            Assert.fail();
        }

        // Clean up the Project
        try {
            projclient.deleteProject("nepa", TestData.projects.id1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectSpecialAuthorityTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Clean up the Reference Special Authority
        try {
            refspecialauthorityclient.deleteSpecialAuthority(TestData.specialauthorities.id1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectSpecialAuthorityTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }
}