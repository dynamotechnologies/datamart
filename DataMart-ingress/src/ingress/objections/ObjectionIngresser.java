package ingress.objections;

import client.ObjectionClient;
import client.ProjectDocumentClient;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.zip.DataFormatException;
import org.w3c.dom.Document;

public class ObjectionIngresser {
    private ObjectionClient client;
    private ProjectDocumentClient docclient;

    public void ingressObjections(Document doc)
            throws IOException, DataFormatException
    {
        client = new ObjectionClient();
        docclient = new ProjectDocumentClient();

        LegacyObjectionList objectionlist = new LegacyObjectionList(doc);

        String timestamp = objectionlist.getTimeStamp();
        Iterator it = objectionlist.getObjectionsIterator();
        while (it.hasNext())
        {
            LegacyObjection objection = (LegacyObjection) it.next();
            String code = objection.getCode();

            String projectname = objection.getProject().getName();
            String projecttype = "nepa";
            String projectid = objection.getProjectId();

            if (!(projectid.length()>0))
            {
                projecttype = "objection";
                projectid = objection.getId();
                ArrayList<String> purposelist = new ArrayList<String>();
                purposelist.add("NA");
                client.ProjectClient projectclient = new client.ProjectClient();
                try {
                    projectclient.getProject(projecttype, projectid);
                    // project already exists. update it
                    projectclient.putProject(projectid, projecttype, projectname, code, "n/a",
                        "1901-01-01T00:00:00", "1", "n/a", "n/a", "1", "NA", "0", "N/A", "N/A", "N/A", purposelist, "1970-1-1", null,
                        false, false, "", false, false);
                } catch (Exception ex) {
                    // project does not exist. create it.
                    projectclient.postProject(projectid, projecttype, projectname, code, "n/a",
                        "1901-01-01T00:00:00", "1", "n/a", "n/a", "1", "NA", "0", "N/A", "N/A", "N/A", purposelist, "1970-1-1", null,
                        false, false, "", false, false);
                }
            }

   //         String responsedate = LegacyUtil.getDateString(objection.getResponse().getDate());

            try {
                docclient.getProjectDocument(projecttype, projectid, objection.getResponse().getDocumentId());
                // doc exists; update it.
                docclient.putProjectDocument(projectid, projecttype, objection.getResponse().getDocumentId(), "1",
            "", "", "Administrative Review Response - "+objection.getObjector(), objection.getResponse().getFileSize(), "0");

            } catch (Exception ex) {
                docclient.postProjectDocument(projectid, projecttype, objection.getResponse().getDocumentId(), "1",
            "", objection.getResponse().getDocumentId(), "Administrative Review Response - "+objection.getObjector(), objection.getResponse().getFileSize(), "0");
            }

            try {
                client.getObjection(objection.getId());
                // objection exists. update it.

         //       client.putObjection(objection.getId(), projectid, projecttype, objection.getObjector(),
    //                    objection.getResponse().getDocumentId(), responsedate);
            } catch (Exception ex) {
                try {
     //               client.postObjection(objection.getId(), projectid, projecttype, objection.getObjector(),
     //                   objection.getResponse().getDocumentId(), responsedate);
                } catch (Exception ex2) {
      //              System.out.println("Objection POST failed.\nid="+objection.getId()+", projectid="+projectid+", projecttype"+projecttype+", objector="+objection.getObjector()+", docid="+objection.getResponse().getDocumentId()+", responsedate="+responsedate);
                }
            }
        }

//        System.out.println("complete!");
    }
}