package test.entity;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import test.TestData;

import client.ResourceAreaClient;
import us.fed.fs.www.nepa.schema.resourcearea.*;
import client.UnitClient;
import us.fed.fs.www.nepa.schema.unit.*;


public class ResourceAreaTest {
    private ResourceAreaClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new ResourceAreaClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postResourceArea(
                    TestData.resourceareas.id1,
                    TestData.resourceareas.testunitid,
                    TestData.resourceareas.description1
                    );
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Resourcearea resourcearea = null;
        try {
            resourcearea = client.getResourceArea(TestData.resourceareas.id1);
        } catch (IOException ex) {
            Logger.getLogger(ResourceAreaTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertTrue(TestData.resourceareas.id1 == resourcearea.getResourceareaid());
        Assert.assertEquals(TestData.resourceareas.description1, resourcearea.getDescription());

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        try {
            client.putResourceArea(
                    TestData.resourceareas.id1,
                    TestData.resourceareas.testunitid,
                    TestData.resourceareas.description2
                    );
        } catch (IOException ex) {
            Logger.getLogger(ResourceAreaTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        Resourcearea resourcearea = null;
        try {
            resourcearea = client.getResourceArea(TestData.resourceareas.id1);
        } catch (IOException ex) {
            Logger.getLogger(ResourceAreaTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        
        Assert.assertTrue(TestData.resourceareas.id1 == resourcearea.getResourceareaid());
        Assert.assertEquals(TestData.resourceareas.description2, resourcearea.getDescription());

        delete();
    }
    
    @Test
    public void testGetAll()
    {
        
        post();
        
        Resourceareas ras = null;
        try {
            ras = client.getResourceAreasByUnit(TestData.resourceareas.testunitid);
        } catch (IOException ex) {
            Logger.getLogger(ResourceAreaTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        
        Assert.assertTrue(ras.getResourcearea().size() == 1);
        
        Resourcearea ra = (Resourcearea)ras.getResourcearea().get(0);
        Assert.assertTrue(TestData.resourceareas.id1 == ra.getResourceareaid());
        Assert.assertEquals(TestData.resourceareas.description1, ra.getDescription());

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getResourceArea(TestData.resourceareas.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteResourceArea(TestData.resourceareas.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }

    private void post()
    {
        try {
            client.postResourceArea(
                    TestData.resourceareas.id1,
                    TestData.resourceareas.testunitid,
                    TestData.resourceareas.description1
                    );
        } catch (IOException ex) {
            Logger.getLogger(ResourceAreaTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteResourceArea(TestData.resourceareas.id1);
        } catch (IOException ex) {
            Logger.getLogger(ResourceAreaTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteResourceArea(TestData.resourceareas.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }

    
    @BeforeClass
    public static void populateDependencies() throws IOException
    {
         // Create test unit
         UnitClient unitclient = new UnitClient();
         unitclient.postUnit(TestData.resourceareas.testunitid, TestData.resourceareas.testunitname);
    }

    @AfterClass
    public static void cleanupDependencies() throws IOException
    {
         // Delete test unit
         UnitClient unitclient = new UnitClient();
         unitclient.deleteUnit(TestData.resourceareas.testunitid);
    }
}
