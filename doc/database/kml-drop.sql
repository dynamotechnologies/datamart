-- drop in reverse order of creation in order to avoid constraint violations

drop table if exists UnitKMLs;
drop table if exists ProjectLocationKMLs;
drop table if exists RefConfigs;
