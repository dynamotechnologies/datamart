package stateless;

import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.project.*;
import beanutil.DataMartException;
import us.fed.fs.www.nepa.schema.projectlisting.Projectlistings;


@Remote
public interface ProjectManagerRemote {
    public void create(Project resource) throws DataMartException;
    public Project get(String type, String id) throws DataMartException;
    public Projects getByUnit(String unitid, Boolean sopaonly) throws DataMartException;
    public Projects getActiveByUnit(String unitid, Boolean sopaonly) throws DataMartException;
    public Projectlistings getListingsByUnit(String unitid, Boolean sopaonly) throws DataMartException;
    public Projectlistings getSpotlightsByUnit(String unitid) throws DataMartException;
    public void update(Project resource) throws DataMartException;
    public void delete(String type, String id) throws DataMartException;
    public Projectsearchresults search(String name, String palsid, String [] unitlist, Integer [] statuslist, Boolean maponly,
            Boolean pubflag, Boolean archived, Integer start, Integer rows) throws DataMartException;
    public Boolean ping();
}