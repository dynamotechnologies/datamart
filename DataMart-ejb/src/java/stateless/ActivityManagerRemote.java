/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package stateless;
import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.activity.*;
import beanutil.DataMartException;

/**
 *
 * @author mhsu
 */
@Remote
public interface ActivityManagerRemote {
    public void create(Activity resource) throws DataMartException;
    public String getName(String id) throws DataMartException;
    public void update(Activity resource) throws DataMartException;
    public void delete(String id) throws DataMartException;
    public Activities getAll() throws DataMartException;
    public Boolean ping();
}