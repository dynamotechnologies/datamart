package test.relationship;

import client.CARAProjectClient;
import client.ProjectClient;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.project.*;
import us.fed.fs.www.nepa.schema.caraproject.*;

public class CaraProjectTest {
    private CARAProjectClient client;
    private ProjectClient projclient;

    @Before
    public void setUp()
            throws IOException
    {
         client = new CARAProjectClient();
         projclient = new ProjectClient();
    }


    @Test
    public void testLink()
    {
        link();

        // Try to relink the same CARA project
        try {
            // second link request should fail
            client.linkProject(TestData.projects.id1, TestData.caraprojects.id1);
            Assert.fail();
        } catch (Exception ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
            {
                Assert.fail();
            }
        }

        // Make sure the Project now has a <link> tag for the CARA project
        // Make sure the Project now has a <link> tag for the list of CARA phases
        try {
            Project p = projclient.getProject("nepa", TestData.projects.id1);
            if (findLink(p, "caraphaselist") == null) {
                Assert.fail("Missing CARA phases link in Project record!");
            }
            if (findLink(p, "caraproject") == null) {
                Assert.fail("Missing CARA project link in Project record");
            }
        } catch (IOException ex) {
            Logger.getLogger(CaraProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        unlink();
    }

    @Test
    public void testUpdate()
    {
        link();

        Caraproject cp = null;

        // Update CARA project readingroomactive flag
        // Flag should default to false
        try {
            cp = client.getProject(TestData.caraprojects.id1);
            if (cp.isReadingroomactive() == true) {
                Assert.fail("New CARA Project has unexpected active RR!");
            }
        } catch (Exception ex) {
            Logger.getLogger(CaraProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Update CARA project
        try {
            Caraproject newProject = cp;
            newProject.setReadingroomactive(true);
            client.putProject(newProject);
        } catch (Exception ex) {
            Logger.getLogger(CaraProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Flag should be true
        try {
            cp = null;
            cp = client.getProject(TestData.caraprojects.id1);
            if (cp.isReadingroomactive() != true) {
                Assert.fail("Updated CARA Project has wrong value for active RR!");
            }
        } catch (Exception ex) {
            Logger.getLogger(CaraProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Revert to previous state
        try {
            Caraproject oldProject = cp;
            oldProject.setReadingroomactive(false);
            client.putProject(oldProject);
        } catch (Exception ex) {
            Logger.getLogger(CaraProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Flag should be false
        try {
            cp = null;
            cp = client.getProject(TestData.caraprojects.id1);
            if (cp.isReadingroomactive() != false) {
                Assert.fail("Reverted CARA Project has wrong value for active RR!");
            }
        } catch (Exception ex) {
            Logger.getLogger(CaraProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        unlink();
   }

    @Test
    public void testReadingRoom()
    {
        link();

        // Activate the reading room
        try {
            client.activateRR(TestData.projects.id1);
        } catch (Exception ex) {
            Logger.getLogger(CaraProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail("Error setting RRactive to true");
        }

        // Confirm reading room active flag is ON
        try {
            Caraproject cp = client.getProject(TestData.caraprojects.id1);
            if (cp.isReadingroomactive() != true) {
                Assert.fail("Failed to set RRactive to true");
            }
        } catch (Exception ex) {
            Logger.getLogger(CaraProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Deactivate the reading room
        try {
            client.deactivateRR(TestData.projects.id1);
        } catch (Exception ex) {
            Logger.getLogger(CaraProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail("Error setting RRactive to false");
        }

        // Confirm reading room active flag is OFF
        try {
            Caraproject cp = client.getProject(TestData.caraprojects.id1);
            if (cp.isReadingroomactive() != false) {
                Assert.fail("Failed to set RRactive to false");
            }
        } catch (Exception ex) {
            Logger.getLogger(CaraProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        unlink();
    }

    @Test
    public void testUnlink()
    {
        link();
        unlink();

        // Try to unlink the same project again
        try {
            // second unlink request should fail
            client.unlinkProject(TestData.caraprojects.id1);
            Assert.fail();
        } catch (Exception ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
            {
                Assert.fail();
            }
        }

        // Make sure the project no longer has an CARA project <link> tag
        try {
            Project p = projclient.getProject("nepa", TestData.projects.id1);
            if (findLink(p, "caraproject") != null) {
                    Assert.fail("Unlinked Project has unexpected <link> tags!");
            }
        } catch (IOException ex) {
            Logger.getLogger(CaraProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    private void link()
    {
        // Link CARA project
        try {
            client.linkProject(TestData.projects.id1, TestData.caraprojects.id1);
        } catch (Exception ex) {
            Logger.getLogger(CaraProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void unlink()
    {
        // Unlink CARA project, if present
        try {
            client.unlinkProject(TestData.caraprojects.id1);
        } catch (Exception ex) {
            Logger.getLogger(CaraProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }
    
    /*
     *   Does this project have a link of the given type?
     *   Returns link if found
     *   Returns null if not found
     */
    private static Link findLink(Project p, String linktype) {
        if (p.getLinks() != null) {
            for (Link link : p.getLinks().getLink()) {
                if (link.getRel().matches(linktype)) {
                    return link;
                }
            }
        }
        return null;
    }

    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.unlinkProject(TestData.caraprojects.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }


    @BeforeClass
    public static void  populateDependencies()
    {
        ProjectClient pc = null;
        try {
            pc = new ProjectClient();
        } catch (Exception ex) {
            Logger.getLogger(CaraProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Project States rely on Projects. Pre-populate a Project.
        ArrayList<String> purposeids1 = new ArrayList<String>();
        purposeids1.add(TestData.projects.purposeid1);
        purposeids1.add(TestData.projects.purposeid2);
        purposeids1.add(TestData.projects.purposeid3);
        try {
            pc.postProject(TestData.projects.id1, TestData.projects.type1,
                    TestData.projects.name1, TestData.projects.unitcode1, TestData.projects.description1,
                    TestData.projects.lastupdate1, TestData.projects.commentreg1, TestData.projects.wwwlink1,
                    TestData.projects.wwwsummary1, TestData.projects.wwwpub1, TestData.projects.analysistypeid1,
                    TestData.projects.statusid1, TestData.projects.contactname1, TestData.projects.contactphone1,
                    TestData.projects.contactemail1, purposeids1, TestData.projects.expirationdate1, TestData.projects.projectdocumentid1,
                    TestData.projects.sopapub, TestData.projects.sopanew, TestData.projects.sopaheader, TestData.projects.esd, TestData.projects.cenodecision);
        } catch (IOException ex) {
            Logger.getLogger(CaraProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        try {
            Project p = pc.getProject(TestData.projects.type1, TestData.projects.id1);
            if (findLink(p, "caraproject") != null) {
                Assert.fail("Non-linked Project has unexpected <link> tags!");
            }
        } catch (IOException ex) {
            Logger.getLogger(CaraProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @AfterClass
    public static void  cleanupDependencies()
    {
        ProjectClient pc = null;
        try {
            pc = new ProjectClient();
        } catch (Exception ex) {
            Assert.fail();
        }

        // Clean up the Project
        try {
            pc.deleteProject("nepa", TestData.projects.id1);
        } catch (IOException ex) {
            Logger.getLogger(CaraProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }
}