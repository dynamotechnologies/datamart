package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.projectresourcearea.*;

import beanutil.*;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Iterator;
import java.util.List;

@Stateless(mappedName="ProjectResourceAreaManagerRemote")

public class ProjectResourceAreaManager implements ProjectResourceAreaManagerRemote {

    @PersistenceContext
    private EntityManager em;

    public void create(Projectresourcearea resource) throws DataMartException {
        entity.ProjectResourceAreas entity = createEntity(resource);

        entity.ProjectResourceAreas badentity = retrieveEntity(resource.getProjecttype(), resource.getProjectid(), resource.getResourceareaid());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        entity = em.merge(entity);
    }

    public Projectresourcearea get(String projecttype, String projectid, Integer resourceareaid)
            throws DataMartException {
        entity.ProjectResourceAreas entity = retrieveEntity(projecttype, projectid, resourceareaid);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return createResource(entity);
    }

    public Projectresourceareas getAllByProject(String projecttype, String projectid)
            throws DataMartException {
        // Get Project
        entity.Projects project = DataMartBeanUtil.getProject(em, projecttype, projectid);

        // Retrieve project resource area entities from the database
        Query query = em.createQuery("SELECT a FROM ProjectResourceAreas a WHERE a.projects = :project ORDER BY a.orderTag, a.id");
        query.setParameter("project", project);
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Projectresourceareas responselist = new Projectresourceareas();
        while (it.hasNext()) {
            responselist.getProjectresourcearea().add(createResource((entity.ProjectResourceAreas)it.next()));
        }
        return responselist;
    }

    public void update(Projectresourcearea resource) throws DataMartException {
        entity.ProjectResourceAreas entity = createEntity(resource);

        entity.ProjectResourceAreas existingentity = retrieveEntity(resource.getProjecttype(), resource.getProjectid(), resource.getResourceareaid());

        if (existingentity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        entity.setId(existingentity.getId());
        entity = em.merge(entity);
    }

    public void delete(String projecttype, String projectid, Integer resourceareaid) throws DataMartException {
        entity.ProjectResourceAreas entity = retrieveEntity(projecttype, projectid, resourceareaid);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));
    }

    public Boolean ping() {
        return true;
    }

    /*
     * Create XML from EJB
     */
    private Projectresourcearea createResource(entity.ProjectResourceAreas entity)
            throws DataMartException
    {
        Projectresourcearea resource = new Projectresourcearea();
        resource.setProjectid(entity.getProjects().getPublicId());
        resource.setProjecttype(entity.getProjects().getProjectTypes().getId());
        resource.setResourceareaid(entity.getResourceAreas().getId());
        resource.setOrder(entity.getOrderTag());
        return resource;
    }

    /*
     * Create EJB from XML
     */
    private entity.ProjectResourceAreas createEntity(Projectresourcearea resource)
            throws DataMartException
    {
        String projecttype = resource.getProjecttype();
        String projectid = resource.getProjectid();
        entity.Projects project = DataMartBeanUtil.getProject(em, projecttype, projectid);
        if (project == null) {
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION, "Project " + projecttype + "/" + projectid + " does not exist");
        }

        Integer resourceareaid = resource.getResourceareaid();
        entity.ResourceAreas resourcearea = em.find(entity.ResourceAreas.class, resourceareaid);
        if (resourcearea == null) {
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION, "Resource area " + resourceareaid + " does not exist");
        }

        entity.ProjectResourceAreas entity = new entity.ProjectResourceAreas();
        entity.setProjects(project);
        entity.setResourceAreas(resourcearea);
        entity.setOrderTag(resource.getOrder());
        return entity;
    }

    private entity.ProjectResourceAreas retrieveEntity(String projecttype, String projectid, Integer resourceareaid)
            throws DataMartException
    {
        // Get Project
        entity.Projects project = DataMartBeanUtil.getProject(em, projecttype, projectid);
        entity.ResourceAreas resourcearea = em.find(entity.ResourceAreas.class, resourceareaid);

        // Retrieve project resource area entities from the database
        Query query = em.createQuery("SELECT a FROM ProjectResourceAreas a WHERE a.projects = :project AND a.resourceAreas = :resourcearea");
        query.setParameter("project", project);
        query.setParameter("resourcearea", resourcearea);

        Object rec = DataMartBeanUtil.getEntityByQueryAllowNull(query, "Multiple rows found!");
        return (entity.ProjectResourceAreas)rec;
    }
}
