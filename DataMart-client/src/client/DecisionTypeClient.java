package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.decisiontype.*;


public class DecisionTypeClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public DecisionTypeClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.decisiontype", "DecisionType.xsd");
    }


    public void postDecisionType(String id, String desc, int palsid)
            throws IOException
    {
        Decisiontype atype = new Decisiontype();
        atype.setId(id);
        atype.setDescription(desc);
        atype.setPalsid(palsid);
        postDecisionType(atype);
    }


    public void postDecisionType(Decisiontype atype)
            throws IOException
    {
        post(objectfactory.createDecisiontype(atype), "/ref/decisiontypes");
    }


    public Decisiontype getDecisionType(String id)
            throws IOException
    {
        return get("/ref/decisiontypes/" + id, Decisiontype.class);
    }


    public Decisiontypes getDecisionTypeList()
            throws IOException
    {
        return get("/ref/decisiontypes", Decisiontypes.class);
    }


    public void putDecisionType(String id, String desc, int palsid)
            throws IOException
    {
        Decisiontype decisiontype = new Decisiontype();
        decisiontype.setId(id);
        decisiontype.setDescription(desc);
        decisiontype.setPalsid(palsid);
        putDecisionType(decisiontype);
    }


    public void putDecisionType(Decisiontype decisiontype)
            throws IOException
    {
        put(objectfactory.createDecisiontype(decisiontype), "/ref/decisiontypes/" + decisiontype.getId());
    }


    public void deleteDecisionType(String id)
            throws IOException
    {
        delete("/ref/decisiontypes/" + id);
    }
}