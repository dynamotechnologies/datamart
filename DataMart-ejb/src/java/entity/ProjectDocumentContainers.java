package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "ProjectDocumentContainers")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProjectDocumentContainers.findAll", query = "SELECT p FROM ProjectDocumentContainers p"),
    @NamedQuery(name = "ProjectDocumentContainers.findById", query = "SELECT p FROM ProjectDocumentContainers p WHERE p.id = :id"),
    @NamedQuery(name = "ProjectDocumentContainers.findByLabel", query = "SELECT p FROM ProjectDocumentContainers p WHERE p.label = :label"),
    @NamedQuery(name = "ProjectDocumentContainers.findByOrderTag", query = "SELECT p FROM ProjectDocumentContainers p WHERE p.orderTag = :orderTag"),
    @NamedQuery(name = "ProjectDocumentContainers.findByPalsContId", query = "SELECT p FROM ProjectDocumentContainers p WHERE p.palsContId = :palsContId")})
public class ProjectDocumentContainers implements Serializable {
    @Basic(optional = false)
    @Column(name = "FixedFlag")
    private boolean fixedFlag;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "Label")
    private String label;
    @Basic(optional = false)
    @Column(name = "OrderTag")
    private int orderTag;
    @Column(name = "PalsContId")
    private Integer palsContId;
    @JoinColumn(name = "ProjectId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Projects projects;
    @OneToMany(mappedBy = "projectDocumentContainers")
    private Collection<ProjectDocumentContainers> projectDocumentContainersCollection;
    @JoinColumn(name = "ContainerId", referencedColumnName = "Id")
    @ManyToOne
    private ProjectDocumentContainers projectDocumentContainers;
    @OneToMany(mappedBy = "projectDocumentContainers")
    private Collection<ProjectDocuments> projectDocumentsCollection;

    public ProjectDocumentContainers() {
    }

    public ProjectDocumentContainers(Integer id) {
        this.id = id;
    }

    public ProjectDocumentContainers(Integer id, String label, int orderTag) {
        this.id = id;
        this.label = label;
        this.orderTag = orderTag;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getOrderTag() {
        return orderTag;
    }

    public void setOrderTag(int orderTag) {
        this.orderTag = orderTag;
    }

    public Integer getPalsContId() {
        return palsContId;
    }

    public void setPalsContId(Integer palsContId) {
        this.palsContId = palsContId;
    }

    public Projects getProjects() {
        return projects;
    }

    public void setProjects(Projects projects) {
        this.projects = projects;
    }

    @XmlTransient
    public Collection<ProjectDocumentContainers> getProjectDocumentContainersCollection() {
        return projectDocumentContainersCollection;
    }

    public void setProjectDocumentContainersCollection(Collection<ProjectDocumentContainers> projectDocumentContainersCollection) {
        this.projectDocumentContainersCollection = projectDocumentContainersCollection;
    }

    public ProjectDocumentContainers getProjectDocumentContainers() {
        return projectDocumentContainers;
    }

    public void setProjectDocumentContainers(ProjectDocumentContainers projectDocumentContainers) {
        this.projectDocumentContainers = projectDocumentContainers;
    }

    @XmlTransient
    public Collection<ProjectDocuments> getProjectDocumentsCollection() {
        return projectDocumentsCollection;
    }

    public void setProjectDocumentsCollection(Collection<ProjectDocuments> projectDocumentsCollection) {
        this.projectDocumentsCollection = projectDocumentsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProjectDocumentContainers)) {
            return false;
        }
        ProjectDocumentContainers other = (ProjectDocumentContainers) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ProjectDocumentContainers[ id=" + id + " ]";
    }

    public boolean getFixedFlag() {
        return fixedFlag;
    }

    public void setFixedFlag(boolean fixedFlag) {
        this.fixedFlag = fixedFlag;
    }

}
