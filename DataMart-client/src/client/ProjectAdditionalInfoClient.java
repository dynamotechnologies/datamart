/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import us.fed.fs.www.nepa.schema.ProjectAdditionalInfo.*;

/**
 *
 * @author dmddeveloper
 */
public class ProjectAdditionalInfoClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public ProjectAdditionalInfoClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.ProjectAdditionalInfo", "ProjectAdditionalInfo.xsd");
    }


    public void postAdditionalInfo(Integer id, String projectid, String description,String updatedBy, String updatedDate)
            throws IOException
    {
        ProjectAdiitionalInfo projectAdiitionalInfo = new ProjectAdiitionalInfo();
        projectAdiitionalInfo.setAddInfoId(id.toString());
        projectAdiitionalInfo.setDescription(description);
        projectAdiitionalInfo.setProjectId(projectid);
        projectAdiitionalInfo.setLastUpdateBy(updatedBy);
        projectAdiitionalInfo.setLasteUpdateDate(updatedDate);
        postAdditionalInfo(projectAdiitionalInfo);
    }


    public void postAdditionalInfo(ProjectAdiitionalInfo projectAdiitionalInfo)
            throws IOException
    {
        post(objectfactory.createProjectAdiitionalInfo(projectAdiitionalInfo), "/projects/nepa/"+projectAdiitionalInfo.getProjectId()+"/projectAdditionainfo/");
    }


    public ProjectAdiitionalInfo getAdditionalInfo(String  projectId)
            throws IOException
    {
       
        ProjectAdiitionalInfos infos=   (ProjectAdiitionalInfos) get("/projects/nepa/"+projectId+"/projectAdditionainfo/", ProjectAdiitionalInfos.class);
        
        if(null !=infos && infos.getProjectAdiitionalInfo().size()>0 )
        return infos.getProjectAdiitionalInfo().get(0);
        else{
    
        return new ProjectAdiitionalInfo();  
        }
    }



    public void putAdditionalInfo(Integer id, String projectid, String description,String updatedBy, String updatedDate)
            throws IOException
    {
        ProjectAdiitionalInfo projectAdiitionalInfo = new ProjectAdiitionalInfo();
        projectAdiitionalInfo.setAddInfoId(id.toString());
        projectAdiitionalInfo.setDescription(description);
        projectAdiitionalInfo.setProjectId(projectid);
        projectAdiitionalInfo.setLastUpdateBy(updatedBy);
        projectAdiitionalInfo.setLasteUpdateDate(updatedDate);
        putAdditionalInfo(projectAdiitionalInfo);
    }


    public void putAdditionalInfo(ProjectAdiitionalInfo projectAdiitionalInfo)
            throws IOException
    {
        put(objectfactory.createProjectAdiitionalInfo(projectAdiitionalInfo), "/projects/nepa/"+projectAdiitionalInfo.getProjectId()+"/projectAdditionainfo/" + projectAdiitionalInfo.getAddInfoId());
    }


    public void deleteAdditionalInfo(String projectid,String addInfoId)
            throws IOException
    {
        delete("/projects/nepa/"+projectid+"/projectAdditionainfo/" + addInfoId);
    }
    
    
}