package stateless;

import javax.ejb.Stateless;
import beanutil.*;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import us.fed.fs.www.nepa.schema.milestone.*;


/**
 *
 * @author mhsu
 */
@Stateless(mappedName="MilestoneManagerRemote")
public class MilestoneManager implements MilestoneManagerRemote {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void create(Milestone resource)
             throws DataMartException
    {
        validate(resource);
        entity.RefMilestones entity = createEntity(resource);

        entity.RefMilestones badentity = createEntity(entity.getId());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        entity = em.merge(entity);
    }

    @Override
    public Milestones getAll()
             throws DataMartException
    {
        // Retrieve all entities from the database
        Query query = em.createQuery("SELECT u FROM RefMilestones u");
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Milestones responselist = new Milestones();
        while (it.hasNext()) {
            responselist.getMilestone().add(createResource( (entity.RefMilestones)it.next() ));
        }
        return responselist;
    }

    @Override
    public String getName(String id)
            throws DataMartException
    {
        entity.RefMilestones entity = createEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return entity.getName();
    }

    @Override
    public void update(Milestone resource)
             throws DataMartException
    {
        validate(resource);
        entity.RefMilestones entity = createEntity(resource);

        if (createEntity(entity.getId())==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        entity = em.merge(entity);
    }

    @Override
    public void delete(String id)
             throws DataMartException
    {
        entity.RefMilestones entity = createEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     *
     * This method does the mapping from jaxb object to entity object and vice versa
     */
    private Milestone createResource(entity.RefMilestones entity)
            throws DataMartException
    {
        Milestone resource = new Milestone();
        resource.setId(entity.getId());
        resource.setName(entity.getName());
        return resource;
    }

    private entity.RefMilestones createEntity(Milestone resource)
            throws DataMartException
    {
        entity.RefMilestones entity = new entity.RefMilestones();
        entity.setId(resource.getId());
        entity.setName(resource.getName());
        return entity;
    }

    // This version of overloaded method createEntity() accepts a String ID as a parameter, and returns the
    //   associated entity, or null if it doesn't exist
    //
    private entity.RefMilestones createEntity(String id)
            throws DataMartException
    {
        return em.find(entity.RefMilestones.class, id);
    }

    private void validate(Milestone resource)
            throws DataMartException
    {
        DataMartBeanUtil.confirmNotNull(resource.getName(), "Name");
    }

}
