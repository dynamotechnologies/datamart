package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "Appeals")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Appeals.findAll", query = "SELECT a FROM Appeals a"),
    @NamedQuery(name = "Appeals.findById", query = "SELECT a FROM Appeals a WHERE a.id = :id"),
    @NamedQuery(name = "Appeals.findByAppellant", query = "SELECT a FROM Appeals a WHERE a.appellant = :appellant"),
    @NamedQuery(name = "Appeals.findByResponseDate", query = "SELECT a FROM Appeals a WHERE a.responseDate = :responseDate")})
public class Appeals implements Serializable {
    @Basic(optional =     false)
    @Column(name = "ResponseDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date responseDate;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private String id;
    @Basic(optional = false)
    @Column(name = "Appellant")
    private String appellant;
    @JoinColumn(name = "RuleId", referencedColumnName = "Id")
    @ManyToOne
    private RefAppealRules refAppealRules;
    @JoinColumn(name = "ProjectId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Projects projects;
    @JoinColumn(name = "ProjectDocumentId", referencedColumnName = "Id")
    @ManyToOne
    private ProjectDocuments projectDocuments;
    @JoinColumn(name = "OutcomeId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private RefAppealOutcomes refAppealOutcomes;
    @JoinColumn(name = "DismissalReasonId", referencedColumnName = "Id")
    @ManyToOne
    private RefAppealDismissalReasons refAppealDismissalReasons;
    @JoinColumn(name = "DecisionId", referencedColumnName = "Id")
    @ManyToOne
    private Decisions decisions;
    @JoinColumn(name = "AppealStatusId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private RefAppealStatuses refAppealStatuses;
    @JoinColumn(name = "AdminUnit", referencedColumnName = "Id")
    @ManyToOne
    private RefUnits refUnits;

    public Appeals() {
    }

    public Appeals(String id) {
        this.id = id;
    }

    public Appeals(String id, String appellant, Date responseDate) {
        this.id = id;
        this.appellant = appellant;
        this.responseDate = responseDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAppellant() {
        return appellant;
    }

    public void setAppellant(String appellant) {
        this.appellant = appellant;
    }

    public RefAppealRules getRefAppealRules() {
        return refAppealRules;
    }

    public void setRefAppealRules(RefAppealRules refAppealRules) {
        this.refAppealRules = refAppealRules;
    }

    public Projects getProjects() {
        return projects;
    }

    public void setProjects(Projects projects) {
        this.projects = projects;
    }

    public ProjectDocuments getProjectDocuments() {
        return projectDocuments;
    }

    public void setProjectDocuments(ProjectDocuments projectDocuments) {
        this.projectDocuments = projectDocuments;
    }

    public RefAppealOutcomes getRefAppealOutcomes() {
        return refAppealOutcomes;
    }

    public void setRefAppealOutcomes(RefAppealOutcomes refAppealOutcomes) {
        this.refAppealOutcomes = refAppealOutcomes;
    }

    public RefAppealDismissalReasons getRefAppealDismissalReasons() {
        return refAppealDismissalReasons;
    }

    public void setRefAppealDismissalReasons(RefAppealDismissalReasons refAppealDismissalReasons) {
        this.refAppealDismissalReasons = refAppealDismissalReasons;
    }

    public Decisions getDecisions() {
        return decisions;
    }

    public void setDecisions(Decisions decisions) {
        this.decisions = decisions;
    }

    public RefAppealStatuses getRefAppealStatuses() {
        return refAppealStatuses;
    }

    public void setRefAppealStatuses(RefAppealStatuses refAppealStatuses) {
        this.refAppealStatuses = refAppealStatuses;
    }

    public RefUnits getRefUnits() {
        return refUnits;
    }

    public void setRefUnits(RefUnits refUnits) {
        this.refUnits = refUnits;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Appeals)) {
            return false;
        }
        Appeals other = (Appeals) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Appeals[ id=" + id + " ]";
    }

    public Date getResponseDate() {
        return responseDate;
    }

    public void setResponseDate(Date responseDate) {
        this.responseDate = responseDate;
    }

}
