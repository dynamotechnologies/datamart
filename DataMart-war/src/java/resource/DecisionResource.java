package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.DecisionManagerRemote;
import stateless.LitigationManagerRemote;
import us.fed.fs.www.nepa.schema.decision.Decision;
import us.fed.fs.www.nepa.schema.litigation.Litigations;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;


@Path ("/decisions")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class DecisionResource extends AbstractResource
{
    private DataMartUtil UTIL = null;
    private DecisionManagerRemote decisionmanager;
    private LitigationManagerRemote litigationmanager;
    int DECISION_ID;
    int LITIGATION_ID;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public DecisionResource()
            throws ServletException
    {
        UTIL = DataMartUtil.getInstance();
        if (UTIL==null)
            throw new ServletException("Horror! Could not get a reference to the Datamart Utility singleton! Exiting in shame.");

        Properties DECISION_PROPS = new Properties();
        DECISION_PROPS.setProperty("schema", "Decision.xsd");
        DECISION_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.decision");
        DECISION_PROPS.setProperty("bean", "DecisionManagerRemote");

        Properties LITIGATION_PROPS = new Properties();
        LITIGATION_PROPS.setProperty("schema", "Litigation.xsd");
        LITIGATION_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.litigation");
        LITIGATION_PROPS.setProperty("bean", "LitigationManagerRemote");
        try {
            DECISION_ID = UTIL.getResourceId(DECISION_PROPS);
            LITIGATION_ID = UTIL.getResourceId(LITIGATION_PROPS);
            decisionmanager = (DecisionManagerRemote) UTIL.getManager(DECISION_ID);
            litigationmanager = (LitigationManagerRemote) UTIL.getManager(LITIGATION_ID);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("DecisionResource failed to start. Could not start the bean manager.");
        }
        if (!( decisionmanager.ping() && litigationmanager.ping() ))
            throw new ServletException("DecisionResource failed to start. Could not ping the bean managers!");
    }


    @POST
    public Response post(InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;
        Decision decision;
        try {
            decision = (Decision)unmarshal(UTIL, DECISION_ID, inputxml, Decision.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+decision.getId());

            // All clear. Now do the insert.
            decisionmanager.create(decision);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the newly created entity
        return created(uri);
    }


    @GET
    @Path("/{id}")
    public Response get(@PathParam("id") int id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the name of the specified entity
            Decision decision = decisionmanager.get(id);

            // Get the query results in a JAXBElement object, marshal and return
            us.fed.fs.www.nepa.schema.decision.ObjectFactory objectfactory = new us.fed.fs.www.nepa.schema.decision.ObjectFactory();
            JAXBElement<Decision> jaxbelement = objectfactory.createDecision(decision);
            JAXBContext jaxbcontext = UTIL.getContext(DECISION_ID);
            Marshaller marshaller = jaxbcontext.createMarshaller();
            marshaller.marshal(jaxbelement, sw);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @GET
    @Path("/{id}/litigations")
    public Response getLitigations(@PathParam("id") int id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the name of the specified entity
            Litigations litigations = litigationmanager.getAllByDecisionId(id);

            // Get the query results in a JAXBElement object, marshal and return
            us.fed.fs.www.nepa.schema.litigation.ObjectFactory objectfactory = new us.fed.fs.www.nepa.schema.litigation.ObjectFactory();
            JAXBElement<Litigations> jaxbelement = objectfactory.createLitigations(litigations);
            JAXBContext jaxbcontext = UTIL.getContext(LITIGATION_ID);
            Marshaller marshaller = jaxbcontext.createMarshaller();
            marshaller.marshal(jaxbelement, sw);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @PUT
    @Path("/{id}")
    public Response put(@PathParam("id") int id, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        Decision decision = null;
        try {
            decision = (Decision) unmarshal(UTIL, DECISION_ID, inputxml, Decision.class);

            if (id!=decision.getId())
                complainLoudly(new Exception("Id in uri must match id in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            decisionmanager.update(decision);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"/"+decision.getId()+"</location>");
    }


    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") int id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            decisionmanager.delete(id);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}