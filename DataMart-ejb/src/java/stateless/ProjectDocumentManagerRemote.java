package stateless;

import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.projectdocument.*;
import beanutil.DataMartException;


/*
 *
 * The Manager beans are responsible for:
 *   1) Implementing business logic and
 *   2) Translating front-end resources to back-end database entities.
 *
 * Often the resources will consist of data from numerous database tables. It is the manager's responsibility
 *   to ensure that these front-end resources can be accessed as though they were cut from a single piece of wood.
 *
 */
@Remote
public interface ProjectDocumentManagerRemote {
    public void create(Projectdocument resource) throws DataMartException;
    public Projectdocument get(String type, String projectid, String docid) throws DataMartException;
    public Projectdocuments getAll(String type, String projectid, Boolean pubflag, Boolean sensitiveflag,
            Boolean ignorecaraflag, Integer start, Integer rows, Integer viewId) throws DataMartException;
    public Projectdocuments getByDocument(String type, String docid) throws DataMartException;
    public void update(Projectdocument resource) throws DataMartException;
    public void delete(String type, String projectid, String docid) throws DataMartException;
     public Boolean ping();
}