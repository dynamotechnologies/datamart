package stateless;

import us.fed.fs.www.nepa.schema.projectactivity.*;

import beanutil.*;
import javax.ejb.Stateless;
import java.util.Collection;
import java.util.Iterator;
import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/*
 * Web API: public IDs and resources
 * Manager: convert public IDs and resources -> entity objects
 * Private methods: entity objects
 */

@Stateless(mappedName="ProjectActivityManagerRemote")
public class ProjectActivityManager implements ProjectActivityManagerRemote
{
    @PersistenceContext
    private EntityManager em;

    private String PROJECT_TYPE = "nepa";


    @Override
    public void merge(Projectactivity resource)
             throws DataMartException
    {
        entity.ProjectActivities entity = createEntity(resource);

        // Check for existing entity and grab the internal ID if it exists
        entity.ProjectActivities oldact = getEntity(resource.getProjectid(), entity.getRefActivities().getId());
        if (oldact!=null)
            entity.setId(oldact.getId());

        // Update the entity. Create a new one if it doesn't exist already
        entity = em.merge(entity);
    }


    @Override
    public void replaceAll(Projectactivities resource)
             throws DataMartException
    {
        // No merge needed if the resource is empty
        if (resource.getProjectactivity().isEmpty())
            return;

        // Delete old entities
        String projectid = resource.getProjectactivity().get(0).getProjectid(); // The resource code verifies that all projects are the same

        // Get a reference to the project
        entity.Projects project = DataMartBeanUtil.getProject(em, this.PROJECT_TYPE, projectid);
        if (project==null)
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION,
                    "Project with type '" + this.PROJECT_TYPE + "' and ID '" + projectid + "' does not exist.");

        // Wipe out old entities
        Query q = em.createQuery("DELETE FROM ProjectActivities p WHERE p.projects = :project");
        q.setParameter("project", project);
        q.executeUpdate();

        Iterator activityiterator = resource.getProjectactivity().iterator();
        while (activityiterator.hasNext()) {
            Projectactivity act = (Projectactivity) activityiterator.next();
            entity.ProjectActivities entity = createEntity(act);
            em.persist(entity);
        }
    }


    /*
     * Retrieve a project activity entity
     * There isn't any extra information located in the entity so actually
     * this function isn't very useful yet
     *
     * Note that this function gets called using public identifiers for the
     * project and activity, although the project type is assumed to be "nepa"
     */
    @Override
    public Projectactivity get(String projectid, String activityid)
            throws DataMartException
    {
        // Get project activity
        entity.ProjectActivities entity = getEntity(projectid, activityid);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        return createResource(entity);
    }


    /*
     * Get list of activities for this project
     */
    @Override
    public Projectactivities getByProject(String projectid) throws DataMartException
    {
        // Get project from public ID
        entity.Projects project = DataMartBeanUtil.getProject(em, this.PROJECT_TYPE, projectid);
        if (project==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Project with type '" + this.PROJECT_TYPE + "' and ID '" + projectid + "' does not exist.");

        Query query = em.createQuery("SELECT p FROM ProjectActivities p WHERE p.projects = :project");
        query.setParameter("project", project);

        @SuppressWarnings("unchecked")
        Collection<entity.ProjectActivities> entitylist = (Collection<entity.ProjectActivities>)query.getResultList();

        // Assume it's OK to have no activities

        // Loop over the query results and build a list of resources
        Projectactivities activitylist = new Projectactivities();
        Iterator it = entitylist.iterator();
        while (it.hasNext())
        {
            activitylist.getProjectactivity().add(createResource((entity.ProjectActivities)it.next()));
        }

        return activitylist;
    }


    @Override
    public void delete(String projectid, String activityid)
             throws DataMartException
    {
        // Verify that this entity exists
        entity.ProjectActivities entity = getEntity(projectid, activityid);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Could not delete nonexistent Activity '"+activityid+"' for Project '"+projectid+"'.");

        // Remove the entity itself
        em.remove(entity);
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     * This method does the mapping from entity object to jaxb object
     *
     * Note that the jaxb resource has less information than the entity
     */
    protected static Projectactivity createResource(entity.ProjectActivities entity)
            throws DataMartException
    {
        Projectactivity resource = new Projectactivity();
        resource.setProjectid(entity.getProjects().getPublicId());
        resource.setActivityid(entity.getRefActivities().getId());
        return resource;
    }


    /*
     * This method does the mapping from jaxb object to entity object
     */
    private entity.ProjectActivities createEntity(Projectactivity resource)
            throws DataMartException
    {
        String projectid = resource.getProjectid(); // Always reference the public ID in error messages

        // Get the project from the public project id
        entity.Projects project = DataMartBeanUtil.getProject(em, this.PROJECT_TYPE, projectid);
        if (project==null)
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION,
                    "Project with type 'nepa' and ID '"+projectid+"' does not exist.");

        // Get the Activity entity
        entity.RefActivities refactivity = (entity.RefActivities) DataMartBeanUtil.getEntityById(em,
                "RefActivities", resource.getActivityid(), "Activity");

        entity.ProjectActivities entity = new entity.ProjectActivities();
        entity.setProjects(project);
        entity.setRefActivities(refactivity);

        return entity;
    }

    /*
     * Retrieve a ProjectActivities entity
     */
    private entity.ProjectActivities getEntity(String projectid, String id)
            throws DataMartException
    {
        // Get the project from the public project id
        entity.Projects project = DataMartBeanUtil.getProject(em, this.PROJECT_TYPE, projectid);

        // Get the Activity entity
        entity.RefActivities refactivity = (entity.RefActivities) DataMartBeanUtil.getEntityById(em,
                "RefActivities", id, "Activity");
        
        // Get the ProjectActivities entity
        Query query = em.createQuery("SELECT p FROM ProjectActivities p WHERE p.projects = :project AND p.refActivities = :activity");
        query.setParameter("project", project);
        query.setParameter("activity", refactivity);

        entity.ProjectActivities activity = (entity.ProjectActivities) DataMartBeanUtil.getEntityByQueryAllowNull(query,
                "Project "+project.getPublicId()+" has more than one Activity with id '"+id+"'.");

        return activity;
    }
}