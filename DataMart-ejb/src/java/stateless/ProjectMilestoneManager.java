package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.projectmilestone.*;

import beanutil.*;
import javax.ejb.Stateless;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Stateless(mappedName="ProjectMilestoneManagerRemote")
public class ProjectMilestoneManager implements ProjectMilestoneManagerRemote
{
    private String PROJECT_TYPE = "nepa";
    @PersistenceContext
    private EntityManager em;

    @Override
    public void merge(Projectmilestone resource)
             throws DataMartException
    {
        entity.ProjectMilestones entity = createEntity(resource);

        // Check for existing entity and grab the internal ID if it exists
        Integer seqnum = DataMartBeanUtil.getIntFromString(resource.getSeqnum(), "MILESTONE_SEQNUM");
        entity.ProjectMilestones oldentity = getEntity(seqnum);
        if (oldentity != null)
            entity.setId(oldentity.getId());

        entity = em.merge(entity);
    }

    @Override
    public void replaceAll(Projectmilestones resource)
             throws DataMartException
    {
        // No merge needed if the resource is empty
        if (resource.getProjectmilestone().isEmpty())
            return;

        // Delete old ProjectMilestone entities
        String projectid = resource.getProjectmilestone().get(0).getProjectid(); // The resource code verifies that all projects are the same

        // Get a reference to the project
        entity.Projects project = DataMartBeanUtil.getProject(em, this.PROJECT_TYPE, projectid);
        if (project==null)
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION,
                    "Project with type '" + this.PROJECT_TYPE + "' and ID '" + projectid + "' does not exist.");

        // Wipe out old milestones
        Query q = em.createQuery("DELETE FROM ProjectMilestones p WHERE p.projects = :project");
        q.setParameter("project", project);
        q.executeUpdate();

        for (Projectmilestone stone : resource.getProjectmilestone()) {
            entity.ProjectMilestones entity = createEntity(stone);
            
            em.persist(entity); //persist is giving IllegalStateException Exception            
            
        }
    }


    @Override
    public Projectmilestone get(String projectid, String id)
            throws DataMartException
    {
        // NOTE: projectid does nothing here, because the sequence number (id) uniquely identifies the row
        Integer intId = DataMartBeanUtil.getIntFromString(id, "MILESTONE_SEQNUM");
        entity.ProjectMilestones entity = getEntity(intId);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        return createResource(entity);
    }


    @Override
    public Projectmilestones getAll(String projectid) throws DataMartException {
        // Get a reference to the project
        entity.Projects project = DataMartBeanUtil.getProject(em, this.PROJECT_TYPE, projectid);
        if (project==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Project with type '" + this.PROJECT_TYPE + "' and ID '" + projectid + "' does not exist.");

        Query query = em.createQuery("SELECT m FROM ProjectMilestones m WHERE m.projects = :project");
        query.setParameter("project", project);

        @SuppressWarnings("unchecked")
        Collection<entity.ProjectMilestones> entitylist = (Collection<entity.ProjectMilestones>)query.getResultList();

        if (!(entitylist.size()>0))
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        // Loop over the query results and build a list of resources
        Projectmilestones milestonelist = new Projectmilestones();
        Iterator it = entitylist.iterator();
        while (it.hasNext())
        {
            milestonelist.getProjectmilestone().add(createResource((entity.ProjectMilestones)it.next()));
        }

        return milestonelist;
    }


    @Override
    public Projectmilestones getByUnit(String unitid, Boolean sopaonly)
            throws DataMartException
    {
        // Confirm that this is a valid unit
        entity.RefUnits unit = (entity.RefUnits) DataMartBeanUtil.getEntityById(em,
                "RefUnits", unitid, "Unit");
        if (unit==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        
        /*
         * The goal here is to get a list of all entities associated with a project that is associated with the specified unit.
         * The unit association may be in either the project-forests or project-districts locations tables.
         * In the future it may also be necessary to check the regions table.
         */
        String sopaclause = (sopaonly) ? "p.sopaPublish = true and " : "";
        Query query1 = em.createQuery("SELECT m FROM ProjectMilestones m JOIN m.projects p JOIN p.projectForestsCollection f JOIN f.refUnits u WHERE " + sopaclause + "u.id LIKE :id");
        Query query2 = em.createQuery("SELECT m FROM ProjectMilestones m JOIN m.projects p JOIN p.projectDistrictsCollection f JOIN f.refUnits u WHERE " + sopaclause + "u.id LIKE :id");
        query1.setParameter("id", unitid+"%");
        query2.setParameter("id", unitid+"%");
        List entitylist1 =  query1.getResultList();
        List entitylist2 =  query2.getResultList();

        if (!(entitylist1.size()>0 || entitylist2.size()>0))
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        // Loop over the database collection. Each row becomes a new object in the response list
        Projectmilestones responselist = new Projectmilestones();
        for (Object e : entitylist1) {
            Projectmilestone m = createResource((entity.ProjectMilestones)e);
            responselist.getProjectmilestone().add(m);
        }
        for (Object e : entitylist2) {
            Projectmilestone m = createResource((entity.ProjectMilestones)e);
            // Remove duplicates from the two queries
            Boolean duped = false;
            for (Object dupe : entitylist1) {
                if ( ((entity.ProjectMilestones)dupe).getId() == ((entity.ProjectMilestones)e).getId() )
                    duped = true;
            }
            if (!duped) {
                responselist.getProjectmilestone().add(m);
            }
        }

        // Get nationwide and regionwide entities for the live sopa
        if (sopaonly) {
            Query query3 = em.createQuery("SELECT m FROM ProjectMilestones m JOIN m.projects p JOIN p.projectDistrictsCollection f JOIN f.refUnits u WHERE p.sopaPublish = true and " +
                "( u.id LIKE '11000000' OR u.id LIKE :regionwide )");
            String regionwide = unitid.substring(0, 4)+"0000";
            query3.setParameter("regionwide", regionwide);
            List entitylist3 = query3.getResultList();
            for (Object e : entitylist3) {
                // Remove duplicates from the two queries
                Boolean duped = false;
                for (Object dupe : entitylist1) {
                    if ( ((entity.ProjectMilestones)dupe).getId() == ((entity.ProjectMilestones)e).getId() )
                        duped = true;
                }
                for (Object dupe : entitylist2) {
                    if ( ((entity.ProjectMilestones)dupe).getId() == ((entity.ProjectMilestones)e).getId() )
                        duped = true;
                }
                if (!duped) {
                    responselist.getProjectmilestone().add(createResource((entity.ProjectMilestones)e));
                }
            }
        }

        return responselist;
    }


    @Override
    public void delete(String id)
             throws DataMartException
    {
        Integer intId = DataMartBeanUtil.getIntFromString(id, "MILESTONE_SEQNUM");

        // Verify that this entity exists
        entity.ProjectMilestones entity = getEntity(intId);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Could not delete nonexistent Project Milestone '"+id+"'.");

        // Remove the entity itself
        em.remove(entity);
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     *
     * This method does the mapping from entity object to jaxb object
     */
    protected static Projectmilestone createResource(entity.ProjectMilestones entity)
            throws DataMartException
    {
        Projectmilestone resource = new Projectmilestone();
        resource.setProjectid(entity.getProjects().getPublicId());
        resource.setSeqnum(entity.getId().toString());
        resource.setMilestonetype(entity.getRefMilestones().getId());
        String dateStr = entity.getMilestoneDate();
        
        if (dateStr.indexOf('\'') > 0)
            dateStr = dateStr.substring(0, dateStr.indexOf('\'') );
        if (dateStr.indexOf('T') > 0)
            dateStr = dateStr.substring(0, dateStr.indexOf('T') );
                
        resource.setDatestring(dateStr);
        resource.setStatus(Character.toString(entity.getMilestoneStatus()));
        resource.setHistory(Character.toString(entity.getHistory()));
        return resource;
    }


    /*
     *
     * This method does the mapping from jaxb object to entity object
     */
    private entity.ProjectMilestones createEntity(Projectmilestone resource)
            throws DataMartException
    {
        String projectid = resource.getProjectid(); // Always reference the public ID in error messages

        // Get a reference to the project
        entity.Projects project = DataMartBeanUtil.getProject(em, "nepa", projectid);
        if (project==null)
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION,
                    "Project with type 'nepa' and ID '"+projectid+"' does not exist.");

        Integer seqnum = DataMartBeanUtil.getIntFromString(resource.getSeqnum(), "MILESTONE_SEQNUM");
        String refmilestoneid = resource.getMilestonetype();
        char status = (DataMartBeanUtil.getBoolFromString(resource.getStatus(), "Status")) ? '1' : '0';
        char history = (DataMartBeanUtil.getBoolFromString(resource.getHistory(), "History")) ? '1' : '0';

        // Make sure the foreign keys are valid, and retrieve a jpa representation of the associated rows
        entity.RefMilestones milestone = (entity.RefMilestones) DataMartBeanUtil.getEntityById(em,
                "RefMilestones", refmilestoneid, "Milestone ID");

        entity.ProjectMilestones entity = new entity.ProjectMilestones();
        entity.setProjects(project);
        entity.setId(seqnum);
        entity.setRefMilestones(milestone);
        entity.setMilestoneDate(resource.getDatestring());
        entity.setMilestoneStatus(status);
        entity.setHistory(history);
        return entity;
    }

    // Accepts a String ID as a parameter, and returns the
    //   associated entity, or null if it doesn't exist
    //
    private entity.ProjectMilestones getEntity(Integer id)
            throws DataMartException
    {
        Query query = em.createNamedQuery("ProjectMilestones.findById");
        query.setParameter("id", id);
        entity.ProjectMilestones pms = (entity.ProjectMilestones) DataMartBeanUtil.getEntityByQueryAllowNull(query,
                "More than one Project Milestone found with id '"+id+"'.");
        return pms;
    }
}