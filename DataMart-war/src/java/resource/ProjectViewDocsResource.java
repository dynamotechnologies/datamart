/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.ProjectViewDocManagerRemote;
import us.fed.fs.www.nepa.schema.ProjectViewDoc.ObjectFactory;
import us.fed.fs.www.nepa.schema.ProjectViewDoc.ProjectViewDoc;
import us.fed.fs.www.nepa.schema.ProjectViewDoc.ProjectViewDocs;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;



@Path ("/projects/{projtype}/{projid}/views/{viewId}/docs")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class ProjectViewDocsResource extends AbstractResource
{
    private ProjectViewDocManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public ProjectViewDocsResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "ProjectViewDoc.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.ProjectViewDoc");
        MY_PROPS.setProperty("bean", "ProjectViewDocManagerRemote");
        try {
            beanmanager = (ProjectViewDocManagerRemote) init(ProjectViewDocManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("ProjectViewDocsResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("ProjectViewDocsResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(@PathParam("projtype") String projtype, @PathParam("projid") String projid, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;

        try {
            ProjectViewDoc projectViewDoc = (ProjectViewDoc)unmarshal(inputxml, ProjectViewDoc.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+projectViewDoc.getId());

           
        
            // All clear. Now do the insert.
            beanmanager.create(projectViewDoc);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the newly created entity
        return created(uri);
    }


    @GET
    @Path("/{viewDocId}")
    public Response get(@PathParam("projtype") String projtype, @PathParam("projid") String projid, @PathParam("viewId") Integer viewId, @PathParam("viewDocId") Integer viewDocId, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            ProjectViewDoc projectViewDoc = beanmanager.get(viewDocId);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<ProjectViewDoc> jaxbelement = objectfactory.createProjectViewDoc(projectViewDoc);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    /*
     *  Get list of all project Meetingnotices under this project
     */
    @GET
    public Response getAllByProjectView(@PathParam("projtype") String projtype, @PathParam("projid") String projid,@PathParam("viewId") Integer viewId,  @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the name of the specified unit
            ProjectViewDocs projectViewDoc = beanmanager.getAllByView(viewId);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<ProjectViewDocs> jaxbelement = objectfactory.createProjectViewDocs(projectViewDoc);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }

    @PUT
    @Path("/{viewDocId}")
    public Response put(@PathParam("projtype") String projtype, @PathParam("projid") String projid, @PathParam("viewId") Integer viewId,@PathParam("viewDocId") Integer viewDocId, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        ProjectViewDoc projectViewDoc = null;
        try {
            projectViewDoc = (ProjectViewDoc) unmarshal(inputxml, ProjectViewDoc.class);

          
           
            if (!viewId.toString().equals(projectViewDoc.getViewId()))
                complainLoudly(new Exception("View ID in uri must match ID in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);
           if (!viewDocId.toString().equals(projectViewDoc.getId()))
                complainLoudly(new Exception("View Doc ID in uri must match ID in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.update(projectViewDoc);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"</location>");
    }


    @DELETE
    @Path("/{viewDocId}")
    public Response delete(@PathParam("projtype") String projtype, @PathParam("projid") String projid, @PathParam("viewId") Integer viewId,@PathParam("viewDocId") Integer viewDocId,  @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(viewDocId);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}

