package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import entity.NepaCategoricalExclusions;
import us.fed.fs.www.nepa.schema.nepacategoricalexclusion.*;

import beanutil.*;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless(mappedName="NepaCategoricalExclusionManagerRemote")
public class NepaCategoricalExclusionManager implements NepaCategoricalExclusionManagerRemote
{
    @PersistenceContext
    private EntityManager em;


    @Override
    public void create(Nepacategoricalexclusion resource)
             throws DataMartException
    {
        entity.NepaCategoricalExclusions entity = createEntity(resource);
        
        // Check for existing entity with this ID
        NepaCategoricalExclusions oldentity = getEntity(resource.getProjectid(), resource.getCategoricalexclusionid());
        if (oldentity != null)
            entity.setId(oldentity.getId());

        em.merge(entity);
    }


    @Override
    public Nepacategoricalexclusion get(String projectid, int id)
            throws DataMartException
    {
        entity.NepaCategoricalExclusions entity = getEntity(projectid, id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "NepaCategoricalExclusion with id '"+id+"' does not exist.");

        return createResource(entity);
    }


    @Override
    public Nepacategoricalexclusions getList(String projectid)
            throws DataMartException
    {
        Nepacategoricalexclusions rval = new Nepacategoricalexclusions();

        // Make sure the foreign key is valid, and retrieve a jpa representation of the associated row
        entity.Projects project = DataMartBeanUtil.getProject(em, "nepa", projectid);
        if (project==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Project with type 'nepa' and id '"+projectid+"' does not exist.");

        Query query = em.createQuery("SELECT e FROM NepaCategoricalExclusions e WHERE e.projects = :project");
        query.setParameter("project", project);

        @SuppressWarnings("unchecked")
        List<entity.NepaCategoricalExclusions> entitylist = (List<entity.NepaCategoricalExclusions>) query.getResultList();

        if (entitylist==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "No NEPA Categorical Exclusions found for project with type 'nepa' and id '"+projectid+"'.");

        for (entity.NepaCategoricalExclusions nce: entitylist) {
            rval.getNepacategoricalexclusion().add(createResource(nce));
        }
        
        return rval;
    }


    @Override
    public void delete(String projectid, int id)
             throws DataMartException
    {
        // Verify that this entity exists
        entity.NepaCategoricalExclusions entity = getEntity(projectid, id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Could not delete nonexistent NepaCategoricalExclusion with id '"+id+"'.");

        em.remove(entity);
    }

    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     *
     * This method does the mapping from entity object to jaxb object
     */
    protected static Nepacategoricalexclusion createResource(entity.NepaCategoricalExclusions entity)
            throws DataMartException
    {
        Nepacategoricalexclusion resource = new Nepacategoricalexclusion();
        resource.setCategoricalexclusionid(entity.getRefCategoricalExclusions().getId());
        resource.setProjectid(entity.getProjects().getPublicId());

        return resource;
    }

    /*
     *
     * This method does the mapping from jaxb object to entity object
     */
    private entity.NepaCategoricalExclusions createEntity(Nepacategoricalexclusion resource)
            throws DataMartException
    {
        entity.NepaCategoricalExclusions entity = new entity.NepaCategoricalExclusions();
        entity.Projects project = (entity.Projects)DataMartBeanUtil.getProject(em, "nepa", resource.getProjectid());
        entity.setProjects(project);
        entity.RefCategoricalExclusions exclusion = (entity.RefCategoricalExclusions)DataMartBeanUtil.getEntityById(em, "RefCategoricalExclusions", resource.getCategoricalexclusionid(), "CategoricalExclusion");
        entity.setRefCategoricalExclusions(exclusion);

        return entity;
    }


    // Returns the associated entity,
    //   or null if it doesn't exist
    private entity.NepaCategoricalExclusions getEntity(String projectid, int id)
            throws DataMartException
    {
        // Make sure the foreign key is valid, and retrieve a jpa representation of the associated row
        entity.Projects project = DataMartBeanUtil.getProject(em, "nepa", projectid);
        if (project==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Project with type 'nepa' and id '"+projectid+"' does not exist.");
        
        entity.RefCategoricalExclusions catexclusion = (entity.RefCategoricalExclusions) DataMartBeanUtil.getEntityById(em,
                "RefCategoricalExclusions", id, "Reference Categorical Exclusion");

        Query query = em.createQuery("SELECT e FROM NepaCategoricalExclusions e WHERE e.projects = :project and e.refCategoricalExclusions = :catexclusion");
        query.setParameter("project", project);
        query.setParameter("catexclusion", catexclusion);
        return (entity.NepaCategoricalExclusions) DataMartBeanUtil.getEntityByQueryAllowNull(query,
                "More than one NepaCategoricalExclusions found with Project ID '"+projectid+"' and id '"+id+"'");
    }
}