package stateless;
import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.mlmproject.*;
import beanutil.DataMartException;

@Remote
public interface MLMProjectManagerRemote {
    public void create(Mlmproject resource) throws DataMartException;
    public Mlmproject get(String mlmid) throws DataMartException;
    public Mlmprojects getAll() throws DataMartException;
    public Mlmprojects getAllByUnit(String unitid) throws DataMartException;
    public Mlmprojects getAllByForestmatch(String forestid) throws DataMartException;
    public void delete(String mlmid) throws DataMartException;
    public Boolean ping();
}

