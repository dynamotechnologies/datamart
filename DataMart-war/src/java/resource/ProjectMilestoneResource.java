package resource;

import beanutil.DataMartException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.xml.bind.JAXBException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;

import lombok.extern.slf4j.Slf4j;
import stateless.ProjectMilestoneManagerRemote;
import us.fed.fs.www.nepa.schema.projectmilestone.*;


@Path ("/projects/nepa/{projectid}/milestones")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class ProjectMilestoneResource extends AbstractResource
{
    private ProjectMilestoneManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public ProjectMilestoneResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "ProjectMilestone.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.projectmilestone");
        MY_PROPS.setProperty("bean", "ProjectMilestoneManagerRemote");
        try {
            beanmanager = (ProjectMilestoneManagerRemote) init(ProjectMilestoneManagerRemote.class);
        } catch (IOException ex) {
            log.error("ProjectMilestoneResource failed to start. Could not start the bean manager.", ex);
            throw new ServletException("ProjectMilestoneResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("ProjectMilestoneResource failed to start. Could not ping the bean manager!");
    }


    @PUT
    public Response put(@PathParam("projectid") String projectid, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        Projectmilestones milestones = null;
        try {
            milestones = (Projectmilestones) unmarshal(inputxml, Projectmilestones.class);
            // loop through all of the projectmilestone objects, and confirm that all project id's match the one in the url
            for (Projectmilestone stone : milestones.getProjectmilestone()) {
                if (!projectid.equalsIgnoreCase(stone.getProjectid()))
                    complainLoudly(new Exception("Project Id in uri must match Project Id in xml. Failed on milestone with seqnum " + stone.getSeqnum() + "."),
                            Response.Status.UNSUPPORTED_MEDIA_TYPE);
            } 

            // All clear. Now do the update.
            beanmanager.replaceAll(milestones);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"</location>");
    }


    @PUT
    @Path("/{id}")
    public Response put(@PathParam("projectid") String projectid, @PathParam("id") String id, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        Projectmilestone milestone = null;
        try {
            milestone = (Projectmilestone) unmarshal(inputxml, Projectmilestone.class);

            if (!projectid.equalsIgnoreCase(milestone.getProjectid()))
                complainLoudly(new Exception("Project Id in uri must match Project Id in xml."),
                        Response.Status.UNSUPPORTED_MEDIA_TYPE);

            if (!id.equalsIgnoreCase(milestone.getSeqnum()))
                complainLoudly(new Exception("Seqnum in uri must match Seqnum in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.merge(milestone);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"/"+milestone.getSeqnum()+"</location>");
    }


    @GET
    public Response get(@PathParam("projectid") String projectid, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get a dump of all the entities in the datamart
            Projectmilestones milestonelist = beanmanager.getAll(projectid);

            // Return 404 if none exist
            if (milestonelist.getProjectmilestone().isEmpty())
                notFound();  //todo: Return empty list, 404 causes problems for client apps

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Projectmilestones> jaxbelement = objectfactory.createProjectmilestones(milestonelist);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @GET
    @Path("/{id}")
    public Response get(@PathParam("projectid") String projectid, @PathParam("id") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the name of the specified entity
            Projectmilestone milestone = beanmanager.get(projectid, id);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Projectmilestone> jaxbelement = objectfactory.createProjectmilestone(milestone);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(id);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}