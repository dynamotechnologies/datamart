package test.reference;

import client.ApplicationClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.application.*;


public class ApplicationTest {
    private ApplicationClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new ApplicationClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postApplication(TestData.applications.id1, TestData.applications.name2);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Application application = null;
        Applications applicationlist = null;
        try {
            application = client.getApplication(TestData.applications.id1);
            applicationlist = client.getApplicationList();
        } catch (IOException ex) {
            Logger.getLogger(ApplicationTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.applications.id1, application.getId());
        Assert.assertEquals(TestData.applications.name1, application.getName());
        Assert.assertTrue(applicationlist.getApplication().get(0).getId().length()>0);
        Assert.assertTrue(applicationlist.getApplication().get(0).getName().length()>0);

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        Application application = null;
        try {
            client.putApplication(TestData.applications.id1, TestData.applications.name2);
            application = client.getApplication(TestData.applications.id1);
        } catch (Exception ex) {
            Logger.getLogger(ApplicationTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.applications.name2, application.getName());

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getApplication(TestData.applications.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteApplication(TestData.applications.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }

    private void post()
    {
        try {
            client.postApplication(TestData.applications.id1, TestData.applications.name1);
        } catch (IOException ex) {
            Logger.getLogger(ApplicationTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteApplication(TestData.applications.id1);
        } catch (IOException ex) {
            Logger.getLogger(ApplicationTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteApplication(TestData.applications.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }
}