package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.unitkml.*;


public class UnitKMLClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public UnitKMLClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.unitkml", "UnitKML.xsd");
    }


    public void postUnitKML(String filename, String label, String maptype, String unitid)
            throws IOException
    {
        Unitkml Unitkml = new Unitkml();

        Unitkml.setFilename(filename);
        Unitkml.setLabel(label);
        Unitkml.setMaptype(maptype);
        Unitkml.setUnitid(unitid);

        postUnitKML(Unitkml);
    }


    public void postUnitKML(Unitkml Unitkml)
            throws IOException
    {
        post(objectfactory.createUnitkml(Unitkml), "/unitkmls");
    }


    public Unitkml getUnitKML(String filename)
            throws IOException
    {
        return get("/unitkmls/" + filename, Unitkml.class);
    }


    public void putUnitKML(String filename, String label, String maptype, String unitid)
            throws IOException
    {
        Unitkml config = new Unitkml();

        config.setFilename(filename);
        config.setLabel(label);
        config.setMaptype(maptype);
        config.setUnitid(unitid);
        putUnitKML(config);
    }


    public void putUnitKML(Unitkml config)
            throws IOException
    {
        put(objectfactory.createUnitkml(config), "/unitkmls/" + config.getFilename());
    }


    public void deleteUnitKML(String filename)
            throws IOException
    {
        delete("/unitkmls/" + filename);
    }
}