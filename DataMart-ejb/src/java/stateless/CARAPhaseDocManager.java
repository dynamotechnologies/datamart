package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.caradocument.*;

import beanutil.*;
import entity.CARAPhaseDocs;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Stateless(mappedName="CARAPhaseDocManagerRemote")
public class CARAPhaseDocManager implements CARAPhaseDocManagerRemote
{
    @PersistenceContext
    private EntityManager em;

    private final String NEPA_TYPE = "nepa";

    @Override
    public void linkdoc(Caralinkdoc resource)
             throws DataMartException
    {
        entity.CARAPhaseDocs entity = createEntity(resource.getCaraid(), resource.getPhaseid(), resource.getDocid());
        entity.CARAPhaseDocs badentity = getEntity(resource.getCaraid(), resource.getPhaseid(), resource.getDocid());
        if (badentity != null)
        {
            throw new DataMartException(DataMartException.ALREADY_EXISTS);
        }
        entity = em.merge(entity);
    }


    @Override
    public void unlinkdoc(Caraunlinkdoc resource)
             throws DataMartException
    {
        entity.CARAPhaseDocs entity = getEntity(resource.getCaraid(), resource.getPhaseid(), resource.getDocid());
        if (entity == null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     * Use a link request resource to construct a CARAProject entity
     */
    private entity.CARAPhaseDocs createEntity(int caraId, int phaseId, String docId)
            throws DataMartException
    {
        entity.CARAPhaseDocs entity = new entity.CARAPhaseDocs();
        entity.Projects proj = DataMartBeanUtil.getCaraProject(em, caraId);
        if (proj == null) {
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND, "No such CARA project");
        }

        entity.ProjectDocuments projdoc = DataMartBeanUtil.getProjectDocument(em, proj, docId);
        if (projdoc == null) {
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND, "No such project document");
        }
        entity.setProjectDocuments(projdoc);

        entity.CommentPhases phase = DataMartBeanUtil.getCommentPhase(em, caraId, phaseId);
        if (phase == null) {
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND, "No such comment phase");
        }
        entity.setCommentPhases(phase);

        return entity;
    }

    /*
     * Find CARAProject record by CARA ID
     */
    private entity.CARAPhaseDocs getEntity(int caraId, int phaseId, String docId)
            throws DataMartException
    {
        String queryText =
                "SELECT d from CARAPhaseDocs d "
                + "WHERE d.commentPhases.cARAProjects.id = :caraid "
                + "AND d.commentPhases.phaseId = :phaseid "
                + "AND d.projectDocuments.documentId = :docid";
        Query query = em.createQuery(queryText);
        query.setParameter("caraid", caraId);
        query.setParameter("phaseid", phaseId);
        query.setParameter("docid", docId);

        try {
            return (entity.CARAPhaseDocs)query.getSingleResult();
        } catch (javax.persistence.NoResultException e) {
            return null;
        }

    }

    @Override
    public List getAll(Integer phaseid) throws DataMartException {
               String queryText =
	                 "SELECT d from CARAPhaseDocs d "
	                 + "WHERE  d.commentPhases.phaseId = :phaseid ";
	                
	         Query query = em.createQuery(queryText);
	        
	         query.setParameter("phaseid", phaseid); 
                 List caradocsList =query.getResultList();
                 
                 List caradocIdList= new ArrayList();
                 if(null !=caradocsList){
                    Iterator it = caradocsList.iterator();
                    while (it.hasNext()) {
                      CARAPhaseDocs doc = (CARAPhaseDocs)it.next();
                      caradocIdList.add(doc.getProjectDocuments().getDocumentId());
                    }
                 }
                 return caradocIdList;
    }
}