package stateless;

import javax.ejb.Stateless;
import beanutil.*;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import us.fed.fs.www.nepa.schema.county.*;


/**
 *
 * @author mhsu
 */
@Stateless(mappedName="CountyManagerRemote")
public class CountyManager implements CountyManagerRemote {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void create(County resource)
             throws DataMartException
    {
        validate(resource);
        entity.RefCounties entity = createEntity(resource);

        entity.RefCounties badentity = createEntity(entity.getId());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        entity = em.merge(entity);
    }

    @Override
    public Counties getAll()
             throws DataMartException
    {
        // Retrieve all entities from the database
        Query query = em.createQuery("SELECT u FROM RefCounties u");
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Counties responselist = new Counties();
        while (it.hasNext()) {
            responselist.getCounty().add(createResource( (entity.RefCounties)it.next() ));
        }
        return responselist;
    }

    @Override
    public String getName(String id)
            throws DataMartException
    {
        entity.RefCounties entity = createEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return entity.getName();
    }

    @Override
    public void update(County resource)
             throws DataMartException
    {
        validate(resource);
        entity.RefCounties entity = createEntity(resource);

        if (createEntity(entity.getId())==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        entity = em.merge(entity);
    }

    @Override
    public void delete(String id)
             throws DataMartException
    {
        entity.RefCounties entity = createEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     *
     * This method does the mapping from jaxb object to entity object and vice versa
     */
    private County createResource(entity.RefCounties entity)
            throws DataMartException
    {
        County resource = new County();
        resource.setId(entity.getId());
        resource.setName(entity.getName());
        return resource;
    }

    private entity.RefCounties createEntity(County resource)
            throws DataMartException
    {
        entity.RefCounties entity = new entity.RefCounties();
        entity.setId(resource.getId());
        entity.setName(resource.getName());
        return entity;
    }

    // This version of overloaded method createEntity() accepts a String ID as a parameter, and returns the
    //   associated entity, or null if it doesn't exist
    //
    private entity.RefCounties createEntity(String id)
            throws DataMartException
    {
        return em.find(entity.RefCounties.class, id);
    }

    private void validate(County resource)
            throws DataMartException
    {
        DataMartBeanUtil.confirmNotNull(resource.getName(), "Name");
    }

}
