package stateless;

import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.config.*;
import beanutil.DataMartException;

@Remote
public interface ConfigManagerRemote {
    public void create(Config resource) throws DataMartException;
    public Config get(String id) throws DataMartException;
    public Configs getAll() throws DataMartException;
    public void update(Config resource) throws DataMartException;
    public void delete(String id) throws DataMartException;
    public Boolean ping();
}