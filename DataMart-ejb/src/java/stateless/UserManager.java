package stateless;

import javax.ejb.Stateless;
import beanutil.*;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import us.fed.fs.www.nepa.schema.user.*;


/**
 *
 * @author mhsu
 */
@Stateless(mappedName="UserManagerRemote")
public class UserManager implements UserManagerRemote {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void create(User resource)
             throws DataMartException
    {
        validate(resource);
        entity.Users entity = createEntity(resource);

        entity.Users badentity = getEntity(entity.getId());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        entity = em.merge(entity);
    }

    @Override
    public Users getAll()
             throws DataMartException
    {
        // Retrieve all entities from the database
        Query query = em.createQuery("SELECT u FROM Users u");
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Users responselist = new Users();
        while (it.hasNext()) {
            responselist.getUser().add(createResource( (entity.Users)it.next() ));
        }
        return responselist;
    }

    @Override
    public User get(String shortname)
            throws DataMartException
    {
        entity.Users entity = getEntity(shortname);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return createResource(entity);
    }

    @Override
    public void update(User resource)
             throws DataMartException
    {
        validate(resource);
        entity.Users entity = createEntity(resource);

        if (getEntity(entity.getId())==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        entity = em.merge(entity);
    }

    @Override
    public void delete(String shortname)
             throws DataMartException
    {
        entity.Users entity = getEntity(shortname);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));
    }


    @Override
    public Usersearchresults search(String shortname, String lastname, String firstname, Integer start, Integer rows)
            throws DataMartException
    {
        Usersearchresults results = new Usersearchresults();
        String matchShortname = "%";
        String matchLastname = "%";
        String matchFirstname = "%";
        Integer matchStart = 0;
        Integer matchRows = 100;    // Documented default value

        if (shortname != null) {
            matchShortname = shortname + "%";
        }

        if (lastname != null) {
            matchLastname = lastname.toLowerCase() + "%";   // case-insensitive
        }

        if (firstname != null) {
            matchFirstname = firstname.toLowerCase() + "%"; // case-insensitive
        }

        if (start != null) {
            matchStart = start;
            if (matchStart < 0) {
                matchStart = 0;
            }
        }

        if (rows != null) {
            matchRows = rows;
            if (matchRows < 0) {
                matchRows = 0;
            }
        }
        
        Query query = em.createQuery("SELECT u FROM Users u WHERE ((u.id LIKE :shortname) AND (lower(u.lastName) LIKE :lastname) AND (lower(u.firstName) LIKE :firstname)) ORDER BY u.lastName, u.firstName, u.id");
        query.setParameter("shortname", matchShortname);
        query.setParameter("lastname", matchLastname);
        query.setParameter("firstname", matchFirstname);
        query.setFirstResult(matchStart);
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Users responselist = new Users();
        while (it.hasNext()) {
            responselist.getUser().add(createResource( (entity.Users)it.next() ));
            if (responselist.getUser().size() >= matchRows) {
                break;
            }
        }

        Usersearchresultmetadata metadata = new Usersearchresultmetadata();
        metadata.setRequest("Request string here");
        metadata.setRowcount(responselist.getUser().size());
        metadata.setTotalcount(entitylist.size() + matchStart); // wrong if start > size of full set
        results.setResultmetadata(metadata);
        results.setUsers(responselist);

        return results;
    }

    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     * This method does the mapping from jaxb object to entity object and vice versa
     */
    private User createResource(entity.Users entity)
            throws DataMartException
    {
        User resource = new User();
        resource.setShortname(entity.getId());
        resource.setFirstname(entity.getFirstName());
        resource.setLastname(entity.getLastName());
        resource.setPhone(entity.getPhoneNumber());
        resource.setTitle(entity.getTitle());
        resource.setEmail(entity.getEmail());

        // Retrieve and set UnitRoles for user
        Userunitroles unitroles = new Userunitroles();
        Query query = em.createQuery("SELECT r FROM UnitRoles r WHERE r.users.id = :shortname");
        query.setParameter("shortname", entity.getId());
        List roles = query.getResultList();
        Iterator it = roles.iterator();
        while (it.hasNext()) {
            Userunitrole roleRsrc = new Userunitrole();
            entity.UnitRoles role = (entity.UnitRoles)it.next();
            
            //roleRsrc.setId(role.getId()); // ID is the public Access Group ID
            roleRsrc.setRole(role.getRefRoles().getId());
            //roleRsrc.setShortname(role.getUsers().getId());
            roleRsrc.setUnit(role.getRefUnits().getId());
            unitroles.getUnitrole().add(roleRsrc);
        }
        resource.setUnitroles(unitroles);

        return resource;
    }

    private entity.Users createEntity(User resource)
            throws DataMartException
    {
        entity.Users entity = new entity.Users();
        entity.setId(resource.getShortname());
        entity.setFirstName(resource.getFirstname());
        entity.setLastName(resource.getLastname());
        entity.setPhoneNumber(resource.getPhone());
        entity.setTitle(resource.getTitle());
        entity.setEmail(resource.getEmail());

        // Do not assign user unit roles from the User manager
        // This is inconsistent with other behavior but roles are only assigned
        // through an administrative process so the <unitroles> structure in the
        // <user> schema is effectively read-only

        return entity;
    }

    // Accepts a String ID as a parameter, and returns the
    //   associated entity, or null if it doesn't exist
    //
    private entity.Users getEntity(String id)
            throws DataMartException
    {
        return em.find(entity.Users.class, id);
    }

    private void validate(User resource)
            throws DataMartException
    {
        // No enforceable validations known at this time
        // DataMartBeanUtil.confirmNotNull(resource.getLastname(), "Last Name");
    }

}