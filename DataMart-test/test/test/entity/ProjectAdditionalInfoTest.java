/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.entity;

import client.ProjectAdditionalInfoClient;

import java.io.IOException;

import java.util.ArrayList;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.ProjectAdditionalInfo.ProjectAdiitionalInfo;


/**
 *
 * @author gauri
 */

public class ProjectAdditionalInfoTest {
    private ProjectAdditionalInfoClient client;
    

    @Before
    public void setUp()
            throws IOException
    {
        client = new ProjectAdditionalInfoClient();
       
    }

    @Test
    public void testPost()
    {
        // This POST should fail, because it violates a constraint. The unit code is bad
        try {
           addAddtionalInfo();
            // If the above statement did not throw an exception then something has gone terribly wrong
            //Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.BAD_PARENTS)
                Assert.fail();
        } catch (Exception ex) {
            Logger.getLogger(ProjectAdditionalInfoTest.class.getName()).log(Level.SEVERE, null, ex);
             Assert.fail();
        }

        

        
    }

    @Test
    public void testGet()
    {
     

        ProjectAdiitionalInfo project = null;
         
        try {
            project = client.getAdditionalInfo("21183");
            
            System.out.println(project.getDescription());
            System.out.println(project.getLasteUpdateDate());
            
        } catch (IOException ex) {
            Logger.getLogger(ProjectAdditionalInfoTest.class.getName()).log(Level.SEVERE, null, ex);
             Assert.fail();
        }
            
          
       

       
     

       
    }

    @Test
    public void testPut()
    {
        

        ProjectAdiitionalInfo project = null;
        try {
            modifyAddtionalInfo();
            project = client.getAdditionalInfo("17735");
        } catch (Exception ex) {
            Logger.getLogger(ProjectAdditionalInfoTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

       
    }

   
    
 


    @Test
    public void testDelete()
    {
        

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            deleteAddtionalInfo();
          
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        } catch (Exception ex) {
            Logger.getLogger(ProjectAdditionalInfoTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
      
    }

 
    @After
    public void cleanFailed()
            throws Exception
    {
      
    }


    @BeforeClass
    public static void populateDependencies()
    {
        // No dependencies
    }

    @AfterClass
    public static void cleanupDependencies()
    {
        // No cleanup
    }
    
    public static void addAddtionalInfo() throws Exception{
	    	
	    	client.ProjectAdditionalInfoClient client = new client.ProjectAdditionalInfoClient();
	    	us.fed.fs.www.nepa.schema.ProjectAdditionalInfo.ProjectAdiitionalInfo p = new us.fed.fs.www.nepa.schema.ProjectAdditionalInfo.ProjectAdiitionalInfo();
	    	p.setAddInfoId("2");
                p.setDescription("test2");
                p.setLastUpdateBy("oshroff");
                p.setLasteUpdateDate("2016-07-07");
                p.setProjectId("39356");
	    	client.postAdditionalInfo(p);
	    }
    
	 public static void deleteAddtionalInfo() throws Exception{
	    	
	    	client.ProjectAdditionalInfoClient client = new client.ProjectAdditionalInfoClient();
                client.deleteAdditionalInfo("17735","2");
         }   
         
          public static void modifyAddtionalInfo() throws Exception{
	    	client.ProjectAdditionalInfoClient client = new client.ProjectAdditionalInfoClient();
  	        us.fed.fs.www.nepa.schema.ProjectAdditionalInfo.ProjectAdiitionalInfo p = new us.fed.fs.www.nepa.schema.ProjectAdditionalInfo.ProjectAdiitionalInfo();
	    	p.setAddInfoId("1");
                p.setDescription("test3");
                p.setLastUpdateBy("oshroff");
                p.setProjectId("17735");
                p.setLasteUpdateDate("2016-07-08");
	    	client.putAdditionalInfo(p);
         }   
           public static void main(String[] args){
	        System.out.println("start");
                try {
                    addAddtionalInfo();
                } catch (Exception ex) {
                    Logger.getLogger(ProjectAdditionalInfoClient.class.getName()).log(Level.SEVERE, null, ex);
                }
           }
}
