package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.ProjectManagerRemote;
import stateless.UserManagerRemote;
import us.fed.fs.www.nepa.schema.project.Projectsearchresults;
import us.fed.fs.www.nepa.schema.user.Usersearchresults;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Properties;

@Path ("/utils")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class UtilsResource extends AbstractResource
{
    private DataMartUtil UTIL = null;
    private ProjectManagerRemote projectmanager;
    private UserManagerRemote usermanager;
    int PROJECT_ID;
    int USER_ID;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public UtilsResource()
            throws ServletException
    {
        UTIL = DataMartUtil.getInstance();
        if (UTIL==null)
            throw new ServletException("Horror! Could not get a reference to the Datamart Utility singleton! Exiting in shame.");

        Properties PROJECT_PROPS = new Properties();
        PROJECT_PROPS.setProperty("schema", "Project.xsd");
        PROJECT_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.project");
        PROJECT_PROPS.setProperty("bean", "ProjectManagerRemote");

        Properties USER_PROPS = new Properties();
        USER_PROPS.setProperty("schema", "User.xsd");
        USER_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.user");
        USER_PROPS.setProperty("bean", "UserManagerRemote");
        try {
            PROJECT_ID = UTIL.getResourceId(PROJECT_PROPS);
            USER_ID = UTIL.getResourceId(USER_PROPS);
            projectmanager = (ProjectManagerRemote) UTIL.getManager(PROJECT_ID);
            usermanager = (UserManagerRemote) UTIL.getManager(USER_ID);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("UserResource failed to start. Could not start the bean manager.");
        }

        if (!( projectmanager.ping() && usermanager.ping() ))
            throw new ServletException("UtilsResource failed to start. Could not ping the bean managers!");
    }


    @GET
    @Path("/mapprojects")
    public Response getMapProjects(@QueryParam("pubflag") Integer pubflagval,
                @QueryParam("archiveflag") Integer archiveflagval,
                @QueryParam("rows") Integer rows,
                @QueryParam("start") Integer start,
                @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            Boolean pubflag = getBooleanFromInt("pubflag", pubflagval);
            Boolean archiveflag = getBooleanFromInt("archiveflag", archiveflagval);

            Integer resultRows = 100000;   // TODO: Make this a config
            if (rows != null) {
                resultRows = rows;
            }

            Integer resultStart = 0;
            if (start != null) {
                resultStart = start;
            }

            Projectsearchresults results = projectmanager.search(null, null, null, null, true, pubflag, archiveflag, resultStart, resultRows);

            // Get the query results in a JAXBElement object, marshal and return
            us.fed.fs.www.nepa.schema.project.ObjectFactory objectfactory = new us.fed.fs.www.nepa.schema.project.ObjectFactory();
            JAXBElement<Projectsearchresults> jaxbelement = objectfactory.createProjectsearchresults(results);
            JAXBContext jaxbcontext = UTIL.getContext(PROJECT_ID);
            Marshaller marshaller = jaxbcontext.createMarshaller();
            marshaller.marshal(jaxbelement, sw);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @GET
    @Path("/findUser")
    public Response get(@QueryParam("lastname") String lastname,
            @QueryParam("firstname") String firstname,
            @QueryParam("shortname") String shortname,
            @QueryParam("rows") Integer rows,
            @QueryParam("start") Integer start,
            @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();

        Integer resultRows = 100;   // TODO: Make this a config
        if (rows != null) {
            resultRows = rows;
        }

        Integer resultStart = 0;
        if (start != null) {
            resultStart = start;
        }

        try {
            // Retrieve the specified user
            Usersearchresults results = usermanager.search(fixWildcards(shortname), fixWildcards(lastname), fixWildcards(firstname), resultStart, resultRows);

            results.getResultmetadata().setRequest(uriInfo.getRequestUri().toString());

            // Get the query results in a JAXBElement object, marshal and return
            us.fed.fs.www.nepa.schema.user.ObjectFactory objectfactory = new us.fed.fs.www.nepa.schema.user.ObjectFactory();
            JAXBElement<Usersearchresults> jaxbelement = objectfactory.createUsersearchresults(results);
            JAXBContext jaxbcontext = UTIL.getContext(USER_ID);
            Marshaller marshaller = jaxbcontext.createMarshaller();
            marshaller.marshal(jaxbelement, sw);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }

    /*
     * Make asterisk the match-all wildcard
     * Replace JPQL wildcards with escaped versions of same
     *
     * Note that literal backslashes need to be paired for Java strings.
     * Also: don't use String.replaceAll unless you desperately want regex substitution
     */
    private String fixWildcards(String src) {
        if (src == null) return src;

        String result = src;
        result = result.replace("%", "\\%");
        result = result.replace("_", "\\_");
        result = result.replace("*", "%");

        return result;
    }
}
