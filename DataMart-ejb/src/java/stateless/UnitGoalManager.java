package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.unitgoal.*;

import beanutil.*;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Iterator;
import java.util.List;

@Stateless(mappedName="UnitGoalManagerRemote")

public class UnitGoalManager implements UnitGoalManagerRemote {

    @PersistenceContext
    private EntityManager em;

    public void create(Unitgoal resource) throws DataMartException {
        entity.UnitGoals entity = createEntity(resource);

        entity.UnitGoals badentity = retrieveEntity(resource.getUnitid(), resource.getGoalid());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        entity = em.merge(entity);
    }

    public Unitgoal get(String unitid, Integer goalid)
            throws DataMartException {
        entity.UnitGoals entity = retrieveEntity(unitid, goalid);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return createResource(entity);
    }

    public Unitgoals getAllByUnit(String unitid)
            throws DataMartException {
        // Get Unit
        entity.RefUnits unit = em.find(entity.RefUnits.class, unitid);

        // Retrieve unit resource area entities from the database
        Query query = em.createQuery("SELECT a FROM UnitGoals a WHERE a.refUnits = :unit ORDER BY a.orderTag, a.id");
        query.setParameter("unit", unit);
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Unitgoals responselist = new Unitgoals();
        while (it.hasNext()) {
            responselist.getUnitgoal().add(createResource((entity.UnitGoals)it.next()));
        }
        return responselist;
    }

    public void update(Unitgoal resource) throws DataMartException {
        entity.UnitGoals entity = createEntity(resource);

        entity.UnitGoals existingentity = retrieveEntity(resource.getUnitid(), resource.getGoalid());

        if (existingentity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        entity.setId(existingentity.getId());
        entity = em.merge(entity);
    }

    public void delete(String unitid, Integer goalid) throws DataMartException {
        entity.UnitGoals entity = retrieveEntity(unitid, goalid);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));
    }

    public Boolean ping() {
        return true;
    }

    /*
     * Create XML from EJB
     */
    private Unitgoal createResource(entity.UnitGoals entity)
            throws DataMartException
    {
        Unitgoal resource = new Unitgoal();
        resource.setUnitid(entity.getRefUnits().getId());
        resource.setGoalid(entity.getGoals().getId());
        resource.setOrder(entity.getOrderTag());
        return resource;
    }

    /*
     * Create EJB from XML
     */
    private entity.UnitGoals createEntity(Unitgoal resource)
            throws DataMartException
    {
        String unitid = resource.getUnitid();
        entity.RefUnits unit = em.find(entity.RefUnits.class, unitid);
        if (unit == null) {
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION, "Unit " + unitid + " does not exist");
        }

        Integer goalid = resource.getGoalid();
        entity.Goals goal = em.find(entity.Goals.class, goalid);
        if (goal == null) {
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION, "Goal " + goalid + " does not exist");
        }

        entity.UnitGoals entity = new entity.UnitGoals();
        entity.setRefUnits(unit);
        entity.setGoals(goal);
        entity.setOrderTag(resource.getOrder());
        return entity;
    }

    private entity.UnitGoals retrieveEntity(String unitid, Integer goalid)
            throws DataMartException
    {
        // Get Unit
        entity.RefUnits unit = em.find(entity.RefUnits.class, unitid);
        entity.Goals goal = em.find(entity.Goals.class, goalid);

        // Retrieve unit resource area entities from the database
        Query query = em.createQuery("SELECT a FROM UnitGoals a WHERE a.refUnits = :unit AND a.goals = :goal");
        query.setParameter("unit", unit);
        query.setParameter("goal", goal);

        Object rec = DataMartBeanUtil.getEntityByQueryAllowNull(query, "Multiple rows found!");
        return (entity.UnitGoals)rec;
    }
}
