package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.CustomProjectManagerRemote;
import us.fed.fs.www.nepa.schema.customproject.Customproject;
import us.fed.fs.www.nepa.schema.customproject.ObjectFactory;

import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;



@Path ("/customprojects")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class CustomProjectResource extends AbstractResource{
    
    private CustomProjectManagerRemote beanmanager;
    
    
    public CustomProjectResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "CustomProject.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.customproject");
        MY_PROPS.setProperty("bean", "CustomProjectManagerRemote");
        try {
            beanmanager = (CustomProjectManagerRemote) init(CustomProjectManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("CustomProjectResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("CustomProjectResource failed to start. Could not ping the bean manager!");
    }
    
    @POST
    public Response post(InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;
        Customproject cp;
        try {
            cp = (Customproject)unmarshal(inputxml, Customproject.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+cp.getId());

            // All clear. Now do the insert.
            beanmanager.create(cp);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        } catch (DataMartException ex) {
     //               complainLoudly(new Exception(ex.getCode()+" "+ex.getMessage()), Response.Status.INTERNAL_SERVER_ERROR);

            handleException(ex);
        } catch(Exception ex){
            log.error(ex.toString());
            complainLoudly(new Exception(ex.toString()), Response.Status.INTERNAL_SERVER_ERROR);
        }
        // Return a link to the newly created entity
        return created(uri);
    }
    

    
    @GET
    @Path("/{code}")
    public Response get(@PathParam("code") String code, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the name of the specified entity
            Customproject cp = beanmanager.get(code);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Customproject> jaxbelement = objectfactory.createCustomproject(cp);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }
    
    @PUT
    @Path("/{code}")
    public Response put(@PathParam("code") String code, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        Customproject cp = null;
        try {
            cp = (Customproject) unmarshal(inputxml, Customproject.class);

            if (!code.equalsIgnoreCase(cp.getId()))
                complainLoudly(new Exception("Code in uri must match code in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);


            
            // All clear. Now do the update.
            beanmanager.update(cp);
        }catch(Exception ex){
            log.error(ex.toString());
            complainLoudly(new Exception(ex.toString()), Response.Status.INTERNAL_SERVER_ERROR);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"/"+cp.getId()+"</location>");
    }
        
    @DELETE
    @Path("/{code}")
    public Response delete(@PathParam("code") String code, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
       try {
            beanmanager.delete(code);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();

    }
    

}
