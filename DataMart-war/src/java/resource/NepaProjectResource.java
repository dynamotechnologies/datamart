package resource;

import beanutil.DataMartBeanUtil;
import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import stateless.*;
import us.fed.fs.www.nepa.schema.appeal.Appeal;
import us.fed.fs.www.nepa.schema.appeal.Appeals;
import us.fed.fs.www.nepa.schema.decision.Decision;
import us.fed.fs.www.nepa.schema.decision.Decisions;
import us.fed.fs.www.nepa.schema.decisionappealrule.Decisionappealrule;
import us.fed.fs.www.nepa.schema.decisionappealrule.Decisionappealrules;
import us.fed.fs.www.nepa.schema.decisionmaker.Decisionmaker;
import us.fed.fs.www.nepa.schema.decisionmaker.Decisionmakers;
import us.fed.fs.www.nepa.schema.decisiontype.Decisiontype;
import us.fed.fs.www.nepa.schema.decisiontype.Decisiontypes;
import us.fed.fs.www.nepa.schema.litigation.Litigation;
import us.fed.fs.www.nepa.schema.litigation.Litigations;
import us.fed.fs.www.nepa.schema.nepacategoricalexclusion.Nepacategoricalexclusion;
import us.fed.fs.www.nepa.schema.nepacategoricalexclusion.Nepacategoricalexclusions;
import us.fed.fs.www.nepa.schema.project.Nepainfo;
import us.fed.fs.www.nepa.schema.project.Project;
import us.fed.fs.www.nepa.schema.project.Purposeids;
import us.fed.fs.www.nepa.schema.project.Sopainfo;
import us.fed.fs.www.nepa.schema.projectactivity.Projectactivities;
import us.fed.fs.www.nepa.schema.projectactivity.Projectactivity;
import us.fed.fs.www.nepa.schema.projectgoal.Projectgoal;
import us.fed.fs.www.nepa.schema.projectgoal.Projectgoals;
import us.fed.fs.www.nepa.schema.projectlocation.*;
import us.fed.fs.www.nepa.schema.projectmilestone.Projectmilestone;
import us.fed.fs.www.nepa.schema.projectmilestone.Projectmilestones;
import us.fed.fs.www.nepa.schema.projectresourcearea.Projectresourcearea;
import us.fed.fs.www.nepa.schema.projectresourcearea.Projectresourceareas;
import us.fed.fs.www.nepa.schema.projectspecialauthority.Projectspecialauthorities;
import us.fed.fs.www.nepa.schema.projectspecialauthority.Projectspecialauthority;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.*;


@Path ("/nepaprojects")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class NepaProjectResource extends AbstractResource
{
    private ProjectManagerRemote projectmanager;
    private ProjectLocationManagerRemote locationmanager;
    private ProjectActivityManagerRemote activitymanager;
    private ProjectMilestoneManagerRemote milestonemanager;
    private ProjectSpecialAuthorityManagerRemote authoritymanager;
    private ProjectGoalManagerRemote goalmanager;
    private ProjectResourceAreaManagerRemote resourceareamanager;
    private NepaCategoricalExclusionManagerRemote categoryexlusionmanager;

    private DecisionManagerRemote decisionmanager;
    private DecisionTypeManagerRemote decisiontypemanager;
    private AppealManagerRemote appealmanager;
    private LitigationManagerRemote litigationmanager;
    private DecisionAppealRuleManagerRemote darmanager;
    private DecisionMakerManagerRemote decisionmakermanager;

    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public NepaProjectResource()
            throws ServletException
    {
        final String SERVLET_ERROR = "NepaProjectResource failed to start. Could not start the bean manager.";

        //note that because we don't call init function the marshall/unmarshall functions of the base class
        //without explicit an explicit resource id won't work.  Nor will anything that requires the unti object
        //(which is private so we can't initialize it here)

        DataMartUtil local_util = DataMartUtil.getInstance();
        if (local_util == null)
        {
            throw new ServletException("Horror! Could not get a reference to the Datamart Utility singleton! Exiting in shame.");
        }

        //intialize all of the managers we'll need to handle an update here
        projectmanager = initManager(local_util, "Project");
        locationmanager = initManager(local_util, "ProjectLocation");
        activitymanager = initManager(local_util, "ProjectActivity");
        milestonemanager = initManager(local_util, "ProjectMilestone");
        authoritymanager = initManager(local_util, "ProjectSpecialAuthority");
        goalmanager = initManager(local_util, "ProjectGoal");
        resourceareamanager = initManager(local_util, "ProjectResourceArea");
        categoryexlusionmanager = initManager(local_util, "NepaCategoricalExclusion");

        decisionmanager = initManager(local_util, "Decision");
        decisiontypemanager = initManager(local_util, "DecisionType");
        appealmanager = initManager(local_util, "Appeal");
        litigationmanager = initManager(local_util, "Litigation");
        darmanager = initManager(local_util, "DecisionAppealRule");
        decisionmakermanager = initManager(local_util, "DecisionMaker");
    }

    private <T extends Object> T initManager(DataMartUtil util, String managerName)
        throws ServletException
    {
        final String SERVLET_ERROR = "NepaProjectResource failed to start. Could not start the bean manager.";

        try
        {
            Properties managerProps = new Properties();
            managerProps.setProperty("schema", managerName + ".xsd");
            managerProps.setProperty("package", "us.fed.fs.www.nepa.schema." + managerName.toLowerCase());
            managerProps.setProperty("bean", managerName + "ManagerRemote");

            int managerId = util.getResourceId(managerProps);

            //we always need to have T be some kind of resource manager.
            //otherwise bad things will happen.  But there isn't really a good way to
            //check for that so we'll assume the caller knows what they are doing
            @SuppressWarnings("unchecked")
            T manager = (T) util.getManager(managerId);

            //this is *not* the right way to do this.  However the right way involves creating
            //a base type for all of the manager classes (probably a parent for the various
            //*Remote interfaces rather than a base class) so that we can properly constraing T
            //and call ping directly.  However this does not require changing all of the manager
            //classes so we'll do it for now.
            if (!(Boolean) manager.getClass().getMethod("ping").invoke(manager))
            {
                throw new ServletException(SERVLET_ERROR);
            }

            return manager;
        }
        catch (IOException ex)
        {
            log.error(ex.toString());
            throw new ServletException(SERVLET_ERROR);
        } 
        catch (RuntimeException ex)
        {
            log.error(ex.toString());
            throw new ServletException(SERVLET_ERROR);
        }
        catch (NoSuchMethodException ex)
        {
            log.error(ex.toString());
            throw new ServletException(SERVLET_ERROR);
        }
        catch (IllegalAccessException ex)
        {
            log.error(ex.toString());
            throw new ServletException(SERVLET_ERROR);
        }
        catch (InvocationTargetException ex) {
            log.error(ex.toString());
            throw new ServletException(SERVLET_ERROR);
        }
    }

    @POST
    public Response post(InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        complainLoudly(new Exception("Function is not implemented"), Response.Status.INTERNAL_SERVER_ERROR);
        return ok("");
    }

    @PUT
    @Path("/{id}")
    public Response put(@PathParam("id") String id, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        /*
            The update happens in three main phases
            1.  XML validation.  We validate the xml according to structure and base data1 type
                This is currently specific to this class/function, however its intended to be
                extractable for generic XML validation (it's not as brittle or limited as xsd)
            2.  Data extraction to the auto generated "entity" objects usually generated by
                the XML unmarshall process.  We build them by hand.  This also catches some
                validation issues that either are too specific for general validation or
                are sufficiently tricky that putting them in the general validation didn't
                make sense.
            3.  Update the project record and numberous related tables from the entity objects

            We structure it this way to catch as many errors as possible before we start updated.
            There is no transaction or rollback logic so if we fail midway through we will be in
            an inconsistant state. This isn't any different from legacy process this replaces
            (it should be better due to combination of doing the projects one at a time and
            doing more of the validation upfront) but its still a potential risk.
        */
        try
        {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            Document project = factory.newDocumentBuilder().parse(inputxml);
//            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
//            int nRead;
//            byte[] data1 = new byte[16384];
//
//            while ((nRead = inputxml.read(data1, 0, data1.length)) != -1) {
//              buffer.write(data1, 0, nRead);
//            }
//            buffer.flush();
//            System.out.println("inputxml:"+buffer.toString());


            NepaProjectXmlValidator validator = new NepaProjectXmlValidator();
            if(!validator.validate(project))
            {
                throw new DataMartException(DataMartException.BAD_FORMAT, validator.getErrorMessage());
            }

            NepaProjectData data = new NepaProjectData(project, decisiontypemanager);

            //Update the project record.
            Project projectEntity = data.getProjectEntity();
            if (!id.equalsIgnoreCase(data.getProjectEntity().getId()))
            {
                complainLoudly(new Exception("Id in uri must match id in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);
            }

            //a bit of an undocumented feature, allow the caller to validate the 
            //xml without actually processing.
            if (!data.isValidationOnly())
            {
                fixProjectDefaults(projectEntity);

                // All clear. Now do the update.
                projectmanager.update(data.getProjectEntity());
                //if location data1 is empty, should we remove it from the project?
                if (data.getLocationEntity() != null)
                {
                    locationmanager.merge(data.getLocationEntity());
                }

                //if we have no activities, should we remove any existing?
                if (!data.getActivityEntities().isEmpty())
                {
                    Projectactivities activities = new Projectactivities();
                    activities.getProjectactivity().addAll(data.getActivityEntities());
                    activitymanager.replaceAll(activities);
                }

                //should we blank the milestone list if we do not get any in the xml?
                if (!data.getMilestoneEntities().isEmpty())
                {
                    Projectmilestones milestones = new Projectmilestones();
                    milestones.getProjectmilestone().addAll(data.getMilestoneEntities());
                    milestonemanager.replaceAll(milestones);
                }

                //we nuke the authorities before we update per the legacy ingress approach.
                //this appears to be a poor man's replace all, but the handling of an empty
                //list is inconsistant for these records (since we'll empty the list even
                //if we don't have anything to put back).  Its not clear which one we really
                //want and has probably not been noticed because it's rare to go from
                //having items in the list to not having items.
                Projectspecialauthorities specialauthlist = authoritymanager.getByProject(projectEntity.getId());
                for(Projectspecialauthority todelete : specialauthlist.getProjectspecialauthority())
                {
                    authoritymanager.delete(todelete.getProjectid(), todelete.getSpecialauthorityid());
                }

                for(Projectspecialauthority toadd : data.getAuthorityEntities())
                {
                    authoritymanager.merge(toadd);
                }

                //similarly delete and refresh goals -- same issues apply as for authorities
                Projectgoals goals = goalmanager.getAllByProject(projectEntity.getType(), projectEntity.getId());
                for(Projectgoal todelete : goals.getProjectgoal())
                {
                    goalmanager.delete(todelete.getProjecttype(), todelete.getProjectid(), todelete.getGoalid());
                }

                for(Projectgoal toadd : data.getGoalEntities())
                {
                    goalmanager.create(toadd);
                }

                //similarly delete and refresh resource areas -- same issues apply as for authorities
                Projectresourceareas areas = resourceareamanager.getAllByProject(projectEntity.getType(), projectEntity.getId());
                for (Projectresourcearea todelete : areas.getProjectresourcearea()) {
                    resourceareamanager.delete(todelete.getProjecttype(), todelete.getProjectid(), todelete.getResourceareaid());
                }

                for(Projectresourcearea toadd : data.getResourceareaEntities())
                {
                    resourceareamanager.create(toadd);
                }

                //update decisions here.

                //similarly delete and refresh category exlusions -- same issues apply as for authorities
                Nepacategoricalexclusions nces = categoryexlusionmanager.getList(projectEntity.getId());
                for (Nepacategoricalexclusion todelete : nces.getNepacategoricalexclusion()) {
                    categoryexlusionmanager.delete(todelete.getProjectid(), todelete.getCategoricalexclusionid());
                }

                for(Nepacategoricalexclusion toadd : data.getNepacategoricalexclusionEntities())
                {
                    categoryexlusionmanager.create(toadd);
                }

                deleteDecisions(projectEntity.getId(), projectEntity.getType());
                for (NepaProjectData.DecisionData decisionData : data.getDecisions())
                {
                    updateDecision(decisionData);
                }
            }
            
            //return a link to the project updated, but use the real project resource.
            return ok(uriInfo.getBaseUri().toString() + "projects/" + projectEntity.getType() + projectEntity.getId());
        }
        catch(DataMartException ex)
        {
            this.handleException(ex);
        }
        //this should pass through despite the general rule which will trap it.
        catch(WebApplicationException ex)
        {
            throw ex;
        }
        catch(IOException ex)
        {
            complainLoudly(ex, Response.Status.UNSUPPORTED_MEDIA_TYPE);
        }
        catch(SAXException ex)
        {
            complainLoudly(ex, Response.Status.UNSUPPORTED_MEDIA_TYPE);
        }
        catch(ParserConfigurationException ex)
        {
            complainLoudly(ex, Response.Status.UNSUPPORTED_MEDIA_TYPE);
        }

        //we really don't want unhandled exceptions to float out of this function.  Let's
        //trap and log everything.  Otherwise things like "null pointer exceptions" cause some very strange
        //errors to the caller.
        catch(RuntimeException ex)
        {
            //some of Runtime Exceptions don't have a message so this makes sure we log something.
//            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Unknown Exception", ex);
            log.error("Unknown Exception", ex);
            complainLoudly(new Exception("Unknown Exception of type: " + ex.getClass().getName()), Response.Status.INTERNAL_SERVER_ERROR);
        }

        //Java can't figure out that we can't get here and complains if we don't return something.
        //The alternative involves moving the actual return out of the try block, which complicates
        //variable scoping.  So we put in a useless return to avoid a compiler error.
        return ok("I don't know how I got here");
    }

    private void deleteDecisions(String projectid, String projectType)
        throws DataMartException
    {
        // Gets a list of all appeals in the DataMart. There is currently no service to retrieve appeals by decision id.
        Appeals oldapps = appealmanager.getAll();

        Decisions olddecisions = decisionmanager.getAllByProject(projectType, projectid);
        for (Decision d : olddecisions.getDecision())
        {
            Integer decisionId = d.getId();

            // Clear out old litigations for this decision
            Litigations oldlits = litigationmanager.getAllByDecisionId(decisionId);
            for (Litigation l : oldlits.getLitigation())
            {
                litigationmanager.delete(l.getId());
            }

            // Clear out DAR's for this decision
            Decisionappealrules darlist = darmanager.getList(decisionId);
            for (Decisionappealrule dar : darlist.getDecisionappealrule())
            {
                darmanager.delete(decisionId, dar.getAppealrule());
            }

            Decisionmakers decMakerslist = decisionmakermanager.getList(decisionId);
            for (Decisionmaker dm : decMakerslist.getDecisionmaker())
            {
                decisionmakermanager.delete(decisionId, dm.getId());
            }

            // Clear out old appeals for this decision
            for (Appeal appeal : oldapps.getAppeal())
            {
                if (appeal.getDecisionid() !=null && appeal.getDecisionid().equals(decisionId.toString()))
                {
                    appealmanager.delete(appeal.getId());
                }
            }

            // Delete the actual decision
            decisionmanager.delete(decisionId);
        }
    }

    private void updateDecision(NepaProjectData.DecisionData decisionData) throws DataMartException
    {
        Decision decision = decisionData.getDecisionEntity();

        try
        {
            //if this doesn't exist, it will fail with an exception ...
            //(on second thought, this should always happen because we should have
            //deleted all of the decisions for this project already.  Leavin this
            //in because its part of the legacy code and we should only delete the
            //decisions we no longer need and do it this way)
            decisionmanager.update(decision);
        }
        catch(DataMartException ex)
        {
            if (ex.getCode() == DataMartException.RESOURCE_NOT_FOUND)
            {
                //... and we'll try to creat it -- if that fails we're out of ideas.
                decisionmanager.create(decision);
            }
            else
            {
                throw ex;
            }
        }

        // comment from legacy code.  We no longer import the Appeals and haven't for years.  I'm not going to start now
        // that means that the appeals data1 from the xml is completely unused.
        // ingressAppeals();     disabled jps 2011-09-30, because we have implemented conflicting requirements and this no longer works

        for (Decisionmaker dm : decisionData.getDecisionMakerEntities())
        {
            decisionmakermanager.create(dm);
        }

        //same as for decisions above.  Not sure why the legacy code does this for some 
        //items and not others, but I'm following the legacy code here rather than make 
        //a change that could have a bad effect.  
        //The update should always fail and fall to the create code
        for (Litigation lit : decisionData.getLitigationEntities())
        {
            try
            {
                litigationmanager.update(lit);
            }
            catch(DataMartException ex)
            {
                if (ex.getCode() == DataMartException.RESOURCE_NOT_FOUND)
                {
                    litigationmanager.create(lit);
                }
                else
                {
                    throw ex;
                }
            }
        }

        for (Decisionappealrule dar : decisionData.getDecisionAppealRuleEntities())
        {
            darmanager.create(dar);
        }
    }


    /*
        This code is copied from the ProjectResource::put method.  It also appears in the post method.
        Spreading this code around is a bug waiting to happen.  It should be centralized somewhere.
        The ProjectManager class seems like the best place -- if we need to run this on the Project entity
        class before we call create or update (which the existing code implies but doesn't state) we
        should just run it on update/create directly so nobody can screw it up.

        However I really don't want to dig into changing the manager code for this, so it stays here for now.
    */
    private void fixProjectDefaults(Project project)
    {
        // TODO: pull this from the authentication credentials
        project.setAdminapp("pals");

        //note I don't think any of the things checked can actually happen
        //if the validator is working correctly, but in the intersest of paranoia
        //we'll do it anyway
        
        // Optional fields must not be null. set to defaults
        // if this changes need to update similar code in the ProjectResource post and put methods
        if (project.getNepainfo().getSopainfo() == null)
        {
            project.getNepainfo().setSopainfo(new Sopainfo());
        }
        
        if (project.getNepainfo().getSopainfo().isPublishflag() == null)
        {
            project.getNepainfo().getSopainfo().setPublishflag(false);
        }
        
        /*if (project.getNepainfo().getSopainfo().isNewflag() == null)
        {
            project.getNepainfo().getSopainfo().setNewflag(false);
        }*/
        
        if (project.getNepainfo().getSopainfo().getHeadercategory() == null)
        {
            project.getNepainfo().getSopainfo().setHeadercategory("");
        }
    }

    /*
     *
     */
    private static class NepaProjectXmlValidator
    {
        public static enum ValidationType {ROOT, REQUIRED, NOT_REQUIRED, LIST, LIST_REQUIRED};
        public static enum DataType {COMPOSITE, STRING, NOT_EMPTY, INTEGER, INTEGER_EMPTY, DOUBLE, DOUBLE_EMPTY, BOOLEAN};

        private ArrayList<String> errors = new ArrayList<String>();

        private static NodeValidator root;
        static
        {
            //define the rules for each individual block from the bottom up (since we are composing structure as we go)
            //we use linked hash maps so that when we print the ruleset we preserve the
            //order of the fields contained here.

            Map<String, NodeValidator> resourceareaFields = new LinkedHashMap<String, NodeValidator>();
            resourceareaFields.put("resourceareaid", new NodeValidator(ValidationType.REQUIRED, DataType.INTEGER));
            resourceareaFields.put("resourceareaorder", new NodeValidator(ValidationType.NOT_REQUIRED, DataType.INTEGER));

            Map<String, NodeValidator> goalFields = new LinkedHashMap<String, NodeValidator>();
            goalFields.put("goalid", new NodeValidator(ValidationType.REQUIRED, DataType.INTEGER));
            goalFields.put("goalorder", new NodeValidator(ValidationType.NOT_REQUIRED, DataType.INTEGER));

            Map<String, NodeValidator> sopaFields = new LinkedHashMap<String, NodeValidator>();
            sopaFields.put("sopaFlag", new NodeValidator(ValidationType.REQUIRED, DataType.BOOLEAN));
            sopaFields.put("isNewProjectFlag", new NodeValidator(ValidationType.NOT_REQUIRED, DataType.BOOLEAN));
            sopaFields.put("sopaHeaderUnit", new NodeValidator(ValidationType.NOT_REQUIRED, DataType.NOT_EMPTY));

            Map<String, NodeValidator> decmakerFields = new LinkedHashMap<String, NodeValidator>();
            decmakerFields.put("decmakerId", new NodeValidator(ValidationType.REQUIRED, DataType.INTEGER));
            decmakerFields.put("decmakertitle", new NodeValidator(ValidationType.REQUIRED, DataType.STRING));
            decmakerFields.put("decmakername", new NodeValidator(ValidationType.REQUIRED, DataType.STRING));
            decmakerFields.put("decmakesigneddate", new NodeValidator(ValidationType.REQUIRED, DataType.STRING));
            
            Map<String, NodeValidator> declitFields = new LinkedHashMap<String, NodeValidator>();
            declitFields.put("litid", new NodeValidator(ValidationType.REQUIRED, DataType.INTEGER));
            declitFields.put("litname", new NodeValidator(ValidationType.REQUIRED, DataType.STRING));
            declitFields.put("litstat", new NodeValidator(ValidationType.REQUIRED, DataType.INTEGER));
            declitFields.put("litoutcome", new NodeValidator(ValidationType.NOT_REQUIRED, DataType.INTEGER));
            declitFields.put("litclosed", new NodeValidator(ValidationType.NOT_REQUIRED, DataType.STRING));

            Map<String, NodeValidator> responseFields = new LinkedHashMap<String, NodeValidator>();
            responseFields.put("date", new NodeValidator(ValidationType.REQUIRED, DataType.STRING));
            responseFields.put("documentid", new NodeValidator(ValidationType.NOT_REQUIRED, DataType.STRING));
            responseFields.put("appealresponsefilesize", new NodeValidator(ValidationType.NOT_REQUIRED, DataType.STRING, true));

            Map<String, NodeValidator> decappFields = new LinkedHashMap<String, NodeValidator>();
            decappFields.put("appealid", new NodeValidator(ValidationType.REQUIRED, DataType.NOT_EMPTY));
            decappFields.put("appealadminunitcode", new NodeValidator(ValidationType.REQUIRED, DataType.NOT_EMPTY));
            decappFields.put("rule", new NodeValidator(ValidationType.REQUIRED, DataType.NOT_EMPTY));
            decappFields.put("appellant", new NodeValidator(ValidationType.NOT_REQUIRED, DataType.STRING));
            decappFields.put("outcome", new NodeValidator(ValidationType.NOT_REQUIRED, DataType.STRING));
            decappFields.put("response", new NodeValidator(ValidationType.NOT_REQUIRED, responseFields));

            Map<String, NodeValidator> decisionFields = new LinkedHashMap<String, NodeValidator>();
            decisionFields.put("decname", new NodeValidator(ValidationType.REQUIRED, DataType.NOT_EMPTY));
            decisionFields.put("decisionId", new NodeValidator(ValidationType.REQUIRED, DataType.INTEGER));
            decisionFields.put("dectype", new NodeValidator(ValidationType.REQUIRED, DataType.INTEGER));
            decisionFields.put("decdate", new NodeValidator(ValidationType.NOT_REQUIRED, DataType.STRING));
            decisionFields.put("decnotice", new NodeValidator(ValidationType.NOT_REQUIRED, DataType.STRING));
            decisionFields.put("decconstraint", new NodeValidator(ValidationType.REQUIRED, DataType.NOT_EMPTY));
            decisionFields.put("decmaker", new NodeValidator(ValidationType.LIST, decmakerFields));
            decisionFields.put("decapprule", new NodeValidator(ValidationType.NOT_REQUIRED, DataType.STRING));
            decisionFields.put("decappstat", new NodeValidator(ValidationType.NOT_REQUIRED, DataType.STRING));
            decisionFields.put("decapp", new NodeValidator(ValidationType.LIST, decappFields));
            decisionFields.put("declit", new NodeValidator(ValidationType.LIST, declitFields));
            decisionFields.put("decareasize", new NodeValidator(ValidationType.NOT_REQUIRED, DataType.DOUBLE_EMPTY));
            decisionFields.put("decareaunits", new NodeValidator(ValidationType.NOT_REQUIRED, DataType.STRING));

            Map<String, NodeValidator> locationFields = new LinkedHashMap<String, NodeValidator>();
            locationFields.put("locdesc", new NodeValidator(ValidationType.REQUIRED, DataType.NOT_EMPTY));
            locationFields.put("locfor", new NodeValidator(ValidationType.LIST_REQUIRED, DataType.STRING));
            locationFields.put("locdst", new NodeValidator(ValidationType.LIST_REQUIRED, DataType.STRING));
            locationFields.put("locstate", new NodeValidator(ValidationType.LIST_REQUIRED, DataType.INTEGER));
            locationFields.put("loccount", new NodeValidator(ValidationType.LIST_REQUIRED, DataType.STRING));
            locationFields.put("loclegal", new NodeValidator(ValidationType.NOT_REQUIRED, DataType.STRING));
            locationFields.put("loclat", new NodeValidator(ValidationType.NOT_REQUIRED, DataType.DOUBLE));
            locationFields.put("loclong", new NodeValidator(ValidationType.NOT_REQUIRED, DataType.DOUBLE));

            Map<String, NodeValidator> purposeFields = new LinkedHashMap<String, NodeValidator>();
            purposeFields.put("purposename", new NodeValidator(ValidationType.LIST_REQUIRED, DataType.INTEGER));

            Map<String, NodeValidator> milestoneFields = new LinkedHashMap<String, NodeValidator>();
            milestoneFields.put("msseq", new NodeValidator(ValidationType.REQUIRED, DataType.INTEGER));
            milestoneFields.put("mstype", new NodeValidator(ValidationType.REQUIRED, DataType.NOT_EMPTY));
            milestoneFields.put("msdate", new NodeValidator(ValidationType.REQUIRED, DataType.NOT_EMPTY));
            milestoneFields.put("msstatus", new NodeValidator(ValidationType.REQUIRED, DataType.NOT_EMPTY));
            milestoneFields.put("mshistoryflag", new NodeValidator(ValidationType.REQUIRED, DataType.NOT_EMPTY));

            Map<String, NodeValidator> contactFields = new LinkedHashMap<String, NodeValidator>();
            contactFields.put("contactShortName", new NodeValidator(ValidationType.REQUIRED, DataType.STRING));
            contactFields.put("contactname", new NodeValidator(ValidationType.REQUIRED, DataType.STRING));
            contactFields.put("contactphone", new NodeValidator(ValidationType.REQUIRED, DataType.STRING));
            contactFields.put("contactemail", new NodeValidator(ValidationType.REQUIRED, DataType.STRING));

            //dates are currently not validated on the first pass and will be handled
            //during the extraction phase.  This may change.
            Map<String, NodeValidator> projectFields = new LinkedHashMap<String, NodeValidator>();
            projectFields.put("id", new NodeValidator(ValidationType.REQUIRED, DataType.NOT_EMPTY));
            projectFields.put("projectdocumentid", new NodeValidator(ValidationType.REQUIRED, DataType.INTEGER));
            projectFields.put("expirationdate", new NodeValidator(ValidationType.NOT_REQUIRED, DataType.STRING));
            projectFields.put("name", new NodeValidator(ValidationType.REQUIRED, DataType.NOT_EMPTY));
            projectFields.put("status", new NodeValidator(ValidationType.REQUIRED, DataType.NOT_EMPTY));
            projectFields.put("type", new NodeValidator(ValidationType.REQUIRED, DataType.INTEGER));
            projectFields.put("url", new NodeValidator(ValidationType.NOT_REQUIRED, DataType.STRING));
            projectFields.put("appealslink", new NodeValidator(ValidationType.REQUIRED, DataType.NOT_EMPTY, true));
            projectFields.put("objectionslink", new NodeValidator(ValidationType.REQUIRED, DataType.NOT_EMPTY, true));
            projectFields.put("recordcomplete", new NodeValidator(ValidationType.NOT_REQUIRED, DataType.STRING, true));
            projectFields.put("description", new NodeValidator(ValidationType.REQUIRED, DataType.NOT_EMPTY));
            projectFields.put("projectsummary", new NodeValidator(ValidationType.REQUIRED, DataType.NOT_EMPTY));
            projectFields.put("contact", new NodeValidator(ValidationType.REQUIRED, contactFields));
            projectFields.put("catexclusion", new NodeValidator(ValidationType.LIST, DataType.INTEGER));
            projectFields.put("specauth", new NodeValidator(ValidationType.LIST_REQUIRED, DataType.STRING));
            projectFields.put("milestone", new NodeValidator(ValidationType.LIST, milestoneFields));
            projectFields.put("purpose", new NodeValidator(ValidationType.REQUIRED, purposeFields));
            projectFields.put("adminunit", new NodeValidator(ValidationType.REQUIRED, DataType.NOT_EMPTY));
            projectFields.put("activity", new NodeValidator(ValidationType.LIST_REQUIRED, DataType.INTEGER));
            projectFields.put("commreg", new NodeValidator(ValidationType.NOT_REQUIRED, DataType.STRING));
            projectFields.put("location", new NodeValidator(ValidationType.NOT_REQUIRED, locationFields));
            projectFields.put("decision", new NodeValidator(ValidationType.LIST, decisionFields));
            projectFields.put("pubflag", new NodeValidator(ValidationType.REQUIRED, DataType.NOT_EMPTY));
            projectFields.put("addinfoflag", new NodeValidator(ValidationType.NOT_REQUIRED, DataType.NOT_EMPTY));
            projectFields.put("implinfoflag", new NodeValidator(ValidationType.NOT_REQUIRED, DataType.NOT_EMPTY));
            projectFields.put("meetingflag", new NodeValidator(ValidationType.NOT_REQUIRED, DataType.NOT_EMPTY));
            projectFields.put("updatedate", new NodeValidator(ValidationType.REQUIRED, DataType.STRING));
            projectFields.put("sopa", new NodeValidator(ValidationType.REQUIRED, sopaFields));
            projectFields.put("primaryProjectManager", new NodeValidator(ValidationType.NOT_REQUIRED, DataType.STRING));
            projectFields.put("secondaryProjectManager", new NodeValidator(ValidationType.NOT_REQUIRED, DataType.STRING));
            projectFields.put("dataEntryPerson", new NodeValidator(ValidationType.NOT_REQUIRED, DataType.STRING));
            projectFields.put("goal", new NodeValidator(ValidationType.LIST, goalFields));
            projectFields.put("resourcearea", new NodeValidator(ValidationType.LIST, resourceareaFields));
            projectFields.put("esd", new NodeValidator(ValidationType.NOT_REQUIRED, DataType.BOOLEAN));
            projectFields.put("cenodecision", new NodeValidator(ValidationType.NOT_REQUIRED, DataType.BOOLEAN));

            root = new NodeValidator(projectFields);
        }

        public boolean validate(Document project)
        {
            errors = new ArrayList<String>();
            root.validateNode(project.getDocumentElement(), errors);
            return errors.isEmpty();
        }

        public String getRuleText()
        {
            StringBuilder sb = new StringBuilder();
            root.appendRulesText(0, sb);
            return sb.toString();
        }

        public String getErrorMessage()
        {
           if(errors.isEmpty())
           {
               return "";
           }
           else
           {
               StringBuilder sb = new StringBuilder();
               sb.append("Errors in XML:\n");
               for (String error : errors) {
                   sb.append(error).append("\n");
               }
               return sb.toString();
            }
        }

        private static class NodeValidator
        {
            private static final String ERROR_NODE_REQUIRED = "* Required node '%s' not found.";
            private static final String ERROR_NODE_TOO_MANY = "* Node '%s' does not allow multiple entries.";
            private static final String ERROR_NODE_INVALID = "* Unexpected node '%s' found.";
            private static final String ERROR_NODE_CANNOT_BE_EMPTY = "* Node '%s' must have a value.";
            private static final String ERROR_NODE_INVALID_INTEGER = "* Node '%s' with value '%s' is not a valid integer";
            private static final String ERROR_NODE_INVALID_DOUBLE = "* Node '%s' with value '%s' is not a valid double";
            private static final String ERROR_NODE_INVALID_YESNO = "* Node '%s' must be 'Y' or 'N' (not case sensitive)";
            private static final String ERROR_NODE_INVALID_BOOLEAN = "* Node '%s' must one of (%s)";

            //if you chance this, make sure you change the logic in the extractor that interprets the values.
            private static final String[] BOOLEAN_VALUES = {"true", "false", "y", "n", "yes", "no", "1", "0"};

            private final Map<String, NodeValidator> children;
            private final ValidationType validationType;
            private final DataType dataType;
            private boolean unused = false;

            public NodeValidator(ValidationType validationType, Map<String, NodeValidator> children) {
                this.children = children;
                this.validationType = validationType;
                this.unused = false;
                this.dataType = DataType.COMPOSITE;
            }

            public NodeValidator(ValidationType validationType, Map<String, NodeValidator> children, boolean unused) {
                this.children = children;
                this.validationType = validationType;
                this.unused = unused;
                this.dataType = DataType.COMPOSITE;
            }

            public NodeValidator(ValidationType validationType, DataType dataType) {
                this.children = null;
                this.validationType = validationType;
                this.unused = false;
                this.dataType = dataType;

                if (dataType == DataType.COMPOSITE)
                {
                    throw new IllegalArgumentException("Scalar nodes cannot have the composite type");
                }
            }

            public NodeValidator(ValidationType validationType, DataType dataType, boolean unused) {
                this.children = null;
                this.validationType = validationType;
                this.unused = unused;
                this.dataType = dataType;

                if (dataType == DataType.COMPOSITE)
                {
                    throw new IllegalArgumentException("Scalar nodes cannot have the composite type");
                }
            }

            public NodeValidator(Map<String, NodeValidator> children) {
                this.children = children;
                this.validationType = ValidationType.ROOT;
                this.unused = false;
                this.dataType = DataType.COMPOSITE;
            }

            public void validateNode(Node n, ArrayList<String> errors)
            {
                //nothing to do.
                if (children == null)
                {
                    //string types have no validation.
                    //composite types will be validated based on the child list and will skip this section.
                    String content = n.getTextContent();
                    switch (dataType)
                    {
                        case NOT_EMPTY:
                            if (content.length() == 0)
                            {
                                errors.add(String.format(ERROR_NODE_CANNOT_BE_EMPTY, n.getNodeName()));
                            }
                            break;
                        case INTEGER:
                            try
                            {
                                Integer.parseInt(content);
                            }
                            catch(NumberFormatException ex)
                            {
                                errors.add(String.format(ERROR_NODE_INVALID_INTEGER, n.getNodeName(), content));
                            }
                            break;
                        case INTEGER_EMPTY:
                            try
                            {
                                if(!content.trim().equals(""))
                                {
                                    Integer.parseInt(content);
                                }
                            }
                            catch(NumberFormatException ex)
                            {
                                errors.add(String.format(ERROR_NODE_INVALID_INTEGER, n.getNodeName(), content));
                            }
                            break;
                        case DOUBLE:
                            try
                            {
                                Double.parseDouble(content);
                            }
                            catch(NumberFormatException ex)
                            {
                                errors.add(String.format(ERROR_NODE_INVALID_DOUBLE, n.getNodeName(), content));
                            }
                            break;
                        case DOUBLE_EMPTY:
                            try
                            {
                                if(!content.trim().equals(""))
                                {
                                    Double.parseDouble(content);
                                }
                            }
                            catch(NumberFormatException ex)
                            {
                                errors.add(String.format(ERROR_NODE_INVALID_DOUBLE, n.getNodeName(), content));
                            }
                            break;
                        case BOOLEAN:
                            boolean found = false;
                            for(String test: BOOLEAN_VALUES)
                            {
                                if(content.equalsIgnoreCase(test))
                                {
                                    found = true;
                                    break;
                                }
                            }

                            if (!found)
                            {
                                StringBuilder sb = new StringBuilder();
                                for(String test: BOOLEAN_VALUES)
                                {
                                    sb.append("'").append(test).append("', ");
                                }
                                sb.delete(sb.length()-2, sb.length());
                                errors.add(String.format(ERROR_NODE_INVALID_BOOLEAN, n.getNodeName(), sb.toString()));
                            }
                            break;
                    }
                    return;
                }

                Counter fieldCounts = new Counter();

                NodeList subnodes = n.getChildNodes();
                for (int i = 0, len = subnodes.getLength(); i < len; i++)
                {
                    Node child = subnodes.item(i);
                    String name = child.getNodeName();

                    if (child.getNodeType() == Node.ELEMENT_NODE)
                    {
                        fieldCounts.increment(name);
                        NodeValidator childValidator = children.get(name);
                        if(childValidator != null)
                        {
                            childValidator.validateNode(child, errors);
                        }
                        else
                        {
                            errors.add(String.format(ERROR_NODE_INVALID, name));
                        }
                    }
                }

                for(String name : children.keySet())
                {
                    int count = fieldCounts.getCount(name);

                    //there are two constraints.  Must have one, must have no more than one.
                    //REQUIRED has both constraints
                    //NOT_REQUIRED has second contraint
                    //LIST_REQUIRED has first constraint
                    //LIST has neigther (nodes of this type can't fail validation here)
                    ValidationType type = children.get(name).validationType;
                    if (count == 0 && (type == ValidationType.REQUIRED || type == ValidationType.LIST_REQUIRED))
                    {
                        errors.add(String.format(ERROR_NODE_REQUIRED, name));
                    }
                    else if (count > 1 && (type == ValidationType.REQUIRED || type == ValidationType.NOT_REQUIRED))
                    {
                        errors.add(String.format(ERROR_NODE_TOO_MANY, name));
                    }
                }
            }

            public void appendRulesText(int depth, StringBuilder sb)
            {
                if (children == null)
                {
                    return;
                }

                for(String name : children.keySet())
                {
                    for(int i = 0; i < depth; i++)
                    {
                        sb.append("\t");
                    }

                    NodeValidator childValidator = children.get(name);
                    sb.append("* ").append(name);

                    //would be better to handle the child items during the recursion call
                    //rather than the parent peeking into the child objects but this is complicated
                    //by two factors.
                    //1) We don't have the field name at that point, which means we'd need to pass it
                    //2) We don't display or indent for the root node (which always has children), which means
                    //  always looking a the current nodes children actually works better.
                    switch (childValidator.dataType)
                    {
                        case COMPOSITE:
                            //intentionally blank
                            break;
                        case STRING:
                            sb.append(" (string)");
                            break;
                        case NOT_EMPTY:
                            sb.append(" (not empty)");
                            break;
                        case INTEGER:
                            sb.append(" (integer)");
                            break;
                        case INTEGER_EMPTY:
                            sb.append(" (integer empty allowed)");
                            break;
                        case DOUBLE:
                            sb.append(" (double)");
                            break;
                        case DOUBLE_EMPTY:
                            sb.append(" (double empty allowed)");
                            break;
                        case BOOLEAN:
                            sb.append(" (boolean)");
                            break;
                        default:
                            sb.append(" (undocumented type: ").append(children.get(name).toString()).append(")");
                            break;
                    }

                    switch (childValidator.validationType)
                    {
                        case REQUIRED:
                            sb.append(" -- Required");
                            break;
                        case NOT_REQUIRED:
                            sb.append(" -- Optional");
                            break;
                        case LIST:
                            sb.append(" -- Optional, Multiple Allowed");
                            break;
                        case LIST_REQUIRED:
                            sb.append(" -- Required, Multiple Allowed");
                            break;
                        default:
                            sb.append(" -- Undocumented rule ").append(children.get(name).toString());
                            break;
                    }

                    if (childValidator.unused)
                    {
                        sb.append(" (Field is unused)");
                    }

                    sb.append("\n");
                    childValidator.appendRulesText(depth+1, sb);
                }
            }
        }

        private static class Counter
        {
            private final Map<String, Integer> counter = new HashMap<String, Integer>();

            public void increment(String key)
            {
                Integer value = counter.get(key);
                if (value == null)
                {
                    counter.put(key, 1);
                }
                else
                {
                    counter.put(key, value + 1);
                }
            }

            public int getCount(String key)
            {
                Integer value = counter.get(key);
                if (value == null)
                {
                    return 0;
                }
                else
                {
                    return value;
                }
            }

            public void remove(String key)
            {
                counter.remove(key);
            }

            public Set<String> ketSet()
            {
                return counter.keySet();
            }
        }
   }


    private class NepaProjectData
    {
        private static final String ERROR_INVALID_ACTIVITYID = "Activity id '%s' is not valid";
        private static final String ERROR_INVALID_PURPOSEID = "Purpose id '%s' is not valid";
        private static final String ERROR_INVALID_STATEID = "State id '%s' is not valid";
        private static final String ERROR_INVALID_ANALYSISTYPE = "Analysis type '%s' is not valid";
        private static final String ERROR_INVALID_DATE = "Value '%s' for field '%s' is not a valid date";
        private static final String ERROR_INVALID_INTEGER = "Value '%s' for field '%s' is not a valid integer";
        private static final String ERROR_INVALID_APPEALRULE = "Appeal rule '%s' is not valid";
        private static final String ERROR_INVALID_APPEALOUTCOME = "Appeal outcome '%s' is not valid";
        private static final String ERROR_INVALID_DECISIONTYPE = "Decision type '%s' is not valid";

        //all projects sent through this interface will have the "nepa" project type
        private static final String PROJECT_TYPE = "nepa";

        //normally public members are very bad, but this is a private utility
        //class soley intended to group this data1 into one place so it shouldn't be a problem
        //this class should not be exposed outside of the containing class without
        //fixing this
        private boolean validationOnly;
        private Project projectEntity;
        private Locations locationEntity;
        private List<Projectactivity> activityEntities;
        private List<Projectmilestone> milestoneEntities;
        private List<Projectgoal> goalEntities;
        private List<Projectspecialauthority> authorityEntities;
        private List<Projectresourcearea> resourceareaEntities;
        private List<Nepacategoricalexclusion> nepacategoricalexclusionEntities;
        private List<DecisionData> decisions;

        private final ArrayList<String> errors;

        private final DecisionTypeManagerRemote decisiontypemanager;
        private Map<Integer, String> DecisionTypeMap;

        public NepaProjectData(Document project, DecisionTypeManagerRemote decisiontypemanager)
            throws DataMartException
        {
            this.errors = new ArrayList<String>();
            this.decisiontypemanager = decisiontypemanager;
            extractData(project);
        }

        private void extractData(Document project)
            throws DataMartException
        {
            //documents and containers -- previously handled as "sections" in the import xml
            //are now handled elsewhere. The logic from the ingressor is complicated and will
            //not be replicated here.  The section element does not appear in any of the recently
            //generated import xml.

            Element root = project.getDocumentElement();
            validationOnly = root.getAttribute("validationonly").equals("1");

            //get the main project entity
            projectEntity = extractProjectData(root);
            String projectid = getProjectEntity().getId();

            //get related data1.  This could be done marginally more efficiently in one pass, but by walking
            //through the DOM seperately for each variable we need to populate we can break this
            //down and make the coding more sane.  A couple of extra CPU cycles isn't going to hurt
            locationEntity = extractLocationData(projectid, root);
            activityEntities = extractActivityData(projectid, root);
            milestoneEntities = extractMilestoneData(projectid, root);
            authorityEntities = extractAuthorityData(projectid, root);
            goalEntities = extractGoalData(projectid, getProjectEntity().getType(), root);
            resourceareaEntities = extractResourceAreaData(projectid, getProjectEntity().getType(), root);
            nepacategoricalexclusionEntities = extractExclusionData(projectid, root);
            decisions = extractDecisionData(projectid, getProjectEntity().getType(), root);
        }

        private Project extractProjectData(Node projectNode)
            throws DataMartException
        {
            //main object we are trying to populate
            Project projectLocal = new Project();

            //the projects we handle are always of type "nepa"
            projectLocal.setType(PROJECT_TYPE);

            //the xml has this as one record, but we need to split it into a sub record
            Nepainfo nepainfo = new Nepainfo();
            Purposeids purposeids = null;
            Sopainfo sopainfo = null;

            NodeList subnodes = projectNode.getChildNodes();
            for (int i = 0, len = subnodes.getLength(); i < len; i++)
            {
                Node child = subnodes.item(i);
                String name = child.getNodeName();

                //Complex fields that need further breakdown (in sub functions)
                //should call continue to avoid getting the text content unnecesarily
                if (name.equals("contact"))
                {
                    //the mess in the function interface reflects the project xml --
                    //the nepa information is split weirdly in the xml structure.
                    addContactToNepaInfo(nepainfo, child);
                    continue;
                }
                else if (name.equals("milestone"))
                {
                    //this is a hack.  The "DecisionDecided" field is captured from a milestone of type 7
                    //which is not imported as a milestone because it doesn't fit the same format as other
                    //milestones in the datamart.  Instead we capture it as a single field.  This was
                    //done previously to resolve a discrepency between PALS and the Datamart and has yet
                    //to be corrected in a sane way.
                    //We only expect a single type 7 record in the XML, but we won't validate that.  Instead
                    //if we get more than one, we'll take the last one and call it good.
                    XMLGregorianCalendar date = extractDecisionDecided(child);
                    if (date != null)
                    {
                        nepainfo.setDecisiondecided(date);
                    }

                    continue;
                }
                //skip fields we'll handle in a later pass.  Skips generating the content text
                else if(
                    name.equals("activity") ||
                    name.equals("specauth") ||
                    name.equals("catexclusion") ||
                    name.equals("goal") ||
                    name.equals("resourcearea") ||
                    name.equals("decision") ||
                    name.equals("location")
                )
                {
                    continue;
                }

                //"scalar" fields that use the content value directly.
                String content = child.getTextContent();

                //main project fields
                if (name.equals("id"))
                {
                    projectLocal.setId(content);
                }
                else if (name.equals("name"))
                {
                    projectLocal.setName(content);
                }
                else if (name.equals("adminunit"))
                {
                    projectLocal.setUnitcode(content);
                }
                else if (name.equals("description"))
                {
                    projectLocal.setDescription(content);
                }
                else if (name.equals("updatedate"))
                {
                    projectLocal.setLastupdate(getDate(content, name));
                }
                else if (name.equals("commreg"))
                {
                    projectLocal.setCommentreg(content);
                }
                else if (name.equals("url"))
                {
                    projectLocal.setWwwlink(content);
                }
                else if (name.equals("projectsummary"))
                {
                    projectLocal.setWwwsummary(content);
                }
                else if (name.equals("pubflag"))
                {
                    projectLocal.setWwwpub(content);
                }
                else if (name.equals("addinfoflag"))
                {
                    projectLocal.setAddinfopubflag(content);
                }
                else if (name.equals("implinfoflag"))
                {
                    projectLocal.setImplinfopubflag(content);
                }
                else if (name.equals("meetingflag"))
                {
                    //System.out.println("meetingflag value:"+content);
                    projectLocal.setMeetingpubflag(content);
                }
                else if (name.equals("expirationdate"))
                {
                    projectLocal.setExpirationdate(getDate(content, name));
                }

                //nepa info fields
                else if (name.equals("projectdocumentid"))
                {
                    nepainfo.setProjectdocumentid(Integer.parseInt(content));
                }
                else if (name.equals("type"))
                {
                    String analysistype = getAnalysisTypeIdFromPalsId(Integer.parseInt(content));
                    if (analysistype != null)
                    {
                        nepainfo.setAnalysistypeid(analysistype);
                    }
                    else
                    {
                        errors.add(String.format(ERROR_INVALID_ANALYSISTYPE, content));
                    }
                }
                else if (name.equals("status"))
                {
                    nepainfo.setStatusid(content);
                }
                //does not appear to exist in current xml schema
                else if (name.equals("esd"))
                {
                    nepainfo.setEsd(stringToBoolean(content));
                }
                //does not appear to exist in current xml schema
                else if (name.equals("cenodecision"))
                {
                    nepainfo.setCenodecision(stringToBoolean(content));
                }
                else if (name.equals("dataEntryPerson"))
                {
                    nepainfo.setDataentryperson(content);
                }
                else if (name.equals("primaryProjectManager"))
                {
                    nepainfo.setPrimaryprojectmanager(content);
                }
                else if (name.equals("secondaryProjectManager"))
                {
                    nepainfo.setSecondaryprojectmanager(content);
                }
                else if (name.equals("purpose"))
                {
                    purposeids = extractPurposeids(child);
                }
                else if (name.equals("sopa"))
                {
                    sopainfo = extractSopainfo(child);
                }
            }

            bailOnError();

            //stitch up the project data1 and return
            nepainfo.setPurposeids(purposeids);
            nepainfo.setSopainfo(sopainfo);
            projectLocal.setNepainfo(nepainfo);
            return projectLocal;
        }

       private XMLGregorianCalendar extractDecisionDecided(Node milestoneNode)
            throws DataMartException
        {
            //this is a hack.  All we care about is the type and the date.  We really only
            //care about the date, but we ignore anything but type 7.
            NodeList subnodes = milestoneNode.getChildNodes();

            String type = "";
            String date = "";
            for (int i = 0, len = subnodes.getLength(); i < len; i++)
            {
                Node child = subnodes.item(i);
                String name = child.getNodeName();
                String content = child.getTextContent();

                if (name.equals("mstype"))
                {
                    type = content;
                }
                else if (name.equals("msdate"))
                {
                    date = content;
                }
            }

            if(type.equals("7"))
            {
                return getDate(date, "msdate");
            }
            else
            {
                return null;
            }
        }

        private void addContactToNepaInfo(Nepainfo nepainfo, Node contactNode)
        {
            NodeList subnodes = contactNode.getChildNodes();
            for (int i = 0, len = subnodes.getLength(); i < len; i++)
            {
                Node child = subnodes.item(i);
                String name = child.getNodeName();
                String content = child.getTextContent();

                if (name.equals("contactname"))
                {
                    nepainfo.setContactname(content);
                }
                else if (name.equals("contactShortName"))
                {
                    nepainfo.setContactshortname(content);
                }
                else if (name.equals("contactphone"))
                {
                    nepainfo.setContactphone(content);
                }
                else if (name.equals("contactemail"))
                {
                    nepainfo.setContactemail(content);
                }
            }
        }

        private Purposeids extractPurposeids(Node purposeNode)
        {
            Purposeids purposes = new Purposeids();
            List<String> list = purposes.getPurposeid();

            NodeList subnodes = purposeNode.getChildNodes();
            for (int i = 0, len = subnodes.getLength(); i < len; i++)
            {
                Node child = subnodes.item(i);
                String name = child.getNodeName();
                String content = child.getTextContent();

                if (name.equals("purposename")) {
                    String p = getPurposeIdFromPalsId(Integer.parseInt(content));
                    if (p != null)
                    {
                        list.add(p);
                    }
                    else
                    {
                        errors.add(String.format(ERROR_INVALID_PURPOSEID, content));
                    }
                }
            }

            return purposes;
        }

        Sopainfo extractSopainfo(Node sopaNode)
        {
            Sopainfo sopainfo = new Sopainfo();
            sopainfo.setNewflag(null);
            NodeList subnodes = sopaNode.getChildNodes();
            for (int i = 0, len = subnodes.getLength(); i < len; i++)
            {
                Node child = subnodes.item(i);
                String name = child.getNodeName();
                String content = child.getTextContent();

                if (name.equals("sopaFlag"))
                {
                    sopainfo.setPublishflag(stringToBoolean(content));
                }
                else if (name.equals("isNewProjectFlag"))
                {
                    sopainfo.setNewflag(stringToBoolean(content));
                }
                else if (name.equals("sopaHeaderUnit"))
                {
                    sopainfo.setHeadercategory(content);
                }
            }

            return sopainfo;
        }

        private Locations extractLocationData(String projectid, Node projectNode)
            throws DataMartException
        {
            ArrayList<Node> nodes = getNamedChildren(projectNode, "location");
            if(nodes.isEmpty())
            {
                return null;
            }

            Locations locations = new Locations();
            locations.setProjectid(projectid);

            Regions regions = new Regions();
            Forests forests = new Forests();
            Districts districts = new Districts();
            States states = new States();
            Counties counties = new Counties();

            //we validate elsewhere that the location is at most 1
            NodeList subnodes = nodes.get(0).getChildNodes();
            for (int i = 0, len = subnodes.getLength(); i < len; i++)
            {
                Node child = subnodes.item(i);
                String name = child.getNodeName();
                String content = child.getTextContent();

                if (name.equals("locdesc"))
                {
                    locations.setLocationdesc(content);
                }
                else if (name.equals("locfor"))
                {
                    forests.getForestid().add(content);
                }
                else if (name.equals("locdst"))
                {
                    districts.getDistrictid().add(content);
                }
                else if (name.equals("locstate"))
                {
                    String state = getStateIdFromPalsId(Integer.parseInt(content));
                    if (state != null)
                    {
                        states.getStateid().add(state);
                    }
                    else
                    {
                        errors.add(String.format(ERROR_INVALID_STATEID, content));
                    }
                }
                else if (name.equals("loccount"))
                {
                    counties.getCountyid().add(content);
                }
                else if (name.equals("loclegal"))
                {
                  locations.setLocationlegaldesc(content);
                }
                else if (name.equals("loclat"))
                {
                    locations.setLatitude(Double.parseDouble(content));
                }
                else if (name.equals("loclong"))
                {
                    locations.setLongitude(Double.parseDouble(content));
                }
            }

            bailOnError();

            locations.setRegions(regions);
            locations.setForests(forests);
            locations.setDistricts(districts);
            locations.setStates(states);
            locations.setCounties(counties);
            return locations;
        }

       private List<Projectactivity> extractActivityData(String projectid, Node projectNode)
            throws DataMartException
        {
            ArrayList<Projectactivity> activities = new ArrayList<Projectactivity>();

            ArrayList<Node> nodes = getNamedChildren(projectNode, "activity");
            if(nodes.isEmpty())
            {
                return activities;
            }

            for(Node n : nodes)
            {
                Projectactivity act = new Projectactivity();
                String activityid = getActivityIdFromPalsId(Integer.parseInt(n.getTextContent()));
                if (activityid != null)
                {
                    act.setProjectid(projectid);
                    act.setActivityid(activityid);
                    activities.add(act);
                }
                else
                {
                    errors.add(String.format(ERROR_INVALID_ACTIVITYID, n.getTextContent()));
                }
            }

            bailOnError();
            return activities;
        }

       private List<Projectmilestone> extractMilestoneData(String projectid, Node projectNode)
            throws DataMartException
        {
            ArrayList<Projectmilestone> milestones = new ArrayList<Projectmilestone>();

            ArrayList<Node> nodes = getNamedChildren(projectNode, "milestone");
            if(nodes.isEmpty())
            {
                return milestones;
            }

            for(Node n : nodes)
            {
                Projectmilestone milestone = new Projectmilestone();
                milestone.setProjectid(projectid);

                NodeList subnodes = n.getChildNodes();
                for (int i = 0, len = subnodes.getLength(); i < len; i++)
                {
                    Node child = subnodes.item(i);
                    String name = child.getNodeName();
                    String content = child.getTextContent();

                    if (name.equals("msseq"))
                    {
                        //should this be an int?
                        milestone.setSeqnum(content);
                    }
                    else if (name.equals("mstype"))
                    {
                        //should this be an int and/or validated against a specific list
                        milestone.setMilestonetype(content);
                    }
                    else if (name.equals("msdate"))
                    {
                        //not sure why we don't validate as date here
                        milestone.setDatestring(content);
                    }
                    else if (name.equals("msstatus"))
                    {
                        //int or boolean?
                        milestone.setStatus(content);
                    }
                    else if (name.equals("mshistoryflag"))
                    {
                        //not sure why this isn't a boolean
                        milestone.setHistory(content);
                    }
                }

                //Type 7 is special and doesn't get created as a milestone.
                //This is hopefully going away in the near future.  If so we should
                //throw an error here -- and/or validate all of the milestone types correctly.
                if(!milestone.getMilestonetype().equals("7"))
                {
                    milestones.add(milestone);
                }
            }

            bailOnError();
            return milestones;
        }

        private List<Projectgoal> extractGoalData(String projectid, String projecttype, Node projectNode)
            throws DataMartException
        {
            ArrayList<Projectgoal> goals = new ArrayList<Projectgoal>();

            ArrayList<Node> nodes = getNamedChildren(projectNode, "goal");
            if(nodes.isEmpty())
            {
                return goals;
            }

            for(Node n : nodes)
            {
                Projectgoal goal = new Projectgoal();
                goal.setProjectid(projectid);
                goal.setProjecttype(projecttype);

                NodeList subnodes = n.getChildNodes();
                for (int i = 0, len = subnodes.getLength(); i < len; i++)
                {
                    Node child = subnodes.item(i);
                    String name = child.getNodeName();
                    String content = child.getTextContent();

                    if (name.equals("goalid"))
                    {
                        goal.setGoalid(Integer.parseInt(content));
                    }
                    if (name.equals("goalorder"))
                    {
                        goal.setOrder(Integer.parseInt(content));
                    }
                }
                goals.add(goal);
            }

            bailOnError();
            return goals;
        }

       private List<Projectspecialauthority> extractAuthorityData(String projectid, Node projectNode)
            throws DataMartException
        {
            ArrayList<Projectspecialauthority> authorities = new ArrayList<Projectspecialauthority>();

            ArrayList<Node> nodes = getNamedChildren(projectNode, "specauth");
            if(nodes.isEmpty())
            {
                return authorities;
            }

            for(Node n : nodes)
            {
                Projectspecialauthority authority = new Projectspecialauthority();
                authority.setProjectid(projectid);

                NodeList subnodes = n.getChildNodes();
                for (int i = 0, len = subnodes.getLength(); i < len; i++)
                {
                    //should this be an integer?  The sample data1 always contains an int.
                    authority.setSpecialauthorityid(subnodes.item(i).getTextContent());
                }
                authorities.add(authority);
            }

            bailOnError();
            return authorities;
        }

        private List<Projectresourcearea> extractResourceAreaData(String projectid, String projecttype, Node projectNode)
            throws DataMartException
        {
            ArrayList<Projectresourcearea> resourceAreas = new ArrayList<Projectresourcearea>();

            ArrayList<Node> nodes = getNamedChildren(projectNode, "resourcearea");
            if(nodes.isEmpty())
            {
                return resourceAreas;
            }

            for(Node n : nodes)
            {
                Projectresourcearea resourceArea = new Projectresourcearea();
                resourceArea.setProjectid(projectid);
                resourceArea.setProjecttype(projecttype);
                
                NodeList subnodes = n.getChildNodes();
                for (int i = 0, len = subnodes.getLength(); i < len; i++)
                {
                    Node child = subnodes.item(i);
                    String name = child.getNodeName();
                    String content = child.getTextContent();

                    if (name.equals("resourceareaid"))
                    {
                        resourceArea.setResourceareaid(Integer.parseInt(content));
                    }
                    else if (name.equals("resourceareaorder"))
                    {
                        resourceArea.setOrder(Integer.parseInt(content));
                    }
                }
                resourceAreas.add(resourceArea);
            }

            bailOnError();
            return resourceAreas;
        }

        private List<Nepacategoricalexclusion> extractExclusionData(String projectid, Node projectNode)
            throws DataMartException
        {
            ArrayList<Nepacategoricalexclusion> exclusions = new ArrayList<Nepacategoricalexclusion>();

            ArrayList<Node> nodes = getNamedChildren(projectNode, "catexclusion");
            if(nodes.isEmpty())
            {
                return exclusions;
            }

            for(Node n : nodes)
            {
                Nepacategoricalexclusion exclusion = new Nepacategoricalexclusion();
                exclusion.setProjectid(projectid);

                NodeList subnodes = n.getChildNodes();
                for (int i = 0, len = subnodes.getLength(); i < len; i++)
                {
                    exclusion.setCategoricalexclusionid(Integer.parseInt(subnodes.item(i).getTextContent()));
                }
                exclusions.add(exclusion);
            }

            bailOnError();
            return exclusions;
        }

        //this is a more complicated subsection
        private List<DecisionData> extractDecisionData(String projectid, String projecttype, Node projectNode)
            throws DataMartException
        {
            ArrayList<DecisionData> localDecisions = new ArrayList<DecisionData>();

            ArrayList<Node> nodes = getNamedChildren(projectNode, "decision");
            if(nodes.isEmpty())
            {
                return localDecisions;
            }

            for(Node decisionNode : nodes)
            {
                DecisionData decision = new DecisionData();
                decision.setDecisionEntity(extractDecisionDetails(projectid, projecttype, decisionNode));

                Integer decisionid = decision.getDecisionEntity().getId();

                decision.addDecisionAppealRules(extractDecisionAppealRuleData(decisionid, decisionNode));
                decision.addDecisionMakers(extractDecisionMakerData(decisionid, decisionNode));
                decision.addAppeals(extractAppealData(projectid, projecttype, decisionid, decisionNode));
                decision.addLitigations(extractLitigationData(decisionid, decisionNode));

                bailOnError();
                localDecisions.add(decision);
            }

            return localDecisions;
        }

        private Decision extractDecisionDetails(String projectid, String projecttype, Node decisionNode)
            throws DataMartException
        {
            Decision decision = new Decision();
            decision.setProjectid(projectid);
            decision.setProjecttype(projecttype);

            NodeList subnodes = decisionNode.getChildNodes();
            for (int i = 0, len = subnodes.getLength(); i < len; i++)
            {
                Node child = subnodes.item(i);
                String name = child.getNodeName();

                //skip some list fields we don't process here before grabbing the content string
                if (
                    name.equals("decapprule") ||
                    name.equals("decmaker") ||
                    name.equals("decapp") ||
                    name.equals("declit")
                )
                {
                    continue;
                }

                String content = child.getTextContent();
                if (name.equals("decname"))
                {
                    decision.setName(content);
                }
                else if (name.equals("decisionId"))
                {
                    decision.setId(Integer.parseInt(content));
                }
                else if (name.equals("dectype"))
                {
                    String dectype = getDecisionTypeIdFromPalsId(Integer.parseInt(content));
                    if (dectype != null)
                    {
                        decision.setDectype(dectype);
                    }
                    else
                    {
                        errors.add(String.format(ERROR_INVALID_DECISIONTYPE, content));
                    }
                }
                else if (name.equals("decdate"))
                {
                    decision.setDecdate(getDate(content, "decdate"));
                }
                else if (name.equals("decnotice"))
                {
                    decision.setLegalnoticedate(getDate(content, "decnotice"));
                }
                else if (name.equals("decconstraint"))
                {
                    decision.setConstraint(content);
                }
                else if (name.equals("decappstat"))
                {
                    decision.setAppealstatus(content);
                }
                else if (name.equals("decareasize"))
                {
                    //if this is blank treat it like the field wasn't present at all.
                    //a hack to deal with badly behaving import xml
                    if (content.trim().length() > 0)
                    {
                        decision.setAreasize(Double.parseDouble(content));
                    }
                }
                else if (name.equals("decareaunits"))
                {
                    decision.setAreaunits(content);
                }
            }

            return decision;
        }

        private List<Decisionappealrule> extractDecisionAppealRuleData(Integer decisionid, Node decisionNode)
        {
            ArrayList<Decisionappealrule> decisionAppealRules = new ArrayList<Decisionappealrule>();

            ArrayList<Node> nodes = getNamedChildren(decisionNode, "decapprule");
            if(nodes.isEmpty())
            {
                return decisionAppealRules;
            }

            for(Node n : nodes)
            {
                String content = n.getTextContent();
                if (content.trim().equals(""))
                {
                    continue;
                }

                String[] values = content.split(",");
                for (String value : values)
                {
                    //we don't validate this field earlier because of the CSV angle
                    //so we need to deal with Integer parse issues here.
                    //it might be worth trying add CSV lists to the validator in a refactor
                    try
                    {
                        String rule = getAppealRuleIdFromPalsId(Integer.parseInt(value));
                        if (rule != null)
                        {
                            Decisionappealrule dar = new Decisionappealrule();
                            dar.setDecisionid(decisionid);
                            dar.setAppealrule(rule);
                            decisionAppealRules.add(dar);
                        }
                        else
                        {
                            errors.add(String.format(ERROR_INVALID_APPEALRULE, value));
                        }
                    }
                    catch (NumberFormatException ex)
                    {
                        errors.add(String.format(ERROR_INVALID_INTEGER, value, "decapprule"));
                    }                    
                }
            }

            return decisionAppealRules;
        }

       private List<Decisionmaker> extractDecisionMakerData(Integer decisionid, Node decisionNode)
        {
            ArrayList<Decisionmaker> decisionMakers = new ArrayList<Decisionmaker>();

            ArrayList<Node> nodes = getNamedChildren(decisionNode, "decmaker");
            if(nodes.isEmpty())
            {
                return decisionMakers;
            }

            for(Node n : nodes)
            {
                Decisionmaker decisionMaker = new Decisionmaker();
                decisionMaker.setDecisionid(decisionid);

                NodeList subnodes = n.getChildNodes();
                for (int i = 0, len = subnodes.getLength(); i < len; i++)
                {
                    Node child = subnodes.item(i);
                    String name = child.getNodeName();
                    String content = child.getTextContent();

                    if (name.equals("decmakerId"))
                    {
                        decisionMaker.setId(new Integer(content));
                    }
                    else if (name.equals("decmakertitle"))
                    {
                        decisionMaker.setTitle(content);
                    }
                    else if (name.equals("decmakername"))
                    {
                        decisionMaker.setName(content);
                    }
                    else if (name.equals("decmakesigneddate"))
                    {
                        decisionMaker.setSigneddate(getDate(content, "decmakesigneddate"));
                    }
                }
                decisionMakers.add(decisionMaker);
            }

            return decisionMakers;
        }

        private List<Appeal> extractAppealData(String projectId, String projectType, Integer decisionid, Node decisionNode)
        {
            ArrayList<Appeal> appeals = new ArrayList<Appeal>();

            ArrayList<Node> nodes = getNamedChildren(decisionNode, "decapp");
            if(nodes.isEmpty())
            {
                return appeals;
            }

            for(Node n : nodes)
            {
                Appeal appeal = new Appeal();
                appeal.setProjectid(projectId);
                appeal.setProjecttype(projectType);
                appeal.setDecisionid(decisionid.toString());

                NodeList subnodes = n.getChildNodes();
                for (int i = 0, len = subnodes.getLength(); i < len; i++)
                {
                    Node child = subnodes.item(i);
                    String name = child.getNodeName();
                    String content = child.getTextContent();

                    if (name.equals("appealid"))
                    {
                        appeal.setId(content);
                    }
                    else if (name.equals("appealadminunitcode"))
                    {
                        appeal.setAppadminunit(content);
                    }
                    else if (name.equals("rule"))
                    {
                        appeal.setRuleid(content);
                    }
                    else if (name.equals("appellant"))
                    {
                        appeal.setAppellant(content);
                    }
                    else if (name.equals("outcome"))
                    {
                        String outcomeid = getOutcomeIdFromName(content);
                        if (outcomeid == null)
                        {
                            errors.add(String.format(ERROR_INVALID_APPEALOUTCOME, content));
                        }
                        else
                        {
                            appeal.setOutcomeid(outcomeid);
                            if (isDismissalReason(content))
                            {
                                appeal.setDismissalid(getDismissalIdFromOutcomeId(outcomeid));
                                appeal.setStatusid("2");
                            }
                            else
                            {
                                appeal.setStatusid("4");
                            }
                       }
                    }
                    else if (name.equals("response"))
                    {
                        addReponseFields(appeal, child);
                    }
                }
                appeals.add(appeal);
            }

            return appeals;
        }

        private void addReponseFields(Appeal appeal, Node responseNode)
        {
            NodeList subnodes = responseNode.getChildNodes();
            for (int i = 0, len = subnodes.getLength(); i < len; i++)
            {
                Node child = subnodes.item(i);
                String name = child.getNodeName();
                String content = child.getTextContent();

                if (name.equals("date"))
                {
                    appeal.setResponsedate(getDate(content, "response date"));
                }
                else if (name.equals("documentid"))
                {
                    appeal.setDocid(content);
                }
            }
        }

        private List<Litigation> extractLitigationData(Integer decisionid, Node decisionNode)
        {
            ArrayList<Litigation> litigations = new ArrayList<Litigation>();

            ArrayList<Node> nodes = getNamedChildren(decisionNode, "declit");
            if(nodes.isEmpty())
            {
                return litigations;
            }

            for(Node n : nodes)
            {
                Litigation litigation = new Litigation();
                litigation.setDecisionid(decisionid);

                NodeList subnodes = n.getChildNodes();
                for (int i = 0, len = subnodes.getLength(); i < len; i++)
                {
                    Node child = subnodes.item(i);
                    String name = child.getNodeName();
                    String content = child.getTextContent();

                    if (name.equals("litname"))
                    {
                        litigation.setCasename(content);
                    }
                    if (name.equals("litstat"))
                    {
                        litigation.setStatus(Integer.parseInt(content));
                    }
                    if (name.equals("litoutcome"))
                    {
                        litigation.setOutcome(Integer.parseInt(content));
                    }
                    if (name.equals("litclosed"))
                    {
                        litigation.setClosed(getDate(content, "litclosed"));
                    }
                    if (name.equals("litid"))
                    {
                        litigation.setId(Integer.parseInt(content));
                    }
                }
                litigations.add(litigation);
            }

            return litigations;
        }

        private void bailOnError()
            throws DataMartException
        {
            //if we have errors at this point we want to bail
            if(!errors.isEmpty())
            {
                StringBuilder sb = new StringBuilder();
                sb.append("Errors in XML:\n");
                for (String error : errors) {
                    sb.append(error).append("\n");
		}

                throw new DataMartException(DataMartException.BAD_FORMAT, sb.toString());
            }
        }

        private XMLGregorianCalendar getDate(String content, String fieldname)
        {
            try
            {
                return DataMartBeanUtil.getXMLGregorianFromDateTime(DataMartBeanUtil.getDateFromString(content));
            }
            catch(DataMartException ex)
            {
                errors.add(String.format(ERROR_INVALID_DATE, fieldname, ex.getMessage()));
            }
            return null;
        }

        private boolean stringToBoolean(String content)
        {
            return (
                content.equalsIgnoreCase("true") ||
                content.equalsIgnoreCase("y") ||
                content.equalsIgnoreCase("yes") ||
                content.equalsIgnoreCase("1")
            );
        }

        //
        //  A couple of XML helper functions for things we'll need to do a couple of times
        //


        //this is basically getElementsByTagName except that it only returns
        //immediate children and isn't restricted to Elements (but in practice
        //the names of other nodes won't match unless that's specifically
        //what you are after.
        private ArrayList<Node> getNamedChildren(Node parent, String name)
        {
            ArrayList<Node> children = new ArrayList<Node>();

            NodeList subnodes = parent.getChildNodes();
            for (int i = 0, len = subnodes.getLength(); i < len; i++)
            {
                Node child = subnodes.item(i);
                if (child.getNodeName().equals(name))
                {
                    children.add(child);
                }
            }
            return children;
        }

        //
        // Some mapping classes ported from LegacyUtil in the ingresser code
        // I think they are only going to be relevant to this code so I am not
        // exposing them to the world, but we should do that rather than copy them
        // from place to place (copied here because the ingresser should not be referenced
        // here and, in fact, should be nuked with extreme vigor)
        //
        private String getAnalysisTypeIdFromPalsId(Integer palsid)
        {
            switch (palsid)
            {
                case 1: return "EIS";
                case 2: return "EA";
                case 3: return "CE";
                default: return null;
            }
        }

        private String getPurposeIdFromPalsId(Integer palsid)
        {
            switch (palsid)
            {
                case 1: return "RO";
                case 2: return "PN";
                case 3: return "RW";
                case 4: return "HR";
                case 5: return "RU";
                case 6: return "WF";
                case 7: return "RG";
                case 8: return "TM";
                case 9: return "VM";
                case 10: return "HF";
                case 11: return "WM";
                case 12: return "MG";
                case 13: return "LM";
                case 14: return "LW";
                case 15: return "SU";
                case 16: return "FC";
                case 17: return "RD";
                case 18: return "FR";
                default: return null;
            }
        }

        private String getActivityIdFromPalsId(Integer palsid)
        {
            switch (palsid)
            {
                case 1: return "RC";
                case 2: return "DC";
                case 3: return "OC";
                case 4: return "CP";
                case 5: return "MP";
                case 6: return "MT";
                case 7: return "DS";
                case 8: return "WD";
                case 9: return "GA";
                case 10: return "TR";
                case 11: return "SC";
                case 12: return "HR";
                case 13: return "SA";
                case 14: return "RA";
                case 15: return "HI";
                case 16: return "PE";
                case 17: return "GP";
                case 18: return "GR";
                case 19: return "SI";
                case 20: return "TS";
                case 21: return "SS";
                case 22: return "NC";
                case 23: return "RV";
                case 24: return "FV";
                case 25: return "NW";
                case 26: return "FN";
                case 27: return "WC";
                case 28: return "ML";
                case 29: return "EC";
                case 30: return "MO";
                case 31: return "AL";
                case 32: return "BL";
                case 33: return "LP";
                case 34: return "PJ";
                case 35: return "LA";
                case 36: return "FI";
                case 37: return "MF";
                case 38: return "RI";
                case 39: return "RD";
                case 40: return "DR";
                case 41: return "RE";
                case 42: return "SL";
                case 43: return "GT";
                case 44: return "WI";
                case 45: return "OL";
                case 46: return "NG";
                case 47: return "BM";
                case 48: return "HP";
                case 49: return "ET";
                default: return null;
            }
        }

        private String getStateIdFromPalsId(Integer palsid)
        {
            switch (palsid)
            {
                case 1: return "AL";
                case 2: return "AK";
                case 4: return "AZ";
                case 5: return "AR";
                case 6: return "CA";
                case 8: return "CO";
                case 9: return "CT";
                case 10: return "DE";
                case 11: return "DC";
                case 12: return "FL";
                case 13: return "GA";
                case 15: return "HI";
                case 16: return "ID";
                case 17: return "IL";
                case 18: return "IN";
                case 19: return "IA";
                case 20: return "KS";
                case 21: return "KY";
                case 22: return "LA";
                case 23: return "ME";
                case 24: return "MD";
                case 25: return "MA";
                case 26: return "MI";
                case 27: return "MN";
                case 28: return "MS";
                case 29: return "MO";
                case 30: return "MT";
                case 31: return "NE";
                case 32: return "NV";
                case 33: return "NH";
                case 34: return "NJ";
                case 35: return "NM";
                case 36: return "NY";
                case 37: return "NC";
                case 38: return "ND";
                case 39: return "OH";
                case 40: return "OK";
                case 41: return "OR";
                case 42: return "PA";
                case 44: return "RI";
                case 45: return "SC";
                case 46: return "SD";
                case 47: return "TN";
                case 48: return "TX";
                case 49: return "UT";
                case 50: return "VT";
                case 51: return "VA";
                case 53: return "WA";
                case 54: return "WV";
                case 55: return "WI";
                case 56: return "WY";
                case 60: return "AS";
                case 64: return "FM";
                case 66: return "GU";
                case 68: return "MH";
                case 69: return "MP";
                case 70: return "PW";
                case 72: return "PR";
                case 74: return "UM";
                case 78: return "VI";
                case 99: return "US";
                default: return null;
            }
        }

        private String getAppealRuleIdFromPalsId(Integer palsid)
        {
            switch (palsid)
            {
                case 1: return "None";
                case 2: return "215";
                case 3: return "217";
                case 4: return "218";
                case 5: return "251";
                case 6: return "214";
                default: return null;
            }
        }

        /*
         * Look up the ID of the outcome based on the name in the xml.
         * The outcome names in the xml have multiple spaces in between some of the words, so
         *     we need to remove all whitespace before performing the comparison
         */
        public String getOutcomeIdFromName(String name)
        {
            String n = name.replaceAll(" ", "");

            if (n.equalsIgnoreCase("NotTimely"))
            {
                return "1";
            }
            else if (n.equalsIgnoreCase("ReliefCannotBeGranted"))
            {
                return "2";
            }
            else if (n.equalsIgnoreCase("AppealPendingOtherAuthority"))
            {
                return "3";
            }
            else if (n.equalsIgnoreCase("DecisionNotSubjecttoAppeal"))
            {
                return "4";
            }
            else if (n.equalsIgnoreCase("NoCommentsFiled"))
            {
                return "5";
            }
            else if (n.equalsIgnoreCase("FSWithdrewDecision"))
            {
                return "6";
            }
            else if (n.equalsIgnoreCase("AppellantWithdrewAppeal"))
            {
                return "7";
            }
            else if (n.equalsIgnoreCase("InsufficientInfo"))
            {
                return "8";
            }
            else if (n.equalsIgnoreCase("Affirmed"))
            {
                return "9";
            }
            else if (n.equalsIgnoreCase("AffirmedwithInstructions"))
            {
                return "10";
            }
            else if (n.equalsIgnoreCase("ReversedinWhole"))
            {
                return "11";
            }
            else if (n.equalsIgnoreCase("ReversedinPart"))
            {
                return "12";
            }
            else if (n.equalsIgnoreCase("NoAppealDecisionIssued"))
            {
                return "13";
            }
            else if (n.equalsIgnoreCase("None"))
            {
                return "14";
            }
            else if (n.equalsIgnoreCase("Dismissed"))
            {
                return "15";
            }
            return null;
        }


        public Boolean isDismissalReason(String name)
        {
            //We don't need to worry about name not being valid because we will always
            //check getOutcomeIdFromName first and any invalid names will trigger an error
            //there.  Not that "Unknown" will never trigger because it does not match
            //in getOutcomeIdFromName.  Leaving that in because it was in the legacy code this
            //was copied from.
            String n = name.replaceAll(" ", "");
            return (n.equalsIgnoreCase("Unknown") ||
                    n.equalsIgnoreCase("NotTimely") ||
                    n.equalsIgnoreCase("ReliefCannotBeGranted") ||
                    n.equalsIgnoreCase("AppealPendingOtherAuthority") ||
                    n.equalsIgnoreCase("DecisionNotSubjecttoAppeal") ||
                    n.equalsIgnoreCase("NoCommentsFiled") ||
                    n.equalsIgnoreCase("FSWithdrewDecision") ||
                    n.equalsIgnoreCase("AppellantWithdrewAppeal") ||
                    n.equalsIgnoreCase("InsufficientInfo") ||
                    n.equalsIgnoreCase("Dismissed"));
        }

        public String getDismissalIdFromOutcomeId(String outcomeid)
        {
            if (outcomeid.equalsIgnoreCase("15"))
            {
                return "9";
            }
            else
            {
                return outcomeid;
            }
        }

        public String getDecisionTypeIdFromPalsId(Integer palsid)
            throws DataMartException
        {
            if (DecisionTypeMap == null) {
                // Initialize mapping from PALS code to Decision Type ID
                DecisionTypeMap = new HashMap<Integer, String>();
                Decisiontypes dectypelist = decisiontypemanager.getAll();
                for (Decisiontype d : dectypelist.getDecisiontype())
                {
                    DecisionTypeMap.put(d.getPalsid(), d.getId());
                }
            }

            return DecisionTypeMap.get(palsid);
        }

        /**
         * @return the projectEntity
         */
        public Project getProjectEntity() {
            return projectEntity;
        }

        /**
         * @return the locationEntity
         */
        public Locations getLocationEntity() {
            return locationEntity;
        }

        /**
         * @return the activityEntities
         */
        public List<Projectactivity> getActivityEntities() {
            return activityEntities;
        }

        /**
         * @return the milestoneEntities
         */
        public List<Projectmilestone> getMilestoneEntities() {
            return milestoneEntities;
        }

        /**
         * @return the goalEntities
         */
        public List<Projectgoal> getGoalEntities() {
            return goalEntities;
        }

        /**
         * @return the authorityEntities
         */
        public List<Projectspecialauthority> getAuthorityEntities() {
            return authorityEntities;
        }

        /**
         * @return the resourceareaEntities
         */
        public List<Projectresourcearea> getResourceareaEntities() {
            return resourceareaEntities;
        }

        /**
         * @return the nepacategoricalexclusionEntities
         */
        public List<Nepacategoricalexclusion> getNepacategoricalexclusionEntities() {
            return nepacategoricalexclusionEntities;
        }

        /**
         * @return the validationOnly
         */
        public boolean isValidationOnly() {
            return validationOnly;
        }

        /**
         * @return the decisions
         */
        public List<DecisionData> getDecisions() {
            return decisions;
        }

        /*
            Container to hold decision data1 since the entity classes aren't hierarchical themselves
            It might makes sense to refactor the code so that this class does the data1 extraction
            but sorting out how to do that without repeating logic from the main extraction class
            will take some thinking and time is growing short.
        */
        public class DecisionData
        {
            private Decision decisionEntity = new Decision();
            private final List<Appeal> appealEntities = new ArrayList<Appeal>();
            private final List<Decisionmaker> decisionMakerEntities = new ArrayList<Decisionmaker>();
            private final List<Litigation> litigationEntities = new ArrayList<Litigation>();
            private final List<Decisionappealrule> decisionAppealRuleEntities = new ArrayList<Decisionappealrule>();

            /**
             * @return the decisionEntity
             */
            public Decision getDecisionEntity() {
                return decisionEntity;
            }

            /**
             * @param decisionEntity the decisionEntity to set
             */
            public void setDecisionEntity(Decision decisionEntity) {
                this.decisionEntity = decisionEntity;
            }

            /**
             * @return the appealEntities
             */
            public List<Appeal> getAppealEntities() {
                return appealEntities;
            }

            /**
             * @return the decisionMakerEntities
             */
            public List<Decisionmaker> getDecisionMakerEntities() {
                return decisionMakerEntities;
            }

            /**
             * @return the litigationEntities
             */
            public List<Litigation> getLitigationEntities() {
                return litigationEntities;
            }

            /**
             * @return the decisionappealruleEntities
             */
            public List<Decisionappealrule> getDecisionAppealRuleEntities() {
                return decisionAppealRuleEntities;
            }

            public void addAppeals(List<Appeal> appeals)
            {
                appealEntities.addAll(appeals);
            }

            public void addDecisionMakers(List<Decisionmaker> dms)
            {
                decisionMakerEntities.addAll(dms);
            }

            public void addLitigations(List<Litigation> lits)
            {
                litigationEntities.addAll(lits);
            }

            public void addDecisionAppealRules(List<Decisionappealrule> dars) {
                decisionAppealRuleEntities.addAll(dars);
            }
        }
    }
}
