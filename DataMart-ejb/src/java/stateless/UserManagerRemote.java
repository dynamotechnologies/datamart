/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package stateless;
import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.user.*;
import beanutil.DataMartException;

@Remote
public interface UserManagerRemote {
    public void create(User resource) throws DataMartException;
    public User get(String shortname) throws DataMartException;
    public void update(User resource) throws DataMartException;
    public void delete(String shortname) throws DataMartException;
    public Users getAll() throws DataMartException;
    public Usersearchresults search(String shortname, String lastname, String firstname, Integer start, Integer rows) throws DataMartException;
    public Boolean ping();
}