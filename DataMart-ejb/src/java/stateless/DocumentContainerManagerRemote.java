package stateless;

import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.documentcontainer.*;
import beanutil.DataMartException;


/*
 *
 * The Manager beans are responsible for:
 *   1) Implementing business logic and
 *   2) Translating front-end resources to back-end database entities.
 *
 * Often the resources will consist of data from numerous database tables. It is the manager's responsibility
 *   to ensure that these front-end resources can be accessed as though they were cut from a single piece of wood.
 *
 */
@Remote
public interface DocumentContainerManagerRemote {
    public void create(String projectType, String projectId, Containers resource) throws DataMartException;
    public Containers get(String projectType, String projectId, Boolean pubflag, Boolean ignorecaraflag,Boolean sortdesc,String viewId) throws DataMartException;

    // Note that update() currently just replaces the entire container tree
    public void update(String projectType, String projectId, Containers resource) throws DataMartException;

    public Containerdocs getContainerDocsFromContainer(String projectType, String projectId, Integer contid, Integer start, Integer rows, Integer phaseId, Integer viewId) throws DataMartException;
    public void updateContainerDocs(String projectType, String projectId, Containerdocs doclist, boolean fixedflag, boolean forceParentUpdate) throws DataMartException;

    public Containers getContainerTemplate(String projectType, String projectId) throws DataMartException;
    public Containers getDownloaderContainerTemplate(String projectType, String projectId, String viewId)
            throws DataMartException;
    public void updateContainerTemplate(String projectType, String projectId, Containers containerRoot) throws DataMartException;

    public void delete(String projectType, String projectId) throws DataMartException;
    public void updatePhaseIds()throws DataMartException;
    
 
    public Boolean ping();
}
