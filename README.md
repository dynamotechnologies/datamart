# Datamart README


### to build the DataMart ear
from the project root: `ant` to build and `ant clean` to clean

### To run Datamart-test
from the DataMart-test root: `ant test`

It will then perform all the tests found in DataMart-test/test/test/**/*Test.java through the weblogic endpoint. The endpoint is configured at DataMart-client/src/client/client.properties
Make sure its set to use https (if you use http you'll get a 302 response) and don't include the port number when using a domain.
Test results will get dumped onto the console and as well as at DataMart-test/build/test/results/ (scroll to the bottom for content that was dumped to console in easier to read format)

Note the endpoint set in client.properties is what datamart client uses to connect to weblogic (i think), so for a deployment to the actual server you will likely have to switch it back to localhost:7001

