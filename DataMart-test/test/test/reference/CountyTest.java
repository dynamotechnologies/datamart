package test.reference;

import client.CountyClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.county.*;


public class CountyTest {

    private CountyClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new CountyClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postCounty(TestData.counties.id1, TestData.counties.name2);
            // If the above countyment did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        County county = null;
        Counties countylist = null;
        try {
            county = client.getCounty(TestData.counties.id1);
            countylist = client.getCountyList();
        } catch (IOException ex) {
            Logger.getLogger(CountyTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.counties.id1, county.getId());
        Assert.assertEquals(TestData.counties.name1, county.getName());
        Assert.assertTrue(countylist.getCounty().get(0).getName().length()>0);
        Assert.assertTrue(countylist.getCounty().get(0).getId().length()>0);

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        County county = null;
        try {
            client.putCounty(TestData.counties.id1, TestData.counties.name2);
            county = client.getCounty(TestData.counties.id1);
        } catch (Exception ex) {
            Logger.getLogger(CountyTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.counties.name2, county.getName());

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getCounty(TestData.counties.id1);
            // If the above countyment did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteCounty(TestData.counties.id1);
            // If the above countyment did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }

    private void post()
    {
        try {
            client.postCounty(TestData.counties.id1, TestData.counties.name1);
        } catch (IOException ex) {
            Logger.getLogger(CountyTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteCounty(TestData.counties.id1);
        } catch (IOException ex) {
            Logger.getLogger(CountyTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteCounty(TestData.counties.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }
}