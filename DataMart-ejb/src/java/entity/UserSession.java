/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sam
 */
@Entity
@Table(name = "UserSession")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserSession.findAll", query = "SELECT u FROM UserSession u"),
    @NamedQuery(name = "UserSession.findByToken", query = "SELECT u FROM UserSession u WHERE u.token = :token"),
    @NamedQuery(name = "UserSession.findByCreated", query = "SELECT u FROM UserSession u WHERE u.created = :created"),
    @NamedQuery(name = "UserSession.findByLastActivity", query = "SELECT u FROM UserSession u WHERE u.lastActivity = :lastActivity")})
public class UserSession implements Serializable {
    @Basic(optional = false)
    @Column(name = "ProjectAccessLevel")
    private String projectAccessLevel;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Token")
    private String token;
    @Basic(optional = false)
    @Column(name = "Created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    @Basic(optional = false)
    @Column(name = "LastActivity")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastActivity;
    @JoinColumn(name = "UserId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Users userId;
    @JoinColumn(name = "ProjectId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Projects projectId;

    public UserSession() {
    }

    public UserSession(String token) {
        this.token = token;
    }

    public UserSession(String token, Date created, Date lastActivity) {
        this.token = token;
        this.created = created;
        this.lastActivity = lastActivity;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastActivity() {
        return lastActivity;
    }

    public void setLastActivity(Date lastActivity) {
        this.lastActivity = lastActivity;
    }

    public Users getUserId() {
        return userId;
    }

    public void setUserId(Users userId) {
        this.userId = userId;
    }

    public Projects getProjectId() {
        return projectId;
    }

    public void setProjectId(Projects projectId) {
        this.projectId = projectId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (token != null ? token.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserSession)) {
            return false;
        }
        UserSession other = (UserSession) object;
        if ((this.token == null && other.token != null) || (this.token != null && !this.token.equals(other.token))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.UserSession[ token=" + token + " ]";
    }

    public String getProjectAccessLevel() {
        return projectAccessLevel;
    }

    public void setProjectAccessLevel(String projectAccessLevel) {
        this.projectAccessLevel = projectAccessLevel;
    }
    
}
