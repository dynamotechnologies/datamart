package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.SubscriberManagerRemote;
import us.fed.fs.www.nepa.schema.subscriber.ObjectFactory;
import us.fed.fs.www.nepa.schema.subscriber.Subscribers;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.StringWriter;


@Path ("/utils/subscribers")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class SubscriberUtilsResource extends AbstractResource
{
    private SubscriberManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public SubscriberUtilsResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "Subscriber.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.subscriber");
        MY_PROPS.setProperty("bean", "SubscriberManagerRemote");
        try {
            beanmanager = (SubscriberManagerRemote) init(SubscriberManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("SubscriberUtilsResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("SubscriberUtilsResource failed to start. Could not ping the bean manager!");
    }
    
    /*
     * Get list of unsubscribed subscribers
     * 
     * contactmethod - if not null, all subscribers must match this contact method
     *                 (usually either Email or Mail)
     * activetopic - if > 0, return only new subscribers for active mailing lists
     *               if <= 0, return only new subscribers for inactive mailing lists
     *               if null, do not filter on mailing list status
     *               (you can subscribe to an inactive mailing list for some reason)
     * start - if not null, start returning the Nth row (0-based)
     * rows - if not null, return N rows
     */
    @GET
    @Path("/unsubscribed")
    public Response get(
            @QueryParam("contactmethod") String contactmethod,
            @QueryParam("activetopic") Integer activetopicval, 
            @QueryParam("start") Integer start, 
            @QueryParam("rows") Integer rows,
            @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            Boolean activetopicflag = null;
            if (activetopicval != null) {
                activetopicflag = (activetopicval > 0);
            }
            
            // Get a dump of all the entities in the datamart
            Subscribers subscriberlist = beanmanager.getAllUnsubscribed(contactmethod, activetopicflag, start, rows);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Subscribers> jaxbelement = objectfactory.createSubscribers(subscriberlist);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }

}