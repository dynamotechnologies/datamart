package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.appealoutcome.*;

import beanutil.*;
import java.util.Iterator;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless(mappedName="AppealOutcomeManagerRemote")
public class AppealOutcomeManager implements AppealOutcomeManagerRemote
{

    @PersistenceContext
    private EntityManager em;


    @Override
    public void create(Appealoutcome resource)
             throws DataMartException
    {
        validate(resource);
        entity.RefAppealOutcomes entity = createEntity(resource);

        entity.RefAppealOutcomes badentity = createEntity(resource.getId());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        entity = em.merge(entity);
    }

    @Override
    public Appealoutcomes getAll()
             throws DataMartException
    {
        // Retrieve all entities from the database
        Query query = em.createQuery("SELECT u FROM RefAppealOutcomes u");
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Appealoutcomes responselist = new Appealoutcomes();
        while (it.hasNext()) {
            responselist.getAppealoutcome().add(createResource( (entity.RefAppealOutcomes)it.next() ));
        }
        return responselist;
    }

    @Override
    public String getName(String id)
            throws DataMartException
    {
        entity.RefAppealOutcomes entity = createEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return entity.getOutcome();
    }

    @Override
    public void update(Appealoutcome resource)
             throws DataMartException
    {
        validate(resource);
        entity.RefAppealOutcomes entity = createEntity(resource);

        if (createEntity(resource.getId())==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        entity = em.merge(entity);
    }

    @Override
    public void delete(String id)
             throws DataMartException
    {
        entity.RefAppealOutcomes entity = createEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     *
     * This method does the mapping from jaxb object to entity object and vice versa
     */
    private Appealoutcome createResource(entity.RefAppealOutcomes entity)
            throws DataMartException
    {
        Appealoutcome resource = new Appealoutcome();
        resource.setId(entity.getId().toString());
        resource.setName(entity.getOutcome());
        return resource;
    }

    private entity.RefAppealOutcomes createEntity(Appealoutcome resource)
            throws DataMartException
    {
        int id;
        try {
            id = Integer.parseInt(resource.getId());
        } catch (NumberFormatException ex) {
            throw new DataMartException(DataMartException.BAD_FORMAT, "Id must be an integer");
        }
        entity.RefAppealOutcomes entity = new entity.RefAppealOutcomes();
        entity.setId(id);
        entity.setOutcome(resource.getName());
        return entity;
    }

    // This version of overloaded method createEntity() accepts a String ID as a parameter, and returns the
    //   associated entity, or null if it doesn't exist
    private entity.RefAppealOutcomes createEntity(String id)
            throws DataMartException
    {
        int intId;
        try {
            intId = Integer.parseInt(id);
        } catch (NumberFormatException ex) {
            throw new DataMartException(DataMartException.BAD_FORMAT, "Id must be an integer");
        }
        return em.find(entity.RefAppealOutcomes.class, intId);
    }

    private void validate(Appealoutcome resource)
            throws DataMartException
    {
        DataMartBeanUtil.confirmNotNull(resource.getName(), "Name");
    }
}
