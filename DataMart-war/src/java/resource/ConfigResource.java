package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.ConfigManagerRemote;
import us.fed.fs.www.nepa.schema.config.Config;
import us.fed.fs.www.nepa.schema.config.Configs;
import us.fed.fs.www.nepa.schema.config.ObjectFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;


@Path ("/ref/configs")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class ConfigResource extends AbstractResource
{
    private ConfigManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public ConfigResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "Config.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.config");
        MY_PROPS.setProperty("bean", "ConfigManagerRemote");
        try {
            beanmanager = (ConfigManagerRemote) init(ConfigManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("ConfigResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("ConfigResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;
        try {
            Config Config = (Config)unmarshal(inputxml, Config.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+Config.getKey());

            // All clear. Now do the insert.
            beanmanager.create(Config);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the newly created entity
        return created(uri);
    }


    @GET
    /*
     * Get list of config resources
     */
    public Response get(@Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get a dump of all the entities in the datamart
            Configs Configlist = beanmanager.getAll();

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Configs> jaxbelement = objectfactory.createConfigs(Configlist);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }

    @GET
    @Path("/{key}")
    /*
     * Get single config resource
     */
    public Response get(@PathParam("key") String key, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            Config Config = beanmanager.get(key);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Config> jaxbelement = objectfactory.createConfig(Config);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @PUT
    @Path("/{key}")
    public Response put(@PathParam("key") String key, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        Config Config = null;
        try {
            Config = (Config) unmarshal(inputxml, Config.class);

            if (!key.equalsIgnoreCase(Config.getKey()))
                complainLoudly(new Exception("Key in uri must match key in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.update(Config);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"/"+Config.getKey()+"</location>");
    }


    @DELETE
    @Path("/{key}")
    public Response delete(@PathParam("key") String key, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(key);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}

// TODO: restrict access to the admin application