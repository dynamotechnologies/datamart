package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.unit.*;


public class UnitClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public UnitClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.unit", "Unit.xsd");
    }

    /*
     *
     * TODO: implement EntityTag logic for conditional requests. Do this for all clients.
     */
    public void postUnit(String id, String name, Boolean extendedDetails, Boolean projectMap, String address1, String address2, String city, String state, String zip, String phone, String wwwLink, String commentEmail, String newspaper, String newspaperURL, String boundaryURL, boolean active, String spotlightid1, String spotlightid2, String spotlightid3)
            throws IOException
    {
        Unit unit = new Unit();
        unit.setCode(id);
        unit.setName(name);
        unit.setExtendeddetails(extendedDetails);
        unit.setProjectmap(projectMap);
        unit.setAddress1(address1);
        unit.setAddress2(address2);
        unit.setCity(city);
        unit.setState(state);
        unit.setZip(zip);
        unit.setPhonenumber(phone);
        unit.setWwwlink(wwwLink);
        unit.setCommentemail(commentEmail);
        unit.setNewspaper(newspaper);
        unit.setNewspaperurl(newspaperURL);
        unit.setBoundaryurl(boundaryURL);
        unit.setActive(active);
        unit.setSpotlightid1(spotlightid1);
        unit.setSpotlightid2(spotlightid2);
        unit.setSpotlightid3(spotlightid3);
        postUnit(unit);
    }


    /*
     * Overloaded postUnit with default values
     */
    public void postUnit(String id, String name)
            throws IOException
    {
        postUnit(id, name,
                false,  /* extendedDetails */
                false,  /* projectMap */
                null,   /* address1 */
                null,   /* address2 */
                null,   /* city */
                null,   /* state */
                null,   /* zip */
                null,   /* phone */
                null,   /* wwwLink */
                null,   /* commentemail */
                null,   /* newspaper */
                null,   /* newspaperurl */
                null,   /* boundaryurl */
                true,   /* active */
                null,   /* spotlight1 */
                null,   /* spotlight2 */
                null    /* spotlight3 */
                );
    }


    public void postUnit(Unit unit)
            throws IOException
    {
        post(objectfactory.createUnit(unit), "/ref/units");
    }


    public Unit getUnit(String id)
            throws IOException
    {
        return get("/ref/units/" + id, Unit.class);
    }


    public Units getUnitList()
            throws IOException
    {
        return get("/ref/units", Units.class);
    }

    public Units getRegions()
            throws IOException
    {
        return get("/units/regions", Units.class);
    }

    public Units getSubunits(String id)
            throws IOException
    {
        return get("/units/" + id + "/subunits", Units.class);
    }


    public void putUnit(String id, String name, Boolean extendedDetails, Boolean projectMap, String address1, String address2, String city, String state, String zip, String phone, String wwwLink, String commentEmail, String newspaper, String newspaperURL, String boundaryURL, boolean active, String spotlight1, String spotlight2, String spotlight3)
            throws IOException
    {
        Unit unit = new Unit();
        unit.setCode(id);
        unit.setName(name);
        unit.setExtendeddetails(extendedDetails);
        unit.setProjectmap(projectMap);
        unit.setAddress1(address1);
        unit.setAddress2(address2);
        unit.setCity(city);
        unit.setState(state);
        unit.setZip(zip);
        unit.setPhonenumber(phone);
        unit.setWwwlink(wwwLink);
        unit.setCommentemail(commentEmail);
        unit.setNewspaper(newspaper);
        unit.setNewspaperurl(newspaperURL);
        unit.setBoundaryurl(boundaryURL);
        unit.setActive(active);
        unit.setSpotlightid1(spotlight1);
        unit.setSpotlightid2(spotlight2);
        unit.setSpotlightid3(spotlight3);
        putUnit(unit);
    }


    /*
     * Overloaded putUnit with a default value "extendedDetails = false"
     */
    public void putUnit(String id, String name)
            throws IOException
    {
        putUnit(id, name,
                false,  /* extendedDetails */
                false,  /* projectMap */
                null,   /* address1 */
                null,   /* address2 */
                null,   /* city */
                null,   /* state */
                null,   /* zip */
                null,   /* phone */
                null,   /* wwwLink */
                null,   /* commentemail */
                null,   /* newspaper */
                null,   /* newspaperurl */
                null,   /* boundaryurl */
                true,    /* active */
                null,   /* spotlight1 */
                null,   /* spotlight2 */
                null    /* spotlight3 */
                );
    }


    public void putUnit(Unit unit)
            throws IOException
    {
        put(objectfactory.createUnit(unit), "/ref/units/" + unit.getCode());
    }

    
    public void deleteUnit(String id)
            throws IOException
    {
        delete("/ref/units/" + id);
    }
}