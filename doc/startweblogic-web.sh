#!/bin/sh
#
# chkconfig: 2345 95 20
# description: Starts and Stops WebLogic Server
# processname: weblogic
#
#
#

export JAVA_HOME=/usr/lib/jvm/jdk1.6.0_24/jre
export MW_HOME=/app/Oracle/Middleware
export WLDOMAIN=datamartweb
export DOMAIN_HOME=$MW_HOME/user_projects/domains/$WLDOMAIN
export ADMINLOG=$MW_HOME/weblogic-admin.log
export WEBSERVERLOG=$MW_HOME/weblogic.log

case $1 in
'start')
(echo 'starting WebLogic:')
cd $DOMAIN_HOME
. $MW_HOME/wlserver_10.3/server/bin/setWLSEnv.sh
$JAVA_HOME/bin/java -Xmx512m -XX:MaxPermSize=256m weblogic.Server > $ADMINLOG 2>&1 &
sleep 30
echo 'Giving the Admin Server a 5 minute head start...'
sleep 270
$JAVA_HOME/bin/java -server -Dweblogic.ThreadPoolSize=100 -Dweblogic.MaxMessageSize=30000000 -Xms4096m -Xmx4096m -XX:MaxPermSize=4096m -Dweblogic.Name=web -Djava.security.policy=$MW_HOME/wlserver_10.3/server/lib/weblogic.policy -Dweblogic.ProductionModeEnabled=true -Dweblogic.security.SSL.trustedCAKeyStore=$MW_HOME/wlserver_10.3/server/lib/cacerts -Xverify:none -da -Dplatform.home=$MW_HOME/wlserver_10.3 -Dwls.home=$MW_HOME/wlserver_10.3/server -Dweblogic.home=$MW_HOME/wlserver_10.3/server -Dweblogic.management.discover=false -Dweblogic.management.server=http://localhost:7001 -Dwlw.iterativeDev=false -Dwlw.testConsole=false -Dwlw.logErrorsToConsole=false -Dweblogic.ext.dirs=$MW_HOME/patch_wls1033/profiles/default/sysext_manifest_classpath weblogic.Server > $WEBSERVERLOG 2>&1 &
echo 'Check the logs to verify that the servers started successfully:'
echo $ADMINLOG
echo $WEBSERVERLOG
echo ''
;;
'stop')
(echo 'stopping WebLogic:')
cd $DOMAIN_HOME
. $MW_HOME/wlserver_10.3/server/bin/setWLSEnv.sh
$JAVA_HOME/bin/java weblogic.Admin -url t3://localhost:7001/ -username datamart -password p1cgorcl SHUTDOWN web
$JAVA_HOME/bin/java weblogic.Admin -url t3://localhost:7001/ -username datamart -password p1cgorcl SHUTDOWN AdminServer
;;
'status')
ps aux | grep weblogic\.Server | grep -v grep
;;
*)
echo "usage: `basename $0` {start|stop}"
;;
esac

