package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.analysistype.*;

import beanutil.*;
import java.util.Iterator;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless(mappedName="AnalysisTypeManagerRemote")
public class AnalysisTypeManager implements AnalysisTypeManagerRemote
{

    @PersistenceContext
    private EntityManager em;


    @Override
    public void create(Analysistype resource)
             throws DataMartException
    {
        validate(resource);
        entity.RefAnalysisTypes entity = createEntity(resource);

        entity.RefAnalysisTypes badentity = createEntity(resource.getId());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        entity = em.merge(entity);
    }

    @Override
    public Analysistypes getAll()
             throws DataMartException
    {
        // Retrieve all entities from the database
        Query query = em.createQuery("SELECT u FROM RefAnalysisTypes u");
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Analysistypes responselist = new Analysistypes();
        while (it.hasNext()) {
            responselist.getAnalysistype().add(createResource( (entity.RefAnalysisTypes)it.next() ));
        }
        return responselist;
    }

    @Override
    public String getName(String id)
            throws DataMartException
    {
        entity.RefAnalysisTypes entity = createEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return entity.getName();
    }

    @Override
    public void update(Analysistype resource)
             throws DataMartException
    {
        validate(resource);
        entity.RefAnalysisTypes entity = createEntity(resource);

        if (createEntity(resource.getId())==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        entity = em.merge(entity);
    }

    @Override
    public void delete(String id)
             throws DataMartException
    {
        entity.RefAnalysisTypes entity = createEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     *
     * This method does the mapping from jaxb object to entity object and vice versa
     */
    private Analysistype createResource(entity.RefAnalysisTypes entity)
            throws DataMartException
    {
        Analysistype resource = new Analysistype();
        resource.setId(entity.getId());
        resource.setName(entity.getName());
        return resource;
    }

    private entity.RefAnalysisTypes createEntity(Analysistype resource)
            throws DataMartException
    {
        entity.RefAnalysisTypes entity = new entity.RefAnalysisTypes();
        entity.setId(resource.getId());
        entity.setName(resource.getName());
        return entity;
    }

    // This version of overloaded method createEntity() accepts a String ID as a parameter, and returns the
    //   associated entity, or null if it doesn't exist
    private entity.RefAnalysisTypes createEntity(String id)
            throws DataMartException
    {
        return em.find(entity.RefAnalysisTypes.class, id);
    }

    private void validate(Analysistype resource)
            throws DataMartException
    {
        DataMartBeanUtil.confirmNotNull(resource.getName(), "Name");
    }
}
