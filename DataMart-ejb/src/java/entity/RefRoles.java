package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "RefRoles")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RefRoles.findAll", query = "SELECT r FROM RefRoles r"),
    @NamedQuery(name = "RefRoles.findById", query = "SELECT r FROM RefRoles r WHERE r.id = :id"),
    @NamedQuery(name = "RefRoles.findByRoleName", query = "SELECT r FROM RefRoles r WHERE r.roleName = :roleName"),
    @NamedQuery(name = "RefRoles.findByDescription", query = "SELECT r FROM RefRoles r WHERE r.description = :description")})
public class RefRoles implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "RoleName")
    private String roleName;
    @Basic(optional = false)
    @Column(name = "Description")
    private String description;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refRoles")
    private Collection<UnitRoles> unitRolesCollection;

    public RefRoles() {
    }

    public RefRoles(Integer id) {
        this.id = id;
    }

    public RefRoles(Integer id, String roleName, String description) {
        this.id = id;
        this.roleName = roleName;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public Collection<UnitRoles> getUnitRolesCollection() {
        return unitRolesCollection;
    }

    public void setUnitRolesCollection(Collection<UnitRoles> unitRolesCollection) {
        this.unitRolesCollection = unitRolesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RefRoles)) {
            return false;
        }
        RefRoles other = (RefRoles) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RefRoles[ id=" + id + " ]";
    }

}
