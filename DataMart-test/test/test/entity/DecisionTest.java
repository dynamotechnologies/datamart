package test.entity;

import client.DecisionClient;
import client.ProjectClient;
import client.ProjectForestClient;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.*;
import test.TestData;
import us.fed.fs.www.nepa.schema.decision.Decision;
import us.fed.fs.www.nepa.schema.decision.Decisions;


public class DecisionTest {
    private DecisionClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new DecisionClient();
    }

    @Test
    public void testPost()
    {
        post1();

        // This second POST should fail, because the entity already exists
        try {
            client.postDecision(TestData.decisions.id1,
                    TestData.projects.id1,
                    TestData.projects.type1,
                    TestData.decisions.name2,
                    TestData.appealstatuses.id1,
                    TestData.decisions.constraint1,
                    TestData.decisions.legalnoticedate2,
                    null,
                    null,
                    TestData.decisions.dectype1,
                    TestData.decisions.decdate1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
        
        // Post with optional fields
        post2();
        delete();
    }

    @Test
    public void testGet()
    {
        post1();

        Decision decision = null;
        Decisions decisionlist1 = null;
        Decisions decisionlist2 = null;
        try {
            decision = client.getDecision(TestData.decisions.id1);
        } catch (IOException ex) {
            Logger.getLogger(DecisionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        try {
            decisionlist1 = client.getDecisionList(TestData.projects.id1, TestData.projects.type1);
        } catch (IOException ex) {
            Logger.getLogger(DecisionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        try {
            decisionlist2 = client.getByUnit(TestData.projects.unitcode1);
        } catch (IOException ex) {
            Logger.getLogger(DecisionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.decisions.id1, decision.getId());
        Assert.assertEquals(TestData.decisions.name1, decision.getName());
        Assert.assertEquals(TestData.decisions.constraint1, decision.getConstraint());
        Assert.assertEquals(TestData.decisions.legalnoticedate1, decision.getLegalnoticedate().toString());

        Assert.assertTrue(decisionlist1.getDecision().size()>0);
        Assert.assertTrue(decisionlist2.getDecision().size()>0);
        
        Assert.assertTrue(decisionlist1.getDecision().get(0).getName().length()>0);
        Assert.assertTrue(decisionlist1.getDecision().get(0).getConstraint().length()>0);

        Assert.assertTrue(decisionlist2.getDecision().get(0).getName().length()>0);
        Assert.assertTrue(decisionlist2.getDecision().get(0).getConstraint().length()>0);

        Assert.assertEquals(null, decision.getAreasize());
        Assert.assertEquals(null, decision.getAreaunits());
        
        delete();
        
        // Test optional fields
        post2();
        try {
            decision = client.getDecision(TestData.decisions.id1);
        } catch (IOException ex) {
            Logger.getLogger(DecisionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.decisions.areasize1, decision.getAreasize());
        Assert.assertEquals(TestData.decisions.areaunits1, decision.getAreaunits());
        
        delete();
    }

    @Test
    public void testPut()
    {
        post1();

        Decision decision = null;
        try {
            client.putDecision(TestData.decisions.id1,
                    TestData.projects.id1,
                    TestData.projects.type1,
                    TestData.decisions.name2,
                    TestData.appealstatuses.id1,
                    TestData.decisions.constraint1,
                    TestData.decisions.legalnoticedate2,
                    null,
                    null,
                    TestData.decisions.dectype2,
                    TestData.decisions.decdate2);
            decision = client.getDecision(TestData.decisions.id1);
        } catch (Exception ex) {
            Logger.getLogger(DecisionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.decisions.id1, decision.getId());
        Assert.assertEquals(TestData.decisions.name2, decision.getName());
        Assert.assertEquals(TestData.decisions.constraint1, decision.getConstraint());
        Assert.assertEquals(TestData.decisions.legalnoticedate2, decision.getLegalnoticedate().toString());
        Assert.assertEquals(null, decision.getAreasize());
        Assert.assertEquals(null, decision.getAreaunits());
        Assert.assertEquals(TestData.decisions.dectype2, decision.getDectype());
        Assert.assertEquals(TestData.decisions.decdate2, decision.getDecdate().toString());

        // Test optional fields
        try {
            client.putDecision(TestData.decisions.id1,
                    TestData.projects.id1,
                    TestData.projects.type1,
                    TestData.decisions.name2,
                    TestData.appealstatuses.id1,
                    TestData.decisions.constraint1,
                    TestData.decisions.legalnoticedate2,
                    TestData.decisions.areasize1,
                    TestData.decisions.areaunits1,
                    TestData.decisions.dectype1,
                    TestData.decisions.decdate1);
            decision = client.getDecision(TestData.decisions.id1);
        } catch (Exception ex) {
            Logger.getLogger(DecisionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.decisions.areasize1, decision.getAreasize());
        Assert.assertEquals(TestData.decisions.areaunits1, decision.getAreaunits());
        
        delete();
    }

    @Test
    public void testDelete()
    {
        post1();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getDecision(TestData.decisions.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteDecision(TestData.decisions.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }

    private void post1()
    {
        try {
            client.postDecision(TestData.decisions.id1,
                    TestData.projects.id1,
                    TestData.projects.type1,
                    TestData.decisions.name1,
                    TestData.appealstatuses.id1,
                    TestData.decisions.constraint1,
                    TestData.decisions.legalnoticedate1,
                    null,
                    null,
                    TestData.decisions.dectype1,
                    TestData.decisions.decdate1
                    );

        } catch (IOException ex) {
            Logger.getLogger(DecisionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }
    
    
    private void post2()
    {
        try {
            client.postDecision(TestData.decisions.id1,
                    TestData.projects.id1,
                    TestData.projects.type1,
                    TestData.decisions.name1,
                    TestData.appealstatuses.id1,
                    TestData.decisions.constraint1,
                    TestData.decisions.legalnoticedate1,
                    TestData.decisions.areasize1,
                    TestData.decisions.areaunits1,
                    TestData.decisions.dectype1,
                    TestData.decisions.decdate1);

        } catch (IOException ex) {
            Logger.getLogger(DecisionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }
    

    private void delete()
    {
        try {
            client.deleteDecision(TestData.decisions.id1);
        } catch (IOException ex) {
            Logger.getLogger(DecisionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteDecision(TestData.decisions.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }


    @BeforeClass
    public static void  populateDependencies()
    {
        System.out.println("DecisionTest: Constructing test dependencies");
        ProjectClient projclient = null;
        ProjectForestClient projforestclient = null;
        try {
            projclient = new ProjectClient();
            projforestclient = new ProjectForestClient();
        } catch (Exception ex) {
            Assert.fail();
        }

        // Project Documents rely on Projects. Pre-populate a Project.
        ArrayList<String> purposeids1 = new ArrayList<String>();
        purposeids1.add(TestData.projects.purposeid1);
        purposeids1.add(TestData.projects.purposeid2);
        purposeids1.add(TestData.projects.purposeid3);
        try {
            projclient.postProject(TestData.projects.id1, TestData.projects.type1,
                    TestData.projects.name1, TestData.projects.unitcode1, TestData.projects.description1,
                    TestData.projects.lastupdate1, TestData.projects.commentreg1, TestData.projects.wwwlink1,
                    TestData.projects.wwwsummary1, TestData.projects.wwwpub1, TestData.projects.analysistypeid1,
                    TestData.projects.statusid1, TestData.projects.contactname1, TestData.projects.contactphone1,
                    TestData.projects.contactemail1, purposeids1, TestData.projects.expirationdate1, TestData.projects.projectdocumentid1,
                    TestData.projects.sopapub, TestData.projects.sopanew, TestData.projects.sopaheader, TestData.projects.esd, TestData.projects.cenodecision);
            projforestclient.putProjectForest(TestData.projects.id1, TestData.projects.unitcode1);
        } catch (IOException ex) {
            Logger.getLogger(DecisionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @AfterClass
    public static void  cleanupDependencies()
    {
        System.out.println("DecisionTest: Cleaning up dependencies");
        ProjectForestClient projforestclient = null;
        ProjectClient projclient = null;
        try {
            projforestclient = new ProjectForestClient();
            projclient = new ProjectClient();
        } catch (Exception ex) {
            Assert.fail();
        }

        // Clean up the Project
        try {
            System.out.println("Deleting project forest "+TestData.projects.unitcode1);
            projforestclient.deleteProjectForest(TestData.projects.id1, TestData.projects.unitcode1);
        } catch (IOException ex) {
            Logger.getLogger(DecisionTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            System.out.println("Deleting project "+TestData.projects.id1);
            projclient.deleteProject("nepa", TestData.projects.id1);
        } catch (IOException ex) {
            Logger.getLogger(DecisionTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}