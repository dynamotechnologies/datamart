package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.projectlocation.*;


public class ProjectForestClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public ProjectForestClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.projectlocation", "ProjectLocation.xsd");
    }


    public Forests getProjectForest(String projectid, String id)
            throws IOException
    {
        return get("/projects/nepa/" + projectid + "/locations/forests/" + id, Forests.class);
    }


    public void putProjectForest(String projectid, String id)
            throws IOException
    {
        Forests forest = new Forests();
        forest.getForestid().add(id);
        put(objectfactory.createForests(forest), "/projects/nepa/" + projectid + "/locations/forests/" + id);
    }


    public void deleteProjectForest(String projectid, String id)
            throws IOException
    {
        delete("/projects/nepa/" + projectid + "/locations/forests/" + id);
    }
}