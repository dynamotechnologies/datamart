package entity;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "UnitRoles")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UnitRoles.findAll", query = "SELECT u FROM UnitRoles u"),
    @NamedQuery(name = "UnitRoles.findById", query = "SELECT u FROM UnitRoles u WHERE u.id = :id")})
public class UnitRoles implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @JoinColumn(name = "UnitId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private RefUnits refUnits;
    @JoinColumn(name = "ShortName", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Users users;
    @JoinColumn(name = "RoleId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private RefRoles refRoles;

    public UnitRoles() {
    }

    public UnitRoles(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public RefUnits getRefUnits() {
        return refUnits;
    }

    public void setRefUnits(RefUnits refUnits) {
        this.refUnits = refUnits;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public RefRoles getRefRoles() {
        return refRoles;
    }

    public void setRefRoles(RefRoles refRoles) {
        this.refRoles = refRoles;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UnitRoles)) {
            return false;
        }
        UnitRoles other = (UnitRoles) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.UnitRoles[ id=" + id + " ]";
    }

}
