package stateless;

import javax.ejb.Stateless;
import beanutil.*;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import us.fed.fs.www.nepa.schema.state.*;

/**
 *
 * @author mhsu
 */
@Stateless(mappedName="StateManagerRemote")
public class StateManager implements StateManagerRemote {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void create(State resource)
             throws DataMartException
    {
        validate(resource);
        entity.RefStates entity = createEntity(resource);

        entity.RefStates badentity = createEntity(entity.getId());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        entity = em.merge(entity);
    }

    @Override
    public States getAll()
             throws DataMartException
    {
        // Retrieve all entities from the database
        Query query = em.createQuery("SELECT u FROM RefStates u");
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        States responselist = new States();
        while (it.hasNext()) {
            responselist.getState().add(createResource( (entity.RefStates)it.next() ));
        }
        return responselist;
    }

    @Override
    public String getName(String id)
            throws DataMartException
    {
        entity.RefStates entity = createEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return entity.getName();
    }

    @Override
    public void update(State resource)
             throws DataMartException
    {
        validate(resource);
        entity.RefStates entity = createEntity(resource);

        if (createEntity(entity.getId())==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        entity = em.merge(entity);
    }

    @Override
    public void delete(String id)
             throws DataMartException
    {
        entity.RefStates entity = createEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     *
     * This method does the mapping from jaxb object to entity object and vice versa
     */
    private State createResource(entity.RefStates entity)
            throws DataMartException
    {
        State resource = new State();
        resource.setId(entity.getId());
        resource.setName(entity.getName());
        return resource;
    }

    private entity.RefStates createEntity(State resource)
            throws DataMartException
    {
        entity.RefStates entity = new entity.RefStates();
        entity.setId(resource.getId());
        entity.setName(resource.getName());
        return entity;
    }

    // This version of overloaded method createEntity() accepts a String ID as a parameter, and returns the
    //   associated entity, or null if it doesn't exist
    //
    private entity.RefStates createEntity(String id)
            throws DataMartException
    {
        return em.find(entity.RefStates.class, id);
    }

    private void validate(State resource)
            throws DataMartException
    {
        DataMartBeanUtil.confirmNotNull(resource.getName(), "Name");
    }

}