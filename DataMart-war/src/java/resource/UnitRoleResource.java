package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.UnitRoleManagerRemote;
import us.fed.fs.www.nepa.schema.unitrole.ObjectFactory;
import us.fed.fs.www.nepa.schema.unitrole.Unitrole;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;


@Path ("/unitroles")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces (DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class UnitRoleResource extends AbstractResource
{
    private UnitRoleManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public UnitRoleResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "UnitRole.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.unitrole");
        MY_PROPS.setProperty("bean", "UnitRoleManagerRemote");
        try {
            beanmanager = (UnitRoleManagerRemote) init(UnitRoleManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("UserResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("UserResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;
        try {
            Unitrole role = (Unitrole)unmarshal(inputxml, Unitrole.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+role.getId());

            // All clear. Now do the insert.
            beanmanager.create(role);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the newly created entity
        return created(uri);
    }


    /*
    @GET
    public Response get(@Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get a dump of all the Users in the datamart
            Unitroles rolelist = beanmanager.getAll();

            // Return 404 if none exist
            if (rolelist.getUnitrole().isEmpty())
                notFound();

            // Get the query results in a JAXBElement<Users> object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Unitroles> jaxbelement = objectfactory.createUnitroles(rolelist);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }
     * 
     */


    @GET
    @Path("/{agid}")
    public Response get(@PathParam("agid") int agid, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Retrieve the specified user
            Unitrole role = beanmanager.get(agid);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Unitrole> jaxbelement = objectfactory.createUnitrole(role);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @PUT
    @Path("/{agid}")
    public Response put(@PathParam("agid") int agid, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        Unitrole role = null;
        try {
            role = (Unitrole)unmarshal(inputxml, Unitrole.class);

            if (agid != role.getId())
                complainLoudly(new Exception("Access Group ID in uri must match id in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.update(role);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"</location>");
    }


    @DELETE
    @Path("/{agid}")
    public Response delete(@PathParam("agid") int agid, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(agid);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}
