package test.reference;

import client.AppealStatusClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.appealstatus.*;


public class AppealStatusTest {
    private AppealStatusClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new AppealStatusClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postAppealStatus(TestData.appealstatuses.id1, TestData.appealstatuses.name2);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Appealstatus appealstatus = null;
        Appealstatuses appealstatuslist = null;
        try {
            appealstatus = client.getAppealStatus(TestData.appealstatuses.id1);
            appealstatuslist = client.getAppealStatusList();
        } catch (IOException ex) {
            Logger.getLogger(AppealStatusTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.appealstatuses.id1, appealstatus.getId());
        Assert.assertEquals(TestData.appealstatuses.name1, appealstatus.getName());
        Assert.assertTrue(appealstatuslist.getAppealstatus().get(0).getId().length()>0);
        Assert.assertTrue(appealstatuslist.getAppealstatus().get(0).getName().length()>0);

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        Appealstatus appealstatus = null;
        try {
            client.putAppealStatus(TestData.appealstatuses.id1, TestData.appealstatuses.name2);
            appealstatus = client.getAppealStatus(TestData.appealstatuses.id1);
        } catch (Exception ex) {
            Logger.getLogger(AppealStatusTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.appealstatuses.name2, appealstatus.getName());

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getAppealStatus(TestData.appealstatuses.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteAppealStatus(TestData.appealstatuses.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }

    private void post()
    {
        try {
            client.postAppealStatus(TestData.appealstatuses.id1, TestData.appealstatuses.name1);
        } catch (IOException ex) {
            Logger.getLogger(AppealStatusTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteAppealStatus(TestData.appealstatuses.id1);
        } catch (IOException ex) {
            Logger.getLogger(AppealStatusTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteAppealStatus(TestData.appealstatuses.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }
}