package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.ProjectAdditionalInfoManagerRemote;
import us.fed.fs.www.nepa.schema.ProjectAdditionalInfo.ObjectFactory;
import us.fed.fs.www.nepa.schema.ProjectAdditionalInfo.ProjectAdiitionalInfo;
import us.fed.fs.www.nepa.schema.ProjectAdditionalInfo.ProjectAdiitionalInfos;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;


@Path ("/projects/{projtype}/{projid}/projectAdditionainfo")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class ProjectAdditionalInfoResource extends AbstractResource
{
    private ProjectAdditionalInfoManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public ProjectAdditionalInfoResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "ProjectAdditionalInfo.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.ProjectAdditionalInfo");
        MY_PROPS.setProperty("bean", "ProjectAdditionalInfoManagerRemote");
        try {
            beanmanager = (ProjectAdditionalInfoManagerRemote) init(ProjectAdditionalInfoManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("ProjectAdditionalInfoResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("ProjectAdditionalInfoResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;
        try {
            
            us.fed.fs.www.nepa.schema.ProjectAdditionalInfo.ProjectAdiitionalInfo ProjectAdditionalInfo= (us.fed.fs.www.nepa.schema.ProjectAdditionalInfo.ProjectAdiitionalInfo)unmarshal(inputxml, us.fed.fs.www.nepa.schema.ProjectAdditionalInfo.ProjectAdiitionalInfo.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+ProjectAdditionalInfo.getAddInfoId());

            // All clear. Now do the insert.
            beanmanager.create(ProjectAdditionalInfo);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the newly created entity
        return created(uri);
    }
    @GET
    @Path("/{addInfoId}")
    public Response get(@PathParam("projtype") String projtype, @PathParam("projid") String projid,  @PathParam("addInfoId") Integer addInfoId, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            ProjectAdiitionalInfo projectAddInfo = beanmanager.get(addInfoId.toString());

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<ProjectAdiitionalInfo> jaxbelement = objectfactory.createProjectAdiitionalInfo(projectAddInfo);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }
 /*
     *  Get list of all project implInfo under this project
     */
    @GET
    public Response getAllByProject(@PathParam("projtype") String projtype, @PathParam("projid") String projid, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the name of the specified unit
            ProjectAdiitionalInfos addInfo = beanmanager.getAllByProject(projtype, projid);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<ProjectAdiitionalInfos> jaxbelement = objectfactory.createProjectAdiitionalInfos(addInfo);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }
 

    @PUT
    @Path("/{AddInfoId}")
    public Response put(@PathParam("ProjectId") String projid, @PathParam("AddInfoId") Integer additionalInfoId, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        ProjectAdiitionalInfo ProjectAdditionalInfo = null;
        try {
            ProjectAdditionalInfo = (us.fed.fs.www.nepa.schema.ProjectAdditionalInfo.ProjectAdiitionalInfo) unmarshal(inputxml, us.fed.fs.www.nepa.schema.ProjectAdditionalInfo.ProjectAdiitionalInfo.class);

        
            if (additionalInfoId.compareTo(new Integer(ProjectAdditionalInfo.getAddInfoId())) != 0)
                complainLoudly(new Exception("Resource goal ID in uri must match ID in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

           beanmanager.update(ProjectAdditionalInfo);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("");
    }


    @DELETE
    @Path("/{AddInfoId}")
    public Response delete(@PathParam("AddInfoId") String filename, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(filename);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}

// TODO: restrict access to the admin application