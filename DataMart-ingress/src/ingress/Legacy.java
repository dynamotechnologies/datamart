package ingress;

import client.*;
import ingress.appeals.AppealIngresser;
import ingress.detailprojects.LegacyProjectUnitList;
import ingress.objections.ObjectionIngresser;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.zip.DataFormatException;
import org.w3c.dom.Document;
import us.fed.fs.www.nepa.schema.appeal.Appeal;
import us.fed.fs.www.nepa.schema.appeal.Appeals;
import us.fed.fs.www.nepa.schema.decision.Decision;
import us.fed.fs.www.nepa.schema.decision.Decisions;
import us.fed.fs.www.nepa.schema.decisionappealrule.Decisionappealrule;
import us.fed.fs.www.nepa.schema.decisionappealrule.Decisionappealrules;
import us.fed.fs.www.nepa.schema.decisionmaker.Decisionmaker;
import us.fed.fs.www.nepa.schema.decisionmaker.Decisionmakers;
import us.fed.fs.www.nepa.schema.documentcontainer.Containers;
import us.fed.fs.www.nepa.schema.litigation.Litigation;
import us.fed.fs.www.nepa.schema.litigation.Litigations;
import us.fed.fs.www.nepa.schema.nepacategoricalexclusion.Nepacategoricalexclusion;
import us.fed.fs.www.nepa.schema.nepacategoricalexclusion.Nepacategoricalexclusions;
import us.fed.fs.www.nepa.schema.project.Project;
import us.fed.fs.www.nepa.schema.projectactivity.Projectactivities;
import us.fed.fs.www.nepa.schema.projectactivity.Projectactivity;
import us.fed.fs.www.nepa.schema.projectdocument.Projectdocument;
import us.fed.fs.www.nepa.schema.projectgoal.Projectgoal;
import us.fed.fs.www.nepa.schema.projectgoal.Projectgoals;
import us.fed.fs.www.nepa.schema.projectlocation.Locations;
import us.fed.fs.www.nepa.schema.projectmilestone.Projectmilestone;
import us.fed.fs.www.nepa.schema.projectmilestone.Projectmilestones;
import us.fed.fs.www.nepa.schema.projectresourcearea.Projectresourcearea;
import us.fed.fs.www.nepa.schema.projectresourcearea.Projectresourceareas;
import us.fed.fs.www.nepa.schema.projectspecialauthority.Projectspecialauthorities;
import us.fed.fs.www.nepa.schema.projectspecialauthority.Projectspecialauthority;
import us.fed.fs.www.nepa.schema.unit.Unit;
import us.fed.fs.www.nepa.schema.unit.Units;


public class Legacy {
    private static final int CANT_PUT_DONT_EXIST = 404;
    private Properties ingressprops;
    private String DATA_URL;
    private static final int MIN_ARGS = 1;
    private static final int MAX_ARGS = 2;
    private static final String PROJECT_TYPE = "nepa";

    LegacyExtractor extractor;

    UnitClient unitClient;
    ProjectClient projectClient;
    ProjectDocumentClient docClient;
    DocumentContainerClient containerClient;
    ProjectLocationClient locationClient;
    ProjectActivityClient activityClient;
    ProjectMilestoneClient milestoneClient;
    ProjectSpecialAuthorityClient specAuthClient;
    ProjectGoalClient goalClient;
    ProjectResourceAreaClient resourceAreaClient;
    AppealClient appealClient;
    DecisionClient decisionClient;
    DecisionMakerClient decisionMakerClient;
    LitigationClient litigationClient;
    DecisionAppealRuleClient darClient;
    NepaCategoricalExclusionClient nceClient;

    public Legacy()
            throws IOException
    {
        ingressprops = LegacyUtil.getIngressProps();
        DATA_URL = ingressprops.getProperty("data.path");

        unitClient = new UnitClient();
        projectClient = new ProjectClient();
        docClient = new ProjectDocumentClient();
        containerClient = new DocumentContainerClient();
        locationClient = new ProjectLocationClient();
        activityClient = new ProjectActivityClient();
        milestoneClient = new ProjectMilestoneClient();
        specAuthClient = new ProjectSpecialAuthorityClient();
        goalClient = new ProjectGoalClient();
        resourceAreaClient = new ProjectResourceAreaClient();
        appealClient = new AppealClient();
        decisionClient = new DecisionClient();
        decisionMakerClient = new DecisionMakerClient();
        litigationClient = new LitigationClient();
        darClient = new DecisionAppealRuleClient();
        nceClient = new NepaCategoricalExclusionClient();
    }

    public static void usage() {
        System.out.println("\nUsage: java ingress.Legacy [-t] <xml-file-to-ingress>");
        System.out.println("-t is for test mode. ");
        System.out.println("Example: java ingress.Legacy file:/tmp/xml/ProjectXml_104.xml");
        System.out.println("Note: the client and other dependencies must be on your classpath.\n");
    }

    public static void main(String[] args)
            throws IOException, DataFormatException
    {
        if (args.length < MIN_ARGS || args.length > MAX_ARGS) {
            usage();
            return;
        }
        String ingressfile = null;
        LegacyUtil.setTestMode(false);
        if (args.length==2) {
            if (args[0].equalsIgnoreCase("-t")) {
                LegacyUtil.setTestMode(true);
                ingressfile = args[1];
            } else {
                usage();
                return;
            }
        } else {
            ingressfile = args[0];
        }

        try {
            // Instantiating myself gets around the limitations of being called from a static method
            Legacy ingresstool = new Legacy();

            // Invoke the appropriate ingress method based on the file name format
            if (ingressfile.contains("ProjectXml")) {
                ingresstool.ingressDetails(ingressfile);
            } else if (ingressfile.contains("appeals")) {
                ingresstool.ingressOtherAppeals(ingressfile);
            } else if (ingressfile.contains("objections")) {
                ingresstool.ingressObjections(ingressfile);
            } else {
                throw new DataFormatException("Unknown ingress file name format '" + ingressfile +
                        "'.\nHint: the file name should look like one of these: {'ProjectXml_123.xml','appeals.xml','objections.xml'}.");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println(" hello Legacy Ingress terminated with unhandled exceptions.\n"+ex.getMessage());
        }
    }


    public void ingressDetails(String fileToIngress)
            throws DataFormatException, IOException
    {
        try{
        Document detailProjectsDoc = LegacyUtil.getDocFromUrl(fileToIngress);

      //  LegacyProjectUnitList unitlist = new LegacyProjectUnitList(detailProjectsDoc);
        extractor = new LegacyExtractor(detailProjectsDoc);

        /*
         * Loop over projects and delete all existing subordinate resources.
         * If a project does not show up in the new xml, it's subordinate resources will not be cleared.
         */
        clearOldSubordinates();

        ingressProjects();
        // ingressProjectDocuments();   // Documents now created only by the Broker or through direct requests to the Datamart
        // ingressDocumentContainers(); // PALS is now managing containers directly
        ingressLocations();
        ingressActivities();
        ingressMilestones();
        ingressSpecialAuthorities();
        ingressGoals();
        ingressResourceAreas();
  //      ingressUnits(); disabled, because pals is sending invalid extended details flags

        ingressDecisions();
        ingressDecisionMakers(); //Disabled for now
//        ingressAppeals();     disabled jps 2011-09-30, because we have implemented conflicting requirements and this no longer works
        ingressLitigations();

        ingressDecisionAppealRules();
        ingressNepaCategoricalExclusions();
        }catch(Exception e){
        }
    }


    public void ingressOtherAppeals(String fileToIngress)
            throws DataFormatException, IOException
    {
        // Ingress the non-215 appeals - not associated with a project
        new AppealIngresser().ingressAppeals(
                LegacyUtil.getDocFromUrl(DATA_URL + "/appeals.xml"));
    }


    public void ingressObjections(String fileToIngress)
            throws DataFormatException, IOException
    {
        // Ingress the Objections - not associated with a project
        new ObjectionIngresser().ingressObjections(
                LegacyUtil.getDocFromUrl(DATA_URL + "/objections.xml"));
    }



    /*
     *
     * Private
     */


    private void ingressProjects()
            throws DataFormatException
    {
        List<Project> projects = extractor.getProjects();
        if (projects.isEmpty())
            return;

        for (Project p : projects ) {
            try {
                sendProject(p);
            } catch (Exception ex) {
                System.out.println("Failed to merge project with id '" + p.getId() + "'.\n" + ex.getMessage());
            }
        }
    }


    private void ingressProjectDocuments()
    {
        List<Projectdocument> docs = extractor.getProjectDocuments();
        if (docs.isEmpty())
            return;

        for (Projectdocument d : docs) {
            try {
                sendProjectDocument(d);
            } catch (Exception ex) {
                System.out.println("Failed to merge project document with id '" + d.getDocid() +
                        "' and project id '" + d.getProjectid() + "'.\n" + ex.getMessage());
            }
        }
    }


    private void ingressDocumentContainers()
    {
        HashMap<String, Containers> containermap = extractor.getContainers();
        if (containermap.isEmpty())
            return;

        Iterator it = containermap.keySet().iterator();
        while (it.hasNext())
        {
            String projectid = (String) it.next();
            Containers c = containermap.get(projectid);
            try {
                containerClient.putContainerTree(Legacy.PROJECT_TYPE, projectid, c.getContainerOrContainerdoc());
            } catch (Exception ex) {
                System.out.println("Failed to merge containers for project '" + projectid + "'.\n" + ex.getMessage());
            }
        }
    }


    private void ingressLocations()
    {
        List<Locations> locations = extractor.getLocations();
        if (locations.isEmpty())
            return;

        Iterator it = locations.iterator();
        while (it.hasNext())
        {
            Locations l = (Locations) it.next();
            try {
                locationClient.putProjectLocations(l);
            } catch (Exception ex) {
                System.out.println("Failed to merge location for project with id '" +
                        l.getProjectid() + "'.\n" + ex.getMessage());
            }
        }
    }


    private void ingressActivities()
    {
        HashMap<String, Projectactivities> activitiesmap = new HashMap<String, Projectactivities>();
        // Create a map of projectactivities, indexed by project id
        Iterator it = extractor.getActivities().iterator();
        while (it.hasNext()) {
            Projectactivity act = (Projectactivity) it.next();
            if (null==activitiesmap.get(act.getProjectid()))
                activitiesmap.put(act.getProjectid(), new Projectactivities());
            activitiesmap.get(act.getProjectid()).getProjectactivity().add(act);
        }

        String lastproject = "unknown";
        try {
            for (String thisproject : activitiesmap.keySet()) {
                lastproject = thisproject;
                activityClient.replaceProjectActivityList(activitiesmap.get(thisproject));
            }
        } catch (Exception ex) {
            System.out.println("Failed to merge activities for project with id '" +
                    lastproject + "'.\n" + ex.getMessage());
        }
    }


    private void ingressMilestones()
    {
        HashMap<String, Projectmilestones> milestonesmap = new HashMap<String, Projectmilestones>();
        
        // Create a map of projectmilestones, indexed by project id
        for (Projectmilestone ms : extractor.getMilestones()) {
            String projectid = ms.getProjectid();

            if (null==milestonesmap.get(projectid)) {
                milestonesmap.put(projectid, new Projectmilestones());
            }
            
            milestonesmap.get(projectid).getProjectmilestone().add(ms);
        }

        String lastproject = "unknown";
        try {
            for (String thisproject : milestonesmap.keySet()) {
                lastproject = thisproject;
                milestoneClient.replaceProjectMilestoneList(milestonesmap.get( thisproject ));
            }
        } catch (Exception ex) {
            System.out.println("Failed to merge milestones for project with id '" +
                    lastproject + "'.\n" + ex.getMessage());
        }
    }


    private void ingressSpecialAuthorities()
            throws IOException
    {
        List<Projectspecialauthority> authorities = extractor.getSpecialAuthorities();
        if (authorities.isEmpty())
            return;

        for (Projectspecialauthority a : authorities)
        {
            try {
                specAuthClient.putProjectSpecialAuthority(a.getProjectid(), a.getSpecialauthorityid());
            } catch (Exception ex) {
                System.out.println("Failed to merge special authority for project with id '" +
                        a.getProjectid() + "'.\n" + ex.getMessage());
            }
        }
    }


    private void ingressGoals()
            throws IOException
    {
        List<Projectgoal> goals = extractor.getGoals();
        if (goals.isEmpty())
            return;

        for (Projectgoal g : goals)
        {
            try {
                goalClient.postProjectGoal(g);
            } catch (Exception ex) {
                System.out.println("Failed to merge goal for project with id '" +
                        g.getProjectid() + "' and type '" + g.getProjecttype() + "'.\n" + ex.getMessage());
            }
        }
    }
    
    
    private void ingressResourceAreas()
            throws IOException
    {
        List<Projectresourcearea> resourceareas = extractor.getResourceAreas();
        if (resourceareas.isEmpty())
            return;

        for (Projectresourcearea a : resourceareas)
        {
            try {
                resourceAreaClient.postProjectResourceArea(a);
            } catch (Exception ex) {
                System.out.println("Failed to merge resource area for project with id '" +
                        a.getProjectid() + "' and type '" + a.getProjecttype() + "'.\n" + ex.getMessage());
            }
        }
    }



    private void ingressAppeals()
            throws IOException
    {
        List<Appeal> appeals = extractor.getAppeals();
        if (appeals.isEmpty())
            return;

        for (Appeal a : appeals)
        {
            try {
                sendAppeal(a);
            } catch (Exception ex) {
                System.out.println("Failed to merge appeal for project with id '" +
                        a.getProjectid() + "'.\n" + ex.getMessage());
            }
        }
    }


/**    private void ingressUnits()
            throws IOException
    {
        Units refunits = unitClient.getUnitList();
        List<Unit> units = extractor.getUnits();
        if (units.isEmpty())
            return;

        Iterator it = units.iterator();
        while (it.hasNext())
        {
            Unit u = (Unit) it.next();
            if (!u.isExtendeddetails())
                continue;
            try {
                // The legacy xml contains no unit information other than code, and the extended details flag
                // Instead of sending the extracted unit, we send the matching reference unit with the flag updated
                Iterator refit = refunits.getUnit().iterator();
                while (refit.hasNext())
                {
                    Unit refu = (Unit) refit.next();
                    if (!refu.getCode().equalsIgnoreCase(u.getCode()))
                        continue;
                    refu.setExtendeddetails(true);
                    unitClient.putUnit(refu);
                    break;
                }
            } catch (Exception ex) {
                System.out.println("Failed to merge unit with code '" +
                        u.getCode() + "'.\n" + ex.getMessage());
            }
        }
    }
*/

    private void ingressDecisions()
        throws IOException
    {
        for (Decision dec : extractor.getDecisions()) {
            try {
                // Remap the decision type from a PALS ID to a Decision Type code (DM/DN/ROD)
                String palsdectype = dec.getDectype();
                if (palsdectype != "") {
                        String dectype = LegacyUtil.getDecisionTypeIdFromPalsId(Integer.parseInt(palsdectype));
                        dec.setDectype(dectype);
                }
                // Attempt to PUT (update) the entity. This will fail if it doesn't already exist.
                decisionClient.putDecision(dec);
            } catch (IOException ex) {
                // If the PUT failed with some unexpected http status code, re-throw the exception
                if (decisionClient.getStatus()!=CANT_PUT_DONT_EXIST)
                    throw ex;
                // ...Otherwise, POST a new entity because it doesn't exist yet.
                decisionClient.postDecision(dec);
            } catch (DataFormatException ex) {
                // Could not translate decision type
                throw new IOException(ex.getMessage());
            }
        }
    }

    
    private void ingressLitigations()
        throws IOException
    {
        for (Litigation lit : extractor.getLitigations()) {
            try {
                // Attempt to PUT (update) the entity. This will fail if it doesn't already exist.
                litigationClient.putLitigation(lit);
            } catch (IOException ex) {
                // If the PUT failed with some unexpected http status code, re-throw the exception
                if (litigationClient.getStatus()!=CANT_PUT_DONT_EXIST)
                    throw ex;
                // ...Otherwise, POST a new entity because it doesn't exist yet.
                litigationClient.postLitigation(lit);
            }
        }
    }


    private void ingressDecisionAppealRules()
            throws IOException
    {
        for (Decisionappealrule dar : extractor.getDecisionAppealRules()) {
            darClient.postDecisionAppealRule(dar);
        }
    }
    
    private void ingressDecisionMakers()
        throws IOException
    {
        for (Decisionmaker dar : extractor.getDecisionMakers()) {
            decisionMakerClient.postDecisionMaker(dar);
        }
        
    }
    
    private void ingressNepaCategoricalExclusions()
            throws IOException
    {
        for (Nepacategoricalexclusion nce : extractor.getNepaCategoricalExclusions()) {
            nceClient.postNepaCategoricalExclusion(nce);
        }
    }


    /*
     * Indiscriminately destroys the subordinate resources of all projects in the xml.
     * 
     */
    private void clearOldSubordinates()
            throws IOException
    {
        List<Project> projects = extractor.getProjects();
        if (projects.isEmpty())
            return;

        for (Project p : projects ) {
            String projectid = p.getId();
            
            // Confirm that this project exists in the datamart
            try {
                projectClient.getProject(PROJECT_TYPE, p.getId());
            } catch (IOException e) {
                if (projectClient.getStatus()!=404) {
                    throw new IOException(e);
                } else {
                    // if the project does not exist, skip to the next project
                    continue;
                }
            }

            // Clear out old special authorities
            Projectspecialauthorities oldspecauths = specAuthClient.getProjectSpecialAuthorityList(projectid);
            for (Projectspecialauthority s : oldspecauths.getProjectspecialauthority()) {
                specAuthClient.deleteProjectSpecialAuthority(projectid, s.getSpecialauthorityid());
            }

            // Clear out old nce's
            Nepacategoricalexclusions oldnces = nceClient.getNepaCategoricalExclusionList(projectid);
            for (Nepacategoricalexclusion n : oldnces.getNepacategoricalexclusion()) {
                nceClient.deleteNepaCategoricalExclusion(projectid, n.getCategoricalexclusionid());
            }
            
            // Clear out old goals
            Projectgoals oldgoals = goalClient.getProjectGoalsByProject(PROJECT_TYPE, projectid);
            for (Projectgoal g : oldgoals.getProjectgoal()) {
                goalClient.deleteProjectGoal(PROJECT_TYPE, projectid, g.getGoalid());
            }
            
            // Clear out old resource areas
            Projectresourceareas oldareas = resourceAreaClient.getProjectResourceAreasByProject(PROJECT_TYPE, projectid);
            for (Projectresourcearea ra : oldareas.getProjectresourcearea()) {
                resourceAreaClient.deleteProjectResourceArea(PROJECT_TYPE, projectid, ra.getResourceareaid());
            }

            // Clear out old decisions
            clearOldDecisions(projectid);
        }
    }


    /*
     * Indiscriminately destroys all decisions (and subordinate resources) for a specified project
     *
     */
    private void clearOldDecisions(String projectid)
            throws IOException
    {
        // Gets a list of all appeals in the DataMart. There is currently no service to retrieve appeals by decision id.
        Appeals oldapps = appealClient.getAppealList();

        Decisions olddecisions = decisionClient.getDecisionList(projectid, Legacy.PROJECT_TYPE);
        for (Decision d : olddecisions.getDecision()) {
            Integer did = d.getId();

            // Clear out old litigations for this decision
            Litigations oldlits = litigationClient.getLitigationList(did);
            for (Litigation l : oldlits.getLitigation()) {
                litigationClient.deleteLitigation(l.getId());
            }

            // Clear out DAR's for this decision
            Decisionappealrules darlist = darClient.getDecisionAppealRuleList(did);
            for (Decisionappealrule dar : darlist.getDecisionappealrule()) {
                darClient.deleteDecisionAppealRule(did, dar.getAppealrule());
            }

            Decisionmakers decMakerslist = decisionMakerClient.getDecisionMakerList(did);
            for (Decisionmaker dar : decMakerslist.getDecisionmaker()){
                decisionMakerClient.deleteDecisionMaker(did, dar.getId());
            }
            
            // Clear out old appeals for this decision
            for (Appeal appeal : oldapps.getAppeal()) {
                if (appeal.getDecisionid()!=null && appeal.getDecisionid().equals(did.toString())) {
                    appealClient.deleteAppeal(appeal.getId());
                }
            }

            // Delete the actual decision
            decisionClient.deleteDecision(d.getId());
        }
    }


    private void sendProject(Project p)
            throws IOException
    {
        try {
            // Attempt to PUT (update) the project. This will fail if it doesn't already exist.
            projectClient.putProject(p);
        } catch (IOException ex) {
            // If the PUT failed with some unexpected http status code, re-throw the exception
            if (projectClient.getStatus()!=CANT_PUT_DONT_EXIST)
                throw ex;
            // ...Otherwise, POST a new project because it doesn't exist yet.
            projectClient.postProject(p);
        }
    }


    private void sendProjectDocument(Projectdocument projdoc)
            throws IOException
    {
        try {
            docClient.putProjectDocument(projdoc);
        } catch (IOException ex) {
            // If the PUT failed with some unexpected http status code, re-throw the exception
            if (docClient.getStatus()!=CANT_PUT_DONT_EXIST)
                throw ex;
            // ...Otherwise, POST a new project document because it doesn't exist yet.
            docClient.postProjectDocument(projdoc);
        }
    }


    private void sendAppeal(Appeal appeal)
            throws IOException
    {
        try {
            appealClient.putAppeal(appeal);
        } catch (IOException ex) {
            // If the PUT failed with some unexpected http status code, re-throw the exception
            if (appealClient.getStatus()!=CANT_PUT_DONT_EXIST)
                throw ex;
            // ...Otherwise, POST a new project document because it doesn't exist yet.
            appealClient.postAppeal(appeal);
        }
    }

    private void sendDecisionmaker(Decisionmaker dm)
            throws IOException
    {
        
        try {
            decisionMakerClient.putDecisionMaker(dm);
        } catch (IOException ex) {
            // If the PUT failed with some unexpected http status code, re-throw the exception
            if (decisionMakerClient.getStatus()!=CANT_PUT_DONT_EXIST)
                throw ex;
            // ...Otherwise, POST a new project document because it doesn't exist yet.
            decisionMakerClient.postDecisionMaker(dm);
        }
    }

}