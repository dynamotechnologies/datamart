package schema;

import lombok.extern.slf4j.Slf4j;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.LSResourceResolver;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.*;

/*
 *
 * @James 2010-10-20
 * This class exists because method Class.getResourceAsStream() is a prickly bitch and cannot be trusted,
 *   unless the schema files reside in the same directory as the calling class. Putting this class in the same
 *   package as the schema files fulfills that prerequisite. Unfortunately this package is private, so some
 *   other means must be responsible for distributing the schema files in order to ensure that clients are
 *   validating to the same standard.
 *
 * Both ServletConfig.getServletContext.getResourceAsStream() and @javax.ws.rs.core.Context (container injected) sound
 *   like attractive alternatives, but neither will play nice unless accessed via the container as a Servlet.
 *   ---
 *   In other words this will return null every time:
 *   AsdfServlet asdfservlet = new AsdfServlet();
 *   asdfservlet.getServletConfig.getServletContext.getResourceAsStream("/schemas/asdf.xsd");
 *
 */
@Slf4j
public class DataMartSchemaGetter
{
    private static final SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
    private static MyLSResourceResolver myResolver = null;

    // Prevent instantiation
    private DataMartSchemaGetter() {}


    public static Schema getSchema(String schemafile)
            throws IOException
    {
        if (myResolver == null) {
            try {
                myResolver = new MyLSResourceResolver();
            }
            catch (ClassNotFoundException e) {
                throw (new IOException("Can't init resource resolver, ClassNotFound Exception"));
            }
            catch (IllegalAccessException e) {
                throw (new IOException("Can't init resource resolver, IllegalAccessException"));
            }
            catch (InstantiationException e) {
                throw (new IOException("Can't init resource resolver, Instantiation Exception"));
            }
            sf.setResourceResolver(myResolver);
        }

        if (!schemafile.endsWith(".xsd"))
            throw new IOException("Could not retrieve schema file.");
        //
        // TODO: test filename for special characters, etc. should be alphanumeric only
        //
        try {
            InputStream schemastream = DataMartSchemaGetter.class.getResourceAsStream(schemafile);
            StreamSource schemasource = new StreamSource(schemastream);
            return sf.newSchema(schemasource);
        } catch (SAXException ex) {
            log.error(ex.toString());
            throw new IOException("Could not parse schema file '"+schemafile+"'");
        }
    }

    public static String getSchemaAsString(String schemafile)
            throws IOException
    {
        if (!schemafile.endsWith(".xsd"))
            throw new IOException("Could not retrieve schema file '"+schemafile+"'");
        //
        // TODO: test filename for special characters, etc. should be alphanumeric only
        //
        InputStream schemastream = DataMartSchemaGetter.class.getResourceAsStream(schemafile);
        String s = convertStreamToString(schemastream);
        if (!(s.length()>0))
            throw new IOException("Could not retrieve schema file '"+schemafile+"'");
        return s;
    }

    private static String convertStreamToString(InputStream is)
            throws IOException {
        if (is != null) {
            Writer writer = new StringWriter();

            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(
                        new InputStreamReader(is, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            } finally {
                is.close();
            }
            return writer.toString();
        } else {
            return "";
        }
    }

    /*
     * When one Schema includes another, the Netbeans IDE indicates the included
     * Schema with a relative path, using "../" at the start of the schemaLocation.
     * This works fine at compile time, where it is treated as a filesystem path,
     * but breaks at runtime, where it is treated as a resource path within the jarfile.
     *
     * This Resource Resolver rewrites any resource path starting with "../" so
     * that the Schema Getter can find the appropriate schema file.
     */
    private static class MyLSResourceResolver implements LSResourceResolver {

        private DOMImplementationLS domImplementationLS;

        private MyLSResourceResolver()
                throws ClassNotFoundException, IllegalAccessException, InstantiationException
        {
            DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
            domImplementationLS = (DOMImplementationLS) registry.getDOMImplementation("LS");
        }

        public LSInput resolveResource(String type, String namespaceURI, String publicId, String systemId, String baseURI)
        {
            // System.out.println("==== Resolving '" + type + "' '" + namespaceURI + "' '" + publicId + "' '" + systemId + "' '" + baseURI + "'");

            LSInput lsInput = domImplementationLS.createLSInput();
            String newLocation = "";
            if (systemId.indexOf("../") == 0) {
                newLocation = "/" + systemId.substring(3);
            } else {
                newLocation = "/" + systemId;
            }
            InputStream is = getClass().getResourceAsStream(newLocation);
            lsInput.setByteStream(is);
            lsInput.setSystemId(systemId);
            return lsInput;
        }
    }
}
