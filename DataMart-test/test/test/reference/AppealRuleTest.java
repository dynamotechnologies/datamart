package test.reference;

import client.AppealRuleClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.appealrule.*;


public class AppealRuleTest {
    private AppealRuleClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new AppealRuleClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postAppealRule(TestData.appealrules.id1, TestData.appealrules.rule2, TestData.appealrules.desc1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Appealrule appealrule = null;
        Appealrules appealrulelist = null;
        try {
            appealrule = client.getAppealRule(TestData.appealrules.id1);
            appealrulelist = client.getAppealRuleList();
        } catch (IOException ex) {
            Logger.getLogger(AppealRuleTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.appealrules.id1, appealrule.getId());
        Assert.assertEquals(TestData.appealrules.rule1, appealrule.getRule());
        Assert.assertTrue(appealrulelist.getAppealrule().get(0).getId().length()>0);
        Assert.assertTrue(appealrulelist.getAppealrule().get(0).getRule().length()>0);

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        Appealrule appealrule = null;
        try {
            client.putAppealRule(TestData.appealrules.id1, TestData.appealrules.rule2, TestData.appealrules.desc1);
            appealrule = client.getAppealRule(TestData.appealrules.id1);
        } catch (Exception ex) {
            Logger.getLogger(AppealRuleTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.appealrules.rule2, appealrule.getRule());

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getAppealRule(TestData.appealrules.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteAppealRule(TestData.appealrules.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }

    private void post()
    {
        try {
            client.postAppealRule(TestData.appealrules.id1, TestData.appealrules.rule1, TestData.appealrules.desc1);
        } catch (IOException ex) {
            Logger.getLogger(AppealRuleTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteAppealRule(TestData.appealrules.id1);
        } catch (IOException ex) {
            Logger.getLogger(AppealRuleTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteAppealRule(TestData.appealrules.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }
}