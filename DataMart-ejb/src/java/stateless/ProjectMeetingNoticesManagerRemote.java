/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateless;



import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.ProjectMeetingNotices.*;
import beanutil.DataMartException;

@Remote
public interface ProjectMeetingNoticesManagerRemote {
/*
 *
 * The Manager beans are responsible for:
 *   1) Implementing business logic and
 *   2) Translating front-end resources to back-end database entities.
 *
 * Often the resources will consist of data from numerous database tables. It is the manager's responsibility
 *   to ensure that these front-end resources can be accessed as though they were cut from a single piece of wood.
 *
 */
    public void create(Meetingnotice resource) throws DataMartException;
    public Meetingnotice get(String projecttype, String projectid, Integer meetingId) throws DataMartException;
    public Meetingnotices getAllByProject(String projecttype, String projectid) throws DataMartException;
    public void update(Meetingnotice resource) throws DataMartException;
    public void delete(String projecttype, String projectid, Integer meetingId) throws DataMartException;
    public Boolean ping();
}

