/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.ProjectViewDoc.*;

/**
 *
 * @author gauri
 */

public class ProjectViewDocsClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public ProjectViewDocsClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.ProjectViewDoc", "ProjectViewDoc.xsd");
    }


   


    public void postProjectViewDocs(String projectId,ProjectViewDoc projectViewDoc)
            throws IOException
    {
      post(objectfactory.createProjectViewDoc(projectViewDoc), "/projects/nepa/"+projectId+"/views/"+projectViewDoc.getViewId()+"/docs");

    }


    public ProjectViewDocs getProjectViewDocs(String projectId,String viewId)
            throws IOException
    {
        return get("/projects/nepa/"+projectId+"/views/"+viewId+"/docs/" , ProjectViewDocs.class);
    }



    


    public void putProjectViewDocs(String projectId,ProjectViewDoc projectViewDoc)
            throws IOException
    {
     put(objectfactory.createProjectViewDoc(projectViewDoc), "/projects/nepa/"+projectId+"/views/"+projectViewDoc.getViewId()+"/docs/" + projectViewDoc.getId());
    }


    public void deleteProjectViewDocs(String projectId,String viewId, String viewDocId)
            throws IOException
    {
        delete("/projects/nepa/"+projectId+"/views/"+viewId+"/docs/" + viewDocId);
    }
    
 
          
}