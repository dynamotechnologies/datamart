package ingress.detailprojects;

import ingress.LegacyUtil;
import java.util.Date;
import java.util.zip.DataFormatException;
import javax.xml.datatype.XMLGregorianCalendar;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 *
 * <!DOCTYPE unitprojectsdetail [
 * <!ELEMENT unitprojectsdetail (timestamp, unit*)>
 * <!ELEMENT timestamp (#PCDATA)>
 * <!ELEMENT unit (code,extendeddetails,project+)>
 * <!ELEMENT code (#PCDATA)>
 * <!ELEMENT extendeddetails (#PCDATA)>
 * <!ELEMENT project (id,projectdocumentid,expirationdate?,name,status,type,url?,appealslink,objectionslink,description,projectsummary,contact,catexclusion*,specauth+,milestone*,purpose,adminunit,activity+,commreg?,esd?,cenodecision?,location?,decision*,pubflag,updatedate,sopa,primaryProjectManager?,secondaryProjectManager?,dataEntryPerson?,goal*,resourcearea*)>
 * <!ELEMENT id (#PCDATA)>
 * <!ELEMENT projectdocumentid (#PCDATA)>
 * <!ELEMENT expirationdate (#PCDATA)>
 * <!ELEMENT name (#PCDATA)>
 * <!ELEMENT status (#PCDATA)>
 * <!ELEMENT type (#PCDATA)>
 * <!ELEMENT url (#PCDATA)>
 * <!ELEMENT appealslink (#PCDATA)>
 * <!ELEMENT objectionslink (#PCDATA)>
 * <!ELEMENT description (#PCDATA)>
 * <!ELEMENT projectsummary (#PCDATA)>
 * <!ELEMENT contact (contactShortName,contactname, contactphone, contactemail)>
 * <!ELEMENT contactShortName (#PCDATA)>
 * <!ELEMENT contactname (#PCDATA)>
 * <!ELEMENT contactphone (#PCDATA)>
 * <!ELEMENT contactemail (#PCDATA)>
 * <!ELEMENT catexclusion (#PCDATA)>
 * <!ELEMENT specauth (#PCDATA)>
 * <!ELEMENT milestone (msseq,mstype,msdate,msstatus,mshistoryflag)>
 * <!ELEMENT msseq (#PCDATA)>
 * <!ELEMENT mstype (#PCDATA)>
 * <!ELEMENT msdate (#PCDATA)>
 * <!ELEMENT msstatus (#PCDATA)>
 * <!ELEMENT mshistoryflag (#PCDATA)>
 * <!ELEMENT purpose (purposename+)>
 * <!ELEMENT purposename (#PCDATA)>
 * <!ELEMENT adminunit (#PCDATA)>
 * <!ELEMENT activity (#PCDATA)>
 * <!ELEMENT commreg (#PCDATA)>
 * <!ELEMENT location (locdesc,locfor+,locdst+,locstate+,loccount+,loclegal?,loclat?,loclong?)>
 * <!ELEMENT locdesc (#PCDATA)>
 * <!ELEMENT locfor (#PCDATA)>
 * <!ELEMENT locdst (#PCDATA)>
 * <!ELEMENT locstate (#PCDATA)>
 * <!ELEMENT loccount (#PCDATA)>
 * <!ELEMENT loclegal (#PCDATA)>
 * <!ELEMENT loclat (#PCDATA)>
 * <!ELEMENT loclong (#PCDATA)>
 * <!ELEMENT decision (decname,decisionId,decnotice?,decconstraint,decapprule,decappstat?,decapp*,declit*,decareasize?,decareaunits?)>
 * <!ELEMENT decname (#PCDATA)>
 * <!ELEMENT decisionId (#PCDATA)>
 * <!ELEMENT decnotice (#PCDATA)>
 * <!ELEMENT decconstraint (#PCDATA)>
 * <!ELEMENT decapprule (#PCDATA)>
 * <!ELEMENT decappstat (#PCDATA)>
 * <!ELEMENT decapp (appealid,appealadminunitcode,rule,appellant?,outcome?,response?)>
 * <!ELEMENT appealid (#PCDATA)>
 * <!ELEMENT appealadminunitcode (#PCDATA)>
 * <!ELEMENT rule (#PCDATA)>
 * <!ELEMENT appellant (#PCDATA)>
 * <!ELEMENT outcome (#PCDATA)>
 * <!ELEMENT response (date,documentid?,appealresponsefilesize?)>
 * <!ELEMENT date (#PCDATA)>
 * <!ELEMENT documentid (#PCDATA)>
 * <!ELEMENT appealresponsefilesize (#PCDATA)>
 * <!ELEMENT declit (litid,litname,litstat,litoutcome?,litclosed?)>
 * <!ELEMENT litid (#PCDATA)>
 * <!ELEMENT litname (#PCDATA)>
 * <!ELEMENT litstat (#PCDATA)>
 * <!ELEMENT litoutcome (#PCDATA)>
 * <!ELEMENT litclosed (#PCDATA)>
 * <!ELEMENT pubflag (#PCDATA)>
 * <!ELEMENT updatedate (#PCDATA)>
 * <!ELEMENT sopa (sopaFlag,isNewProjectFlag?,sopaHeaderUnit?)>
 * <!ELEMENT sopaFlag (#PCDATA)>
 * <!ELEMENT isNewProjectFlag (#PCDATA)>
 * <!ELEMENT sopaHeaderUnit (#PCDATA)>
 * <!ELEMENT primaryProjectManager (#PCDATA)>
 * <!ELEMENT secondaryProjectManager (#PCDATA)>
 * <!ELEMENT dataEntryPerson (#PCDATA)>
 * <!ELEMENT goal (goalid,goalorder?)>
 * <!ELEMENT goalid (#PCDATA)>
 * <!ELEMENT goalorder (#PCDATA)>
 * <!ELEMENT resourcearea (resourceareaid,resourceareaorder?)>
 * <!ELEMENT resourceareaid (#PCDATA)>
 * <!ELEMENT resourceareaorder (#PCDATA)>
 * <!ELEMENT decareasize (#PCDATA)>
 * <!ELEMENT decareaunits (#PCDATA)>
 * <!ATTLIST unitprojects xmlns CDATA 'http://www.fs.fed.us/pals/xmlns/unitprojectsdetail/'>
 * ]>
 *
 * This class is responsible for encapsulating element `response` as specified above
 */

public class LegacyDecisionResponse {
    private XMLGregorianCalendar date;
    private String documentid = ""; // Optional
    private String filesize = ""; // Optional

    public LegacyDecisionResponse()
    {
        // allow instantiation without nodes
    }

    public LegacyDecisionResponse(Node n)
            throws DataFormatException
    {
        NodeList mynodes = n.getChildNodes();
        for (int i=0; i<mynodes.getLength(); i++)
        {
            if (mynodes.item(i).getNodeName().equals("date"))
                setDate(mynodes.item(i).getTextContent());
            if (mynodes.item(i).getNodeName().equals("documentid"))
                documentid = mynodes.item(i).getTextContent();
            if (mynodes.item(i).getNodeName().equals("appealresponsefilesize"))
                filesize = mynodes.item(i).getTextContent();
        }

        if (date==null || !(date.toString().length()>0))
            throw new DataFormatException("date not found.");
    }

    private void setDate(String d)
            throws DataFormatException
    {
        Date d2 = LegacyUtil.getDateFromString(d, "Response Date");
        date = LegacyUtil.getXMLGregorianFromDateTime(d2);
    }

    public XMLGregorianCalendar getDate()
    {
        return date;
    }

    public String getDocId()
    {
        return documentid;
    }

    public String getFileSize()
    {
        return filesize;
    }
}