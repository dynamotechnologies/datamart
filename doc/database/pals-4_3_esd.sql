-- Emergency Situation Determination (ESD) flag for Projects
ALTER TABLE Projects ADD COLUMN Esd tinyint(1) NOT NULL DEFAULT 0;

