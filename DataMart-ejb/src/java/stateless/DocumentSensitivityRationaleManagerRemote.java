package stateless;

import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.documentsensitivityrationale.*;
import beanutil.DataMartException;

/*
 *
 * The Manager beans are responsible for:
 *   1) Implementing business logic and
 *   2) Translating front-end resources to back-end database entities.
 *
 * Often the resources will consist of data from numerous database tables. It is the manager's responsibility
 *   to ensure that these front-end resources can be accessed as though they were cut from a single piece of wood.
 *
 */
@Remote
public interface DocumentSensitivityRationaleManagerRemote {
    public void create(Documentsensitivityrationale resource) throws DataMartException;
    public void update(Documentsensitivityrationale resource) throws DataMartException;
    public Documentsensitivityrationale get(Integer id) throws DataMartException;
    public Documentsensitivityrationales getAll() throws DataMartException;
    public void delete(Integer id) throws DataMartException;
    public Boolean ping();
}