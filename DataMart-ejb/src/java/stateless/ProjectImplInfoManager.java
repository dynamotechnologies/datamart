package stateless;

import javax.ejb.Stateless;
import beanutil.*;
import java.sql.Date;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import static stateless.ProjectMeetingNoticesManager.fill;

import us.fed.fs.www.nepa.schema.ProjectImplInfo.*;

@Stateless(mappedName="ProjectImplInfoManagerRemote")
public class ProjectImplInfoManager implements ProjectImplInfoManagerRemote {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void create(us.fed.fs.www.nepa.schema.ProjectImplInfo.ProjectImplInfo resource)
             throws DataMartException
    {
        validate(resource);
        entity.ProjectImplInfo entity = createEntity(resource);

        entity.ProjectImplInfo badentity = createEntity(entity.getImplId());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        entity = em.merge(entity);
    }

    @Override
    public ProjectImplInfo get(String key)
             throws DataMartException
    {
        entity.ProjectImplInfo entity = createEntity(new Integer(key));
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return createResource(entity);
    }
    public ProjectImplInfos getAllByProject(String projecttype, String projectid)
            throws DataMartException {
        // Get Project
        entity.Projects project = DataMartBeanUtil.getProject(em, projecttype, projectid);

        // Retrieve project resource area entities from the database
        Query query = em.createQuery("SELECT a FROM ProjectImplInfo a WHERE a.projectid = :project");
        query.setParameter("project", project);
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        ProjectImplInfos responselist = new ProjectImplInfos();
        while (it.hasNext()) {
            responselist.getProjectImplInfo().add(createResource((entity.ProjectImplInfo)it.next()));
        }
        return responselist;
    }
    @Override
    public void update(ProjectImplInfo resource)
             throws DataMartException
    {
        validate(resource);
        entity.ProjectImplInfo entity = createEntity(resource);

        if (createEntity(entity.getImplId())==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        entity = em.merge(entity);
    }

    @Override
    public void delete(String key)
             throws DataMartException
    {
        entity.ProjectImplInfo entity = createEntity(new Integer(key));
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     *
     * This method does the mapping from jaxb object to entity object and vice versa
     */
    private ProjectImplInfo createResource(entity.ProjectImplInfo entity)
            throws DataMartException
    {
        ProjectImplInfo resource = new ProjectImplInfo();
        resource.setImplInfoId(entity.getImplId().toString());
        resource.setProjectId(entity.getProjectid().getPublicId());
        resource.setDescription(entity.getDescription());
        resource.setLastUpdateBy(entity.getLastUpdatedBy());
        Calendar cal = Calendar.getInstance();
        cal.setTime(entity.getLastUpdatedDate());
        String formatedDate = fill('0',2,""+(cal.get(Calendar.MONTH) + 1) )+ "/" + cal.get(Calendar.DATE) + "/" +cal.get(Calendar.YEAR);
        resource.setLasteUpdateDate(formatedDate);    

        return resource;
    }

    private entity.ProjectImplInfo createEntity(ProjectImplInfo resource)
            throws DataMartException
    {
        entity.ProjectImplInfo entity = new entity.ProjectImplInfo();
        entity.Projects project = DataMartBeanUtil.getProject(em, "nepa", resource.getProjectId());
       // entity..setProjects(project);
        
        //entity.setAddInfoId(Integer.SIZE);
        entity.setImplId(new Integer(resource.getImplInfoId()));
        entity.setDescription(resource.getDescription());
        entity.setLastUpdatedBy(resource.getLastUpdateBy());
        entity.setLastUpdatedDate(Date.valueOf(resource.getLasteUpdateDate()));
        entity.setProjectid(project);        
        return entity;
    }

    private entity.ProjectImplInfo createEntity(Integer key)
            throws DataMartException
    {
        return em.find(entity.ProjectImplInfo.class, key);
    }

    private void validate(ProjectImplInfo resource)
            throws DataMartException
    {
         //Ensure these are not empty strings
        DataMartBeanUtil.confirmNotNull(resource.getImplInfoId(), "implInfoId");
        DataMartBeanUtil.confirmNotNull(resource.getProjectId(), "projectid");
    }

}