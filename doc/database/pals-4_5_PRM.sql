
-- Fields for Document Metadata originally stored in UCM
ALTER TABLE ProjectDocuments ADD COLUMN DocumentDate varchar(50);
ALTER TABLE ProjectDocuments ADD COLUMN Author varchar(255);
ALTER TABLE ProjectDocuments ADD COLUMN Addressee varchar(255);
ALTER TABLE ProjectDocuments ADD COLUMN DateLabel varchar(255);
alter table ProjectDocuments add column SensitivityRationale int(11);
ALTER TABLE ProjectDocuments ADD COLUMN DateAdded datetime;
ALTER TABLE ProjectDocuments ADD COLUMN AddedBy varchar(255);
ALTER TABLE ProjectDocuments ADD CONSTRAINT FkProjectDocuments_SensitivityRationale FOREIGN KEY (SensitivityRationale) REFERENCES RefDocumentSensitivityRationales (Id);


ALTER TABLE ProjectDocumentContainers ADD COLUMN FixedFlag tinyint(1) NOT NULL DEFAULT 0;




