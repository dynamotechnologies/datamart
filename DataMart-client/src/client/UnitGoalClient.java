package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.unitgoal.*;


public class UnitGoalClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public UnitGoalClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.unitgoal", "UnitGoal.xsd");
    }


    public void postUnitGoal(String unitid, Integer goalid, Integer order)
            throws IOException
    {
        Unitgoal unitgoal = new Unitgoal();
        unitgoal.setUnitid(unitid);
        unitgoal.setGoalid(goalid);
        unitgoal.setOrder(order);
        postUnitGoal(unitgoal);
    }


    public void postUnitGoal(Unitgoal unitgoal)
            throws IOException
    {
        String unitid  = unitgoal.getUnitid();
        post(objectfactory.createUnitgoal(unitgoal), "/ref/units/"+unitid+"/goals/");
    }


    public Unitgoal getUnitGoal(String unitid, Integer goalid)
            throws IOException
    {
        return get("/ref/units/"+unitid+"/goals/"+ goalid, Unitgoal.class);
    }

    public Unitgoals getUnitGoalsByUnit(String unitid)
            throws IOException
    {
        return get("/ref/units/"+unitid+"/goals/", Unitgoals.class);
    }
    
    public void putUnitGoal(String unitid, Integer goalid, Integer order)
            throws IOException
    {
        Unitgoal unitgoal = new Unitgoal();
        unitgoal.setUnitid(unitid);
        unitgoal.setGoalid(goalid);
        unitgoal.setOrder(order);
        putUnitGoal(unitgoal);
    }


    public void putUnitGoal(Unitgoal unitgoal)
            throws IOException
    {
        String unitid = unitgoal.getUnitid();
        Integer goalid = unitgoal.getGoalid();
        put(objectfactory.createUnitgoal(unitgoal), "/ref/units/"+unitid+"/goals/"+ goalid);
    }


    public void deleteUnitGoal(String unitid, Integer goalid)
            throws IOException
    {
        delete("/ref/units/"+unitid+"/goals/" + goalid);
    }

}
