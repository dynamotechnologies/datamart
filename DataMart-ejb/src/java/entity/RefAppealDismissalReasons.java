package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "RefAppealDismissalReasons")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RefAppealDismissalReasons.findAll", query = "SELECT r FROM RefAppealDismissalReasons r"),
    @NamedQuery(name = "RefAppealDismissalReasons.findById", query = "SELECT r FROM RefAppealDismissalReasons r WHERE r.id = :id"),
    @NamedQuery(name = "RefAppealDismissalReasons.findByName", query = "SELECT r FROM RefAppealDismissalReasons r WHERE r.name = :name")})
public class RefAppealDismissalReasons implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "Name")
    private String name;
    @OneToMany(mappedBy = "refAppealDismissalReasons")
    private Collection<Appeals> appealsCollection;

    public RefAppealDismissalReasons() {
    }

    public RefAppealDismissalReasons(Integer id) {
        this.id = id;
    }

    public RefAppealDismissalReasons(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Collection<Appeals> getAppealsCollection() {
        return appealsCollection;
    }

    public void setAppealsCollection(Collection<Appeals> appealsCollection) {
        this.appealsCollection = appealsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RefAppealDismissalReasons)) {
            return false;
        }
        RefAppealDismissalReasons other = (RefAppealDismissalReasons) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RefAppealDismissalReasons[ id=" + id + " ]";
    }

}
