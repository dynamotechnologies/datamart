package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.analysisapplicable.*;

import beanutil.*;
import javax.ejb.Stateless;
import java.util.Collection;
import java.util.Iterator;
import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Stateless(mappedName="AnalysisApplicableManagerRemote")
public class AnalysisApplicableManager implements AnalysisApplicableManagerRemote
{

    @PersistenceContext
    private EntityManager em;


    @Override
    public void create(Analysisapplicable resource)
             throws DataMartException
    {
        entity.RefAnalysisApplicables entity = createEntity(resource);

        // Check for existing entity with this id
        entity.RefAnalysisApplicables badentity = getEntity(entity.getId());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        em.persist(entity);
    }


    @Override
    public Analysisapplicable get(Integer id)
            throws DataMartException
    {
        entity.RefAnalysisApplicables entity = getEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        return createResource(entity);
    }


    @Override
    public Analysisapplicables getAll() throws DataMartException {
        Query query = em.createNamedQuery("RefAnalysisApplicables.findAll");

        @SuppressWarnings("unchecked")
        Collection<entity.RefAnalysisApplicables> entitylist = (Collection<entity.RefAnalysisApplicables>)query.getResultList();

        if (!(entitylist.size()>0))
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        // Loop over the query results and build a list of resources
        Analysisapplicables aalist = new Analysisapplicables();
        Iterator it = entitylist.iterator();
        while (it.hasNext())
        {
            aalist.getAnalysisapplicable().add(createResource((entity.RefAnalysisApplicables)it.next()));
        }

        return aalist;
    }


    @Override
    public void update(Analysisapplicable resource)
             throws DataMartException
    {
        entity.RefAnalysisApplicables entity = createEntity(resource);

        // Query for the (hopefully) existing entity. Complain if not found.
        entity.RefAnalysisApplicables existingentity = getEntity(entity.getId());
        if (existingentity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND, "Nonexistent AnalysisApplicable '"+entity.getId()+"' cannot be updated.");

        // Update the entity
        entity = em.merge(entity);
    }


    @Override
    public void delete(Integer id)
             throws DataMartException
    {
        // Verify that this entity exists
        entity.RefAnalysisApplicables entity = getEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Could not delete nonexistent AnalysisApplicable '"+id+"'.");

        // Remove the entity itself
        em.remove(entity);
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     *
     * This method does the mapping from entity object to jaxb object
     */
    protected static Analysisapplicable createResource(entity.RefAnalysisApplicables entity)
            throws DataMartException
    {
        Analysisapplicable resource = new Analysisapplicable();
        resource.setId(entity.getId());
        resource.setAnalysistypeid(entity.getAnalysisTypeId());
        resource.setApplicableregid(entity.getApplicableRegId());
        resource.setCommentperioddays(entity.getCommentPeriodDays());
        resource.setObjectionperioddays(entity.getObjectionPeriodDays());
        resource.setActive(entity.getActive());

        return resource;
    }

    /*
     *
     * This method does the mapping from jaxb object to entity object
     */
    private entity.RefAnalysisApplicables createEntity(Analysisapplicable resource)
            throws DataMartException
    {
        // Make sure the foreign keys are valid, and retrieve a jpa representation of the associated rows
        entity.RefAnalysisTypes analtype = (entity.RefAnalysisTypes) DataMartBeanUtil.getEntityById(em,
                "RefAnalysisTypes", resource.getAnalysistypeid(), "Analysis Type");
        entity.RefCommentRegulations commentreg = (entity.RefCommentRegulations) DataMartBeanUtil.getEntityById(em,
                "RefCommentRegulations", resource.getApplicableregid(), "Comment Regulation");

        entity.RefAnalysisApplicables entity = new entity.RefAnalysisApplicables();
        entity.setId(resource.getId());
        entity.setAnalysisTypeId(resource.getAnalysistypeid());
        entity.setApplicableRegId(resource.getApplicableregid());
        entity.setCommentPeriodDays(resource.getCommentperioddays());
        entity.setObjectionPeriodDays(resource.getObjectionperioddays());
        entity.setActive(resource.isActive());

        return entity;
    }

    // Accepts a String ID as a parameter, and returns the
    //   associated entity, or null if it doesn't exist
    //
    private entity.RefAnalysisApplicables getEntity(Integer id)
            throws DataMartException
    {
        Query query = em.createNamedQuery("RefAnalysisApplicables.findById");
        query.setParameter("id", id);

        entity.RefAnalysisApplicables aa = (entity.RefAnalysisApplicables) DataMartBeanUtil.getEntityByQueryAllowNull(query,
                "More than one AnalysisApplicable found with id '"+id+"'.");

        return aa;
    }
}