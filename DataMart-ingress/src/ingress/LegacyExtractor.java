package ingress;

import ingress.detailprojects.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.zip.DataFormatException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.XMLGregorianCalendar;
import org.w3c.dom.Document;
import us.fed.fs.www.nepa.schema.appeal.Appeal;
import us.fed.fs.www.nepa.schema.decision.Decision;
import us.fed.fs.www.nepa.schema.decisionmaker.Decisionmaker;
import us.fed.fs.www.nepa.schema.decisionappealrule.Decisionappealrule;
import us.fed.fs.www.nepa.schema.documentcontainer.Container;
import us.fed.fs.www.nepa.schema.documentcontainer.Containerdoc;
import us.fed.fs.www.nepa.schema.documentcontainer.Containers;
import us.fed.fs.www.nepa.schema.litigation.Litigation;
import us.fed.fs.www.nepa.schema.nepacategoricalexclusion.Nepacategoricalexclusion;
import us.fed.fs.www.nepa.schema.project.Nepainfo;
import us.fed.fs.www.nepa.schema.project.Project;
import us.fed.fs.www.nepa.schema.project.Purposeids;
import us.fed.fs.www.nepa.schema.project.Sopainfo;
import us.fed.fs.www.nepa.schema.projectactivity.Projectactivity;
import us.fed.fs.www.nepa.schema.projectdocument.Projectdocument;
import us.fed.fs.www.nepa.schema.projectgoal.Projectgoal;
import us.fed.fs.www.nepa.schema.projectlocation.*;
import us.fed.fs.www.nepa.schema.projectmilestone.Projectmilestone;
import us.fed.fs.www.nepa.schema.projectresourcearea.Projectresourcearea;
import us.fed.fs.www.nepa.schema.projectspecialauthority.Projectspecialauthority;
import us.fed.fs.www.nepa.schema.unit.Unit;


/*
 * The job of the Extractor is to convert the Legacy encapsulation objects into front-end
 * schema-generated jaxrs objects. No data validation or conversion should occur in this class.
 *
 */
public class LegacyExtractor {
    public static final String PROJECT_TYPE = "nepa";

    // These are the DataMart's schema-generated jaxrs objects
    private LegacyProjectUnitList legacyprojects = new LegacyProjectUnitList();
    private List<Project> projects = new ArrayList<Project>();
    private List<Projectdocument> projdocs = new ArrayList<Projectdocument>();
    private HashMap<String, Containers> containermap = new HashMap<String, Containers>();
    private List<Appeal> appeals = new ArrayList<Appeal>();
    private List<Unit> units = new ArrayList<Unit>();
    private List<Locations> locations = new ArrayList<Locations>();
    private List<Projectactivity> activities = new ArrayList<Projectactivity>();
    private List<Projectmilestone> milestones = new ArrayList<Projectmilestone>();
    private List<Projectspecialauthority> authorities = new ArrayList<Projectspecialauthority>();
    private List<Projectgoal> goals = new ArrayList<Projectgoal>();
    private List<Projectresourcearea> resourceareas = new ArrayList<Projectresourcearea>();
    private List<Decision> decisions = new ArrayList<Decision>();
    private List<Decisionmaker> decisionMakers = new ArrayList<Decisionmaker>();
    private List<Litigation> litigations = new ArrayList<Litigation>();
    private List<Decisionappealrule> decisionappealrules = new ArrayList<Decisionappealrule>();
    private List<Nepacategoricalexclusion> nepacategoricalexclusions = new ArrayList<Nepacategoricalexclusion>();

    
    /*
     * Constructor takes xml as arguments, using it to populate the legacy objects.
     * It then loops over all of these legacy objects, populating the DataMart schema-generated
     * jaxrs objects as it goes. The public methods can then be used to retrieve the jaxrs objects.
     *
     */
    LegacyExtractor(Document detailsDoc)
            throws DataFormatException
    {
        // Instantiate the encapsulation objects. This will throw exceptions if we have any
        //   data issues
        legacyprojects = new LegacyProjectUnitList(detailsDoc);

        // Loop Units
        for (LegacyProjectUnit legacyunit : legacyprojects.getUnits()) {
            units.add( getUnitFromLegacyUnit(legacyunit) );

            // Loop Projects
            for (LegacyProject legacyproject : legacyunit.getProjects()) {
                String projectid = legacyproject.getId();
                Project currentProject = getProjectFromLegacyProject(legacyproject);
                projects.add( currentProject );
                locations.add( getLocationsFromLegacyLoc(projectid,
                        legacyproject.getLocation() ));

                // Loop Sections
                Containers rootContainer = new Containers(); // Create a top-level container for this project
                for (LegacySection legacysection : legacyproject.getSections()) {
                    rootContainer.getContainerOrContainerdoc().add(getContainerFromSection(legacysection));

                    // Loop Project Documents
                    for (LegacyDocument legacydoc : getProjDocListFromLegacySection(legacysection)) {
                        Projectdocument doc = getProjDocFromLegacyDoc(projectid, legacydoc);
                        if (doc!=null)
                            projdocs.add(doc);
                    }
                }
                containermap.put(projectid,
                        rootContainer );

                // Loop Decisions
                for (LegacyDecision legacydecision : legacyproject.getDecisions()) {
                    Integer decisionid = legacydecision.getDecisionId();
                    decisions.add( getDecisionFromLegacyDecision(projectid, legacydecision) );

                    for (LegacyDecisionMaker legacyDecisionMaker : legacydecision.getDecisionMakers()){
                        Decisionmaker d = getDecisionMakerFromLegacyDecisionMaker(decisionid, legacyDecisionMaker);
                        if (d!=null)
                            decisionMakers.add( d );
                        
                    }
                    
                    // Loop Appeals
                    for (LegacyDecisionAppeal legacyappeal : legacydecision.getAppeals()) {
                        Appeal a = getAppealFromLegacyAppeal(projectid, decisionid, legacyappeal);
                        if (a!=null)
                            appeals.add( a );

                        // Add Appeal Documents
                        Projectdocument doc = getProjDocFromLegacyDecisionApp(projectid, legacyappeal);
                        if (doc!=null)
                            projdocs.add(doc);
                    }

                    for (LegacyDecisionLitigation legacylit : legacydecision.getLitigations()) {
                        litigations.add( getLitigationFromLegacyLitigation(decisionid, legacylit) );
                    }
                    // Add Decision Appeal Rules
                    for (String rule : legacydecision.getAppealRule()) {
                        decisionappealrules.add( getDARfromLegacyDAR(decisionid, rule) );
                    }

                } // Loop Decisions

                // Loop Activities
                for (String activityid : legacyproject.getActivities()) {
                    activities.add( getActivityFromLegacyActivity(projectid, activityid) );
                }

                // Loop Milestones
                for (LegacyMilestone legacystone : legacyproject.getMilestones()) {
                    if (legacystone.getType().equals("7")){
                        String decisionDecided = legacystone.getDate();
                        Date date = LegacyUtil.getDateFromString(decisionDecided, "Decision Decided Date");
                        
                        XMLGregorianCalendar cal = LegacyUtil.getXMLGregorianFromDateTime(date);
                                    cal.setTime(
                DatatypeConstants.FIELD_UNDEFINED,
                DatatypeConstants.FIELD_UNDEFINED,
                DatatypeConstants.FIELD_UNDEFINED);
                   //     cal.setHour(12);
                   //     cal.setMinute(0);
                   //     cal.setSecond(0);
                        currentProject.getNepainfo().setDecisiondecided( cal );
                    }else{
                        milestones.add( getMilestoneFromLegacyMilestone(projectid, legacystone) );
                    }
                }

                // Loop Special Authorities
                for (String legacyauthority : legacyproject.getSpecAuths()) {
                    authorities.add( getSpecAuthFromLegacySpecAuth(projectid, legacyauthority) );
                }
                
                // Loop Goals
                for (LegacyGoal legacygoal : legacyproject.getGoals()) {
                    goals.add( getGoalFromLegacyGoal(projectid, PROJECT_TYPE, legacygoal));
                }
                
                // Loop Resource Areas
                for (LegacyResourceArea legacyresarea : legacyproject.getResourceAreas()) {
                    resourceareas.add( getResourceAreaFromLegacyResourceArea(projectid, PROJECT_TYPE, legacyresarea));
                }

                // Add Nepa Categorical Exclusions
                for (Integer legacynceid : legacyproject.getCatExclusions()) {
                    nepacategoricalexclusions.add( getNCEfromLegacyNCE(projectid, legacynceid) );
                }

            } // Loop Projects

        } // Loop Units

    } // Constructor
    



    /*
     *
     * Public accessor methods
     */



    public XMLGregorianCalendar getTimeStamp()
            throws DataFormatException
    {
        return legacyprojects.getTimeStamp();
    }


    public List<Project> getProjects()
    {
        return projects;
    }


    public List<Projectdocument> getProjectDocuments()
    {
        return projdocs;
    }


    /*
     *
     * Returns a map of containers, indexed by project id (String).
     */
    public HashMap<String, Containers> getContainers()
    {
        return containermap;
    }


    public List<Appeal> getAppeals()
    {
        return appeals;
    }


    public List<Unit> getUnits()
    {
        return units;
    }


    /*
     *
     * Returns a map of locations, indexed by project id (String).
     */
    public List<Locations> getLocations()
    {
        return locations;
    }


    public List<Projectactivity> getActivities()
    {
        return activities;
    }


    public List<Projectmilestone> getMilestones()
    {
        return milestones;
    }


    public List<Projectspecialauthority> getSpecialAuthorities()
    {
        return authorities;
    }
    
    
    public List<Projectgoal> getGoals()
    {
        return goals;
    }
    
    
    public List<Projectresourcearea> getResourceAreas()
    {
        return resourceareas;
    }


    public List<Decision> getDecisions() {
        return decisions;
    }

    public List<Decisionmaker> getDecisionMakers(){
        return decisionMakers;
    }

    public List<Litigation> getLitigations() {
        return litigations;
    }


    public List<Decisionappealrule> getDecisionAppealRules() {
        return decisionappealrules;
    }


    public List<Nepacategoricalexclusion> getNepaCategoricalExclusions() {
        return nepacategoricalexclusions;
    }

    
    
    /*
     *
     * Private translation methods
     */



    private Project getProjectFromLegacyProject(LegacyProject legacyproject)
    {
        Project newproject = new Project();
        newproject.setId(legacyproject.getId()); // SCHEMALINE "project.id"
        newproject.setType(LegacyExtractor.PROJECT_TYPE);
        newproject.setName(legacyproject.getName()); // SCHEMALINE "project.name"
        newproject.setUnitcode(legacyproject.getAdminUnit()); // SCHEMALINE "project.adminunit"
        newproject.setDescription(legacyproject.getDescription()); // SCHEMALINE "project.description"
        newproject.setLastupdate(legacyproject.getUpdateDate()); // SCHEMALINE "project.updatedate"
        newproject.setCommentreg(legacyproject.getCommentRegulation()); // SCHEMALINE "project.commreg"
        newproject.setWwwlink(legacyproject.getUrl()); // SCHEMALINE "project.url"
        newproject.setAddinfopubflag(legacyproject.getAddinfoflag()); // SCHEMALINE "project.addinfoflag "
        newproject.setImplinfopubflag(legacyproject.getImplinfoflag()); // SCHEMALINE "project.implinfoflag "
        newproject.setMeetingpubflag(legacyproject.getMeetingflag()); // SCHEMALINE "project.meetingflag "

        newproject.setWwwsummary(legacyproject.getProjectSummary()); // SCHEMALINE "project.projectsummary"
        newproject.setWwwpub(legacyproject.getPubFlag()); // SCHEMALINE "project.pubflag"

        Purposeids purposeids = new Purposeids();
        purposeids.getPurposeid().addAll(legacyproject.getPurposes());

        Nepainfo nepainfo = new Nepainfo();
        nepainfo.setProjectdocumentid(legacyproject.getProjectDocumentId()); // SCHEMALINE "project.projectdocumentid"
        nepainfo.setAnalysistypeid(legacyproject.getAnalysisType()); // SCHEMALINE "project.type"
        nepainfo.setStatusid(legacyproject.getStatus()); // SCHEMALINE "project.status"
        nepainfo.setContactname(legacyproject.getContact().getName()); // SCHEMALINE "contact.contactname"
        nepainfo.setContactphone(legacyproject.getContact().getPhone()); // SCHEMALINE "contact.contactphone"
        nepainfo.setContactemail(legacyproject.getContact().getEmail()); // SCHEMALINE "contact.contactemail"
        nepainfo.setPurposeids(purposeids); // SCHEMALINE "project.purpose"
        nepainfo.setEsd(legacyproject.getEsd().equals("1")); // SCHEMALINE "project.esd"
        nepainfo.setCenodecision(legacyproject.getCenodecision().equals("1")); // SCHEMALINE "project.esd"
        
        nepainfo.setDataentryperson(legacyproject.getDataEntryPerson());
        nepainfo.setPrimaryprojectmanager(legacyproject.getPrimaryProjectManager());
        nepainfo.setSecondaryprojectmanager(legacyproject.getSecondaryProjectManager());
        nepainfo.setContactshortname(legacyproject.getContact().getShortName());
        
        
        newproject.setNepainfo(nepainfo);
        newproject.setExpirationdate(legacyproject.getExpirationDate()); // SCHEMALINE "project.expirationdate"

        // TODO:  SCHEMALINE "contact.contactShortName"
        // TODO:  SCHEMALINE "project.primaryProjectManager"
        // TODO:  SCHEMALINE "project.secondaryProjectManager"
        // TODO:  SCHEMALINE "project.dataEntryPerson"

        LegacySopa legacysopa = legacyproject.getSopa();
        Sopainfo sopainfo = new Sopainfo();
        sopainfo.setPublishflag(legacysopa.getSopaFlag()); // SCHEMALINE "project.sopa.sopaFlag"
        sopainfo.setNewflag(legacysopa.getNewProjectFlag()); // SCHEMALINE "project.sopa.isNewProjectFlag"
        sopainfo.setHeadercategory(legacysopa.getHeaderUnit()); // SCHEMALINE "project.sopa.sopaHeaderUnit"
        newproject.getNepainfo().setSopainfo(sopainfo);

        return newproject;
    }


    private ArrayList<LegacyDocument> getProjDocListFromLegacySection(LegacySection s)
    {
        ArrayList<LegacyDocument> docs = new ArrayList<LegacyDocument>();
        docs.addAll(s.getDocuments());

        // Loop over sub-sections and extract all of those documents as well
        for (Iterator it = s.getSections().iterator(); it.hasNext();) {
            LegacySection s2 = (LegacySection) it.next();
            docs.addAll(getProjDocListFromLegacySection(s2));
        }
        return docs;
    }


    private Projectdocument getProjDocFromLegacyDoc(String projectid, LegacyDocument legacydoc)
    {
        // Avoid trying to submit non-documents
        if (legacydoc.getId()==null)
            return null;

        String pubflag = (legacydoc.getPubFlag()) ? "1" : "0";

        Projectdocument newdoc = new Projectdocument();
        newdoc.setDocid(legacydoc.getId());
        newdoc.setProjectid(projectid);
        newdoc.setProjecttype(LegacyExtractor.PROJECT_TYPE);
        newdoc.setPubflag(pubflag); // SCHEMALINE "document.publishflag"
        newdoc.setWwwlink(legacydoc.getUrl());  // SCHEMALINE "document.documenturl"
        newdoc.setDocname(legacydoc.getName()); // SCHEMALINE "document.documentname"
        newdoc.setDescription(legacydoc.getDocumentText()); // SCHEMALINE "document.documenttext"
        newdoc.setPdffilesize(legacydoc.getFileSize()); // SCHEMALINE "document.filesize"
        return newdoc;
    }


    private Projectdocument getProjDocFromLegacyDecisionApp(String projectid, LegacyDecisionAppeal app)
    {
        // Avoid trying to submit non-documents
        if (app.getResponse()==null || app.getResponse().getDocId()==null || app.getResponse().getDocId().length()==0)
            return null;

        Projectdocument newdoc = new Projectdocument();
        newdoc.setDocid(app.getResponse().getDocId());  // SCHEMALINE "response.documentid"
        newdoc.setProjectid(projectid);
        newdoc.setProjecttype(LegacyExtractor.PROJECT_TYPE);
        newdoc.setPubflag("1");
        newdoc.setWwwlink("");
        newdoc.setDocname("");
        newdoc.setDescription("Administrative Review Response - " + app.getAppellant());
        newdoc.setPdffilesize(app.getResponse().getFileSize()); // SCHEMALINE "response.appealresponsefilesize"
        return newdoc;
    }

    
    private Container getContainerFromSection(LegacySection section)
    {
        Container container = new Container();
        if (section.getId()!=null) {
            container.setContid(section.getId());     // SCHEMALINE "section.sectionid"
        }
        container.setLabel(section.getName());  // SCHEMALINE "section.sectioname"
        container.setOrder(section.getOrder());   // SCHEMALINE "section.order"

        // loop over elements of type document
        Iterator docs = section.getDocuments().iterator();
        while (docs.hasNext())
        {
            Containerdoc containerdoc = new Containerdoc();
            LegacyDocument legacydoc = (LegacyDocument) docs.next();  // xml element document

            containerdoc.setDocid(legacydoc.getId());
            containerdoc.setOrder(legacydoc.getOrder());    // SCHEMALINE "document.order"
            container.getContainerOrContainerdoc().add(containerdoc);
        }

        // loop over sub-elements of type section
        Iterator subsections = section.getSections().iterator();
        while (subsections.hasNext())
        {
            LegacySection legacysection = (LegacySection) subsections.next();
            Container nextcont = getContainerFromSection(legacysection);  // xml sub-element section
            // Add each one to the response tree
            container.getContainerOrContainerdoc().add(nextcont);
        }

        return container;
    }


    private Locations getLocationsFromLegacyLoc(String projectid, LegacyLocation legacyloc)
    {
        Locations l = new Locations();
        l.setProjectid(projectid);
        l.setLocationdesc(legacyloc.getDescription());  // SCHEMALINE "location.locdesc"
        l.setLocationlegaldesc(legacyloc.getLegal());   // SCHEMALINE "location.loclegal"
        if (!legacyloc.getLatitude().isNaN())
            l.setLatitude(legacyloc.getLatitude());     // SCHEMALINE "location.loclat"
        if (!legacyloc.getLongitude().isNaN())
            l.setLongitude(legacyloc.getLongitude());   // SCHEMALINE "location.loclong"

        Forests forests = new Forests();
        forests.getForestid().addAll(legacyloc.getForests());   // SCHEMALINE "location.locfor"
        l.setForests(forests);

        Districts districts = new Districts();
        districts.getDistrictid().addAll(legacyloc.getDistricts()); // SCHEMALINE "location.locdst"
        l.setDistricts(districts);

        States states = new States();
        states.getStateid().addAll(legacyloc.getStates());  // SCHEMALINE "location.locstate"
        l.setStates(states);

        Counties counties = new Counties();
        counties.getCountyid().addAll(legacyloc.getCounties()); // SCHEMALINE "location.loccount"
        l.setCounties(counties);
        return l;
    }


    private Unit getUnitFromLegacyUnit(LegacyProjectUnit legacyunit) {
        Unit unit = new Unit();
        unit.setCode(legacyunit.getCode());
        unit.setExtendeddetails(legacyunit.getExtendedDetails().equalsIgnoreCase("Y"));
        return unit;
    }


    private Decision getDecisionFromLegacyDecision(String projectid, LegacyDecision legacydecision) {
        Decision decision = new Decision();
        decision.setName(legacydecision.getName());         // SCHEMALINE "decision.decname"
        decision.setId(legacydecision.getDecisionId());                         // SCHEMALINE "decision.decisionId"
        if (legacydecision.getNoticeDate()!=null)
            decision.setLegalnoticedate(legacydecision.getNoticeDate()); // SCHEMALINE "decision.decnotice"
        if (legacydecision.getDecisionDate() != null)
            decision.setDecdate(legacydecision.getDecisionDate());
        decision.setDectype(legacydecision.getType());
        decision.setAppealstatus(legacydecision.getAppealStatus()); // SCHEMALINE "decision.decappstat"
        decision.setConstraint(legacydecision.getConstraint()); // SCHEMALINE "decision.decconstraint"
        decision.setProjectid(projectid);
        decision.setProjecttype(PROJECT_TYPE);
        if (legacydecision.getAreaSize()!=null)
            decision.setAreasize(legacydecision.getAreaSize());
        if (legacydecision.getAreaUnits()!=null)
            decision.setAreaunits(legacydecision.getAreaUnits());
        
        return decision;
    }

    private Decisionmaker getDecisionMakerFromLegacyDecisionMaker(Integer decisionid, LegacyDecisionMaker legacyDecMaker){
        
        Decisionmaker decisionMaker = new Decisionmaker();
        decisionMaker.setId(new Integer(legacyDecMaker.getId()));
        decisionMaker.setDecisionid(decisionid);
        decisionMaker.setTitle(legacyDecMaker.getTitle());
        decisionMaker.setName(legacyDecMaker.getName());
        decisionMaker.setSigneddate(legacyDecMaker.getSignedDate());
        
        return decisionMaker;
    }

    private Appeal getAppealFromLegacyAppeal(String projectid, Integer decisionid, LegacyDecisionAppeal legacyappeal) {
        if (legacyappeal.getResponse()==null || legacyappeal.getResponse().getDate()==null)
            return null;
        Appeal appeal = new Appeal();
        appeal.setId(legacyappeal.getId()); // SCHEMALINE "decapp.appealid"
        appeal.setProjectid(projectid);
        appeal.setProjecttype(LegacyExtractor.PROJECT_TYPE);
        appeal.setAppellant(legacyappeal.getAppellant());   // SCHEMALINE "decapp.appellant"
        appeal.setOutcomeid(legacyappeal.getOutcomeId());   // SCHEMALINE "decapp.outcome"
        appeal.setDocid(legacyappeal.getResponse().getDocId()); // SCHEMALINE "response.documentid"
        appeal.setStatusid(legacyappeal.getStatusId());
        appeal.setDismissalid(legacyappeal.getDismissalId());
        appeal.setRuleid(legacyappeal.getRuleId()); // SCHEMALINE "decapp.rule"
        appeal.setResponsedate(legacyappeal.getResponse().getDate());   // SCHEMALINE "response.date"
        appeal.setAppadminunit(legacyappeal.getAppealAdminUnitCode()); // SCHEMALINE "decapp.appealadminunitcode"
        appeal.setDecisionid(Integer.toString(decisionid));
        return appeal;
    }


    private Litigation getLitigationFromLegacyLitigation(Integer decisionid, LegacyDecisionLitigation legacylit) {
        Litigation litigation = new Litigation();
        litigation.setCasename(legacylit.getName()); // SCHEMALINE "declit.litname"
        litigation.setClosed(legacylit.getCloseDate()); // SCHEMALINE "declit.litclosed"
        litigation.setDecisionid(decisionid);
        litigation.setId(legacylit.getLitigationID()); // SCHEMALINE "declit.litid"
        litigation.setOutcome(legacylit.getOutcome()); // SCHEMALINE "declit.litoutcome"
        litigation.setStatus(legacylit.getStatus()); // SCHEMALINE "declit.litstat"
        return litigation;
    }


    private Decisionappealrule getDARfromLegacyDAR(Integer decisionid, String appealrule) {
        Decisionappealrule dar = new Decisionappealrule();
        dar.setDecisionid(decisionid);
        dar.setAppealrule(appealrule); // SCHEMALINE "decision.decapprule"
        return dar;
    }


    private Projectactivity getActivityFromLegacyActivity(String projectid, String activityid) {
        Projectactivity act = new Projectactivity();
        act.setActivityid( activityid ); // SCHEMALINE "project.activity"
        act.setProjectid(projectid);
        return act;
    }


    private Projectmilestone getMilestoneFromLegacyMilestone(String projectid, LegacyMilestone legacystone) {
        Projectmilestone stone = new Projectmilestone();
        stone.setProjectid(projectid);
        stone.setSeqnum(legacystone.getSequence()); // SCHEMALINE "milestone.msseq"
        stone.setMilestonetype(legacystone.getType());  // SCHEMALINE "milestone.mstype"
        stone.setDatestring(legacystone.getDate()); // SCHEMALINE "milestone.msdate"
        stone.setStatus(legacystone.getStatus());   // SCHEMALINE "milestone.msstatus"
        stone.setHistory(legacystone.getHistoryFlag()); // SCHEMALINE "milestone.mshistoryflag"
        return stone;
    }


    private Projectspecialauthority getSpecAuthFromLegacySpecAuth(String projectid, String legacyauthority) {
        Projectspecialauthority auth = new Projectspecialauthority();
        auth.setProjectid(projectid);
        auth.setSpecialauthorityid( legacyauthority ); // SCHEMALINE "project.specauth"
        return auth;
    }
    
    
    private Projectgoal getGoalFromLegacyGoal(String projectid, String projecttype, LegacyGoal legacygoal) {
        Projectgoal goal = new Projectgoal();
        goal.setProjectid(projectid);
        goal.setProjecttype(projecttype);
        goal.setGoalid(legacygoal.getGoalId());
        goal.setOrder(legacygoal.getGoalOrder());
        return goal;
    }
    
    
    private Projectresourcearea getResourceAreaFromLegacyResourceArea(String projectid, String projecttype, LegacyResourceArea legacyresarea) {
        Projectresourcearea resarea = new Projectresourcearea();
        resarea.setProjectid(projectid);
        resarea.setProjecttype(projecttype);
        resarea.setResourceareaid(legacyresarea.getResourceAreaId());
        resarea.setOrder(legacyresarea.getResourceAreaOrder());
        return resarea;
    }


    private Nepacategoricalexclusion getNCEfromLegacyNCE(String projectid, Integer legacynceid) {
        Nepacategoricalexclusion nce = new Nepacategoricalexclusion();
        nce.setProjectid(projectid);
        nce.setCategoricalexclusionid(legacynceid); // SCHEMALINE "project.catexclusion"
        return nce;
    }
}