-- drop in reverse order of creation in order to avoid constraint violations

drop table ProjectResourceAreas;
drop table UnitGoals;
drop table ProjectGoals;
drop table ResourceAreas;
drop table Goals;

drop table UnitKMLs;
drop table ProjectKMLs;
drop table RefConfigs;

drop table MLMProjects;
drop table CARAPhaseDocs;
drop table CommentPhases;
drop table CARAProjects;
drop table MailingListSubscribers;
drop table ProjectMilestones;
drop table ProjectCounties;
drop table ProjectStates;
drop table ProjectDistricts;
drop table ProjectForests;
drop table ProjectRegions;
drop table ProjectActivities;
drop table ProjectSpecialAuthorities;

drop table Appeals;
drop table NepaCategoricalExclusions;
drop table DecisionAppealRules;
drop table Litigations;
drop table Decisions;
drop table Objections;
drop table ProjectPurposes;
drop table ProjectDocuments;
drop table ProjectDocumentContainers;
drop table Projects;
drop table Applications;
drop table ProjectTypes;
drop table UnitRoles;

drop table RefLitigationStatuses;
drop table RefLitigationOutcomes;
drop table RefCategoricalExclusions;
drop table RefCommentRegulations;
drop table RefAppealDismissalReasons;
drop table RefAppealOutcomes;
drop table RefAppealStatuses;
drop table RefAppealRules;
drop table RefAnalysisTypes;
drop table RefPurposes;
drop table RefStatuses;
drop table RefUnits;

drop table RefActivities;
drop table RefSpecialAuthorities;
drop table RefMilestones;
drop table RefCounties;
drop table RefStates;

drop table Users;
drop table RefRoles;
drop table RefAnalysisApplicables;

drop table RefDocSensitivityRationales;

SELECT OBJECT_NAME from USER_OBJECTS WHERE OBJECT_TYPE = 'TABLE';
