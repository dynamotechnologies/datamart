package test.reference;

import client.AnalysisApplicableClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.analysisapplicable.*;


public class AnalysisApplicableTest {

    private AnalysisApplicableClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new AnalysisApplicableClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postAnalysisApplicable(TestData.analysisapplicables.id1, TestData.analysisapplicables.analysistypeid1,
                    TestData.analysisapplicables.applicableregid1, TestData.analysisapplicables.commentperioddays1,
                    TestData.analysisapplicables.objectionperioddays1, TestData.analysisapplicables.active1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Analysisapplicable aa = null;
        Analysisapplicables aalist = null;
        try {
            aa = client.getAnalysisApplicable(TestData.analysisapplicables.id1);
            aalist = client.getAnalysisApplicableList();
        } catch (IOException ex) {
            Logger.getLogger(AnalysisApplicableTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals((Integer) TestData.analysisapplicables.id1, (Integer) aa.getId());
        Assert.assertEquals(TestData.analysisapplicables.analysistypeid1, aa.getAnalysistypeid());
        Assert.assertEquals((long) TestData.analysisapplicables.applicableregid1, (long) aa.getApplicableregid());
        Assert.assertEquals(TestData.analysisapplicables.commentperioddays1, aa.getCommentperioddays());
        Assert.assertEquals(TestData.analysisapplicables.objectionperioddays1, aa.getObjectionperioddays());
        Assert.assertEquals(TestData.analysisapplicables.active1, aa.isActive());
        Assert.assertTrue(aalist.getAnalysisapplicable().get(0).getId()>0);

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        Analysisapplicable aa = null;
        try {
            client.putAnalysisApplicable(TestData.analysisapplicables.id1, TestData.analysisapplicables.analysistypeid2,
                    TestData.analysisapplicables.applicableregid2, TestData.analysisapplicables.commentperioddays2,
                    TestData.analysisapplicables.objectionperioddays2, TestData.analysisapplicables.active2);
            aa = client.getAnalysisApplicable(TestData.analysisapplicables.id1);
        } catch (Exception ex) {
            Logger.getLogger(AnalysisApplicableTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.analysisapplicables.analysistypeid2, aa.getAnalysistypeid());
        Assert.assertEquals((long) TestData.analysisapplicables.applicableregid2, (long) aa.getApplicableregid());
        Assert.assertEquals(TestData.analysisapplicables.commentperioddays2, aa.getCommentperioddays());
        Assert.assertEquals(TestData.analysisapplicables.objectionperioddays2, aa.getObjectionperioddays());
        Assert.assertEquals(TestData.analysisapplicables.active2, aa.isActive());

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getAnalysisApplicable(TestData.analysisapplicables.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteAnalysisApplicable(TestData.analysisapplicables.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }

    private void post()
    {
        try {
            client.postAnalysisApplicable(TestData.analysisapplicables.id1, TestData.analysisapplicables.analysistypeid1,
                    TestData.analysisapplicables.applicableregid1, TestData.analysisapplicables.commentperioddays1,
                    TestData.analysisapplicables.objectionperioddays1, TestData.analysisapplicables.active1);
        } catch (IOException ex) {
            Logger.getLogger(AnalysisApplicableTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteAnalysisApplicable(TestData.analysisapplicables.id1);
        } catch (IOException ex) {
            Logger.getLogger(AnalysisApplicableTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteAnalysisApplicable(TestData.analysisapplicables.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }
}