package entity;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "MLMProjects")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MLMProjects.findAll", query = "SELECT m FROM MLMProjects m"),
    @NamedQuery(name = "MLMProjects.findById", query = "SELECT m FROM MLMProjects m WHERE m.id = :id")})
public class MLMProjects implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private String id;
    @JoinColumn(name = "ProjectId", referencedColumnName = "Id")
    @OneToOne(optional = false)
    private Projects projects;

    public MLMProjects() {
    }

    public MLMProjects(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Projects getProjects() {
        return projects;
    }

    public void setProjects(Projects projects) {
        this.projects = projects;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MLMProjects)) {
            return false;
        }
        MLMProjects other = (MLMProjects) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.MLMProjects[ id=" + id + " ]";
    }

}
