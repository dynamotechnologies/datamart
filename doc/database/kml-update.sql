CREATE TABLE RefConfigs (
  Id varchar(50) NOT NULL,
  Value varchar(255) NOT NULL,
  CONSTRAINT PkRefConfigs_Id PRIMARY KEY (Id)
) ENGINE=INNODB;

CREATE TABLE ProjectKMLs (
  Id varchar(255) NOT NULL,	-- Filename
  ProjectId int(11) NOT NULL,
  Label varchar(255),
  MapType varchar(50),
  CONSTRAINT PkProjectKMLs_Id PRIMARY KEY (Id),
  CONSTRAINT FkProjectKMLs_ProjectId FOREIGN KEY (ProjectId) REFERENCES Projects (Id)
) ENGINE=INNODB;

CREATE TABLE UnitKMLs (
  Id varchar(255) NOT NULL,	-- Filename
  UnitId varchar(255) NOT NULL,
  Label varchar(255),
  MapType varchar(50),
  CONSTRAINT PkUnitKMLs_Id PRIMARY KEY (Id),
  CONSTRAINT FkUnitKMLs_UnitId FOREIGN KEY (UnitId) REFERENCES RefUnits (Id)
) ENGINE=INNODB;
