package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.activity.*;


public class ActivityClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public ActivityClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.activity", "Activity.xsd");
    }


    public void postActivity(String id, String name)
            throws IOException
    {
        Activity activity = new Activity();
        activity.setId(id);
        activity.setName(name);
        postActivity(activity);
    }


    public void postActivity(Activity activity)
            throws IOException
    {
        post(objectfactory.createActivity(activity), "/ref/activities");
    }


    public Activity getActivity(String id)
            throws IOException
    {
        return get("/ref/activities/" + id, Activity.class);
    }


    public Activities getActivityList()
            throws IOException
    {
        return get("/ref/activities", Activities.class);
    }
    

    public void putActivity(String id, String name)
            throws IOException
    {
        Activity activity = new Activity();
        activity.setId(id);
        activity.setName(name);
        putActivity(activity);
    }


    public void putActivity(Activity activity)
            throws IOException
    {
        put(objectfactory.createActivity(activity), "/ref/activities/" + activity.getId());
    }
    

    public void deleteActivity(String id)
            throws IOException
    {
        delete("/ref/activities/" + id);
    }
}