package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.purpose.*;

import beanutil.*;
import java.util.Iterator;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless(mappedName="PurposeManagerRemote")
public class PurposeManager implements PurposeManagerRemote
{

    @PersistenceContext
    private EntityManager em;


    @Override
    public void create(Purpose resource)
             throws DataMartException
    {
        validate(resource);
        entity.RefPurposes entity = createEntity(resource);

        entity.RefPurposes badentity = createEntity(resource.getId());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        entity = em.merge(entity);
    }

    @Override
    public Purposes getAll()
             throws DataMartException
    {
        // Retrieve all entities from the database
        Query query = em.createQuery("SELECT u FROM RefPurposes u");
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Purposes responselist = new Purposes();
        while (it.hasNext()) {
            responselist.getPurpose().add(createResource( (entity.RefPurposes)it.next() ));
        }
        return responselist;
    }

    @Override
    public String getName(String id)
            throws DataMartException
    {
        entity.RefPurposes entity = createEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return entity.getName();
    }

    @Override
    public void update(Purpose resource)
             throws DataMartException
    {
        validate(resource);
        entity.RefPurposes entity = createEntity(resource);

        if (createEntity(entity.getId())==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        entity = em.merge(entity);
    }

    @Override
    public void delete(String id)
             throws DataMartException
    {
        entity.RefPurposes entity = createEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     *
     * This method does the mapping from jaxb object to entity object and vice versa
     */
    private Purpose createResource(entity.RefPurposes entity)
            throws DataMartException
    {
        Purpose resource = new Purpose();
        resource.setId(entity.getId());
        resource.setName(entity.getName());
        return resource;
    }

    private entity.RefPurposes createEntity(Purpose resource)
            throws DataMartException
    {
        entity.RefPurposes entity = new entity.RefPurposes();
        entity.setId(resource.getId());
        entity.setName(resource.getName());
        return entity;
    }

    // This version of overloaded method createEntity() accepts a String ID as a parameter, and returns the
    //   associated entity, or null if it doesn't exist
    private entity.RefPurposes createEntity(String id)
            throws DataMartException
    {
        return em.find(entity.RefPurposes.class, id);
    }

    private void validate(Purpose resource)
            throws DataMartException
    {
        DataMartBeanUtil.confirmNotNull(resource.getName(), "Name");
    }
}
