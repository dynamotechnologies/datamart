package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.projectlocation.*;

import beanutil.*;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless(mappedName="ProjectStateManagerRemote")
public class ProjectStateManager implements ProjectStateManagerRemote
{

    @PersistenceContext
    private EntityManager em;

    private String PROJECT_TYPE = "nepa";

    @Override
    public void merge(String projectid, String id)
             throws DataMartException
    {
        entity.ProjectStates state = createEntity(projectid, id);

        // Check for existing entity and grab the internal ID if it exists
        entity.ProjectStates oldstate = getEntity(projectid, id);
        if (oldstate != null)
            state.setId(oldstate.getId());

        em.merge(state);
    }


    @Override
    public States get(String projectid, String id)
            throws DataMartException
    {
        States rval = new States();
        entity.ProjectStates state = getEntity(projectid, id);
        if (state==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "NEPA Project " + projectid + " is not associated with state '" + id + "'");

        rval.getStateid().add(state.getRefStates().getId());
        return rval;
    }


    @Override
    public void delete(String projectid, String id)
            throws DataMartException
    {
        entity.ProjectStates state = getEntity(projectid, id);
        if (state==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "NEPA Project " + projectid + " is not associated with state '" + id + "'");

        em.remove(state);
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    private entity.ProjectStates createEntity(String projectid, String id)
            throws DataMartException
    {
        // Get a reference to the project
        entity.Projects project = DataMartBeanUtil.getProject(em, this.PROJECT_TYPE, projectid);
        if (project==null)
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION,
                    "Project with type '" + this.PROJECT_TYPE + "' and ID '" + projectid + "' does not exist.");

        // Get the reference state associated with this id
        entity.RefStates ref = (entity.RefStates) DataMartBeanUtil.getEntityById(em,
                "RefStates", id, "State Code");

        entity.ProjectStates state = new entity.ProjectStates();
        state.setProjects(project);
        state.setRefStates(ref);
        return state;
    }


    private entity.ProjectStates getEntity(String projectid, String id)
            throws DataMartException
    {
        // Get a reference to the project
        entity.Projects project = DataMartBeanUtil.getProject(em, this.PROJECT_TYPE, projectid);
        if (project==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Project with type '" + this.PROJECT_TYPE + "' and ID '" + projectid + "' does not exist.");

        // Get the reference state associated with this id
        entity.RefStates ref = (entity.RefStates) DataMartBeanUtil.getEntityById(em,
                "RefStates", id, "State Code");
        if (ref==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Reference State with id '" + id + "' does not exist.");

        // Query for the relationship table row
        Query q = em.createQuery("SELECT p FROM ProjectStates p WHERE p.projects = :project AND p.refStates = :ref");
        q.setParameter("project", project);
        q.setParameter("ref", ref);
        entity.ProjectStates state = (entity.ProjectStates) DataMartBeanUtil.getEntityByQueryAllowNull(q,
                "NEPA Project " + projectid + " has multiple state associations with id '" + id + "'");

        return state;
    }
}