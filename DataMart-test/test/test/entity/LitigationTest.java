package test.entity;

import client.DecisionClient;
import client.LitigationClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.litigation.*;


public class LitigationTest {
    private LitigationClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new LitigationClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postLitigation(TestData.litigations.id1,
                TestData.decisions.id1,
                TestData.litigations.casename2,
                TestData.litigations.status2,
                TestData.litigations.outcome1,
                TestData.litigations.closed2);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Litigation litigation = null;
        Litigations litigationlist = null;
        try {
            litigation = client.getLitigation(TestData.litigations.id1);
            litigationlist = client.getLitigationList(TestData.decisions.id1);
        } catch (IOException ex) {
            Logger.getLogger(LitigationTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.litigations.id1, litigation.getId());
        Assert.assertEquals(TestData.decisions.id1, litigation.getDecisionid());
        Assert.assertEquals(TestData.litigations.casename1, litigation.getCasename());
        Assert.assertEquals(TestData.litigations.status1, litigation.getStatus());
        Assert.assertEquals(TestData.litigations.outcome1, (int)litigation.getOutcome());
        Assert.assertEquals(TestData.litigations.closed1, litigation.getClosed().toString());

        Assert.assertTrue(litigationlist.getLitigation().get(0).getCasename().length()>0);
        Assert.assertTrue(litigationlist.getLitigation().get(0).getClosed().toString().length()>0);

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        Litigation litigation = null;
        try {
            client.putLitigation(TestData.litigations.id1,
                TestData.decisions.id1,
                TestData.litigations.casename2,
                TestData.litigations.status2,
                TestData.litigations.outcome1,
                TestData.litigations.closed2);
            litigation = client.getLitigation(TestData.litigations.id1);
        } catch (Exception ex) {
            Logger.getLogger(LitigationTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.litigations.id1, litigation.getId());
        Assert.assertEquals(TestData.decisions.id1, litigation.getDecisionid());
        Assert.assertEquals(TestData.litigations.casename2, litigation.getCasename());
        Assert.assertEquals(TestData.litigations.status2, litigation.getStatus());
        Assert.assertEquals(TestData.litigations.outcome1, (int)litigation.getOutcome());
        Assert.assertEquals(TestData.litigations.closed2, litigation.getClosed().toString());

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getLitigation(TestData.litigations.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteLitigation(TestData.litigations.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }

    private void post()
    {
        try {
            client.postLitigation(TestData.litigations.id1,
                TestData.decisions.id1,
                TestData.litigations.casename1,
                TestData.litigations.status1,
                TestData.litigations.outcome1,
                TestData.litigations.closed1);

        } catch (IOException ex) {
            Logger.getLogger(LitigationTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteLitigation(TestData.litigations.id1);
        } catch (IOException ex) {
            Logger.getLogger(LitigationTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteLitigation(TestData.litigations.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }


    @BeforeClass
    public static void  populateDependencies()
    {
        DecisionClient decisionclient = null;
        try {
            decisionclient = new DecisionClient();
        } catch (Exception ex) {
            Assert.fail();
        }

        // Litigations rely on Decisions. Pre-populate a Decision.
        try {
            // First, pre-populate Decision dependencies
            DecisionTest.populateDependencies();

            decisionclient.postDecision(TestData.decisions.id1,
                    TestData.projects.id1,
                    TestData.projects.type1,
                    TestData.decisions.name1,
                    TestData.appealstatuses.id1,
                    TestData.decisions.constraint1,
                    TestData.decisions.legalnoticedate1,
                    TestData.decisions.areasize1,
                    TestData.decisions.areaunits1,
                    TestData.decisions.dectype1,
                    TestData.decisions.decdate1);
        } catch (IOException ex) {
            Logger.getLogger(LitigationTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @AfterClass
    public static void  cleanupDependencies()
    {
        DecisionClient decisionclient = null;
        try {
            decisionclient = new DecisionClient();
        } catch (Exception ex) {
            Assert.fail();
        }

        // Clean up the Project
        try {
            decisionclient.deleteDecision(TestData.decisions.id1);

            // Finally, clean up Decision dependencies
            DecisionTest.cleanupDependencies();
        } catch (IOException ex) {
            Logger.getLogger(LitigationTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }
}