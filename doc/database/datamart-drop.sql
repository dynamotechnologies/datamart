-- drop in reverse order of creation in order to avoid constraint violations

drop table if exists ProjectResourceAreas;
drop table if exists UnitGoals;
drop table if exists ProjectGoals;
drop table if exists ResourceAreas;
drop table if exists Goals;

drop table if exists UnitKMLs;
drop table if exists ProjectKMLs;
drop table if exists RefConfigs;

drop table if exists MLMProjects;		-- CARA
drop table if exists CARAPhaseDocs;		-- CARA
drop table if exists CommentPhases;		-- CARA
drop table if exists CARAProjects;		-- CARA
drop table if exists MailingListSubscribers;	-- CARA
drop table if exists ProjectMilestones;
drop table if exists ProjectCounties;
drop table if exists ProjectStates;
drop table if exists ProjectDistricts;
drop table if exists ProjectForests;
drop table if exists ProjectRegions;
drop table if exists ProjectActivities;
drop table if exists ProjectSpecialAuthorities;

drop table if exists Appeals;
drop table if exists NepaCategoricalExclusions;
drop table if exists DecisionAppealRules;
drop table if exists Litigations;
drop table if exists Decisions;
drop table if exists Objections;
drop table if exists ProjectPurposes;
drop table if exists ProjectDocuments;
drop table if exists ProjectDocumentContainers;
drop table if exists Projects;
drop table if exists Applications;
drop table if exists ProjectTypes;
drop table if exists UnitRoles;			-- CARA

drop table if exists RefLitigationStatuses;
drop table if exists RefLitigationOutcomes;
drop table if exists RefCategoricalExclusions;
drop table if exists RefCommentRegulations;
drop table if exists RefAppealDismissalReasons;
drop table if exists RefAppealOutcomes;
drop table if exists RefAppealStatuses;
drop table if exists RefAppealRules;
drop table if exists RefAnalysisTypes;
drop table if exists RefPurposes;
drop table if exists RefStatuses;
drop table if exists RefUnits;

drop table if exists RefActivities;
drop table if exists RefSpecialAuthorities;
drop table if exists RefMilestones;
drop table if exists RefCounties;
drop table if exists RefStates;

drop table if exists Users;			-- CARA
drop table if exists RefRoles;			-- CARA
drop table if exists RefAnalysisApplicables;	-- CARA

drop table if exists RefDocumentSensitivityRationales;	-- uploader

drop table if exists DecisionMakers;
drop table if exists RefDecisionTypes;

show tables;
