package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.appeal.*;

import beanutil.*;
import javax.ejb.Stateless;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Stateless(mappedName="AppealManagerRemote")
public class AppealManager implements AppealManagerRemote
{

    @PersistenceContext
    private EntityManager em;


    @Override
    public void create(Appeal resource)
             throws DataMartException
    {
        entity.Appeals entity = createEntity(resource);

        // Check for existing entity with this id
        entity.Appeals badentity = getEntity(entity.getId());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        em.persist(entity);
    }


    @Override
    public Appeal get(String id)
            throws DataMartException
    {
        entity.Appeals entity = getEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        return createResource(entity);
    }


    @Override
    public Appeals getAll() throws DataMartException {
        Query query = em.createNamedQuery("Appeals.findAll");

        @SuppressWarnings("unchecked")
        Collection<entity.Appeals> entitylist = (Collection<entity.Appeals>)query.getResultList();

        if (!(entitylist.size()>0))
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        // Loop over the query results and build a list of resources
        Appeals appealslist = new Appeals();
        Iterator it = entitylist.iterator();
        while (it.hasNext())
        {
            appealslist.getAppeal().add(createResource((entity.Appeals)it.next()));
        }

        return appealslist;
    }


    public Appeals getByUnit(String unitid, String startdate, String enddate, String statusfilter)
            throws DataMartException
    {
        // Confirm that this is a valid unit
        entity.RefUnits unit = (entity.RefUnits) DataMartBeanUtil.getEntityById(em,
                "RefUnits", unitid, "Unit");
        if (unit==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        // Confirm that the status is valid
        entity.RefAppealStatuses status;
        if (statusfilter!=null)
            status = (entity.RefAppealStatuses) DataMartBeanUtil.getEntityById(em,
                "RefAppealStatuses", statusfilter, "Status");


        // TODO: get status, startdate and enddate filters working

        
        // Get appeals for this unit
        Query query = em.createQuery("SELECT a FROM Appeals a JOIN a.projects p JOIN p.refUnits u WHERE u.id LIKE :id");
        query.setParameter("id", unitid+"%");
        List entitylist =  query.getResultList();

        if (!(entitylist.size()>0))
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        // Loop over the database collection. Each row becomes a new object in the response list
        Iterator it = entitylist.iterator();
        Appeals responselist = new Appeals();
        while (it.hasNext()) {
            responselist.getAppeal().add(AppealManager.createResource( (entity.Appeals)it.next() ));
        }
        return responselist;
    }


    @Override
    public void update(Appeal resource)
             throws DataMartException
    {
        entity.Appeals entity = createEntity(resource);

        // Query for the (hopefully) existing entity. Complain if not found.
        entity.Appeals existingentity = getEntity(entity.getId());
        if (existingentity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND, "Nonexistent Appeal '"+entity.getId()+"' cannot be updated.");

        // Update the entity
        entity = em.merge(entity);
    }


    @Override
    public void delete(String id)
             throws DataMartException
    {
        // Verify that this entity exists
        entity.Appeals entity = getEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Could not delete nonexistent appeal '"+id+"'.");

        // Remove the entity itself
        em.remove(entity);
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     *
     * This method does the mapping from entity object to jaxb object
     */
    protected static Appeal createResource(entity.Appeals entity)
            throws DataMartException
    {
        Appeal resource = new Appeal();
        resource.setAppellant(entity.getAppellant());
        resource.setDocid(entity.getProjectDocuments().getDocumentId());
        resource.setId(entity.getId());
        resource.setOutcomeid(entity.getRefAppealOutcomes().getId().toString());
        resource.setProjectid(entity.getProjects().getPublicId());
        resource.setProjecttype(entity.getProjects().getProjectTypes().getId());
        resource.setDismissalid(entity.getRefAppealDismissalReasons().getId().toString());
        resource.setResponsedate(
                DataMartBeanUtil.getXMLGregorianFromDate( entity.getResponseDate() ));
        resource.setRuleid(entity.getRefAppealRules().getId());
        resource.setStatusid(entity.getRefAppealStatuses().getId().toString());

        if (entity.getRefUnits()==null) {
            resource.setAppadminunit(""); // Remove after appeals have been populated; appadminunit is required.
        } else {
            resource.setAppadminunit(entity.getRefUnits().getId());
        }

        if (entity.getDecisions()!=null)
            resource.setDecisionid(entity.getDecisions().getId().toString());

        return resource;
    }

    /*
     *
     * This method does the mapping from jaxb object to entity object
     */
    private entity.Appeals createEntity(Appeal resource)
            throws DataMartException
    {
        String projectid = resource.getProjectid(); // Always reference the public ID in error messages

        // Get a reference to the project
        entity.Projects project = DataMartBeanUtil.getProject(em, resource.getProjecttype(), projectid);
        if (project==null)
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION, 
                    "Project with type '"+resource.getProjecttype()+"' and ID '"+projectid+"' does not exist.");

        // Get a reference to the project document
        entity.ProjectDocuments projdoc = DataMartBeanUtil.getProjectDocument(em, project, resource.getDocid());
        if (projdoc==null)
            throw new DataMartException(DataMartException.MISSING_ELEMENT, 
                    "Project Document '"+resource.getDocid()+"' does not exist, or is not associated with project '"+projectid+"'.");

        Integer intOutcome = DataMartBeanUtil.getIntFromString(resource.getOutcomeid(), "Appeal Outcome");
        Integer intReasonId = DataMartBeanUtil.getIntFromString(resource.getDismissalid(), "Dismissal Reason");
        Integer intStatusId = DataMartBeanUtil.getIntFromString(resource.getStatusid(), "Appeal Status");

        /*
         *
         * It sucks running all these queries, but it's probably better than returning five pages of
         *    incomprehensible database output when one of your constraints fails. And it's certainly better
         *    than inserting nulls. If JPA has some other way to handle FK checking, that might be the way to go.
         */
        // Make sure the foreign keys are valid, and retrieve a jpa representation of the associated rows
        entity.RefAppealDismissalReasons reason = (entity.RefAppealDismissalReasons) DataMartBeanUtil.getEntityById(em,
                "RefAppealDismissalReasons", intReasonId, "Dismissal Reason");
        entity.RefAppealOutcomes outcome = (entity.RefAppealOutcomes) DataMartBeanUtil.getEntityById(em,
                "RefAppealOutcomes", intOutcome, "Outcome");
        entity.RefAppealRules rule = (entity.RefAppealRules) DataMartBeanUtil.getEntityById(em,
                "RefAppealRules", resource.getRuleid(), "Appeal Rule");
        entity.RefAppealStatuses status = (entity.RefAppealStatuses) DataMartBeanUtil.getEntityById(em,
                "RefAppealStatuses", intStatusId, "Status");
        entity.RefUnits appadminunit = (entity.RefUnits) DataMartBeanUtil.getEntityById(em,
                "RefUnits", resource.getAppadminunit(), "Appeal Admin Unit");

        entity.Appeals entity = new entity.Appeals();
        
        entity.Decisions decision = new entity.Decisions();
        if (resource.getDecisionid()!=null && resource.getDecisionid().length()>0) {
            Integer intDecisionId = DataMartBeanUtil.getIntFromString(resource.getDecisionid(), "Decision ID");
            decision = (entity.Decisions) DataMartBeanUtil.getEntityById(em,
                "Decisions", intDecisionId, "Decision");
            entity.setDecisions(decision);
        }

        entity.setAppellant(resource.getAppellant());
        entity.setId(resource.getId());
        entity.setProjectDocuments(projdoc);
        entity.setProjects(project);
        entity.setRefAppealDismissalReasons(reason);
        entity.setRefAppealOutcomes(outcome);
        entity.setRefAppealRules(rule);
        entity.setRefAppealStatuses(status);
        entity.setResponseDate(
                DataMartBeanUtil.getDateFromXMLGregorian( resource.getResponsedate() ));
        entity.setRefUnits(appadminunit);

        return entity;
    }

    // Accepts a String ID as a parameter, and returns the
    //   associated entity, or null if it doesn't exist
    //
    private entity.Appeals getEntity(String id)
            throws DataMartException
    {
        Query query = em.createNamedQuery("Appeals.findById");
        query.setParameter("id", id);

        entity.Appeals appeal = (entity.Appeals) DataMartBeanUtil.getEntityByQueryAllowNull(query,
                "More than one Appeal found with id '"+id+"'.");

        return appeal;
    }
}