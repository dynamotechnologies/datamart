package ingress.detailprojects;

import ingress.LegacyUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.zip.DataFormatException;
import javax.xml.datatype.XMLGregorianCalendar;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 *
 * <!DOCTYPE unitprojectsdetail [
<!ELEMENT unitprojectsdetail (timestamp, unit*)>
<!ELEMENT timestamp (#PCDATA)>
<!ELEMENT unit (code,extendeddetails,project+)>
<!ELEMENT code (#PCDATA)>
<!ELEMENT extendeddetails (#PCDATA)>
<!ELEMENT project (id,projectdocumentid,expirationdate?,name,status,type,url?,appealslink,objectionslink,description,projectsummary,contact,catexclusion*,specauth+,milestone*,purpose,adminunit,activity+,commreg?,esd?,cenodecision?,location?,decision*,pubflag,updatedate,sopa,primaryProjectManager?,secondaryProjectManager?,dataEntryPerson?,goal*,resourcearea*)>
<!ELEMENT id (#PCDATA)>
<!ELEMENT projectdocumentid (#PCDATA)>
<!ELEMENT expirationdate (#PCDATA)>
<!ELEMENT name (#PCDATA)>
<!ELEMENT status (#PCDATA)>
<!ELEMENT type (#PCDATA)>
<!ELEMENT url (#PCDATA)>
<!ELEMENT appealslink (#PCDATA)>
<!ELEMENT objectionslink (#PCDATA)>
<!ELEMENT description (#PCDATA)>
<!ELEMENT projectsummary (#PCDATA)>
<!ELEMENT contact (contactShortName,contactname, contactphone, contactemail)>
<!ELEMENT contactShortName (#PCDATA)>
<!ELEMENT contactname (#PCDATA)>
<!ELEMENT contactphone (#PCDATA)>
<!ELEMENT contactemail (#PCDATA)>
<!ELEMENT catexclusion (#PCDATA)>
<!ELEMENT specauth (#PCDATA)>
<!ELEMENT milestone (msseq,mstype,msdate,msstatus,mshistoryflag)>
<!ELEMENT msseq (#PCDATA)>
<!ELEMENT mstype (#PCDATA)>
<!ELEMENT msdate (#PCDATA)>
<!ELEMENT msstatus (#PCDATA)>
<!ELEMENT mshistoryflag (#PCDATA)>
<!ELEMENT purpose (purposename+)>
<!ELEMENT purposename (#PCDATA)>
<!ELEMENT adminunit (#PCDATA)>
<!ELEMENT activity (#PCDATA)>
<!ELEMENT commreg (#PCDATA)>
<!ELEMENT location (locdesc,locfor+,locdst+,locstate+,loccount+,loclegal?,loclat?,loclong?)>
<!ELEMENT locdesc (#PCDATA)>
<!ELEMENT locfor (#PCDATA)>
<!ELEMENT locdst (#PCDATA)>
<!ELEMENT locstate (#PCDATA)>
<!ELEMENT loccount (#PCDATA)>
<!ELEMENT loclegal (#PCDATA)>
<!ELEMENT loclat (#PCDATA)>
<!ELEMENT loclong (#PCDATA)>
<!ELEMENT decision (decname,decisionId, dectype,decdate,decnotice?,decconstraint,decapprule,decmaker+,decappstat?,decapp*,declit*,decareasize?,decareaunits?)>
<!ELEMENT decname (#PCDATA)>
<!ELEMENT decisionId (#PCDATA)>
<!ELEMENT dectype (#PCDATA)>
<!ELEMENT decdate (#PCDATA)>
<!ELEMENT decnotice (#PCDATA)>
<!ELEMENT decconstraint (#PCDATA)>
<!ELEMENT decapprule (#PCDATA)>
<!ELEMENT decmaker (decmakerId,decmakername,decmakertitle,decmakesigneddate)>
<!ELEMENT decmakerId (#PCDATA)>
<!ELEMENT decmakername (#PCDATA)>
<!ELEMENT decmakertitle (#PCDATA)>
<!ELEMENT decmakesigneddate (#PCDATA)>
<!ELEMENT decappstat (#PCDATA)>
<!ELEMENT decapp (appealid,appealadminunitcode,rule,appellant?,outcome?,response?)>
<!ELEMENT appealid (#PCDATA)>
<!ELEMENT appealadminunitcode (#PCDATA)>
<!ELEMENT rule (#PCDATA)>
<!ELEMENT appellant (#PCDATA)>
<!ELEMENT outcome (#PCDATA)>
<!ELEMENT response (date,documentid?,appealresponsefilesize?)>
<!ELEMENT date (#PCDATA)>
<!ELEMENT documentid (#PCDATA)>
<!ELEMENT appealresponsefilesize (#PCDATA)>
<!ELEMENT declit (litid,litname,litstat,litoutcome?,litclosed?)>
<!ELEMENT litid (#PCDATA)>
<!ELEMENT litname (#PCDATA)>
<!ELEMENT litstat (#PCDATA)>
<!ELEMENT litoutcome (#PCDATA)>
<!ELEMENT litclosed (#PCDATA)>
<!ELEMENT pubflag (#PCDATA)>
<!ELEMENT updatedate (#PCDATA)>
<!ELEMENT sopa (sopaFlag,isNewProjectFlag?,sopaHeaderUnit?)>
<!ELEMENT sopaFlag (#PCDATA)>
<!ELEMENT isNewProjectFlag (#PCDATA)>
<!ELEMENT sopaHeaderUnit (#PCDATA)>
<!ELEMENT primaryProjectManager (#PCDATA)>
<!ELEMENT secondaryProjectManager (#PCDATA)>
<!ELEMENT dataEntryPerson (#PCDATA)>
<!ELEMENT goal (goalid,goalorder?)>
<!ELEMENT goalid (#PCDATA)>
<!ELEMENT goalorder (#PCDATA)>
<!ELEMENT resourcearea (resourceareaid,resourceareaorder?)>
<!ELEMENT resourceareaid (#PCDATA)>
<!ELEMENT resourceareaorder (#PCDATA)>
<!ELEMENT decareasize (#PCDATA)>
<!ELEMENT decareaunits (#PCDATA)>
<!ATTLIST unitprojects xmlns CDATA 'http://www.fs.fed.us/pals/xmlns/unitprojectsdetail/'>
]>
 *
 * This class is responsible for encapsulating element `decision` as specified above
 */
public class LegacyDecision {
    private String name = "";
    private String type = "";
    private XMLGregorianCalendar decisiondate; // optional
    
    private XMLGregorianCalendar noticedate; // optional
    private String constraint = "";
    private ArrayList<String> appealrules = new ArrayList<String>();
    private String appealstatus = ""; // optional
    private ArrayList<LegacyDecisionAppeal> appeals = new ArrayList<LegacyDecisionAppeal>(); // zero or many
    private ArrayList<LegacyDecisionLitigation> litigation = new ArrayList<LegacyDecisionLitigation>(); // zero or many
    private ArrayList<LegacyDecisionMaker> decisionMakers = new ArrayList<LegacyDecisionMaker>(); // one or many
    private Integer decisionId;
    private Double areasize; // optional
    private String areaunits = ""; // optional

    public LegacyDecision()
    {
        // allow instantiation without nodes
    }

    public LegacyDecision(Node n)
            throws DataFormatException
    {
        NodeList mynodes = n.getChildNodes();
        for (int i=0; i<mynodes.getLength(); i++)
        {
            if (mynodes.item(i).getNodeName().equals("decname"))
                name = mynodes.item(i).getTextContent();
            
            if (mynodes.item(i).getNodeName().equals("dectype"))
                type = mynodes.item(i).getTextContent();
            if (mynodes.item(i).getNodeName().equals("decdate"))
                setDecisionDate(mynodes.item(i).getTextContent());
            
            if (mynodes.item(i).getNodeName().equals("decnotice"))
                setNoticeDate(mynodes.item(i).getTextContent());
            if (mynodes.item(i).getNodeName().equals("decconstraint"))
                constraint = mynodes.item(i).getTextContent();
            if (mynodes.item(i).getNodeName().equals("decapprule"))
                setDecAppRule(mynodes.item(i).getTextContent());
            
            
            if (mynodes.item(i).getNodeName().equals("decmaker"))
                addDecisionMaker(mynodes.item(i));
            
            if (mynodes.item(i).getNodeName().equals("decappstat"))
                appealstatus = mynodes.item(i).getTextContent();
            if (mynodes.item(i).getNodeName().equals("decapp"))
                addAppeal(mynodes.item(i));
            if (mynodes.item(i).getNodeName().equals("declit"))
                addLitigation(mynodes.item(i));
            if (mynodes.item(i).getNodeName().equals("decisionId"))
                decisionId = Integer.parseInt(mynodes.item(i).getTextContent());
            if (mynodes.item(i).getNodeName().equals("decareasize") && mynodes.item(i).getTextContent().length() > 0)
                areasize = Double.parseDouble(mynodes.item(i).getTextContent());
            if (mynodes.item(i).getNodeName().equals("decareaunits"))
                areaunits = mynodes.item(i).getTextContent();
        }
        if (!(name.length()>0))
            throw new DataFormatException("decname not found.");
        if (!(constraint.length()>0))
            throw new DataFormatException("decconstraint not found.");
     //This is not required as of PALS 4.5
     //   if (!(appealrules.size()>0))
     //       throw new DataFormatException("decapprule not found.");
        if (decisionId==null)
            throw new DataFormatException("decisionId not found.");
    }

    private void setDecAppRule(String palsdata)
            throws DataFormatException
    {
        try{
        if (palsdata.contains(",")) {
            String[] palslist = palsdata.split(",");
            for (String n : palslist)
                appealrules.add( LegacyUtil.getAppealRuleIdFromPalsId(Integer.parseInt(n)) );
        } else {
            appealrules.add(LegacyUtil.getAppealRuleIdFromPalsId(Integer.parseInt(palsdata)));
        }
        }catch(Exception e){
            //6-11-2015 - As of PALS 4.5, Appeal Rule is no longer necessary and should be expected as optional
            //Eating this exception.
        }
    }

    private void setDecisionDate(String d)
            throws DataFormatException
    {
        Date date = LegacyUtil.getDateFromString(d, "Decision Date");
        decisiondate = LegacyUtil.getXMLGregorianFromDateTime(date);
    }
    private void setNoticeDate(String d)
            throws DataFormatException
    {
        Date date = LegacyUtil.getDateFromString(d, "Notice Date");
        noticedate = LegacyUtil.getXMLGregorianFromDateTime(date);
    }

    private void addAppeal(Node n)
            throws DataFormatException
    {
        appeals.add(new LegacyDecisionAppeal(n));
    }

    private void addLitigation(Node n)
            throws DataFormatException
    {
        litigation.add(new LegacyDecisionLitigation(n));
    }

    private void addDecisionMaker(Node n)
            throws DataFormatException
    {
        decisionMakers.add(new LegacyDecisionMaker(n));
    }
    
    public String getName()
    {
        return name;
    }
    
    public String getType()
    {
        return type;
    }

    public XMLGregorianCalendar getDecisionDate()
    {
        return decisiondate;
    }
    
    public XMLGregorianCalendar getNoticeDate()
    {
        return noticedate;
    }

    public String getConstraint()
    {
        return constraint;
    }

    public ArrayList<String> getAppealRule()
    {
        return appealrules;
    }

    public String getAppealStatus()
    {
        return appealstatus;
    }

    public Double getAreaSize()
    {
        return areasize;
    }

    public String getAreaUnits()
    {
        return areaunits;
    }
    
    public ArrayList<LegacyDecisionMaker> getDecisionMakers()
    {
        return decisionMakers;
    }
    
    public ArrayList<LegacyDecisionAppeal> getAppeals()
    {
        return appeals;
    }

    public ArrayList<LegacyDecisionLitigation> getLitigations()
    {
        return litigation;
    }

    public Integer getDecisionId()
    {
        return decisionId;
    }
}