package stateless;

import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.projectgoal.*;
import beanutil.DataMartException;

@Remote
public interface ProjectGoalManagerRemote {
/*
 *
 * The Manager beans are responsible for:
 *   1) Implementing business logic and
 *   2) Translating front-end resources to back-end database entities.
 *
 * Often the resources will consist of data from numerous database tables. It is the manager's responsibility
 *   to ensure that these front-end resources can be accessed as though they were cut from a single piece of wood.
 *
 */
    public void create(Projectgoal resource) throws DataMartException;
    public Projectgoal get(String projecttype, String projectid, Integer goalid) throws DataMartException;
    public Projectgoals getAllByProject(String projecttype, String projectid) throws DataMartException;
    public void update(Projectgoal resource) throws DataMartException;
    public void delete(String projecttype, String projectid, Integer goalid) throws DataMartException;
    public Boolean ping();
}
