package beanutil;

public class DataMartException extends Exception
{
    /*
     * NOTE: Would it be easier for the client to catch/parse a single exception with differentiating
     *   codes (as-is), or with a separate exception for each type of problem?
     *   Using a single custom exception keeps the bean interface from getting out of hand.
     *   You get this:
     *     public void put(Long unitId, String unitName) throws DataMartException;
     *   instead of this:
     *     public void put(Long unitId, String unitName) throws dmCannotUpdateNonexistentResourceException,
     *          dmCannotUpdateLockedResourceException, dmCannotUpdateYourMomsResourceException, etc, etc, etc;
     *
     *   The try/catch block would be similarly nasty. There may yet be a better way around this however.
     */
    public static final int UNKNOWN_ERROR = 0;
    public static final int RESOURCE_NOT_FOUND = 1;
    public static final int ALREADY_EXISTS = 2;
    public static final int MISSING_ELEMENT = 3;
    public static final int BAD_FORMAT = 4;
    public static final int CONSTRAINT_VIOLATION = 5;
    // NOTE: when you add an exception code to this list, you must also update 
    //    AbstractResource.handleException(), so the resources know how to handle it.
    

    private int code;
    private String message;

    public DataMartException()
    {
        code = UNKNOWN_ERROR;
        message = "";
    }

    public DataMartException(int c)
    {
        code = c;
        message = "";
    }

    public DataMartException(String s)
    {
        code = UNKNOWN_ERROR;
        message = s;
    }

    public DataMartException(int c, String s)
    {
        code = c;
        message = s;
    }

    public int getCode()
    {
        return code;
    }

    @Override // Exception.getMessage()
    public String getMessage()
    {
        return message;
    }
}
