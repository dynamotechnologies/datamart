package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.ServletException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.xml.bind.*;
import javax.xml.namespace.QName;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.net.URI;
import java.util.Properties;

/*
 *
 * This class defines basic properties and methods for the entity resources. The resource
 *   itself may choose to override any or all of these, or it may call them directly.
 * The main purpose of this class is to promote code re-use across resource implementations. It should only
 *   contain generic resource code. General application utility methods should go in DataMartUtil.
 *
 * TODO: define the various kinds of resources, and create abstract method signatures here.
 *   These methods can then be implemented by the resource classes. The advantage of this model
 *   is that a compiler error will give us a heads up if we forget to define one of the methods in the resource.
 *
 */
@Slf4j
public abstract class AbstractResource
{
    protected DataMartUtil UTIL = null;
    protected Properties MY_PROPS = new Properties();
    protected int RESOURCE_ID=-1;


    // Method init accepts a bean interface class type as its only parameter,
    //    and returns an object of that type
    // TODO: fix misleading name. should probably be called getBeanManager if it's going to return a bean manager
    protected <T extends Object> T init(Class<T> t)
            throws ServletException, IOException
    {
        if (MY_PROPS.size()!=3)
            throw new ServletException("Each resource must set it's own properties prior to initialization.");

        UTIL = DataMartUtil.getInstance();
        if (UTIL==null)
            throw new ServletException("Horror! Could not get a reference to the Datamart Utility singleton! Exiting in shame.");

        RESOURCE_ID = UTIL.getResourceId(MY_PROPS);

        // Returns the manager object for the specified resource. Only the resource itself
        //   knows the name of the desired remote interface. So we suppress type conversion
        //   warnings and trust that the resource has been configured properly.
        @SuppressWarnings("unchecked")
        T manager = (T)UTIL.getManager(RESOURCE_ID);

        return manager;
    }


    public StringWriter marshal(int ID, Object jaxbelement)
            throws JAXBException
    {
        StringWriter sw = new StringWriter();
        JAXBContext jaxbcontext = UTIL.getContext(ID);
        Marshaller marshaller = jaxbcontext.createMarshaller();

        marshaller.setSchema(UTIL.getSchema(ID));
        marshaller.marshal(jaxbelement, sw);
        return sw;
    }

    public StringWriter marshal(Object jaxbelement)
            throws JAXBException
    {
        return marshal(RESOURCE_ID, jaxbelement);
    }


    public <T extends Object> T unmarshal(DataMartUtil util, int ID, InputStream is, Class<T> type)
            throws WebApplicationException
    {
        // Need to duplicate the input XML before the unmarshaller gets it
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        byte[] buf = new byte[1024];
        int bytesread = 0;
        do {
            try {
                bytesread = is.read(buf);
                if (bytesread > 0) {
                    os.write(buf, 0, bytesread);
                }
            } catch (IOException e) {
                bytesread = 0;
            }
        } while (bytesread > 0);
        String logmsg = os.toString();
        is = new ByteArrayInputStream(os.toByteArray());
        // log request

//        Logger mylogger = Logger.getLogger(this.getClass()); // maybe needs .getSimpleName()
//        mylogger.info(logmsg);
        log.info(logmsg);

        try {
            JAXBContext jaxbcontext = util.getContext(ID);
            Unmarshaller u = jaxbcontext.createUnmarshaller();

            u.setSchema(util.getSchema(ID));
            // Unmarshal the xml into a generic Object
            JAXBElement<T> root = (JAXBElement<T>) u.unmarshal(new StreamSource(is), (Class<T>)type);
            // The xsd schema may define several elements, any of which will validate successfully.
            //   It is important to confirm that we're dealing with the expected element (parameter `type`).
            //   This is done with a simple string comparison.
            QName elementName = util.getElementName(ID, root);
            if (!type.getSimpleName().toLowerCase().equals(elementName.getLocalPart().toLowerCase())) {
                throw new WebApplicationException(Response.Status.UNSUPPORTED_MEDIA_TYPE);
            }
            return (T) root.getValue();
        } catch (JAXBException ex) {
            log.error(null, ex);
            complainLoudly(ex, Response.Status.UNSUPPORTED_MEDIA_TYPE);
        }
        throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
    }


    public <T extends Object> T unmarshal(int ID, InputStream is, Class<T> type)
            throws WebApplicationException
    {
        if (null==UTIL)
            complainLoudly(new Exception("UTIL must be initialized before unmarshalling"), Response.Status.INTERNAL_SERVER_ERROR);
        return unmarshal(UTIL, ID, is, type);
    }

    public <T extends Object> T unmarshal(InputStream is, Class<T> type)
            throws WebApplicationException
    {
        return unmarshal(RESOURCE_ID, is, type);
    }


    protected Response ok(String s)
    {
        StringWriter sw = new StringWriter();
        sw.write(s);
        return ok(sw);
    }

    protected Response ok(StringWriter sw)
    {
        return ok(sw, DataMartUtil.PRIMARY_MEDIA_TYPE);
    }

    protected Response ok(StringWriter sw, String contentType)
    {
        return Response.ok(sw.toString(), contentType).build();
    }

    protected Response created(URI uri)
    {
        return Response.created(uri).build();
    }

    protected Response created(URI uri, StringWriter sw)
    {
        ResponseBuilder b = Response.created(uri);
        b.type(DataMartUtil.PRIMARY_MEDIA_TYPE);
        b.entity(sw.toString());
        return b.build();
    }

    protected Response deleted()
    {
        return Response.noContent().build();
    }

    protected void notFound()
            throws WebApplicationException
    {
        notFound("Not Found");
    }

    protected void notFound(String msg)
            throws WebApplicationException
    {
        complainLoudly(new Exception(msg), Response.Status.NOT_FOUND);
    }


    /*
     * This method maps DataMart exception codes to http status codes
     *
     * An individual Resource may want to override this method in order to perform custom exception handling.
     * This can be done by calling super.handleException() after your custom code. For example:
     *
     *   switch (ex.getCode()) { case: DataMartException.I_AM_A_TEAPOT: log('Cannot make coffee because i am a teapot.'); throw new WebApplicationException(418); }
     *   super.handleException(ex);
     *
     */
    protected void handleException(DataMartException ex)
            throws WebApplicationException
    {
        switch (ex.getCode())
        {
            case DataMartException.UNKNOWN_ERROR:
                complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
            case DataMartException.RESOURCE_NOT_FOUND:
                complainLoudly(ex, Response.Status.NOT_FOUND);
            case DataMartException.ALREADY_EXISTS:
                complainLoudly(ex, Response.Status.CONFLICT);
            case DataMartException.MISSING_ELEMENT:
                complainLoudly(ex, Response.Status.UNSUPPORTED_MEDIA_TYPE);
            case DataMartException.BAD_FORMAT:
                complainLoudly(ex, Response.Status.UNSUPPORTED_MEDIA_TYPE);
            case DataMartException.CONSTRAINT_VIOLATION:
                complainLoudly(ex, Response.Status.CONFLICT);
            // Note: If you add an exception code to DataMartException, you also need to add a handler for it here.
        }
        log.error(null, ex);
        complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
    }

    protected void complainLoudly(java.lang.Throwable t, Response.Status s)
            throws WebApplicationException
    {
        ResponseBuilder builder = Response.status(s);
        builder.type("text/html");
        builder.entity(t.getMessage());
//        Logger.getLogger(AbstractResource.class.getName()).log(Level.FATAL, null, t);
        log.error(s.toString(), t);
        throw new WebApplicationException(t, builder.build());
    }


    protected Boolean getBooleanFromInt(String intname, Integer intval) {
        if (intval == null)
            return null;

        if ((intval == 0) || (intval == 1)) {
            return (intval == 1);
        }
        complainLoudly(new Exception("Bad " + intname + " value"), Response.Status.BAD_REQUEST);
        return null; // so netbeans doesn't complain
    }
}
