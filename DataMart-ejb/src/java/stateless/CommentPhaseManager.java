package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.commentphase.*;

import javax.ejb.Stateless;
import beanutil.*;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.xml.datatype.XMLGregorianCalendar;


@Stateless(mappedName="CommentPhaseManagerRemote")
public class CommentPhaseManager implements CommentPhaseManagerRemote {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void create(Commentphase resource)
             throws DataMartException
    {
        entity.CommentPhases entity = createEntity(resource);

        entity.CommentPhases badentity = retrieveEntity(resource.getCaraprojectid(), resource.getPhaseid());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        em.persist(entity);
    }

    @Override
    public Commentphases getAll(int caraProjectId)
             throws DataMartException
    {
        entity.CARAProjects cproj = (entity.CARAProjects) DataMartBeanUtil.getEntityById(em,
                "CARAProjects", caraProjectId, "CARA Project");
        
        Query query = em.createQuery("SELECT c FROM CommentPhases c WHERE c.cARAProjects = :cproj");
        query.setParameter("cproj", cproj);
        List entitylist =  query.getResultList();
        if (entitylist==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                "No Comment Phases found for project '" + caraProjectId + "'.");

        Commentphases responselist = new Commentphases();
        Iterator it = entitylist.iterator();
        while (it.hasNext()) {
            responselist.getCommentphase().add(createResource((entity.CommentPhases) it.next()));
        }
        return responselist;
    }

    @Override
    public Commentphase get(int caraProjectId, Integer id)
            throws DataMartException
    {
        entity.CommentPhases entity = retrieveEntity(caraProjectId, id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                "Comment Phase with project '" + caraProjectId + "' and id '" + id + "' does not exist.");

        return createResource(entity);
    }

    @Override
    public void update(Commentphase resource)
             throws DataMartException
    {
        entity.CommentPhases entity = createEntity(resource);

        entity.CommentPhases oldentity = retrieveEntity(resource.getCaraprojectid(), resource.getPhaseid());
        if (oldentity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        // set the internal db identifier for the row so jpa knows this is an update and not an insert
        entity.setId(oldentity.getId());

        em.merge(entity);
    }

    @Override
    public void delete(int caraProjectId, Integer id)
             throws DataMartException
    {
        entity.CommentPhases entity = retrieveEntity(caraProjectId, id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     *
     * This method does the mapping from jaxb object to entity object and vice versa
     */
    private Commentphase createResource(entity.CommentPhases entity)
            throws DataMartException
    {
        XMLGregorianCalendar startdate = DataMartBeanUtil.getXMLGregorianFromDate(entity.getStartDate());
        XMLGregorianCalendar finishdate = DataMartBeanUtil.getXMLGregorianFromDate(entity.getFinishDate());

        Commentphase resource = new Commentphase();
        resource.setCaraprojectid(entity.getCARAProjects().getId());
        resource.setPhaseid(entity.getPhaseId());
        resource.setName(entity.getName());
        resource.setStartdate(startdate);
        resource.setFinishdate(finishdate);
        resource.setLettno(entity.getLetterCount());
        resource.setLettuniq(entity.getLetterUniqueCount());
        resource.setLettmst(entity.getLetterMastersCount());
        resource.setLettform(entity.getLetterFormCount());
        resource.setLettformplus(entity.getLetterFormPlusCount());
        resource.setLettformdupe(entity.getLetterFormDupeCount());

        return resource;
    }

    private entity.CommentPhases createEntity(Commentphase resource)
            throws DataMartException
    {
        if (resource.getCaraprojectid()==null)
            throw new DataMartException(DataMartException.MISSING_ELEMENT, "CARA Project ID is required.");

        Integer caraProjectId = resource.getCaraprojectid();
        entity.CARAProjects cproj = (entity.CARAProjects) DataMartBeanUtil.getEntityById(em,
                "CARAProjects", caraProjectId, "CARA Project");
        if (cproj == null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "CARA Project with id '" + caraProjectId + "' does not exist.");

        Date startdate = DataMartBeanUtil.getDateFromXMLGregorian(resource.getStartdate());
        Date finishdate = DataMartBeanUtil.getDateFromXMLGregorian(resource.getFinishdate());

        entity.CommentPhases entity = new entity.CommentPhases();
        entity.setCARAProjects(cproj);
        entity.setPhaseId(resource.getPhaseid());
        entity.setName(resource.getName());
        entity.setStartDate(startdate);
        entity.setFinishDate(finishdate);
        entity.setLetterCount(resource.getLettno());
        entity.setLetterUniqueCount(resource.getLettuniq());
        entity.setLetterMastersCount(resource.getLettmst());
        entity.setLetterFormCount(resource.getLettform());
        entity.setLetterFormPlusCount(resource.getLettformplus());
        entity.setLetterFormDupeCount(resource.getLettformdupe());

        return entity;
    }

    /*
     * Retrieve an entity by caraproject/id
     * return null if project not found
     */
    private entity.CommentPhases retrieveEntity(int caraProjectId, int id)
            throws DataMartException
    {
        entity.CARAProjects cproj = (entity.CARAProjects) DataMartBeanUtil.getEntityById(em,
                "CARAProjects", caraProjectId, "CARA Project");
        
        Query query = em.createQuery("SELECT c FROM CommentPhases c WHERE c.phaseId = :id AND c.cARAProjects = :cproj");
        query.setParameter("id", id);
        query.setParameter("cproj", cproj);
        return (entity.CommentPhases) DataMartBeanUtil.getEntityByQueryAllowNull(query, "Multiple Comment Phases found for cara project '" + caraProjectId + "' with id '" + id + "'.");
    }
}