package stateless;

import us.fed.fs.www.nepa.schema.customproject.*;
import us.fed.fs.www.nepa.schema.projectlisting.*;
import beanutil.DataMartException;
import javax.ejb.Remote;



@Remote
public interface CustomProjectManagerRemote {
    
    public void create(Customproject resource) throws DataMartException;
    public Customproject get(String id) throws DataMartException;
    public Customprojects getAll() throws DataMartException;
    public Customprojects getAllForUnit(String unitid) throws DataMartException;
    public Projectlistings getListingsByUnit(String unitid) throws DataMartException;
    public void update(Customproject resource) throws DataMartException;
    public void delete(String id) throws DataMartException;
    public Boolean ping();
}
