package entity;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "ProjectKMLs")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProjectKMLs.findAll", query = "SELECT p FROM ProjectKMLs p"),
    @NamedQuery(name = "ProjectKMLs.findById", query = "SELECT p FROM ProjectKMLs p WHERE p.id = :id"),
    @NamedQuery(name = "ProjectKMLs.findByLabel", query = "SELECT p FROM ProjectKMLs p WHERE p.label = :label"),
    @NamedQuery(name = "ProjectKMLs.findByMapType", query = "SELECT p FROM ProjectKMLs p WHERE p.mapType = :mapType")})
public class ProjectKMLs implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private String id;
    @Column(name = "Label")
    private String label;
    @Column(name = "MapType")
    private String mapType;
    @JoinColumn(name = "ProjectId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Projects projects;

    public ProjectKMLs() {
    }

    public ProjectKMLs(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getMapType() {
        return mapType;
    }

    public void setMapType(String mapType) {
        this.mapType = mapType;
    }

    public Projects getProjects() {
        return projects;
    }

    public void setProjects(Projects projects) {
        this.projects = projects;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProjectKMLs)) {
            return false;
        }
        ProjectKMLs other = (ProjectKMLs) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ProjectKMLs[ id=" + id + " ]";
    }

}
