package stateless;

import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.projectkml.*;
import beanutil.DataMartException;

@Remote
public interface ProjectKMLManagerRemote {
    public void create(Projectkml resource) throws DataMartException;
    public Projectkml get(String filename) throws DataMartException;
    public void update(Projectkml resource) throws DataMartException;
    public void delete(String filename) throws DataMartException;
    public Boolean ping();
}