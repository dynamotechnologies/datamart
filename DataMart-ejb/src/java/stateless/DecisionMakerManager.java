package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.decisionmaker.*;

import beanutil.*;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Iterator;
import java.util.List;
import us.fed.fs.www.nepa.schema.decisionmaker.Decisionmaker;
import us.fed.fs.www.nepa.schema.decisionmaker.Decisionmakers;

@Stateless(mappedName="DecisionMakerManagerRemote")
public class DecisionMakerManager implements DecisionMakerManagerRemote {

    @PersistenceContext
    private EntityManager em;
    
    @Override
    public void create(Decisionmaker resource) throws DataMartException {
        entity.DecisionMakers entity = createEntity(resource);

        entity.DecisionMakers badentity = retrieveEntity(resource.getId());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS, "Decisionmaker " + resource.getName() + " already exists");

        entity = em.merge(entity);
    }

    @Override
    public void update(Decisionmaker resource) throws DataMartException {
        entity.DecisionMakers entity = createEntity(resource);

        if (retrieveEntity(entity.getId())==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND, "Decisionmaker " + entity.getId() + " does not exist");

        entity = em.merge(entity);
    }

    @Override
    public Decisionmaker get(int decisionid, int id)
            throws DataMartException
    {
        entity.DecisionMakers entity = getEntity(decisionid, id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "DecisionMaker with id '"+id+"' is not associated with decision '"+decisionid+"'.");

        return createResource(entity);
    }


    @Override
    public Decisionmakers getList(int decisionid)
            throws DataMartException
    {
        Decisionmakers rval = new Decisionmakers();

        // Get the Decision
        entity.Decisions d = null;
        
        try {
            d = (entity.Decisions) DataMartBeanUtil.getEntityById(em,
                "Decisions", decisionid, "Decision");
        } catch (DataMartException e) {
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Decision with id '" + decisionid + "' does not exist.");
        }

        Query query = em.createQuery("SELECT r FROM DecisionMakers r WHERE r.decisions = :decision");
        query.setParameter("decision", d);

        @SuppressWarnings("unchecked")
        List<entity.DecisionMakers> entitylist = (List<entity.DecisionMakers>) query.getResultList();

        for (entity.DecisionMakers dar: entitylist) {
            rval.getDecisionmaker().add(createResource(dar));
        }

        return rval;
    }


    @Override
    public void delete(int decisionid, int id)
             throws DataMartException
    {
        // Verify that this entity exists
        entity.DecisionMakers entity = getEntity(decisionid, id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "DecisionMaker with id '"+id+"' is not associated with decision '"+decisionid+"'.");

        em.remove(entity);
    }
    
    public Boolean ping() {
        return true;
    }

    /*
     * Create XML from EJB
     */
    private Decisionmaker createResource(entity.DecisionMakers entity)
            throws DataMartException
    {
        Decisionmaker resource = new Decisionmaker();
        resource.setId(entity.getId());
        resource.setDecisionid(entity.getDecisions().getId());
        resource.setName(entity.getName());
        resource.setTitle(entity.getTitle());
        resource.setSigneddate(DataMartBeanUtil.getXMLGregorianFromDateTime(entity.getDecMakeSignedDate()));
        return resource;
    }

    /*
     * Create EJB from XML
     */
    private entity.DecisionMakers createEntity(Decisionmaker resource)
            throws DataMartException
    {
        entity.DecisionMakers entity = new entity.DecisionMakers();
        
        // This must return exactly one Decision record or else
        // a DataMartException of type CONSTRAINT_VIOLATION is thrown
        entity.Decisions decisions = (entity.Decisions) DataMartBeanUtil.getEntityById(em,
                "Decisions", resource.getDecisionid(), "Decision");
        
        
        
        entity.setId(resource.getId() );
        entity.setName(resource.getName());
        entity.setTitle(resource.getTitle() );
        entity.setDecMakeSignedDate(DataMartBeanUtil.getDateFromXMLGregorian(resource.getSigneddate()));
        entity.setDecisions( decisions );
        return entity;
    }

    private entity.DecisionMakers retrieveEntity(Integer Decisionmakerid)
            throws DataMartException
    {
        return em.find(entity.DecisionMakers.class, Decisionmakerid);
    }
    

    // Accepts ID parameter, and returns the associated entity,
    //   or null if it doesn't exist
    private entity.DecisionMakers getEntity(Integer decisionid, Integer id)
            throws DataMartException
    {
        // Get the Decision
        entity.Decisions d = null;
        try {
            d = (entity.Decisions) DataMartBeanUtil.getEntityById(em,
                "Decisions", decisionid, "Decision");
        } catch (DataMartException e) {
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Decision with id '" + decisionid + "' does not exist.");
        }
// System.out.println("DecisionMakerManager.getENtity() "+id);
        // Get the DecisionMaker
        entity.DecisionMakers dm = null;
        try {
            dm = (entity.DecisionMakers) DataMartBeanUtil.getEntityById(em,
                "DecisionMakers", id, "Decision Maker");
        } catch (DataMartException e) {
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Decision Maker with id '" + id + "' does not exist.");
        }

        Query query = em.createQuery("SELECT r FROM DecisionMakers r WHERE r.decisions = :decision AND r.id = :decmaker");
        query.setParameter("decision", d);
        query.setParameter("decmaker", id);
        return (entity.DecisionMakers)DataMartBeanUtil.getEntityByQueryAllowNull(
                query, "Found multiple DecisionMakers for decisionid "+decisionid+" and decision maker id "+id);
    }
}
