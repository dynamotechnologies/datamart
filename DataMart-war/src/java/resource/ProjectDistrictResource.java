package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.ProjectDistrictManagerRemote;
import us.fed.fs.www.nepa.schema.projectlocation.Districts;
import us.fed.fs.www.nepa.schema.projectlocation.ObjectFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;


@Path ("/projects/nepa/{projectid}/locations/districts")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class ProjectDistrictResource extends AbstractResource
{
    private ProjectDistrictManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public ProjectDistrictResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "ProjectLocation.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.projectlocation");
        MY_PROPS.setProperty("bean", "ProjectDistrictManagerRemote");
        try {
            beanmanager = (ProjectDistrictManagerRemote) init(ProjectDistrictManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("ProjectDistrictResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("ProjectDistrictResource failed to start. Could not ping the bean manager!");
    }


    @GET
    @Path("/{id}")
    public Response get(@PathParam("projectid") String projectid, @PathParam("id") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            Districts district = beanmanager.get(projectid, id);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Districts> jaxbelement = objectfactory.createDistricts(district);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @PUT
    @Path("/{id}")
    public Response put(@PathParam("projectid") String projectid, @PathParam("id") String id, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.merge(projectid, id);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"</location>");
    }


    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("projectid") String projectid, @PathParam("id") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(projectid, id);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}
