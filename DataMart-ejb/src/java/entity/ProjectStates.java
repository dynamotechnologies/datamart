package entity;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "ProjectStates")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProjectStates.findAll", query = "SELECT p FROM ProjectStates p"),
    @NamedQuery(name = "ProjectStates.findById", query = "SELECT p FROM ProjectStates p WHERE p.id = :id")})
public class ProjectStates implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @JoinColumn(name = "StateId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private RefStates refStates;
    @JoinColumn(name = "ProjectId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Projects projects;

    public ProjectStates() {
    }

    public ProjectStates(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public RefStates getRefStates() {
        return refStates;
    }

    public void setRefStates(RefStates refStates) {
        this.refStates = refStates;
    }

    public Projects getProjects() {
        return projects;
    }

    public void setProjects(Projects projects) {
        this.projects = projects;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProjectStates)) {
            return false;
        }
        ProjectStates other = (ProjectStates) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ProjectStates[ id=" + id + " ]";
    }

}
