package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.caradocument.*;


public class CARAPhaseDocClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public CARAPhaseDocClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.caradocument", "CaraDocument.xsd");
    }


    public void linkDoc(int caraid, int phaseid, String docid)
            throws IOException
    {
        Caralinkdoc linkrequest = new Caralinkdoc();
        linkrequest.setCaraid(caraid);
        linkrequest.setPhaseid(phaseid);
        linkrequest.setDocid(docid);
        linkDoc(linkrequest);
    }


    public void linkDoc(Caralinkdoc linkrequest)
            throws IOException
    {
        post(objectfactory.createCaralinkdoc(linkrequest), "/utils/cara/linkDoc");
    }


    public void unlinkDoc(int caraid, int phaseid, String docid)
            throws IOException
    {
        Caraunlinkdoc unlinkrequest = new Caraunlinkdoc();
        unlinkrequest.setCaraid(caraid);
        unlinkrequest.setPhaseid(phaseid);
        unlinkrequest.setDocid(docid);
        unlinkDoc(unlinkrequest);
    }


    public void unlinkDoc(Caraunlinkdoc unlinkrequest)
            throws IOException
    {
        post(objectfactory.createCaraunlinkdoc(unlinkrequest), "/utils/cara/unlinkDoc");
    }

}