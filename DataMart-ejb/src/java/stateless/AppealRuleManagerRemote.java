package stateless;

import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.appealrule.*;
import beanutil.DataMartException;


@Remote
public interface AppealRuleManagerRemote {
    public void create(Appealrule resource) throws DataMartException;
    public String getRule(String id) throws DataMartException;
    public String getDescription(String id) throws DataMartException;
    public void update(Appealrule resource) throws DataMartException;
    public void delete(String id) throws DataMartException;
    public Appealrules getAll() throws DataMartException;
    public Boolean ping();
}
