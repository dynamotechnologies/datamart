package test.entity;

import client.CARAProjectClient;
import client.ProjectClient;
import client.SubscriberClient;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.subscriber.*;


public class SubscriberTest {
    private SubscriberClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new SubscriberClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postSubscriber(  TestData.caraprojects.id1,
                                    TestData.subscribers.id1,
                                    TestData.subscribers.email2,
                                    TestData.subscribers.lastname2,
                                    TestData.subscribers.firstname2,
                                    TestData.subscribers.title2,
                                    TestData.subscribers.org1,
                                    TestData.subscribers.orgtype1,
                                    TestData.subscribers.subscribertype1,
                                    TestData.subscribers.addstreet1_1,
                                    TestData.subscribers.addstreet2_1,
                                    TestData.subscribers.city1,
                                    TestData.subscribers.state1,
                                    TestData.subscribers.zip1,
                                    TestData.subscribers.prov1,
                                    TestData.subscribers.country1,
                                    TestData.subscribers.phone1,
                                    TestData.subscribers.contactmethod1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Subscriber subscriber = null;
        Subscribers subscriberlist = null;
        try {
            subscriber = client.getSubscriber(TestData.caraprojects.id1, TestData.subscribers.id1);
            subscriberlist = client.getSubscriberList(TestData.caraprojects.id1);
        } catch (IOException ex) {
            Logger.getLogger(SubscriberTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.subscribers.id1, subscriber.getSubscriberid());
        Assert.assertEquals(TestData.subscribers.email1, subscriber.getEmail());
        Assert.assertEquals(TestData.subscribers.lastname1, subscriber.getLastname());
        Assert.assertEquals(TestData.subscribers.firstname1, subscriber.getFirstname());
        Assert.assertEquals(TestData.subscribers.title1, subscriber.getTitle());
        Assert.assertEquals(TestData.subscribers.org1, subscriber.getOrg());
        Assert.assertEquals(TestData.subscribers.orgtype1, subscriber.getOrgtype());
        Assert.assertEquals(TestData.subscribers.subscribertype1, subscriber.getSubscribertype());
        Assert.assertEquals(TestData.subscribers.addstreet1_1, subscriber.getAddstreet1());
        Assert.assertEquals(TestData.subscribers.addstreet2_1, subscriber.getAddstreet2());
        Assert.assertEquals(TestData.subscribers.city1, subscriber.getCity());
        Assert.assertEquals(TestData.subscribers.state1, subscriber.getState());
        Assert.assertEquals(TestData.subscribers.zip1, subscriber.getZip());
        Assert.assertEquals(TestData.subscribers.prov1, subscriber.getProv());
        Assert.assertEquals(TestData.subscribers.country1, subscriber.getCountry());
        Assert.assertEquals(TestData.subscribers.phone1, subscriber.getPhone());
        Assert.assertEquals(TestData.subscribers.contactmethod1, subscriber.getContactmethod());

        Assert.assertTrue(subscriberlist.getSubscriber().size()>0);
        Assert.assertTrue(subscriberlist.getSubscriber().get(0).getAddstreet1().length()>0);
        Assert.assertTrue(subscriberlist.getSubscriber().get(0).getFirstname().length()>0);
        Assert.assertTrue(subscriberlist.getSubscriber().get(0).getPhone().length()>0);

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        Subscriber subscriber = null;
        try {
            client.putSubscriber(  TestData.caraprojects.id1,
                                    TestData.subscribers.id1,
                                    TestData.subscribers.email2,
                                    TestData.subscribers.lastname2,
                                    TestData.subscribers.firstname2,
                                    TestData.subscribers.title2,
                                    TestData.subscribers.org1,
                                    TestData.subscribers.orgtype1,
                                    TestData.subscribers.subscribertype1,
                                    TestData.subscribers.addstreet1_1,
                                    TestData.subscribers.addstreet2_1,
                                    TestData.subscribers.city1,
                                    TestData.subscribers.state1,
                                    TestData.subscribers.zip1,
                                    TestData.subscribers.prov1,
                                    TestData.subscribers.country1,
                                    TestData.subscribers.phone1,
                                    TestData.subscribers.contactmethod1);
            subscriber = client.getSubscriber(TestData.caraprojects.id1, TestData.subscribers.id1);
        } catch (Exception ex) {
            Logger.getLogger(SubscriberTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.subscribers.id1, subscriber.getSubscriberid());
        Assert.assertEquals(TestData.subscribers.email2, subscriber.getEmail());
        Assert.assertEquals(TestData.subscribers.lastname2, subscriber.getLastname());
        Assert.assertEquals(TestData.subscribers.firstname2, subscriber.getFirstname());
        Assert.assertEquals(TestData.subscribers.title2, subscriber.getTitle());
        Assert.assertEquals(TestData.subscribers.org1, subscriber.getOrg());
        Assert.assertEquals(TestData.subscribers.orgtype1, subscriber.getOrgtype());
        Assert.assertEquals(TestData.subscribers.subscribertype1, subscriber.getSubscribertype());
        Assert.assertEquals(TestData.subscribers.addstreet1_1, subscriber.getAddstreet1());
        Assert.assertEquals(TestData.subscribers.addstreet2_1, subscriber.getAddstreet2());
        Assert.assertEquals(TestData.subscribers.city1, subscriber.getCity());
        Assert.assertEquals(TestData.subscribers.state1, subscriber.getState());
        Assert.assertEquals(TestData.subscribers.zip1, subscriber.getZip());
        Assert.assertEquals(TestData.subscribers.prov1, subscriber.getProv());
        Assert.assertEquals(TestData.subscribers.country1, subscriber.getCountry());
        Assert.assertEquals(TestData.subscribers.phone1, subscriber.getPhone());
        Assert.assertEquals(TestData.subscribers.contactmethod1, subscriber.getContactmethod());

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getSubscriber(TestData.caraprojects.id1, TestData.subscribers.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteSubscriber(TestData.caraprojects.id1, TestData.subscribers.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }

    private void post()
    {
        try {
            client.postSubscriber(  TestData.caraprojects.id1,
                                    TestData.subscribers.id1,
                                    TestData.subscribers.email1,
                                    TestData.subscribers.lastname1,
                                    TestData.subscribers.firstname1,
                                    TestData.subscribers.title1,
                                    TestData.subscribers.org1,
                                    TestData.subscribers.orgtype1,
                                    TestData.subscribers.subscribertype1,
                                    TestData.subscribers.addstreet1_1,
                                    TestData.subscribers.addstreet2_1,
                                    TestData.subscribers.city1,
                                    TestData.subscribers.state1,
                                    TestData.subscribers.zip1,
                                    TestData.subscribers.prov1,
                                    TestData.subscribers.country1,
                                    TestData.subscribers.phone1,
                                    TestData.subscribers.contactmethod1);
        } catch (IOException ex) {
            Logger.getLogger(SubscriberTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteSubscriber(TestData.caraprojects.id1, TestData.subscribers.id1);
        } catch (IOException ex) {
            Logger.getLogger(SubscriberTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteSubscriber(TestData.caraprojects.id1, TestData.subscribers.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }


    @BeforeClass
    public static void  populateDependencies()
    {
        ProjectClient projclient = null;
        CARAProjectClient caraprojclient = null;
        try {
            projclient = new ProjectClient();
            caraprojclient = new CARAProjectClient();
        } catch (Exception ex) {
            Assert.fail();
        }

        // Project Documents rely on Projects. Pre-populate a Project.
        ArrayList<String> purposeids1 = new ArrayList<String>();
        purposeids1.add(TestData.projects.purposeid1);
        purposeids1.add(TestData.projects.purposeid2);
        purposeids1.add(TestData.projects.purposeid3);
        try {
            projclient.postProject(TestData.projects.id1, TestData.projects.type1,
                    TestData.projects.name1, TestData.projects.unitcode1, TestData.projects.description1,
                    TestData.projects.lastupdate1, TestData.projects.commentreg1, TestData.projects.wwwlink1,
                    TestData.projects.wwwsummary1, TestData.projects.wwwpub1, TestData.projects.analysistypeid1,
                    TestData.projects.statusid1, TestData.projects.contactname1, TestData.projects.contactphone1,
                    TestData.projects.contactemail1, purposeids1, TestData.projects.expirationdate1, TestData.projects.projectdocumentid1,
                    TestData.projects.sopapub, TestData.projects.sopanew, TestData.projects.sopaheader, TestData.projects.esd, TestData.projects.cenodecision);
        } catch (IOException ex) {
            Logger.getLogger(ProjectDocumentTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Link to a CARA ID
        try {
            caraprojclient.linkProject(TestData.projects.id1, TestData.caraprojects.id1);
        } catch (Exception ex) {
            Logger.getLogger(CommentPhaseTest.class.getName()).log(Level.SEVERE, null, ex);
            cleanupDependencies();
            Assert.fail();
        }
    }

    @AfterClass
    public static void cleanupDependencies()
    {
        ProjectClient projclient = null;
        CARAProjectClient caraprojclient = null;
        try {
            projclient = new ProjectClient();
            caraprojclient = new CARAProjectClient();
        } catch (Exception ex) {
            Assert.fail();
        }

        // Unlink the CARA project
        try {
            caraprojclient.unlinkProject(TestData.caraprojects.id1);
        } catch (Exception ex) {
            Logger.getLogger(CommentPhaseTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Clean up the Project
        try {
            projclient.deleteProject("nepa", TestData.projects.id1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectDocumentTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }
}