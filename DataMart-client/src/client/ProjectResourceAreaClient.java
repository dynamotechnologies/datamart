package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.projectresourcearea.*;


public class ProjectResourceAreaClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public ProjectResourceAreaClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.projectresourcearea", "ProjectResourceArea.xsd");
    }


    public void postProjectResourceArea(String projecttype, String projectid, Integer resourceareaid, Integer order)
            throws IOException
    {
        Projectresourcearea projectresourcearea = new Projectresourcearea();
        projectresourcearea.setProjecttype(projecttype);
        projectresourcearea.setProjectid(projectid);
        projectresourcearea.setResourceareaid(resourceareaid);
        projectresourcearea.setOrder(order);
        postProjectResourceArea(projectresourcearea);
    }


    public void postProjectResourceArea(Projectresourcearea projectresourcearea)
            throws IOException
    {
        String projecttype = projectresourcearea.getProjecttype();
        String projectid = projectresourcearea.getProjectid();
        post(objectfactory.createProjectresourcearea(projectresourcearea), "/projects/"+projecttype+"/"+projectid+"/resourceareas/");
    }


    public Projectresourcearea getProjectResourceArea(String projecttype, String projectid, Integer resourceareaid)
            throws IOException
    {
        return get("/projects/"+projecttype+"/"+projectid+"/resourceareas/"+ resourceareaid, Projectresourcearea.class);
    }

    public Projectresourceareas getProjectResourceAreasByProject(String projecttype, String projectid)
            throws IOException
    {
        return get("/projects/"+projecttype+"/"+projectid+"/resourceareas/", Projectresourceareas.class);
    }
    
    public void putProjectResourceArea(String projecttype, String projectid, Integer resourceareaid, Integer order)
            throws IOException
    {
        Projectresourcearea projectresourcearea = new Projectresourcearea();
        projectresourcearea.setProjecttype(projecttype);
        projectresourcearea.setProjectid(projectid);
        projectresourcearea.setResourceareaid(resourceareaid);
        projectresourcearea.setOrder(order);
        putProjectResourceArea(projectresourcearea);
    }


    public void putProjectResourceArea(Projectresourcearea projectresourcearea)
            throws IOException
    {
        String projecttype = projectresourcearea.getProjecttype();
        String projectid = projectresourcearea.getProjectid();
        Integer resourceareaid = projectresourcearea.getResourceareaid();
        put(objectfactory.createProjectresourcearea(projectresourcearea), "/projects/"+projecttype+"/"+projectid+"/resourceareas/"+ resourceareaid);
    }


    public void deleteProjectResourceArea(String projecttype, String projectid, Integer resourceareaid)
            throws IOException
    {
        delete("/projects/"+projecttype+"/"+projectid+"/resourceareas/"+ resourceareaid);
    }

}
