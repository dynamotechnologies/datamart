package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "CARAProjects")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CARAProjects.findAll", query = "SELECT c FROM CARAProjects c"),
    @NamedQuery(name = "CARAProjects.findById", query = "SELECT c FROM CARAProjects c WHERE c.id = :id"),
    @NamedQuery(name = "CARAProjects.findByReadingRoomActive", query = "SELECT c FROM CARAProjects c WHERE c.readingRoomActive = :readingRoomActive")})
public class CARAProjects implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "ReadingRoomActive")
    private boolean readingRoomActive;
    @JoinColumn(name = "ProjectId", referencedColumnName = "Id")
    @OneToOne(optional = false)
    private Projects projects;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cARAProjects")
    private Collection<CommentPhases> commentPhasesCollection;

    public CARAProjects() {
    }

    public CARAProjects(Integer id) {
        this.id = id;
    }

    public CARAProjects(Integer id, boolean readingRoomActive) {
        this.id = id;
        this.readingRoomActive = readingRoomActive;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean getReadingRoomActive() {
        return readingRoomActive;
    }

    public void setReadingRoomActive(boolean readingRoomActive) {
        this.readingRoomActive = readingRoomActive;
    }

    public Projects getProjects() {
        return projects;
    }

    public void setProjects(Projects projects) {
        this.projects = projects;
    }

    @XmlTransient
    public Collection<CommentPhases> getCommentPhasesCollection() {
        return commentPhasesCollection;
    }

    public void setCommentPhasesCollection(Collection<CommentPhases> commentPhasesCollection) {
        this.commentPhasesCollection = commentPhasesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CARAProjects)) {
            return false;
        }
        CARAProjects other = (CARAProjects) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CARAProjects[ id=" + id + " ]";
    }

}
