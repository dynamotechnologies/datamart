package test.entity;

import client.DocumentContainerClient;
import client.ProjectClient;
import client.ProjectDocumentClient;
import java.io.IOException;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Collections;
import java.util.Comparator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.BeforeClass;
import org.junit.AfterClass;
import test.TestData;
import us.fed.fs.www.nepa.schema.documentcontainer.*;
import us.fed.fs.www.nepa.schema.project.*;
import us.fed.fs.www.nepa.schema.projectdocument.*;


public class DocumentContainerTest {
    private DocumentContainerClient client;

    private String container1_serialized = "{CONTAINER:ROOT:null{DOC:docID1:1}{CONTAINER:this is label 2:2{DOC:docID2:2}{DOC:docID3:3}{CONTAINER:this is label 3:3{DOC:docID4:4}{DOC:docID5:5}}}}";
    private String container1_serialized_pub = "{CONTAINER:ROOT:null{DOC:docID1:1}{CONTAINER:this is label 2:2{DOC:docID2:2}{DOC:docID3:3}{CONTAINER:this is label 3:3{DOC:docID4:4}}}}";
    private String container2_serialized = "{CONTAINER:ROOT:null{DOC:docID2:2}{CONTAINER:this is label 3:3{CONTAINER:this is label 2:2{DOC:docID1:1}{DOC:docID3:3}}{DOC:docID4:4}{DOC:docID5:5}}}";
    private String emptytree_serialized  = "{CONTAINER:ROOT:null{DOC:docID1:0}{DOC:docID2:0}{DOC:docID3:0}{DOC:docID4:0}{DOC:docID5:0}}";
    private String container1template_serialized = "{CONTAINER:ROOT:null{CONTAINER:this is label 2:2{CONTAINER:this is label 3:3}}}";
    private String updateTemplate_1_serialized   = "{CONTAINER:ROOT:null{DOC:docID2:2}{CONTAINER:this is label 3:3{CONTAINER:this is label 2:2{DOC:docID1:1}{DOC:docID3:3}}{DOC:docID4:4}{DOC:docID5:5}}{CONTAINER:Test container 4:4}}";
    private String updateTemplate_2_serialized   = "{CONTAINER:ROOT:null{DOC:docID2:2}{CONTAINER:Test container 4:4{CONTAINER:this is label 3:3{CONTAINER:this is label 2:2{DOC:docID1:1}{DOC:docID3:3}}{DOC:docID4:4}{DOC:docID5:5}}}}";
    private String updateContainerdocs_serialized   = "{CONTAINER:ROOT:null{CONTAINER:this is label 3:3{CONTAINER:this is label 2:2{DOC:docID1:1}{DOC:docID3:3}{DOC:docID4:4}{DOC:docID2:5}}{DOC:docID5:5}}}";

    @Before
    public void setUp()
            throws IOException
    {
         client = new DocumentContainerClient();
    }

    /*
     * Using POST, store Container1 as the container tree, then overwrite it
     * with another POST of Container2
     */
    @Test
    public void testPost()
    {
        resetToTestContainer1();
        
        Container container2 = getTestContainer2();
        
        // A second POST (using test container 2) should just overwrite the first set of data
        try {
            client.postDocumentContainer(TestData.projects.type1, TestData.containers.projectid1, container2.getContainerOrContainerdoc());
        } catch (IOException ex) {
            Logger.getLogger(DocumentContainerTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        
        // Check results
        try {
            Containers containerlist = client.getContainerTree(TestData.projects.type1, TestData.containers.projectid1);
            Assert.assertEquals(container2_serialized, serializeContainers(containerlist));
        } catch (IOException ex) {
            Logger.getLogger(DocumentContainerTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        
        deleteTestContainer();
    }

    /*
     * Using GET, examine the contents of the tree created in resetToTestContainer1()
     * This should match the definition of Container1
     */
    @Test
    public void testGet()
    {
        resetToTestContainer1();

        try {
            Containers containerlist = client.getContainerTree(TestData.projects.type1, TestData.containers.projectid1);
            Assert.assertEquals(container1_serialized, serializeContainers(containerlist));
        } catch (IOException ex) {
            Logger.getLogger(DocumentContainerTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Confirm unpublished document does not come back when pubflag is specified
        try {
            Containers containerlist = client.getContainerTree(TestData.projects.type1, TestData.containers.projectid1, false);
            Assert.assertEquals(container1_serialized, serializeContainers(containerlist));

            containerlist = client.getContainerTree(TestData.projects.type1, TestData.containers.projectid1, null);
            Assert.assertEquals(container1_serialized, serializeContainers(containerlist));

            containerlist = client.getContainerTree(TestData.projects.type1, TestData.containers.projectid1, true);
            Assert.assertEquals(container1_serialized_pub, serializeContainers(containerlist));
        } catch (IOException ex) {
            Logger.getLogger(DocumentContainerTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        deleteTestContainer();
    }

    /*
     * Get the container structure only (no docs)
     */
    @Test
    public void testGetTemplate()
    {
        resetToTestContainer1();

        Containers containerlist = null;
        try {
            containerlist = client.getContainerTemplate(TestData.projects.type1, TestData.containers.projectid1);
            Assert.assertEquals(container1template_serialized, serializeContainers(containerlist));
        } catch (IOException ex) {
            Logger.getLogger(DocumentContainerTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    /*
     * Using PUT, replace the container tree once again with Container2
     */
    @Test
    public void testPut()
    {
        resetToTestContainer1();
        
        Container container2 = getTestContainer2();
        
        /*
         * Test should create a local container tree, put it to Datamart,
         * retrieve it, compare retrieved version to original through asserts
         */
        try {
            client.putContainerTree(TestData.projects.type1, TestData.containers.projectid1, container2.getContainerOrContainerdoc());
            Containers containerlist = client.getContainerTree(TestData.projects.type1, TestData.containers.projectid1);
            Assert.assertEquals(container2_serialized, serializeContainers(containerlist));
        } catch (Exception ex) {
            Logger.getLogger(DocumentContainerTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        deleteTestContainer();
    }

    /*
     * Get the document list from a specific container
     */
    @Test
    public void testGetContainerdocs()
    {
        /* TEST: Initialize to test container1, which has no contid values */
        resetToTestContainer1();

        try {
            Containerdocs containerdoclist = null;
            // This should fail because of missing contid values
            containerdoclist = client.getContainerDocs(TestData.projects.type1, TestData.containers.projectid1,
                    TestData.containers.contid2, null, null);
            Assert.fail();
        } catch (IOException ex) {
            System.out.println("getContainerDocs failed with: " + ex.getMessage() + "\n");
            String expectederror = "Cannot find container 200 ";
            if (ex.getMessage().startsWith(expectederror)) {
                System.out.println("OK--expected behavior\n");
            } else {
                Assert.fail();
            }
        }

        /* TEST: Reinitialize to test container2, which has unique contids, and retry */
        resetToTestContainer2();

        try {
            Containerdocs containerdoclist = null;
            containerdoclist = client.getContainerDocs(TestData.projects.type1, TestData.containers.projectid1,
                    TestData.containers.contid2, null, null);
            Assert.assertEquals(TestData.containers.docid1, containerdoclist.getContainerdoc().get(0).getDocid());
            Assert.assertEquals(TestData.containers.docid3, containerdoclist.getContainerdoc().get(1).getDocid());
            Assert.assertEquals(containerdoclist.getContainerdoc().size(), 2);
        } catch (IOException ex) {
            Logger.getLogger(DocumentContainerTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        /* TEST: Fetch paged results */
        /*  Container 2 has 2 documents, docid1 and docid3 */
        /* start = start, rows = 1 */
        try {
            Containerdocs containerdoclist = null;
            containerdoclist = client.getContainerDocs(TestData.projects.type1, TestData.containers.projectid1,
                    TestData.containers.contid2, null, 1);
            Assert.assertEquals(TestData.containers.docid1, containerdoclist.getContainerdoc().get(0).getDocid());
            Assert.assertEquals(containerdoclist.getContainerdoc().size(), 1);
        } catch (IOException ex) {
            Logger.getLogger(DocumentContainerTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        /* start = 1, rows = 1 */
        try {
            Containerdocs containerdoclist = null;
            containerdoclist = client.getContainerDocs(TestData.projects.type1, TestData.containers.projectid1,
                    TestData.containers.contid2, 1, 1);
            Assert.assertEquals(TestData.containers.docid3, containerdoclist.getContainerdoc().get(0).getDocid());
            Assert.assertEquals(containerdoclist.getContainerdoc().size(), 1);
        } catch (IOException ex) {
            Logger.getLogger(DocumentContainerTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        /* start = 2, rows = null */
        try {
            Containerdocs containerdoclist = null;
            containerdoclist = client.getContainerDocs(TestData.projects.type1, TestData.containers.projectid1,
                    TestData.containers.contid2, 2, null);
            Assert.assertEquals(containerdoclist.getContainerdoc().size(), 0);
        } catch (IOException ex) {
            Logger.getLogger(DocumentContainerTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    /*
     * Update the template only, without losing the document structure
     */
    @Test
    public void testUpdateTemplate()
    {
        resetToTestContainer2();    // Use test container2, which has unique contid values in Container nodes

        // Try updating with a container with documents (should fail as bad request)
        Container templateroot = getTestContainer2();
        try {
            client.putContainerTemplate(TestData.projects.type1, TestData.containers.projectid1, templateroot.getContainerOrContainerdoc());
            Assert.fail();
        } catch (IOException ex) {
            System.out.println("putContainerTemplate failed with: " + ex.getMessage() + "\n");
            String expectederrorpat = "(?s).*, bad request contains non-container nodes.*";
            if (ex.getMessage().trim().matches(expectederrorpat)) {
                System.out.println("OK--expected behavior\n");
            } else {
                Assert.fail();
            }
        }

        // Try adding a container 4 to the root
        // Setup
        templateroot.getContainerOrContainerdoc().clear();
        templateroot.getContainerOrContainerdoc().addAll(buildTestContainer2(true).getContainerOrContainerdoc());
        Container container4 = new Container();
        container4.setContid(TestData.containers.contid4);
        container4.setLabel("Test container 4");
        container4.setOrder(TestData.containers.contorder4);
        templateroot.getContainerOrContainerdoc().add(container4);

        // Update the container
        try {
            client.putContainerTemplate(TestData.projects.type1, TestData.containers.projectid1, templateroot.getContainerOrContainerdoc());
        } catch (IOException ex) {
            Logger.getLogger(DocumentContainerTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        // Check results
        try {
            Containers containerlist = client.getContainerTree(TestData.projects.type1, TestData.containers.projectid1);
            System.out.println(serializeContainers(containerlist));
            Assert.assertEquals(updateTemplate_1_serialized, serializeContainers(containerlist));
        } catch (IOException ex) {
            Logger.getLogger(DocumentContainerTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Try deleting container 3 (should fail because non-empty)
        // Setup
        Boolean containerdeleted = false;
        Container container3 = null;
        for (Object obj : templateroot.getContainerOrContainerdoc()) {
            if (obj instanceof Container) {
                Container cont = (Container)obj;
                if (cont.getContid() == TestData.containers.contid3) {
                    container3 = cont;
                    templateroot.getContainerOrContainerdoc().remove(cont);
                    containerdeleted = true;
                    break;
                }
            }
        }
        if (containerdeleted == false) {
            Assert.fail();
        }
        // Update the container, check for exception
        try {
            client.putContainerTemplate(TestData.projects.type1, TestData.containers.projectid1, templateroot.getContainerOrContainerdoc());
            Assert.fail();
        } catch (IOException ex) {
            System.out.println("putContainerTemplate failed with: " + ex.getMessage() + "\n");
            String expectederror = "Container 200 in project nepa/99912345 has documents";
            if (ex.getMessage().startsWith(expectederror)) {
                System.out.println("OK--expected behavior\n");
            } else {
                Assert.fail();
            }
        }

        // Try moving container 3 into container 4
        // Setup
        Boolean containermoved = false;
        for (Object obj : templateroot.getContainerOrContainerdoc()) {
            if (obj instanceof Container) {
                Container cont = (Container)obj;
                if (cont.getContid() == TestData.containers.contid4) {
                    cont.getContainerOrContainerdoc().add(container3);
                    containermoved = true;
                    break;
                }
            }
        }
        // Update the container
        try {
            client.putContainerTemplate(TestData.projects.type1, TestData.containers.projectid1, templateroot.getContainerOrContainerdoc());
        } catch (IOException ex) {
            Logger.getLogger(DocumentContainerTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        // Check results
        try {
            Containers containerlist = client.getContainerTree(TestData.projects.type1, TestData.containers.projectid1);
            System.out.println(serializeContainers(containerlist));
            Assert.assertEquals(updateTemplate_2_serialized, serializeContainers(containerlist));
        } catch (IOException ex) {
            Logger.getLogger(DocumentContainerTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Try restoring original container structure (deleting container 4)
        // Setup
        templateroot.getContainerOrContainerdoc().clear();
        templateroot.getContainerOrContainerdoc().addAll(buildTestContainer2(true).getContainerOrContainerdoc());
        // Update the container
        try {
            client.putContainerTemplate(TestData.projects.type1, TestData.containers.projectid1, templateroot.getContainerOrContainerdoc());
        } catch (IOException ex) {
            Logger.getLogger(DocumentContainerTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        // Check results
        try {
            Containers containerlist = client.getContainerTree(TestData.projects.type1, TestData.containers.projectid1);
            System.out.println(serializeContainers(containerlist));
            Assert.assertEquals(container2_serialized, serializeContainers(containerlist));
        } catch (IOException ex) {
            Logger.getLogger(DocumentContainerTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    /*
     * Update attributes for a list of container docs, including parent container
     */
    @Test
    public void testUpdateContainerdcocs()
    {
        resetToTestContainer2();    // Use test container2, which has unique contid values in Container nodes

        // Move both doc2 and doc4 into container contid=2
        Containerdocs request = new Containerdocs();
        Containerdoc doc = new Containerdoc();
        doc.setDocid(TestData.containers.docid2);
        doc.setParentcontid(TestData.containers.contid2);
        doc.setOrder(TestData.containers.contorder5);   // Set order to 5
        request.getContainerdoc().add(doc);

        doc = new Containerdoc();
        doc.setDocid(TestData.containers.docid4);
        doc.setParentcontid(TestData.containers.contid2);
        request.getContainerdoc().add(doc);

        // Update the documents
        try {
            client.postContainerdocs(TestData.projects.type1, TestData.containers.projectid1, request);
        } catch (IOException ex) {
            Logger.getLogger(DocumentContainerTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        // Check results
        try {
            Containers containerlist = client.getContainerTree(TestData.projects.type1, TestData.containers.projectid1);
            System.out.println(serializeContainers(containerlist));
            Assert.assertEquals(updateContainerdocs_serialized, serializeContainers(containerlist));
        } catch (IOException ex) {
            Logger.getLogger(DocumentContainerTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    /*
     * DELETE the container tree several times (this is permitted)
     */
    @Test
    public void testDelete()
    {
        resetToTestContainer1();
        deleteTestContainer();

        // Subsequent GET requests on this deleted entity should now return empty lists
        try {
            Containers containerlist = client.getContainerTree(TestData.projects.type1, TestData.containers.projectid1);
            Assert.assertEquals(emptytree_serialized, serializeContainers(containerlist));
        } catch (IOException ex) {
            Logger.getLogger(DocumentContainerTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Subsequent DELETEs should be OK, this looks fishy but is permitted
        try {
            client.deleteContainerTree(TestData.projects.type1, TestData.containers.projectid1);
        } catch (IOException ex) {
            Logger.getLogger(DocumentContainerTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    /*
     * Reset the test container tree to look like test container 1
     */
    private void resetToTestContainer1()
    {
        Container container1 = getTestContainer1();
        try {
            client.postDocumentContainer(TestData.projects.type1, TestData.containers.projectid1, container1.getContainerOrContainerdoc());
        } catch (IOException ex) {
            Logger.getLogger(DocumentContainerTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    /*
     * Reset the test container tree to look like test container 2
     */
    private void resetToTestContainer2()
    {
        Container container2 = getTestContainer2();
        try {
            client.postDocumentContainer(TestData.projects.type1, TestData.containers.projectid1, container2.getContainerOrContainerdoc());
        } catch (IOException ex) {
            Logger.getLogger(DocumentContainerTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    /*
     * Erase the test container tree
     */
    private void deleteTestContainer()
    {
        try {
            client.deleteContainerTree(TestData.projects.type1, TestData.containers.projectid1);
        } catch (IOException ex) {
            Logger.getLogger(DocumentContainerTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @BeforeClass
    public static void populateDependencies()
    {
        // Create the required project
        try {
            ProjectClient projectClient = new ProjectClient();
            ArrayList<String> purposeids1 = new ArrayList<String>();
            purposeids1.add(TestData.projects.purposeid1);
            purposeids1.add(TestData.projects.purposeid2);
            purposeids1.add(TestData.projects.purposeid3);
            projectClient.postProject(TestData.containers.projectid1,
                TestData.projects.type1,
                TestData.projects.name1,
                TestData.projects.unitcode1,
                TestData.projects.description1,
                TestData.projects.lastupdate1,
                TestData.projects.commentreg1,
                TestData.projects.wwwlink1,
                TestData.projects.wwwsummary1,
                TestData.projects.wwwpub1,
                TestData.projects.analysistypeid1,
                TestData.projects.statusid1,
                TestData.projects.contactname1,
                TestData.projects.contactphone1,
                TestData.projects.contactemail1,
                purposeids1,
                TestData.projects.expirationdate1,
                TestData.projects.projectdocumentid1,
                TestData.projects.sopapub,
                TestData.projects.sopanew,
                TestData.projects.sopaheader,
                TestData.projects.esd,
                TestData.projects.cenodecision);

            makeProjectDocument(TestData.projects.type1, TestData.containers.projectid1, TestData.containers.docid1);
            makeProjectDocument(TestData.projects.type1, TestData.containers.projectid1, TestData.containers.docid2);
            makeProjectDocument(TestData.projects.type1, TestData.containers.projectid1, TestData.containers.docid3);
            makeProjectDocument(TestData.projects.type1, TestData.containers.projectid1, TestData.containers.docid4);
            makeProjectDocument(TestData.projects.type1, TestData.containers.projectid1, TestData.containers.docid5);

        } catch (IOException ex) {
            Logger.getLogger(DocumentContainerTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        client.deleteContainerTree(TestData.projects.type1, TestData.containers.projectid1);
    }


    @AfterClass
    public static void cleanupDependencies()
    {
       try {
            DocumentContainerClient containerClient = new DocumentContainerClient();
            ProjectClient projectClient = new ProjectClient();
            Project proj = projectClient.getProject(TestData.projects.type1, TestData.containers.projectid1);
            deleteProjectDocument(TestData.projects.type1, TestData.containers.projectid1, TestData.containers.docid1);
            deleteProjectDocument(TestData.projects.type1, TestData.containers.projectid1, TestData.containers.docid2);
            deleteProjectDocument(TestData.projects.type1, TestData.containers.projectid1, TestData.containers.docid3);
            deleteProjectDocument(TestData.projects.type1, TestData.containers.projectid1, TestData.containers.docid4);
            deleteProjectDocument(TestData.projects.type1, TestData.containers.projectid1, TestData.containers.docid5);
            containerClient.deleteContainerTree(TestData.projects.type1, TestData.containers.projectid1);
            projectClient.deleteProject(TestData.projects.type1, TestData.containers.projectid1);
       } catch (IOException ex) {
            Logger.getLogger(DocumentContainerTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
       }
    }


    /*
     * Container1 consists of:
     *
     * root/ (no order for root)
     *      doc1 (order 1)
     *      cont2/ (order 2)
     *              doc2 (order 2)
     *              doc3 (order 3)
     *              cont3/ (order 3)
     *                      doc4 (order 4)
     *                      doc5 (order 5)
     *
     * (This function creates a Container for the root, but in reality it usually
     * gets discarded because the proper root node for a container tree is a resource
     * of type "Containers")
     */
    private Container getTestContainer1()
    {
        Container root = new Container();
        Container cont2 = new Container();
        Container cont3 = new Container();

        Containerdoc doc1 = new Containerdoc();
        Containerdoc doc2 = new Containerdoc();
        Containerdoc doc3 = new Containerdoc();
        Containerdoc doc4 = new Containerdoc();
        Containerdoc doc5 = new Containerdoc();

        doc1.setDocid(TestData.containers.docid1);
        doc2.setDocid(TestData.containers.docid2);
        doc3.setDocid(TestData.containers.docid3);
        doc4.setDocid(TestData.containers.docid4);
        doc5.setDocid(TestData.containers.docid5);

        doc1.setOrder(TestData.containers.docorder1);
        doc2.setOrder(TestData.containers.docorder2);
        doc3.setOrder(TestData.containers.docorder3);
        doc4.setOrder(TestData.containers.docorder4);
        doc5.setOrder(TestData.containers.docorder5);

        // doc 5 is unpublished
        doc5.setPubflag(false);

        // The root doesn't need a label or an order
        // root.setLabel(TestData.containers.contlabel1);
        // root.setOrder(TestData.containers.contorder1);
        root.getContainerOrContainerdoc().add(doc1);

        cont2.setLabel(TestData.containers.contlabel2);
        cont2.setOrder(TestData.containers.contorder2);
        cont2.getContainerOrContainerdoc().add(doc2);
        cont2.getContainerOrContainerdoc().add(doc3);

        cont3.setLabel(TestData.containers.contlabel3);
        cont3.setOrder(TestData.containers.contorder3);
        cont3.getContainerOrContainerdoc().add(doc4);
        cont3.getContainerOrContainerdoc().add(doc5);

        cont2.getContainerOrContainerdoc().add(cont3);
        root.getContainerOrContainerdoc().add(cont2);

        return root;
    }

    /*
     * Container2 consists of:
     *
     * root/ (no order for root)
     *      doc2 (order 2)
     *      cont3/ (order 3)
     *              cont2/ (order 2)
     *                      doc1 (order 1)
     *                      doc3 (order 3)
     *              doc4 (order 4)
     *              doc5 (order 5)
     *
     * This container assigns unique "contid" values to every container.
     * Going forward, this will be required for every project in order
     * to use the new Container APIs.
     */
    private Container getTestContainer2()
    {
        return buildTestContainer2(false);
    }

    private Container buildTestContainer2(Boolean containersonly)
    {
        Container root = new Container();
        Container cont2 = new Container();
        Container cont3 = new Container();

        root.setContid(TestData.containers.contid1);
        cont2.setContid(TestData.containers.contid2);
        cont3.setContid(TestData.containers.contid3);

        cont2.setLabel(TestData.containers.contlabel2);
        cont2.setOrder(TestData.containers.contorder2);
        
        cont3.setLabel(TestData.containers.contlabel3);
        cont3.setOrder(TestData.containers.contorder3);

        cont3.getContainerOrContainerdoc().add(cont2);
        root.getContainerOrContainerdoc().add(cont3);

        if (containersonly == false) {
            Containerdoc doc1 = new Containerdoc();
            Containerdoc doc2 = new Containerdoc();
            Containerdoc doc3 = new Containerdoc();
            Containerdoc doc4 = new Containerdoc();
            Containerdoc doc5 = new Containerdoc();

            doc1.setDocid(TestData.containers.docid1);
            doc2.setDocid(TestData.containers.docid2);
            doc3.setDocid(TestData.containers.docid3);
            doc4.setDocid(TestData.containers.docid4);
            doc5.setDocid(TestData.containers.docid5);

            doc1.setOrder(TestData.containers.docorder1);
            doc2.setOrder(TestData.containers.docorder2);
            doc3.setOrder(TestData.containers.docorder3);
            doc4.setOrder(TestData.containers.docorder4);
            doc5.setOrder(TestData.containers.docorder5);

            // doc 5 is unpublished
            doc5.setPubflag(false);

            root.getContainerOrContainerdoc().add(doc2);

            cont2.getContainerOrContainerdoc().add(doc1);
            cont2.getContainerOrContainerdoc().add(doc3);

            cont3.getContainerOrContainerdoc().add(doc4);
            cont3.getContainerOrContainerdoc().add(doc5);
        }

        return root;
    }

    /*
     * Create project document for testing
     */
    private static void makeProjectDocument(String projectType, String projectId, String docId)
            throws IOException
    {
        ProjectDocumentClient docClient = new ProjectDocumentClient();
        docClient.postProjectDocument(projectId, projectType, docId,
            TestData.projectdocuments.pubflag1, TestData.projectdocuments.wwwlink1, TestData.projectdocuments.docname1,
            TestData.projectdocuments.description1, TestData.projectdocuments.pdffilesize1, TestData.projectdocuments.sensitiveflag1);
    }

    /*
     * Remove project document from test environment (for cleanup)
     */
    private static void deleteProjectDocument(String projectType, String projectId, String docId)
            throws IOException
    {
        ProjectDocumentClient docClient = new ProjectDocumentClient();
        try {
        Projectdocument pdoc = docClient.getProjectDocument(projectType, projectId, docId);
        docClient.deleteProjectDocument(projectType, projectId, docId);
        }
        catch (IOException e) {
            if (docClient.getStatus() == 404) {
                System.out.println("Document not found, ignoring");
            } else {
                throw e;
            }
        }
    }

    /*
     * Define Comparator for sorting Container/Containerdoc entities
     *
     * Sorting rules:
     *
     * 1.  Order by "order" property
     * 2.  When order values are equal, sort Documents before Containers (sort by type)
     * 3.  When all else is equal, sort documents by docid, containers by label ( sort by name)
     */
    private class NodeComparator implements Comparator<Object>
    {
        private int getOrder(Object o)
        {
            if (o.getClass() == Container.class) {
                return ((Container)o).getOrder();
            } else if (o.getClass() == Containerdoc.class) {
                return ((Containerdoc)o).getOrder();
            } else {
                return 0;   // The object is of an improper type anyway
            }
        }
        private int getType(Object o)
        {
            if (o.getClass() == Container.class) {
                return 1;
            } else if (o.getClass() == Containerdoc.class) {
                return -1;
            } else {
                return 0;   // The object is of an improper type anyway
            }
        }
        private String getName(Object o)
        {
            if (o.getClass() == Container.class) {
                return ((Container)o).getLabel();
            } else if (o.getClass() == Containerdoc.class) {
                return ((Containerdoc)o).getDocid();
            } else {
                return "";  // The object is of an improper type anyway
            }
        }

        public int compare(Object o1, Object o2) {
            int o1_order = getOrder(o1);
            int o2_order = getOrder(o2);
            int o1_type = getType(o1);
            int o2_type = getType(o2);
            String o1_name = getName(o1);
            String o2_name = getName(o2);

            if (o1_order > o2_order) { return 1; }
            if (o2_order > o1_order) { return -1; }
            if (o1_type > o2_type) { return 1; }
            if (o2_type > o1_type) { return -1; }
            return (o1_name.compareTo(o2_name));
        }
    }

    private String serializeDoc(Containerdoc doc)
    {
        return "{DOC:" + doc.getDocid() + ":" + doc.getOrder() + "}";
    }

    /*
     * This attempts to canonicalize the contents of a document container tree resource
     * so that each container has the format
     *      {CONTAINER:[label]:[order]contents}
     * where "contents" consists of a list of the contained containers and documents
     *
     * A document has the simple format
     *      {DOC:[docid]:[order]}
     *
     * The contents list is sorted by order, then label.  There shouldn't be multiple
     * test items with the same label and order as this will screw up testing, but
     * there is nothing officially preventing this in live data.
     */
    private String serializeContainer(Container container)
    {
        NodeComparator compare = new NodeComparator();

        String containerDef = container.getLabel() + ":" + container.getOrder();
        String containerContents = "";
        ArrayList<java.io.Serializable> contents = (ArrayList<java.io.Serializable>)container.getContainerOrContainerdoc();
        Collections.sort(contents, compare);
        Iterator i = contents.iterator();
        while (i.hasNext()) {
            Object o = i.next();
            if (o.getClass() == Container.class) {
                containerContents += serializeContainer((Container)o);
            } else if (o.getClass() == Containerdoc.class) {
                containerContents += serializeDoc((Containerdoc)o);
            } else {
                return "BADCLASS " + o.getClass().toString();
            }
        }

        return "{CONTAINER:" + containerDef + containerContents + "}";
    }

    private String serializeContainers(Containers root) {
        Container containerRoot = new Container();
        containerRoot.setLabel("ROOT");
        containerRoot.getContainerOrContainerdoc().addAll(root.getContainerOrContainerdoc());
        return serializeContainer(containerRoot);
    }
}
