package client;

import java.io.IOException;
import java.util.Date;
import us.fed.fs.www.nepa.schema.decisionmaker.Decisionmaker;
import us.fed.fs.www.nepa.schema.decisionmaker.Decisionmakers;
import us.fed.fs.www.nepa.schema.decisionmaker.ObjectFactory;


public class DecisionMakerClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public DecisionMakerClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.decisionmaker", "DecisionMaker.xsd");
    }


    /*
     * Note signedDate must be in format YYYY-MM-DD
     */
    public void postDecisionMaker(Integer id, int decisionId, String name, String signedDate, String title)
            throws IOException
    {
        Decisionmaker decisionMaker = new Decisionmaker();
        decisionMaker.setId(id);
        decisionMaker.setDecisionid(decisionId);
        decisionMaker.setName(name);
        decisionMaker.setSigneddate(dateStrToXML(signedDate));
        decisionMaker.setTitle(title);
        
        postDecisionMaker(decisionMaker);
    }


    public void postDecisionMaker(Decisionmaker decisionMaker)
            throws IOException
    {
        post(objectfactory.createDecisionmaker(decisionMaker), "/decisions/" + decisionMaker.getDecisionid() + "/decisionmakers");
    }


    public Decisionmaker getDecisionMaker(int decisionid, int decisionMakerId)
            throws IOException
    {
        return get("/decisions/" + decisionid+"/decisionmakers/" + decisionMakerId, Decisionmaker.class);
    }

/* These list APIs don't exist yet
    public DecisionMakers getDecisionMakersByProject(String projecttype, String projectid)
            throws IOException
    {
        return get("/decisionMakers/project/"+projecttype+"/"+projectid, DecisionMakers.class);
    }
    
    public DecisionMakers getDecisionMakersByUnit(String unitid)
            throws IOException
    {
        return get("/decisionMakers/unit/"+unitid, DecisionMakers.class);
    }
*/
    
    
    //TODO: Implement this
    public Decisionmakers getDecisionMakerList(int decisionid)
            throws IOException
    {
        return get("/decisions/" + decisionid + "/decisionmakers", Decisionmakers.class);
    }
    
    /*
     * Note signedDate must be in format YYYY-MM-DD
     */
    public void putDecisionMaker(Integer id, int decisionId, String name, String signedDate, String title)
            throws IOException
    {
        Decisionmaker decisionMaker = new Decisionmaker();
        decisionMaker.setId(id);
        decisionMaker.setDecisionid(decisionId);
        decisionMaker.setName(name);
        decisionMaker.setSigneddate(dateStrToXML(signedDate));
        decisionMaker.setTitle(title);
        putDecisionMaker(decisionMaker);
    }


    public void putDecisionMaker(Decisionmaker decisionMaker)
            throws IOException
    {
        put(objectfactory.createDecisionmaker(decisionMaker), "/decisions/" + decisionMaker.getDecisionid() + "/decisionmakers/" + decisionMaker.getId());
    }


    public void deleteDecisionMaker(int decisionid, int decisionMakerId)
            throws IOException
    {
        delete("/decisions/" + decisionid + "/decisionmakers/" + decisionMakerId);
    }

}
