package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.CARAProjectManagerRemote;
import us.fed.fs.www.nepa.schema.caraproject.Caraproject;
import us.fed.fs.www.nepa.schema.caraproject.Caraprojects;
import us.fed.fs.www.nepa.schema.caraproject.ObjectFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;


@Path ("/caraprojects")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class CaraProjectResource extends AbstractResource
{
    private CARAProjectManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public CaraProjectResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "CaraProject.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.caraproject");
        MY_PROPS.setProperty("bean", "CARAProjectManagerRemote");
        try {
            beanmanager = (CARAProjectManagerRemote) init(CARAProjectManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("CaraProjectResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("CaraProjectResource failed to start. Could not ping the bean manager!");
    }


    @GET
    @Path("/{id}")
    public Response get(@PathParam("id") int id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            Caraproject project = beanmanager.get(id);
            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Caraproject> jaxbelement = objectfactory.createCaraproject(project);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @GET
    public Response getAll(@Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            Caraprojects projects = beanmanager.getAll();
            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Caraprojects> jaxbelement = objectfactory.createCaraprojects(projects);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }

    @PUT
    @Path("/{id}")
    public Response put(@PathParam("id") String id, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        Caraproject cp = null;
        try {
            cp = (Caraproject) unmarshal(inputxml, Caraproject.class);

            if (!id.equalsIgnoreCase(Integer.toString(cp.getCaraid())))
                complainLoudly(new Exception("Id in uri must match caraid in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.update(cp);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"/"+cp.getCaraid()+"</location>");
    }

}