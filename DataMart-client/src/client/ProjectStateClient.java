package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.projectlocation.*;


public class ProjectStateClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public ProjectStateClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.projectlocation", "ProjectLocation.xsd");
    }


    public States getProjectState(String projectid, String id)
            throws IOException
    {
        return get("/projects/nepa/" + projectid + "/locations/states/" + id, States.class);
    }


    public void putProjectState(String projectid, String id)
            throws IOException
    {
        States state = new States();
        state.getStateid().add(id);
        put(objectfactory.createStates(state), "/projects/nepa/" + projectid + "/locations/states/" + id);
    }


    public void deleteProjectState(String projectid, String id)
            throws IOException
    {
        delete("/projects/nepa/" + projectid + "/locations/states/" + id);
    }
}