ALTER TABLE Decisions ADD CONSTRAINT FkDecisions_DecisionType FOREIGN KEY (DecisionType) REFERENCES RefDecisionTypes(Id);

ALTER TABLE RefDecisionTypes ADD COLUMN PalsId int(11) NOT NULL;
ALTER TABLE RefDecisionTypes ADD CONSTRAINT UnRefDecisionTypes_PalsId UNIQUE(PalsId);
