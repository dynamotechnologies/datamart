/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sam
 */
@Entity
@Table(name = "CustomProjects")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CustomProjects.findAll", query = "SELECT c FROM CustomProjects c"),
    @NamedQuery(name = "CustomProjects.findById", query = "SELECT c FROM CustomProjects c WHERE c.id = :id"),
    @NamedQuery(name = "CustomProjects.findByName", query = "SELECT c FROM CustomProjects c WHERE c.name = :name"),
    @NamedQuery(name = "CustomProjects.findByType", query = "SELECT c FROM CustomProjects c WHERE c.type = :type"),
    @NamedQuery(name = "CustomProjects.findByUrl", query = "SELECT c FROM CustomProjects c WHERE c.url = :url"),
    @NamedQuery(name = "CustomProjects.findByIsPublished", query = "SELECT c FROM CustomProjects c WHERE c.isPublished = :isPublished"),
    @NamedQuery(name = "CustomProjects.findByArchiveDate", query = "SELECT c FROM CustomProjects c WHERE c.archiveDate = :archiveDate"),
    @NamedQuery(name = "CustomProjects.findByDescription", query = "SELECT c FROM CustomProjects c WHERE c.description = :description"),
    @NamedQuery(name = "CustomProjects.findByStatusId", query = "SELECT c FROM CustomProjects c WHERE c.statusId = :statusId"),
    @NamedQuery(name = "CustomProjects.findByUnitId", query = "SELECT c FROM CustomProjects c WHERE c.unitId = :unitId"),
    @NamedQuery(name = "CustomProjects.findByCreatedBy", query = "SELECT c FROM CustomProjects c WHERE c.createdBy = :createdBy"),
    @NamedQuery(name = "CustomProjects.findByCreatedDate", query = "SELECT c FROM CustomProjects c WHERE c.createdDate = :createdDate")})
public class CustomProjects implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "Name")
    private String name;
    @Column(name = "Type")
    private String type;
    @Column(name = "URL")
    private String url;
    @Column(name = "IsPublished")
    private Boolean isPublished;
    @Column(name = "ArchiveDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date archiveDate;
    @Column(name = "Description")
    private String description;
    @Column(name = "StatusId")
    private Integer statusId;
    @Column(name = "UnitId")
    private Integer unitId;
    @Column(name = "CreatedBy")
    private String createdBy;
    @Column(name = "CreatedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "customProjectId")
    private Collection<CustomProjectPurposes> customProjectPurposesCollection;

    public CustomProjects() {
    }

    public CustomProjects(Integer id) {
        this.id = id;
    }

    public CustomProjects(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean getIsPublished() {
        return isPublished;
    }

    public void setIsPublished(Boolean isPublished) {
        this.isPublished = isPublished;
    }

    public Date getArchiveDate() {
        return archiveDate;
    }

    public void setArchiveDate(Date archiveDate) {
        this.archiveDate = archiveDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public Integer getUnitId() {
        return unitId;
    }

    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @XmlTransient
    public Collection<CustomProjectPurposes> getCustomProjectPurposesCollection() {
        return customProjectPurposesCollection;
    }

    public void setCustomProjectPurposesCollection(Collection<CustomProjectPurposes> customProjectPurposesCollection) {
        this.customProjectPurposesCollection = customProjectPurposesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CustomProjects)) {
            return false;
        }
        CustomProjects other = (CustomProjects) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CustomProjects[ id=" + id + " ]";
    }
    
}
