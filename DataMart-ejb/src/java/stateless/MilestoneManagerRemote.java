/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package stateless;
import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.milestone.*;
import beanutil.DataMartException;

/**
 *
 * @author mhsu
 */
@Remote
public interface MilestoneManagerRemote {
    public void create(Milestone resource) throws DataMartException;
    public String getName(String id) throws DataMartException;
    public void update(Milestone resource) throws DataMartException;
    public void delete(String id) throws DataMartException;
    public Milestones getAll() throws DataMartException;
    public Boolean ping();
}