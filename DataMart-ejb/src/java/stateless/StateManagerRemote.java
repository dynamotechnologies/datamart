/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package stateless;
import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.state.*;
import beanutil.DataMartException;

/**
 *
 * @author mhsu
 */
@Remote
public interface StateManagerRemote {
    public void create(State resource) throws DataMartException;
    public String getName(String id) throws DataMartException;
    public void update(State resource) throws DataMartException;
    public void delete(String id) throws DataMartException;
    public States getAll() throws DataMartException;
    public Boolean ping();
}