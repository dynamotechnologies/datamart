package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.commentregulation.*;


public class CommentRegulationClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public CommentRegulationClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.commentregulation", "CommentRegulation.xsd");
    }


    public void postCommentRegulation(String id, String name)
            throws IOException
    {
        Commentregulation commentregulation = new Commentregulation();
        commentregulation.setId(id);
        commentregulation.setName(name);
        postCommentRegulation(commentregulation);
    }


    public void postCommentRegulation(Commentregulation commentregulation)
            throws IOException
    {
        post(objectfactory.createCommentregulation(commentregulation), "/ref/commentregulations");
    }


    public Commentregulation getCommentRegulation(String id)
            throws IOException
    {
        return get("/ref/commentregulations/" + id, Commentregulation.class);
    }


    public Commentregulations getCommentRegulationList()
            throws IOException
    {
        return get("/ref/commentregulations", Commentregulations.class);
    }


    public void putCommentRegulation(String id, String name)
            throws IOException
    {
        Commentregulation commentregulation = new Commentregulation();
        commentregulation.setId(id);
        commentregulation.setName(name);
        putCommentRegulation(commentregulation);
    }


    public void putCommentRegulation(Commentregulation commentregulation)
            throws IOException
    {
        put(objectfactory.createCommentregulation(commentregulation), "/ref/commentregulations/" + commentregulation.getId());
    }


    public void deleteCommentRegulation(String id)
            throws IOException
    {
        delete("/ref/commentregulations/" + id);
    }
}