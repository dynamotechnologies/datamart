package resource;

import lombok.extern.slf4j.Slf4j;
import schema.DataMartSchemaGetter;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.JAXBIntrospector;
import javax.xml.namespace.QName;
import javax.xml.validation.Schema;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;


/*
 *
 * This is a singleton utility class. It should be loaded once --- when the application starts up.
 * This class contains generic objects and methods that are of general use to the application as a whole.
 *   It should not contain resource specific objects/methods/settings. Those are stored in the
 *   resource class itself.
 */
@Slf4j
public class DataMartUtil
{
    public static final String PRIMARY_MEDIA_TYPE = "application/xml";

    private HashMap<Integer, Config> resconfig = new HashMap<Integer, Config>();
    private static Properties warprops = null;

    // Prevent instantiation
    private DataMartUtil() {}

    public static synchronized DataMartUtil getInstance() {
        return DataMartUtilHolder.INSTANCE;
    }

    private static final class DataMartUtilHolder {
        private static final DataMartUtil INSTANCE = new DataMartUtil();
    }


    public Object getManager(int resourceId)
            throws IOException
    {
        return resconfig.get(resourceId).getManager();
    }


    public JAXBContext getContext(int resourceId)
            throws JAXBException
    {
        Config asdf = resconfig.get(resourceId);
        return asdf.getContext();
    }

        Schema getSchema(int resourceId)
        {
            return resconfig.get(resourceId).getSchema();
        }


    // Get the name of a jaxb element
    public QName getElementName(int resourceId, Object o)
    {
        return resconfig.get(resourceId).getElementName(o);
    }


    public static Properties getWarProps()
            throws IOException
    {
        if (warprops==null)
        {
            warprops = new Properties();
            InputStream propstream = DataMartUtil.class.getResourceAsStream("war.properties");
            warprops.load(propstream);
        }
        return warprops;
    }

    public int getResourceId(Properties props)
            throws IOException
    {
        // TODO: Property `bean` is unique at the moment, but it is possible there may be some circumstance in which
        //     it'd be advantageous to have multiple jersey resources accessing the same bean interface.
        //     This Properties object will need to contain an id which is unique to the resource.
        int key = props.getProperty("bean").hashCode();
        if (!resconfig.containsKey(key))
            resconfig.put(key, new Config(props));
        return key;
    }

    private class Config
    {
        String schemaFileName;
        String jaxbPackage;
        String beanName;
        Object manager;
        Schema schema;
        JAXBContext jaxbcontext;
        JAXBIntrospector jaxbintrospector;
        javax.naming.Context namingcontext;

        Config(Properties p)
                throws IOException
        {
            schemaFileName = p.getProperty("schema");
            jaxbPackage = p.getProperty("package");
            beanName = p.getProperty("bean");
            Properties jndiprops = new Properties();
            Properties warprops = DataMartUtil.getWarProps();
            jndiprops.put(Context.INITIAL_CONTEXT_FACTORY, warprops.getProperty("initial.context.factory"));
            jndiprops.put(Context.PROVIDER_URL, warprops.getProperty("jndi.provider.url"));
            schema = DataMartSchemaGetter.getSchema(schemaFileName);
            try {
                namingcontext = new javax.naming.InitialContext(jndiprops);
                String beanNameMinusRemote = beanName.replace("Remote", "");
                manager = namingcontext.lookup("ejb:DataMart/DataMart-ejb//" + beanNameMinusRemote + "!stateless." + beanName);
                jaxbcontext = JAXBContext.newInstance(jaxbPackage);
                jaxbintrospector = jaxbcontext.createJAXBIntrospector();
            } catch (NamingException ex) {
                log.error(ex.toString());
                java.io.StringWriter sw = new java.io.StringWriter();
                java.io.PrintWriter pw = new java.io.PrintWriter(sw);
                ex.printStackTrace(pw);
                throw new IOException("JNDI lookup failed.\n" + sw.toString());
            } catch (JAXBException ex) {
                log.error(ex.toString());
                throw new IOException("Could not create jaxb context.");
            }
        }

        String getSchemaFileName()
        {
            return schemaFileName;
        }

        Object getManager()
        {
            return manager;
        }

        JAXBContext getContext()
                throws JAXBException
        {
            return jaxbcontext;
        }

        QName getElementName(Object o)
        {
            return jaxbintrospector.getElementName(o);
        }

        Schema getSchema()
        {
            return schema;
        }
    }
 }
