/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author gauri
 */
@Entity
@Table(name = "ProjectImplInfo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProjectImplInfo.findAll", query = "SELECT p FROM ProjectImplInfo p"),
    @NamedQuery(name = "ProjectImplInfo.findByImplId", query = "SELECT p FROM ProjectImplInfo p WHERE p.implId = :implId"),
    @NamedQuery(name = "ProjectImplInfo.findByDescription", query = "SELECT p FROM ProjectImplInfo p WHERE p.description = :description"),
    @NamedQuery(name = "ProjectImplInfo.findByLastUpdatedBy", query = "SELECT p FROM ProjectImplInfo p WHERE p.lastUpdatedBy = :lastUpdatedBy"),
    @NamedQuery(name = "ProjectImplInfo.findByLastUpdatedDate", query = "SELECT p FROM ProjectImplInfo p WHERE p.lastUpdatedDate = :lastUpdatedDate")})
public class ProjectImplInfo implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "implInfoId")
    private Collection<ProjectImplInfoDocs> projectImplInfoDocsCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "implId")
    private Integer implId;
    @Size(max = 4000)
    @Column(name = "description")
    private String description;
    @Size(max = 255)
    @Column(name = "lastUpdatedBy")
    private String lastUpdatedBy;
    @Column(name = "lastUpdatedDate")
    @Temporal(TemporalType.DATE)
    private Date lastUpdatedDate;
    @JoinColumn(name = "projectid", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Projects projectid;

    public ProjectImplInfo() {
    }

    public ProjectImplInfo(Integer implId) {
        this.implId = implId;
    }

    public Integer getImplId() {
        return implId;
    }

    public void setImplId(Integer implId) {
        this.implId = implId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public Projects getProjectid() {
        return projectid;
    }

    public void setProjectid(Projects projectid) {
        this.projectid = projectid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (implId != null ? implId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProjectImplInfo)) {
            return false;
        }
        ProjectImplInfo other = (ProjectImplInfo) object;
        if ((this.implId == null && other.implId != null) || (this.implId != null && !this.implId.equals(other.implId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ProjectImplInfo[ implId=" + implId + " ]";
    }

    @XmlTransient
    public Collection<ProjectImplInfoDocs> getProjectImplInfoDocsCollection() {
        return projectImplInfoDocsCollection;
    }

    public void setProjectImplInfoDocsCollection(Collection<ProjectImplInfoDocs> projectImplInfoDocsCollection) {
        this.projectImplInfoDocsCollection = projectImplInfoDocsCollection;
    }
    
}
