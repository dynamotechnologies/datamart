package entity;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "ProjectSpecialAuthorities")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProjectSpecialAuthorities.findAll", query = "SELECT p FROM ProjectSpecialAuthorities p"),
    @NamedQuery(name = "ProjectSpecialAuthorities.findById", query = "SELECT p FROM ProjectSpecialAuthorities p WHERE p.id = :id")})
public class ProjectSpecialAuthorities implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @JoinColumn(name = "SpecialAuthorityId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private RefSpecialAuthorities refSpecialAuthorities;
    @JoinColumn(name = "ProjectId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Projects projects;

    public ProjectSpecialAuthorities() {
    }

    public ProjectSpecialAuthorities(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public RefSpecialAuthorities getRefSpecialAuthorities() {
        return refSpecialAuthorities;
    }

    public void setRefSpecialAuthorities(RefSpecialAuthorities refSpecialAuthorities) {
        this.refSpecialAuthorities = refSpecialAuthorities;
    }

    public Projects getProjects() {
        return projects;
    }

    public void setProjects(Projects projects) {
        this.projects = projects;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProjectSpecialAuthorities)) {
            return false;
        }
        ProjectSpecialAuthorities other = (ProjectSpecialAuthorities) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ProjectSpecialAuthorities[ id=" + id + " ]";
    }

}
