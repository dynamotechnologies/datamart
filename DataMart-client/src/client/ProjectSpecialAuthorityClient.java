package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.projectspecialauthority.*;


public class ProjectSpecialAuthorityClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public ProjectSpecialAuthorityClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.projectspecialauthority", "ProjectSpecialAuthority.xsd");
    }


    public Projectspecialauthority getProjectSpecialAuthority(String projectid, String id)
            throws IOException
    {
        return get("/projects/nepa/" + projectid + "/specialauthorities/" + id, Projectspecialauthority.class);
    }


    public Projectspecialauthorities getProjectSpecialAuthorityList(String projectid)
            throws IOException
    {
        return get("/projects/nepa/" + projectid + "/specialauthorities/", Projectspecialauthorities.class);
    }


    public void putProjectSpecialAuthority(String projectid, String id)
            throws IOException
    {
        Projectspecialauthority authority = new Projectspecialauthority();
        authority.setProjectid(projectid);
        authority.setSpecialauthorityid(id);
        put(objectfactory.createProjectspecialauthority(authority),
                "/projects/nepa/" + projectid + "/specialauthorities/" + id);
    }


    public void deleteProjectSpecialAuthority(String projectid, String id)
            throws IOException
    {
        delete("/projects/nepa/" + projectid + "/specialauthorities/" + id);
    }
}