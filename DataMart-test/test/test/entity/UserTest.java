package test.entity;

import client.UserClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.user.*;


public class UserTest {
    private UserClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new UserClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postUser(
                    TestData.users.shortname1,
                    TestData.users.firstname1,
                    TestData.users.lastname1,
                    TestData.users.phone1,
                    TestData.users.title1,
                    TestData.users.email1
                    );
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        User user = null;
        try {
            user = client.getUser(TestData.users.shortname1);
        } catch (IOException ex) {
            Logger.getLogger(UserTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.users.shortname1, user.getShortname());
        Assert.assertEquals(TestData.users.firstname1, user.getFirstname());
        Assert.assertEquals(TestData.users.lastname1, user.getLastname());
        Assert.assertEquals(TestData.users.phone1, user.getPhone());
        Assert.assertEquals(TestData.users.title1, user.getTitle());
        Assert.assertEquals(TestData.users.email1, user.getEmail());

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        try {
            client.putUser(
                    TestData.users.shortname1,
                    TestData.users.firstname1,
                    TestData.users.lastname1,
                    TestData.users.phone2,
                    TestData.users.title2,
                    TestData.users.email2
                    );
        } catch (IOException ex) {
            Logger.getLogger(UserTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        User user = null;
        try {
            user = client.getUser(TestData.users.shortname1);
        } catch (IOException ex) {
            Logger.getLogger(UserTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.users.shortname1, user.getShortname());
        Assert.assertEquals(TestData.users.firstname1, user.getFirstname());
        Assert.assertEquals(TestData.users.lastname1, user.getLastname());
        Assert.assertEquals(TestData.users.phone2, user.getPhone());
        Assert.assertEquals(TestData.users.title2, user.getTitle());
        Assert.assertEquals(TestData.users.email2, user.getEmail());

        delete();
    }

    @Test
    public void testFindUsers()
    {
        post();

        // Just make sure findUsers util function doesn't crash
        try {
            String shortname="";
            String firstname="";
            String lastname="Smith";
            client.findUsers(shortname, firstname, lastname);
        } catch (IOException ex) {
            Logger.getLogger(UserTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getUser(TestData.users.shortname1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteUser(TestData.users.shortname1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }

    private void post()
    {
        try {
            client.postUser(
                    TestData.users.shortname1,
                    TestData.users.firstname1,
                    TestData.users.lastname1,
                    TestData.users.phone1,
                    TestData.users.title1,
                    TestData.users.email1
                    // "shortname", "firstname", "lastname", "phone number", "title", "email@host.com"
                    );
        } catch (IOException ex) {
            Logger.getLogger(UserTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteUser(TestData.users.shortname1);
        } catch (IOException ex) {
            Logger.getLogger(UserTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteUser(TestData.users.shortname1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }

    
    @BeforeClass
    public static void populateDependencies()
    {
        // No dependencies
    }

    @AfterClass
    public static void cleanupDependencies()
    {
        // No cleanup
    }
}
