package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.objection.*;


public class ObjectionClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public ObjectionClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.objection", "Objection.xsd");
    }


    public void postObjection(String id, String projectid, String projecttype, String objector, String docid, String responsedate)
            throws IOException
    {
        Objection objection = new Objection();
        objection.setId(id);
        objection.setProjectid(projectid);
        objection.setProjecttype(projecttype);
        objection.setObjector(objector);
        objection.setDocid(docid);
        objection.setResponsedate(dateStrToXML(responsedate));
        postObjection(objection);
    }


    public void postObjection(Objection objection)
            throws IOException
    {
        post(objectfactory.createObjection(objection), "/objections");
    }


    public Objection getObjection(String id)
            throws IOException
    {
        return get("/objections/" + id, Objection.class);
    }


    public Objections getObjectionList()
            throws IOException
    {
        return get("/objections", Objections.class);
    }

    public Objections getObjectionsByUnit(String id)
            throws IOException
    {
        return get("/units/" + id + "/objections", Objections.class);
    }


    public void putObjection(String id, String projectid, String projecttype, String objector, String docid, String responsedate)
            throws IOException
    {
        Objection objection = new Objection();
        objection.setId(id);
        objection.setProjectid(projectid);
        objection.setProjecttype(projecttype);
        objection.setObjector(objector);
        objection.setDocid(docid);
        objection.setResponsedate(dateStrToXML(responsedate));
        putObjection(objection);
    }


    public void putObjection(Objection objection)
            throws IOException
    {
        put(objectfactory.createObjection(objection), "/objections/" + objection.getId());
    }


    public void deleteObjection(String id)
            throws IOException
    {
        delete("/objections/" + id);
    }
}