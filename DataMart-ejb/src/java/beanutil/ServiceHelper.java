/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beanutil;

/**
 *
 * @author gauri
 */
import entity.ProjectPurposes;
import entity.Projects;

import java.io.IOException;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Hashtable;
import java.util.Vector;


import org.apache.xmlrpc.CommonsXmlRpcTransportFactory;
import org.apache.xmlrpc.XmlRpcClient;
import org.apache.xmlrpc.XmlRpcException;


/**
 * @author
 * Created on Dec 26, 2007
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class ServiceHelper
{
   
    private static final int SERVICE_EXCEPTION_UNKNOWN_CODE = 999999999;
     private static final String GET_FILEMETADATA_SERVICE = "getFileMetadata";
   //    public static  String SERVICE_USER_NAME ="br0k3r";//= "p1cg";
//    public static  String SERVICE_USER_PASSWORD="s3rv1c3"; //= "p1cg";
//    public static  String SERVER_URL ="http://dmd-tst.ecosystem-management.org/drmtest/broker/broker.php"; //"http://svcbroker.fstst.phaseonecg.com/drmtest/broker/broker.php";
    //public static  String SERVICE_USER_NAME ="";//= "p1cg";
    //public static  String SERVICE_USER_PASSWORD=""; //= "p1cg";
    //public static  String SERVER_URL =""; //"http://svcbroker.fstst.phaseonecg.com/drmtest/broker/broker.php";

      public static  String         SERVICE_USER_NAME = "br0k3r";
      public static  String        SERVICE_USER_PASSWORD= "s3rv1c3";
      public static  String        SERVER_URL = "http://localhost/dmdpilot/broker/broker.php";

    private static final String SERVICE_PARAM_FILETYPE_KEY = "fileType";
    private static final String SERVICE_PARAM_FILETYPE_PDF = "PDF";
    private static final String KML_UPDATE_SERVICE = "updateKML";
    private static final String KML_DELETE_SERVICE = "deleteKML";
    private static final String ARCHIVE_UPDATE_SERVICE = "updateArchive";
    
    
   
   /**
   * get XmlRpcclient
   * @throws emcnt.utils.ServiceException
   * @return client
   */
    private static XmlRpcClient getClient() throws ServiceException
    {
      
        XmlRpcClient client = null;
        try
        {
            CommonsXmlRpcTransportFactory cFactory = new CommonsXmlRpcTransportFactory(new URL(SERVER_URL));
            cFactory.setConnectionTimeout(60000);
            cFactory.setTimeout(300000);
            cFactory.setBasicAuthentication(SERVICE_USER_NAME, SERVICE_USER_PASSWORD);
            client = new XmlRpcClient(new URL(SERVER_URL) ,cFactory);                
           
        }
        catch (MalformedURLException e)
        {   
            throw new ServiceException(SERVICE_EXCEPTION_UNKNOWN_CODE,"Error in XMLRPC client initialization");
        }

        return client;
    }

 
   

  
      /**
     * get the document from document service.
     * @throws emcnt.utils.ServiceException
     * @return resultBytes
     * @param isPDF
     * @param docID
     */
    public static Hashtable getFileMetaData(String docID, boolean isPDF)
            throws ServiceException
    {
        XmlRpcClient client = getClient();

        Vector params = new Vector();
        params.addElement(docID);

        //set the fileType to PDF if requested.
        if (isPDF)
        {
            Hashtable fileType = new Hashtable();
            fileType.put(SERVICE_PARAM_FILETYPE_KEY, SERVICE_PARAM_FILETYPE_PDF);
            params.addElement(fileType);
        }
        else
        {
            //Hashtable fileType = new Hashtable();
            //fileType.put(SERVICE_PARAM_FILETYPE_KEY, SERVICE_PARAM_FILETYPE_DOC);
            //params.addElement(fileType);
        }

        //execute the service
        Object result = null;
        try
        {
            result = client.execute(GET_FILEMETADATA_SERVICE, params);

        }
        catch (XmlRpcException e)
        {
           //return e.getMessage();
            throw new ServiceException(e.code, e.getMessage());
        }
        catch (IOException e)
        {
          // return e.getMessage();
            throw new ServiceException(SERVICE_EXCEPTION_UNKNOWN_CODE, "IOException");
        }

        if (result instanceof XmlRpcException)
        {
       
            XmlRpcException e = (XmlRpcException) result;
            // return e.getMessage();
            throw new ServiceException(e.code, e.getMessage());
        }
        else if (result instanceof Hashtable)
        {
            //This if for fault tolerance.Should not occur ideally.
            //throw new ServiceException(SERVICE_EXCEPTION_UNKNOWN_CODE, "UNKNOWN");
            Hashtable results = (Hashtable)result;
            //Object oFileSize = results.get("filesize");
            return results;
            //System.out.println(result);
        }
        return null;
    }
   
    
    public static Hashtable updateKML(Projects resource, String unitUrl) throws ServiceException {
        Vector params = new Vector();
        Hashtable struct = new Hashtable();
        params.add(resource.getPublicId());
        
        addMember(struct, "name", resource.getName());
        String defaultUrl = "https://www.fs.usda.gov/project/?project=" + resource.getPublicId();
        String url = resource.getWwwLink() != null && resource.getWwwLink() != "" ?
                resource.getWwwLink() : defaultUrl;
        struct.put("projecturl", url);
        if (resource.getRefStatuses() != null) {
            String status = resource.getRefStatuses().getName();
            if (status == null) {
                struct.put("status", "");
            } else {
                struct.put("status", status + " ");
            }
        } else {
            struct.put("status", "");
        }
        if (resource.getRefAnalysisTypes() != null) {
            addMember(struct, "analysistype", resource.getRefAnalysisTypes().getName());
        } else {
            struct.put("analysistype", "");
        }
        //Becuase Java 7 doesn't have String.join with anything except
        //CharSequences, just going to use a StringBuilder
        String purposesString = "";
        if (resource.getProjectPurposesCollection() != null) {
            StringBuilder sb = new StringBuilder();
            for (ProjectPurposes purpose : resource.getProjectPurposesCollection()) {
                sb.append(purpose.getRefPurposes().getName());
                sb.append("; ");
            }
            //remove the last "; "
            sb.delete(sb.length() - 2, sb.length());
            purposesString = sb.toString();
        }
        //purposesString will never be null
        struct.put("purposes", purposesString);
        if (resource.getLongitude() == null || resource.getLatitude() == null ||
             (resource.getLongitude() == 0. && resource.getLatitude() == 0.))
            return null;
        addMember(struct, "longitude", resource.getLongitude());
        addMember(struct, "latitude", resource.getLatitude());
        if (resource.getRefUnits() != null) {
            String unitId = resource.getRefUnits().getId();
            addMember(struct, "unitid", unitId);
        } else {
            struct.put("unitid", "");
        }
        struct.put("uniturl", unitUrl);
        params.add(struct);
        Object result;
        XmlRpcClient client = getClient();
        try {
           result = client.execute(KML_UPDATE_SERVICE, params);
           if (result instanceof Hashtable) {
               return (Hashtable) result;
           } else if (result instanceof XmlRpcException) {
               XmlRpcException e = (XmlRpcException) result;
               throw new ServiceException(e.code, e.getMessage());
           }
        } catch (XmlRpcException e) {
            throw new ServiceException(e.code, e.getMessage());
        } catch (IOException e) {
            throw new ServiceException(SERVICE_EXCEPTION_UNKNOWN_CODE, e.getMessage());
        }
        return null;
    }

    public static Hashtable deleteKML(String id, String unitId, Boolean... optionalBoolean) 
            throws ServiceException {
        Vector params = new Vector();
        params.add(id);
        params.add(unitId);
        //also accepts a boolean indicating whether it should just be removed
        //from unit KML file.
        //if no optional parameter passed or parameter is null, false, else,
        //value of passed parameter
        boolean unitOnly = optionalBoolean.length > 0 ? 
                              optionalBoolean[0] != null ? 
                                optionalBoolean[0] : false 
                              : false;
        params.add(unitOnly);
        Object result;
        XmlRpcClient client = getClient();
        try {
            result = client.execute(KML_DELETE_SERVICE, params);
            if (result instanceof Hashtable) {
                return (Hashtable) result;
            } else if (result instanceof XmlRpcException) {
                XmlRpcException e = (XmlRpcException) result;
                throw new ServiceException(e.code, e.getMessage());
            }
        } catch (XmlRpcException e) {
            throw new ServiceException(e.code, e.getMessage());
        } catch (IOException e) {
            throw new ServiceException(SERVICE_EXCEPTION_UNKNOWN_CODE, e.getMessage());
        }
        return null;
    }
    
    public static Hashtable updateArchive(Projects resource, 
            boolean putInArchive) throws ServiceException{
        
        Vector params = new Vector();
        Hashtable struct = new Hashtable();
        params.add(resource.getPublicId());
        if (resource.getRefUnits() != null) {
            addMember(struct, "unitid", resource.getRefUnits().getId());
        } else {
            struct.put("unitid", "");
        }
        //putInArchive == true: add or update the archive date in Service Broker
        //putInArchive == false: remove date from Service Broker
        if (putInArchive) {
            if (resource.getExpirationDate() != null) {
                String datePattern = "MM/dd/yyyy HH:mm:ss";
                DateFormat df = new SimpleDateFormat(datePattern);
                String date = df.format(resource.getExpirationDate());
                struct.put("date", date);
            } else {
                //putInArchive should have been false
                putInArchive = false;
            }
        } else {
            //we're removing it, so the date entry can be anything
            struct.put("date", "");
        }
        struct.put("addOrRemove", putInArchive);
        params.add(struct);
        Object result;
        XmlRpcClient client = getClient();
        try {
           result = client.execute(ARCHIVE_UPDATE_SERVICE, params);
           if (result instanceof Hashtable) {
               return (Hashtable) result;
           } else if (result instanceof XmlRpcException) {
               XmlRpcException e = (XmlRpcException) result;
               throw new ServiceException(e.code, e.getMessage());
           }
        } catch (XmlRpcException e) {
            throw new ServiceException(e.code, e.getMessage());
        } catch (IOException e) {
            throw new ServiceException(SERVICE_EXCEPTION_UNKNOWN_CODE, e.getMessage());
        }
        return null;
        
    }
    
    public static Hashtable echoMsg(String msg) throws ServiceException {
        Vector params = new Vector();
        params.add(msg);
        Object result;
        XmlRpcClient client = getClient();
        try {
            result = client.execute("echoMsg", params);
            if (result instanceof Hashtable) {
                return (Hashtable) result;
            } else if (result instanceof XmlRpcException) {
                XmlRpcException e = (XmlRpcException) result;
                throw new ServiceException(e.code, e.getMessage());
            }
        } catch (XmlRpcException e) {
            throw new ServiceException(e.code, e.getMessage());
        } catch (IOException ex) {
            throw new ServiceException(SERVICE_EXCEPTION_UNKNOWN_CODE, ex.getMessage());
        }
        return null;
    }
    
    private static void addMember(Hashtable struct, String name, Object value) {
        //Can't add null to Hashtable
        if (value == null) {
            struct.put(name, "");
        } else {
            struct.put(name, value.toString());
        }
    }
}

