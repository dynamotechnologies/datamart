package entity;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "DecisionAppealRules")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DecisionAppealRules.findAll", query = "SELECT d FROM DecisionAppealRules d"),
    @NamedQuery(name = "DecisionAppealRules.findById", query = "SELECT d FROM DecisionAppealRules d WHERE d.id = :id")})
public class DecisionAppealRules implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @JoinColumn(name = "DecisionId", referencedColumnName = "Id")
    @ManyToOne
    private Decisions decisions;
    @JoinColumn(name = "AppealRule", referencedColumnName = "Id")
    @ManyToOne
    private RefAppealRules refAppealRules;

    public DecisionAppealRules() {
    }

    public DecisionAppealRules(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Decisions getDecisions() {
        return decisions;
    }

    public void setDecisions(Decisions decisions) {
        this.decisions = decisions;
    }

    public RefAppealRules getRefAppealRules() {
        return refAppealRules;
    }

    public void setRefAppealRules(RefAppealRules refAppealRules) {
        this.refAppealRules = refAppealRules;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DecisionAppealRules)) {
            return false;
        }
        DecisionAppealRules other = (DecisionAppealRules) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DecisionAppealRules[ id=" + id + " ]";
    }

}
