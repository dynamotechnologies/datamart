/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author gauri
 */
@Entity
@Table(name = "ProjectMeetingNotices")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProjectMeetingNotices.findAll", query = "SELECT p FROM ProjectMeetingNotices p"),
    @NamedQuery(name = "ProjectMeetingNotices.findByMeetingId", query = "SELECT p FROM ProjectMeetingNotices p WHERE p.meetingId = :meetingId"),
    @NamedQuery(name = "ProjectMeetingNotices.findByMeetingTitle", query = "SELECT p FROM ProjectMeetingNotices p WHERE p.meetingTitle = :meetingTitle"),
    @NamedQuery(name = "ProjectMeetingNotices.findByMeetingDate", query = "SELECT p FROM ProjectMeetingNotices p WHERE p.meetingDate = :meetingDate"),
    @NamedQuery(name = "ProjectMeetingNotices.findByMeetingDescription", query = "SELECT p FROM ProjectMeetingNotices p WHERE p.meetingDescription = :meetingDescription"),
    @NamedQuery(name = "ProjectMeetingNotices.findByMeetingStartTime", query = "SELECT p FROM ProjectMeetingNotices p WHERE p.meetingStartTime = :meetingStartTime"),
    @NamedQuery(name = "ProjectMeetingNotices.findByMeetingEndTime", query = "SELECT p FROM ProjectMeetingNotices p WHERE p.meetingEndTime = :meetingEndTime"),
    @NamedQuery(name = "ProjectMeetingNotices.findByStreet1", query = "SELECT p FROM ProjectMeetingNotices p WHERE p.street1 = :street1"),
    @NamedQuery(name = "ProjectMeetingNotices.findByStreet2", query = "SELECT p FROM ProjectMeetingNotices p WHERE p.street2 = :street2"),
    @NamedQuery(name = "ProjectMeetingNotices.findByCity", query = "SELECT p FROM ProjectMeetingNotices p WHERE p.city = :city"),
    @NamedQuery(name = "ProjectMeetingNotices.findByState", query = "SELECT p FROM ProjectMeetingNotices p WHERE p.state = :state"),
    @NamedQuery(name = "ProjectMeetingNotices.findByZip", query = "SELECT p FROM ProjectMeetingNotices p WHERE p.zip = :zip"),
    @NamedQuery(name = "ProjectMeetingNotices.findByDirections", query = "SELECT p FROM ProjectMeetingNotices p WHERE p.directions = :directions"),
    @NamedQuery(name = "ProjectMeetingNotices.findByLastUpdatedBy", query = "SELECT p FROM ProjectMeetingNotices p WHERE p.lastUpdatedBy = :lastUpdatedBy"),
    @NamedQuery(name = "ProjectMeetingNotices.findByLastUpdatedDate", query = "SELECT p FROM ProjectMeetingNotices p WHERE p.lastUpdatedDate = :lastUpdatedDate"),
    @NamedQuery(name = "ProjectMeetingNotices.findByMeetingOrder", query = "SELECT p FROM ProjectMeetingNotices p WHERE p.meetingOrder = :meetingOrder"),
    @NamedQuery(name = "ProjectMeetingNotices.findByMeetingPubFlag", query = "SELECT p FROM ProjectMeetingNotices p WHERE p.meetingPubFlag = :meetingPubFlag")})
public class ProjectMeetingNotices implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "meetingId")
    private Integer meetingId;
    @Size(max = 100)
    @Column(name = "meetingTitle")
    private String meetingTitle;
    @Column(name = "meetingDate")
    @Temporal(TemporalType.DATE)
    private Date meetingDate;
    @Size(max = 4000)
    @Column(name = "meetingDescription")
    private String meetingDescription;
    @Size(max = 10)
    @Column(name = "meetingStartTime")
    private String meetingStartTime;
    @Size(max = 10)
    @Column(name = "meetingEndTime")
    private String meetingEndTime;
    @Size(max = 50)
    @Column(name = "street1")
    private String street1;
    @Size(max = 50)
    @Column(name = "street2")
    private String street2;
    @Size(max = 100)
    @Column(name = "city")
    private String city;
    @Size(max = 2)
    @Column(name = "state")
    private String state;
    @Size(max = 10)
    @Column(name = "zip")
    private String zip;
    @Size(max = 4000)
    @Column(name = "directions")
    private String directions;
    @Size(max = 50)
    @Column(name = "lastUpdatedBy")
    private String lastUpdatedBy;
    @Column(name = "lastUpdatedDate")
    @Temporal(TemporalType.DATE)
    private Date lastUpdatedDate;
    @Column(name = "meetingOrder")
    private Integer meetingOrder;
    @Size(max = 1)
    @Column(name = "meetingPubFlag")
    private String meetingPubFlag;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "meetingNoticeId")
    private Collection<ProjectMeetingNoticeDocs> projectMeetingNoticeDocsCollection;
    @JoinColumn(name = "projectId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Projects projectId;

    public ProjectMeetingNotices() {
    }

    public ProjectMeetingNotices(Integer meetingId) {
        this.meetingId = meetingId;
    }

    public Integer getMeetingId() {
        return meetingId;
    }

    public void setMeetingId(Integer meetingId) {
        this.meetingId = meetingId;
    }

    public String getMeetingTitle() {
        return meetingTitle;
    }

    public void setMeetingTitle(String meetingTitle) {
        this.meetingTitle = meetingTitle;
    }

    public Date getMeetingDate() {
        return meetingDate;
    }

    public void setMeetingDate(Date meetingDate) {
        this.meetingDate = meetingDate;
    }

    public String getMeetingDescription() {
        return meetingDescription;
    }

    public void setMeetingDescription(String meetingDescription) {
        this.meetingDescription = meetingDescription;
    }

    public String getMeetingStartTime() {
        return meetingStartTime;
    }

    public void setMeetingStartTime(String meetingStartTime) {
        this.meetingStartTime = meetingStartTime;
    }

    public String getMeetingEndTime() {
        return meetingEndTime;
    }

    public void setMeetingEndTime(String meetingEndTime) {
        this.meetingEndTime = meetingEndTime;
    }

    public String getStreet1() {
        return street1;
    }

    public void setStreet1(String street1) {
        this.street1 = street1;
    }

    public String getStreet2() {
        return street2;
    }

    public void setStreet2(String street2) {
        this.street2 = street2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getDirections() {
        return directions;
    }

    public void setDirections(String directions) {
        this.directions = directions;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public Integer getMeetingOrder() {
        return meetingOrder;
    }

    public void setMeetingOrder(Integer meetingOrder) {
        this.meetingOrder = meetingOrder;
    }

    public String getMeetingPubFlag() {
        return meetingPubFlag;
    }

    public void setMeetingPubFlag(String meetingPubFlag) {
        this.meetingPubFlag = meetingPubFlag;
    }

    @XmlTransient
    public Collection<ProjectMeetingNoticeDocs> getProjectMeetingNoticeDocsCollection() {
        return projectMeetingNoticeDocsCollection;
    }

    public void setProjectMeetingNoticeDocsCollection(Collection<ProjectMeetingNoticeDocs> projectMeetingNoticeDocsCollection) {
        this.projectMeetingNoticeDocsCollection = projectMeetingNoticeDocsCollection;
    }

    public Projects getProjectId() {
        return projectId;
    }

    public void setProjectId(Projects projectId) {
        this.projectId = projectId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (meetingId != null ? meetingId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProjectMeetingNotices)) {
            return false;
        }
        ProjectMeetingNotices other = (ProjectMeetingNotices) object;
        if ((this.meetingId == null && other.meetingId != null) || (this.meetingId != null && !this.meetingId.equals(other.meetingId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ProjectMeetingNotices[ meetingId=" + meetingId + " ]";
    }
    
}
