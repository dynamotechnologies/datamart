#!/usr/bin/python

import sys, os
import urllib
import urllib2
from urllib2 import HTTPError
fname = os.path.join(os.path.dirname(__file__), "/var/www/common/dmUtil")
sys.path.insert(0, fname)
import dmUtil

def usage():
	print """
	Usage: users-ingress.py [FILENAME]
	Usage: cat inputfile | users-ingress.py

	This program takes a line of vbar-delimited input and inserts
	the values into the Users table

	Format is: SHORT_NAME|FIRST_NAME|LAST_NAME|PHONE_NUMBER|TITLE|E_MAIL
	"""

#
#	Make sure input looks like the expected CSV file
#
def validateFile(inputstream):
	line1 = inputstream.readline().strip()
	if (line1 != 'QUERY RESULT|||||'):
		print 'Input file looks wrong, check 1st line'
		sys.exit(1)
	line2 = inputstream.readline().strip()
	if (line2 != 'LOTUS_SHORT_NAME|FIRST_NAME|LAST_NAME|PHONE_NUMBER|TITLE|E_MAIL'):
		print 'Input file looks wrong, check 2nd line'
		sys.exit(1)

#
#	Generate the XML for POST and PUT requests
#
def buildXML(short,first,last,phone,title,email):
	return '<?xml version="1.0" encoding="UTF-8"?><user xmlns="http://www.fs.fed.us/nepa/schema"><shortname>'+short+'</shortname><firstname>'+first+'</firstname><lastname>'+last+'</lastname><email>'+email+'</email><phone>'+phone+'</phone><title>'+title+'</title></user>'


"""
====
MAIN
====
"""

if (len(sys.argv) > 2):
	usage()
	sys.exit()

inputstream = None

if (len(sys.argv) > 1):
	fname = sys.argv[1]
	print "Getting input from file " + fname
	inputstream = open(fname, "r")
else:
	print "Getting input from stdin"
	inputstream = sys.stdin

validateFile(inputstream)
dmUtil.initOpener()

USERS_URL="http://localhost:7001/api/1_0/users"

for line in inputstream:
	line = line.strip()
	# print line
	[short, first, last, phone, title, email] = line.split('|')
	print "shortname = " + short
	print "firstname = " + first
	print "lastname = " + last
	print "phone number = " + phone
	print "title = " + title
	print "email = " + email
	xml = buildXML(short, first, last, phone, title, email)
	# print "POST output is: " + dmUtil.post(USERS_URL, xml)
	# print "PUT output is: " + dmUtil.put(USERS_URL + '/' + short, xml)
	print "MERGE output is: " + dmUtil.merge(USERS_URL, short, xml)

