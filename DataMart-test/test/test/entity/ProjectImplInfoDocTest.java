/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.entity;

import client.ProjectImplInfoDocsClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.ProjectImplInfoDoc.ProjectImplInfoDocs;

/**
 *
 * @author gauri
 */

public class ProjectImplInfoDocTest {
    private ProjectImplInfoDocsClient client;
    

    @Before
    public void setUp()
            throws IOException
    {
        client = new ProjectImplInfoDocsClient();
       
    }

    @Test
    public void testPost()
    {
        // This POST should fail, because it violates a constraint. The unit code is bad
        try {
           addImplInfoDocs();
            // If the above statement did not throw an exception then something has gone terribly wrong
            //Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.BAD_PARENTS)
                Assert.fail();
        } catch (Exception ex) {
            Logger.getLogger(ProjectImplInfoDocTest.class.getName()).log(Level.SEVERE, null, ex);
             Assert.fail();
        }

        

        
    }

    @Test
    public void testGet()
    {
     

        ProjectImplInfoDocs project = null;
         
        try {
            project = client.getImplInfoDocs("40090");
        } catch (IOException ex) {
            Logger.getLogger(ProjectImplInfoDocTest.class.getName()).log(Level.SEVERE, null, ex);
             Assert.fail();
        }
            
          
       

       
     

       
    }

    @Test
    public void testPut()
    {
        

        ProjectImplInfoDocs project = null;
        try {
            modifyImplInfoDocs();
            project = client.getImplInfoDocs("40090");
        } catch (Exception ex) {
            Logger.getLogger(ProjectImplInfoDocTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

       
    }

   
    
 


    @Test
    public void testDelete()
    {
        

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            deleteImplInfoDocs();
          
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        } catch (Exception ex) {
            Logger.getLogger(ProjectImplInfoDocTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
      
    }

 
    @After
    public void cleanFailed()
            throws Exception
    {
      
    }


    @BeforeClass
    public static void populateDependencies()
    {
        // No dependencies
    }

    @AfterClass
    public static void cleanupDependencies()
    {
        // No cleanup
    }
    public static void addImplInfoDocs() throws Exception{
	    	
	    	client.ProjectImplInfoDocsClient client = new client.ProjectImplInfoDocsClient();
	    	us.fed.fs.www.nepa.schema.ProjectImplInfoDoc.ProjectImplInfoDoc p = new us.fed.fs.www.nepa.schema.ProjectImplInfoDoc.ProjectImplInfoDoc();
	    	p.setImplInfoId("90");
                p.setDocumentId("FSPLT2_287333");
                p.setOrder(3);
               	client.postImplInfoDocs("40090",p);
	    }
    
	 public static void deleteImplInfoDocs() throws Exception{
	    	
	    	client.ProjectImplInfoDocsClient client = new client.ProjectImplInfoDocsClient();
                client.deleteImplInfoDocs("40090","3");
         }   
         
          public static void modifyImplInfoDocs() throws Exception{
	    	client.ProjectImplInfoDocsClient client = new client.ProjectImplInfoDocsClient();
  	  	us.fed.fs.www.nepa.schema.ProjectImplInfoDoc.ProjectImplInfoDoc p = new us.fed.fs.www.nepa.schema.ProjectImplInfoDoc.ProjectImplInfoDoc();
	        p.setImplInfoDocId("91");
                p.setImplInfoId("90");
                p.setDocumentId("FSPLT2_287333");
                p.setOrder(5);
	    	client.putImplInfoDocs("40090",p);
         }   
}
