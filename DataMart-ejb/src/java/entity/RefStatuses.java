package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "RefStatuses")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RefStatuses.findAll", query = "SELECT r FROM RefStatuses r"),
    @NamedQuery(name = "RefStatuses.findById", query = "SELECT r FROM RefStatuses r WHERE r.id = :id"),
    @NamedQuery(name = "RefStatuses.findByName", query = "SELECT r FROM RefStatuses r WHERE r.name = :name")})
public class RefStatuses implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "Name")
    private String name;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refStatuses")
    private Collection<Projects> projectsCollection;

    public RefStatuses() {
    }

    public RefStatuses(Integer id) {
        this.id = id;
    }

    public RefStatuses(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Collection<Projects> getProjectsCollection() {
        return projectsCollection;
    }

    public void setProjectsCollection(Collection<Projects> projectsCollection) {
        this.projectsCollection = projectsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RefStatuses)) {
            return false;
        }
        RefStatuses other = (RefStatuses) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RefStatuses[ id=" + id + " ]";
    }

}
