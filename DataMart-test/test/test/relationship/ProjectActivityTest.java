package test.relationship;

import client.ActivityClient;
import client.ProjectActivityClient;
import client.ProjectClient;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.projectactivity.*;


public class ProjectActivityTest {
    private ProjectActivityClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new ProjectActivityClient();
    }

    @Test
    public void testPut()
    {
        put();
        
        try {
            // Get a list of all entities and send them back. This should delete & recreate them, leaving the data unchanged.
            client.replaceProjectActivityList(client.getProjectActivityList(TestData.projects.id1));

            // second PUT should overwrite the first
            client.putProjectActivity(TestData.projects.id1, TestData.activities.id1);
        } catch (Exception ex) {
            Logger.getLogger(ProjectActivityTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        put();

        Projectactivity activity = null;
        Projectactivities activities = null;
        try {
            activity = client.getProjectActivity(TestData.projects.id1, TestData.activities.id1);
            activities = client.getProjectActivityList(TestData.projects.id1);
        } catch (Exception ex) {
            Logger.getLogger(ProjectActivityTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // make sure the activity we posted is present in the GET response
        Boolean activityWePostedIsPresentInTheGETResponse = false;
        Iterator it = activities.getProjectactivity().iterator();
        while (it.hasNext()) {
            Projectactivity act = (Projectactivity) it.next();
            if (act.getActivityid().equalsIgnoreCase(TestData.activities.id1)) {
                activityWePostedIsPresentInTheGETResponse = true;
            }
        }
        Assert.assertTrue(activityWePostedIsPresentInTheGETResponse);
        Assert.assertEquals(TestData.projects.id1, activity.getProjectid());
        Assert.assertEquals(TestData.activities.id1, activity.getActivityid());

        delete();
    }

    @Test
    public void testDelete()
    {
        put();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getProjectActivityList(TestData.projects.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteProjectActivity(TestData.projects.id1, TestData.activities.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }


    private void put()
    {
        try {
            client.putProjectActivity(TestData.projects.id1, TestData.activities.id1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectActivityTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteProjectActivity(TestData.projects.id1, TestData.activities.id1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectActivityTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteProjectActivity(TestData.projects.id1, TestData.activities.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }

    
    @BeforeClass
    public static void  populateDependencies()
    {
        ActivityClient refactivityclient = null;
        ProjectClient projclient = null;
        try {
            refactivityclient = new ActivityClient();
            projclient = new ProjectClient();
        } catch (Exception ex) {
            Logger.getLogger(ProjectActivityTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Project Activities rely on Reference Activities. Pre-populate a Reference Activity
        try {
            refactivityclient.postActivity(TestData.activities.id1, TestData.activities.name1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectActivityTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Project Activities rely on Projects. Pre-populate a Project.
        ArrayList<String> purposeids1 = new ArrayList<String>();
        purposeids1.add(TestData.projects.purposeid1);
        purposeids1.add(TestData.projects.purposeid2);
        purposeids1.add(TestData.projects.purposeid3);
        try {
            projclient.postProject(TestData.projects.id1, TestData.projects.type1,
                    TestData.projects.name1, TestData.projects.unitcode1, TestData.projects.description1,
                    TestData.projects.lastupdate1, TestData.projects.commentreg1, TestData.projects.wwwlink1,
                    TestData.projects.wwwsummary1, TestData.projects.wwwpub1, TestData.projects.analysistypeid1,
                    TestData.projects.statusid1, TestData.projects.contactname1, TestData.projects.contactphone1,
                    TestData.projects.contactemail1, purposeids1, TestData.projects.expirationdate1, TestData.projects.projectdocumentid1,
                    TestData.projects.sopapub, TestData.projects.sopanew, TestData.projects.sopaheader, TestData.projects.esd, TestData.projects.cenodecision);
        } catch (IOException ex) {
            Logger.getLogger(ProjectActivityTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @AfterClass
    public static void  cleanupDependencies()
    {
        ActivityClient refactivityclient = null;
        ProjectClient projclient = null;
        try {
            refactivityclient = new ActivityClient();
            projclient = new ProjectClient();
        } catch (Exception ex) {
            Assert.fail();
        }

        // Clean up the Project
        try {
            projclient.deleteProject("nepa", TestData.projects.id1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectActivityTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Clean up the Reference Activity
        try {
            refactivityclient.deleteActivity(TestData.activities.id1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectActivityTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }
}