package test.reference;

import client.StatusClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.status.*;


public class StatusTest {

    private StatusClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new StatusClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postStatus(TestData.statuses.id1, TestData.statuses.name2);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Status status = null;
        Statuses statuslist = null;
        try {
            status = client.getStatus(TestData.statuses.id1);
            statuslist = client.getStatusList();
        } catch (IOException ex) {
            Logger.getLogger(StatusTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.statuses.id1, status.getId());
        Assert.assertEquals(TestData.statuses.name1, status.getName());
        Assert.assertTrue(statuslist.getStatus().get(0).getName().length()>0);
        Assert.assertTrue(statuslist.getStatus().get(0).getId().length()>0);

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        Status status = null;
        try {
            client.putStatus(TestData.statuses.id1, TestData.statuses.name2);
            status = client.getStatus(TestData.statuses.id1);
        } catch (Exception ex) {
            Logger.getLogger(StatusTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.statuses.name2, status.getName());

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getStatus(TestData.statuses.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteStatus(TestData.statuses.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }

    private void post()
    {
        try {
            client.postStatus(TestData.statuses.id1, TestData.statuses.name1);
        } catch (IOException ex) {
            Logger.getLogger(StatusTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteStatus(TestData.statuses.id1);
        } catch (IOException ex) {
            Logger.getLogger(StatusTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteStatus(TestData.statuses.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }
}