`keytool` means {JDK_DIR}/bin/keytool



Create the Java Key Store
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
keytool -genkey -alias datamart -keyalg RSA -keystore data.ecosystem-management.org.jks -keysize 2048
Enter keystore password:  p1cgorcl
What is your first and last name?
  [Unknown]:  data.ecosystem-management.org
What is the name of your organizational unit?
  [Unknown]:  FS EMC eMNEPA
What is the name of your organization?
  [Unknown]:  FS Ecosystem Management
What is the name of your City or Locality?
  [Unknown]:  Alexandria
What is the name of your State or Province?
  [Unknown]:  VA
What is the two-letter country code for this unit?
  [Unknown]:  US
Is CN=data.ecosystem-management.org, OU=FS EMC eMNEPA, O=FS Ecosystem Management, L=Alexandria, ST=VA, C=US correct?
  [no]:  yes
Enter key password for <datamart>
	(RETURN if same as keystore password):  


keytool -genkey -alias datamart -keyalg RSA -keystore dmd-tst.ecosystem-management.org.jks -keysize 2048
<same settings but with dmd-tst as hostname>



Export the Certificate
~~~~~~~~~~~~~~~~~~~~~~~~~~~
keytool -export -alias datamart -keystore data.ecosystem-management.org.jks -file data.ecosystem-management.org.cer
keytool -export -alias datamart -keystore dmd-tst.ecosystem-management.org.jks -file dmd-tst.ecosystem-management.org.cer



Create a Certificate Signing Request
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
keytool -certreq -alias datamart -keystore data.ecosystem-management.org.jks -file data.ecosystem-management.org.csr
keytool -certreq -alias datamart -keystore dmd-tst.ecosystem-management.org.jks -file dmd-tst.ecosystem-management.org.csr


Import the Certificate Authority's public key chain
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
keytool -import -file signed_certs_from_godaddy/data.ecosystem-management.org/gd_bundle.crt -keystore data.ecosystem-management.org.jks -storepass p1cgorcl -alias ca
keytool -import -file signed_certs_from_godaddy/dmd-tst.ecosystem-management.org/gd_bundle.crt -keystore dmd-tst.ecosystem-management.org.jks -storepass p1cgorcl -alias ca


Remove/Delete the Certificate Authority's old/expired public key chain
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
keytool -delete -alias ca -keystore data.ecosystem-management.org.jks
keytool -delete -alias ca -keystore dmd-tst.ecosystem-management.org.jks


Import a (signed) certificate into a JKS keystore
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
keytool -import -v -trustcacerts -alias datamart -file signed_certs_from_godaddy/data.ecosystem-management.org/data.ecosystem-management.org.crt -keystore data.ecosystem-management.org.jks -keypass p1cgorcl -storepass p1cgorcl
keytool -import -v -trustcacerts -alias datamart -file signed_certs_from_godaddy/dmd-tst.ecosystem-management.org/dmd-tst.ecosystem-management.org.crt -keystore dmd-tst.ecosystem-management.org.jks -keypass p1cgorcl -storepass p1cgorcl


Installing new JKS in Weblogic
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
1) Get CRT files from tech support and put them in the signed_certs_from_godaddy directory. Also put GoDaddy's certificate chain in there (gd_bundle.crt). svn diff might show an exact match for gd_bundle.crt, in which case you don't need to do anything with it.
2) Follow instructions above to import the signed certificate into the JKS
3) Find the server's keystore directory. Do this by logging into the Weblogic admin console and then clicking:
Environment->Servers->{AdminServer|web}->Keystores->Custom Identity Keystore
    NOTE: if you just installed WebLogic, change 'Demo Identity and Demo Trust' to 'Custom Identity and Custom Trust'.
Currently the settings look like this:
CoreTst1: /oracle/weblogic/dmd-tst.ecosystem-management.org.jks
SvcBkrProd1: /app/Oracle/Middleware/data.ecosystem-management.org.jks
4) stop weblogic
5) Rename the old JKS file
6) Copy the new JKS over to the Weblogic server, and put it in the Custom Identity Keystore directory
7) restart weblogic; check log file for ssl errors; check "certificate information" in browser and verify expiration date

If this is the first time installing a Datamart ssl key in weblogic, note the above about changing the Keystore type. Then go to:
Environment->Servers->{AdminServer|web}->SSL
Private Key Alias: datamart
Private Key Passphrase: p1cgorcl

Then go to:
Configuration->General
Check 'SSL Listen Port Enabled'
SSL Listen Port: 443
