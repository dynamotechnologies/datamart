#!/usr/bin/python

import sys
import os
import datetime
import legacyconf


def usage():
    print "\nUsage:"
    print "python legacy-ingress.py &\n\nNotes:"
    print "- This program is run by cron, and ingresses legacy xml into the datamart"
    print "- It should be placed in the same directory as the compiled Ingress utility"
    print "- Legacy xml files should be placed here: " + legacyconf.projectfiles_dir
    print "- The non-215 appeals file should be here: " + legacyconf.appeals_file
    print "- The objections file should be here: " + legacyconf.objections_file
    print "- Error output can be found here: " + legacyconf.error_file
    print "- When complete, the index of the most recently ingressed project file will be saved to: " + legacyconf.index_file
    print ""

def ingressFile(f):
    cmd = legacyconf.java_home+"/bin/java " + legacyconf.java_options + " -cp .:"+legacyconf.work_dir+":DataMart-client.jar:asm-3.1.jar:jersey-bundle-1.4.jar:jsr311-api-1.1.1.jar ingress.Legacy " + f + " >> " + legacyconf.error_file + " 2>&1"
    os.system(cmd)
    ifScaredRunAway()
    brag(f)

def getNewFileList(dir, index):
    L=[]
    for subdir, dirs, files in os.walk(dir):
        for filename in files:
            start=filename.find("_")+1
            end=filename.find(".")
            if start<1 or end<1:
                continue
            num = int(filename[start:end])
            if num<=index:
                continue
            L.append(num)
    L.sort()
    return L


def getIndex():
    FILE = open(legacyconf.index_file,"r")
    i=FILE.read()
    FILE.close()
    return int(i)

def putIndex(i):
    FILE = open(legacyconf.index_file,"w")
    FILE.write(str(i))
    FILE.close()

def ifScaredRunAway():
    FILE = open(legacyconf.error_file, "r")
    c = FILE.read()
    FILE.close()
    if len(c)>0:
        runAway()

def runAway():
    if (legacyconf.notifications_enabled):
        os.system("cat " + legacyconf.error_file + " | mailx -s \"DataMart-ingress Failed on "+legacyconf.server_name+"\" "+legacyconf.notify_addresses)
    releaseLock()
    sys.exit()

def clearErrorFile():
    f = open(legacyconf.error_file, "w")
    f.close()

def brag(f):
    FILE = open(legacyconf.log_file, "a")
    FILE.write("Successfully ingressed '" + f + "' at " + str(datetime.datetime.now()) + ".\n")
    FILE.close()

def getLock():
    # bail if ingress is already running in another process
    if os.path.isfile(legacyconf.lock_file):
        if (legacyconf.notifications_enabled):
            os.system("echo Could not get lock file because another process is already running, or because the lockfile is inaccessible. | mailx -s \"DataMart-ingress Failed on "+legacyconf.server_name+"\" "+legacyconf.notify_addresses)
        sys.exit()
    # create the lock file
    f = open(legacyconf.lock_file, "w")
    f.close()
    # bail if unable to create lock file
    if not os.path.isfile(legacyconf.lock_file):
        if (legacyconf.notifications_enabled):
            os.system("echo Could not create lock file. | mailx -s \"DataMart-ingress Failed on "+legacyconf.server_name+"\" "+legacyconf.notify_addresses)
        sys.exit()

def releaseLock():
    os.unlink(legacyconf.lock_file)


# ~~~~~~~~~~~~~~~~~~~~~~~~~
# Start of script execution
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~

if (len(sys.argv) != 1):
    usage()
    sys.exit()

# create a lock file to avoid inter-process meddling
getLock()

# get the index of the last file that was successfully ingressed
index = getIndex()

# Get the indexes of the new files to be ingressed.
# The file names take the form ProjectXml_NN.xml, where NN is an integer.
# If index==44 and there are three new files waiting to be processed in projectfiles_dir,
# `newindexes` will probably look like this: {45,46,47}
newindexes=getNewFileList(legacyconf.projectfiles_dir, index)

# it might make sense to save this with the current date, but the results are emailed so it's probably no big deal to just erase this
clearErrorFile()

# loop over new files and ingress them
for x in newindexes:
    p = "file:" + legacyconf.projectfiles_dir + "ProjectXml_" + str(x) + ".xml"
    if (legacyconf.test_mode):
        p = " -t " + p
    ingressFile(p)
    putIndex(x)


# TODO: fix the java code responsible for ingressing appeals/objections, then un-comment the following two lines
# ingressFile("file:" + legacyconf.appeals_file)
# ingressFile("file:" + legacyconf.objections_file)

releaseLock()

