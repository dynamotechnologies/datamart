package ingress.objections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.zip.DataFormatException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

/*
 * This class encapsulates xml ingressed from files validated with the following DTD:
 *
 * <!DOCTYPE objections [
 * <!ELEMENT objections (timestamp?, objection+)>
 * <!ATTLIST objections xmlns CDATA "http://www.fs.fed.us/pals/xmlns/objections/">
 * <!ELEMENT timestamp (#PCDATA)>
 * <!ELEMENT objection (id?,code,rule,objector,project,response)>
 * <!ELEMENT id (#PCDATA)>
 * <!ELEMENT code (#PCDATA)>
 * <!ELEMENT rule (#PCDATA)>
 * <!ELEMENT objector (#PCDATA)>
 * <!ELEMENT project (name,adminunit)>
 * <!ELEMENT name (#PCDATA)>
 * <!ELEMENT adminunit (#PCDATA)>
 * <!ELEMENT response (date,documentid,filesize)>
 * <!ELEMENT date (#PCDATA)>
 * <!ELEMENT documentid (#PCDATA)>
 * <!ELEMENT filesize (#PCDATA)>
 * ]>
 *
 *
 * This class is responsible for encapsulating element `objections` as specified above
 */
public class LegacyObjectionList {
    private String timestamp = ""; // optional
    private ArrayList<LegacyObjection> objections = new ArrayList<LegacyObjection>(); // one or many

    public LegacyObjectionList(Document doc)
            throws DataFormatException
    {
            putTimeStamp(doc);
            putObjections(doc);
    }

    private void putTimeStamp(Document d)
            throws DataFormatException
    {
        NodeList nodes = d.getElementsByTagName("timestamp");
        if (nodes.getLength()>0)
            timestamp = nodes.item(0).getTextContent();
    }

    private void putObjections(Document d)
            throws DataFormatException
    {
        NodeList nodes = d.getElementsByTagName("objection");
        for (int i=0; i<nodes.getLength(); i++)
        {
            try {
                objections.add(new LegacyObjection(nodes.item(i)));
            } catch (Exception ex) {
                System.out.println("Objection ingress failed.\n"+nodes.item(i).getTextContent()+"\n\n"+ex.getMessage()+"\n\n\n");
            }
        }
        if (!(objections.size()>0))
            throw new DataFormatException("Document does not contain any objections.");
    }

    public String getTimeStamp()
    {
        return timestamp;
    }

    public Iterator getObjectionsIterator()
    {
        return objections.iterator();
    }
}