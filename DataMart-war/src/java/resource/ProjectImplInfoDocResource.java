/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.ProjectImplInfoDocManagerRemote;
import us.fed.fs.www.nepa.schema.ProjectImplInfoDoc.ObjectFactory;
import us.fed.fs.www.nepa.schema.ProjectImplInfoDoc.ProjectImplInfoDoc;
import us.fed.fs.www.nepa.schema.ProjectImplInfoDoc.ProjectImplInfoDocs;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;

/**
 *
 * @author stsbhava
 */
@Path ("/projects/{projtype}/{projid}/ProjectImplInfo/docs")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class ProjectImplInfoDocResource extends AbstractResource
{
    private ProjectImplInfoDocManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public ProjectImplInfoDocResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "ProjectImplInfoDoc.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.ProjectImplInfoDoc");
        MY_PROPS.setProperty("bean", "ProjectImplInfoDocManagerRemote");
        try {
            beanmanager = (ProjectImplInfoDocManagerRemote) init(ProjectImplInfoDocManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("ProjectImplInfoDocResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("ProjectImplInfoDocResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(@PathParam("projtype") String projtype, @PathParam("projid") String projid, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;

        try {
            ProjectImplInfoDoc projectImplInfoDoc = (ProjectImplInfoDoc)unmarshal(inputxml, ProjectImplInfoDoc.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+projectImplInfoDoc.getImplInfoDocId());

           
        
            // All clear. Now do the insert.
            beanmanager.create(projectImplInfoDoc);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the newly created entity
        return created(uri);
    }


    @GET
    @Path("/{implInfoDocId}")
    public Response get(@PathParam("projtype") String projtype, @PathParam("projid") String projid,  @PathParam("implInfoDocId") Integer implInfoDocId, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            ProjectImplInfoDoc projectImplInfoDoc = beanmanager.get(implInfoDocId.toString());

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<ProjectImplInfoDoc> jaxbelement = objectfactory.createProjectImplInfoDoc(projectImplInfoDoc);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    /*
     *  Get list of all project Meetingnotices under this project
     */
    @GET
    public Response getAllByProject(@PathParam("projtype") String projtype, @PathParam("projid") String projid,@Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the name of the specified unit
            ProjectImplInfoDocs projectImplInfoDocs = beanmanager.getAllByProject(projtype, projid);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<ProjectImplInfoDocs> jaxbelement = objectfactory.createProjectImplInfoDocs(projectImplInfoDocs);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }

    @PUT
    @Path("/{implInfoDocId}")
    public Response put(@PathParam("projtype") String projtype, @PathParam("projid") String projid, @PathParam("implInfoDocId") Integer implInfoDocId, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        ProjectImplInfoDoc projectImplInfoDoc = null;
        try {
            projectImplInfoDoc = (ProjectImplInfoDoc) unmarshal(inputxml, ProjectImplInfoDoc.class);

          
           
             if (!implInfoDocId.toString().equals(projectImplInfoDoc.getImplInfoDocId()))
                complainLoudly(new Exception("implInfoDocId in uri must match ID in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.update(projectImplInfoDoc);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"</location>");
    }


    @DELETE
    @Path("/{implInfoDocId}")
    public Response delete(@PathParam("projtype") String projtype, @PathParam("projid") String projid, @PathParam("implInfoDocId") Integer implInfoDocId,  @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(implInfoDocId.toString());
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }

    
}
