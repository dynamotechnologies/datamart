package client;


//import com.sun.jersey.api.client.Client;
//import com.sun.jersey.api.client.WebResource;
//import com.sun.jersey.api.client.ClientResponse;
//import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;


import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;




import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.xml.sax.SAXException;
import org.w3c.dom.ls.LSResourceResolver;
import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;

public class ClientUtil {
    protected static final int OK = 200;
    protected static final int CREATED = 201;
    protected static final int NOCONTENT = 204;
    protected static String CONTENT_TYPE;
    protected static String ENCODING;
    private static MyLSResourceResolver myResolver = null;
    private Properties clientprops = null;
    private HashMap<String, Schema> cachedSchemas = new HashMap<String, Schema>();
    protected static Client myClient = null;

    
    // prevent instantiation
    private ClientUtil()  {}

    private Boolean doInit()
    {
        try {
            CONTENT_TYPE = getPropNoNull("client.content.type");
            ENCODING = getPropNoNull("client.encoding");
            return true;
        } catch (IOException ex) {
            Logger.getLogger(ClientUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    /*
     *
     * Singleton methods
     */
    public static synchronized ClientUtil getInstance() {
        return (ClientUtilHolder.isOk) ? ClientUtilHolder.INSTANCE : null;
    }

    private static class ClientUtilHolder
    {
        private static final ClientUtil INSTANCE = new ClientUtil();
        private static final Boolean isOk = INSTANCE.doInit();
    }


    /*
     *
     * Protected methods
     */
    protected String getServerPath()
            throws IOException
    {
        return getPropNoNull("server.path") + "/" + getPropNoNull("client.version");
    }


    protected String getSchemaPath()
            throws IOException
    {
        return getServerPath() + "/schema/";
    }


    protected String dumpResponse(Response cr)
    {
        return "Status="+cr.getStatus()+". "+cr.toString();
    }
    

    protected synchronized Schema getRemoteSchema(String filename)
            throws IOException
    {
        if (cachedSchemas.containsKey(filename))
        {
            return cachedSchemas.get(filename);
        }
        String path = getSchemaPath() + filename;

        if (myResolver == null) {
            try {
                myResolver = new MyLSResourceResolver();
            }
            catch (ClassNotFoundException e) {
                throw (new IOException("Can't init resource resolver, ClassNotFound Exception"));
            }
            catch (IllegalAccessException e) {
                throw (new IOException("Can't init resource resolver, IllegalAccessException"));
            }
            catch (InstantiationException e) {
                throw (new IOException("Can't init resource resolver, Instantiation Exception"));
            }
        }

        // Retreive the remote schema file
       // Client client = Client.create();
       Client client =ClientBuilder.newClient();
        addAuthFilter(client);

        if (myClient == null) {
            myClient = client;
        }
       // WebResource webResource0 = client.resource(path);
        WebTarget webResource0 = client.target(path);
        Response schemaresponse = webResource0.request().get();
        // Check for communication problems
        if (schemaresponse.getStatus()!=200)
            throw new IOException("Failed to GET the schema file at path '" + path + "'");

        // Create a new schema object from the file and return it
        InputStream schemastream = schemaresponse.readEntity(InputStream.class);
        StreamSource schemasource = new StreamSource(schemastream);
        SchemaFactory schemafactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema;
        schemafactory.setResourceResolver(myResolver);
        try {
            schema = schemafactory.newSchema(schemasource);
        } catch (SAXException ex) {
            Logger.getLogger(ClientUtil.class.getName()).log(Level.SEVERE, null, ex);
            throw new IOException("Could not parse schema '" + path + "'");
        }
        cachedSchemas.put(filename, schema);
        return schema;
    }


    protected void addAuthFilter(Client client)
            throws IOException
    {
        String user = getPropNoNull("client.authuser");
        String pass = getPropNoNull("client.authpass");
        //client.addFilter(new HTTPBasicAuthFilter(user, pass));
        client.register(HttpAuthenticationFeature.basic(user,pass));
    }

    /*
     *
     * Private methods
     */
    private Properties loadClientProps()
            throws IOException
    {
        Properties props = new Properties();
        InputStream propstream = UnitClient.class.getResourceAsStream("client.properties");
        props.load(propstream);
        return props;
    }
    

    private String getPropNoNull(String propname)
            throws IOException
    {
        if (clientprops==null)
            clientprops = loadClientProps();
        String propval = clientprops.getProperty(propname);
        if (propval==null)
            throw new IOException("Property "+propname+" not found.");
        return propval;
    }

    private class MyLSResourceResolver implements LSResourceResolver {

        private DOMImplementationLS domImplementationLS;

        private MyLSResourceResolver()
                throws ClassNotFoundException, IllegalAccessException, InstantiationException
        {
            DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
            domImplementationLS = (DOMImplementationLS) registry.getDOMImplementation("LS");
        }

        public LSInput resolveResource(String type, String namespaceURI, String publicId, String systemId, String baseURI)
        {
             System.out.println("==== Resolving '" + type + "' '" + namespaceURI + "' '" + publicId + "' '" + systemId + "' '" + baseURI + "'");

            LSInput lsInput = domImplementationLS.createLSInput();
            String newLocation = "";
            String serverPath = "SERVERPATHERR/";
            try {
                serverPath = getServerPath() + "/";
            } catch (IOException e) { }

            if (systemId.indexOf("../") == 0) {
                newLocation = serverPath + systemId.substring(3);
            } else {
                newLocation = serverPath + systemId;    // This is probably an error anyway
            }

            // Retrieve the remote schema file
            //WebResource webResource0 = myClient.resource(newLocation);
             WebTarget webResource0 = myClient.target(newLocation);
            //ClientResponse schemaresponse = webResource0.get(ClientResponse.class);
            Response schemaresponse = webResource0.request().get(); //webResource0.get(ClientResponse.class);
            // Check for communication problems
            if (schemaresponse.getStatus()!=200)
                return null;    // bail

            // Create a new schema object from the file and return it
            InputStream schemastream =  schemaresponse.readEntity(InputStream.class); //schemaresponse.getEntityInputStream();

            lsInput.setByteStream(schemastream);
            lsInput.setSystemId(systemId);
            return lsInput;
        }
    }
}