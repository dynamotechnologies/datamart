package stateless;

import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.resourcearea.*;
import beanutil.DataMartException;

@Remote
public interface ResourceAreaManagerRemote {
/*
 *
 * The Manager beans are responsible for:
 *   1) Implementing business logic and
 *   2) Translating front-end resources to back-end database entities.
 *
 * Often the resources will consist of data from numerous database tables. It is the manager's responsibility
 *   to ensure that these front-end resources can be accessed as though they were cut from a single piece of wood.
 *
 */
    public void create(Resourcearea resource) throws DataMartException;
    public Resourcearea get(Integer id) throws DataMartException;
    public Resourceareas getListByUnitId(String unitid) throws DataMartException;
    public void update(Resourcearea resource) throws DataMartException;
    public void delete(Integer id) throws DataMartException;
    public Boolean ping();
}
