/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.ProjectMeetingNotices.*;

/**
 *
 * @author gauri
 */

public class ProjectMeetingNoticesClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public ProjectMeetingNoticesClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.ProjectMeetingNotices", "ProjectMeetingNotices.xsd");
    }


    /*public void postProjectMeetingNotices(Integer id, String projectid, String description,String updatedBy, String updatedDate)
            throws IOException
    {
        Meetingnotice meetingnotice = new Meetingnotice();
        meetingnotice.set(id.toString());
        meetingnotice.setDescription(description);
        meetingnotice.setProjectId(projectid);
        meetingnotice.setLastUpdateBy(updatedBy);
        meetingnotice.setLasteUpdateDate(updatedDate);
        postProjectMeetingNotices(meetingnotice);
    }*/


    public void postProjectMeetingNotices(Meetingnotice meetingnotice)
            throws IOException
    {
        post(objectfactory.createMeetingnotice(meetingnotice), "/projects/nepa/"+meetingnotice.getDocumentId()+"/meetingnotices/");
    }


    public Meetingnotice getProjectMeetingNotices(String projectId,String meetingId)
            throws IOException
    {
        return get("/projects/nepa/"+projectId+"/meetingnotices/" + meetingId, Meetingnotice.class);
    }

    public Meetingnotices getAllProjectMeetingNotices(String projectId)
            throws IOException
    {
        return get("/projects/nepa/"+projectId+"/meetingnotices/" , Meetingnotices.class);
    }

//    public void putProjectMeetingNotices(Integer id, String projectid, String description,String updatedBy, String updatedDate)
//            throws IOException
//    {
//        ProjectMeetingNotices projectImplInfo = new ProjectMeetingNotices();
//        projectImplInfo.setImplInfoId(id.toString());
//        projectImplInfo.setDescription(description);
//        projectImplInfo.setProjectId(projectid);
//        projectImplInfo.setLastUpdateBy(updatedBy);
//        projectImplInfo.setLasteUpdateDate(updatedDate);
//        putProjectMeetingNotices(projectImplInfo);
//    }


    public void putProjectMeetingNotices(Meetingnotice meetingnotice)
            throws IOException
    {
        put(objectfactory.createMeetingnotice(meetingnotice), "/projects/nepa/"+meetingnotice.getDocumentId()+"/meetingnotices/" + meetingnotice.getMeetingId());
    }


    public void deleteProjectMeetingNotices(String projectId,String meetingId)
            throws IOException
    {
        delete("/projects/nepa/"+projectId+"/meetingnotices/" + meetingId);
    }
    
     
           
}