/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.ProjectView.*;

/**
 *
 * @author gauri
 */

public class ProjectViewsClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public ProjectViewsClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.ProjectView", "ProjectView.xsd");
    }


   


    public void postProjectView(ProjectView projectView)
            throws IOException
    {
        post(objectfactory.createProjectView(projectView), "/projects/nepa/"+projectView.getProjectId()+"/views/");
    }


    public ProjectView getProjectViews(String projectId,String viewId)
            throws IOException
    {
        return get("/projects/nepa/"+projectId+"/views/" + viewId, ProjectView.class);
    }

    public ProjectViews getAllProjectViews(String projectId,String userId)
            throws IOException
    {
        return get("/projects/nepa/"+projectId+"/views/?userId="+userId , ProjectViews.class);
    }




    public void putProjectView(ProjectView projectView)
            throws IOException
    {
        put(objectfactory.createProjectView(projectView), "/projects/nepa/"+projectView.getProjectId()+"/views/" + projectView.getId());
    }


    public void deleteProjectView(String projectId,String viewId)
            throws IOException
    {
        delete("/projects/nepa/"+projectId+"/views/" + viewId);
    }
    
     
           
}