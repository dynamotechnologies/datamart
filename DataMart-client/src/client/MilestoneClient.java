package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.milestone.*;


public class MilestoneClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public MilestoneClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.milestone", "Milestone.xsd");
    }


    public void postMilestone(String id, String name)
            throws IOException
    {
        Milestone milestone = new Milestone();
        milestone.setId(id);
        milestone.setName(name);
        postMilestone(milestone);
    }


    public void postMilestone(Milestone milestone)
            throws IOException
    {
        post(objectfactory.createMilestone(milestone), "/ref/milestones");
    }


    public Milestone getMilestone(String id)
            throws IOException
    {
        return get("/ref/milestones/" + id, Milestone.class);
    }


    public Milestones getMilestoneList()
            throws IOException
    {
        return get("/ref/milestones", Milestones.class);
    }


    public void putMilestone(String id, String name)
            throws IOException
    {
        Milestone milestone = new Milestone();
        milestone.setId(id);
        milestone.setName(name);
        putMilestone(milestone);
    }


    public void putMilestone(Milestone milestone)
            throws IOException
    {
        put(objectfactory.createMilestone(milestone), "/ref/milestones/" + milestone.getId());
    }


    public void deleteMilestone(String id)
            throws IOException
    {
        delete("/ref/milestones/" + id);
    }
}