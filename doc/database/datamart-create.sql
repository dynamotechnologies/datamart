CREATE TABLE RefDecisionTypes (
  Id varchar(10) NOT NULL,
  Description varchar(255) NOT NULL,
  PalsId int(11) NOT NULL,
  PRIMARY KEY  (Id),
  CONSTRAINT UnRefDecisionTypes_PalsId UNIQUE(PalsId)
) ENGINE=InnoDB;

CREATE TABLE RefDocumentSensitivityRationales (	-- Added for uploader
  Id int(11) NOT NULL,
  SensitivityId int(11) NOT NULL,
  Description varchar(255) NOT NULL,
  DisplayOrder int(11) NOT NULL,
  CONSTRAINT PkRefDocumentSensitivityRationales_Id PRIMARY KEY (Id)
) ENGINE=INNODB;

CREATE TABLE RefAnalysisApplicables (	-- Added for CARA
  Id int NOT NULL,
  AnalysisTypeId varchar(255) NOT NULL,	-- matches Projects.AnalysisTypeId
  ApplicableRegId int(11) NOT NULL,	-- matches Projects.CommentRegulationId
  CommentPeriodDays varchar(50),
  ObjectionPeriodDays varchar(50),
  Active tinyint(1) NOT NULL,
  CONSTRAINT PkRefAnalysisApplicables_Id PRIMARY KEY (Id)
) ENGINE=INNODB;

CREATE TABLE RefRoles (			-- Added for CARA
  Id int NOT NULL,
  RoleName varchar(50) NOT NULL,
  Description varchar(255) NOT NULL,
  CONSTRAINT PkRefRoles_Id PRIMARY KEY (Id)
) ENGINE=INNODB;

CREATE TABLE Users (			-- Added for CARA
  Id varchar(50) NOT NULL,		-- PALS shortname
  FirstName varchar(100) NOT NULL,
  LastName varchar(100) NOT NULL,
  PhoneNumber varchar(100) NOT NULL,
  Title varchar(50) NOT NULL,
  Email varchar(255) NOT NULL,
  CONSTRAINT PkUsers_Id PRIMARY KEY (Id),
  INDEX (FirstName, LastName)
) ENGINE=INNODB;

CREATE TABLE RefStates (
  Id varchar(8) NOT NULL,
  Name varchar(50) NOT NULL,
  CONSTRAINT PkRefStates_Id PRIMARY KEY (Id)
) ENGINE=INNODB;

CREATE TABLE RefCounties (
  Id varchar(8) NOT NULL,
  Name varchar(50) NOT NULL,
  CONSTRAINT PkRefCounties_Id PRIMARY KEY (Id)
) ENGINE=INNODB;

CREATE TABLE RefMilestones (
  Id varchar(8) NOT NULL,
  Name varchar(255) NOT NULL,
  CONSTRAINT PkRefMilestones_Id PRIMARY KEY (Id)
) ENGINE=INNODB;

CREATE TABLE RefSpecialAuthorities (
  Id varchar(8) NOT NULL,
  Name varchar(255) NOT NULL,
  Description varchar(1000),
  CONSTRAINT PkRefSpecialAuthorities_Id PRIMARY KEY (Id)
) ENGINE=INNODB;

CREATE TABLE RefActivities (
  Id varchar(8) NOT NULL,
  Name varchar(50) NOT NULL,
  CONSTRAINT PkRefActivities_Id PRIMARY KEY (Id)
) ENGINE=INNODB;

CREATE TABLE RefUnits (
  Id varchar(255) NOT NULL,
  Name varchar(255) NOT NULL,
  ExtendedDetails tinyint(1) NOT NULL,
  ProjectMap tinyint(1) NOT NULL,
  Address1 varchar(255),
  Address2 varchar(255),
  City varchar(255),
  State varchar(255),
  Zip varchar(20),
  Phone varchar(20),
  wwwLink varchar(255),
  CommentEmail varchar(255),
  Newspaper varchar(255),
  NewspaperURL varchar(255),
  BoundaryURL varchar(255),
  Active tinyint(1) NOT NULL,
  SpotlightId1 varchar(255),
  SpotlightId2 varchar(255),
  SpotlightId3 varchar(255),
  CONSTRAINT PkRefUnits_Id PRIMARY KEY (Id)
) ENGINE=INNODB;

CREATE TABLE RefStatuses (
  Id int(11) NOT NULL,
  Name varchar(255) NOT NULL,
  CONSTRAINT PkRefStatuses_Id PRIMARY KEY (Id)
) ENGINE=INNODB;

CREATE TABLE RefPurposes (
  Id varchar(2) NOT NULL,
  Name varchar(255) NOT NULL,
  CONSTRAINT PkRefPurposes_Id PRIMARY KEY (Id)
) ENGINE=INNODB;

CREATE TABLE RefAnalysisTypes (
  Id varchar(255) NOT NULL,
  Name varchar(255) NOT NULL,
  CONSTRAINT PkRefAnalysisTypes_Id PRIMARY KEY (Id)
) ENGINE=INNODB;

CREATE TABLE RefAppealRules (
  Id varchar(8) NOT NULL,
  Rule varchar(30) NOT NULL,
  Description varchar(500) NOT NULL,
  CONSTRAINT PkRefAppealRules_Id PRIMARY KEY (Id)
) ENGINE=INNODB;

CREATE TABLE RefAppealStatuses (
  Id int(11) NOT NULL,
  Name varchar(255) NOT NULL,
  CONSTRAINT PkRefAppealStatuses_Id PRIMARY KEY (Id)
) ENGINE=INNODB;

CREATE TABLE RefAppealOutcomes (
  Id int(11) NOT NULL,
  Outcome varchar(255) NOT NULL,
  CONSTRAINT PkRefAppealOutcomes_Id PRIMARY KEY (Id)
) ENGINE=INNODB;

CREATE TABLE RefAppealDismissalReasons (
  Id int(11) NOT NULL,
  Name varchar(255) NOT NULL,
  CONSTRAINT PkRefAppealDismissalReasons_Id PRIMARY KEY (Id)
) ENGINE=INNODB;

CREATE TABLE RefCommentRegulations (
  Id int(11) NOT NULL,
  Name varchar(255) NOT NULL,
  CONSTRAINT PkRefCommentRegulations_Id PRIMARY KEY (Id)
) ENGINE=INNODB;

CREATE TABLE RefCategoricalExclusions (
  Id int(8) NOT NULL,
  Name varchar(255) NOT NULL,
  Description varchar(1024),
  CONSTRAINT PkRefCategoricalExclusions_Id PRIMARY KEY (Id)
) ENGINE=INNODB;

CREATE TABLE RefLitigationOutcomes (
  Id int(8) NOT NULL,
  Name varchar(255) NOT NULL,
  CONSTRAINT PkRefLitigationOutcomes_Id PRIMARY KEY (Id)
) ENGINE=INNODB;

CREATE TABLE RefLitigationStatuses (
  Id int(8) NOT NULL,
  Name varchar(255) NOT NULL,
  CONSTRAINT PkRefLitigationStatuses_Id PRIMARY KEY (Id)
) ENGINE=INNODB;

-- Unit Roles aka Access Group
CREATE TABLE UnitRoles (		-- Added for CARA
  Id int(11) NOT NULL,			-- Assigned by PALS
  ShortName varchar(50) NOT NULL,	-- FK to Users
  UnitId varchar(255) NOT NULL,		-- FK to RefUnits
  RoleId int NOT NULL,			-- FK to RefRoles
  CONSTRAINT PkRefCommentRegulations_Id PRIMARY KEY (Id),
  CONSTRAINT FkUnitRoles_ShortName FOREIGN KEY (ShortName) REFERENCES Users(Id),
  CONSTRAINT FkUnitRoles_UnitId FOREIGN KEY (UnitId) REFERENCES RefUnits (Id),
  CONSTRAINT FkUnitRoles_RoleId FOREIGN KEY (RoleId) REFERENCES RefRoles (Id)
) ENGINE=INNODB;


-- admin_projtype
CREATE TABLE ProjectTypes (
  Id varchar(10) NOT NULL,
  Type varchar(255) NOT NULL,
  CONSTRAINT PkProjectTypes_Id PRIMARY KEY (Id)
) ENGINE=INNODB;


-- admin_app
CREATE TABLE Applications (
  Id varchar(10) NOT NULL,
  Name varchar(255) NOT NULL,
  CONSTRAINT PkApplications_Id PRIMARY KEY (Id)
) ENGINE=INNODB;

-- proj_info
CREATE TABLE Projects (
  Id int(11) NOT NULL AUTO_INCREMENT,
  ProjectTypeId varchar(10) NOT NULL,
  PublicId varchar(255) DEFAULT NULL, -- PALSID
  ApplicationId varchar(10) NOT NULL,
  UnitId varchar(255) NOT NULL,
  ProjectDocumentId int(11),	-- PALS alternate project ID
  AnalysisTypeId varchar(255) DEFAULT NULL,
  StatusId int(11) NOT NULL,
  CommentRegulationId int(11) DEFAULT NULL,
  Name varchar(255) NOT NULL,
  Description varchar(255) DEFAULT NULL,
  LastUpdate DATETIME DEFAULT NULL,
  ContactName varchar(255) DEFAULT NULL,
  ContactPhone varchar(255) DEFAULT NULL,
  ContactEmail varchar(255) DEFAULT NULL,
  wwwIsPublished tinyint(1) NOT NULL,
  wwwSummary varchar(4000) DEFAULT NULL,
  wwwLink varchar(255) DEFAULT NULL,
  ExpirationDate DATETIME DEFAULT NULL,
  LocationDesc varchar(255) DEFAULT NULL,
  LocationLegalDesc varchar(255) DEFAULT NULL,
  Latitude double DEFAULT NULL,
  Longitude double DEFAULT NULL,
  SopaPublish tinyint(1) DEFAULT 0,
  SopaNew tinyint(1) DEFAULT 0,
  SopaHeaderCat varchar(255),
  Esd tinyint(1) NOT NULL DEFAULT 0, 
  Cenodecision tinyint(1) NOT NULL DEFAULT 0, 
  DecisionDecided DATETIME DEFAULT NULL,
  CONSTRAINT PkProjects_Id PRIMARY KEY (Id),
  CONSTRAINT FkProjects_ProjectTypeId FOREIGN KEY (ProjectTypeId) REFERENCES ProjectTypes (Id),
  CONSTRAINT FkProjects_ApplicationId FOREIGN KEY (ApplicationId) REFERENCES Applications (Id),
  CONSTRAINT FkProjects_UnitId FOREIGN KEY (UnitId) REFERENCES RefUnits (Id),
  CONSTRAINT FkProjects_AnalysisTypeId FOREIGN KEY (AnalysisTypeId) REFERENCES RefAnalysisTypes (Id),
  CONSTRAINT FkProjects_StatusId FOREIGN KEY (StatusId) REFERENCES RefStatuses (Id),
  CONSTRAINT FkProjects_CommentRegulationId FOREIGN KEY (CommentRegulationId) REFERENCES RefCommentRegulations (Id),
  CONSTRAINT UnProjects_PublicId_ProjectTypeId UNIQUE (PublicId, ProjectTypeId)
) ENGINE=INNODB;


-- doc_node
CREATE TABLE ProjectDocumentContainers (
  Id int(11) NOT NULL AUTO_INCREMENT,
  ProjectId int(11) NOT NULL,
  ContainerId int(11) DEFAULT NULL,	-- FK to parent container
  Label varchar(255) NOT NULL,
  OrderTag int(11) NOT NULL,
  PalsContId int(11) DEFAULT NULL,	-- Public PALS container ID property
  StaticFlag tinyint(1) NOT NULL DEFAULT 0, 
  CONSTRAINT PkProjectDocumentContainers_Id PRIMARY KEY (Id),
  CONSTRAINT FkProjectDocumentContainers_ProjectId FOREIGN KEY (ProjectId) REFERENCES Projects (Id),
  CONSTRAINT FkProjectDocumentContainers_ContainerId FOREIGN KEY (ContainerId) REFERENCES ProjectDocumentContainers (Id) ON DELETE CASCADE
) ENGINE=INNODB;


-- proj_docs
-- Joins table Projects with imaginary table Documents, which may be added later
CREATE TABLE ProjectDocuments (
  Id int(11) NOT NULL AUTO_INCREMENT,
  ProjectId int(11) NOT NULL,
  ContainerId int(11) DEFAULT NULL,
  DocumentId varchar(255) NOT NULL,
  Label varchar(255) NOT NULL,
  PdfSize varchar(255) NOT NULL,
  isPublished tinyint(1) NOT NULL,
  Description varchar(1000) DEFAULT NULL,
  Link varchar(255) DEFAULT NULL,
  OrderTag int(11) DEFAULT NULL,
  isSensitive tinyint(1) NOT NULL DEFAULT 0,
  SensitivityRationale int(11) NOT NULL DEFAULT 0,
  Author varchar(255),
  Addressee varchar(255),
  DatePublished datetime,
  DocumentDate datetime,
  DateLabel varchar(255),
  CONSTRAINT PkProjectDocuments_Id PRIMARY KEY (Id),
  CONSTRAINT FkProjectDocuments_ProjectId FOREIGN KEY (ProjectId) REFERENCES Projects (Id),  
  CONSTRAINT FkProjectDocuments_SensitivityRationale FOREIGN KEY (SensitivityRationale) REFERENCES RefDocumentSensitivityRationales (Id),
  CONSTRAINT FkProjectDocuments_ContainerId FOREIGN KEY (ContainerId) REFERENCES ProjectDocumentContainers (Id) ON DELETE SET NULL,
  CONSTRAINT UnProjectDocuments_ProjectId_DocumentId UNIQUE (ProjectId, DocumentId)
) ENGINE=INNODB;


-- proj_purp_list
-- joins table Projects with table RefPurposes
CREATE TABLE ProjectPurposes (
  Id int(11) NOT NULL AUTO_INCREMENT,
  ProjectId int(11) NOT NULL,
  PurposeId varchar(2) NOT NULL,
  CONSTRAINT PkProjectPurposes_Id PRIMARY KEY (Id),
  CONSTRAINT FkProjectPurposes_ProjectId FOREIGN KEY (ProjectId) REFERENCES Projects (Id),
  CONSTRAINT FkProjectPurposes_PurposeId FOREIGN KEY (PurposeId) REFERENCES RefPurposes (Id)
) ENGINE=INNODB;


-- obj_info
-- FkObjections_ProjectDocumentId should probably reference table Documents. Since that doesnt exist well use ProjectDocuments instead
CREATE TABLE Objections (
  Id varchar(255) NOT NULL,
  ProjectDocumentId int(11) DEFAULT NULL,
  ProjectId int(11) NOT NULL,
  Objector varchar(255) NOT NULL,
  ResponseDate DATETIME NOT NULL,
  CONSTRAINT PkObjections_Id PRIMARY KEY (Id),
  CONSTRAINT FkObjections_ProjectDocumentId FOREIGN KEY (ProjectDocumentId) REFERENCES ProjectDocuments (Id),
  CONSTRAINT FkObjections_ProjectId FOREIGN KEY (ProjectId) REFERENCES Projects (Id)
) ENGINE=INNODB;


CREATE TABLE Decisions (
  Id int(11) NOT NULL,
  ProjectId int(11) NOT NULL,      -- FK to Projects
  Name varchar(255),
  AppealStatus varchar(255),
  DecConstraint varchar(255),
  LegalNoticeDate DATETIME DEFAULT NULL,
  AreaSize double,		-- PALS 3.3
  AreaUnits varchar(255),	-- PALS 3.3
  DecisionDate datetime,
  DecisionType varchar(10),
  CONSTRAINT PkDecisions_Id PRIMARY KEY (Id),
  CONSTRAINT FkDecisions_ProjectId FOREIGN KEY (ProjectId) REFERENCES Projects(Id),
  CONSTRAINT FkDecisions_DecisionType FOREIGN KEY (DecisionType) REFERENCES RefDecisionTypes(Id)
) ENGINE=INNODB;


CREATE TABLE Litigations (
  Id int(11) NOT NULL,
  DecisionId int(11) NOT NULL,
  CaseName varchar(255),
  Status int(8),
  Outcome int(8),
  ClosedDate DATETIME default NULL,
  CONSTRAINT PKLitigations_Id PRIMARY KEY (Id),
  CONSTRAINT FKLitigations_DecisionId FOREIGN KEY (DecisionId) REFERENCES Decisions(Id),
  CONSTRAINT FKLitigations_Status FOREIGN KEY (Status) REFERENCES RefLitigationStatuses(Id),
  CONSTRAINT FKLitigations_Outcome FOREIGN KEY (Outcome) REFERENCES RefLitigationOutcomes(Id)
) ENGINE=INNODB;


CREATE TABLE DecisionAppealRules (
  Id int(11) NOT NULL AUTO_INCREMENT,
  DecisionId int(11),
  AppealRule varchar(8),
  CONSTRAINT PKDecisionAppealRules_Id PRIMARY KEY (Id),
  CONSTRAINT FKDecisionAppealRules_DecisionId FOREIGN KEY (DecisionId) REFERENCES Decisions(Id),
  CONSTRAINT FKDecisionAppealRules_AppealRule FOREIGN KEY (AppealRule) REFERENCES RefAppealRules(Id),
  CONSTRAINT UnDecisionAppealRules_DecisionId_AppealRule UNIQUE (DecisionId, AppealRule)
) ENGINE=INNODB;


CREATE TABLE NepaCategoricalExclusions (
  Id int(11) NOT NULL AUTO_INCREMENT,
  ProjectId int(11) NOT NULL,
  CategoricalExclusionId int(8) NOT NULL,
  CONSTRAINT PKNepaCategoricalExclusions_Id PRIMARY KEY (Id),
  CONSTRAINT FkNepaCategoricalExclusions_ProjectId FOREIGN KEY (ProjectId) REFERENCES Projects(Id),
  CONSTRAINT FKNepaCategoricalExclusions_CategoricalExclusionId FOREIGN KEY (CategoricalExclusionId) REFERENCES RefCategoricalExclusions(Id),
  CONSTRAINT UnNepaCategoricalExclusions_ProjectId_CategoricalExclusionsId UNIQUE (ProjectId, CategoricalExclusionId)
) ENGINE=INNODB;


-- app_info
-- FkAppeals_ProjectDocumentId should probably reference table Documents. Since that doesnt exist well use ProjectDocuments instead
CREATE TABLE Appeals (
  Id varchar(255) NOT NULL,
  AppealStatusId int(11) NOT NULL,
  OutcomeId int(11) NOT NULL,
  ProjectDocumentId int(11) DEFAULT NULL,
  ProjectId int(11) NOT NULL,
  RuleId varchar(8) DEFAULT NULL,
  DismissalReasonId int(11) DEFAULT NULL,
  Appellant varchar(255) NOT NULL,
  ResponseDate DATETIME NOT NULL,
  AdminUnit varchar(255),
  DecisionId int(11),
  CONSTRAINT PkAppeals_Id PRIMARY KEY (Id),
  CONSTRAINT FkAppeals_AppealStatusId FOREIGN KEY (AppealStatusId) REFERENCES RefAppealStatuses (Id),
  CONSTRAINT FkAppeals_OutcomeId FOREIGN KEY (OutcomeId) REFERENCES RefAppealOutcomes (Id),
  CONSTRAINT FkAppeals_ProjectDocumentId FOREIGN KEY (ProjectDocumentId) REFERENCES ProjectDocuments (Id),
  CONSTRAINT FkAppeals_ProjectId FOREIGN KEY (ProjectId) REFERENCES Projects (Id),
  CONSTRAINT FkAppeals_RuleId FOREIGN KEY (RuleId) REFERENCES RefAppealRules (Id),
  CONSTRAINT FkAppeals_DismissalReasonId FOREIGN KEY (DismissalReasonId) REFERENCES RefAppealDismissalReasons (Id),
  CONSTRAINT FkAppeals_AdminUnit FOREIGN KEY (AdminUnit) REFERENCES RefUnits(Id),
  CONSTRAINT FkAppeals_DecisionId FOREIGN KEY (DecisionId) REFERENCES Decisions(Id)
) ENGINE=INNODB;

-- proj_special_authorities
-- joins table Projects with table RefSpecialAuthorities
CREATE TABLE ProjectSpecialAuthorities (
  Id int(11) NOT NULL AUTO_INCREMENT,
  ProjectId int(11) NOT NULL,
  SpecialAuthorityId varchar(8) NOT NULL,
  CONSTRAINT PkProjectSpecialAuthorities_Id PRIMARY KEY (Id),
  CONSTRAINT FkProjectSpecialAuthorities_ProjectId FOREIGN KEY (ProjectId) REFERENCES Projects (Id),
  CONSTRAINT FkProjectSpecialAuthorities_SpecialAuthorityId FOREIGN KEY (SpecialAuthorityId) REFERENCES RefSpecialAuthorities(Id),
  CONSTRAINT UnProjectSpecialAuthorities_ProjectId_SpecialAuthorityId UNIQUE (ProjectId, SpecialAuthorityId)
) ENGINE=INNODB;

-- proj_activities
-- joins table Projects with table RefActivities
CREATE TABLE ProjectActivities (
  Id int(11) NOT NULL AUTO_INCREMENT,
  ProjectId int(11) NOT NULL,
  ActivityId varchar(8) NOT NULL,
  CONSTRAINT PkProjectActivities_Id PRIMARY KEY (Id),
  CONSTRAINT FkProjectActivities_ProjectId FOREIGN KEY (ProjectId) REFERENCES Projects (Id),
  CONSTRAINT FkProjectActivities_ActivityId FOREIGN KEY (ActivityId) REFERENCES RefActivities(Id),
  CONSTRAINT UnProjectActivities_ProjectId_ActivityId UNIQUE (ProjectId, ActivityId)
) ENGINE=INNODB;

-- proj_regions
-- joins table Projects with table RefUnits
CREATE TABLE ProjectRegions (
  Id int(11) NOT NULL AUTO_INCREMENT,
  ProjectId int(11) NOT NULL,
  RegionId varchar(255) NOT NULL,
  CONSTRAINT PkProjectRegions_Id PRIMARY KEY (Id),
  CONSTRAINT FkProjectRegions_ProjectId FOREIGN KEY (ProjectId) REFERENCES Projects (Id),
  CONSTRAINT FkProjectRegions_RegionId FOREIGN KEY (RegionId) REFERENCES RefUnits(Id),
  CONSTRAINT UnProjectRegions_ProjectId_RegionId UNIQUE (ProjectId, RegionId)
) ENGINE=INNODB;

-- proj_forests
-- joins table Projects with table RefUnits
CREATE TABLE ProjectForests (
  Id int(11) NOT NULL AUTO_INCREMENT,
  ProjectId int(11) NOT NULL,
  ForestId varchar(255) NOT NULL,
  CONSTRAINT PkProjectForests_Id PRIMARY KEY (Id),
  CONSTRAINT FkProjectForests_ProjectId FOREIGN KEY (ProjectId) REFERENCES Projects (Id),
  CONSTRAINT FkProjectForests_ForestId FOREIGN KEY (ForestId) REFERENCES RefUnits(Id),
  CONSTRAINT UnProjectForests_ProjectId_ForestId UNIQUE (ProjectId, ForestId)
) ENGINE=INNODB;

-- proj_districts
-- joins table Projects with table RefUnits
CREATE TABLE ProjectDistricts (
  Id int(11) NOT NULL AUTO_INCREMENT,
  ProjectId int(11) NOT NULL,
  DistrictId varchar(255) NOT NULL,
  CONSTRAINT PkProjectDistricts_Id PRIMARY KEY (Id),
  CONSTRAINT FkProjectDistricts_ProjectId FOREIGN KEY (ProjectId) REFERENCES Projects (Id),
  CONSTRAINT FkProjectDistricts_DistrictId FOREIGN KEY (DistrictId) REFERENCES RefUnits(Id),
  CONSTRAINT UnProjectDistricts_ProjectId_DistrictId UNIQUE (ProjectId, DistrictId)
) ENGINE=INNODB;

-- proj_states
-- joins table Projects with table RefStates
CREATE TABLE ProjectStates (
  Id int(11) NOT NULL AUTO_INCREMENT,
  ProjectId int(11) NOT NULL,
  StateId varchar(8) NOT NULL,
  CONSTRAINT PkProjectStates_Id PRIMARY KEY (Id),
  CONSTRAINT FkProjectStates_ProjectId FOREIGN KEY (ProjectId) REFERENCES Projects (Id),
  CONSTRAINT FkProjectStates_StateId FOREIGN KEY (StateId) REFERENCES RefStates(Id),
  CONSTRAINT UnProjectStates_ProjectId_StateId UNIQUE (ProjectId, StateId)
) ENGINE=INNODB;

-- proj_counties
-- joins table Projects with table RefCounties
CREATE TABLE ProjectCounties (
  Id int(11) NOT NULL AUTO_INCREMENT,
  ProjectId int(11) NOT NULL,
  CountyId varchar(8) NOT NULL,
  CONSTRAINT PkProjectCounties_Id PRIMARY KEY (Id),
  CONSTRAINT FkProjectCounties_ProjectId FOREIGN KEY (ProjectId) REFERENCES Projects (Id),
  CONSTRAINT FkProjectCounties_CountyId FOREIGN KEY (CountyId) REFERENCES RefCounties(Id),
  CONSTRAINT UnProjectCounties_ProjectId_CountyId UNIQUE (ProjectId, CountyId)
) ENGINE=INNODB;

-- proj_milestones
-- joins table Projects with table RefMilestones
CREATE TABLE ProjectMilestones (
  Id int(11) NOT NULL,			-- Milestone sequence number
  ProjectId int(11) NOT NULL,
  MilestoneId varchar(8) NOT NULL,	-- Milestone type
  MilestoneDate varchar(50),		-- Freeform text, either M/Y or M/D/Y
  MilestoneStatus char(1) NOT NULL,	-- Estimated/Not estimated flag (0/1)
  History char(1) NOT NULL,		-- History flag (0/1)
  CONSTRAINT PkProjectMilestones_Id PRIMARY KEY (Id),
  CONSTRAINT FkProjectMilestones_ProjectId FOREIGN KEY (ProjectId) REFERENCES Projects (Id),
  CONSTRAINT FkProjectMilestones_MilestoneId FOREIGN KEY (MilestoneId) REFERENCES RefMilestones(Id)
) ENGINE=INNODB;

-- mailing list subscribers
CREATE TABLE MailingListSubscribers (	-- Added for CARA
  Id int(11) NOT NULL AUTO_INCREMENT,
  ProjectId int(11) NOT NULL,		-- FK to Projects
  SubscriberId int(11) NOT NULL,	-- CARA Subscriber ID
  Email varchar(250),
  LastName varchar(100),
  FirstName varchar(100),
  Title varchar(50),
  Org varchar(100),
  OrgType varchar(100),
  SubscriberType varchar(25),
  AddressStreet1 varchar(200),
  AddressStreet2 varchar(200),
  City varchar(100),
  State varchar(2),
  Zip varchar(10),
  Province varchar(100),
  Country varchar(100),
  Phone varchar(20),
  ContactMethod varchar(5),
  SubscribedDate DATETIME,
  CONSTRAINT PkMailingListSubscribers_Id PRIMARY KEY (Id),
  CONSTRAINT FkMailingListSubscribers_ProjectId FOREIGN KEY (ProjectId) REFERENCES Projects(Id),
  CONSTRAINT UnMailingListSubscribers_ProjectId_SubscriberId UNIQUE (ProjectId, SubscriberId)
) ENGINE=INNODB;

-- CARA projects
CREATE TABLE CARAProjects (		-- Added for CARA
  Id int(11) NOT NULL,			-- CARA ID
  ProjectId int(11) NOT NULL,		-- FK to Projects
  ReadingRoomActive tinyint(1) NOT NULL DEFAULT 0,
  CONSTRAINT PkCARAProjects_Id PRIMARY KEY (Id),
  CONSTRAINT FkCARAProjects_ProjectId FOREIGN KEY (ProjectId) REFERENCES Projects(Id),
  CONSTRAINT UnCARAProjects_ProjectId UNIQUE (ProjectId)
) ENGINE=INNODB;

-- comment phase metadata
CREATE TABLE CommentPhases (		-- Added for CARA
  Id int(11) NOT NULL AUTO_INCREMENT,
  CaraId int(11) NOT NULL,		-- FK to CARAProjects
  PhaseId int(11) NOT NULL,		-- Assigned by CARA
  Name varchar(40),
  StartDate DATETIME DEFAULT NULL,
  FinishDate DATETIME DEFAULT NULL,
  LetterCount int(11) DEFAULT 0,
  LetterUniqueCount int(11) DEFAULT 0,
  LetterMastersCount int(11) DEFAULT 0,
  LetterFormCount int(11) DEFAULT 0,
  LetterFormPlusCount int(11) DEFAULT 0,
  LetterFormDupeCount int(11) DEFAULT 0,
  CONSTRAINT PkCommentPhases_Id PRIMARY KEY (Id),
  CONSTRAINT FkCommentPhases_CaraId FOREIGN KEY (CaraId) REFERENCES CARAProjects(Id),
  CONSTRAINT UnCommentPhases_CaraId_PhaseId UNIQUE (CaraId, PhaseId)
) ENGINE=INNODB;

-- CARA phase documents
CREATE TABLE CARAPhaseDocs (		-- Added for CARA
  Id int(11) NOT NULL AUTO_INCREMENT,
  CommentPhaseId int(11) NOT NULL,	-- FK to CommentPhases
  ProjectDocumentId int(11) NOT NULL,	-- FK to ProjectDocuments
  CONSTRAINT PkCARAPhaseDocs_Id PRIMARY KEY (Id),
  CONSTRAINT FkCARAPhaseDocs_CommentPhaseId FOREIGN KEY (CommentPhaseId) REFERENCES CommentPhases(Id),
  CONSTRAINT FkCARAPhaseDocs_ProjectDocumentId FOREIGN KEY (ProjectDocumentId) REFERENCES ProjectDocuments(Id)
) ENGINE=INNODB;

-- MLM projects
CREATE TABLE MLMProjects (		-- Added for CARA
  Id varchar(50) NOT NULL,		-- GovDelivery MLM ID
  ProjectId int(11) NOT NULL,		-- FK to Projects
  CONSTRAINT PkMLMProjects_Id PRIMARY KEY (Id),
  CONSTRAINT FkMLMProjects_ProjectId FOREIGN KEY (ProjectId) REFERENCES Projects(Id),
  CONSTRAINT UnMLMProjects_ProjectId UNIQUE (ProjectId)
) ENGINE=INNODB;

CREATE TABLE RefConfigs (
  Id varchar(50) NOT NULL,
  Value varchar(255) NOT NULL,
  CONSTRAINT PkRefConfigs_Id PRIMARY KEY (Id)
) ENGINE=INNODB;

CREATE TABLE ProjectKMLs (
  Id varchar(255) NOT NULL,	-- Filename
  ProjectId int(11) NOT NULL,
  Label varchar(255),
  MapType varchar(50),
  CONSTRAINT PkProjectKMLs_Id PRIMARY KEY (Id),
  CONSTRAINT FkProjectKMLs_ProjectId FOREIGN KEY (ProjectId) REFERENCES Projects (Id)
) ENGINE=INNODB;

CREATE TABLE UnitKMLs (
  Id varchar(255) NOT NULL,	-- Filename
  UnitId varchar(255) NOT NULL,
  Label varchar(255),
  MapType varchar(50),
  CONSTRAINT PkUnitKMLs_Id PRIMARY KEY (Id),
  CONSTRAINT FkUnitKMLs_UnitId FOREIGN KEY (UnitId) REFERENCES RefUnits (Id)
) ENGINE=INNODB;

CREATE TABLE Goals ( -- Added for PALS 3.3
  Id int(11) NOT NULL,	-- goalid
  Description varchar(1024),
  CONSTRAINT PkGoals_Id PRIMARY KEY (Id)
) ENGINE=INNODB;

CREATE TABLE ResourceAreas ( -- Added for PALS 3.3
  Id int(11) NOT NULL,	-- resourceareaid
  UnitId varchar(255) NOT NULL,	-- forest unit
  Description varchar(1024),
  CONSTRAINT PkResourceAreas_Id PRIMARY KEY (Id),
  CONSTRAINT FkResourceAreas_UnitId FOREIGN KEY (UnitId) REFERENCES RefUnits (Id)
) ENGINE=INNODB;

CREATE TABLE ProjectGoals ( -- Added for PALS 3.3
  Id int(11) NOT NULL AUTO_INCREMENT,
  ProjectId int(11) NOT NULL,
  GoalId int(11) NOT NULL,
  OrderTag int(11),	-- display order
  CONSTRAINT PkProjectGoals_Id PRIMARY KEY (Id),
  CONSTRAINT FkProjectGoals_ProjectId FOREIGN KEY (ProjectId) REFERENCES Projects (Id),
  CONSTRAINT FkProjectGoals_GoalId FOREIGN KEY (GoalId) REFERENCES Goals (Id),
  CONSTRAINT UnProjectGoals_ProjectId_GoalId UNIQUE (ProjectId, GoalId)
) ENGINE=INNODB;

CREATE TABLE UnitGoals ( -- Added for PALS 3.3
  Id int(11) NOT NULL AUTO_INCREMENT,
  UnitId varchar(255) NOT NULL,	-- forest unit
  GoalId int(11) NOT NULL,
  OrderTag int(11),	-- display order
  CONSTRAINT PkUnitGoals_Id PRIMARY KEY (Id),
  CONSTRAINT FkUnitGoals_UnitId FOREIGN KEY (UnitId) REFERENCES RefUnits (Id),
  CONSTRAINT FkUnitGoals_GoalId FOREIGN KEY (GoalId) REFERENCES Goals (Id),
  CONSTRAINT UnUnitGoals_UnitId_GoalId UNIQUE (UnitId, GoalId)
) ENGINE=INNODB;

CREATE TABLE ProjectResourceAreas ( -- Added for PALS 3.3
  Id int(11) NOT NULL AUTO_INCREMENT,
  ProjectId int(11) NOT NULL,
  ResourceAreaId int(11) NOT NULL,
  OrderTag int(11),	-- display order
  CONSTRAINT PkProjectResourceAreas_Id PRIMARY KEY (Id),
  CONSTRAINT FkProjectResourceAreas_ProjectId FOREIGN KEY (ProjectId) REFERENCES Projects (Id),
  CONSTRAINT FkProjectResourceAreas_ResourceAreaId FOREIGN KEY (ResourceAreaId) REFERENCES ResourceAreas (Id),
  CONSTRAINT UnProjectResourceAreas_ProjectId_ResourceAreaId UNIQUE (ProjectId, ResourceAreaId)
) ENGINE=INNODB;

CREATE TABLE DecisionMakers (	-- Added for PALS 4.1
  Id int(11) NOT NULL,		-- From Decision Maker Id
  DecisionId int(11) NOT NULL,      -- FK to Decisions
  Name varchar(255),
  Title varchar(255),
  DecMakeSignedDate DATETIME DEFAULT NULL,
  CONSTRAINT PkDecisionMakers_Id PRIMARY KEY (Id),
  CONSTRAINT FkDecisionMakers_DecisionId FOREIGN KEY (DecisionId) REFERENCES Decisions(Id)
) ENGINE=INNODB;

-- Web Projects			-- Added for PALS 4.3
CREATE TABLE CustomProjects (
  Id int(11) NOT NULL,	
  Name varchar(255) NOT NULL,
  Type varchar(255),
  URL varchar(255),
  IsPublished tinyint(1),
  ArchiveDate DATETIME DEFAULT NULL,
  Description varchar(4000),
  StatusId int(11),
  UnitId int(11),
  CreatedBy varchar(255),
  CreatedDate DATETIME DEFAULT NULL,
  CONSTRAINT PkCustomProjects_Id PRIMARY KEY (Id)
) ENGINE=INNODB;

-- proj_purp_list
-- joins table CustomProjects with table RefPurposes -- Added for PALS 4.3
CREATE TABLE CustomProjectPurposes (
  Id int(11) NOT NULL AUTO_INCREMENT,
  CustomProjectId int(11) NOT NULL,
  PurposeId varchar(2) NOT NULL,
  CONSTRAINT PkCustomProjectPurposes_Id PRIMARY KEY (Id),
  CONSTRAINT FkCustomProjectPurposes_CustomProjectId FOREIGN KEY (CustomProjectId) REFERENCES CustomProjects (Id),
  CONSTRAINT FkCustomProjectPurposes_PurposeId FOREIGN KEY (PurposeId) REFERENCES RefPurposes (Id)
) ENGINE=INNODB;

show tables;
