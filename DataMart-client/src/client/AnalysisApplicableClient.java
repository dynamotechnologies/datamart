package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.analysisapplicable.*;


public class AnalysisApplicableClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public AnalysisApplicableClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.analysisapplicable", "AnalysisApplicable.xsd");
    }


    public void postAnalysisApplicable(Integer id, String analysistypeid, Integer applicableregid,
            String commentperioddays, String objectionperioddays, Boolean active)
            throws IOException
    {
        Analysisapplicable aa = new Analysisapplicable();
        aa.setId(id);
        aa.setAnalysistypeid(analysistypeid);
        aa.setApplicableregid(applicableregid);
        aa.setCommentperioddays(commentperioddays);
        aa.setObjectionperioddays(objectionperioddays);
        aa.setActive(active);
        postAnalysisApplicable(aa);
    }


    public void postAnalysisApplicable(Analysisapplicable aa)
            throws IOException
    {
        post(objectfactory.createAnalysisapplicable(aa), "/ref/analysisapplicables");
    }


    public Analysisapplicable getAnalysisApplicable(Integer id)
            throws IOException
    {
        return get("/ref/analysisapplicables/" + id, Analysisapplicable.class);
    }


    public Analysisapplicables getAnalysisApplicableList()
            throws IOException
    {
        return get("/ref/analysisapplicables", Analysisapplicables.class);
    }


    public void putAnalysisApplicable(Integer id, String analysistypeid, Integer applicableregid,
            String commentperioddays, String objectionperioddays, Boolean active)
            throws IOException
    {
        Analysisapplicable aa = new Analysisapplicable();
        aa.setId(id);
        aa.setAnalysistypeid(analysistypeid);
        aa.setApplicableregid(applicableregid);
        aa.setCommentperioddays(commentperioddays);
        aa.setObjectionperioddays(objectionperioddays);
        aa.setActive(active);
        putAnalysisApplicable(aa);
    }


    public void putAnalysisApplicable(Analysisapplicable aa)
            throws IOException
    {
        put(objectfactory.createAnalysisapplicable(aa), "/ref/analysisapplicables/" + aa.getId());
    }


    public void deleteAnalysisApplicable(Integer id)
            throws IOException
    {
        delete("/ref/analysisapplicables/" + id);
    }
}