package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.projectkml.*;


public class ProjectKMLClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public ProjectKMLClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.projectkml", "ProjectKML.xsd");
    }


    public void postProjectKML(String filename, String label, String maptype, String projectid, String projecttype)
            throws IOException
    {
        Projectkml Projectkml = new Projectkml();

        Projectkml.setFilename(filename);
        Projectkml.setLabel(label);
        Projectkml.setMaptype(maptype);
        Projectkml.setProjectid(projectid);
        Projectkml.setProjecttype(projecttype);

        postProjectKML(Projectkml);
    }


    public void postProjectKML(Projectkml Projectkml)
            throws IOException
    {
        post(objectfactory.createProjectkml(Projectkml), "/projectkmls");
    }


    public Projectkml getProjectKML(String filename)
            throws IOException
    {
        return get("/projectkmls/" + filename, Projectkml.class);
    }



    public void putProjectKML(String filename, String label, String maptype, String projectid, String projecttype)
            throws IOException
    {
        Projectkml config = new Projectkml();

        config.setFilename(filename);
        config.setLabel(label);
        config.setMaptype(maptype);
        config.setProjectid(projectid);
        config.setProjecttype(projecttype);
        putProjectKML(config);
    }


    public void putProjectKML(Projectkml config)
            throws IOException
    {
        put(objectfactory.createProjectkml(config), "/projectkmls/" + config.getFilename());
    }


    public void deleteProjectKML(String filename)
            throws IOException
    {
        delete("/projectkmls/" + filename);
    }
}