package test.relationship;

import client.MLMProjectClient;
import client.ProjectClient;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.project.*;
import us.fed.fs.www.nepa.schema.mlmproject.*;

public class MlmProjectTest {
    private MLMProjectClient client;
    private ProjectClient projclient;

    @Before
    public void setUp()
            throws IOException
    {
         client = new MLMProjectClient();
         projclient = new ProjectClient();
    }

    @Test
    public void testLink()
    {
        link();

        // Try to relink the same MLM project
        try {
            // second link request should fail
            client.createMlmProject(TestData.mlmprojects.id1, TestData.projects.type1, TestData.projects.id1);
            Assert.fail();
        } catch (Exception ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
            {
                Assert.fail();
            }
        }

        // Make sure the Project now has a <link> tag for the MLM project
        try {
            Project p = projclient.getProject(TestData.projects.type1, TestData.projects.id1);
            if (findLink(p, "mlmproject") == null) {
                Assert.fail("Missing MLM link in Project record!");
            }
        } catch (IOException ex) {
            Logger.getLogger(MlmProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        unlink();
    }


    @Test
    public void testGetAllByUnit()
    {
        link();

        try {
            Mlmprojects mplist = client.getAllByUnit(TestData.projects.unitcode1);
            int numrecs = mplist.getMlmproject().size();
            if (numrecs == 0) {
                Assert.fail("Was expecting at least 1 MLM Project record for unit " +
                        TestData.projects.unitcode1 + ", found none");
            }
        } catch (IOException ex) {
            Logger.getLogger(MlmProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        unlink();
    }


    @Test
    public void testGetAllByForestmatch()
    {
        link();

        try {
            // unitcode1 just happens to be a forest unit
            Mlmprojects mplist = client.getAll(null, TestData.projects.unitcode1);
            int numrecs = mplist.getMlmproject().size();
            if (numrecs == 0) {
                Assert.fail("Was expecting at least 1 MLM Project record for unit " +
                        TestData.projects.unitcode1 + ", found none");
            }
        } catch (IOException ex) {
            Logger.getLogger(MlmProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        unlink();
    }


    @Test
    public void testUnlink()
    {
        link();
        unlink();

        // Try to unlink the same project again
        try {
            // second unlink request should fail
            client.deleteMlmProject(TestData.mlmprojects.id1);
            Assert.fail();
        } catch (Exception ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
            {
                Assert.fail();
            }
        }

        // Make sure the project no longer has an MLM <link> tag
        try {
            Project p = projclient.getProject(TestData.projects.type1, TestData.projects.id1);
            // Projects now have an empty <links> tag by default
            if (findLink(p, "mlmproject") != null) {
                Assert.fail("Unlinked Project has unexpected <link> tags!");
            }
        } catch (IOException ex) {
            Logger.getLogger(MlmProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void link()
    {
        try {
            client.createMlmProject(TestData.mlmprojects.id1, TestData.projects.type1, TestData.projects.id1);
        } catch (Exception ex) {
            Logger.getLogger(MlmProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void unlink()
    {
        try {
            client.deleteMlmProject(TestData.mlmprojects.id1);
        } catch (Exception ex) {
            Logger.getLogger(CaraProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }
    
    /*
     *   Does this project have a link of the given type?
     *   Returns link if found
     *   Returns null if not found
     */
    private static Link findLink(Project p, String linktype) {
        if (p.getLinks() != null) {
            for (Link link : p.getLinks().getLink()) {
                if (link.getRel().matches(linktype)) {
                    return link;
                }
            }
        }
        return null;
    }

    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteMlmProject(TestData.mlmprojects.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }


    @BeforeClass
    public static void  populateDependencies()
    {
        ProjectClient pc = null;
        try {
            pc = new ProjectClient();
        } catch (Exception ex) {
            Logger.getLogger(MlmProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Project States rely on Projects. Pre-populate a Project.
        ArrayList<String> purposeids1 = new ArrayList<String>();
        purposeids1.add(TestData.projects.purposeid1);
        purposeids1.add(TestData.projects.purposeid2);
        purposeids1.add(TestData.projects.purposeid3);
        try {
            pc.postProject(TestData.projects.id1, TestData.projects.type1,
                    TestData.projects.name1, TestData.projects.unitcode1, TestData.projects.description1,
                    TestData.projects.lastupdate1, TestData.projects.commentreg1, TestData.projects.wwwlink1,
                    TestData.projects.wwwsummary1, TestData.projects.wwwpub1, TestData.projects.analysistypeid1,
                    TestData.projects.statusid1, TestData.projects.contactname1, TestData.projects.contactphone1,
                    TestData.projects.contactemail1, purposeids1,
                    TestData.projects.expirationdate1, TestData.projects.projectdocumentid1,
                    TestData.projects.sopapub, TestData.projects.sopanew, TestData.projects.sopaheader, TestData.projects.esd, TestData.projects.cenodecision);
 
        } catch (IOException ex) {
            Logger.getLogger(MlmProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        try {
            Project p = pc.getProject(TestData.projects.type1, TestData.projects.id1);
            if (findLink(p, "mlmproject") != null) {
                Assert.fail("Non-linked Project has unexpected <link> tags!");
            }
        } catch (IOException ex) {
            Logger.getLogger(MlmProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @AfterClass
    public static void  cleanupDependencies()
    {
        ProjectClient pc = null;
        try {
            pc = new ProjectClient();
        } catch (Exception ex) {
            Assert.fail();
        }

        // Clean up the Project
        try {
            pc.deleteProject("nepa", TestData.projects.id1);
        } catch (IOException ex) {
            Logger.getLogger(MlmProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }
}