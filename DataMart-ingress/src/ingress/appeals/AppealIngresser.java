package ingress.appeals;

import client.AppealClient;
import client.ProjectDocumentClient;
import ingress.LegacyUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.zip.DataFormatException;
import org.w3c.dom.Document;

public class AppealIngresser {
    private AppealClient client;
    private ProjectDocumentClient docclient;

    public AppealIngresser()
            throws IOException
    {
         client = new AppealClient();
         docclient = new ProjectDocumentClient();
    }

    public void ingressAppeals(Document doc)
            throws IOException, DataFormatException
    {
//        System.out.print("Processing Appeals...");
        client = new AppealClient();

        LegacyAppealList appeallist = new LegacyAppealList(doc);

        String timestamp = appeallist.getTimeStamp();
        Iterator it = appeallist.getAppeals().iterator();
        while (it.hasNext())
        {
            LegacyAppeal appeal = (LegacyAppeal) it.next();
            String code = appeal.getCode();

            String projectname = appeal.getProject().getName();
            String projecttype = "nepa";
       //     String projectid = appeal.getProjectId();
            


  //          String outcomename = appeal.getOutcome();
   //         String outcomeid = LegacyUtil.getOutcomeIdFromName(outcomename);

            String dismissalid = "";
            String statusid = "";
   //         if (LegacyUtil.isDismissalReason(outcomename))
   //         {
    //            dismissalid = LegacyUtil.getDismissalIdFromOutcomeId(outcomeid);
    //            statusid="2";
   //         } else {
   //             dismissalid = "0";
   //             statusid = "4";
   //         }





       //     if (!(projectid.length()>0))
            {
                projecttype = "appeal";
          //      projectid = appeal.getId();
                ArrayList<String> purposelist = new ArrayList<String>();
                purposelist.add("NA");
                client.ProjectClient projectclient = new client.ProjectClient();
                try {

          //          projectclient.getProject(projecttype, projectid);
                    // project already exists. update it
      //              projectclient.putProject(projectid, projecttype, projectname, code, "n/a",
      //                  "1901-01-01T00:00:00", "1", "n/a", "n/a", "1", "NA", statusid, "N/A", "N/A", "N/A", purposelist);
                } catch (Exception ex) {
                    // project does not exist. create it.
     //               projectclient.postProject(projectid, projecttype, projectname, code, "n/a",
        //                "1901-01-01T00:00:00", "1", "n/a", "n/a", "1", "NA", statusid, "N/A", "N/A", "N/A", purposelist);
                }
            }

        //    String responsedate = LegacyUtil.getDateString(appeal.getResponse().getDate());
            String appellant = appeal.getAppellant();
            if (!(appellant.length()>0))
                appellant = "n/a";

            try {
      //          docclient.getProjectDocument(projecttype, projectid, appeal.getResponse().getDocumentId());
                // doc exists; update it.
//                docclient.putProjectDocument(projectid, projecttype, appeal.getResponse().getDocumentId(), "1",
//            "", "", "Administrative Review Response - "+appeal.getAppellant(), appeal.getResponse().getFileSize());

            } catch (Exception ex) {
     //           docclient.postProjectDocument(projectid, projecttype, appeal.getResponse().getDocumentId(), "1",
     //       "", appeal.getResponse().getDocumentId(), "Administrative Review Response - "+appeal.getAppellant(), appeal.getResponse().getFileSize());
            }

            try {
                client.getAppeal(appeal.getId());
                // appeal exists. update it.
        //        client.putAppeal(appeal.getId(), projectid, projecttype, appellant, outcomeid,
    //                appeal.getResponse().getDocumentId(), dismissalid, responsedate, appeal.getRule(), statusid);
            } catch (Exception ex) {
                try {
       //             client.postAppeal(appeal.getId(), projectid, projecttype, appellant, outcomeid,
         //               appeal.getResponse().getDocumentId(), dismissalid, responsedate, appeal.getRule(), statusid);
                } catch (Exception ex2) {
          //          System.out.println("Appeal ingress failed. \n\n"+"appealid="+appeal.getId()+", projectid="+projectid+", appellant="+appellant+", outcomeid="+outcomeid+", docid="+appeal.getResponse().getDocumentId()+", dismissalid="+dismissalid+", responsedate="+responsedate+", Rule="+appeal.getRule()+", statusid="+statusid+".\n\n"+ex2.getMessage()+"\n"+ex.getMessage()+"\n\n\n");

                }
            }
        }
    }
}

// TODO: compare values to see if any changes have occurred. If not, don't do a PUT.