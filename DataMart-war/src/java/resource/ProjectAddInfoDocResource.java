/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resource;


import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.ProjectAddInfoDocManagerRemote;
import us.fed.fs.www.nepa.schema.ProjectAddInfoDoc.ObjectFactory;
import us.fed.fs.www.nepa.schema.ProjectAddInfoDoc.ProjectAddInfoDoc;
import us.fed.fs.www.nepa.schema.ProjectAddInfoDoc.ProjectAddInfoDocs;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;



@Path ("/projects/{projtype}/{projid}/projectAdditionainfo/docs")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class ProjectAddInfoDocResource extends AbstractResource
{
    private ProjectAddInfoDocManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public ProjectAddInfoDocResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "ProjectAddInfoDoc.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.ProjectAddInfoDoc");
        MY_PROPS.setProperty("bean", "ProjectAddInfoDocManagerRemote");
        try {
            beanmanager = (ProjectAddInfoDocManagerRemote) init(ProjectAddInfoDocManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("ProjectAddInfoDocResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("ProjectAddInfoDocResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(@PathParam("projtype") String projtype, @PathParam("projid") String projid, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;

        try {
            ProjectAddInfoDoc projectAddInfoDoc = (ProjectAddInfoDoc)unmarshal(inputxml, ProjectAddInfoDoc.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+projectAddInfoDoc.getAddInfoDocId());

           
        
            // All clear. Now do the insert.
            beanmanager.create(projectAddInfoDoc);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the newly created entity
        return created(uri);
    }


    @GET
    @Path("/{addInfoDocId}")
    public Response get(@PathParam("projtype") String projtype, @PathParam("projid") String projid,  @PathParam("addInfoDocId") Integer addInfoDocId, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            ProjectAddInfoDoc projectAddInfoDoc = beanmanager.get(addInfoDocId.toString());

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<ProjectAddInfoDoc> jaxbelement = objectfactory.createProjectAddInfoDoc(projectAddInfoDoc);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    /*
     *  Get list of all project Meetingnotices under this project
     */
    @GET
    public Response getAllByProject(@PathParam("projtype") String projtype, @PathParam("projid") String projid,@Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the name of the specified unit
            ProjectAddInfoDocs projectAddInfoDocs = beanmanager.getAllByProject(projtype, projid);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<ProjectAddInfoDocs> jaxbelement = objectfactory.createProjectAddInfoDocs(projectAddInfoDocs);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }

    @PUT
    @Path("/{addInfoDocId}")
    public Response put(@PathParam("projtype") String projtype, @PathParam("projid") String projid, @PathParam("addInfoDocId") Integer addInfoDocId, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        ProjectAddInfoDoc projectAddInfoDoc = null;
        try {
            projectAddInfoDoc = (ProjectAddInfoDoc) unmarshal(inputxml, ProjectAddInfoDoc.class);

          
           
             if (!addInfoDocId.toString().equals(projectAddInfoDoc.getAddInfoDocId()))
                complainLoudly(new Exception("addInfoDocId in uri must match ID in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.update(projectAddInfoDoc);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"</location>");
    }


    @DELETE
    @Path("/{addInfoDocId}")
    public Response delete(@PathParam("projtype") String projtype, @PathParam("projid") String projid, @PathParam("addInfoDocId") Integer addInfoDocId,  @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(addInfoDocId.toString());
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}
