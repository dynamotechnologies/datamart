package stateless;

import javax.ejb.Stateless;
import beanutil.*;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import us.fed.fs.www.nepa.schema.config.*;

@Stateless(mappedName="ConfigManagerRemote")
public class ConfigManager implements ConfigManagerRemote {

    @PersistenceContext
    private EntityManager em;

    /* Define names for well-known configs here */
    static public final String DATAMART_URL_PREFIX = "DATAMART_URL_PREFIX";
    static public final String EDGESERVER_URL_PREFIX = "EDGESERVER_URL_PREFIX";

    @Override
    public void create(Config resource)
             throws DataMartException
    {
        validate(resource);
        entity.RefConfigs entity = createEntity(resource);

        entity.RefConfigs badentity = createEntity(entity.getId());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        entity = em.merge(entity);
    }

    @Override
    public Config get(String key)
             throws DataMartException
    {
        entity.RefConfigs entity = createEntity(key);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return createResource(entity);
    }

    @Override
    public Configs getAll()
             throws DataMartException
    {
        // Retrieve all entities from the database
        Query query = em.createQuery("SELECT u FROM RefConfigs u");
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Configs responselist = new Configs();
        while (it.hasNext()) {
            responselist.getConfig().add(createResource( (entity.RefConfigs)it.next() ));
        }
        return responselist;
    }

    @Override
    public void update(Config resource)
             throws DataMartException
    {
        validate(resource);
        entity.RefConfigs entity = createEntity(resource);

        if (createEntity(entity.getId())==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        entity = em.merge(entity);
    }

    @Override
    public void delete(String key)
             throws DataMartException
    {
        entity.RefConfigs entity = createEntity(key);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     *
     * This method does the mapping from jaxb object to entity object and vice versa
     */
    private Config createResource(entity.RefConfigs entity)
            throws DataMartException
    {
        Config resource = new Config();
        resource.setKey(entity.getId());
        resource.setValue(entity.getValue());
        return resource;
    }

    private entity.RefConfigs createEntity(Config resource)
            throws DataMartException
    {
        entity.RefConfigs entity = new entity.RefConfigs();
        entity.setId(resource.getKey());
        entity.setValue(resource.getValue());
        return entity;
    }

    private entity.RefConfigs createEntity(String key)
            throws DataMartException
    {
        return em.find(entity.RefConfigs.class, key);
    }

    private void validate(Config resource)
            throws DataMartException
    {
        DataMartBeanUtil.confirmNotNull(resource.getKey(), "Key");
    }

}