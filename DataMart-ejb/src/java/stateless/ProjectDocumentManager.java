package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.projectdocument.*;

import beanutil.*;
import javax.ejb.Stateless;
import beanutil.DataMartException;
import entity.CommentPhases;
import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


// ProjectDocuments rows are uniquely identified by ProjectID and DocumentId, where
//   DocumentId is the DMD ID of the document and ProjectID is the internal unique key of the
//   corresponding row in table Projects
//
// We have access to the projecttype and project-publicid, which uniquely identify the Project
//   We do not have direct access to the ProjectID itself.
//
// So on insert, it's necessary to look up ProjectID based on projecttype and Public ID.
//
@Stateless(mappedName="ProjectDocumentManagerRemote")
public class ProjectDocumentManager implements ProjectDocumentManagerRemote
{

    @PersistenceContext
    private EntityManager em;
    private String URLPrefix = "";


    @Override
    public void create(Projectdocument resource)
             throws DataMartException
    {
        if (resource.getPubflag() == null) {
            resource.setPubflag("0");
        }
        if (resource.getSensitiveflag() == null) {
            resource.setSensitiveflag("0");
        }
        entity.ProjectDocuments entity = createEntity(resource);

        // Check for existing entity with this projectid and DocId
        entity.ProjectDocuments badentity = getEntity(entity.getProjects(), entity.getDocumentId());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);
        
        
        java.util.Date currentDateTime= new java.util.Date();
        entity.setDateAdded(currentDateTime);
        if (entity.getIsPublished() == true){
            entity.setDatePublished(currentDateTime);
        }
        em.persist(entity);
                // Get a reference to the project
        entity.Projects projectEntity = DataMartBeanUtil.getProject(em, resource.getProjecttype(), resource.getProjectid());
        projectEntity.setLastUpdate(currentDateTime);
        em.persist(projectEntity);
    }

    /*
     * Get a single project document record
     */
    @Override
    public Projectdocument get(String type, String projectid, String docid)
            throws DataMartException
    {
        entity.Projects project = DataMartBeanUtil.getProject(em, type, projectid);
        if (project==null)
            /*
             *
             * TODO: Is this (RESOURCE_NOT_FOUND) the appropriate way to respond when a dependent object
             *    (In this case the project) is not found? This should be decided and made
             *     consistent throughout all manager beans.
             */
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND, "Project '"+projectid+"' with type '"+type+"' does not exist.");

        this.URLPrefix = DataMartBeanUtil.getConfig(em, ConfigManager.DATAMART_URL_PREFIX);

        entity.ProjectDocuments entity = getEntity(project, docid);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        Projectdocument rsrc = createResource(entity);
        rsrc = attachCaraPhaseLinks(rsrc, entity);
        return rsrc;
    }


    /*
     * Get all documents associated with a Project (type/projectid)
     *   pubflag and sensitiveflag may be null.
     */
    @Override
    public Projectdocuments getAll(String type, String projectid, Boolean pubflag, Boolean sensitiveflag, Boolean ignorecaraflag, Integer start, Integer rows, Integer viewId)
            throws DataMartException
    {
        entity.Projects project = DataMartBeanUtil.getProject(em, type, projectid);
        if (project==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND, "Project '"+projectid+"' with type '"+type+"' does not exist.");

        this.URLPrefix = DataMartBeanUtil.getConfig(em, ConfigManager.DATAMART_URL_PREFIX);

        if (rows != null) {
            rows = rows + 1;    // fetch an extra row to see if there's another page
        }
        List entitylist = getDocs(project, pubflag, sensitiveflag, ignorecaraflag, rows, start,viewId);

        Boolean morerows = false;
        if (rows != null) {
            rows = rows - 1;    // reset rows to original value
            if (entitylist.size() > rows) {
                morerows = true;
                entitylist.remove(rows.intValue());    // drop last value
            }
        }
        // Loop over the query results and build a list of resources
        Projectdocuments doclist = new Projectdocuments();
        for (Object pdoc : entitylist) {
            Projectdocument rsrc = createResource((entity.ProjectDocuments)pdoc);
            doclist.getProjectdocument().add(rsrc);
            //doclist.getProjectdocument().add(attachCaraPhaseLinks(rsrc, (entity.ProjectDocuments)pdoc));
        }
        massAttachCaraPhaseLinks(doclist, project);
        if (morerows) {
            String prefix = this.URLPrefix;
            Integer startval = 0;
            if (start != null) {
                startval = start;
            }
            startval += rows;

            us.fed.fs.www.nepa.schema.projectdocument.Link link = new us.fed.fs.www.nepa.schema.projectdocument.Link();
            String href = prefix + "/projects/" + type + "/" + projectid + "/docs?";
            href += "rows=" + rows + "&start=" + startval;
            if (pubflag != null) {
                href += "&pubflag=" + (pubflag ? "1" : "0");
            }
            if (sensitiveflag != null) {
                href += "&sensitiveflag=" + (sensitiveflag ? "1" : "0");
            }

            link.setHref(href);
            link.setRel("next");

            us.fed.fs.www.nepa.schema.projectdocument.Links links = new us.fed.fs.www.nepa.schema.projectdocument.Links();
            links.getLink().add(link);
            doclist.setLinks(links);
        }
        return doclist;
    }
    
    @Override
    public Projectdocuments getByDocument(String type, String docid) throws DataMartException
    {
        
        // Get projectDocuments for this document
        /*
         * The sql for this join would be pretty straightforward, but it's a bit more convoluted in jpql.
         * Instead the following performs two queries and combines the results.
         */
        Query query1 = em.createQuery("SELECT p FROM ProjectDocuments p WHERE p.documentId LIKE :id");
         query1.setParameter("id", docid+"%");
        List entitylist1 =  query1.getResultList();

        // Loop over the database collection. Each row becomes a new object in the response list
        Projectdocuments responselist = new Projectdocuments();
        for (Object e : entitylist1) {
            entity.ProjectDocuments entity = (entity.ProjectDocuments)e;
            Projectdocument p = ProjectDocumentManager.createResource(entity);
           // attachLinks(entity, p);
            responselist.getProjectdocument().add(p);
        }

        return responselist;
        

        
    }
    
    @Override
    public void update(Projectdocument resource)
             throws DataMartException
    {
        String docid = resource.getDocid();
        String publicid = resource.getProjectid();
        String type = resource.getProjecttype();
        
        // Get reference to the project
        entity.Projects project = DataMartBeanUtil.getProject(em, type, publicid);
        if (project==null)
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION,
                    "Project with type '"+type+"' and project id '"+publicid+"' does not exist.");

        // Query for the (hopefully) existing document entity. Complain if not found.
        entity.ProjectDocuments existingentity = getEntity(project, docid);
        if (existingentity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Could not update nonexistent ProjectDocument '"+docid+"' with type '"+type+"' and project id '"+publicid+"'.");

        java.util.Date currentDateTime= new java.util.Date();
        // Copy old values for any missing optional fields in resource
        if (resource.getPubflag() == null) {
            resource.setPubflag(DataMartBeanUtil.getStringFromBool(existingentity.getIsPublished()));
        }else {
            boolean isPublished = DataMartBeanUtil.getBoolFromString(resource.getPubflag(), "pubflag");
            
            //If document going from unpublished to published, set DatePublished timestamp
            if (isPublished && !existingentity.getIsPublished() && existingentity.getDatePublished() == null){   
                resource.setDatepublished(DataMartBeanUtil.getXMLGregorianFromDateTime(currentDateTime));
            }else {
                resource.setDatepublished(DataMartBeanUtil.getXMLGregorianFromDateTime(existingentity.getDatePublished()));
            }
        }
        if (resource.getSensitiveflag() == null) {
            resource.setSensitiveflag(DataMartBeanUtil.getStringFromBool(existingentity.getIsSensitive()));
        }

        // Create new entity from resource
        entity.ProjectDocuments entity = createEntity(resource);

        // Set the database id (not the public id). This is necessary because the ID
        //   is not passed to us by the resource.
        entity.setId(existingentity.getId());
        entity.setProjectDocumentContainers(existingentity.getProjectDocumentContainers());
        entity.setOrderTag(existingentity.getOrderTag());
        entity.setDateAdded(existingentity.getDateAdded());
        // Update the entity
        entity = em.merge(entity);
        
        //Apply timestamp to project
        project.setLastUpdate(currentDateTime);
        em.persist(project);
    }


    @Override
    public void delete(String type, String projectid, String docid)
             throws DataMartException
    {
        entity.Projects project = DataMartBeanUtil.getProject(em, type, projectid);
        if (project==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND, "Project '"+projectid+"' with type '"+type+"' does not exist.");

        // Verify that this entity exists
        entity.ProjectDocuments entity = getEntity(project, docid);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Could not delete nonexistent ProjectDocument '"+docid+"' with type '"+type+"' and project id '"+projectid+"'.");

        // Verify that document is not in a comment phase
        Query query = em.createQuery("SELECT COUNT(c) FROM CARAPhaseDocs c WHERE c.projectDocuments = :doc");
        query.setParameter("doc", entity);
        Long count = (Long)query.getSingleResult();
        if (count > 0) {
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION,
                "Cannot delete ProjectDocument "+docid+" with type "+type+" and project id "+projectid+", doc is in a comment phase");
        }

        // Remove the entity itself
        em.remove(entity);
        
        //Apply timestamp to project
        project.setLastUpdate(new java.util.Date() );
        em.persist(project);
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     *
     * This method does the mapping from entity object to jaxb object
     */
    protected static Projectdocument createResource(entity.ProjectDocuments entity)
            throws DataMartException
    {
        String published = DataMartBeanUtil.getStringFromBool(entity.getIsPublished());
        String sensitive = DataMartBeanUtil.getStringFromBool(entity.getIsSensitive());

        Projectdocument resource = new Projectdocument();
        resource.setDescription(entity.getDescription());
        resource.setDocid(entity.getDocumentId());
        resource.setDocname(entity.getLabel());
        resource.setPdffilesize(entity.getPdfSize());
        resource.setProjectid(entity.getProjects().getPublicId());
        resource.setProjecttype(entity.getProjects().getProjectTypes().getId());
        resource.setPubflag(published);
        resource.setWwwlink(entity.getLink());
        resource.setSensitiveflag(sensitive);
        resource.setAddressee(entity.getAddressee());
        resource.setAuthor(entity.getAuthor());
        resource.setDocdate(entity.getDocumentDate());
        //resource.setOrdertag(entity.getOrderTag().toString());
        resource.setOrdertag(null !=entity.getOrderTag()? ""+entity.getOrderTag().intValue():"0");
        resource.setPhaseid(null !=entity.getPhaseId()?""+entity.getPhaseId().intValue():"");
        //entity.getProjectDocumentContainers().ge
       //resource.setDocOrder(entity.getOrderTag().toString());
        try
        {
          resource.setContorder(Integer.toString(entity.getProjectDocumentContainers().getOrderTag()));
          resource.setContid(entity.getProjectDocumentContainers().getPalsContId().toString());
        }catch(Exception e){}
        

        resource.setDatelabel(entity.getDateLabel());
        if (entity.getSensitivityRationale() != null){
            resource.setSensitivityrationale(entity.getSensitivityRationale().getId().toString());
        }else{
            
        }
        if (entity.getDatePublished()!= null){
            resource.setDatepublished(DataMartBeanUtil.getXMLGregorianFromDateTime(entity.getDatePublished()));
        }

        if (entity.getDateAdded()!= null){
            resource.setDateadded(DataMartBeanUtil.getXMLGregorianFromDateTime(entity.getDateAdded()));
        }
        resource.setAddedby(entity.getAddedBy());
        
        return resource;
    }

    /*
     * Create <link> tags for every CARA phase that links to this document
     */
    private Projectdocument attachCaraPhaseLinks(Projectdocument resource, entity.ProjectDocuments doc)
            throws DataMartException
    {
        String prefix = this.URLPrefix;

        Query query = em.createQuery("SELECT p FROM CommentPhases p, CARAPhaseDocs c WHERE c.commentPhases = p AND c.projectDocuments = :doc");
        query.setParameter("doc", doc);
        @SuppressWarnings("unchecked")
        List<entity.CommentPhases> entitylist = (List<entity.CommentPhases>)query.getResultList();
        for (entity.CommentPhases p : entitylist) {
            us.fed.fs.www.nepa.schema.projectdocument.Link link = new us.fed.fs.www.nepa.schema.projectdocument.Link();
            int caraid = p.getCARAProjects().getId();
            link.setHref(prefix + "/caraprojects/" + caraid + "/comment/phases/" + p.getPhaseId());
            link.setRel("caraphase");
            link.setTitle(p.getName());

            us.fed.fs.www.nepa.schema.projectdocument.Links links = resource.getLinks();
            if (links == null) {
                links = new us.fed.fs.www.nepa.schema.projectdocument.Links();
                resource.setLinks(links);
            }

            links.getLink().add(link);
            resource.setLinks(links);
        }

        return resource;
    }

    /*
     * Create CARA <link> tags for all documents in list
     * Separate from the single-document attachCaraPhaseLinks() function for efficiency
     */
    private void massAttachCaraPhaseLinks(Projectdocuments doclist, entity.Projects project)
            throws DataMartException
    {
        String prefix = this.URLPrefix;

        // Create a map from project document to list of comment phases

        // Map from phase IDs to Phases
        Query query = em.createQuery("SELECT p FROM CommentPhases p WHERE p.cARAProjects.projects = :project");
        query.setParameter("project", project);
        @SuppressWarnings("unchecked")
        List<entity.CommentPhases> phaselist = (List<entity.CommentPhases>)query.getResultList();
        HashMap<Integer, entity.CommentPhases> phasemap = new HashMap<Integer, entity.CommentPhases>();
        for (entity.CommentPhases p : phaselist) {
            phasemap.put(p.getId(), p);
        }

        // Map from docids to Comments (phase docs), which should have comment phase IDs
        query = em.createQuery("SELECT d FROM CARAPhaseDocs d JOIN FETCH d.commentPhases WHERE d.projectDocuments.projects = :project");
        query.setParameter("project", project);
        @SuppressWarnings("unchecked")
        List<entity.CARAPhaseDocs> commentlist = (List<entity.CARAPhaseDocs>)query.getResultList();
        HashMap<String, List<entity.CommentPhases>> commentmap = new HashMap<String, List<entity.CommentPhases>>();
        for (entity.CARAPhaseDocs c : commentlist) {
            entity.ProjectDocuments d = c.getProjectDocuments();
            entity.CommentPhases commentphase = c.getCommentPhases();
            List<entity.CommentPhases> commentphaselist = commentmap.get(d.getDocumentId());
            if (commentphaselist == null) {
                commentphaselist = new ArrayList<entity.CommentPhases>();
                commentmap.put(d.getDocumentId(), commentphaselist);
            }
            commentphaselist.add(commentphase);
        }

        // List of phases = commentmap.get(doc)
        // Phase data = phasemap[phase.getid()]
        // List of phases may be null if document is not a comment

        for (Projectdocument d : doclist.getProjectdocument()) {
            List<entity.CommentPhases> phases = commentmap.get(d.getDocid());
            if (phases == null) {
                continue;
            }
            for (entity.CommentPhases phase : phases) {
                us.fed.fs.www.nepa.schema.projectdocument.Link link = new us.fed.fs.www.nepa.schema.projectdocument.Link();
                link.setHref(prefix + "/caraprojects/" + phase.getCARAProjects().getId() + "/comment/phases/" + phase.getPhaseId());
                link.setRel("caraphase");
                link.setTitle(phase.getName());

                us.fed.fs.www.nepa.schema.projectdocument.Links links = d.getLinks();
                if (links == null) {
                    links = new us.fed.fs.www.nepa.schema.projectdocument.Links();
                    d.setLinks(links);
                }

                links.getLink().add(link);
                d.setLinks(links);
            }
        }
    }

    /*
     *
     * This method does the mapping from jaxb object to entity object
     */
    private entity.ProjectDocuments createEntity(Projectdocument resource)
            throws DataMartException
    {
        String type = resource.getProjecttype();
        String projectid = resource.getProjectid(); // Always reference the public ID in error messages
        Boolean published = DataMartBeanUtil.getBoolFromString(resource.getPubflag(), "Publish Flag");
        Boolean sensitive = DataMartBeanUtil.getBoolFromString(resource.getSensitiveflag(), "Sensitive Flag");

        // The following fields are optional in the schema, but must be set by the resource class.
        //   We don't need to verify the presence of required fields here, because they've already
        //   been validated by the JAXB unmarshaller.
        /*
         * TODO: Go through all manager beans and confirm that all pseudo-optional fields are
         *     being validated with confirmNotNull(), and that all actually-optional fields are not.
         */
        DataMartBeanUtil.confirmNotNull(type, "Type");

        entity.Projects project = DataMartBeanUtil.getProject(em, type, projectid);
        if (project==null)
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION, 
                    "Project with type '"+type+"' and project id '"+projectid+"' does not exist.");

        String strRefSensitivityId= resource.getSensitivityrationale();
        entity.RefDocumentSensitivityRationales rationale = null;
        if (strRefSensitivityId != null && strRefSensitivityId.length() > 0){
            Integer refSensitivityId = new Integer(strRefSensitivityId);
            rationale = (entity.RefDocumentSensitivityRationales) DataMartBeanUtil.getEntityById(em,
                "RefDocumentSensitivityRationales", refSensitivityId, "Sensitivity ID");
        }

        /*
         *
         * Note: The following is terribly confusing, because `entity` is both a variable name
         *       and a package name.
         */
        entity.ProjectDocuments entity = new entity.ProjectDocuments();
        entity.setDescription(resource.getDescription());
        entity.setDocumentId(resource.getDocid());
        entity.setIsPublished(published);
        entity.setLabel(resource.getDocname());
        entity.setLink(resource.getWwwlink());
        entity.setPdfSize(resource.getPdffilesize());
        entity.setProjects(project);
        entity.setIsSensitive(sensitive);
        entity.setAddressee(resource.getAddressee());
        entity.setAuthor(resource.getAuthor());
        entity.setDateLabel(resource.getDatelabel());
        entity.setDocumentDate(resource.getDocdate());
        entity.setAddedBy(resource.getAddedby());
        if(null!=resource.getPhaseid() && !resource.getPhaseid().equals("")&& !resource.getPhaseid().equals("null"))
        entity.setPhaseId(new Integer(resource.getPhaseid()));
        if (resource.getDateadded()!= null){
            entity.setDateAdded(DataMartBeanUtil.getDateFromXMLGregorian(resource.getDateadded()));

        }

        if (rationale != null){
            entity.setSensitivityRationale(rationale);
        }
        if (resource.getDatepublished()!=null){
            entity.setDatePublished(DataMartBeanUtil.getDateFromXMLGregorian(resource.getDatepublished()));
        }
        return entity;
    }


    // Accepts a String ID as a parameter, and returns the
    //   associated entity, or null if it doesn't exist
    //
    private entity.ProjectDocuments getEntity(entity.Projects project, String docid)
            throws DataMartException
    {
        Query query = em.createQuery("SELECT p FROM ProjectDocuments p WHERE p.projects = :projects and p.documentId = :documentId");
        query.setParameter("projects", project);
        query.setParameter("documentId", docid);
        entity.ProjectDocuments doc = (entity.ProjectDocuments) DataMartBeanUtil.getEntityByQueryAllowNull(query,
                "More than one ProjectDocuments found with Project ID '"+project.getPublicId()+"' and DocID '"+docid+"'");

        return doc;
    }


    /*
     * Get list of project documents
     * Canonical order is by documentid
     */
    private List getDocs(entity.Projects project, Boolean pubflag, Boolean sensitiveflag, Boolean ignorecaraflag, Integer rows, Integer start, Integer viewId)
            throws DataMartException
    {
        // This shouldn't have been hard.  It was.
        // We started with JPQL query that looked roughly like
        //
        // SELECT p FROM ProjectDocuments p WHERE p.projects = :projects AND p.projectDocumentContainers.palsContId = '57'
        //
        // there are two problems with this
        // 1) If the document doesn't have a container than p.projectDocumentContainers.palsContId will be null
        //  and null != null is not true.  That's easy to fix.
        // 2) p.projectDocumentContainers triggers a join accorting to the JPA relations defined.  This join
        //  will *always* be a inner join regardless of if the foriegn key column is nullable or any other
        //  relation defined.  This is annoying because the entire point of JPA is to define the relationships
        //  at the entity level and have the queries do the right thing for those relationships.  There are
        //  some implementation specific annotations that promise to do they don't seem to exist in the
        //  (admittidely creaky) version that we are using.
        //
        // However you are supposed to be able to explicitly do a LEFT JOIN in JPQL.  Which isn't as elegant, but
        // isn't hard either
        //
        // SELECT p FROM ProjectDocuments p LEFT JOIN p.projectDocumentContainers pdc
        //  WHERE p.projects = :projects AND (p.projectDocumentContainers IS NULL OR pdc.palsContId = '57')
        //
        // This works after a fashion in that the query is accepted as valid by JPA and converted into a sql query.
        // However that sql query will still use an inner join.  There is no explanation as to why anyone thought
        // that was a good idea.
        //
        // So we fall back to writing a MySQL query directly and hooking it up to JPA.  Which has its own
        // weirdness and oddities, such as manually doing somethings that JPA should handle automatically
        // (and make sure are robust over schmema/entity changes -- or at least caught by the compiler.
        //
        // *** THIS WILL ALMOST CERTAINLY BREAK IF THE ProjectDocuments CHANGES ***
        
        List<String> joins = new ArrayList<String>();
        List<String> filters = new ArrayList<String>();
        Map<Integer, Object> params = new HashMap<Integer, Object>();

        //note use the ordinal parameter method because named parameters don't work for native queries.
        //however it doesn't seem to have a problem if the numbers aren't squential or in a particular order
        //(they look sequentional, but depending on how we are called not every parameter actually makes
        //it into the query)
        joins.add("ProjectDocuments pd");
        filters.add("pd.ProjectId = ?1");
        params.put(1, project.getId());
        
       

        if (pubflag != null) {
            filters.add("pd.isPublished = ?2");
            params.put(2, pubflag);
        }

        if (sensitiveflag != null)
        {
            filters.add("pd.isSensitive = ?3");
            params.put(3, sensitiveflag);
        }

        if (ignorecaraflag != null)
        {
            //handling nulls in sql costs SAN points.  Basically palsContId will be null when the
            //document does not have a containter (due to the LEFT JOIN logic).
            //null != 57 isn't true, its null.  Since true AND null is also null and a record will
            //be reject if the WHERE clause is null, without the IS NULL part this will skip
            //records without containers despite the left join.
            //true or null, however is true so the filter works as written -- this definitely works
            //with mysql and appears to be standard sql but should be evaluated before moving
            //to a different DBMS
            filters.add("(pdc.palsContId IS NULL OR pdc.palsContId != 57)");
            joins.add("LEFT JOIN ProjectDocumentContainers pdc ON (pd.ContainerId = pdc.Id)");
        }
        
         if(null != viewId)
        {
          joins.add(" ,ProjectViewDocs vd");
          filters.add("vd.viewId=?4 AND vd.docId = pd.id");
          params.put(4, viewId);
        }

        String sort = "pd.DocumentId ASC";

        //**THIS NEED TO CONTAIN ALL OF THE FIELDS FROM THE ProjectDocuments TABLE**
        //this is going to be fragile.  JPA requires, for reasons that are not clear, that the
        //field names be in upper case in the returned query (it does *not* do this for the internal
        //queries it generates for its own use nor does there seems to be a way to get the fields
        //expected from the entity object).  We could potentially grab the fields from the table,
        //but that requires some dancing to both get the result set and to avoid calling for it
        //every time we run this query.  So we'll live with hardcoding the values.
        String[] fields = {
            "Id", "AddedBy", "Addressee", "Author", "DateAdded", "DateLabel", "DatePublished",
            "Description", "DocumentDate", "DocumentId", "isPublished", "isSensitive", 
            "Label", "Link", "OrderTag", "PdfSize", "ContainerId", "ProjectId", 
            "SensitivityRationale"
        };

        //stitch the query from the components -- this should need to change to change the
        //query (assuming we don't add clauses etc);
        StringBuilder sql = new StringBuilder();

        sql.append("SELECT ");
        String joiner = ", ";
        for(String field : fields)
        {
            //some versions of Weblogic demand upper case field names.  Some demand lower.
            //The ways are mysterious and the errors returned cryptic.  There does not appear to
            //be any way to actually discern what is expected via the API, so we resort to providing
            //both in the query and hoping that *that* doesn't cause something somewhere to fail.
            //this works both my dev and test so it should work in all the places we need it to.
            sql.append("pd.").append(field).append(" AS ").append(field.toUpperCase()).append(joiner);
            sql.append("pd.").append(field).append(" AS ").append(field).append(joiner);
        }
        sql.delete(sql.length() - joiner.length(), sql.length());

        sql.append(" FROM ");
        joiner = " ";
        for(String join : joins)
        {
            sql.append(join).append(joiner);
        }
        sql.delete(sql.length() - joiner.length(), sql.length());

        sql.append(" WHERE ");
        joiner = " AND ";
        for(String filter : filters)
        {
            sql.append(filter).append(joiner);
        }
        sql.delete(sql.length() - joiner.length(), sql.length());

        sql.append(" ORDER BY ").append(sort);

        Query query = em.createNativeQuery(sql.toString(), entity.ProjectDocuments.class);
        for (Map.Entry<Integer, Object> entry : params.entrySet())
        {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        if (rows != null) {
            query.setMaxResults(rows);
        }

        if (start != null) {
            query.setFirstResult(start);
        }

        // run the query and return results
        List results = query.getResultList();
        return results;
    }

}
