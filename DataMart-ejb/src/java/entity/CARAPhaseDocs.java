package entity;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CARAPhaseDocs")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CARAPhaseDocs.findAll", query = "SELECT c FROM CARAPhaseDocs c"),
    @NamedQuery(name = "CARAPhaseDocs.findById", query = "SELECT c FROM CARAPhaseDocs c WHERE c.id = :id")})
public class CARAPhaseDocs implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @JoinColumn(name = "ProjectDocumentId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private ProjectDocuments projectDocuments;
    @JoinColumn(name = "CommentPhaseId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private CommentPhases commentPhases;

    public CARAPhaseDocs() {
    }

    public CARAPhaseDocs(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ProjectDocuments getProjectDocuments() {
        return projectDocuments;
    }

    public void setProjectDocuments(ProjectDocuments projectDocuments) {
        this.projectDocuments = projectDocuments;
    }

    public CommentPhases getCommentPhases() {
        return commentPhases;
    }

    public void setCommentPhases(CommentPhases commentPhases) {
        this.commentPhases = commentPhases;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CARAPhaseDocs)) {
            return false;
        }
        CARAPhaseDocs other = (CARAPhaseDocs) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CARAPhaseDocs[ id=" + id + " ]";
    }

}
