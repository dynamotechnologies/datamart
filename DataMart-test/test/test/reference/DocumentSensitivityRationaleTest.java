package test.reference;

import client.DocumentSensitivityRationaleClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.documentsensitivityrationale.*;


public class DocumentSensitivityRationaleTest {
    private DocumentSensitivityRationaleClient client;
    Documentsensitivityrationale rationale1;
    Documentsensitivityrationale rationale2;

    @Before
    public void setUp()
            throws IOException
    {
        client = new DocumentSensitivityRationaleClient();

        rationale1 = new Documentsensitivityrationale();
        rationale1.setId(TestData.documentsensitivityrationales.id1);
        rationale1.setSensitivityid(TestData.documentsensitivityrationales.sensitivity1);
        rationale1.setDescription(TestData.documentsensitivityrationales.desc1);
        rationale1.setDisplayorder(TestData.documentsensitivityrationales.order1);

        rationale2 = new Documentsensitivityrationale();
        rationale2.setId(TestData.documentsensitivityrationales.id1);
        rationale2.setSensitivityid(TestData.documentsensitivityrationales.sensitivity2);
        rationale2.setDescription(TestData.documentsensitivityrationales.desc2);
        rationale2.setDisplayorder(TestData.documentsensitivityrationales.order2);
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postDocumentSensitivityRationale(rationale1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Documentsensitivityrationale r = null;
        Documentsensitivityrationales rlist = null;
        try {
            r = client.getDocumentSensitivityRationale(new Integer(TestData.documentsensitivityrationales.id1).toString());
            rlist = client.getDocumentSensitivityRationaleList();
        } catch (IOException ex) {
            Logger.getLogger(DocumentSensitivityRationaleTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.documentsensitivityrationales.id1, r.getId());
        Assert.assertEquals(TestData.documentsensitivityrationales.desc1, r.getDescription());
        Assert.assertEquals(TestData.documentsensitivityrationales.sensitivity1, r.getSensitivityid());
        Assert.assertEquals(TestData.documentsensitivityrationales.order1, r.getDisplayorder());
        Assert.assertTrue(rlist.getDocumentsensitivityrationale().get(0).getId()>0);
        Assert.assertTrue(rlist.getDocumentsensitivityrationale().get(0).getDescription().length()>0);

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        Documentsensitivityrationale r = null;
        try {
            client.putDocumentSensitivityRationale(rationale2);
            r = client.getDocumentSensitivityRationale(new Integer(TestData.documentsensitivityrationales.id1).toString());
        } catch (Exception ex) {
            Logger.getLogger(DocumentSensitivityRationaleTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.documentsensitivityrationales.desc2, r.getDescription());

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getDocumentSensitivityRationale(new Integer(TestData.documentsensitivityrationales.id1).toString());
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteDocumentSensitivityRationale(new Integer(TestData.documentsensitivityrationales.id1).toString());
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }

    private void post()
    {
        try {
            client.postDocumentSensitivityRationale(rationale1);
        } catch (IOException ex) {
            Logger.getLogger(DocumentSensitivityRationaleTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteDocumentSensitivityRationale(new Integer(TestData.documentsensitivityrationales.id1).toString());
        } catch (IOException ex) {
            Logger.getLogger(CategoricalExclusionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteDocumentSensitivityRationale(new Integer(TestData.documentsensitivityrationales.id1).toString());
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }
}