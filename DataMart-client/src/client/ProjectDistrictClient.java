package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.projectlocation.*;


public class ProjectDistrictClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public ProjectDistrictClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.projectlocation", "ProjectLocation.xsd");
    }


    public Districts getProjectDistrict(String projectid, String id)
            throws IOException
    {
        return get("/projects/nepa/" + projectid + "/locations/districts/" + id, Districts.class);
    }


    public void putProjectDistrict(String projectid, String id)
            throws IOException
    {
        Districts district = new Districts();
        district.getDistrictid().add(id);
        put(objectfactory.createDistricts(district), "/projects/nepa/" + projectid + "/locations/districts/" + id);
    }


    public void deleteProjectDistrict(String projectid, String id)
            throws IOException
    {
        delete("/projects/nepa/" + projectid + "/locations/districts/" + id);
    }
}