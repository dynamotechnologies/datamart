-- Categorical Exclusion No Decision (cenodecision) flag for Projects
ALTER TABLE Projects ADD COLUMN Cenodecision tinyint(1) NOT NULL DEFAULT 0;

