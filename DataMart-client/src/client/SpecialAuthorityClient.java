package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.specialauthority.*;


public class SpecialAuthorityClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public SpecialAuthorityClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.specialauthority", "SpecialAuthority.xsd");
    }


    public void postSpecialAuthority(String id, String name, String description)
            throws IOException
    {
        Specialauthority auth = new Specialauthority();
        auth.setId(id);
        auth.setName(name);
        auth.setDescription(description);
        postSpecialAuthority(auth);
    }


    public void postSpecialAuthority(Specialauthority auth)
            throws IOException
    {
        post(objectfactory.createSpecialauthority(auth), "/ref/specialauthorities");
    }


    public Specialauthority getSpecialAuthority(String id)
            throws IOException
    {
        return get("/ref/specialauthorities/" + id, Specialauthority.class);
    }


    public Specialauthorities getSpecialAuthorityList()
            throws IOException
    {
        return get("/ref/specialauthorities", Specialauthorities.class);
    }


    public void putSpecialAuthority(String id, String name, String description)
            throws IOException
    {
        Specialauthority auth = new Specialauthority();
        auth.setId(id);
        auth.setName(name);
        auth.setDescription(description);
        putSpecialAuthority(auth);
    }


    public void putSpecialAuthority(Specialauthority auth)
            throws IOException
    {
        put(objectfactory.createSpecialauthority(auth), "/ref/specialauthorities/" + auth.getId());
    }


    public void deleteSpecialAuthority(String id)
            throws IOException
    {
        delete("/ref/specialauthorities/" + id);
    }
}