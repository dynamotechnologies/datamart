package entity;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "UnitGoals")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UnitGoals.findAll", query = "SELECT u FROM UnitGoals u"),
    @NamedQuery(name = "UnitGoals.findById", query = "SELECT u FROM UnitGoals u WHERE u.id = :id"),
    @NamedQuery(name = "UnitGoals.findByOrderTag", query = "SELECT u FROM UnitGoals u WHERE u.orderTag = :orderTag")})
public class UnitGoals implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Column(name = "OrderTag")
    private Integer orderTag;
    @JoinColumn(name = "UnitId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private RefUnits refUnits;
    @JoinColumn(name = "GoalId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Goals goals;

    public UnitGoals() {
    }

    public UnitGoals(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOrderTag() {
        return orderTag;
    }

    public void setOrderTag(Integer orderTag) {
        this.orderTag = orderTag;
    }

    public RefUnits getRefUnits() {
        return refUnits;
    }

    public void setRefUnits(RefUnits refUnits) {
        this.refUnits = refUnits;
    }

    public Goals getGoals() {
        return goals;
    }

    public void setGoals(Goals goals) {
        this.goals = goals;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UnitGoals)) {
            return false;
        }
        UnitGoals other = (UnitGoals) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.UnitGoals[ id=" + id + " ]";
    }

}
