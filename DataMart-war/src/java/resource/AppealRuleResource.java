package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.AppealRuleManagerRemote;
import us.fed.fs.www.nepa.schema.appealrule.Appealrule;
import us.fed.fs.www.nepa.schema.appealrule.Appealrules;
import us.fed.fs.www.nepa.schema.appealrule.ObjectFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;


@Path ("/ref/appealrules")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class AppealRuleResource extends AbstractResource
{
    private AppealRuleManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public AppealRuleResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "AppealRule.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.appealrule");
        MY_PROPS.setProperty("bean", "AppealRuleManagerRemote");
        try {
            beanmanager = (AppealRuleManagerRemote) init(AppealRuleManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("AppealRuleResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("AppealRuleResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;
        try {
            Appealrule appealrule = (Appealrule)unmarshal(inputxml, Appealrule.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+appealrule.getId());

            // All clear. Now do the insert.
            beanmanager.create(appealrule);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        // Return a link to the newly created entity
        return created(uri);
    }


    @GET
    public Response get(@Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get a dump of all the AppealRules in the datamart
            Appealrules appealrulelist = beanmanager.getAll();

            // Return 404 if none exist
            if (appealrulelist.getAppealrule().isEmpty())
                notFound();

            // Get the query results in a JAXBElement<AppealRules> object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Appealrules> jaxbelement = objectfactory.createAppealrules(appealrulelist);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }

    
    @GET
    @Path("/{id}")
    public Response get(@PathParam("id") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the details of the specified rule
            String rule = beanmanager.getRule(id);
            String description = beanmanager.getDescription(id);
            Appealrule appealrule = new Appealrule();
            appealrule.setId(id);
            appealrule.setRule(rule);
            appealrule.setDescription(description);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Appealrule> jaxbelement = objectfactory.createAppealrule(appealrule);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @PUT
    @Path("/{id}")
    public Response put(@PathParam("id") String id, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        Appealrule appealrule = null;
        try {
            appealrule = (Appealrule) unmarshal(inputxml, Appealrule.class);

            if (!id.equalsIgnoreCase(appealrule.getId()))
                complainLoudly(new Exception("Id in uri must match id in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.update(appealrule);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"/"+appealrule.getId()+"</location>");
    }


    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(id);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}
