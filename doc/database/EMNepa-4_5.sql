-- PRM Updates


-- Fields for Document Metadata originally stored in UCM
ALTER TABLE ProjectDocuments ADD COLUMN DocumentDate varchar(50);
ALTER TABLE ProjectDocuments ADD COLUMN Author varchar(255);
ALTER TABLE ProjectDocuments ADD COLUMN Addressee varchar(255);
ALTER TABLE ProjectDocuments ADD COLUMN DateLabel varchar(255);
ALTER TABLE ProjectDocuments add column SensitivityRationale int(11);
ALTER TABLE ProjectDocuments ADD COLUMN DateAdded datetime;
ALTER TABLE ProjectDocuments ADD COLUMN AddedBy varchar(255);
ALTER TABLE ProjectDocuments ADD CONSTRAINT FkProjectDocuments_SensitivityRationale FOREIGN KEY (SensitivityRationale) REFERENCES RefDocumentSensitivityRationales (Id);

ALTER TABLE ProjectDocumentContainers ADD COLUMN FixedFlag tinyint(1) NOT NULL DEFAULT 0;

CREATE TABLE UserSession (
  Token varchar(255) NOT NULL,
  UserId varchar(255) NOT NULL,	-- Shortname
  ProjectId int(11) NOT NULL,
  Created datetime NOT NULL,
  LastActivity datetime NOT NULL,
  CONSTRAINT PkUserSession_Token PRIMARY KEY (Token),
  CONSTRAINT FkUserSession_UserId FOREIGN KEY (UserId) REFERENCES Users (Id),
  CONSTRAINT FkUserSession_ProjectId FOREIGN KEY (ProjectId) REFERENCES Projects (Id)
) ENGINE=INNODB;


-- Updates for CARA
ALTER TABLE Projects ADD COLUMN PrimaryProjectManager varchar(255);
ALTER TABLE Projects ADD COLUMN SecondaryProjectManager varchar(255);
ALTER TABLE Projects ADD COLUMN DataEntryPerson varchar(255);
ALTER TABLE Projects ADD COLUMN ContactShortName varchar(255);

ALTER TABLE Objections ADD COLUMN ObjectionRule varchar(255);
