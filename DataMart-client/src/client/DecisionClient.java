package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.decision.Decision;
import us.fed.fs.www.nepa.schema.decision.Decisions;
import us.fed.fs.www.nepa.schema.decision.ObjectFactory;


public class DecisionClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public DecisionClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.decision", "Decision.xsd");
    }

    @Deprecated
    public void postDecision(Integer id,
            String projectid,
            String projecttype,
            String name,
            String appealstatus,
            String constraint,
            String legalnoticedate)
                throws IOException
    {
        postDecision(id,
            projectid,
            projecttype,
            name,
            appealstatus,
            constraint,
            legalnoticedate,
            null,
            null,
            "",
            "");
    }
    
    
    public void postDecision(Integer id,
            String projectid,
            String projecttype,
            String name,
            String appealstatus,
            String constraint,
            String legalnoticedate,
            Double areasize,
            String areaunits,
            String dectype,
            String decdate)
                throws IOException
    {
        Decision d = new Decision();
        d.setId(id);
        d.setProjectid(projectid);
        d.setProjecttype(projecttype);
        d.setName(name);
        d.setAppealstatus(appealstatus);
        d.setConstraint(constraint);
        d.setLegalnoticedate(dateStrToXML(legalnoticedate));
        d.setAreasize(areasize);
        d.setAreaunits(areaunits);
        d.setDectype(dectype);
        d.setDecdate(dateStrToXML(decdate));
        
        postDecision(d);
    }


    public void postDecision(Decision d)
            throws IOException
    {
        post(objectfactory.createDecision(d), "/decisions");
    }


    public Decision getDecision(Integer id)
            throws IOException
    {
        return get("/decisions/" + id, Decision.class);
    }


    public Decisions getDecisionList(String projectid, String projecttype)
            throws IOException
    {
        return get("/projects/" + projecttype + "/" + projectid + "/decisions/", Decisions.class);
    }


    public Decisions getByUnit(String unitcode)
            throws IOException
    {
        return get("/units/" + unitcode + "/decisions/", Decisions.class);
    }


    @Deprecated
    public void putDecision(Integer id,
            String projectid,
            String projecttype,
            String name,
            String appealstatus,
            String constraint,
            String legalnoticedate)
                throws IOException
    {
        putDecision(id,
            projectid,
            projecttype,
            name,
            appealstatus,
            constraint,
            legalnoticedate,
            null,
            null,
            "",
            "");
    }
    
    
    public void putDecision(Integer id,
            String projectid,
            String projecttype,
            String name,
            String appealstatus,
            String constraint,
            String legalnoticedate,
            Double areasize,
            String areaunits,
            String dectype,
            String decdate)
                throws IOException
    {
        Decision d = new Decision();
        d.setId(id);
        d.setProjectid(projectid);
        d.setProjecttype(projecttype);
        d.setName(name);
        d.setAppealstatus(appealstatus);
        d.setConstraint(constraint);
        d.setLegalnoticedate(dateStrToXML(legalnoticedate));
        d.setAreasize(areasize);
        d.setAreaunits(areaunits);
        d.setDectype(dectype);
        d.setDecdate(dateStrToXML(decdate));
        
        putDecision(d);
    }
    

    public void putDecision(Decision d)
            throws IOException
    {
        put(objectfactory.createDecision(d), "/decisions/" + d.getId());
    }


    public void deleteDecision(Integer id)
            throws IOException
    {
        delete("/decisions/" + id);
    }
}