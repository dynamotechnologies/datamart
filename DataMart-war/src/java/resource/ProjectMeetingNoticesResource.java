/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resource;

/**
 *
 * @author gauri
 */


import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.ProjectMeetingNoticesManagerRemote;
import us.fed.fs.www.nepa.schema.ProjectMeetingNotices.Meetingnotice;
import us.fed.fs.www.nepa.schema.ProjectMeetingNotices.Meetingnotices;
import us.fed.fs.www.nepa.schema.ProjectMeetingNotices.ObjectFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;


@Path ("/projects/{projtype}/{projid}/meetingnotices")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class ProjectMeetingNoticesResource extends AbstractResource
{
    private ProjectMeetingNoticesManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public ProjectMeetingNoticesResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "ProjectMeetingNotices.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.ProjectMeetingNotices");
        MY_PROPS.setProperty("bean", "ProjectMeetingNoticesManagerRemote");
        try {
            beanmanager = (ProjectMeetingNoticesManagerRemote) init(ProjectMeetingNoticesManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("ProjectMeetingNoticesResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("ProjectMeetingNoticesResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(@PathParam("projtype") String projtype, @PathParam("projid") String projid, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;

        try {
            Meetingnotice meetingnotice = (Meetingnotice)unmarshal(inputxml, Meetingnotice.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+meetingnotice.getMeetingId());

           
            if (projid.compareTo(meetingnotice.getDocumentId()) != 0)
                complainLoudly(new Exception("Project ID in uri must match ID in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the insert.
            beanmanager.create(meetingnotice);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the newly created entity
        return created(uri);
    }


    @GET
    @Path("/{meetingId}")
    public Response get(@PathParam("projtype") String projtype, @PathParam("projid") String projid, @PathParam("meetingId") Integer meetingId, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            Meetingnotice meetingnotice = beanmanager.get(projtype, projid, meetingId);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Meetingnotice> jaxbelement = objectfactory.createMeetingnotice(meetingnotice);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    /*
     *  Get list of all project Meetingnotices under this project
     */
    @GET
    public Response getAllByProject(@PathParam("projtype") String projtype, @PathParam("projid") String projid, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the name of the specified unit
            Meetingnotices meetingnotices = beanmanager.getAllByProject(projtype, projid);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Meetingnotices> jaxbelement = objectfactory.createMeetingnotices(meetingnotices);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }

    @PUT
    @Path("/{meetingId}")
    public Response put(@PathParam("projtype") String projtype, @PathParam("projid") String projid, @PathParam("meetingId") Integer meetingId, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        Meetingnotice meetingnotice = null;
        try {
            meetingnotice = (Meetingnotice) unmarshal(inputxml, Meetingnotice.class);

          
            if (projid.compareTo(meetingnotice.getDocumentId()) != 0)
                complainLoudly(new Exception("Project ID in uri must match ID in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            if (!meetingId.toString().equals(meetingnotice.getMeetingId()))
                complainLoudly(new Exception("Meetingnotice ID in uri must match ID in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.update(meetingnotice);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"</location>");
    }


    @DELETE
    @Path("/{meetingId}")
    public Response delete(@PathParam("projtype") String projtype, @PathParam("projid") String projid, @PathParam("meetingId") Integer meetingId, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(projtype, projid, meetingId);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}
