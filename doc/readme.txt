For installation instructions see install.txt




The DataMart consists of six separate Java projects:

EAR - In NetBeans this is simply called `DataMart`. It is the Enterprise Application project. Can be used to quickly deploy the EJB and WAR to the same server during development testing.
EJB - The EJB 3.0 (Manager) classes and interfaces. This project implements business logic, validation, etc. Could theoretically be accessed directly by the Client.
WAR - The web application project handles xml marshalling and unmarshalling. It is the point of interface with external applications and the implementation of the REST API spec.
Client - A reference client for accessing the REST API. This can be used directly, or as a guide, to communicate with the DataMart.
Test - System testing suite. This project relies on the Client project. It performs functional testing of the DataMart.
Ingress - The Legacy Ingress tool is responsible for the periodic ingress of legacy xml documents. It also uses the Client project to communicate with the DataMart. It has a main method and is meant to be run from the command line. The Legacy Ingress tool is deprecated, and should not be used for anything unless absolutely necessary.


This document gives detailed instructions on how to add a new resource to the DataMart. The example case is a car. The car has properties make and model which are stored in the database and exposed via the API.

The basic steps are:

1) Create an XSD schema based on the API specification
2) Generate the JAXB classes for the new resource
3) Create a client to consume the REST Resource
4) Create a test suite for the new resource
5) Create the JPA Entity classes
6) Define an interface for the manager object
7) Create the manager class
8) Create a JAX-RS Resource class to expose the manager via REST
9) Test


Before beginning, it may be helpful to map out your fields.
Create one list of fields in the API and another list of all of the fields in the database. Draw a line from each API field to its corresponding database field. 
When done, the diagram should represent what happens to your API data during PUT AND POST requests.


1) Create an XSD schema based on the API specification

The API specification is the authoritative definition of the public DataMart services. The XSD schemas are used by the DataMart application to ensure compliance with the specification. JAXB java classes are generated from the XSD schema, and these classes are used by clients and by the DataMart itself to encapsulate data for communication.

The XSD schemas 'live' in the DataMart-war project's source directory. A class called 'SchemaGetter' exposes these XSD schemas via http for direct access by client applications.

To create a new XSD schema, open the DataMart-war project and add a new empty file. Use the existing schemas as a guide with regard to syntax, and the API as a guide with regard to structure. An example schema might look like this:

<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                        targetNamespace="http://www.fs.fed.us/nepa/schema"
                                        xmlns="http://www.fs.fed.us/nepa/schema"
                                        elementFormDefault="qualified"
                                        attributeFormDefault="unqualified">
<xs:element name="cars" type="cars"/>
<xs:element name="car" type="car"/>
<xs:complexType name="car">
<xs:all>
    <xs:element name="make">
        <xs:simpleType>
            <xs:restriction base="xs:string"/>
        </xs:simpleType>
    </xs:element>
    <xs:element name="model">
        <xs:simpleType>
            <xs:restriction base="xs:string"/>
        </xs:simpleType>
    </xs:element>
</xs:all>
</xs:complexType>
<xs:complexType name="cars">
<xs:sequence maxOccurs="unbounded" minOccurs="0">
    <xs:element name="car" type="car"></xs:element>
</xs:sequence>
</xs:complexType>
</xs:schema> 



2) Generate the JAXB classes for the new resource

The JAXB generated classes are stored in the DataMart-client project. Open this project and right-click->New->JAXB Binding...

Browse to the XSD schema you created in step 1, and fill in the remaining information based on an existing binding. You can view the details of an existing binding by clicking on "Change JAXB Options" after right-clicking the Binding.
 
As an example, assuming the above schema was stored in the file Car.xsd:

	Binding Name:		Car
	Project:		Datamart-ejb
	Select From Local File System:	./Datamart-war/src/java/schema/Car.xsd
	Schema Type:		XML Schema
	Package Name:		us.fed.fs.www.nepa.schema.car

The classes will automatically generate after you click 'Finish', and you should see them under the "Generated Sources" folder. Look through the contents of these classes and understand the structure. A good understanding of how these classes work will be important later on.



3) Create a client to consume the REST Resource

In the DataMart-client project you'll see the existing client classes, e.g., UnitClient. Copy one of these and name it after your new resource, e.g. CarClient. Carefully adapt this existing client class to fit your new resource. When done you should have exposed a method for every operation that can be performed on this resource, as defined by the API.



4) Create a test suite for the new resource

It is vitally important that the new resource is tested thoroughly to ensure compliance with the API, and to ensure reliable operation of the DataMart. The tests are contained in project DataMart-test. 

The test classes themselves shouldn't contain any test data. Instead, add test data for every possible attribute of your new resource to the TestData class. Add multiple versions of test data in order to test updating (PUTting) the resource. for example:

public static final String car_make1 = "Dodge";
public static final String car_model1 = "Ram";
public static final String car_model2 = "Challenger";

You'll see the existing test classes, e.g., UnitTest. Copy one of these and name it after your resource, e.g. CarTest. Carefully adapt this existing test class to fit your new resource.

Add logic to test all elements of the request and response, and all business and validation rules that are to be enforced. Ideally the test will contain logic to detect all potential coding problems before they are committed to source control.

The PUT test for your Car resource might look like this:

@Test
public void test3_put()
{
	Car car = new Car();
	car.setMake(TestData.car_make1);
	car.setModel(TestData.car_model2);
	try {
	    client.putCar(car);
	    car = client.getCar(TestData.car_make1);
	} catch (Exception ex) {
	    Logger.getLogger(CarTest.class.getName()).log(Level.SEVERE, null, ex);
	    Assert.fail();
	}
	Assert.assertEquals(TestData.car_model2, car.getModel());
}

The primary goal of all development beyond this point is to get these tests to pass.

At this point you should build/deploy the project and run the tests. Confirm that you haven't broken any existing resources. Also take a look at what kind of failure you get from the test/client you've just created. You should see failures like this:

java.io.IOException: Status=404. POST http://localhost:7001/api/1_0/cars returned a response status of 404

You should not see errors like this:

The content of element 'car' is not complete. One of '{"http://www.fs.fed.us/nepa/schema":make}' is expected.]

The above error says that 'make' is a required field for resource 'Car', and that your test/client did not submit a value.



5) Create the JPA Entity classes

These are plain java classes and compose the interface between the java application and its data store (Oracle DB for now).

There are a number of ways to go about doing this:
- Manually create the class from scratch
- Use the NetBeans default template available via the wizard
- Copy an existing entity and modify it to suit
- Create the database table manually, and then generate the JPA class via the NetBeans wizard. 

I prefer the last option because it quickly gets something resembling your table into NetBeans, without a lot of fuss.
If using mysql on your development machine, the sql create script might look like this:

CREATE TABLE ref_cars (
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	make VARCHAR(255),
	model VARCHAR(255)
);

Having created the database table above manually, open up NetBeans and the DataMart-ejb project. 
- Expand Source-Packages
- Right-click `entity` and select New->Other... 
- Select Persistence->Entity Classes From Database
- Add your table and select Next
- Fill in any desired options and click next until the JPA class is generated. 

NetBeans will generate a lot of code that you might not want or need. You can go through the generated class and delete/modify as desired.

Note: The 'doc' folder contains mysql table scripts. They are here for historical purposes only, and may not reflect the most recent data model. The JPA entity classes in the EJB project reflect the most recent data model. That said, it may be a good idea to make an attempt to keep these two representations in synch.



6) Define an interface for the manager object

For example:

@Remote
public interface CarManagerRemote {
    public void build(Car) throws DataMartException;
    public Carlist getAll() throws DataMartException;
    public String getMake(String id) throws DataMartException;
    public String getModel(String id) throws DataMartException;
    public Boolean ping();
}

You will notice the following from the above example:
- The interfaces are EJB 3.0 Remote Interfaces
- All methods should throw DataMartException
- Every interface should have a `ping` method, which always returns true
- The ID parameters for GET methods are Strings. Number format conversions (and all other conversions) should take place within the manager bean itself --- not in the JAX-RS resource. Coversion exceptions should be thrown by the manager, which is uniquely capable of generating informative debugging information about the problem.
- Methods that modify the resource accept a single parameter (Car in this case) matching the JAXB-generated class we created earlier
- Methods returning a list of resources use the JAXB-generated list class. In this case it's called Carlist.



7) Create the manager class

NOTE: After adding a new manager class and interface, you need to edit weblogic-ejb-jar to enable 'call-by-reference'. If this is not done, it will break when you push it to the production server.

- The manager class implements the remote interface we created in step 6, and is responsible for tying together the front-end resources and their back-end entities. For example there may be a back-end JPA Entity class called Car, a manager called CarManager, and a JAX-RS resource class named CarResource. The manager must perform the necessary translation between Car and CarResource. It is expected that there will often be multiple JPA entity classes tied to a single JAX-RS resource class.
- The manager class is responsible for enforcing business rules, data validation, etc
- The java `import` keyword is used to avoid typing the full classpath, e.g. String rather than java.lang.String. A convention has developed whereby within a manager class we do not import JPA Entity classes. The primary reason is that there are often conflicting class names between entity and resource, so both cannot be imported. 
- In addition to the above rule regarding namespaces, it is also helpful to name the resource objects 'resource' and the entity objects 'entity'. There is a lot going on in the manager classes, and this convention helps clarify what's going on. For example consider the following method definition from the remote interface:

public void create(Car resource) throws DataMartException;

It might look funny here calling the car variable 'resource', but the benefits become more clear when you look at the manager code:

public void create(Car resource)
     throws DataMartException
{
	validate(resource);
	...
	entity.Car entity = createEntity(resource);
	entity = em.merge(entity);
}

While the above code may not be completely intuitive, it is much more comprehensible than it would have been if the entity and resource were named car1 and car2. This convention also allows us to use the same variable names across multiple manager classes, which makes the overall product easier to understand.



8) Create a JAX-RS Resource class to expose the manager via REST

The first time you do this, you'll probably want to start from scratch and add each line one by one, learning what everything means as you go. After you're familiar with how everything works it'll be much easier to copy an existing resource class and perform a careful/manual search-and-replace on the resource name. Then add additional resources as defined in the API.



9) Test

Run the test suite that was created in a previous step. It will probably fail gloriously. Hopefully the failing component will provide debugging information. If the failure does not provide adequate debugging information, make a note of it in the code after you track down the problem. This convention seems to work well: "TODO: Add debugging info here when x fails due to initialization failure in module y."



