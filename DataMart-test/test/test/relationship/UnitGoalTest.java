package test.relationship;

import client.UnitGoalClient;
import client.GoalClient;
import client.UnitClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import org.junit.*;
import test.TestData;
import us.fed.fs.www.nepa.schema.unitgoal.*;


public class UnitGoalTest {
    private UnitGoalClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new UnitGoalClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postUnitGoal(
                    TestData.goals.testunitid,
                    TestData.goals.id1,
                    TestData.unitgoals.order1);

            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Unitgoal projectGoal = null;
        try {
            projectGoal = client.getUnitGoal(TestData.goals.testunitid, TestData.goals.id1);
        } catch (IOException ex) {
            Logger.getLogger(UnitGoalTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        Assert.assertTrue(TestData.goals.id1 == projectGoal.getGoalid());
        Assert.assertTrue(TestData.unitgoals.order1 == projectGoal.getOrder());

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        try {
            // Update project goal, change order
            client.putUnitGoal(
                    TestData.goals.testunitid,
                    TestData.goals.id1,
                    TestData.unitgoals.order2
                    );
        } catch (IOException ex) {
            Logger.getLogger(UnitGoalTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        Unitgoal projectGoal = null;
        try {
            projectGoal = client.getUnitGoal(TestData.goals.testunitid, TestData.goals.id1);
        } catch (IOException ex) {
            Logger.getLogger(UnitGoalTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        
        Assert.assertTrue(TestData.goals.id1 == projectGoal.getGoalid());
        Assert.assertTrue(TestData.unitgoals.order2 == projectGoal.getOrder());

        delete();
    }

    @Test
    public void testGetAll()
    {
        post();
        
        Unitgoals ugs = null;
        try {
            ugs = client.getUnitGoalsByUnit(TestData.goals.testunitid);
        } catch (IOException ex) {
            Logger.getLogger(UnitGoalTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        
        Assert.assertTrue(ugs.getUnitgoal().size() == 1);
        
        Unitgoal ug = (Unitgoal)ugs.getUnitgoal().get(0);
        Assert.assertTrue(TestData.goals.id1 == ug.getGoalid());
        Assert.assertEquals(TestData.unitgoals.order1, ug.getOrder());

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getUnitGoal(TestData.goals.testunitid, TestData.goals.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteUnitGoal(TestData.goals.testunitid, TestData.goals.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }
    
    private void post()
    {
        try {
            client.postUnitGoal(
                    TestData.goals.testunitid,
                    TestData.goals.id1,
                    TestData.unitgoals.order1);
        } catch (IOException ex) {
            Logger.getLogger(UnitGoalTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteUnitGoal(TestData.goals.testunitid, TestData.goals.id1);
        } catch (IOException ex) {
            Logger.getLogger(UnitGoalTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    @After
    public void cleanFailed()
            throws Exception
    { }

    @BeforeClass
    public static void populateDependencies() throws IOException
    {
        // Create test unit
        UnitClient unitclient = new UnitClient();
        unitclient.postUnit(TestData.goals.testunitid, TestData.goals.testunitname);
        
        // Create test goal
        GoalClient gclient = new GoalClient();
        gclient.postGoal(TestData.goals.id1, TestData.goals.description1);
    }

    @AfterClass
    public static void cleanupDependencies() throws IOException
    {
        GoalClient gclient = new GoalClient();
        try {
            // Delete test goal
            gclient.deleteGoal(TestData.goals.id1);
        } catch (IOException e) {
            if (gclient.getStatus() != 404) {
                throw e;
            }
        }
        
        UnitClient unitclient = new UnitClient();
        try {
            // Delete test unit
            unitclient.deleteUnit(TestData.goals.testunitid);
        } catch (IOException e) {
            if (unitclient.getStatus() != 404) {
                throw e;
            }
        }
    }
}
