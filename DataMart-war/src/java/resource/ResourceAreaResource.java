package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.ResourceAreaManagerRemote;
import us.fed.fs.www.nepa.schema.resourcearea.ObjectFactory;
import us.fed.fs.www.nepa.schema.resourcearea.Resourcearea;
import us.fed.fs.www.nepa.schema.resourcearea.Resourceareas;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;


@Path ("/resourceareas")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class ResourceAreaResource extends AbstractResource
{
    private ResourceAreaManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public ResourceAreaResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "ResourceArea.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.resourcearea");
        MY_PROPS.setProperty("bean", "ResourceAreaManagerRemote");
        try {
            beanmanager = (ResourceAreaManagerRemote) init(ResourceAreaManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("ResourceAreaResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("ResourceAreaResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;
        try {
            Resourcearea area = (Resourcearea)unmarshal(inputxml, Resourcearea.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+area.getResourceareaid());

            // All clear. Now do the insert.
            beanmanager.create(area);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the newly created entity
        return created(uri);
    }


    @GET
    @Path("/{id}")
    public Response get(@PathParam("id") Integer id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the name of the specified unit
            Resourcearea area = beanmanager.get(id);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Resourcearea> jaxbelement = objectfactory.createResourcearea(area);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }

    @GET
    @Path("/unit/{unitid}")
    public Response getListByUnitId(@PathParam("unitid") String unitid, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the name of the specified unit
            Resourceareas areas = beanmanager.getListByUnitId(unitid);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Resourceareas> jaxbelement = objectfactory.createResourceareas(areas);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @PUT
    @Path("/{id}")
    public Response put(@PathParam("id") Integer id, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        Resourcearea area = null;
        try {
            area = (Resourcearea) unmarshal(inputxml, Resourcearea.class);

            if (id != area.getResourceareaid())
                complainLoudly(new Exception("Id in uri must match id in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.update(area);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"</location>");
    }


    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") Integer id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(id);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}
