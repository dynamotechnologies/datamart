package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "RefLitigationStatuses")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RefLitigationStatuses.findAll", query = "SELECT r FROM RefLitigationStatuses r"),
    @NamedQuery(name = "RefLitigationStatuses.findById", query = "SELECT r FROM RefLitigationStatuses r WHERE r.id = :id"),
    @NamedQuery(name = "RefLitigationStatuses.findByName", query = "SELECT r FROM RefLitigationStatuses r WHERE r.name = :name")})
public class RefLitigationStatuses implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "Name")
    private String name;
    @OneToMany(mappedBy = "refLitigationStatuses")
    private Collection<Litigations> litigationsCollection;

    public RefLitigationStatuses() {
    }

    public RefLitigationStatuses(Integer id) {
        this.id = id;
    }

    public RefLitigationStatuses(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Collection<Litigations> getLitigationsCollection() {
        return litigationsCollection;
    }

    public void setLitigationsCollection(Collection<Litigations> litigationsCollection) {
        this.litigationsCollection = litigationsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RefLitigationStatuses)) {
            return false;
        }
        RefLitigationStatuses other = (RefLitigationStatuses) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RefLitigationStatuses[ id=" + id + " ]";
    }

}
