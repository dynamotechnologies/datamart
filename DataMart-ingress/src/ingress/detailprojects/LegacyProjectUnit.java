package ingress.detailprojects;

import java.util.ArrayList;
import java.util.zip.DataFormatException;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 *
 * <!DOCTYPE unitprojectsdetail [
<!ELEMENT unitprojectsdetail (timestamp, unit*)>
<!ELEMENT timestamp (#PCDATA)>
<!ELEMENT unit (code,extendeddetails,project+)>
<!ELEMENT code (#PCDATA)>
<!ELEMENT extendeddetails (#PCDATA)>
<!ELEMENT project (id,projectdocumentid,expirationdate?,name,status,type,url?,appealslink,objectionslink,description,projectsummary,contact,catexclusion*,specauth+,milestone*,purpose,adminunit,activity+,commreg?,esd?,cenodecision?,location?,decision*,pubflag,updatedate,sopa,primaryProjectManager?,secondaryProjectManager?,dataEntryPerson?,goal*,resourcearea*)>
<!ELEMENT id (#PCDATA)>
<!ELEMENT projectdocumentid (#PCDATA)>
<!ELEMENT expirationdate (#PCDATA)>
<!ELEMENT name (#PCDATA)>
<!ELEMENT status (#PCDATA)>
<!ELEMENT type (#PCDATA)>
<!ELEMENT url (#PCDATA)>
<!ELEMENT appealslink (#PCDATA)>
<!ELEMENT objectionslink (#PCDATA)>
<!ELEMENT description (#PCDATA)>
<!ELEMENT projectsummary (#PCDATA)>
<!ELEMENT contact (contactShortName,contactname, contactphone, contactemail)>
<!ELEMENT contactShortName (#PCDATA)>
<!ELEMENT contactname (#PCDATA)>
<!ELEMENT contactphone (#PCDATA)>
<!ELEMENT contactemail (#PCDATA)>
<!ELEMENT catexclusion (#PCDATA)>
<!ELEMENT specauth (#PCDATA)>
<!ELEMENT milestone (msseq,mstype,msdate,msstatus,mshistoryflag)>
<!ELEMENT msseq (#PCDATA)>
<!ELEMENT mstype (#PCDATA)>
<!ELEMENT msdate (#PCDATA)>
<!ELEMENT msstatus (#PCDATA)>
<!ELEMENT mshistoryflag (#PCDATA)>
<!ELEMENT purpose (purposename+)>
<!ELEMENT purposename (#PCDATA)>
<!ELEMENT adminunit (#PCDATA)>
<!ELEMENT activity (#PCDATA)>
<!ELEMENT commreg (#PCDATA)>
<!ELEMENT location (locdesc,locfor+,locdst+,locstate+,loccount+,loclegal?,loclat?,loclong?)>
<!ELEMENT locdesc (#PCDATA)>
<!ELEMENT locfor (#PCDATA)>
<!ELEMENT locdst (#PCDATA)>
<!ELEMENT locstate (#PCDATA)>
<!ELEMENT loccount (#PCDATA)>
<!ELEMENT loclegal (#PCDATA)>
<!ELEMENT loclat (#PCDATA)>
<!ELEMENT loclong (#PCDATA)>
<!ELEMENT decision (decname,decisionId, dectype,decdate,decnotice?,decconstraint,decapprule,decmaker+,decappstat?,decapp*,declit*,decareasize?,decareaunits?)>
<!ELEMENT decname (#PCDATA)>
<!ELEMENT decisionId (#PCDATA)>
<!ELEMENT dectype (#PCDATA)>
<!ELEMENT decdate (#PCDATA)>
<!ELEMENT decnotice (#PCDATA)>
<!ELEMENT decconstraint (#PCDATA)>
<!ELEMENT decapprule (#PCDATA)>
<!ELEMENT decmaker (decmakerId,decmakername,decmakertitle,decmakesigneddate)>
<!ELEMENT decmakerId (#PCDATA)>
<!ELEMENT decmakername (#PCDATA)>
<!ELEMENT decmakertitle (#PCDATA)>
<!ELEMENT decmakesigneddate (#PCDATA)>
<!ELEMENT decappstat (#PCDATA)>
<!ELEMENT decapp (appealid,appealadminunitcode,rule,appellant?,outcome?,response?)>
<!ELEMENT appealid (#PCDATA)>
<!ELEMENT appealadminunitcode (#PCDATA)>
<!ELEMENT rule (#PCDATA)>
<!ELEMENT appellant (#PCDATA)>
<!ELEMENT outcome (#PCDATA)>
<!ELEMENT response (date,documentid?,appealresponsefilesize?)>
<!ELEMENT date (#PCDATA)>
<!ELEMENT documentid (#PCDATA)>
<!ELEMENT appealresponsefilesize (#PCDATA)>
<!ELEMENT declit (litid,litname,litstat,litoutcome?,litclosed?)>
<!ELEMENT litid (#PCDATA)>
<!ELEMENT litname (#PCDATA)>
<!ELEMENT litstat (#PCDATA)>
<!ELEMENT litoutcome (#PCDATA)>
<!ELEMENT litclosed (#PCDATA)>
<!ELEMENT pubflag (#PCDATA)>
<!ELEMENT updatedate (#PCDATA)>
<!ELEMENT sopa (sopaFlag,isNewProjectFlag?,sopaHeaderUnit?)>
<!ELEMENT sopaFlag (#PCDATA)>
<!ELEMENT isNewProjectFlag (#PCDATA)>
<!ELEMENT sopaHeaderUnit (#PCDATA)>
<!ELEMENT primaryProjectManager (#PCDATA)>
<!ELEMENT secondaryProjectManager (#PCDATA)>
<!ELEMENT dataEntryPerson (#PCDATA)>
<!ELEMENT goal (goalid,goalorder?)>
<!ELEMENT goalid (#PCDATA)>
<!ELEMENT goalorder (#PCDATA)>
<!ELEMENT resourcearea (resourceareaid,resourceareaorder?)>
<!ELEMENT resourceareaid (#PCDATA)>
<!ELEMENT resourceareaorder (#PCDATA)>
<!ELEMENT decareasize (#PCDATA)>
<!ELEMENT decareaunits (#PCDATA)>
<!ATTLIST unitprojects xmlns CDATA 'http://www.fs.fed.us/pals/xmlns/unitprojectsdetail/'>
]>
 *
 * This class is responsible for encapsulating element `unit` as specified above
 */
public class LegacyProjectUnit {
    private String code = "";
    private String extendeddetails = "";
    private ArrayList<LegacyProject> projects = new ArrayList<LegacyProject>();

    public LegacyProjectUnit()
    {
        // allow instantiation without nodes
    }

    public LegacyProjectUnit(Node n)
            throws DataFormatException
    {
            NodeList mynodes = n.getChildNodes();
            for (int i=0; i<mynodes.getLength(); i++)
            {
                if (mynodes.item(i).getNodeName().equals("code"))
                    code = mynodes.item(i).getTextContent();
                if (mynodes.item(i).getNodeName().equals("extendeddetails"))
                    extendeddetails = mynodes.item(i).getTextContent();
                if (mynodes.item(i).getNodeName().equals("project")) {
                    String projectId = "";
                    try {
                        Node projectNode = mynodes.item(i);
                        projectId = getProjectId(projectNode);
                        addProject(projectNode);
                    } catch (Exception ex)  {
                        System.out.println("Error "+ex.getStackTrace());
                        ex.printStackTrace(System.out);
                        ex.printStackTrace();
                        System.out.println("\n"+mynodes.item(i).getTextContent()+"\n");
                        System.out.println("Project : "+projectId);
                        System.out.println( "\nThe legacy data encapsulation has failed. " + ex.getMessage() + "\n" );
                        System.out.println(mynodes.item(i));
                    }
                }
            }
            if (code==null || !(code.length()>0))
                throw new DataFormatException("code not found.");
            if (extendeddetails==null || !(extendeddetails.length()>0))
                throw new DataFormatException("extendeddetails not found.");
            if (!(projects.size()>0))
                throw new DataFormatException("projects not found.");
    }
    
    private String getProjectId(Node projectNode) throws DataFormatException {
        
        NodeList subnodes = projectNode.getChildNodes();
        String id = "";
        for (int i=0; i<subnodes.getLength(); i++)
        {
            if (subnodes.item(i).getNodeName().equals("id"))
                id = subnodes.item(i).getTextContent();
        }
        if (id.length() == 0)
            throw new DataFormatException("Project Node missing id.");
        return id;
    }

    private void addProject(Node n)
            throws DataFormatException
    {
        projects.add(new LegacyProject(n));
    }

    public String getCode()
    {
        return code;
    }

    public String getExtendedDetails()
    {
        return extendeddetails;
    }

    public ArrayList<LegacyProject> getProjects()
    {
        return projects;
    }
}
