/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package stateless;
import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.usersession.*;
import beanutil.DataMartException;

/**
 *
 * @author mhsu
 */
@Remote
public interface UserSessionManagerRemote {
    public Usersession create(Usersession resource) throws DataMartException;
    public Usersession get(String token) throws DataMartException;
    public void update(Usersession resource) throws DataMartException;
    public void delete(String id) throws DataMartException;
    public Usersessions getAll() throws DataMartException;
    public Boolean ping();
}