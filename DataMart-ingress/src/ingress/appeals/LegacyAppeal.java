package ingress.appeals;

import ingress.LegacyUtil;
import java.util.zip.DataFormatException;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 * This class encapsulates xml ingressed from files validated with the following DTD:
 *
 * <!DOCTYPE appeals [
 * <!ELEMENT appeals (timestamp?, appeal+)>
 * <!ATTLIST appeals xmlns CDATA "http://www.fs.fed.us/pals/xmlns/appeals/">
 * <!ELEMENT timestamp (#PCDATA)>
 * <!ELEMENT appeal (id,projectid?,code,rule,appellant?,outcome,project,response)>
 * <!ELEMENT id (#PCDATA)>
 * <!ELEMENT projectid (#PCDATA)>
 * <!ELEMENT code (#PCDATA)>
 * <!ELEMENT rule (#PCDATA)>
 * <!ELEMENT appellant (#PCDATA)>
 * <!ELEMENT outcome (#PCDATA)>
 * <!ELEMENT project (name,adminunit)>
 * <!ELEMENT name (#PCDATA)>
 * <!ELEMENT adminunit (#PCDATA)>
 * <!ELEMENT response (date,documentid,filesize)>
 * <!ELEMENT date (#PCDATA)>
 * <!ELEMENT documentid (#PCDATA)>
 * <!ELEMENT filesize (#PCDATA)>
 * ]>
 *
 *
 * This class is responsible for encapsulating element `appeal` as specified above
 */
public class LegacyAppeal {
    private String id;
    private String code;
    private String rule;
    private String appellant = ""; // Optional
    private String outcomeid;
    private LegacyAppealProject project;
    private LegacyAppealResponse response;
    private String dismissalid = "0";
    private String statusid = "4";

    public LegacyAppeal(Node n)
            throws DataFormatException
    {
        NodeList mynodes = n.getChildNodes();
        for (int i=0; i<mynodes.getLength(); i++)
        {
            if (mynodes.item(i).getNodeName().equals("id"))
                id = mynodes.item(i).getTextContent();
            if (mynodes.item(i).getNodeName().equals("code"))
                code = mynodes.item(i).getTextContent();
            if (mynodes.item(i).getNodeName().equals("rule"))
                rule = mynodes.item(i).getTextContent();
            if (mynodes.item(i).getNodeName().equals("appellant"))
                appellant = mynodes.item(i).getTextContent();
            if (mynodes.item(i).getNodeName().equals("outcome"))
                setOutcome(mynodes.item(i));
            if (mynodes.item(i).getNodeName().equals("project"))
                project = new LegacyAppealProject(mynodes.item(i));
            if (mynodes.item(i).getNodeName().equals("response"))
                response = new LegacyAppealResponse(mynodes.item(i));
        }
        if (!(id.length()>0))
            throw new DataFormatException("id not found.");
        if (!(code.length()>0))
            throw new DataFormatException("code not found.");
        if (!(rule.length()>0))
            throw new DataFormatException("rule not found.");
        if (!(outcomeid.length()>0 && dismissalid.length()>0 && statusid.length()>0))
            throw new DataFormatException("outcome not found.");
        if (project==null)
            throw new DataFormatException("project not found.");
        if (response==null)
            throw new DataFormatException("response not found.");
    }

    private void setOutcome(Node n)
            throws DataFormatException
    {
        String outcomename = n.getTextContent();
        outcomeid = LegacyUtil.getOutcomeIdFromName(outcomename);
        if (LegacyUtil.isDismissalReason(outcomename))
        {
            dismissalid = LegacyUtil.getDismissalIdFromOutcomeId(outcomeid);
            statusid="2";
        }
    }

    public String getId()
    {
        return id;
    }

    public String getCode()
    {
        return code;
    }

    public String getRule()
    {
        return rule;
    }

    public String getAppellant()
    {
        return appellant;
    }

    public String getOutcomeId()
    {
        return outcomeid;
    }

    public LegacyAppealProject getProject()
    {
        return project;
    }

    public LegacyAppealResponse getResponse()
    {
        return response;
    }

    public String getStatusId()
    {
        return statusid;
    }

    public String getDismissalId()
    {
        return dismissalid;
    }
}