package client;

import java.io.IOException;
import java.util.ArrayList;
import javax.xml.datatype.XMLGregorianCalendar;
import us.fed.fs.www.nepa.schema.projectlisting.*;


public class ProjectListingClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public ProjectListingClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.projectlisting", "ProjectListing.xsd");
    }

    
    public Projectlistings getProjectListingsByUnit(String id)
            throws IOException
    {
        return get("/units/" + id + "/projectlistings", Projectlistings.class);
    }
    
    public Projectlistings getProjectSpotlightsByUnit(String id)
            throws IOException
    {
        return get("/units/" + id + "/projectspotlights", Projectlistings.class);
    }
    
}