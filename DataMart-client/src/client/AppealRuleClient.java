package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.appealrule.*;


public class AppealRuleClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public AppealRuleClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.appealrule", "AppealRule.xsd");
    }


    public void postAppealRule(String id, String rule, String description)
            throws IOException
    {
        Appealrule appealrule = new Appealrule();
        appealrule.setId(id);
        appealrule.setRule(rule);
        appealrule.setDescription(description);
        postAppealRule(appealrule);
    }


    public void postAppealRule(Appealrule appealrule)
            throws IOException
    {
        post(objectfactory.createAppealrule(appealrule), "/ref/appealrules");
    }


    public Appealrule getAppealRule(String id)
            throws IOException
    {
        return get("/ref/appealrules/" + id, Appealrule.class);
    }


    public Appealrules getAppealRuleList()
            throws IOException
    {
        return get("/ref/appealrules", Appealrules.class);
    }


    public void putAppealRule(String id, String rule, String description)
            throws IOException
    {
        Appealrule appealrule = new Appealrule();
        appealrule.setId(id);
        appealrule.setRule(rule);
        appealrule.setDescription(description);
        putAppealRule(appealrule);
    }


    public void putAppealRule(Appealrule appealrule)
            throws IOException
    {
        put(objectfactory.createAppealrule(appealrule), "/ref/appealrules/" + appealrule.getId());
    }


    public void deleteAppealRule(String id)
            throws IOException
    {
        delete("/ref/appealrules/" + id);
    }
}