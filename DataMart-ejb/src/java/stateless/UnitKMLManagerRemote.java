package stateless;

import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.unitkml.*;
import beanutil.DataMartException;

@Remote
public interface UnitKMLManagerRemote {
    public void create(Unitkml resource) throws DataMartException;
    public Unitkml get(String filename) throws DataMartException;
    public void update(Unitkml resource) throws DataMartException;
    public void delete(String filename) throws DataMartException;
    public Boolean ping();
}