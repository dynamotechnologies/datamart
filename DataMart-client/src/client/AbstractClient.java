package client;

//import com.sun.jersey.api.client.Client;
//import com.sun.jersey.api.client.ClientResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.core.Response;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.GenericType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;

public class AbstractClient
{
    protected String SERVER_PATH;
    protected ClientUtil clientutil;
    protected Marshaller marshaller;
    protected Unmarshaller unmarshaller;
    protected int httpstatus = 0;
    Client client;

    
    protected void init(String jaxb_path, String schema_file)
            throws IOException
    {
        clientutil = ClientUtil.getInstance();
        if (clientutil==null)
            throw new IOException("Could not get a reference to the ClientUtil singleton.");
        SERVER_PATH = clientutil.getServerPath();

        try {
            JAXBContext jaxbcontext = JAXBContext.newInstance(jaxb_path);
            marshaller = jaxbcontext.createMarshaller();
            unmarshaller = jaxbcontext.createUnmarshaller();
        } catch (JAXBException ex) {
            Logger.getLogger(AbstractClient.class.getName()).log(Level.SEVERE, null, ex);
            throw new IOException("Failed to initialize client.");
        }

        // Get the schema from the datamart server and tell the marshaller/unmarshaller
        //      to use it for validation
        Schema schema = clientutil.getRemoteSchema(schema_file);
        marshaller.setSchema(schema);
        unmarshaller.setSchema(schema);

        //client = Client.create();
        //client = ClientBuilder.newClient();
        //Build new client with connection timeout and read timeout settings
        ClientConfig configuration = new ClientConfig();
        configuration = configuration.property(ClientProperties.CONNECT_TIMEOUT, 3000);
        configuration = configuration.property(ClientProperties.READ_TIMEOUT, 300000);
        client = ClientBuilder.newClient(configuration);
        clientutil.addAuthFilter(client);
    }


    protected void post(JAXBElement jaxbelement, String path)
            throws IOException
    {
        StringWriter sw = new StringWriter();
        try {
            // Marshall the code and name into an xml document having said values.
              //if(path.indexOf("updatecontainerdocs") != -1)///projects/
              //{
                 //String data="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><containerdocs xmlns=\"http://www.fs.fed.us/nepa/schema\"><containerdoc parentcontid=\"57\" docid=\"FSTST3_2305579\" order=\"1\" /></containerdocs>"; 
                 //String data="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?><project xmlns=\"http://www.fs.fed.us/nepa/schema\"><type>nepa</type><id>50299</id><name>Test Deployment 9/24</name><unitcode>11010200</unitcode><adminapp>pals</adminapp><description>test</description><commentreg>14</commentreg><wwwsummary></wwwsummary><wwwpub>0</wwwpub><nepainfo><projectdocumentid>105175</projectdocumentid><analysistypeid>EIS</analysistypeid><statusid>1</statusid><purposeids><purposeid>TM</purposeid><purposeid>HF</purposeid></purposeids></nepainfo><addinfopubflag>0</addinfopubflag><implinfopubflag>0</implinfopubflag><meetingpubflag>0</meetingpubflag></project>";
                 // sw.write(data);
               //}  
               //else
            marshaller.marshal(jaxbelement, sw);
        } catch (JAXBException ex) {
            Logger.getLogger(AbstractClient.class.getName()).log(Level.SEVERE, null, ex);
            throw new IOException("Failed to marshal values into an xml document. " + ex.getMessage());
        }

        // Make the http request
        //ClientResponse response = client.resource(SERVER_PATH + path).type(ClientUtil.CONTENT_TYPE).post(ClientResponse.class, sw.toString());
        Response response = client.target(SERVER_PATH + path).request(ClientUtil.CONTENT_TYPE).post(Entity.entity(sw.toString(),MediaType.APPLICATION_XML),Response.class);
 
        // Verify the response code
        httpstatus = response.getStatus();
        if ((httpstatus!=ClientUtil.CREATED) && (httpstatus!=ClientUtil.OK))
            throw new IOException(
                    //convertStreamToString(response.getEntityInputStream()) +  "\n\n" + clientutil.dumpResponse(response)
                    convertStreamToString(response.readEntity(InputStream.class)) + "\n\n" + clientutil.dumpResponse(response)
            );
    }


    protected <T extends Object> T get(String path, Class<T> t)
            throws IOException
    {
        // Make the http request
        //ClientResponse response = client.resource(SERVER_PATH + path).type(ClientUtil.CONTENT_TYPE).get(ClientResponse.class);
        
        Response response = client.target(SERVER_PATH + path).request(ClientUtil.CONTENT_TYPE).get(Response.class);
        // Verify the response code
        httpstatus = response.getStatus();
        if (httpstatus!=ClientUtil.OK)
            throw new IOException(
                    convertStreamToString(response.readEntity(InputStream.class)) + "\n\n" + clientutil.dumpResponse(response)
                    //convertStreamToString(response.getEntityInputStream()) + "\n\n" + clientutil.dumpResponse(response)
            );
        try {
             JAXBElement<T> tmp = (JAXBElement<T>) unmarshaller.unmarshal(new StreamSource(response.readEntity(InputStream.class)), t);
             return  tmp.getValue();
        } catch (JAXBException ex) {
            Logger.getLogger(AbstractClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    
    protected void put(JAXBElement jaxbelement, String path)
            throws IOException
    {
        StringWriter sw = new StringWriter();
        try {
            // Marshall the object into an xml document.
          
            /*if(path.indexOf("nepaprojects") != -1)
            {
              //String data ="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?><nepaproject xmlns=\"http://www.fs.fed.us/nepa/schema\"> <id>7346</id> <projectdocumentid>8303</projectdocumentid> <expirationdate>2004-10-20&apos;T&apos;00:00:00</expirationdate> <name>Frank Davis Exploration, Inc. Permit to Drill</name> <status>5</status> <type>3</type> <appealslink>http://www.fs.fed.us/appeals/appeals_list.php?r=110807</appealslink> <objectionslink>http://www.fs.fed.us/objections/objections_list.php?r=110807</objectionslink> <description>Drilling Exploration</description> <projectsummary>Drilling Exploration</projectsummary> <recordcomplete></recordcomplete> <contact> <contactShortName>hhowell</contactShortName> <contactname>Hunter Howell</contactname> <contactphone>601-384-5876</contactphone> <contactemail>hhowell@fs.fed.us</contactemail> </contact> <catexclusion>15</catexclusion> <specauth>1</specauth> <milestone> <msseq>31153</msseq> <mstype>1</mstype> <msdate>08/04/2004&apos;T&apos;00:00:00</msdate> <msstatus>1</msstatus> <mshistoryflag>0</mshistoryflag> </milestone> <milestone> <msseq>31154</msseq> <mstype>5</mstype> <msdate>09/01/2004&apos;T&apos;00:00:00</msdate> <msstatus>0</msstatus> <mshistoryflag>0</mshistoryflag> </milestone> <milestone> <msseq>31155</msseq> <mstype>6</mstype> <msdate>10/01/2004&apos;T&apos;00:00:00</msdate> <msstatus>0</msstatus> <mshistoryflag>0</mshistoryflag> </milestone> <purpose> <purposename>12</purposename> </purpose> <adminunit>11080704</adminunit> <activity>30</activity> <commreg>1</commreg> <esd>0</esd> <location> <locdesc>Franklin County, Mississippi</locdesc> <locfor>110807</locfor> <locdst>11080704</locdst> <locstate>28</locstate> <loccount>28037</loccount> <loclegal>Township 6 North, Range 1 East section 39 </loclegal> </location> <decision> <decname>               Frank Davis Exploration, Inc.</decname> <decisionId>2391</decisionId> <dectype>3</dectype> <decdate>09/20/2004</decdate> <decnotice>09/20/2004</decnotice> <decconstraint>None</decconstraint> <decapprule>1</decapprule> <decmaker> <decmakerId>1548</decmakerId> <decmakername>dstrawbridge</decmakername> <decmakertitle>100</decmakertitle> <decmakesigneddate>09/20/2004</decmakesigneddate> </decmaker> <decappstat></decappstat> <declit> <litid>751</litid> <litname>Margarey v. Sansa</litname> <litstat>1</litstat> </declit> <declit> <litid>774</litid> <litname>Test C</litname> <litstat>1</litstat> </declit> <declit> <litid>723</litid> <litname>Federal Forest Resource Coalition v. Vilsack</litname> <litstat>1</litstat> <litoutcome>3</litoutcome> </declit> <declit> <litid>747</litid> <litname>Test</litname> <litstat>1</litstat> </declit> <declit> <litid>877</litid> <litname>test kam</litname> <litstat>1</litstat> </declit> <decareasize></decareasize> <decareaunits></decareaunits> </decision> <pubflag>0</pubflag> <addinfoflag>0</addinfoflag> <implinfoflag>0</implinfoflag> <meetingflag>0</meetingflag> <updatedate>09/27/2004&apos;T&apos;11:48:34</updatedate> <sopa> <sopaFlag>N</sopaFlag> <isNewProjectFlag>N</isNewProjectFlag> <sopaHeaderUnit>11080704</sopaHeaderUnit> </sopa> <primaryProjectManager>dochabreck</primaryProjectManager> <secondaryProjectManager>hhowell</secondaryProjectManager> <dataEntryPerson>mawaite</dataEntryPerson> </nepaproject>";
                      //"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?><nepaproject xmlns=\"http://www.fs.fed.us/nepa/schema\"> <id>41820</id> <projectdocumentid>95594</projectdocumentid> <name>West Zone Post and Pole Project</name> <status>1</status> <type>3</type> <appealslink>http://www.fs.fed.us/appeals/appeals_list.php?r=110621</appealslink> <objectionslink>http://www.fs.fed.us/objections/objections_list.php?r=110621</objectionslink> <description>The proposal is to remove post and pole products from 27 units totaling approximately 1,489 acres and from dead and down lodgepole within 200 feet of an open road as depicted by the current Motor Vehicle Use Map.</description> <projectsummary>The proposal is to remove post and pole products from 27 units totaling approximately 1,489 acres and from dead and down lodgepole within 200 feet of an open road as depicted by the current Motor Vehicle Use Map.</projectsummary> <recordcomplete></recordcomplete> <contact> <contactShortName>williamshook</contactShortName> <contactname>William Shook</contactname> <contactphone>509-738-7769</contactphone> <contactemail>williamshook@fs.fed.us</contactemail> </contact> <catexclusion>22</catexclusion> <specauth>1</specauth> <milestone> <msseq>526107</msseq> <mstype>1</mstype> <msdate>07/01/2013&apos;T&apos;00:00:00</msdate> <msstatus>0</msstatus> <mshistoryflag>0</mshistoryflag> </milestone> <milestone> <msseq>526108</msseq> <mstype>5</mstype> <msdate>09/01/2013&apos;T&apos;00:00:00</msdate> <msstatus>0</msstatus> <mshistoryflag>0</mshistoryflag> </milestone> <milestone> <msseq>526109</msseq> <mstype>6</mstype> <msdate>12/01/2013&apos;T&apos;00:00:00</msdate> <msstatus>0</msstatus> <mshistoryflag>0</mshistoryflag> </milestone> <purpose> <purposename>8</purposename> </purpose> <adminunit>11062104</adminunit> <activity>22</activity> <commreg>1</commreg> <esd>0</esd> <location> <locdesc>SF Barnaby Cr, Sherman Cr, Deadman Cr&amp; trib, Gold Cr, EF Cedar Cr,WF Silver Cr, Hartbauer Cr, Rabbit Cr, Deer Cr, NF Chewelah Cr, Sixmile Cr, Butte Cr, Hartill Cr, WF Trout Cr,   Goodrich Cr. </locdesc> <locfor>110621</locfor> <locdst>11062104</locdst> <locdst>11062112</locdst> <locstate>53</locstate> <loccount>53019</loccount> <loccount>53065</loccount> <loclegal> variety of locations on the Republic and Three Rivers Ranger Districts</loclegal> </location> <pubflag>1</pubflag> <addinfoflag>1</addinfoflag> <implinfoflag>1</implinfoflag> <meetingflag>0</meetingflag> <updatedate>06/28/2013&apos;T&apos;16:47:45</updatedate> <sopa> <sopaFlag>Y</sopaFlag> <isNewProjectFlag>N</isNewProjectFlag> <sopaHeaderUnit>2</sopaHeaderUnit> </sopa> <primaryProjectManager>cesarramirez</primaryProjectManager> <secondaryProjectManager>williamshook</secondaryProjectManager> <dataEntryPerson>jlavell</dataEntryPerson> </nepaproject>";            
              //sw.write(data);
            //    String data ="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?><nepaproject xmlns=\"http://www.fs.fed.us/nepa/schema\"> <id>34111</id> <projectdocumentid>73484</projectdocumentid> <name>01-108 - AT&amp;T Fiber Optic Cable - Special Use Permit</name> <status>4</status> <type>3</type> <appealslink>http://www.fs.fed.us/appeals/appeals_list.php?r=110803</appealslink> <objectionslink>http://www.fs.fed.us/objections/objections_list.php?r=110803</objectionslink> <description>Special Use Permit for AT&amp;T Fiber Optic Cable</description> <projectsummary>Special Use Permit for AT&amp;T Fiber Optic Cable</projectsummary> <recordcomplete></recordcomplete> <contact> <contactShortName>jwheeler</contactShortName> <contactname>Joni Wheeler</contactname> <contactphone>770-297-3063</contactphone> <contactemail>jwheeler@fs.fed.us</contactemail> </contact> <catexclusion>15</catexclusion> <specauth>1</specauth> <milestone> <msseq>527131</msseq> <mstype>1</mstype> <msdate>12/01/2010&apos;T&apos;00:00:00</msdate> <msstatus>0</msstatus> <mshistoryflag>0</mshistoryflag> </milestone> <milestone> <msseq>527132</msseq> <mstype>5</mstype> <msdate>01/01/2011&apos;T&apos;00:00:00</msdate> <msstatus>0</msstatus> <mshistoryflag>0</mshistoryflag> </milestone> <milestone> <msseq>527133</msseq> <mstype>6</mstype> <msdate>03/01/2011&apos;T&apos;00:00:00</msdate> <msstatus>0</msstatus> <mshistoryflag>0</mshistoryflag> </milestone> <purpose> <purposename>15</purposename> </purpose> <adminunit>11080304</adminunit> <activity>35</activity> <commreg>1</commreg> <esd>0</esd> <location> <locdesc>Walker and Whitfield Counties, Georgia</locdesc> <locfor>110803</locfor> <locdst>11080301</locdst> <locstate>13</locstate> <loccount>13295</loccount> <loccount>13313</loccount> </location> <pubflag>1</pubflag> <addinfoflag>0</addinfoflag> <implinfoflag>0</implinfoflag> <meetingflag>0</meetingflag> <updatedate>12/15/2010&apos;T&apos;10:17:13</updatedate> <sopa> <sopaFlag>N</sopaFlag> <isNewProjectFlag>N</isNewProjectFlag> <sopaHeaderUnit>11080301</sopaHeaderUnit> </sopa> <primaryProjectManager>mhjones</primaryProjectManager> <secondaryProjectManager>jeromebennett</secondaryProjectManager> <dataEntryPerson>jwheeler</dataEntryPerson> </nepaproject>";
            //    sw.write(data);
                String data ="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?><nepaproject xmlns=\"http://www.fs.fed.us/nepa/schema\"> <id>100270</id> <projectdocumentid>127724</projectdocumentid> <name>Test SOPA NEW Flag</name> <status>2</status> <type>2</type> <appealslink>http://www.fs.fed.us/appeals/appeals_list.php?r=110511</appealslink> <objectionslink>http://www.fs.fed.us/objections/objections_list.php?r=110511</objectionslink> <description>fg</description> <projectsummary>fg</projectsummary> <recordcomplete></recordcomplete> <contact> <contactShortName>tduenas</contactShortName> <contactname>Antonio Duenas</contactname> <contactphone>530-836-7156</contactphone> <contactemail>jf@fs.fed.us</contactemail> </contact> <specauth>1</specauth> <milestone> <msseq>537532</msseq> <mstype>1</mstype> <msdate>01/17/2017&apos;T&apos;00:00:00</msdate> <msstatus>1</msstatus> <mshistoryflag>0</mshistoryflag> </milestone> <milestone> <msseq>537533</msseq> <mstype>2</mstype> <msdate>01/17/2017&apos;T&apos;00:00:00</msdate> <msstatus>1</msstatus> <mshistoryflag>0</mshistoryflag> </milestone> <milestone> <msseq>537534</msseq> <mstype>4</mstype> <msdate>01/17/2017&apos;T&apos;00:00:00</msdate> <msstatus>1</msstatus> <mshistoryflag>0</mshistoryflag> </milestone> <milestone> <msseq>537535</msseq> <mstype>5</mstype> <msdate>01/01/2017&apos;T&apos;00:00:00</msdate> <msstatus>0</msstatus> <mshistoryflag>0</mshistoryflag> </milestone> <milestone> <msseq>537536</msseq> <mstype>6</mstype> <msdate>01/01/2017&apos;T&apos;00:00:00</msdate> <msstatus>0</msstatus> <mshistoryflag>0</mshistoryflag> </milestone> <purpose> <purposename>15</purposename> </purpose> <adminunit>11051101</adminunit> <activity>35</activity> <commreg>4</commreg> <esd>0</esd> <location> <locdesc>test</locdesc> <locfor>110511</locfor> <locdst>11051101</locdst> <locstate>6</locstate> <loccount>6063</loccount> <loclegal>Not Applicable</loclegal> </location> <pubflag>1</pubflag> <addinfoflag>0</addinfoflag> <implinfoflag>0</implinfoflag> <meetingflag>0</meetingflag> <updatedate>01/17/2017&apos;T&apos;09:37:56</updatedate> <sopa> <sopaFlag>Y</sopaFlag> <sopaHeaderUnit>11051101</sopaHeaderUnit> </sopa> <primaryProjectManager>oshroff</primaryProjectManager> <secondaryProjectManager>sbarron</secondaryProjectManager> <dataEntryPerson>oshroff</dataEntryPerson> </nepaproject>";
                sw.write(data);
                
            }
           else  */           
            marshaller.marshal(jaxbelement, sw);    
        } catch (JAXBException ex) {
            Logger.getLogger(AbstractClient.class.getName()).log(Level.SEVERE, null, ex);
            throw new IOException("Failed to marshal values into an xml document. " + ex.getMessage());
        }

        // Make the http request
        //ClientResponse response = client.resource(SERVER_PATH + path).type(ClientUtil.CONTENT_TYPE).put(ClientResponse.class, sw.toString());
           Response response = client.target(SERVER_PATH + path).request(ClientUtil.CONTENT_TYPE).put(Entity.entity(sw.toString(),MediaType.APPLICATION_XML),Response.class);
 
        // Verify the response code
        httpstatus = response.getStatus();
        if (httpstatus!=ClientUtil.OK)
            throw new IOException(
                    convertStreamToString(response.readEntity(InputStream.class)) + "\n\n" + clientutil.dumpResponse(response)
                    //convertStreamToString(response.getEntityInputStream()) + "\n\n" + clientutil.dumpResponse(response)
            );
    }


    protected void delete(String path)
            throws IOException
    {
        // Make the http request
        //ClientResponse response = client.resource(SERVER_PATH + path).type(ClientUtil.CONTENT_TYPE).delete(ClientResponse.class);
        Response response = client.target(SERVER_PATH + path).request(ClientUtil.CONTENT_TYPE).delete(Response.class);

        // Verify the response code
        httpstatus = response.getStatus();
        if (httpstatus!=ClientUtil.NOCONTENT)
            throw new IOException(
                    convertStreamToString(response.readEntity(InputStream.class)) + "\n\n" + clientutil.dumpResponse(response)
                    //convertStreamToString(response.getEntityInputStream()) + "\n\n" + clientutil.dumpResponse(response)
            );
    }


    public String convertStreamToString(InputStream is)
            throws IOException {
        /*
         * To convert the InputStream to String we use the
         * Reader.read(char[] buffer) method. We iterate until the
         * Reader return -1 which means there's no more data to
         * read. We use the StringWriter class to produce the string.
         */
        if (is != null) {
            Writer writer = new StringWriter();

            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(
                        new InputStreamReader(is, ClientUtil.ENCODING));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            } finally {
                is.close();
            }
            return writer.toString();
        } else {
            return "";
        }
    }


    /*
     * Returns the HTTP Status Code of the most recent request
     */
    public int getStatus()
    {
        return httpstatus;
    }


    /*
     * Convert a YYYY-MM-DD string into an XMLGregorianCalendar so that
     * we can assign JAXB date types
     */
    public XMLGregorianCalendar dateStrToXML(String datestring)
            throws IOException
    {
        String[] splitDate = datestring.split("-");
        int year = Integer.parseInt(splitDate[0]);
        int month = Integer.parseInt(splitDate[1]) - 1; // month is 0-based
        int day = Integer.parseInt(splitDate[2]);

        GregorianCalendar gc = new GregorianCalendar(year, month, day);
        try {
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
        }
        catch (Exception ex) {
            throw new IOException("Couldn't convert date string '" + datestring + "'. " + ex.getMessage());
        }
    }


    /*
     * Convert a YYYY-MM-DD'T'HH:MM:SS string into an XMLGregorianCalendar so that
     * we can assign JAXB date types
     */
    public XMLGregorianCalendar dateTimeStrToXML(String datetimestring)
            throws IOException
    {
        String[] splitString = datetimestring.split("T");
        if (splitString.length!=2)
            throw new IOException("Could not parse DateTime string '" + datetimestring + "'.");

        String[] datepart = splitString[0].split("-");
        if (datepart.length!=3)
            throw new IOException("Could not parse date part of DateTime string '" + datetimestring + "'.");
        int year = Integer.parseInt(datepart[0]);
        int month = Integer.parseInt(datepart[1]) - 1; // month is 0-based
        int day = Integer.parseInt(datepart[2]);

        String[] timepart = splitString[1].split(":");
        if (timepart.length!=3)
            throw new IOException("Could not parse time part of DateTime string '" + datetimestring + "'.");
        int hour = Integer.parseInt(timepart[0]);
        int minute = Integer.parseInt(timepart[1]);
        int second = Integer.parseInt(timepart[2]);

        GregorianCalendar gc = new GregorianCalendar(year, month, day, hour, minute, second);
        try {
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
        }
        catch (Exception ex) {
            throw new IOException("Couldn't convert DateTime string '" + datetimestring + "'. " + ex.getMessage());
        }
    }
}