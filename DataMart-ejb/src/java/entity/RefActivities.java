package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "RefActivities")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RefActivities.findAll", query = "SELECT r FROM RefActivities r"),
    @NamedQuery(name = "RefActivities.findById", query = "SELECT r FROM RefActivities r WHERE r.id = :id"),
    @NamedQuery(name = "RefActivities.findByName", query = "SELECT r FROM RefActivities r WHERE r.name = :name")})
public class RefActivities implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private String id;
    @Basic(optional = false)
    @Column(name = "Name")
    private String name;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refActivities")
    private Collection<ProjectActivities> projectActivitiesCollection;

    public RefActivities() {
    }

    public RefActivities(String id) {
        this.id = id;
    }

    public RefActivities(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Collection<ProjectActivities> getProjectActivitiesCollection() {
        return projectActivitiesCollection;
    }

    public void setProjectActivitiesCollection(Collection<ProjectActivities> projectActivitiesCollection) {
        this.projectActivitiesCollection = projectActivitiesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RefActivities)) {
            return false;
        }
        RefActivities other = (RefActivities) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RefActivities[ id=" + id + " ]";
    }

}
