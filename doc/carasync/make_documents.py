#!/usr/bin/python

import sys, os
fname = os.path.join(os.path.dirname(__file__), "pythonlibs")
sys.path.insert(0, fname)
import time
import dmUtil
from sys import argv
from urllib2 import HTTPError
from xml.sax.saxutils import escape

def storeDoc(docid, fname, projectid):
    # check for existing doc
    url="http://localhost:7001/api/1_0/projects/nepa/" + projectid + "/docs/" + docid
    try:
    	resp=dmUtil.get(url)
    	log("" + projectid + "/" + docid + " exists, nothing to do")
	return
    except HTTPError, e:
    	if (e.code == 404):
    		print "" + projectid + "/" + docid + " not found, insert document"
    	else:
    		raise e
    
    # if doc missing,
    #   format request
    request = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>	\
    <projectdocument xmlns=\"http://www.fs.fed.us/nepa/schema\">	\
        <docid>" + docid + "</docid>				\
        <projecttype>nepa</projecttype>				\
        <projectid>" + projectid + "</projectid>		\
        <pubflag>0</pubflag>					\
        <wwwlink/>						\
        <docname>" + escape(fname) + "</docname>		\
        <description/>						\
        <pdffilesize/>						\
    </projectdocument>"
    
    #   put doc
    try:
        url="http://localhost:7001/api/1_0/projects/nepa/" + projectid + "/docs/"
        dmUtil.post(url, request)
        log("Created document " + projectid + "/" + docid)
    except HTTPError, e:
        #   if error, write to errlog
        log(currline + "|" + str(e.code))

def log(msg):
    print(msg)
    flog.write(msg + "\n")

#############
# START HERE
#############

dmUtil.initOpener()

# open error log
flog = open("app.log", "a")
runid = time.time()
log("Run " + str(runid) + " starting at " +
    time.strftime('%Y, %B %d, %H:%M:%S', time.localtime()))

if (len(argv) < 2):
    print "Usage: " + argv[0] + " <filename>\n";
    sys.exit(1)

fname = argv[1]
log("Opening " + fname)
fin = open(fname)

for line in fin:
    currline = line.strip()

    # split input into docid|fname|projectid
    [docid, fname, projectid] = currline.split('|')

    try:
        storeDoc(docid, fname, projectid)
    except:
        log(currline + "|" + sys.exc_info()[0])

log("Run " + str(runid) + " ending at " +
    time.strftime('%Y, %B %d, %H:%M:%S', time.localtime()))
flog.close()

