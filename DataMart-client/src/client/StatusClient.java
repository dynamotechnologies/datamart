package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.status.*;


public class StatusClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public StatusClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.status", "Status.xsd");
    }


    public void postStatus(String id, String name)
            throws IOException
    {
        Status status = new Status();
        status.setId(id);
        status.setName(name);
        postStatus(status);
    }


    public void postStatus(Status status)
            throws IOException
    {
        post(objectfactory.createStatus(status), "/ref/statuses");
    }


    public Status getStatus(String id)
            throws IOException
    {
        return get("/ref/statuses/" + id, Status.class);
    }


    public Statuses getStatusList()
            throws IOException
    {
        return get("/ref/statuses", Statuses.class);
    }


    public void putStatus(String id, String name)
            throws IOException
    {
        Status status = new Status();
        status.setId(id);
        status.setName(name);
        putStatus(status);
    }


    public void putStatus(Status status)
            throws IOException
    {
        put(objectfactory.createStatus(status), "/ref/statuses/" + status.getId());
    }


    public void deleteStatus(String id)
            throws IOException
    {
        delete("/ref/statuses/" + id);
    }
}