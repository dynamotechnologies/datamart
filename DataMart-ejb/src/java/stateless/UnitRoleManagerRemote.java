package stateless;
import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.unitrole.*;
import beanutil.DataMartException;

@Remote
public interface UnitRoleManagerRemote {
    public void create(Unitrole resource) throws DataMartException;
    public Unitrole get(int accessgroupid) throws DataMartException;
    public void update(Unitrole resource) throws DataMartException;
    public void delete(int accessgroupid) throws DataMartException;
    public Unitroles getAll() throws DataMartException;
    public Boolean ping();
}
