--
-- Script to update null PalsContId values for Category-level containers
--
select distinct(Label) from ProjectDocumentContainers where PalsContId is null and ContainerId is null;

update ProjectDocumentContainers set Label = "Decision" where PalsContId is null and ContainerId is null and Label = "Decision Documents";

update ProjectDocumentContainers set Label = "Scoping" where PalsContId is null and ContainerId is null and Label = "Scoping Documents";

update ProjectDocumentContainers set Label = "Supporting" where PalsContId is null and ContainerId is null and Label = "Supporting Documents";

update ProjectDocumentContainers set Label = "Analysis" where PalsContId is null and ContainerId is null and Label = "Analysis Documents";

update ProjectDocumentContainers set PalsContId = -1 where PalsContId is null and ContainerId is null and Label = "Pre-Scoping";

update ProjectDocumentContainers set PalsContId = -2 where PalsContId is null and ContainerId is null and Label = "Scoping";

update ProjectDocumentContainers set PalsContId = -3 where PalsContId is null and ContainerId is null and Label = "Analysis";

update ProjectDocumentContainers set PalsContId = -4 where PalsContId is null and ContainerId is null and Label = "Supporting";

update ProjectDocumentContainers set PalsContId = -5 where PalsContId is null and ContainerId is null and Label = "Decision";

update ProjectDocumentContainers set PalsContId = -6 where PalsContId is null and ContainerId is null and Label = "Post-Decision";

select distinct(Label) from ProjectDocumentContainers where PalsContId is null and ContainerId is null;

--
-- Schema changes
--

CREATE TABLE Goals ( -- Added for PALS 3.3
  Id int(11) NOT NULL,	-- goalid
  Description varchar(1024),
  CONSTRAINT PkGoals_Id PRIMARY KEY (Id)
) ENGINE=INNODB;

CREATE TABLE ResourceAreas ( -- Added for PALS 3.3
  Id int(11) NOT NULL,	-- resourceareaid
  UnitId varchar(255) NOT NULL,	-- forest unit
  Description varchar(1024),
  CONSTRAINT PkResourceAreas_Id PRIMARY KEY (Id),
  CONSTRAINT FkResourceAreas_UnitId FOREIGN KEY (UnitId) REFERENCES RefUnits (Id)
) ENGINE=INNODB;

CREATE TABLE ProjectGoals ( -- Added for PALS 3.3
  Id int(11) NOT NULL AUTO_INCREMENT,
  ProjectId int(11) NOT NULL,
  GoalId int(11) NOT NULL,
  OrderTag int(11),	-- display order
  CONSTRAINT PkProjectGoals_Id PRIMARY KEY (Id),
  CONSTRAINT FkProjectGoals_ProjectId FOREIGN KEY (ProjectId) REFERENCES Projects (Id),
  CONSTRAINT FkProjectGoals_GoalId FOREIGN KEY (GoalId) REFERENCES Goals (Id),
  CONSTRAINT UnProjectGoals_ProjectId_GoalId UNIQUE (ProjectId, GoalId)
) ENGINE=INNODB;

CREATE TABLE UnitGoals ( -- Added for PALS 3.3
  Id int(11) NOT NULL AUTO_INCREMENT,
  UnitId varchar(255) NOT NULL,	-- forest unit
  GoalId int(11) NOT NULL,
  OrderTag int(11),	-- display order
  CONSTRAINT PkUnitGoals_Id PRIMARY KEY (Id),
  CONSTRAINT FkUnitGoals_UnitId FOREIGN KEY (UnitId) REFERENCES RefUnits (Id),
  CONSTRAINT FkUnitGoals_GoalId FOREIGN KEY (GoalId) REFERENCES Goals (Id),
  CONSTRAINT UnUnitGoals_UnitId_GoalId UNIQUE (UnitId, GoalId)
) ENGINE=INNODB;

CREATE TABLE ProjectResourceAreas ( -- Added for PALS 3.3
  Id int(11) NOT NULL AUTO_INCREMENT,
  ProjectId int(11) NOT NULL,
  ResourceAreaId int(11) NOT NULL,
  OrderTag int(11),	-- display order
  CONSTRAINT PkProjectResourceAreas_Id PRIMARY KEY (Id),
  CONSTRAINT FkProjectResourceAreas_ProjectId FOREIGN KEY (ProjectId) REFERENCES Projects (Id),
  CONSTRAINT FkProjectResourceAreas_ResourceAreaId FOREIGN KEY (ResourceAreaId) REFERENCES ResourceAreas (Id),
  CONSTRAINT UnProjectResourceAreas_ProjectId_ResourceAreaId UNIQUE (ProjectId, ResourceAreaId)
) ENGINE=INNODB;

ALTER TABLE Decisions ADD COLUMN AreaSize double;		-- PALS 3.3
ALTER TABLE Decisions ADD COLUMN AreaUnits varchar(255);	-- PALS 3.3

