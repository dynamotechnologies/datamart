package stateless;

import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.ProjectAdditionalInfo.*;
import beanutil.DataMartException;

@Remote
public interface ProjectAdditionalInfoManagerRemote {
    public void create(ProjectAdiitionalInfo resource) throws DataMartException;
    public ProjectAdiitionalInfo get(String ProjectId) throws DataMartException;
      public ProjectAdiitionalInfos getAllByProject(String projecttype, String projectid) throws DataMartException;
  
    public void update(ProjectAdiitionalInfo resource) throws DataMartException;
    public void delete(String filename) throws DataMartException;
    public Boolean ping();

 }