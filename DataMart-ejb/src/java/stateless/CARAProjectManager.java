package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.caraproject.*;

import beanutil.*;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Iterator;
import java.util.List;
import javax.persistence.Query;
import org.jboss.logging.Logger;


@Stateless(mappedName="CARAProjectManagerRemote")
public class CARAProjectManager implements CARAProjectManagerRemote
{
    @PersistenceContext
    private EntityManager em;

    private final String NEPA_TYPE = "nepa";

    @Override
    public void linkproject(Caralinkproject linkRequest)
             throws DataMartException
    {
        int caraid = linkRequest.getCaraid();
        String palsid = linkRequest.getPalsid();

        // Log request
        String logmsg = "Linking CARA project (" + caraid + "/" + palsid + ")";
        Logger logger = Logger.getLogger(this.getClass());
        logger.info(logmsg);


        // Check to see if project is already linked
        entity.CARAProjects badentity = getEntity(caraid);
        if (badentity != null)
        {
            throw new DataMartException(DataMartException.ALREADY_EXISTS, "CARA project ID " + caraid + " already in use");
        }
        entity.CARAProjects badentity2 = getEntityByPalsID(palsid);
        if (badentity2 != null)
        {
            throw new DataMartException(DataMartException.ALREADY_EXISTS, "PALS project ID " + palsid + " already linked to CARA Project " + badentity2.getId());
        }

        // Link project
        Caraproject resource = new Caraproject();
        resource.setCaraid(caraid);
        resource.setPalsid(palsid);
        resource.setReadingroomactive(false);
        entity.CARAProjects entity = createEntity(resource);
        entity = em.merge(entity);
    }


    @Override
    public void unlinkproject(Caraunlinkproject unlinkRequest)
             throws DataMartException
    {
        int caraid = unlinkRequest.getCaraid();

        // Log request
        String logmsg = "Unlinking CARA project " + caraid + ")";
        Logger logger = Logger.getLogger(this.getClass());
        logger.info(logmsg);

        entity.CARAProjects entity = getEntity(caraid);
        if (entity == null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(entity);
    }


    public void update(Caraproject resource)
            throws DataMartException
    {
        entity.CARAProjects entity = createEntity(resource);

        entity.CARAProjects oldEntity = getEntity(resource.getCaraid());
        if (oldEntity == null) {
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND, "Nonexistent CARA Project cannot be updated.");
        }
        String requestPalsId = resource.getPalsid();
        String currentPalsId = oldEntity.getProjects().getPublicId();
        if (!requestPalsId.equals(currentPalsId)) {

            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION, "Inconsistent PALS ID (" + requestPalsId + " != " + currentPalsId + "), cannot update CARA project.");
        }
        entity = em.merge(entity);
    }


    @Override
    public Caraproject get(int caraid)
            throws DataMartException
    {
        entity.CARAProjects entity = getEntity(caraid);
        if (entity == null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return createResource(entity);
    }


    @Override
    public Caraproject getByPalsId(String palsid)
            throws DataMartException
    {
        entity.CARAProjects entity = getEntityByPalsID(palsid);
        if (entity == null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return createResource(entity);
    }


    @Override
    public Caraprojects getAll()
            throws DataMartException
    {
        // Retrieve all entities from the database
        Query query = em.createQuery("SELECT p FROM CARAProjects p ORDER BY p.id");
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Caraprojects responselist = new Caraprojects();
        while (it.hasNext()) {
            responselist.getCaraproject().add(createResource((entity.CARAProjects)it.next() ));
        }
        return responselist;
    }


    @Override
    public Boolean ping()
    {
        return true;
    }

    private entity.CARAProjects createEntity(Caraproject resource)
            throws DataMartException
    {
        String palsId = resource.getPalsid();
        int caraId = resource.getCaraid();
        boolean rractive = resource.isReadingroomactive();

        entity.CARAProjects entity = new entity.CARAProjects();
        entity.setId(caraId);
        entity.Projects project = DataMartBeanUtil.getProject(em, NEPA_TYPE, palsId);
        entity.setProjects(project);
        entity.setReadingRoomActive(rractive);
        return entity;
    }

    /*
     * Find CARAProject record by CARA ID
     */
    private entity.CARAProjects getEntity(int caraid)
    {
        return em.find(entity.CARAProjects.class, caraid);
    }

    /*
     * Find CARAProject record by PALS ID
     */
    private entity.CARAProjects getEntityByPalsID(String palsid)
            throws DataMartException
    {
        entity.Projects project = DataMartBeanUtil.getProject(em, "nepa", palsid);
        if (project == null) {
            return null;
        }

        Query query = em.createQuery("SELECT p FROM CARAProjects p WHERE p.projects = :project");
        query.setParameter("project", project);

        entity.CARAProjects cp = (entity.CARAProjects) DataMartBeanUtil.getEntityByQueryAllowNull(query,
                "More than one CARA project found for nepa project '"+palsid+"'");

        return cp;
    }

    /*
     * Create resource from entity
     */
    private Caraproject createResource(entity.CARAProjects entity)
    {
        Caraproject rsrc = new Caraproject();
        rsrc.setCaraid(entity.getId());
        rsrc.setPalsid(entity.getProjects().getPublicId());
        rsrc.setReadingroomactive(entity.getReadingRoomActive());
        return rsrc;
    }
}
