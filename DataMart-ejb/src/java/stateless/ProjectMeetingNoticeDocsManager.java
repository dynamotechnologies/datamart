/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateless;

import beanutil.DataMartBeanUtil;
import beanutil.DataMartException;
import java.util.Iterator;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import us.fed.fs.www.nepa.schema.ProjectMeetingNoticeDoc.ProjectMeetingNoticeDoc;
import us.fed.fs.www.nepa.schema.ProjectMeetingNoticeDoc.ProjectMeetingNoticeDocs;
import us.fed.fs.www.nepa.schema.ProjectMeetingNotices.Meetingnotices;

/**
 *
 * @author gauri
 */
@Stateless(mappedName="ProjectMeetingNoticeDocsManagerRemote")
public class ProjectMeetingNoticeDocsManager implements ProjectMeetingNoticeDocsManagerRemote {
   @PersistenceContext
    private EntityManager em;
    @Override
    public void create(ProjectMeetingNoticeDoc resource) throws DataMartException {
     entity.ProjectMeetingNoticeDocs entity = createEntity(resource);

        entity.ProjectMeetingNoticeDocs badentity = retrieveEntity(entity.getMeetingNoticeDocId());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        entity = em.merge(entity);    
    }

    @Override
    public ProjectMeetingNoticeDoc get(String projecttype, String projectid, Integer meetingId, Integer docId) throws DataMartException {
     entity.ProjectMeetingNoticeDocs entity = retrieveEntity(docId);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return createResource(entity);    }

    @Override
    public ProjectMeetingNoticeDocs getAllByProject(String projecttype, String projectid, Integer meetingId) throws DataMartException {
    entity.ProjectMeetingNotices mn = em.find(entity.ProjectMeetingNotices.class, meetingId);

        // Retrieve project resource area entities from the database
        
        Query query = em.createQuery("SELECT a FROM ProjectMeetingNoticeDocs a WHERE a.meetingNoticeId = :meetingNoticeId ORDER BY a.documentOrder");
        query.setParameter("meetingNoticeId", mn);
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        ProjectMeetingNoticeDocs responselist = new ProjectMeetingNoticeDocs();
        while (it.hasNext()) {
            responselist.getProjectMeetingNoticeDoc().add(createResource((entity.ProjectMeetingNoticeDocs )it.next()));
        }
        return responselist;  
    }

    @Override
    public void update(ProjectMeetingNoticeDoc resource) throws DataMartException {
        entity.ProjectMeetingNoticeDocs entity = createEntity(resource);

        entity.ProjectMeetingNoticeDocs existingentity = retrieveEntity(new Integer(resource.getMeetingDocId()));

        if (existingentity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        entity.setMeetingNoticeDocId(existingentity.getMeetingNoticeDocId());
        entity = em.merge(entity);  
    }

    @Override
    public void delete(String projecttype, String projectid, Integer meetingId, Integer docId) throws DataMartException {
        entity.ProjectMeetingNoticeDocs entity = retrieveEntity(docId);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));
       }

    @Override
    public Boolean ping() {
        return true;
    }
     /*
     * Create XML from EJB
     */
    private ProjectMeetingNoticeDoc createResource(entity.ProjectMeetingNoticeDocs entity)
            throws DataMartException
    {
        ProjectMeetingNoticeDoc resource = new ProjectMeetingNoticeDoc();
  
    resource.setMeetingId(entity.getMeetingNoticeId().getMeetingId().toString()) ;
    resource.setMeetingDocId(entity.getMeetingNoticeDocId().toString()) ;
    resource.setDocumentId(entity.getDocumentId().getDocumentId()) ;
    resource.setOrder(entity.getDocumentOrder()) ;
    resource.setDocname(entity.getDocumentId().getLabel());
    resource.setWwwlink(entity.getDocumentId().getLink());
    resource.setPdffilesize(entity.getDocumentId().getPdfSize());
        return resource;
    }

    /*
     * Create EJB from XML
     */
    private entity.ProjectMeetingNoticeDocs createEntity(ProjectMeetingNoticeDoc resource)
            throws DataMartException
    {
        
        

        Integer meetingId = new Integer(resource.getMeetingId()) ;
        entity.ProjectMeetingNotices mn = em.find(entity.ProjectMeetingNotices.class, meetingId);
         entity.ProjectDocuments doc = DataMartBeanUtil.getProjectDocument(em, mn.getProjectId(), resource.getDocumentId());

        if (mn == null) {
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION, "MeetingNotice " + meetingId + " does not exist");
        }

        entity.ProjectMeetingNoticeDocs entity = new entity.ProjectMeetingNoticeDocs();
        entity.setMeetingNoticeId(mn);
        entity.setDocumentId(doc);
        entity.setDocumentOrder(resource.getOrder());
       
        
        return entity;
    }

    private entity.ProjectMeetingNoticeDocs retrieveEntity(Integer key)
            throws DataMartException
    {
         if(null !=key)
         return em.find(entity.ProjectMeetingNoticeDocs.class, key);
         else
         return null;    
      
    }
}
