package test.relationship;

import client.ProjectLocationClient;
import client.ProjectClient;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.projectlocation.*;


public class ProjectLocationTest {
    private ProjectLocationClient client;
    ProjectClient projclient;
    private ArrayList<String> regions1 = new ArrayList<String>();
    private ArrayList<String> forests1 = new ArrayList<String>();
    private ArrayList<String> districts1 = new ArrayList<String>();
    private ArrayList<String> states1 = new ArrayList<String>();
    private ArrayList<String> counties1 = new ArrayList<String>();

    @Before
    public void setUp()
            throws IOException
    {
         client = new ProjectLocationClient();
         projclient = new ProjectClient();
         
         regions1.add(TestData.projectlocations.regionid1);
         forests1.add(TestData.projectlocations.forestid1);
         districts1.add(TestData.projectlocations.districtid1);
         states1.add(TestData.projectlocations.stateid1);
         counties1.add(TestData.projectlocations.countyid1);
    }

    
    @Test
    public void testGet()
    {
        confirmLocationsEmpty();

        put();

        confirmLocations1();

        delete();
    }

    @Test
    public void testPut()
    {
        put();

        try {
            // second PUT should overwrite the first
            client.putProjectLocations( TestData.projects.id1,
                                        TestData.projectlocations.description2,
                                        TestData.projectlocations.legaldesc2,
                                        TestData.projectlocations.latitude1,
                                        TestData.projectlocations.longitude1,
                                        regions1,
                                        forests1,
                                        districts1,
                                        states1,
                                        counties1);
        } catch (Exception ex) {
            Logger.getLogger(ProjectLocationTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        confirmLocations2();

        delete();
    }

    @Test
    public void testUpdateProject()
    {
        put();

        // The locations info should not change when the project is updated     
        ArrayList<String> purposeids1 = new ArrayList<String>();
        purposeids1.add(TestData.projects.purposeid1);
        purposeids1.add(TestData.projects.purposeid2);
        purposeids1.add(TestData.projects.purposeid3);
        try {
            projclient.putProject(TestData.projects.id1, TestData.projects.type1,
                    TestData.projects.name1, TestData.projects.unitcode1, TestData.projects.description1,
                    TestData.projects.lastupdate1, TestData.projects.commentreg1, TestData.projects.wwwlink1,
                    TestData.projects.wwwsummary1, TestData.projects.wwwpub1, TestData.projects.analysistypeid1,
                    TestData.projects.statusid1, TestData.projects.contactname2, TestData.projects.contactphone1,
                    TestData.projects.contactemail1, purposeids1, TestData.projects.expirationdate2, TestData.projects.projectdocumentid2,
                    TestData.projects.sopapub, TestData.projects.sopanew, TestData.projects.sopaheader, TestData.projects.esd, TestData.projects.cenodecision);
        } catch (IOException ex) {
            Logger.getLogger(ProjectLocationTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        confirmLocations1();

        delete();
    }

    @Test
    public void testErase()
    {
        put();
        delete();

        confirmLocationsEmpty();
    }


    private void put()
    {
        try {
            client.putProjectLocations( TestData.projects.id1,
                                        TestData.projectlocations.description1,
                                        TestData.projectlocations.legaldesc1,
                                        TestData.projectlocations.latitude1,
                                        TestData.projectlocations.longitude1,
                                        regions1,
                                        forests1,
                                        districts1,
                                        states1,
                                        counties1);
        } catch (Exception ex) {
            Logger.getLogger(ProjectLocationTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.putProjectLocations( TestData.projects.id1,
                                        "",
                                        null,
                                        null,
                                        null,
                                        new ArrayList<String>(),
                                        new ArrayList<String>(),
                                        new ArrayList<String>(),
                                        new ArrayList<String>(),
                                        new ArrayList<String>());
        } catch (Exception ex) {
            Logger.getLogger(ProjectLocationTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void confirmLocationsEmpty()
    {
        // Should return empty elements because location info has not yet been set
        Locations locations = null;
        try {
            locations = client.getProjectLocations(TestData.projects.id1);
        } catch (Exception ex) {
            Logger.getLogger(ProjectLocationTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // make sure the location info is empty
        Assert.assertEquals(TestData.projects.id1, locations.getProjectid());
        Assert.assertEquals("", locations.getLocationdesc());
        Assert.assertEquals(null, locations.getLocationlegaldesc());
        Assert.assertEquals(null, locations.getLatitude());
        Assert.assertEquals(null, locations.getLongitude());

        Assert.assertEquals(0, locations.getRegions().getRegionid().size());
        Assert.assertEquals(0, locations.getForests().getForestid().size());
        Assert.assertEquals(0, locations.getDistricts().getDistrictid().size());
        Assert.assertEquals(0, locations.getStates().getStateid().size());
        Assert.assertEquals(0, locations.getCounties().getCountyid().size());
    }

    private void confirmLocations1()
    {
        Locations locations = null;
        Locationslist locationslist = null;
        try {
            locations = client.getProjectLocations(TestData.projects.id1);
            locationslist = client.getByUnit(TestData.projectlocations.forestid1);
        } catch (Exception ex) {
            Logger.getLogger(ProjectLocationTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // make sure the locations we posted are present in the GET response
        Assert.assertEquals(TestData.projects.id1, locations.getProjectid());
        Assert.assertEquals(TestData.projectlocations.description1, locations.getLocationdesc());
        Assert.assertEquals(TestData.projectlocations.legaldesc1, locations.getLocationlegaldesc());
        Assert.assertEquals((Double) TestData.projectlocations.latitude1, (Double) locations.getLatitude());
        Assert.assertEquals((Double) TestData.projectlocations.longitude1, (Double) locations.getLongitude());

        Assert.assertEquals(TestData.projectlocations.regionid1, locations.getRegions().getRegionid().get(0));
        Assert.assertEquals(TestData.projectlocations.forestid1, locations.getForests().getForestid().get(0));
        Assert.assertEquals(TestData.projectlocations.districtid1, locations.getDistricts().getDistrictid().get(0));
        Assert.assertEquals(TestData.projectlocations.stateid1, locations.getStates().getStateid().get(0));
        Assert.assertEquals(TestData.projectlocations.countyid1, locations.getCounties().getCountyid().get(0));

        // Make sure the list is populated and that our entity is in it
        Assert.assertTrue(locationslist.getLocations().size()>0);
        Boolean locfound = false;
        for (Locations l : locationslist.getLocations()) {
            if (l.getProjectid().equalsIgnoreCase(TestData.projects.id1))
                locfound = true;
        }
        Assert.assertTrue(locfound);
    }

    private void confirmLocations2()
    {
        Locations locations = null;
        Locationslist locationslist = null;
        try {
            locations = client.getProjectLocations(TestData.projects.id1);
            locationslist = client.getByUnit(TestData.projectlocations.forestid1);
        } catch (Exception ex) {
            Logger.getLogger(ProjectLocationTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // make sure the locations we posted are present in the GET response
        Assert.assertEquals(TestData.projects.id1, locations.getProjectid());
        Assert.assertEquals(TestData.projectlocations.description2, locations.getLocationdesc());
        Assert.assertEquals(TestData.projectlocations.legaldesc2, locations.getLocationlegaldesc());
        Assert.assertEquals((Double) TestData.projectlocations.latitude1, (Double) locations.getLatitude());
        Assert.assertEquals((Double) TestData.projectlocations.longitude1, (Double) locations.getLongitude());

        Assert.assertEquals(TestData.projectlocations.regionid1, locations.getRegions().getRegionid().get(0));
        Assert.assertEquals(TestData.projectlocations.forestid1, locations.getForests().getForestid().get(0));
        Assert.assertEquals(TestData.projectlocations.districtid1, locations.getDistricts().getDistrictid().get(0));
        Assert.assertEquals(TestData.projectlocations.stateid1, locations.getStates().getStateid().get(0));
        Assert.assertEquals(TestData.projectlocations.countyid1, locations.getCounties().getCountyid().get(0));

        // Make sure the list is populated and that our entity is in it
        Assert.assertTrue(locationslist.getLocations().size()>0);
        Boolean locfound = false;
        for (Locations l : locationslist.getLocations()) {
            if (l.getProjectid().equalsIgnoreCase(TestData.projects.id1))
                locfound = true;
        }
        Assert.assertTrue(locfound);
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        client.putProjectLocations( TestData.projects.id1,
                                    "",
                                    null,
                                    null,
                                    null,
                                    new ArrayList<String>(),
                                    new ArrayList<String>(),
                                    new ArrayList<String>(),
                                    new ArrayList<String>(),
                                    new ArrayList<String>());
    }
    
    
    @BeforeClass
    public static void  populateDependencies()
    {
        ProjectClient projclient = null;
        try {
            projclient = new ProjectClient();
        } catch (Exception ex) {
            Logger.getLogger(ProjectLocationTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Project Locations rely on Projects. Pre-populate a Project.
        ArrayList<String> purposeids1 = new ArrayList<String>();
        purposeids1.add(TestData.projects.purposeid1);
        purposeids1.add(TestData.projects.purposeid2);
        purposeids1.add(TestData.projects.purposeid3);
        try {
            projclient.postProject(TestData.projects.id1, TestData.projects.type1,
                    TestData.projects.name1, TestData.projects.unitcode1, TestData.projects.description1,
                    TestData.projects.lastupdate1, TestData.projects.commentreg1, TestData.projects.wwwlink1,
                    TestData.projects.wwwsummary1, TestData.projects.wwwpub1, TestData.projects.analysistypeid1,
                    TestData.projects.statusid1, TestData.projects.contactname1, TestData.projects.contactphone1,
                    TestData.projects.contactemail1, purposeids1, TestData.projects.expirationdate1, TestData.projects.projectdocumentid1,
                    TestData.projects.sopapub, TestData.projects.sopanew, TestData.projects.sopaheader, TestData.projects.esd, TestData.projects.cenodecision);
        } catch (IOException ex) {
            Logger.getLogger(ProjectLocationTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @AfterClass
    public static void  cleanupDependencies()
    {
        ProjectClient projclient = null;
        try {
            projclient = new ProjectClient();
        } catch (Exception ex) {
            Assert.fail();
        }

        // Clean up the Project
        try {
            projclient.deleteProject("nepa", TestData.projects.id1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectLocationTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }
}