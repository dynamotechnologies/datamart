package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "CommentPhases")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CommentPhases.findAll", query = "SELECT c FROM CommentPhases c"),
    @NamedQuery(name = "CommentPhases.findById", query = "SELECT c FROM CommentPhases c WHERE c.id = :id"),
    @NamedQuery(name = "CommentPhases.findByPhaseId", query = "SELECT c FROM CommentPhases c WHERE c.phaseId = :phaseId"),
    @NamedQuery(name = "CommentPhases.findByName", query = "SELECT c FROM CommentPhases c WHERE c.name = :name"),
    @NamedQuery(name = "CommentPhases.findByStartDate", query = "SELECT c FROM CommentPhases c WHERE c.startDate = :startDate"),
    @NamedQuery(name = "CommentPhases.findByFinishDate", query = "SELECT c FROM CommentPhases c WHERE c.finishDate = :finishDate"),
    @NamedQuery(name = "CommentPhases.findByLetterCount", query = "SELECT c FROM CommentPhases c WHERE c.letterCount = :letterCount"),
    @NamedQuery(name = "CommentPhases.findByLetterUniqueCount", query = "SELECT c FROM CommentPhases c WHERE c.letterUniqueCount = :letterUniqueCount"),
    @NamedQuery(name = "CommentPhases.findByLetterMastersCount", query = "SELECT c FROM CommentPhases c WHERE c.letterMastersCount = :letterMastersCount"),
    @NamedQuery(name = "CommentPhases.findByLetterFormCount", query = "SELECT c FROM CommentPhases c WHERE c.letterFormCount = :letterFormCount"),
    @NamedQuery(name = "CommentPhases.findByLetterFormPlusCount", query = "SELECT c FROM CommentPhases c WHERE c.letterFormPlusCount = :letterFormPlusCount"),
    @NamedQuery(name = "CommentPhases.findByLetterFormDupeCount", query = "SELECT c FROM CommentPhases c WHERE c.letterFormDupeCount = :letterFormDupeCount")})
public class CommentPhases implements Serializable {
    @Column(name =     "StartDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name =     "FinishDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date finishDate;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "PhaseId")
    private int phaseId;
    @Column(name = "Name")
    private String name;
    @Column(name = "LetterCount")
    private Integer letterCount;
    @Column(name = "LetterUniqueCount")
    private Integer letterUniqueCount;
    @Column(name = "LetterMastersCount")
    private Integer letterMastersCount;
    @Column(name = "LetterFormCount")
    private Integer letterFormCount;
    @Column(name = "LetterFormPlusCount")
    private Integer letterFormPlusCount;
    @Column(name = "LetterFormDupeCount")
    private Integer letterFormDupeCount;
    @JoinColumn(name = "CaraId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private CARAProjects cARAProjects;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "commentPhases")
    private Collection<CARAPhaseDocs> cARAPhaseDocsCollection;

    public CommentPhases() {
    }

    public CommentPhases(Integer id) {
        this.id = id;
    }

    public CommentPhases(Integer id, int phaseId) {
        this.id = id;
        this.phaseId = phaseId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getPhaseId() {
        return phaseId;
    }

    public void setPhaseId(int phaseId) {
        this.phaseId = phaseId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLetterCount() {
        return letterCount;
    }

    public void setLetterCount(Integer letterCount) {
        this.letterCount = letterCount;
    }

    public Integer getLetterUniqueCount() {
        return letterUniqueCount;
    }

    public void setLetterUniqueCount(Integer letterUniqueCount) {
        this.letterUniqueCount = letterUniqueCount;
    }

    public Integer getLetterMastersCount() {
        return letterMastersCount;
    }

    public void setLetterMastersCount(Integer letterMastersCount) {
        this.letterMastersCount = letterMastersCount;
    }

    public Integer getLetterFormCount() {
        return letterFormCount;
    }

    public void setLetterFormCount(Integer letterFormCount) {
        this.letterFormCount = letterFormCount;
    }

    public Integer getLetterFormPlusCount() {
        return letterFormPlusCount;
    }

    public void setLetterFormPlusCount(Integer letterFormPlusCount) {
        this.letterFormPlusCount = letterFormPlusCount;
    }

    public Integer getLetterFormDupeCount() {
        return letterFormDupeCount;
    }

    public void setLetterFormDupeCount(Integer letterFormDupeCount) {
        this.letterFormDupeCount = letterFormDupeCount;
    }

    public CARAProjects getCARAProjects() {
        return cARAProjects;
    }

    public void setCARAProjects(CARAProjects cARAProjects) {
        this.cARAProjects = cARAProjects;
    }

    @XmlTransient
    public Collection<CARAPhaseDocs> getCARAPhaseDocsCollection() {
        return cARAPhaseDocsCollection;
    }

    public void setCARAPhaseDocsCollection(Collection<CARAPhaseDocs> cARAPhaseDocsCollection) {
        this.cARAPhaseDocsCollection = cARAPhaseDocsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CommentPhases)) {
            return false;
        }
        CommentPhases other = (CommentPhases) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CommentPhases[ id=" + id + " ]";
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

}
