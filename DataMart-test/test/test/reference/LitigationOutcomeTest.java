package test.reference;

import client.LitigationOutcomeClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.litigationoutcome.*;


public class LitigationOutcomeTest {
    private LitigationOutcomeClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new LitigationOutcomeClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postLitOutcome(TestData.litoutcomes.id1, TestData.litoutcomes.name2);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Litigationoutcome outcome = null;
        Litigationoutcomes outcomelist = null;
        try {
            outcome = client.getLitOutcome(TestData.litoutcomes.id1);
            outcomelist = client.getLitOutcomeList();
        } catch (IOException ex) {
            Logger.getLogger(LitigationOutcomeTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.litoutcomes.id1, outcome.getId());
        Assert.assertEquals(TestData.litoutcomes.name1, outcome.getName());
        Assert.assertTrue(outcomelist.getLitigationoutcome().get(0).getId()>0);
        Assert.assertTrue(outcomelist.getLitigationoutcome().get(0).getName().length()>0);

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        Litigationoutcome outcome = null;
        try {
            client.putLitOutcome(TestData.litoutcomes.id1, TestData.litoutcomes.name2);
            outcome = client.getLitOutcome(TestData.litoutcomes.id1);
        } catch (Exception ex) {
            Logger.getLogger(LitigationOutcomeTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.litoutcomes.name2, outcome.getName());

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getLitOutcome(TestData.litoutcomes.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteLitOutcome(TestData.litoutcomes.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }

    private void post()
    {
        try {
            client.postLitOutcome(TestData.litoutcomes.id1, TestData.litoutcomes.name1);
        } catch (IOException ex) {
            Logger.getLogger(LitigationOutcomeTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteLitOutcome(TestData.litoutcomes.id1);
        } catch (IOException ex) {
            Logger.getLogger(LitigationOutcomeTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteLitOutcome(TestData.litoutcomes.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }
}