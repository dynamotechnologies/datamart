package stateless;

import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.specialauthority.*;
import beanutil.DataMartException;

/*
 *
 * The Manager beans are responsible for:
 *   1) Implementing business logic and
 *   2) Translating front-end resources to back-end database entities.
 *
 * Often the resources will consist of data from numerous database tables. It is the manager's responsibility
 *   to ensure that these front-end resources can be accessed as though they were cut from a single piece of wood.
 *
 */
@Remote
public interface SpecialAuthorityManagerRemote {
    public void create(Specialauthority resource) throws DataMartException;
    public Specialauthority get(String id) throws DataMartException;
    public void update(Specialauthority resource) throws DataMartException;
    public void delete(String id) throws DataMartException;
    public Specialauthorities getAll() throws DataMartException;
    public Boolean ping();
}