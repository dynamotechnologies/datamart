package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.litigation.*;

import beanutil.*;
import java.util.Iterator;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;


@Stateless(mappedName="LitigationManagerRemote")
public class LitigationManager implements LitigationManagerRemote
{
    @PersistenceContext
    private EntityManager em;


    @Override
    public void create(Litigation resource)
             throws DataMartException
    {
        entity.Litigations entity = createEntity(resource);
        
        // Check for existing entity with this ID
        entity.Litigations badentity = getEntity(entity.getId());
        if (badentity != null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS,
                    "Litigation with id '"+entity.getId()+"' already exists.");

        em.persist(entity);
    }


    @Override
    public Litigation get(Integer id)
            throws DataMartException
    {
        entity.Litigations entity = getEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Litigation with id '"+id+"' does not exist.");

        return createResource(entity);
    }


    public Litigations getAllByDecisionId(Integer decisionid)
            throws DataMartException
    {
        // Retrieve all entities from the database
        Query query = em.createQuery("SELECT l FROM Litigations l JOIN l.decisions d WHERE d.id= :decisionid");
        query.setParameter("decisionid", decisionid);
        List entitylist =  query.getResultList();

        if (entitylist==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "No litigations found for decision '"+decisionid+"'.");

        // Loop over the database collection. Each row becomes a new object in the response list
        Iterator it = entitylist.iterator();
        Litigations responselist = new Litigations();
        while (it.hasNext()) {
            responselist.getLitigation().add(createResource( (entity.Litigations)it.next() ));
        }
        return responselist;
    }


    @Override
    public void update(Litigation resource)
             throws DataMartException
    {
        entity.Litigations entity = createEntity(resource);

        // Query for the (hopefully) existing entity. Complain if not found.
        if (getEntity(entity.getId())==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND, "Nonexistent Litigation cannot be updated.");

        // Update the entity
        entity = em.merge(entity);
    }

    @Override
    public void delete(Integer id)
             throws DataMartException
    {
        // Verify that this entity exists
        entity.Litigations entity = getEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Could not delete nonexistent Litigation with id '"+id+"'.");

        em.remove(entity);
    }

    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     *
     * This method does the mapping from entity object to jaxb object
     */
    protected static Litigation createResource(entity.Litigations entity)
            throws DataMartException
    {
        Litigation resource = new Litigation();
        resource.setId(entity.getId());
        resource.setDecisionid(entity.getDecisions().getId());
        resource.setCasename(entity.getCaseName());
        if(null != entity.getRefLitigationStatuses())
        resource.setStatus(entity.getRefLitigationStatuses().getId());
        if(null !=entity.getRefLitigationOutcomes())
        resource.setOutcome(entity.getRefLitigationOutcomes().getId());
        resource.setClosed(DataMartBeanUtil.getXMLGregorianFromDateTime(entity.getClosedDate()));

        return resource;
    }

    /*
     *
     * This method does the mapping from jaxb object to entity object
     */
    private entity.Litigations createEntity(Litigation resource)
            throws DataMartException
    {
        entity.Litigations entity = new entity.Litigations();

        entity.setId(resource.getId());
        entity.Decisions decision = (entity.Decisions)DataMartBeanUtil.getEntityById(em, "Decisions", resource.getDecisionid(), "Decision");
        entity.setDecisions(decision);
        entity.setCaseName(resource.getCasename());
        entity.RefLitigationStatuses status = (entity.RefLitigationStatuses)DataMartBeanUtil.getEntityById(em, "RefLitigationStatuses", resource.getStatus(), "LitigationStatus");
        entity.setRefLitigationStatuses(status);

        if (resource.getOutcome() != null)
        {
            entity.RefLitigationOutcomes outcome = (entity.RefLitigationOutcomes) DataMartBeanUtil.getEntityById(em,
                "RefLitigationOutcomes", resource.getOutcome(), "LitigationOutcome");
            entity.setRefLitigationOutcomes(outcome);
        }
        entity.setClosedDate(DataMartBeanUtil.getDateFromXMLGregorian(resource.getClosed()));

        return entity;
    }


    // Accepts ID parameter, and returns the associated entity,
    //   or null if it doesn't exist
    private entity.Litigations getEntity(Integer id)
            throws DataMartException
    {
        return em.find(entity.Litigations.class, id);
    }
}