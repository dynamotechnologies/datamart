package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.nepacategoricalexclusion.*;


public class NepaCategoricalExclusionClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public NepaCategoricalExclusionClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.nepacategoricalexclusion", "NepaCategoricalExclusion.xsd");
    }

    public void postNepaCategoricalExclusion(String projectid, int id)
                throws IOException
    {
        Nepacategoricalexclusion nce = new Nepacategoricalexclusion();
        nce.setProjectid(projectid);
        nce.setCategoricalexclusionid(id);
        postNepaCategoricalExclusion(nce);
    }


    public void postNepaCategoricalExclusion(Nepacategoricalexclusion nce)
            throws IOException
    {
        post(objectfactory.createNepacategoricalexclusion(nce), "/projects/nepa/" + nce.getProjectid() + "/nepacategoricalexclusions");
    }


    public Nepacategoricalexclusion getNepaCategoricalExclusion(String projectid, int id)
            throws IOException
    {
        return get("/projects/nepa/" + projectid + "/nepacategoricalexclusions/" + id, Nepacategoricalexclusion.class);
    }


    public Nepacategoricalexclusions getNepaCategoricalExclusionList(String projectid)
            throws IOException
    {
        return get("/projects/nepa/" + projectid + "/nepacategoricalexclusions", Nepacategoricalexclusions.class);
    }

    
    public void deleteNepaCategoricalExclusion(String projectid, int id)
            throws IOException
    {
        delete("/projects/nepa/" + projectid + "/nepacategoricalexclusions/" + id);
    }
}