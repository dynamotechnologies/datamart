package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "RefMilestones")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RefMilestones.findAll", query = "SELECT r FROM RefMilestones r"),
    @NamedQuery(name = "RefMilestones.findById", query = "SELECT r FROM RefMilestones r WHERE r.id = :id"),
    @NamedQuery(name = "RefMilestones.findByName", query = "SELECT r FROM RefMilestones r WHERE r.name = :name")})
public class RefMilestones implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private String id;
    @Basic(optional = false)
    @Column(name = "Name")
    private String name;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refMilestones")
    private Collection<ProjectMilestones> projectMilestonesCollection;

    public RefMilestones() {
    }

    public RefMilestones(String id) {
        this.id = id;
    }

    public RefMilestones(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Collection<ProjectMilestones> getProjectMilestonesCollection() {
        return projectMilestonesCollection;
    }

    public void setProjectMilestonesCollection(Collection<ProjectMilestones> projectMilestonesCollection) {
        this.projectMilestonesCollection = projectMilestonesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RefMilestones)) {
            return false;
        }
        RefMilestones other = (RefMilestones) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RefMilestones[ id=" + id + " ]";
    }

}
