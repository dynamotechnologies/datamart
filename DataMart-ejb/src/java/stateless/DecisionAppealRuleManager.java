package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import entity.DecisionAppealRules;
import us.fed.fs.www.nepa.schema.decisionappealrule.*;

import beanutil.*;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless(mappedName="DecisionAppealRuleManagerRemote")
public class DecisionAppealRuleManager implements DecisionAppealRuleManagerRemote
{
    @PersistenceContext
    private EntityManager em;


    @Override
    public void create(Decisionappealrule resource)
             throws DataMartException
    {
        entity.DecisionAppealRules entity = createEntity(resource);
        
        // Check for existing entity with this ID
        DecisionAppealRules oldentity = getEntity(resource.getDecisionid(), resource.getAppealrule());
        if (oldentity != null)
            entity.setId(oldentity.getId());

        em.merge(entity);
    }


    @Override
    public Decisionappealrule get(int decisionid, String id)
            throws DataMartException
    {
        entity.DecisionAppealRules entity = getEntity(decisionid, id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "DecisionAppealRule with id '"+id+"' is not associated with decision '"+decisionid+"'.");

        return createResource(entity);
    }


    @Override
    public Decisionappealrules getList(int decisionid)
            throws DataMartException
    {
        Decisionappealrules rval = new Decisionappealrules();

        // Get the Decision
        entity.Decisions d = (entity.Decisions) DataMartBeanUtil.getEntityById(em,
                "Decisions", decisionid, "Decision");
        if (d==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Decision with id '" + decisionid + "' does not exist.");

        Query query = em.createQuery("SELECT r FROM DecisionAppealRules r WHERE r.decisions = :decision");
        query.setParameter("decision", d);

        @SuppressWarnings("unchecked")
        List<entity.DecisionAppealRules> entitylist = (List<entity.DecisionAppealRules>) query.getResultList();

        for (entity.DecisionAppealRules dar: entitylist) {
            rval.getDecisionappealrule().add(createResource(dar));
        }

        return rval;
    }


    @Override
    public void delete(int decisionid, String id)
             throws DataMartException
    {
        // Verify that this entity exists
        entity.DecisionAppealRules entity = getEntity(decisionid, id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "DecisionAppealRule with id '"+id+"' is not associated with decision '"+decisionid+"'.");

        em.remove(entity);
    }

    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     *
     * This method does the mapping from entity object to jaxb object
     */
    protected static Decisionappealrule createResource(entity.DecisionAppealRules entity)
            throws DataMartException
    {
        Decisionappealrule resource = new Decisionappealrule();
        resource.setAppealrule(entity.getRefAppealRules().getId());
        resource.setDecisionid(entity.getDecisions().getId());

        return resource;
    }

    /*
     *
     * This method does the mapping from jaxb object to entity object
     */
    private entity.DecisionAppealRules createEntity(Decisionappealrule resource)
            throws DataMartException
    {
        entity.DecisionAppealRules entity = new entity.DecisionAppealRules();
        entity.Decisions decision = (entity.Decisions)DataMartBeanUtil.getEntityById(em, "Decisions", resource.getDecisionid(), "Decision");
        entity.setDecisions(decision);
        entity.RefAppealRules appealrule = (entity.RefAppealRules)DataMartBeanUtil.getEntityById(em, "RefAppealRules", resource.getAppealrule(), "Appeal Rule");
        entity.setRefAppealRules(appealrule);

        return entity;
    }


    // Accepts ID parameter, and returns the associated entity,
    //   or null if it doesn't exist
    private entity.DecisionAppealRules getEntity(Integer decisionid, String appealruleid)
            throws DataMartException
    {
        // Get the Decision
        entity.Decisions d = (entity.Decisions) DataMartBeanUtil.getEntityById(em,
                "Decisions", decisionid, "Decision");
        if (d==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Decision with id '" + decisionid + "' does not exist.");

        // Get the Appeal Rule
        entity.RefAppealRules r = (entity.RefAppealRules) DataMartBeanUtil.getEntityById(em,
                "RefAppealRules", appealruleid, "Reference Appeal Rule");
        if (r==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Reference Appeal Rule with id '" + appealruleid + "' does not exist.");

        Query query = em.createQuery("SELECT r FROM DecisionAppealRules r WHERE r.decisions = :decision AND r.refAppealRules = :appealrule");
        query.setParameter("decision", d);
        query.setParameter("appealrule", r);
        return (entity.DecisionAppealRules)DataMartBeanUtil.getEntityByQueryAllowNull(
                query, "Found multiple DecisionAppealRules for decisionid "+decisionid+" and appealruleid "+appealruleid);
    }
}