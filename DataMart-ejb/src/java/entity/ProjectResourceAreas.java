package entity;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "ProjectResourceAreas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProjectResourceAreas.findAll", query = "SELECT p FROM ProjectResourceAreas p"),
    @NamedQuery(name = "ProjectResourceAreas.findById", query = "SELECT p FROM ProjectResourceAreas p WHERE p.id = :id"),
    @NamedQuery(name = "ProjectResourceAreas.findByOrderTag", query = "SELECT p FROM ProjectResourceAreas p WHERE p.orderTag = :orderTag")})
public class ProjectResourceAreas implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Column(name = "OrderTag")
    private Integer orderTag;
    @JoinColumn(name = "ResourceAreaId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private ResourceAreas resourceAreas;
    @JoinColumn(name = "ProjectId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Projects projects;

    public ProjectResourceAreas() {
    }

    public ProjectResourceAreas(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOrderTag() {
        return orderTag;
    }

    public void setOrderTag(Integer orderTag) {
        this.orderTag = orderTag;
    }

    public ResourceAreas getResourceAreas() {
        return resourceAreas;
    }

    public void setResourceAreas(ResourceAreas resourceAreas) {
        this.resourceAreas = resourceAreas;
    }

    public Projects getProjects() {
        return projects;
    }

    public void setProjects(Projects projects) {
        this.projects = projects;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProjectResourceAreas)) {
            return false;
        }
        ProjectResourceAreas other = (ProjectResourceAreas) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ProjectResourceAreas[ id=" + id + " ]";
    }

}
