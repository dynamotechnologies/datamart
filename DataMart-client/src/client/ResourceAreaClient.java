package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.resourcearea.*;


public class ResourceAreaClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public ResourceAreaClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.resourcearea", "ResourceArea.xsd");
    }


    public void postResourceArea(Integer resourceareaid, String unitid, String description)
            throws IOException
    {
        Resourcearea resourcearea = new Resourcearea();
        resourcearea.setResourceareaid(resourceareaid);
        resourcearea.setDescription(description);
        resourcearea.setUnitid(unitid);
        postResourceArea(resourcearea);
    }


    public void postResourceArea(Resourcearea resourcearea)
            throws IOException
    {
        post(objectfactory.createResourcearea(resourcearea), "/resourceareas");
    }


    public Resourcearea getResourceArea(Integer resourceareaid)
            throws IOException
    {
        return get("/resourceareas/" + resourceareaid, Resourcearea.class);
    }

/* These list APIs don't exist yet
    public Resourceareas getResourceAreasByProject(String projecttype, String projectid)
            throws IOException
    {
        return get("/resourceareas/project/"+projecttype+"/"+projectid, Resourceareas.class);
    }
*/
    
    public Resourceareas getResourceAreasByUnit(String unitid)
            throws IOException
    {
        return get("/resourceareas/unit/"+unitid, Resourceareas.class);
    }

    public void putResourceArea(Integer resourceareaid, String unitid, String description)
            throws IOException
    {
        Resourcearea resourcearea = new Resourcearea();
        resourcearea.setResourceareaid(resourceareaid);
        resourcearea.setUnitid(unitid);
        resourcearea.setDescription(description);
        putResourceArea(resourcearea);
    }


    public void putResourceArea(Resourcearea resourcearea)
            throws IOException
    {
        put(objectfactory.createResourcearea(resourcearea), "/resourceareas/" + resourcearea.getResourceareaid());
    }


    public void deleteResourceArea(Integer resourceareaid)
            throws IOException
    {
        delete("/resourceareas/" + resourceareaid);
    }

}
