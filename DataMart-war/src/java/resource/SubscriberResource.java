package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.SubscriberManagerRemote;
import us.fed.fs.www.nepa.schema.subscriber.ObjectFactory;
import us.fed.fs.www.nepa.schema.subscriber.Subscriber;
import us.fed.fs.www.nepa.schema.subscriber.Subscribers;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;


@Path ("/caraprojects/{caraprojectid}/mailinglist/subscribers")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class SubscriberResource extends AbstractResource
{
    private SubscriberManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public SubscriberResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "Subscriber.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.subscriber");
        MY_PROPS.setProperty("bean", "SubscriberManagerRemote");
        try {
            beanmanager = (SubscriberManagerRemote) init(SubscriberManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("SubscriberResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("SubscriberResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(@PathParam("caraprojectid") int caraprojectid, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;
        try {
            Subscriber subscriber = (Subscriber)unmarshal(inputxml, Subscriber.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+subscriber.getSubscriberid());

            if (subscriber.getCaraprojectid()==null)
                subscriber.setCaraprojectid(caraprojectid);

            if (caraprojectid!=subscriber.getCaraprojectid())
                complainLoudly(new Exception("CARA Project ID in uri must match CARA Project ID in xml."),
                        Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the insert.
            beanmanager.create(subscriber);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        // Return a link to the newly created entity
        return created(uri);
    }


    @GET
    public Response get(@PathParam("caraprojectid") int caraprojectid, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            Subscribers subscribers = beanmanager.getAll(caraprojectid);
            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Subscribers> jaxbelement = objectfactory.createSubscribers(subscribers);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @GET
    @Path("/{id}")
    public Response get(@PathParam("caraprojectid") int caraprojectid, @PathParam("id") int id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            Subscriber subscriber = beanmanager.get(caraprojectid, id);
            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Subscriber> jaxbelement = objectfactory.createSubscriber(subscriber);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }

    @PUT
    @Path("/{id}")
    public Response put(@PathParam("caraprojectid") int caraprojectid, @PathParam("id") int id, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            Subscriber subscriber = (Subscriber) unmarshal(inputxml, Subscriber.class);

            if (subscriber.getCaraprojectid()==null)
                subscriber.setCaraprojectid(caraprojectid);
            
            if (caraprojectid!=subscriber.getCaraprojectid())
                complainLoudly(new Exception("CARA Project ID in uri must match CARA Project ID in xml."),
                        Response.Status.UNSUPPORTED_MEDIA_TYPE);

            if (id!=subscriber.getSubscriberid())
                complainLoudly(new Exception("Subscriber Id in uri must match Subscriber id in xml."),
                        Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.update(subscriber);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"</location>");
    }


    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("caraprojectid") int caraprojectid, @PathParam("id") int id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(caraprojectid, id);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}