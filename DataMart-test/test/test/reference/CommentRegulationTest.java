package test.reference;

import client.CommentRegulationClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.commentregulation.*;


public class CommentRegulationTest {
    private CommentRegulationClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new CommentRegulationClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postCommentRegulation(TestData.commentregulations.id1, TestData.commentregulations.name2);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Commentregulation commentregulation = null;
        Commentregulations commentregulationlist = null;
        try {
            commentregulation = client.getCommentRegulation(TestData.commentregulations.id1);
            commentregulationlist = client.getCommentRegulationList();
        } catch (IOException ex) {
            Logger.getLogger(CommentRegulationTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.commentregulations.id1, commentregulation.getId());
        Assert.assertEquals(TestData.commentregulations.name1, commentregulation.getName());
        Assert.assertTrue(commentregulationlist.getCommentregulation().get(0).getId().length()>0);
        Assert.assertTrue(commentregulationlist.getCommentregulation().get(0).getName().length()>0);

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        Commentregulation commentregulation = null;
        try {
            client.putCommentRegulation(TestData.commentregulations.id1, TestData.commentregulations.name2);
            commentregulation = client.getCommentRegulation(TestData.commentregulations.id1);
        } catch (Exception ex) {
            Logger.getLogger(CommentRegulationTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.commentregulations.name2, commentregulation.getName());

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getCommentRegulation(TestData.commentregulations.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteCommentRegulation(TestData.commentregulations.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }

    private void post()
    {
        try {
            client.postCommentRegulation(TestData.commentregulations.id1, TestData.commentregulations.name1);
        } catch (IOException ex) {
            Logger.getLogger(CommentRegulationTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteCommentRegulation(TestData.commentregulations.id1);
        } catch (IOException ex) {
            Logger.getLogger(CommentRegulationTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteCommentRegulation(TestData.commentregulations.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }
}