package test.reference;

import client.ConfigClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.config.*;


public class ConfigTest {
    private ConfigClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new ConfigClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postConfig(TestData.configs.key1, TestData.configs.value2);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Config config = null;
        Configs configlist = null;
        try {
            config = client.getConfig(TestData.configs.key1);
            configlist = client.getConfigList();
        } catch (IOException ex) {
            Logger.getLogger(ConfigTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.configs.key1, config.getKey());
        Assert.assertEquals(TestData.configs.value1, config.getValue());
        Assert.assertTrue(configlist.getConfig().get(0).getKey().length()>0);
        Assert.assertTrue(configlist.getConfig().get(0).getValue().length()>0);

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        Config config = null;
        try {
            client.putConfig(TestData.configs.key1, TestData.configs.value2);
            config = client.getConfig(TestData.configs.key1);
        } catch (Exception ex) {
            Logger.getLogger(ConfigTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.configs.value2, config.getValue());

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getConfig(TestData.configs.key1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteConfig(TestData.configs.key1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }

    private void post()
    {
        try {
            client.postConfig(TestData.configs.key1, TestData.configs.value1);
        } catch (IOException ex) {
            Logger.getLogger(ConfigTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteConfig(TestData.configs.key1);
        } catch (IOException ex) {
            Logger.getLogger(ConfigTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteConfig(TestData.configs.key1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }
}