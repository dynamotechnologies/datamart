package test.entity;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import test.TestData;

import client.GoalClient;
import us.fed.fs.www.nepa.schema.goal.*;


public class GoalTest {
    private GoalClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new GoalClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postGoal(
                    TestData.goals.id1,
                    TestData.goals.description1
                    );
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Goal Goal = null;
        try {
            Goal = client.getGoal(TestData.goals.id1);
        } catch (IOException ex) {
            Logger.getLogger(GoalTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertTrue(TestData.goals.id1 == Goal.getGoalid());
        Assert.assertEquals(TestData.goals.description1, Goal.getDescription());

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        try {
            client.putGoal(
                    TestData.goals.id1,
                    TestData.goals.description1
                    );
        } catch (IOException ex) {
            Logger.getLogger(GoalTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        Goal Goal = null;
        try {
            Goal = client.getGoal(TestData.goals.id1);
        } catch (IOException ex) {
            Logger.getLogger(GoalTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        
        Assert.assertTrue(TestData.goals.id1 == Goal.getGoalid());
        Assert.assertEquals(TestData.goals.description1, Goal.getDescription());

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getGoal(TestData.goals.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteGoal(TestData.goals.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }

    private void post()
    {
        try {
            client.postGoal(
                    TestData.goals.id1,
                    TestData.goals.description1
                    );
        } catch (IOException ex) {
            Logger.getLogger(GoalTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteGoal(TestData.goals.id1);
        } catch (IOException ex) {
            Logger.getLogger(GoalTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteGoal(TestData.goals.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }

    
    @BeforeClass
    public static void populateDependencies()
    {
        // No dependencies
    }

    @AfterClass
    public static void cleanupDependencies()
    {
        // No cleanup
    }
}
