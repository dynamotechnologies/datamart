package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.ProjectActivityManagerRemote;
import us.fed.fs.www.nepa.schema.projectactivity.ObjectFactory;
import us.fed.fs.www.nepa.schema.projectactivity.Projectactivities;
import us.fed.fs.www.nepa.schema.projectactivity.Projectactivity;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Iterator;


@Path("/projects/nepa/{projectid}/activities")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class ProjectActivityResource extends AbstractResource
{
    private ProjectActivityManagerRemote beanmanager;

    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public ProjectActivityResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "ProjectActivity.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.projectactivity");
        MY_PROPS.setProperty("bean", "ProjectActivityManagerRemote");
        try {
            beanmanager = (ProjectActivityManagerRemote) init(ProjectActivityManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("ProjectActivityResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("ProjectActivityResource failed to start. Could not ping the bean manager!");
    }


    @GET
    public Response getActivities(  @PathParam("projectid") String projectid,
                                    @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get a dump of all the Project Activities in the datamart
            Projectactivities activitylist = beanmanager.getByProject(projectid);

            // Return 404 if none exist
            if (activitylist.getProjectactivity().isEmpty())
                notFound();

            // Get the query results in a JAXBElement<Milestones> object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Projectactivities> jaxbelement = objectfactory.createProjectactivities(activitylist);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }

    @GET
    @Path("/{id}")
    public Response getActivity(    @PathParam("projectid") String projectid,
                                    @PathParam("id") String id,
                                    @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            Projectactivity activity = beanmanager.get(projectid, id);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Projectactivity> jaxbelement = objectfactory.createProjectactivity(activity);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @PUT
    public Response put(    @PathParam("projectid") String projectid, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        Projectactivities activities = null;
        try {
            activities = (Projectactivities) unmarshal(inputxml, Projectactivities.class);
            Iterator activityiterator = activities.getProjectactivity().iterator();
            // loop through all of the projectactivity objects, and confirm that all project id's match the one in the url if specified
            while (activityiterator.hasNext()) {
                Projectactivity activity = (Projectactivity) activityiterator.next();
                
                // project id may be excluded from the xml...
                if (activity.getProjectid()==null || activity.getProjectid().length()==0)
                    activity.setProjectid(projectid);

                // ...but if specified it must match the url pattern
                if (!projectid.equalsIgnoreCase(activity.getProjectid()))
                    complainLoudly(new Exception("Project Id in uri must match Project Id in xml. Failed on activity with id " + activity.getActivityid() + "."),
                            Response.Status.UNSUPPORTED_MEDIA_TYPE);
            }

            // All clear. Now do the update.
            beanmanager.replaceAll(activities);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"</location>");
    }


    @PUT
    @Path("/{id}")
    public Response put(    @PathParam("projectid") String projectid,
                            @PathParam("id") String id,
                            InputStream inputxml,
                            @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        Projectactivity activity = null;
        try {
            activity = (Projectactivity) unmarshal(inputxml, Projectactivity.class);

            // project id may be excluded from the xml...
            if (activity.getProjectid()==null || activity.getProjectid().length()>0)
                activity.setProjectid(projectid);
            
            // ...but if specified it must match the url pattern
            if (!projectid.equalsIgnoreCase(activity.getProjectid()))
                complainLoudly(new Exception("Project Id in uri must match Project Id in xml."),
                        Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // activity id must match the url pattern
            if (!id.equalsIgnoreCase(activity.getActivityid()))
                complainLoudly(new Exception("Id in uri must match id in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.merge(activity);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"/"+activity.getActivityid()+"</location>");
    }

    @DELETE
    @Path("/{id}")
    public Response delete( @PathParam("projectid") String projectid,
                            @PathParam("id") String id,
                            @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(projectid, id);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}