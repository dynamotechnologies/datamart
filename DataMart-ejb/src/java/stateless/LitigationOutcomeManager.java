package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.litigationoutcome.*;

import beanutil.*;
import java.util.Iterator;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;


@Stateless(mappedName="LitigationOutcomeManagerRemote")
public class LitigationOutcomeManager implements LitigationOutcomeManagerRemote
{
    @PersistenceContext
    private EntityManager em;


    @Override
    public void create(Litigationoutcome resource)
             throws DataMartException
    {
        entity.RefLitigationOutcomes entity = createEntity(resource);

        // Check for existing entity with this ID
        if (getEntity(entity.getId()) != null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS,
                    "Litigation Outcome with id '"+entity.getId()+"' already exists.");

        em.persist(entity);
    }


    @Override
    public Litigationoutcome get(Integer id)
            throws DataMartException
    {
        entity.RefLitigationOutcomes entity = getEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Litigation Outcome with id '"+id+"' does not exist.");

        return createResource(entity);
    }


    public Litigationoutcomes getAll()
            throws DataMartException
    {
        // Retrieve all entities from the database
        Query query = em.createQuery("SELECT o FROM RefLitigationOutcomes o");
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Litigationoutcomes responselist = new Litigationoutcomes();
        while (it.hasNext()) {
            responselist.getLitigationoutcome().add(createResource( (entity.RefLitigationOutcomes)it.next() ));
        }
        return responselist;
    }


    @Override
    public void update(Litigationoutcome resource)
             throws DataMartException
    {
        entity.RefLitigationOutcomes entity = createEntity(resource);

        // Query for the (hopefully) existing entity. Complain if not found.
        if (getEntity(entity.getId())==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND, "Nonexistent Litigation Outcome cannot be updated.");

        // Update the entity
        entity = em.merge(entity);
    }

    @Override
    public void delete(Integer id)
             throws DataMartException
    {
        // Verify that this entity exists
        entity.RefLitigationOutcomes entity = getEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Could not delete nonexistent Litigation Outcome with id '"+id+"'.");

        em.remove(entity);
    }

    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     *
     * This method does the mapping from entity object to jaxb object
     */
    protected static Litigationoutcome createResource(entity.RefLitigationOutcomes entity)
            throws DataMartException
    {
        Litigationoutcome resource = new Litigationoutcome();
        resource.setId(entity.getId());
        resource.setName(entity.getName());
        return resource;
    }

    /*
     *
     * This method does the mapping from jaxb object to entity object
     */
    private entity.RefLitigationOutcomes createEntity(Litigationoutcome resource)
            throws DataMartException
    {
        entity.RefLitigationOutcomes entity = new entity.RefLitigationOutcomes();
        entity.setId(resource.getId());
        entity.setName(resource.getName());
        return entity;
    }


    // Accepts ID parameter, and returns the associated entity,
    //   or null if it doesn't exist
    private entity.RefLitigationOutcomes getEntity(Integer id)
            throws DataMartException
    {
        return em.find(entity.RefLitigationOutcomes.class, id);
    }
}