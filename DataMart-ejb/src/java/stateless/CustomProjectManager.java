package stateless;

import beanutil.DataMartBeanUtil;
import beanutil.DataMartException;
import java.util.Iterator;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;
import us.fed.fs.www.nepa.schema.customproject.Customproject;
import us.fed.fs.www.nepa.schema.customproject.Customprojects;
import us.fed.fs.www.nepa.schema.customproject.Purposeids;
import us.fed.fs.www.nepa.schema.projectlisting.Projectlisting;
import us.fed.fs.www.nepa.schema.projectlisting.Projectlistings;




@Stateless(mappedName="CustomProjectManagerRemote")
public class CustomProjectManager implements CustomProjectManagerRemote {
    
    @PersistenceContext
    private EntityManager em;

    @Override
    public void create(Customproject resource) throws DataMartException
    {
        validate(resource);
        entity.CustomProjects entity = createEntity(resource);

       // entity.CustomProjects badentity = createEntity(entity.getId());
       // if (badentity!=null)
       //     throw new DataMartException(DataMartException.ALREADY_EXISTS);

        entity = em.merge(entity);
    }
    
    @Override
    public Customproject get(String id) throws DataMartException
    {
        entity.CustomProjects entity = createEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        Customproject response = createResource(entity);
        
        
        return response;
    }
    
    @Override
    public Customprojects getAll() throws DataMartException
    {
        // Retrieve all entities from the database
        // TODO: use named query here. Look through all manager beans for createQuery() and similar methods. Create a named query for
        //       all queries. Also don't query anything from this level. Look at ProjectManager.java and use
        //       the getEntity() method as an example for how this should work.
        //
        Query query = em.createQuery("SELECT p FROM CustomProjects p");
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Customprojects responselist = new Customprojects();
        while (it.hasNext()) {
            entity.CustomProjects entity = (entity.CustomProjects)it.next();
            Customproject resource = createResource(entity);

            responselist.getCustomproject().add(resource);
        }
        return responselist;
    }
    
    @Override    
    public Customprojects getAllForUnit(String unitid) throws DataMartException
    {
                // Confirm that this is a valid unit
        entity.RefUnits unit = (entity.RefUnits) DataMartBeanUtil.getEntityById(em,
                "RefUnits", unitid, "Unit");
        if (unit==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        // Get custom projects for this unit
        Query query1 = em.createQuery("SELECT p FROM CustomProjects p WHERE p.unitId =:id");
        query1.setParameter("id", new Integer(unitid));
        List entitylist1 =  query1.getResultList();
    //    if (!(entitylist1.size()>0) )
   //         throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        // Loop over the database collection. Each row becomes a new object in the response list
        Customprojects responselist = new Customprojects();
        for (Object e : entitylist1) {
            entity.CustomProjects entity = (entity.CustomProjects)e;
            Customproject p = createResource(entity);

            responselist.getCustomproject().add(p);
        }
        
        return responselist;
    }
    
    @Override
    public Projectlistings getListingsByUnit(String unitid) throws DataMartException
    {
                        // Confirm that this is a valid unit
        entity.RefUnits unit = (entity.RefUnits) DataMartBeanUtil.getEntityById(em,
                "RefUnits", unitid, "Unit");
        if (unit==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        // Get custom projects for this unit
        Query query1 = em.createQuery("SELECT p FROM CustomProjects p WHERE p.unitId =:id");
        query1.setParameter("id", new Integer(unitid));
        List entitylist1 =  query1.getResultList();

        // Loop over the database collection. Each row becomes a new object in the response list
        Projectlistings responselist = new Projectlistings();
        for (Object e : entitylist1) {
            entity.CustomProjects entity = (entity.CustomProjects)e;
            Projectlisting p = createListingResource(entity);

            responselist.getProjectlisting().add(p);
        }
        
        return responselist;
    }
    
    
    @Override    
    public void update(Customproject resource) throws DataMartException
    {
        validate(resource);
        entity.CustomProjects entity = createEntity(resource);
        
        if (createEntity(resource.getId())==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        entity = em.merge(entity);
    }
    
    @Override
    public void delete(String id) throws DataMartException
    {
        entity.CustomProjects entity = createEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));
    }
    
    @Override
    public Boolean ping()
    {
        return true;
    }
    
    private Projectlisting createListingResource(entity.CustomProjects entity)
            throws DataMartException
    {

        
        Projectlisting resource = new Projectlisting();
        resource.setArchivedate(DataMartBeanUtil.getXMLGregorianFromDate( entity.getArchiveDate() ));
        
        resource.setDescription(entity.getDescription());
        resource.setId(""+entity.getId());
        if (entity.getIsPublished()!= null)
            resource.setIspublished(entity.getIsPublished().toString());
        else
            resource.setIspublished("false");
        
        
        resource.setName(entity.getName());
        
        
/** Supply a default value in case this field was not supplied by PALS. 
 *  The portal requires this field.  Ideally all custom projects will have a 
 *  'type' specified, but for now it is a generic category.  August 12, 2014 -SRC
 * I
 **/
        if (entity.getType() == null || entity.getType().length() < 1){
            resource.setType("Other");
        }else{
            resource.setType(entity.getType());
        }
        
        us.fed.fs.www.nepa.schema.projectlisting.Purposeids purposeids 
                    = new us.fed.fs.www.nepa.schema.projectlisting.Purposeids();
    //    if (cp.getPu != null)
    //        customproject.setPurposeid(cp.getPurposeId().toString());
                // Put the Purpose ID's into a Purposeids object
        for (Iterator it = entity.getCustomProjectPurposesCollection().iterator(); it.hasNext();)
        {
            entity.CustomProjectPurposes pp = (entity.CustomProjectPurposes)it.next();
            purposeids.getPurposeid().add(pp.getPurposeId().getId());  //.getRefPurposes().getId());
        }
        
        resource.setPurposeids(purposeids);
        
        
        if (entity.getStatusId() != null)
            resource.setStatusid(entity.getStatusId().toString());
        if (entity.getUnitId() != null)
            resource.setUnitid(entity.getUnitId().toString());
        resource.setUrl(entity.getUrl());
        return resource;
        
    }
    
    private Customproject createResource(entity.CustomProjects cp)
            throws DataMartException
    {
        
        Customproject customproject = new Customproject();
        customproject.setArchivedate(DataMartBeanUtil.getXMLGregorianFromDate(cp.getArchiveDate()));
        customproject.setCreatedby(cp.getCreatedBy());
        customproject.setCreateddate(DataMartBeanUtil.getXMLGregorianFromDate(cp.getCreatedDate()));
        customproject.setDescription(cp.getDescription());
        customproject.setId(""+cp.getId());
        if (cp.getIsPublished()!= null)
            customproject.setIspublished(cp.getIsPublished().toString());
        else
            customproject.setIspublished("false");
        customproject.setName(cp.getName());
        customproject.setType(cp.getType());
        
        Purposeids purposeids = new Purposeids();
    //    if (cp.getPu != null)
    //        customproject.setPurposeid(cp.getPurposeId().toString());
                // Put the Purpose ID's into a Purposeids object
        for (Iterator it = cp.getCustomProjectPurposesCollection().iterator(); it.hasNext();)
        {
            entity.CustomProjectPurposes pp = (entity.CustomProjectPurposes)it.next();
            purposeids.getPurposeid().add(pp.getPurposeId().getId());  //.getRefPurposes().getId());
        }
        
        customproject.setPurposeids(purposeids);
       // nepainfo.setPurposeids(purposeids);
        
        
        
        if (cp.getStatusId() != null)
            customproject.setStatusid(cp.getStatusId().toString());
        if (cp.getUnitId() != null)
            customproject.setUnitid(cp.getUnitId().toString());
        customproject.setUrl(cp.getUrl());
        return customproject;
    }
    
     // This version of overloaded method createEntity() accepts a String ID as a parameter, and returns the
    //   associated entity, or null if it doesn't exist
    //
    // TODO: should this method be called getEntity(id) ?
    private entity.CustomProjects createEntity(String id)
            throws DataMartException
    {
        return em.find(entity.CustomProjects.class, new Integer(id));
    }
    
    // This version of overloaded method createEntity() accepts a String ID as a parameter, and returns the
    //   associated entity, or null if it doesn't exist
    //
    private entity.CustomProjects createEntity(Customproject resource)
            throws DataMartException
    {
        
        resource = convertPurposeids(resource);
        
        entity.CustomProjects customProject = new entity.CustomProjects();
        customProject.setId(new Integer(resource.getId()) );
        customProject.setName(resource.getName());
        customProject.setType(resource.getType());
        customProject.setDescription(resource.getDescription());
        customProject.setUrl(resource.getUrl());
        customProject.setUnitId(new Integer(resource.getUnitid()));
        
        if (resource.getStatusid() != null)
            customProject.setStatusId(new Integer(resource.getStatusid()));
        
        if (resource.getIspublished() != null)
            customProject.setIsPublished(new Boolean(resource.getIspublished()));
        else
            customProject.setIsPublished(Boolean.FALSE);
        if (resource.getArchivedate() != null)
            customProject.setArchiveDate(DataMartBeanUtil.getDateFromXMLGregorian(resource.getArchivedate()));
        
        customProject.setCreatedBy(resource.getCreatedby());
        
        if (resource.getCreateddate() != null)
            customProject.setCreatedDate(DataMartBeanUtil.getDateFromXMLGregorian(resource.getCreateddate()));

        
        // The following block populates a jointable. It reads the purposes
        //   specified in the resource and adds each one to the jointable, along with a
        //   reference to this entity.
        Collection<entity.CustomProjectPurposes> joinpurposes = new ArrayList<entity.CustomProjectPurposes>();

        Iterator it = resource.getPurposeids().getPurposeid().iterator();
        // Loop over the purposes specified in this resource
        while (it.hasNext()) {
            // Get the id passed to us in the resource; use it to create a new entity object
            entity.RefPurposes refp = (entity.RefPurposes) DataMartBeanUtil.getEntityById(em,
                    "RefPurposes", it.next(), "Project Purpose");
            // Create a new jointable object to store the result of the join
            entity.CustomProjectPurposes joinpurp = new entity.CustomProjectPurposes();
            // Populate the jointable object...
            joinpurp.setCustomProjectId(customProject);
            joinpurp.setPurposeId(refp);
            // ...and add it to the response list
            joinpurposes.add(joinpurp);
        }
        // Finally, tell the entity object to use this response list
        customProject.setCustomProjectPurposesCollection(joinpurposes);
        
        
        return customProject;
    }

    private void validate(Customproject resource)
            throws DataMartException
    {
        DataMartBeanUtil.confirmNotNull(resource.getName(), "Name");
    }
    
    private Customproject convertPurposeids(Customproject resource)
        throws DataMartException
    {
        Purposeids newpurposeids = new Purposeids();

        
        Purposeids p = resource.getPurposeids();
            if ( (p != null) && p.getPurposeid() != null){
                List<String> pids = p.getPurposeid();
                if (pids != null && pids.size() > 0){
                    Iterator<String> pIter = pids.iterator();
                    
                    while (pIter.hasNext()){
                        String value = pIter.next();
                        if (isInteger(value)){
                            value = getPurposeIdFromPalsId(Integer.parseInt(value));
                        }
                        newpurposeids.getPurposeid().add(value);
                    }
                    
                    
                }
            }
        
        resource.setPurposeids(newpurposeids);
        return resource;
    }
    
    public static boolean isInteger(String s){
        
        try{
            Integer.parseInt(s);
        }catch(NumberFormatException e){
            return false;   
        }
        return true;
    }
    
    public static String getPurposeIdFromPalsId(Integer palsid)
            throws DataMartException
    {
        switch (palsid)
        {
            case 1: return "RO";
            case 2: return "PN";
            case 3: return "RW";
            case 4: return "HR";
            case 5: return "RU";
            case 6: return "WF";
            case 7: return "RG";
            case 8: return "TM";
            case 9: return "VM";
            case 10: return "HF";
            case 11: return "WM";
            case 12: return "MG";
            case 13: return "LM";
            case 14: return "LW";
            case 15: return "SU";
            case 16: return "FC";
            case 17: return "RD";
            case 18: return "FR";
            default: throw new DataMartException("Unknown purpose '" + palsid.toString() + "'.");
        }
    }
    
}
