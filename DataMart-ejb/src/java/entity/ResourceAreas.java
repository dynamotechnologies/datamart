package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "ResourceAreas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ResourceAreas.findAll", query = "SELECT r FROM ResourceAreas r"),
    @NamedQuery(name = "ResourceAreas.findById", query = "SELECT r FROM ResourceAreas r WHERE r.id = :id"),
    @NamedQuery(name = "ResourceAreas.findByDescription", query = "SELECT r FROM ResourceAreas r WHERE r.description = :description")})
public class ResourceAreas implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Column(name = "Description")
    private String description;
    @JoinColumn(name = "UnitId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private RefUnits refUnits;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "resourceAreas")
    private Collection<ProjectResourceAreas> projectResourceAreasCollection;

    public ResourceAreas() {
    }

    public ResourceAreas(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public RefUnits getRefUnits() {
        return refUnits;
    }

    public void setRefUnits(RefUnits refUnits) {
        this.refUnits = refUnits;
    }

    @XmlTransient
    public Collection<ProjectResourceAreas> getProjectResourceAreasCollection() {
        return projectResourceAreasCollection;
    }

    public void setProjectResourceAreasCollection(Collection<ProjectResourceAreas> projectResourceAreasCollection) {
        this.projectResourceAreasCollection = projectResourceAreasCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ResourceAreas)) {
            return false;
        }
        ResourceAreas other = (ResourceAreas) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ResourceAreas[ id=" + id + " ]";
    }

}
