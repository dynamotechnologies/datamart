
-- Fields req'd for CARA 4.5 
ALTER TABLE Projects ADD COLUMN PrimaryProjectManager varchar(255);
ALTER TABLE Projects ADD COLUMN SecondaryProjectManager varchar(255);
ALTER TABLE Projects ADD COLUMN DataEntryPerson varchar(255);
ALTER TABLE Projects ADD COLUMN ContactShortName varchar(255);

ALTER TABLE Objections ADD COLUMN ObjectionRule varchar(255);
