/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.ProjectMeetingNoticeDoc.*;

/**
 *
 * @author gauri
 */

public class ProjectMeetingNoticeDocsClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public ProjectMeetingNoticeDocsClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.ProjectMeetingNoticeDoc", "ProjectMeetingNoticeDoc.xsd");
    }


   


    public void postProjectMeetingNoticeDocs(String projectId,ProjectMeetingNoticeDoc projectMeetingNoticeDoc)
            throws IOException
    {
        post(objectfactory.createProjectMeetingNoticeDoc(projectMeetingNoticeDoc), "/projects/nepa/"+projectId+"/meetingnotices/"+projectMeetingNoticeDoc.getMeetingId()+"/docs");
          //post(objectfactory.createProjectMeetingNoticeDoc(projectMeetingNoticeDoc), "/projects/nepa/33517/containers/updatecontainerdocs" );

    }


    public ProjectMeetingNoticeDocs getProjectMeetingNoticeDocs(String projectId,String meetingId)
            throws IOException
    {
        return get("/projects/nepa/"+projectId+"/meetingnotices/"+meetingId+"/docs/" , ProjectMeetingNoticeDocs.class);
    }



    


    public void putProjectMeetingNoticeDocs(String projectId,ProjectMeetingNoticeDoc projectMeetingNoticeDoc)
            throws IOException
    {
     put(objectfactory.createProjectMeetingNoticeDoc(projectMeetingNoticeDoc), "/projects/nepa/"+projectId+"/meetingnotices/"+projectMeetingNoticeDoc.getMeetingId()+"/docs/" + projectMeetingNoticeDoc.getMeetingDocId());
     //put(objectfactory.createProjectMeetingNoticeDoc(projectMeetingNoticeDoc), "/nepaprojects/100270" );
    }


    public void deleteProjectMeetingNoticeDocs(String projectId,String meetingId, String meetingDocId)
            throws IOException
    {
        delete("/projects/nepa/"+projectId+"/meetingnotices/"+meetingId+"/docs/" + meetingDocId);
    }
    
 
          
}