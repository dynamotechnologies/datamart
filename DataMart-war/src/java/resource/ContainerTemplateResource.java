package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.ContainerTemplateManagerRemote;
import us.fed.fs.www.nepa.schema.containertemplate.Containertemplate;
import us.fed.fs.www.nepa.schema.containertemplate.Containertemplateheaders;
import us.fed.fs.www.nepa.schema.containertemplate.ObjectFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;


@Path ("/containertemplates")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class ContainerTemplateResource extends AbstractResource {

    private ContainerTemplateManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public ContainerTemplateResource()
        throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "ContainerTemplate.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.containertemplate");
        MY_PROPS.setProperty("bean", "ContainerTemplateManagerRemote");
        try
        {
            beanmanager = (ContainerTemplateManagerRemote) init(ContainerTemplateManagerRemote.class);
        } 
        catch (IOException ex)
        {
            log.error(ex.toString());
            throw new ServletException("ContainerTemplateResource failed to start. Could not start the bean manager.");
        }
        
        if (!beanmanager.ping())
        {
            throw new ServletException("ContainerTemplateResource failed to start. Could not ping the bean manager!");
        }
    }

    @POST
    public Response post(InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        int templateid = 0;
        try {
            Containertemplate template = (Containertemplate)unmarshal(inputxml, Containertemplate.class);
            templateid = beanmanager.create(template);
        } 
        catch (DataMartException ex)
        {
            handleException(ex);
        }

        URI uri = null;
        try
        {
            uri = new URI(uriInfo.getAbsolutePath().toString() + "/" + Integer.toString(templateid));
        }
        catch (URISyntaxException ex)
        {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }

        // Return a link to the newly created entity
        return created(uri);
    }


    @GET
    @Path("/{templateid}")
    public Response get(@PathParam("templateid") Integer templateid, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try
        {
            Containertemplate template = beanmanager.get(templateid);
            sw = marshal(new ObjectFactory().createContainertemplate(template));
        } 
        catch (DataMartException ex)
        {
            handleException(ex);
        } 
        catch (JAXBException ex)
        {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }

    @GET
    @Path("/headers")
    public Response getHeaders(
        @QueryParam("start") Integer start,
        @QueryParam("rows") Integer rows,
        @QueryParam("unitid") List<String> unitids,
        @Context UriInfo uriInfo
    )
        throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try
        {
            Containertemplateheaders templates;
            if (unitids.isEmpty())
            {
                templates = beanmanager.getHeaders(start, rows);
            }
            else
            {
                templates = beanmanager.getHeaders(unitids, start, rows);
            }
            sw = marshal(new ObjectFactory().createContainertemplateheaders(templates));
        }
        catch (DataMartException ex)
        {
            handleException(ex);
        }
        catch (JAXBException ex)
        {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }
    
    /*
    @PUT
    public Response put(InputStream inputxml, @Context UriInfo uriInfo)
        throws WebApplicationException
    {
        URI uri = null;
        try 
        {
            Containertemplate template = (Containertemplate)unmarshal(inputxml, Containertemplate.class);
            uri = new URI(uriInfo.getAbsolutePath().toString() + "/" + template.getTemplateid().toString());
            beanmanager.update(template);
        } 
        catch (DataMartException ex)
        {
            handleException(ex);
        }
        catch (URISyntaxException ex)
        {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }

        // Return a link to the updated entity
        return ok("<location>" + uri.toString() + "</location>");
    }
*/

    @DELETE
    @Path("/{templateid}")
    public Response delete(@PathParam("templateid") Integer templateid, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try
        {
            beanmanager.delete(templateid);
        } 
        catch (DataMartException ex)
        {
            handleException(ex);
        }
        return deleted();
    }

}
