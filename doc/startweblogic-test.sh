#!/bin/sh
#
# chkconfig: 2345 95 20
# description: Starts and Stops WebLogic Server
# processname: weblogic
#
#
#

export JAVA_HOME=/usr/lib/jvm/jdk1.6.0_24/jre
export MW_HOME=/app/Oracle/Middleware
export WLDOMAIN=datamart
export DOMAIN_HOME=$MW_HOME/domains/$WLDOMAIN
export ADMINLOG=$MW_HOME/weblogic-admin.log

case $1 in
'start')
(echo 'starting WebLogic:')
cd $DOMAIN_HOME
. $MW_HOME/wlserver_10.3/server/bin/setWLSEnv.sh
$JAVA_HOME/bin/java -Dweblogic.ThreadPoolSize=100 -Dweblogic.MaxMessageSize=30000000 -Xmx4096m -XX:MaxPermSize=4096m weblogic.Server > $ADMINLOG 2>&1 &
echo 'Check the log to verify that the servers started successfully:'
echo $ADMINLOG
echo ''
;;
'stop')
(echo 'stopping WebLogic:')
cd $DOMAIN_HOME
. $MW_HOME/wlserver_10.3/server/bin/setWLSEnv.sh
$JAVA_HOME/bin/java weblogic.Admin -url t3://localhost:7001/ -username datamart -password p1cgorcl SHUTDOWN AdminServer
;;
'status')
ps aux | grep weblogic\.Server | grep -v grep
;;
*)
echo "usage: `basename $0` {start|stop}"
;;
esac

