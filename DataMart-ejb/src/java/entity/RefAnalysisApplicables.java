package entity;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "RefAnalysisApplicables")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RefAnalysisApplicables.findAll", query = "SELECT r FROM RefAnalysisApplicables r"),
    @NamedQuery(name = "RefAnalysisApplicables.findById", query = "SELECT r FROM RefAnalysisApplicables r WHERE r.id = :id"),
    @NamedQuery(name = "RefAnalysisApplicables.findByAnalysisTypeId", query = "SELECT r FROM RefAnalysisApplicables r WHERE r.analysisTypeId = :analysisTypeId"),
    @NamedQuery(name = "RefAnalysisApplicables.findByApplicableRegId", query = "SELECT r FROM RefAnalysisApplicables r WHERE r.applicableRegId = :applicableRegId"),
    @NamedQuery(name = "RefAnalysisApplicables.findByCommentPeriodDays", query = "SELECT r FROM RefAnalysisApplicables r WHERE r.commentPeriodDays = :commentPeriodDays"),
    @NamedQuery(name = "RefAnalysisApplicables.findByObjectionPeriodDays", query = "SELECT r FROM RefAnalysisApplicables r WHERE r.objectionPeriodDays = :objectionPeriodDays"),
    @NamedQuery(name = "RefAnalysisApplicables.findByActive", query = "SELECT r FROM RefAnalysisApplicables r WHERE r.active = :active")})
public class RefAnalysisApplicables implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "AnalysisTypeId")
    private String analysisTypeId;
    @Basic(optional = false)
    @Column(name = "ApplicableRegId")
    private int applicableRegId;
    @Column(name = "CommentPeriodDays")
    private String commentPeriodDays;
    @Column(name = "ObjectionPeriodDays")
    private String objectionPeriodDays;
    @Basic(optional = false)
    @Column(name = "Active")
    private boolean active;

    public RefAnalysisApplicables() {
    }

    public RefAnalysisApplicables(Integer id) {
        this.id = id;
    }

    public RefAnalysisApplicables(Integer id, String analysisTypeId, int applicableRegId, boolean active) {
        this.id = id;
        this.analysisTypeId = analysisTypeId;
        this.applicableRegId = applicableRegId;
        this.active = active;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAnalysisTypeId() {
        return analysisTypeId;
    }

    public void setAnalysisTypeId(String analysisTypeId) {
        this.analysisTypeId = analysisTypeId;
    }

    public int getApplicableRegId() {
        return applicableRegId;
    }

    public void setApplicableRegId(int applicableRegId) {
        this.applicableRegId = applicableRegId;
    }

    public String getCommentPeriodDays() {
        return commentPeriodDays;
    }

    public void setCommentPeriodDays(String commentPeriodDays) {
        this.commentPeriodDays = commentPeriodDays;
    }

    public String getObjectionPeriodDays() {
        return objectionPeriodDays;
    }

    public void setObjectionPeriodDays(String objectionPeriodDays) {
        this.objectionPeriodDays = objectionPeriodDays;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RefAnalysisApplicables)) {
            return false;
        }
        RefAnalysisApplicables other = (RefAnalysisApplicables) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RefAnalysisApplicables[ id=" + id + " ]";
    }

}
