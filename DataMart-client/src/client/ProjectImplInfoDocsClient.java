/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.io.IOException;

import us.fed.fs.www.nepa.schema.ProjectImplInfoDoc.*;

/**
 *
 * @author gauri
 */

public class ProjectImplInfoDocsClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public ProjectImplInfoDocsClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.ProjectImplInfoDoc", "ProjectImplInfoDoc.xsd");
    }


   


    public void postImplInfoDocs(String projectId, ProjectImplInfoDoc projectImplInfoDoc)
            throws IOException
    {
        post(objectfactory.createProjectImplInfoDoc(projectImplInfoDoc), "/projects/nepa/"+projectId+"/ProjectImplInfo/docs");
    }


    public ProjectImplInfoDocs getImplInfoDocs(String projectId)
            throws IOException
    {
        return get("/projects/nepa/"+projectId+"/ProjectImplInfo/docs/" , ProjectImplInfoDocs.class);
    }



    


    public void putImplInfoDocs(String projectId,ProjectImplInfoDoc projectImplInfoDoc)
            throws IOException
    {
        put(objectfactory.createProjectImplInfoDoc(projectImplInfoDoc), "/projects/nepa/"+projectId+"/ProjectImplInfo/docs/" + projectImplInfoDoc.getImplInfoDocId());
    }


    public void deleteImplInfoDocs(String projectId,String docId)
            throws IOException
    {
        delete("/projects/nepa/"+projectId+"/ProjectImplInfo/docs/" + docId);
    }
    
    
          
}