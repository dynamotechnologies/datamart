package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.appeal.*;


public class AppealClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public AppealClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.appeal", "Appeal.xsd");
    }

    public void postAppeal(String id, String projectid, String projecttype, String appellant, String outcomeid, String docid,
            String dismissalid, String responsedate, String ruleid, String statusid, String appadminunit, int decisionid)
            throws IOException
    {
        Appeal appeal = new Appeal();
        appeal.setId(id);
        appeal.setProjectid(projectid);
        appeal.setAppellant(appellant);
        appeal.setOutcomeid(outcomeid);
        appeal.setDocid(docid);
        appeal.setDismissalid(dismissalid);
        appeal.setProjecttype(projecttype);
        appeal.setResponsedate(dateStrToXML(responsedate));
        appeal.setRuleid(ruleid);
        appeal.setStatusid(statusid);
        appeal.setAppadminunit(appadminunit);
        appeal.setDecisionid(Integer.toString(decisionid));
        postAppeal(appeal);
    }


    @Deprecated
    public void postAppeal(String id, String projectid, String projecttype, String appellant, String outcomeid, String docid,
            String dismissalid, String responsedate, String ruleid, String statusid, String appadminunit)
            throws IOException
    {
        Appeal appeal = new Appeal();
        appeal.setId(id);
        appeal.setProjectid(projectid);
        appeal.setAppellant(appellant);
        appeal.setOutcomeid(outcomeid);
        appeal.setDocid(docid);
        appeal.setDismissalid(dismissalid);
        appeal.setProjecttype(projecttype);
        appeal.setResponsedate(dateStrToXML(responsedate));
        appeal.setRuleid(ruleid);
        appeal.setStatusid(statusid);
        appeal.setAppadminunit(appadminunit);
        appeal.setDecisionid("");
        postAppeal(appeal);
    }


    public void postAppeal(Appeal appeal)
            throws IOException
    {
        post(objectfactory.createAppeal(appeal), "/appeals");
    }


    public Appeal getAppeal(String id)
            throws IOException
    {
        return get("/appeals/" + id, Appeal.class);
    }


    public Appeals getAppealList()
            throws IOException
    {
        return get("/appeals", Appeals.class);
    }

    public Appeals getAppealsByUnit(String id)
            throws IOException
    {
        return get("/units/" + id + "/appeals", Appeals.class);
    }


    public void putAppeal(String id, String projectid, String projecttype, String appellant, String outcomeid, String docid,
            String dismissalid, String responsedate, String ruleid, String statusid, String appadminunit, int decisionid)
            throws IOException
    {
        Appeal appeal = new Appeal();
        appeal.setId(id);
        appeal.setProjectid(projectid);
        appeal.setAppellant(appellant);
        appeal.setOutcomeid(outcomeid);
        appeal.setDocid(docid);
        appeal.setDismissalid(dismissalid);
        appeal.setProjecttype(projecttype);
        appeal.setResponsedate(dateStrToXML(responsedate));
        appeal.setRuleid(ruleid);
        appeal.setStatusid(statusid);
        appeal.setAppadminunit(appadminunit);
        appeal.setDecisionid(Integer.toString(decisionid));
        putAppeal(appeal);
    }


    @Deprecated
    public void putAppeal(String id, String projectid, String projecttype, String appellant, String outcomeid, String docid,
            String dismissalid, String responsedate, String ruleid, String statusid, String appadminunit)
            throws IOException
    {
        Appeal appeal = new Appeal();
        appeal.setId(id);
        appeal.setProjectid(projectid);
        appeal.setAppellant(appellant);
        appeal.setOutcomeid(outcomeid);
        appeal.setDocid(docid);
        appeal.setDismissalid(dismissalid);
        appeal.setProjecttype(projecttype);
        appeal.setResponsedate(dateStrToXML(responsedate));
        appeal.setRuleid(ruleid);
        appeal.setStatusid(statusid);
        appeal.setAppadminunit(appadminunit);
        putAppeal(appeal);
    }


    public void putAppeal(Appeal appeal)
            throws IOException
    {
        put(objectfactory.createAppeal(appeal), "/appeals/" + appeal.getId());
    }


    public void deleteAppeal(String id)
            throws IOException
    {
        delete("/appeals/" + id);
    }
}