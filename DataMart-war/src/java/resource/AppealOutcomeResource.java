package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.AppealOutcomeManagerRemote;
import us.fed.fs.www.nepa.schema.appealoutcome.Appealoutcome;
import us.fed.fs.www.nepa.schema.appealoutcome.Appealoutcomes;
import us.fed.fs.www.nepa.schema.appealoutcome.ObjectFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;


@Path ("/ref/appealoutcomes")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class AppealOutcomeResource extends AbstractResource
{
    private AppealOutcomeManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public AppealOutcomeResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "AppealOutcome.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.appealoutcome");
        MY_PROPS.setProperty("bean", "AppealOutcomeManagerRemote");
        try {
            beanmanager = (AppealOutcomeManagerRemote) init(AppealOutcomeManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("AppealOutcomeResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("AppealOutcomeResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;
        try {
            Appealoutcome outcome = (Appealoutcome)unmarshal(inputxml, Appealoutcome.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+outcome.getId());

            // All clear. Now do the insert.
            beanmanager.create(outcome);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        // Return a link to the newly created entity
        return created(uri);
    }


    @GET
    public Response get(@Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get a dump of all the entities in the datamart
            Appealoutcomes outcomelist = beanmanager.getAll();

            // Return 404 if none exist
            if (outcomelist.getAppealoutcome().isEmpty())
                notFound();

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Appealoutcomes> jaxbelement = objectfactory.createAppealoutcomes(outcomelist);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }

    @GET
    @Path("/{id}")
    public Response get(@PathParam("id") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the name of the specified entity
            String name = beanmanager.getName(id);
            Appealoutcome outcome = new Appealoutcome();
            outcome.setId(id);
            outcome.setName(name);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Appealoutcome> jaxbelement = objectfactory.createAppealoutcome(outcome);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @PUT
    @Path("/{id}")
    public Response put(@PathParam("id") String id, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        Appealoutcome outcome = null;
        try {
            outcome = (Appealoutcome) unmarshal(inputxml, Appealoutcome.class);

            if (!id.equalsIgnoreCase(outcome.getId()))
                complainLoudly(new Exception("Id in uri must match id in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.update(outcome);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"/"+outcome.getId()+"</location>");
    }


    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(id);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}