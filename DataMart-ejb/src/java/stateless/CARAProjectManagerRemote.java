package stateless;
import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.caraproject.*;
import beanutil.DataMartException;

@Remote
public interface CARAProjectManagerRemote {
    public void linkproject(Caralinkproject resource) throws DataMartException;
    public void unlinkproject(Caraunlinkproject resource) throws DataMartException;
    public void update(Caraproject resource) throws DataMartException;
    public Boolean ping();
    public Caraproject get(int caraid) throws DataMartException;
    public Caraproject getByPalsId(String palsid) throws DataMartException;
    public Caraprojects getAll() throws DataMartException;
}