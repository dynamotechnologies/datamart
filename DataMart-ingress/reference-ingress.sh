#!/bin/sh

INGRESSWORKDIR=/app/DataMart-ingress
LOGFILE=referenceingress.log

cd $INGRESSWORKDIR
java -cp .:DataMart-client.jar:asm-3.1.jar:jersey-bundle-1.4.jar:jsr311-api-1.1.1.jar ingress.Reference > $LOGFILE

FILESIZE=$(stat -c%s "$LOGFILE")
if [ $FILESIZE != "0" ]
then
    echo $FILESIZE
    cat $LOGFILE | mailx -s "DataMart reference-ingress Failed" fs-drm-alerts@phaseonecg.com
fi
