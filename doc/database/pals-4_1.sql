--
-- Script to create 
--
alter table Decisions add column DecisionType varchar(10);
alter table Decisions add column DecisionDate datetime;


CREATE TABLE DecisionMakers (
  Id int(11) NOT NULL,		-- From Decision Maker Id
  DecisionId int(11) NOT NULL,      -- FK to Decisions
  Name varchar(255),
  Title varchar(255),
  DecMakeSignedDate DATETIME DEFAULT NULL,
  CONSTRAINT PkDecisionMakers_Id PRIMARY KEY (Id),
  CONSTRAINT FkDecisionMakers_DecisionId FOREIGN KEY (DecisionId) REFERENCES Decisions(Id)
) ENGINE=INNODB;


CREATE TABLE RefDecisionTypes (
  Id varchar(10) NOT NULL,
  Description varchar(255) NOT NULL,
  PRIMARY KEY  (Id)
) ENGINE=InnoDB;
