package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.GoalManagerRemote;
import us.fed.fs.www.nepa.schema.goal.Goal;
import us.fed.fs.www.nepa.schema.goal.ObjectFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;


@Path ("/goals")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class GoalResource extends AbstractResource
{
    private GoalManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public GoalResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "Goal.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.goal");
        MY_PROPS.setProperty("bean", "GoalManagerRemote");
        try {
            beanmanager = (GoalManagerRemote) init(GoalManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("StatusResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("StatusResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;
        try {
            Goal area = (Goal)unmarshal(inputxml, Goal.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+area.getGoalid());

            // All clear. Now do the insert.
            beanmanager.create(area);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the newly created entity
        return created(uri);
    }


    @GET
    @Path("/{id}")
    public Response get(@PathParam("id") Integer id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the name of the specified unit
            Goal area = beanmanager.get(id);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Goal> jaxbelement = objectfactory.createGoal(area);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @PUT
    @Path("/{id}")
    public Response put(@PathParam("id") Integer id, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        Goal area = null;
        try {
            area = (Goal) unmarshal(inputxml, Goal.class);

            if (id != area.getGoalid())
                complainLoudly(new Exception("Id in uri must match id in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.update(area);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"</location>");
    }


    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") Integer id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(id);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}
