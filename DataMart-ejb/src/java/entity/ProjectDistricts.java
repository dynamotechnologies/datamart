package entity;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "ProjectDistricts")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProjectDistricts.findAll", query = "SELECT p FROM ProjectDistricts p"),
    @NamedQuery(name = "ProjectDistricts.findById", query = "SELECT p FROM ProjectDistricts p WHERE p.id = :id")})
public class ProjectDistricts implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @JoinColumn(name = "ProjectId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Projects projects;
    @JoinColumn(name = "DistrictId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private RefUnits refUnits;

    public ProjectDistricts() {
    }

    public ProjectDistricts(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Projects getProjects() {
        return projects;
    }

    public void setProjects(Projects projects) {
        this.projects = projects;
    }

    public RefUnits getRefUnits() {
        return refUnits;
    }

    public void setRefUnits(RefUnits refUnits) {
        this.refUnits = refUnits;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProjectDistricts)) {
            return false;
        }
        ProjectDistricts other = (ProjectDistricts) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ProjectDistricts[ id=" + id + " ]";
    }

}
