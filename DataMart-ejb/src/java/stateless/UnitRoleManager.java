package stateless;

import javax.ejb.Stateless;
import beanutil.*;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import us.fed.fs.www.nepa.schema.unitrole.*;

@Stateless(mappedName="UnitRoleManagerRemote")
public class UnitRoleManager implements UnitRoleManagerRemote {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void create(Unitrole resource) throws DataMartException
    {
        // validate(resource);
        entity.UnitRoles entity = createEntity(resource);

        entity.UnitRoles badentity = createEntity(entity.getId());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        entity = em.merge(entity);
    }

    @Override
    public Unitrole get(int accessgroupid) throws DataMartException
    {
        entity.UnitRoles entity = createEntity(accessgroupid);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        Unitrole response = createResource(entity);
        return response;
    }

    @Override
    public void update(Unitrole resource) throws DataMartException
    {
        // validate(resource);
        entity.UnitRoles entity = createEntity(resource);

        if (createEntity(entity.getId())==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        entity = em.merge(entity);
    }

    @Override
    public void delete(int accessgroupid) throws DataMartException
    {
        entity.UnitRoles entity = createEntity(accessgroupid);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));
    }

    @Override
    public Unitroles getAll() throws DataMartException
    {
        Query query = em.createNamedQuery("UnitRoles.findAll");
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Unitroles responselist = new Unitroles();
        while (it.hasNext()) {
            responselist.getUnitrole().add(createResource( (entity.UnitRoles)it.next() ));
        }
        return responselist;
    }

    @Override
    public Boolean ping()
    {
        return true;
    }

    /*
     *
     * This method does the mapping from jaxb object to entity object and vice versa
     */
    private Unitrole createResource(entity.UnitRoles entity)
            throws DataMartException
    {
        Unitrole resource = new Unitrole();
        resource.setId(entity.getId());
        resource.setRole(entity.getRefRoles().getId());
        resource.setShortname(entity.getUsers().getId());
        resource.setUnit(entity.getRefUnits().getId());
        return resource;
    }

    private entity.UnitRoles createEntity(Unitrole resource)
            throws DataMartException
    {
        entity.UnitRoles entity = new entity.UnitRoles();
        entity.setId(resource.getId());

        entity.RefRoles role = (entity.RefRoles)DataMartBeanUtil.getEntityById(em, "RefRoles", resource.getRole(), "Unit Role");
        entity.setRefRoles(role);

        entity.RefUnits unit = (entity.RefUnits)DataMartBeanUtil.getEntityById(em, "RefUnits", resource.getUnit(), "Unit");
        entity.setRefUnits(unit);

        entity.Users user = (entity.Users)DataMartBeanUtil.getEntityById(em, "Users", resource.getShortname(), "User");
        entity.setUsers(user);

        return entity;
    }

    // This version of overloaded method createEntity() accepts a String ID as a parameter, and returns the
    //   associated entity, or null if it doesn't exist
    //
    private entity.UnitRoles createEntity(int accessgroupid)
            throws DataMartException
    {
        return em.find(entity.UnitRoles.class, accessgroupid);
    }

}
