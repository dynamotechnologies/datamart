package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.AnalysisApplicableManagerRemote;
import us.fed.fs.www.nepa.schema.analysisapplicable.Analysisapplicable;
import us.fed.fs.www.nepa.schema.analysisapplicable.Analysisapplicables;
import us.fed.fs.www.nepa.schema.analysisapplicable.ObjectFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;


@Path ("/ref/analysisapplicables")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class AnalysisApplicableResource extends AbstractResource
{
    private AnalysisApplicableManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public AnalysisApplicableResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "AnalysisApplicable.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.analysisapplicable");
        MY_PROPS.setProperty("bean", "AnalysisApplicableManagerRemote");
        try {
            beanmanager = (AnalysisApplicableManagerRemote) init(AnalysisApplicableManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("AnalysisApplicableResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("AnalysisApplicableResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;
        try {
            Analysisapplicable aa = (Analysisapplicable)unmarshal(inputxml, Analysisapplicable.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+aa.getId());

            // All clear. Now do the insert.
            beanmanager.create(aa);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        }
        // Return a link to the newly created entity
        return created(uri);
    }


    @GET
    public Response get(@Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get a dump of all the entities in the datamart
            Analysisapplicables aalist = beanmanager.getAll();

            // Return 404 if none exist
            if (aalist.getAnalysisapplicable().isEmpty())
                notFound();

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Analysisapplicables> jaxbelement = objectfactory.createAnalysisapplicables(aalist);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }

    @GET
    @Path("/{id}")
    public Response get(@PathParam("id") Integer id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            Analysisapplicable aa = beanmanager.get(id);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Analysisapplicable> jaxbelement = objectfactory.createAnalysisapplicable(aa);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @PUT
    @Path("/{id}")
    public Response put(@PathParam("id") Integer id, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        Analysisapplicable aa = null;
        try {
            aa = (Analysisapplicable) unmarshal(inputxml, Analysisapplicable.class);

            if (id != aa.getId())
                complainLoudly(new Exception("Id in uri must match id in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.update(aa);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"/"+aa.getId()+"</location>");
    }


    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") Integer id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(id);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}