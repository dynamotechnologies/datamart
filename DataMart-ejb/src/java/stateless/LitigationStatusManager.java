package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.litigationstatus.*;

import beanutil.*;
import java.util.Iterator;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;


@Stateless(mappedName="LitigationStatusManagerRemote")
public class LitigationStatusManager implements LitigationStatusManagerRemote
{
    @PersistenceContext
    private EntityManager em;


    @Override
    public void create(Litigationstatus resource)
             throws DataMartException
    {
        entity.RefLitigationStatuses entity = createEntity(resource);

        // Check for existing entity with this ID
        if (getEntity(entity.getId()) != null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS,
                    "Litigation Status with id '"+entity.getId()+"' already exists.");

        em.persist(entity);
    }


    @Override
    public Litigationstatus get(Integer id)
            throws DataMartException
    {
        entity.RefLitigationStatuses entity = getEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Litigation Status with id '"+id+"' does not exist.");

        return createResource(entity);
    }


    public Litigationstatuses getAll()
            throws DataMartException
    {
        // Retrieve all entities from the database
        Query query = em.createQuery("SELECT s FROM RefLitigationStatuses s");
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        Litigationstatuses responselist = new Litigationstatuses();
        while (it.hasNext()) {
            responselist.getLitigationstatus().add(createResource( (entity.RefLitigationStatuses)it.next() ));
        }
        return responselist;
    }


    @Override
    public void update(Litigationstatus resource)
             throws DataMartException
    {
        entity.RefLitigationStatuses entity = createEntity(resource);

        // Query for the (hopefully) existing entity. Complain if not found.
        if (getEntity(entity.getId())==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND, "Nonexistent Litigation Status cannot be updated.");

        // Update the entity
        entity = em.merge(entity);
    }

    @Override
    public void delete(Integer id)
             throws DataMartException
    {
        // Verify that this entity exists
        entity.RefLitigationStatuses entity = getEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Could not delete nonexistent Litigation Status with id '"+id+"'.");

        em.remove(entity);
    }

    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     *
     * This method does the mapping from entity object to jaxb object
     */
    protected static Litigationstatus createResource(entity.RefLitigationStatuses entity)
            throws DataMartException
    {
        Litigationstatus resource = new Litigationstatus();
        resource.setId(entity.getId());
        resource.setName(entity.getName());
        return resource;
    }

    /*
     *
     * This method does the mapping from jaxb object to entity object
     */
    private entity.RefLitigationStatuses createEntity(Litigationstatus resource)
            throws DataMartException
    {
        entity.RefLitigationStatuses entity = new entity.RefLitigationStatuses();
        entity.setId(resource.getId());
        entity.setName(resource.getName());
        return entity;
    }


    // Accepts ID parameter, and returns the associated entity,
    //   or null if it doesn't exist
    private entity.RefLitigationStatuses getEntity(Integer id)
            throws DataMartException
    {
        return em.find(entity.RefLitigationStatuses.class, id);
    }
}