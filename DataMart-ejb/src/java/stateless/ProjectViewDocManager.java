/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateless;

import beanutil.DataMartBeanUtil;
import beanutil.DataMartException;
import java.util.Iterator;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


import us.fed.fs.www.nepa.schema.ProjectViewDoc.ProjectViewDoc;
import us.fed.fs.www.nepa.schema.ProjectViewDoc.ProjectViewDocs;

/**
 *
 * @author gauri
 */
@Stateless(mappedName="ProjectViewDocManagerRemote")
public class ProjectViewDocManager implements ProjectViewDocManagerRemote {
     @PersistenceContext
     private EntityManager em; 

    @Override
    public void create(ProjectViewDoc resource) throws DataMartException {
     entity.ProjectViewDocs entity = createEntity(resource);

       
        if(null != entity)
        entity = em.merge(entity); 
    }

    @Override
    public ProjectViewDoc get(Integer id) throws DataMartException {
      entity.ProjectViewDocs entity = retrieveEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return createResource(entity); 
    }

    @Override
    public ProjectViewDocs getAllByView(Integer viewId) throws DataMartException {
    
        // Retrieve project resource area entities from the database
        Query query = em.createQuery("SELECT a FROM ProjectViews a WHERE a.viewId = :viewId");
        query.setParameter("viewId", viewId);
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        ProjectViewDocs responselist = new ProjectViewDocs();
        entity.ProjectViews projectView=null;
        while (it.hasNext()) {
            projectView= (entity.ProjectViews)it.next();
        }
        query = em.createQuery("SELECT a FROM ProjectViewDocs a WHERE a.viewId = :view");
        query.setParameter("view", projectView);
        entitylist =  query.getResultList();
        it = entitylist.iterator();
        while (it.hasNext()) {
            responselist.getProjectViewDoc().add(createResource((entity.ProjectViewDocs)it.next()));
        }
        return responselist; 
    }

    @Override
    public void update(ProjectViewDoc resource) throws DataMartException {
        entity.ProjectViewDocs entity = createEntity(resource);

        entity.ProjectViewDocs existingentity = retrieveEntity(new Integer(resource.getId()));

        if (existingentity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        entity.setProjectViewDocsId(existingentity.getProjectViewDocsId());
        entity = em.merge(entity); 
    }

    @Override
    public void delete(Integer id) throws DataMartException {
      entity.ProjectViewDocs entity = retrieveEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));   
    }

    @Override
    public Boolean ping() {
 return true;
    }
    private void validate(ProjectViewDoc resource)
            throws DataMartException
    {
         //Ensure these are not empty strings
        DataMartBeanUtil.confirmNotNull(resource.getViewId(), "viewId");
        DataMartBeanUtil.confirmNotNull(resource.getDocumentId(), "documentId");
    }
    /*
     *
     * This method does the mapping from jaxb object to entity object and vice versa
     */
    private ProjectViewDoc createResource(entity.ProjectViewDocs entity)
            throws DataMartException
    {
        ProjectViewDoc resource = new ProjectViewDoc();
        resource.setViewId(""+entity.getViewId().getViewId().intValue());
        resource.setDocumentId(entity.getDocId().getDocumentId());
      

        return resource;
    }

    private entity.ProjectViewDocs createEntity(ProjectViewDoc resource)
            throws DataMartException
    {
      
       
        Integer viewId = new Integer(resource.getViewId()) ;
        Query query = em.createQuery("SELECT a FROM ProjectViews a WHERE a.viewId = :viewId");
        query.setParameter("viewId", viewId);
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

       
        entity.ProjectViews projView=null;
        while (it.hasNext()) {
            projView= (entity.ProjectViews)it.next();
        }
        entity.ProjectDocuments doc = DataMartBeanUtil.getProjectDocument(em, projView.getProjectId(), resource.getDocumentId());

          
        query = em.createQuery("SELECT a FROM ProjectViewDocs a WHERE a.viewId = :view AND a.docId=:doc");
        query.setParameter("view", projView);
        query.setParameter("doc", doc);
        entitylist =  query.getResultList();
        entity.ProjectViewDocs entity = null;
        if(null == entitylist || entitylist.size()==0){
          //throw new DataMartException(DataMartException.ALREADY_EXISTS, "docId " + resource.getDocumentId() + " already exist in view "+ resource.getViewId());
      
        if (projView == null) {
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION, "view " + resource.getViewId() + " does not exist");
        }
        entity = new entity.ProjectViewDocs();
     
        entity.setViewId(projView);
        entity.setDocId(doc);
        }
        return entity;
    }
      private entity.ProjectViewDocs retrieveEntity(Integer key)
            throws DataMartException
    {
        if(null !=key)
        return em.find(entity.ProjectViewDocs.class, key);
        else
        return null;
    }
}
