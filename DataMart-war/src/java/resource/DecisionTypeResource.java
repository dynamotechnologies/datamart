package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.DecisionTypeManagerRemote;
import us.fed.fs.www.nepa.schema.decisiontype.Decisiontype;
import us.fed.fs.www.nepa.schema.decisiontype.Decisiontypes;
import us.fed.fs.www.nepa.schema.decisiontype.ObjectFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;


@Path ("/ref/decisiontypes")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class DecisionTypeResource extends AbstractResource
{
    private DecisionTypeManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public DecisionTypeResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "DecisionType.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.decisiontype");
        MY_PROPS.setProperty("bean", "DecisionTypeManagerRemote");
        try {
            beanmanager = (DecisionTypeManagerRemote) init(DecisionTypeManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("DecisionTypeResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("DecisionTypeResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;
        try {
            Decisiontype decisiontype = (Decisiontype)unmarshal(inputxml, Decisiontype.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+decisiontype.getId());

            // All clear. Now do the insert.
            beanmanager.create(decisiontype);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (URISyntaxException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        // Return a link to the newly created entity
        return created(uri);
    }


    @GET
    public Response get(@Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get a dump of all the entities in the datamart
            Decisiontypes decisiontypelist = beanmanager.getAll();

            // Return 404 if none exist
            if (decisiontypelist.getDecisiontype().isEmpty())
                notFound();

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Decisiontypes> jaxbelement = objectfactory.createDecisiontypes(decisiontypelist);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }

    @GET
    @Path("/{id}")
    public Response get(@PathParam("id") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the specified entity
            Decisiontype dectype = beanmanager.get(id);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Decisiontype> jaxbelement = objectfactory.createDecisiontype(dectype);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @PUT
    @Path("/{id}")
    public Response put(@PathParam("id") String id, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        Decisiontype decisiontype = null;
        try {
            decisiontype = (Decisiontype) unmarshal(inputxml, Decisiontype.class);

            if (!id.equalsIgnoreCase(decisiontype.getId()))
                complainLoudly(new Exception("Id in uri must match id in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.update(decisiontype);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"/"+decisiontype.getId()+"</location>");
    }


    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(id);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}