package test.reference;

import client.ActivityClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.activity.*;


public class ActivityTest {

    private ActivityClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new ActivityClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postActivity(TestData.activities.id1, TestData.activities.name2);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Activity activity = null;
        Activities activitylist = null;
        try {
            activity = client.getActivity(TestData.activities.id1);
            activitylist = client.getActivityList();
        } catch (IOException ex) {
            Logger.getLogger(ActivityTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.activities.id1, activity.getId());
        Assert.assertEquals(TestData.activities.name1, activity.getName());
        Assert.assertTrue(activitylist.getActivity().get(0).getName().length()>0);
        Assert.assertTrue(activitylist.getActivity().get(0).getId().length()>0);

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        Activity activity = null;
        try {
            client.putActivity(TestData.activities.id1, TestData.activities.name2);
            activity = client.getActivity(TestData.activities.id1);
        } catch (Exception ex) {
            Logger.getLogger(ActivityTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.activities.name2, activity.getName());

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();
        
        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getActivity(TestData.activities.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteActivity(TestData.activities.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }

    private void post()
    {
        try {
            client.postActivity(TestData.activities.id1, TestData.activities.name1);
        } catch (IOException ex) {
            Logger.getLogger(ActivityTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteActivity(TestData.activities.id1);
        } catch (IOException ex) {
            Logger.getLogger(ActivityTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteActivity(TestData.activities.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }
}