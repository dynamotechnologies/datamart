package resource.logging;

import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.container.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

//@Logged  //add this annotation back in to log only methods annotated with @Logged instead of everything
@Provider
@Slf4j
public class CustomLoggingFilter implements ContainerResponseFilter {

    @Context
    ResourceInfo resourceInfo;

    //Logging Responses
    @Override
    public void filter(ContainerRequestContext requestContext,
                       ContainerResponseContext responseContext) throws IOException {

        String method;
        String className = "[N/A]";
        String methodName = "[N/A]";

        method = requestContext.getMethod();
        MultivaluedMap<String, String> paramMap = requestContext.getUriInfo().getPathParameters();

        if (resourceInfo != null) {
            if (resourceInfo.getResourceClass() != null &&
                resourceInfo.getResourceMethod() != null) {
              className = resourceInfo.getResourceClass().getName();
              methodName = resourceInfo.getResourceMethod().getName();
            }
        }

        int responseStatus = responseContext.getStatus();

        log.info("\n=======================\n" +
                "Request Method: " + method + "\n" +
                "Calling " + className + "." + methodName + "\n" +
                "Param Map: " + paramMap + "\n" +
                "Response Status: " + responseStatus + "\n" +
                "------------------------");
    }
}
