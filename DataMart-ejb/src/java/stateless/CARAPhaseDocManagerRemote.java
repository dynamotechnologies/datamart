package stateless;
import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.caradocument.*;
import beanutil.DataMartException;
import java.util.List;

@Remote
public interface CARAPhaseDocManagerRemote {
    public void linkdoc(Caralinkdoc resource) throws DataMartException;
    public void unlinkdoc(Caraunlinkdoc resource) throws DataMartException;
    public  List getAll(Integer phaseid)throws DataMartException;
    public Boolean ping();
}