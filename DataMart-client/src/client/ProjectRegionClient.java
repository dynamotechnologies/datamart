package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.projectlocation.*;


public class ProjectRegionClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public ProjectRegionClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.projectlocation", "ProjectLocation.xsd");
    }


    public Regions getProjectRegion(String projectid, String id)
            throws IOException
    {
        return get("/projects/nepa/" + projectid + "/locations/regions/" + id, Regions.class);
    }


    public void putProjectRegion(String projectid, String id)
            throws IOException
    {
        Regions region = new Regions();
        region.getRegionid().add(id);
        put(objectfactory.createRegions(region), "/projects/nepa/" + projectid + "/locations/regions/" + id);
    }


    public void deleteProjectRegion(String projectid, String id)
            throws IOException
    {
        delete("/projects/nepa/" + projectid + "/locations/regions/" + id);
    }
}