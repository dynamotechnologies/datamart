package entity;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "ProjectGoals")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProjectGoals.findAll", query = "SELECT p FROM ProjectGoals p"),
    @NamedQuery(name = "ProjectGoals.findById", query = "SELECT p FROM ProjectGoals p WHERE p.id = :id"),
    @NamedQuery(name = "ProjectGoals.findByOrderTag", query = "SELECT p FROM ProjectGoals p WHERE p.orderTag = :orderTag")})
public class ProjectGoals implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Column(name = "OrderTag")
    private Integer orderTag;
    @JoinColumn(name = "ProjectId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Projects projects;
    @JoinColumn(name = "GoalId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Goals goals;

    public ProjectGoals() {
    }

    public ProjectGoals(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOrderTag() {
        return orderTag;
    }

    public void setOrderTag(Integer orderTag) {
        this.orderTag = orderTag;
    }

    public Projects getProjects() {
        return projects;
    }

    public void setProjects(Projects projects) {
        this.projects = projects;
    }

    public Goals getGoals() {
        return goals;
    }

    public void setGoals(Goals goals) {
        this.goals = goals;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProjectGoals)) {
            return false;
        }
        ProjectGoals other = (ProjectGoals) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ProjectGoals[ id=" + id + " ]";
    }

}
