package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.project.*;
import us.fed.fs.www.nepa.schema.projectlisting.*;

import beanutil.*;
import java.util.Iterator;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;


@Stateless(mappedName="ProjectListingManagerRemote")
public class ProjectListingManager implements ProjectListingManagerRemote
{

    @Override
    public Boolean ping()
    {
        return true;
    }
    

}