package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.RoleManagerRemote;
import us.fed.fs.www.nepa.schema.role.ObjectFactory;
import us.fed.fs.www.nepa.schema.role.Role;
import us.fed.fs.www.nepa.schema.role.Roles;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;

@Path ("/ref/roles")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class RoleResource extends AbstractResource
{
    private RoleManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public RoleResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "Role.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.role");
        MY_PROPS.setProperty("bean", "RoleManagerRemote");
        try {
            beanmanager = (RoleManagerRemote) init(RoleManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("RoleResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("RoleResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;
        try {
            Role role = (Role)unmarshal(inputxml, Role.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+role.getId());

            // All clear. Now do the insert.
            beanmanager.create(role);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the newly created entity
        return created(uri);
    }


    @GET
    public Response get(@Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get a dump of all the Roles in the datamart
            Roles rolelist = beanmanager.getAll();

            // Return 404 if none exist
            if (rolelist.getRole().isEmpty())
                notFound();

            // Get the query results in a JAXBElement<Roles> object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Roles> jaxbelement = objectfactory.createRoles(rolelist);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @GET
    @Path("/{id}")
    public Response get(@PathParam("id") int id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Retrieve the specified role
            Role role = beanmanager.get(id);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Role> jaxbelement = objectfactory.createRole(role);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @PUT
    @Path("/{id}")
    public Response put(@PathParam("id") int id, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        Role role = null;
        try {
            role = (Role)unmarshal(inputxml, Role.class);

            if (id != role.getId())
                complainLoudly(new Exception("Shortname in uri must match shortname in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.update(role);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"/"+role.getId()+"</location>");
    }


    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") int id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(id);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}
