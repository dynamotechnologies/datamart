package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.AnalysisTypeManagerRemote;
import us.fed.fs.www.nepa.schema.analysistype.Analysistype;
import us.fed.fs.www.nepa.schema.analysistype.Analysistypes;
import us.fed.fs.www.nepa.schema.analysistype.ObjectFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;


@Path ("/ref/analysistypes")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class AnalysisTypeResource extends AbstractResource
{
    private AnalysisTypeManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public AnalysisTypeResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "AnalysisType.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.analysistype");
        MY_PROPS.setProperty("bean", "AnalysisTypeManagerRemote");
        try {
            beanmanager = (AnalysisTypeManagerRemote) init(AnalysisTypeManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("AnalysisTypeResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("AnalysisTypeResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;
        try {
            Analysistype analysistype = (Analysistype)unmarshal(inputxml, Analysistype.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+analysistype.getId());

            // All clear. Now do the insert.
            beanmanager.create(analysistype);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (URISyntaxException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        // Return a link to the newly created entity
        return created(uri);
    }


    @GET
    public Response get(@Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get a dump of all the entities in the datamart
            Analysistypes analysistypelist = beanmanager.getAll();

            // Return 404 if none exist
            if (analysistypelist.getAnalysistype().isEmpty())
                notFound();

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Analysistypes> jaxbelement = objectfactory.createAnalysistypes(analysistypelist);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }

    @GET
    @Path("/{id}")
    public Response get(@PathParam("id") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the name of the specified entity
            String name = beanmanager.getName(id);
            Analysistype analysistype = new Analysistype();
            analysistype.setId(id);
            analysistype.setName(name);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Analysistype> jaxbelement = objectfactory.createAnalysistype(analysistype);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @PUT
    @Path("/{id}")
    public Response put(@PathParam("id") String id, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        Analysistype analysistype = null;
        try {
            analysistype = (Analysistype) unmarshal(inputxml, Analysistype.class);

            if (!id.equalsIgnoreCase(analysistype.getId()))
                complainLoudly(new Exception("Id in uri must match id in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.update(analysistype);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"/"+analysistype.getId()+"</location>");
    }


    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(id);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}