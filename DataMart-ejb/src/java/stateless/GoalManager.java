package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.goal.*;

import beanutil.*;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Iterator;
import java.util.List;

@Stateless(mappedName="GoalManagerRemote")
public class GoalManager implements GoalManagerRemote {

    @PersistenceContext
    private EntityManager em;

    public void create(Goal resource) throws DataMartException {
        entity.Goals entity = createEntity(resource);

        entity.Goals badentity = retrieveEntity(resource.getGoalid());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS, "Goal " + resource.getGoalid() + " already exists");

        entity = em.merge(entity);
    }

    public Goal get(Integer id) throws DataMartException {
        entity.Goals entity = retrieveEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return createResource(entity);
    }

    public void update(Goal resource) throws DataMartException {
        entity.Goals entity = createEntity(resource);

        if (retrieveEntity(entity.getId())==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND, "Goal " + entity.getId() + " does not exist");

        entity = em.merge(entity);
    }

    public void delete(Integer id) throws DataMartException {
        entity.Goals entity = retrieveEntity(id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        
        // Cascade deletes to ProjectGoal
        Query query = em.createQuery("SELECT p FROM ProjectGoals p WHERE p.goals = :goal");
        query.setParameter("goal", entity);
        for (Object i : query.getResultList()) {
            entity.ProjectGoals pg = (entity.ProjectGoals)i;
            em.remove(em.merge(pg));
        }
        
        // Cascade deletes to UnitGoal
        query = em.createQuery("SELECT u FROM UnitGoals u WHERE u.goals = :goal");
        query.setParameter("goal", entity);
        for (Object i : query.getResultList()) {
            entity.UnitGoals ug = (entity.UnitGoals)i;
            em.remove(em.merge(ug));
        }
        
        em.remove(em.merge(entity));
    }

    public Boolean ping() {
        return true;
    }

    /*
     * Create XML from EJB
     */
    private Goal createResource(entity.Goals entity)
            throws DataMartException
    {
        Goal resource = new Goal();
        resource.setGoalid(entity.getId());
        resource.setDescription(entity.getDescription());
        return resource;
    }

    /*
     * Create EJB from XML
     */
    private entity.Goals createEntity(Goal resource)
            throws DataMartException
    {
        entity.Goals entity = new entity.Goals();
        entity.setId(resource.getGoalid());
        entity.setDescription(resource.getDescription());
        return entity;
    }

    private entity.Goals retrieveEntity(Integer goalid)
            throws DataMartException
    {
        return em.find(entity.Goals.class, goalid);
    }
}
