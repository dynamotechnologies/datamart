package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.unitrole.*;


public class UnitRoleClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public UnitRoleClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.unitrole", "UnitRole.xsd");
    }


    public void postUnitRole(int agid, String shortname, String unit, int role)
            throws IOException
    {
        Unitrole unitrole = new Unitrole();
        unitrole.setId(agid);
        unitrole.setShortname(shortname);
        unitrole.setUnit(unit);
        unitrole.setRole(role);
        postUnitRole(unitrole);
    }


    public void postUnitRole(Unitrole unitrole)
            throws IOException
    {
        post(objectfactory.createUnitrole(unitrole), "/unitroles");
    }


    public Unitrole getUnitRole(int agid)
            throws IOException
    {
        return get("/unitroles/" + agid, Unitrole.class);
    }


    public Unitroles getUnitRoleList()
            throws IOException
    {
        return get("/unitroles", Unitroles.class);
    }


    public void putUnitRole(int agid, String shortname, String unit, int role)
            throws IOException
    {
        Unitrole unitrole = new Unitrole();
        unitrole.setId(agid);
        unitrole.setShortname(shortname);
        unitrole.setUnit(unit);
        unitrole.setRole(role);
        putUnitRole(unitrole);
    }


    public void putUnitRole(Unitrole unitrole)
            throws IOException
    {
        put(objectfactory.createUnitrole(unitrole), "/unitroles/" + unitrole.getId());
    }


    public void deleteUnitRole(int agid)
            throws IOException
    {
        delete("/unitroles/" + agid);
    }
}