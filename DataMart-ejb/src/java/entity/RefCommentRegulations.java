package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "RefCommentRegulations")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RefCommentRegulations.findAll", query = "SELECT r FROM RefCommentRegulations r"),
    @NamedQuery(name = "RefCommentRegulations.findById", query = "SELECT r FROM RefCommentRegulations r WHERE r.id = :id"),
    @NamedQuery(name = "RefCommentRegulations.findByName", query = "SELECT r FROM RefCommentRegulations r WHERE r.name = :name")})
public class RefCommentRegulations implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "Name")
    private String name;
    @OneToMany(mappedBy = "refCommentRegulations")
    private Collection<Projects> projectsCollection;

    public RefCommentRegulations() {
    }

    public RefCommentRegulations(Integer id) {
        this.id = id;
    }

    public RefCommentRegulations(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Collection<Projects> getProjectsCollection() {
        return projectsCollection;
    }

    public void setProjectsCollection(Collection<Projects> projectsCollection) {
        this.projectsCollection = projectsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RefCommentRegulations)) {
            return false;
        }
        RefCommentRegulations other = (RefCommentRegulations) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RefCommentRegulations[ id=" + id + " ]";
    }

}
