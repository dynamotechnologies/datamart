package test.relationship;


import client.UnitKMLClient;
import client.UnitClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.unitkml.*;
import us.fed.fs.www.nepa.schema.unit.*;


public class UnitKMLTest {
    private UnitKMLClient client;
    private UnitClient unitclient;

    @Before
    public void setUp()
            throws IOException
    {
        client = new UnitKMLClient();
        unitclient = new UnitClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postUnitKML(TestData.unitkmls.filename1, TestData.unitkmls.label1, TestData.unitkmls.maptype1, TestData.units.code1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Unitkml unitkml = null;
        try {
            unitkml = client.getUnitKML(TestData.unitkmls.filename1);

        } catch (IOException ex) {
            Logger.getLogger(UnitKMLTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.unitkmls.filename1,    unitkml.getFilename());
        Assert.assertEquals(TestData.unitkmls.label1,       unitkml.getLabel());
        Assert.assertEquals(TestData.unitkmls.maptype1,     unitkml.getMaptype());
        Assert.assertEquals(TestData.units.code1,           unitkml.getUnitid());

        // List-context check of links in the Unit entity
        try {
            Unit u = unitclient.getUnit(TestData.units.code1);
            if (u.getLinks() == null) {
                Assert.fail("Unit with linked KML has no <link> tags!");
            }
            boolean foundKMLlink = false;
            for (Link link : u.getLinks().getLink()) {
                if (link.getRel().matches("unitkml")) {
//                    System.out.print("Link: " + link.getHref() + "\n");
                    if (link.getHref().matches(".*/" + TestData.unitkmls.filename1 + "$")) {
                        Assert.assertEquals(TestData.unitkmls.label1, link.getTitle());
                        Assert.assertEquals(TestData.unitkmls.maptype1, link.getType());
                        foundKMLlink = true;
                    }
                }
            }
            if (foundKMLlink == false) {
                Assert.fail("Couldn't find matching KML link in Unit record!");
            }
        } catch (IOException ex) {
            Logger.getLogger(UnitKMLTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        Unitkml unitkml = null;
        try {
            client.putUnitKML(TestData.unitkmls.filename1, TestData.unitkmls.label2, TestData.unitkmls.maptype2, TestData.projects.unitcode1);
            unitkml = client.getUnitKML(TestData.unitkmls.filename1);
        } catch (Exception ex) {
            Logger.getLogger(UnitKMLTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.unitkmls.filename1,    unitkml.getFilename());
        Assert.assertEquals(TestData.unitkmls.label2,       unitkml.getLabel());
        Assert.assertEquals(TestData.unitkmls.maptype2,     unitkml.getMaptype());
        Assert.assertEquals(TestData.projects.unitcode1,    unitkml.getUnitid());

        // TODO: Add a list-context check of links in the Unit entity

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getUnitKML(TestData.unitkmls.filename1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteUnitKML(TestData.unitkmls.filename1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }


    private void post()
    {
        try {
            client.postUnitKML(TestData.unitkmls.filename1, TestData.unitkmls.label1, TestData.unitkmls.maptype1, TestData.units.code1);
        } catch (IOException ex) {
            Logger.getLogger(UnitKMLTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteUnitKML(TestData.unitkmls.filename1);
        } catch (IOException ex) {
            Logger.getLogger(UnitKMLTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteUnitKML(TestData.unitkmls.filename1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }


    @BeforeClass
    public static void  populateDependencies()
    {
        UnitClient uc = null;
        try {
            uc = new UnitClient();
        } catch (Exception ex) {
            Assert.fail();
        }

        // Create the Unit
        try {
            uc.postUnit(TestData.units.code1, TestData.units.name1);
        } catch (IOException ex) {
            Logger.getLogger(UnitKMLTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    @AfterClass
    public static void cleanupDependencies()
    {
        UnitClient uc = null;
        try {
            uc = new UnitClient();
        } catch (Exception ex) {
            Assert.fail();
        }

        // Remove the test Unit
        try {
            uc.deleteUnit(TestData.units.code1);
        } catch (IOException ex) {
            Logger.getLogger(UnitKMLTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }
}