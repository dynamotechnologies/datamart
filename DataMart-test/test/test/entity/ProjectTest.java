package test.entity;

import client.ProjectClient;
import client.ProjectLocationClient;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.project.*;
import us.fed.fs.www.nepa.schema.projectlocation.Locations;


public class ProjectTest {
    private ProjectClient client;
    ArrayList<String> purposeids1;
    ArrayList<String> purposeids2;

    @Before
    public void setUp()
            throws IOException
    {
        client = new ProjectClient();
        purposeids1 = new ArrayList<String>();
        purposeids1.add(TestData.projects.purposeid1);
        purposeids1.add(TestData.projects.purposeid2);
        purposeids1.add(TestData.projects.purposeid3);

        purposeids2 = new ArrayList<String>();
        purposeids2.add(TestData.projects.purposeid4);
        purposeids2.add(TestData.projects.purposeid5);
        purposeids2.add(TestData.projects.purposeid6);
    }

    @Test
    public void testPost()
    {
        // This POST should fail, because it violates a constraint. The unit code is bad
        try {
            client.postProject(TestData.projects.id1,
                    TestData.projects.type1,
                    TestData.projects.name1,
                    TestData.projects.badunitcode1,
                    TestData.projects.description1,
                    TestData.projects.lastupdate1,
                    TestData.projects.commentreg1,
                    TestData.projects.wwwlink1,
                    TestData.projects.wwwsummary1,
                    TestData.projects.wwwpub1,
                    TestData.projects.analysistypeid1,
                    TestData.projects.statusid1,
                    TestData.projects.contactname1,
                    TestData.projects.contactphone1,
                    TestData.projects.contactemail1,
                    purposeids1,
                    TestData.projects.expirationdate1,
                    TestData.projects.projectdocumentid1,
                    TestData.projects.sopapub,
                    TestData.projects.sopanew,
                    TestData.projects.sopaheader,
                    TestData.projects.esd,
                    TestData.projects.cenodecision);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.BAD_PARENTS)
                Assert.fail();
        }

        // This POST should fail, because it violates a constraint. The analysis type is bad.
        try {
            client.postProject(TestData.projects.id1,
                    TestData.projects.type1,
                    TestData.projects.name1,
                    TestData.projects.unitcode1,
                    TestData.projects.description1,
                    TestData.projects.lastupdate1,
                    TestData.projects.commentreg1,
                    TestData.projects.wwwlink1,
                    TestData.projects.wwwsummary1,
                    TestData.projects.wwwpub1,
                    TestData.projects.badanalysistypeid1,
                    TestData.projects.statusid1,
                    TestData.projects.contactname1,
                    TestData.projects.contactphone1,
                    TestData.projects.contactemail1,
                    purposeids1,
                    TestData.projects.expirationdate1,
                    TestData.projects.projectdocumentid1,
                    TestData.projects.sopapub,
                    TestData.projects.sopanew,
                    TestData.projects.sopaheader,
                    TestData.projects.esd,
                    TestData.projects.cenodecision);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.BAD_PARENTS)
                Assert.fail();
        }

        // POST the entity
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postProject(TestData.projects.id1,
                    TestData.projects.type1,
                    TestData.projects.name1,
                    TestData.projects.unitcode1,
                    TestData.projects.description1,
                    TestData.projects.lastupdate1,
                    TestData.projects.commentreg1,
                    TestData.projects.wwwlink1,
                    TestData.projects.wwwsummary1,
                    TestData.projects.wwwpub1,
                    TestData.projects.analysistypeid1,
                    TestData.projects.statusid1,
                    TestData.projects.contactname1,
                    TestData.projects.contactphone1,
                    TestData.projects.contactemail1,
                    purposeids1,
                    TestData.projects.expirationdate1,
                    TestData.projects.projectdocumentid1,
                    TestData.projects.sopapub,
                    TestData.projects.sopanew,
                    TestData.projects.sopaheader,
                    TestData.projects.esd,
                    TestData.projects.cenodecision);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Project project = null;
        Projects unitprojects = null;
        try {
            project = client.getProject("nepa", TestData.projects.id1);
            
            /* Set Locations for the test project
             * because getProjectsByUnit only returns projects associated with
             * the unit via project-forests or project-districts
             */
            ArrayList<String> projForests = new ArrayList<String>();
            projForests.add(TestData.projects.unitcode1);
            new ProjectLocationClient().putProjectLocations( TestData.projects.id1,
                            TestData.projectlocations.description1,
                            TestData.projectlocations.legaldesc1,
                            TestData.projectlocations.latitude1,
                            TestData.projectlocations.longitude1,
                            new ArrayList<String>(),
                            projForests,
                            new ArrayList<String>(),
                            new ArrayList<String>(),
                            new ArrayList<String>());
            unitprojects = client.getProjectsByUnit(TestData.projects.unitcode1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Nepainfo nepainfo = project.getNepainfo();
        Purposeids purposeids = nepainfo.getPurposeids();
        Assert.assertEquals(TestData.projects.id1, project.getId());
        Assert.assertEquals(TestData.projects.name1, project.getName());
        Assert.assertEquals(TestData.projects.unitcode1, project.getUnitcode());
        Assert.assertEquals(TestData.projects.description1, project.getDescription());
        Assert.assertEquals(TestData.projects.lastupdate1, project.getLastupdate().toString());
        Assert.assertEquals(TestData.projects.commentreg1, project.getCommentreg());
        Assert.assertEquals(TestData.projects.wwwlink1, project.getWwwlink());
        Assert.assertEquals(TestData.projects.wwwsummary1, project.getWwwsummary());
        Assert.assertEquals(TestData.projects.wwwpub1, project.getWwwpub());
        Assert.assertEquals(TestData.projects.sopapub, project.getNepainfo().getSopainfo().isPublishflag());
        Assert.assertEquals(TestData.projects.sopanew, project.getNepainfo().getSopainfo().isNewflag());
        Assert.assertEquals(TestData.projects.sopaheader, project.getNepainfo().getSopainfo().getHeadercategory());
        Assert.assertEquals(TestData.projects.analysistypeid1, nepainfo.getAnalysistypeid());
        Assert.assertEquals(TestData.projects.statusid1, nepainfo.getStatusid());
        Assert.assertEquals(TestData.projects.contactname1, nepainfo.getContactname());
        Assert.assertEquals(TestData.projects.contactphone1, nepainfo.getContactphone());
        Assert.assertEquals(TestData.projects.contactemail1, nepainfo.getContactemail());
                
        Assert.assertTrue(purposeids.getPurposeid().contains(TestData.projects.purposeid1));
        Assert.assertTrue(purposeids.getPurposeid().contains(TestData.projects.purposeid2));
        Assert.assertTrue(purposeids.getPurposeid().contains(TestData.projects.purposeid3));

        Assert.assertEquals(TestData.projects.expirationdate1, project.getExpirationdate().toString());
        if (TestData.projects.projectdocumentid1 != nepainfo.getProjectdocumentid()) {
            Assert.fail();
        }

        Assert.assertTrue(unitprojects.getProject().size()>0);
        // Check that every project returned in the response is associated with the specified unit code, or with a
        //     sub-unit of the specified unit code
        for (Project p : unitprojects.getProject()) {
            try {
                Locations projectLocations = new ProjectLocationClient().getProjectLocations(p.getId());
                Boolean forestmatch = false;
                for (String f : projectLocations.getForests().getForestid())
                    if (f.startsWith(TestData.projects.unitcode1))
                        forestmatch = true;
                Boolean districtmatch = false;
                for (String d : projectLocations.getDistricts().getDistrictid())
                    if (d.startsWith(TestData.projects.unitcode1))
                        districtmatch = true;
                Assert.assertTrue(forestmatch || districtmatch);
            } catch (IOException ex) {
                Logger.getLogger(ProjectTest.class.getName()).log(Level.SEVERE, null, ex);
                Assert.fail();
            }
        }

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        Project project = null;
        try {
            client.putProject(TestData.projects.id1,
                    TestData.projects.type1,
                    TestData.projects.name2,
                    TestData.projects.unitcode1,
                    TestData.projects.description1,
                    TestData.projects.lastupdate1,
                    TestData.projects.commentreg1,
                    TestData.projects.wwwlink1,
                    TestData.projects.wwwsummary1,
                    TestData.projects.wwwpub1,
                    TestData.projects.analysistypeid1,
                    TestData.projects.statusid1,
                    TestData.projects.contactname2,
                    TestData.projects.contactphone1,
                    TestData.projects.contactemail1,
                    purposeids2,
                    TestData.projects.expirationdate2,
                    TestData.projects.projectdocumentid2,
                    TestData.projects.sopapub,
                    TestData.projects.sopanew,
                    TestData.projects.sopaheader,
                    TestData.projects.esd,
                    TestData.projects.cenodecision);
            project = client.getProject("nepa", TestData.projects.id1);
        } catch (Exception ex) {
            Logger.getLogger(ProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // If successful this PUT would revert the previous one. But it should
        //   fail, because it violates a constraint. Specifically, the Status is invalid.
        try {
            client.putProject(TestData.projects.id1,
                    TestData.projects.type1,
                    TestData.projects.name1,
                    TestData.projects.unitcode1,
                    TestData.projects.description1,
                    TestData.projects.lastupdate1,
                    TestData.projects.commentreg1,
                    TestData.projects.wwwlink1,
                    TestData.projects.wwwsummary1,
                    TestData.projects.wwwpub1,
                    TestData.projects.analysistypeid1,
                    TestData.projects.badstatusid1,
                    TestData.projects.contactname1,
                    TestData.projects.contactphone1,
                    TestData.projects.contactemail1,
                    purposeids1,
                    TestData.projects.expirationdate1,
                    TestData.projects.projectdocumentid2,
                    TestData.projects.sopapub,
                    TestData.projects.sopanew,
                    TestData.projects.sopaheader,
                    TestData.projects.esd,
                    TestData.projects.cenodecision);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.BAD_PARENTS)
                Assert.fail();
        }
        // TODO: Is it necessary to test every constraint on every table?
        //  How can we test the non-public jointables?

        Nepainfo nepainfo = project.getNepainfo();
        Purposeids purposeids = nepainfo.getPurposeids();
        Assert.assertEquals(TestData.projects.name2, project.getName());
        Assert.assertEquals(TestData.projects.contactname2, nepainfo.getContactname());
        Assert.assertTrue(purposeids.getPurposeid().contains(TestData.projects.purposeid4));
        Assert.assertTrue(purposeids.getPurposeid().contains(TestData.projects.purposeid5));
        Assert.assertTrue(purposeids.getPurposeid().contains(TestData.projects.purposeid6));

        // Make sure the old purposes have been overwritten by the new ones
        Assert.assertFalse(purposeids.getPurposeid().contains(TestData.projects.purposeid1));
        Assert.assertFalse(purposeids.getPurposeid().contains(TestData.projects.purposeid2));
        Assert.assertFalse(purposeids.getPurposeid().contains(TestData.projects.purposeid3));

        delete();
    }

    @Test
    public void testFindNEPAProjects()
    {
        post();

        // Just make sure util function doesn't crash
        try {
            String name="";
            String id=TestData.projects.id1;
            String unitlist="";
            String statuslist = "";
            client.findNEPAProject(name, id, unitlist, statuslist);
        } catch (IOException ex) {
            Logger.getLogger(ProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        delete();
    }

    @Test
    public void testGetMapProjects()
    {
        post();
        
        try {
            // get un-published, archived projects with lat/long
            List<Project> projects = client.getMapProjects("0", "1", "", "").getProjects().getProject();
            // The test project should not be found, because lat/long are not set
            if (projects != null)
                Assert.assertFalse(listContainsProject(projects, TestData.projects.id1));

            // Set latitude and longitude for the test project
            new ProjectLocationClient().putProjectLocations( TestData.projects.id1,
                            TestData.projectlocations.description1,
                            TestData.projectlocations.legaldesc1,
                            TestData.projectlocations.latitude1,
                            TestData.projectlocations.longitude1,
                            new ArrayList<String>(),
                            new ArrayList<String>(),
                            new ArrayList<String>(),
                            new ArrayList<String>(),
                            new ArrayList<String>());

            // get un-published, un-archived projects with lat/long
            projects = client.getMapProjects("0", "0", "", "").getProjects().getProject();
            // The test project still shouldn't be found, because this should filter out archived projects
            if (projects != null)
                Assert.assertFalse(listContainsProject(projects, TestData.projects.id1));

            // get published, un-archived projects with lat/long
            projects = client.getMapProjects("1", "0", "", "").getProjects().getProject();
            // The test project still shouldn't be found, because this should filter out archived projects
            if (projects != null)
                Assert.assertFalse(listContainsProject(projects, TestData.projects.id1));

            // get un-published, archived projects with lat/long
            projects = client.getMapProjects("1", "1", "", "").getProjects().getProject();
            // The test project should still not be in the list, because it is not published
            if (projects != null)
                Assert.assertFalse(listContainsProject(projects, TestData.projects.id1));

            // get un-published, archived projects with lat/long
            projects = client.getMapProjects("0", "1", "", "").getProjects().getProject();
            // Now the project should be in the list. Woohoo!
            Assert.assertNotSame(projects, null);
            Assert.assertTrue(listContainsProject(projects, TestData.projects.id1));

            // update expiration date to be in the future
            client.putProject(TestData.projects.id1,
                    TestData.projects.type1,
                    TestData.projects.name2,
                    TestData.projects.unitcode1,
                    TestData.projects.description1,
                    TestData.projects.lastupdate1,
                    TestData.projects.commentreg1,
                    TestData.projects.wwwlink1,
                    TestData.projects.wwwsummary1,
                    TestData.projects.wwwpub2,
                    TestData.projects.analysistypeid1,
                    TestData.projects.statusid1,
                    TestData.projects.contactname2,
                    TestData.projects.contactphone1,
                    TestData.projects.contactemail1,
                    purposeids2,
                    TestData.projects.expirationdate2,
                    TestData.projects.projectdocumentid2,
                    TestData.projects.sopapub,
                    TestData.projects.sopanew,
                    TestData.projects.sopaheader,
                    TestData.projects.esd,
                    TestData.projects.cenodecision);

            // get published, archived projects with lat/long
            projects = client.getMapProjects("1", "1", "", "").getProjects().getProject();
            // This should now break, because the project's expiration date is in the future
            if (projects != null)
                Assert.assertFalse(listContainsProject(projects, TestData.projects.id1));

            // get published, un-archived projects with lat/long
            projects = client.getMapProjects("1", "0", "", "").getProjects().getProject();
            // woohoo!
            Assert.assertNotSame(projects, null);
            Assert.assertTrue(listContainsProject(projects, TestData.projects.id1));

            // Get today's date
            String DATE_FORMAT = "yyyy-MM-dd";
            SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
            Calendar c1 = Calendar.getInstance(); // today
            String today = sdf.format(c1.getTime());

            // update expiration date to be in the future
            client.putProject(TestData.projects.id1,
                    TestData.projects.type1,
                    TestData.projects.name2,
                    TestData.projects.unitcode1,
                    TestData.projects.description1,
                    TestData.projects.lastupdate1,
                    TestData.projects.commentreg1,
                    TestData.projects.wwwlink1,
                    TestData.projects.wwwsummary1,
                    TestData.projects.wwwpub2,
                    TestData.projects.analysistypeid1,
                    TestData.projects.statusid1,
                    TestData.projects.contactname2,
                    TestData.projects.contactphone1,
                    TestData.projects.contactemail1,
                    purposeids2,
                    today,
                    TestData.projects.projectdocumentid2,
                    TestData.projects.sopapub,
                    TestData.projects.sopanew,
                    TestData.projects.sopaheader,
                    TestData.projects.esd,
                    TestData.projects.cenodecision);

            // get published, un-archived projects with lat/long
            projects = client.getMapProjects("1", "0", "", "").getProjects().getProject();
            // Still comes back in list, because projects expiring today are still active until the end of the day
            Assert.assertNotSame(projects, null);
            Assert.assertTrue(listContainsProject(projects, TestData.projects.id1));

            // update expiration date to be null
            client.putProject(TestData.projects.id1,
                    TestData.projects.type1,
                    TestData.projects.name2,
                    TestData.projects.unitcode1,
                    TestData.projects.description1,
                    TestData.projects.lastupdate1,
                    TestData.projects.commentreg1,
                    TestData.projects.wwwlink1,
                    TestData.projects.wwwsummary1,
                    TestData.projects.wwwpub2,
                    TestData.projects.analysistypeid1,
                    TestData.projects.statusid1,
                    TestData.projects.contactname2,
                    TestData.projects.contactphone1,
                    TestData.projects.contactemail1,
                    purposeids2,
                    null,
                    TestData.projects.projectdocumentid2,
                    TestData.projects.sopapub,
                    TestData.projects.sopanew,
                    TestData.projects.sopaheader,
                    TestData.projects.esd,
                    TestData.projects.cenodecision);

            // get published, un-archived projects with lat/long
            projects = client.getMapProjects("1", "0", "", "").getProjects().getProject();
            // Still comes back in list, because projects without expiration date should be active
            Assert.assertNotSame(projects, null);
            Assert.assertTrue(listContainsProject(projects, TestData.projects.id1));
        } catch (IOException ex) {
            Logger.getLogger(ProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        delete();
    }

    
    private Boolean listContainsProject(List<Project> projects, String project_id) {
//        System.out.println("DEBUG: Looking for project id " + project_id + " in list of size " + projects.size());
        for (Project p : projects) {
//            System.out.println("DEBUG: p.getId()=" + p.getId());
            if (project_id.equalsIgnoreCase(p.getId())) {
                return true;
            }
        }
        return false;
    }


    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getProject("nepa", TestData.projects.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteProject("nepa", TestData.projects.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }

    @Test
    public void testAddMinimalProject() {
        // Add the entity
        Project project = new Project();
        project.setType(TestData.projects.type1);
        project.setId(TestData.projects.id1);
        project.setName(TestData.projects.name1);
        project.setUnitcode(TestData.projects.unitcode1);
        // Adminapp is set by Project resource class
        project.setDescription(TestData.projects.description1);
        project.setCommentreg(TestData.projects.commentreg1);
        project.setWwwsummary(TestData.projects.wwwsummary1);
        project.setWwwpub(TestData.projects.wwwpub1);

        Nepainfo nepainfo = new Nepainfo();
        nepainfo.setProjectdocumentid(TestData.projects.projectdocumentid1);
        nepainfo.setAnalysistypeid(TestData.projects.analysistypeid1);
        nepainfo.setStatusid(TestData.projects.statusid1);

        Purposeids purposeids = new Purposeids();
        purposeids.getPurposeid().add(TestData.projects.purposeid1);

        nepainfo.setPurposeids(purposeids);
        project.setNepainfo(nepainfo);

        try {
            client.postProject(project);
        } catch (IOException ex) {
            Logger.getLogger(ProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Delete the entity
        try {
            client.deleteProject("nepa", TestData.projects.id1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void post()
    {
        try {
            client.postProject(TestData.projects.id1,
                    TestData.projects.type1,
                    TestData.projects.name1,
                    TestData.projects.unitcode1,
                    TestData.projects.description1,
                    TestData.projects.lastupdate1,
                    TestData.projects.commentreg1,
                    TestData.projects.wwwlink1,
                    TestData.projects.wwwsummary1,
                    TestData.projects.wwwpub1,
                    TestData.projects.analysistypeid1,
                    TestData.projects.statusid1,
                    TestData.projects.contactname1,
                    TestData.projects.contactphone1,
                    TestData.projects.contactemail1,
                    purposeids1,
                    TestData.projects.expirationdate1,
                    TestData.projects.projectdocumentid1,
                    TestData.projects.sopapub,
                    TestData.projects.sopanew,
                    TestData.projects.sopaheader,
                    TestData.projects.esd,
                    TestData.projects.cenodecision);
        } catch (IOException ex) {
            Logger.getLogger(ProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            // Clean up locations
            new ProjectLocationClient().putProjectLocations( TestData.projects.id1,
                                        "",
                                        null,
                                        null,
                                        null,
                                        new ArrayList<String>(),
                                        new ArrayList<String>(),
                                        new ArrayList<String>(),
                                        new ArrayList<String>(),
                                        new ArrayList<String>());
            client.deleteProject("nepa", TestData.projects.id1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteProject("nepa", TestData.projects.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }


    @BeforeClass
    public static void populateDependencies()
    {
        // No dependencies
    }

    @AfterClass
    public static void cleanupDependencies()
    {
        // No cleanup
    }
}
