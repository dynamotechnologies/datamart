package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.CARAPhaseDocManagerRemote;
import stateless.CARAProjectManagerRemote;
import stateless.ProjectManagerRemote;
import us.fed.fs.www.nepa.schema.caradocument.Caralinkdoc;
import us.fed.fs.www.nepa.schema.caradocument.Caraunlinkdoc;
import us.fed.fs.www.nepa.schema.caraproject.Caralinkproject;
import us.fed.fs.www.nepa.schema.caraproject.Caraproject;
import us.fed.fs.www.nepa.schema.caraproject.Caraunlinkproject;
import us.fed.fs.www.nepa.schema.project.Projectsearchresults;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Properties;


@Path ("/utils/cara")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class CaraUtilsResource extends AbstractResource
{
    private ProjectManagerRemote projectbeanmanager;
    private CARAProjectManagerRemote caraprojectbeanmanager;
    private CARAPhaseDocManagerRemote caraphasedocbeanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    private DataMartUtil UTIL = null;
    int PROJECT_ID;
    int CARAPROJECT_ID;
    int CARAPHASEDOC_ID;

    public CaraUtilsResource()
            throws ServletException
    {
        UTIL = DataMartUtil.getInstance();
        if (UTIL==null)
            throw new ServletException("Horror! Could not get a reference to the Datamart Utility singleton! Exiting in shame.");

        // Initialize managers
        Properties PROJECT_PROPS = new Properties();
        PROJECT_PROPS.setProperty("schema", "Project.xsd");
        PROJECT_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.project");
        PROJECT_PROPS.setProperty("bean", "ProjectManagerRemote");

        Properties CARAPROJECT_PROPS = new Properties();
        CARAPROJECT_PROPS.setProperty("schema", "CaraProject.xsd");
        CARAPROJECT_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.caraproject");
        CARAPROJECT_PROPS.setProperty("bean", "CARAProjectManagerRemote");

        Properties CARAPHASEDOC_PROPS = new Properties();
        CARAPHASEDOC_PROPS.setProperty("schema", "CaraDocument.xsd");
        CARAPHASEDOC_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.caradocument");
        CARAPHASEDOC_PROPS.setProperty("bean", "CARAPhaseDocManagerRemote");

        try {
            PROJECT_ID = UTIL.getResourceId(PROJECT_PROPS);
            CARAPROJECT_ID = UTIL.getResourceId(CARAPROJECT_PROPS);
            CARAPHASEDOC_ID = UTIL.getResourceId(CARAPHASEDOC_PROPS);
            projectbeanmanager = (ProjectManagerRemote)UTIL.getManager(PROJECT_ID);
            caraprojectbeanmanager = (CARAProjectManagerRemote)UTIL.getManager(CARAPROJECT_ID);
            caraphasedocbeanmanager = (CARAPhaseDocManagerRemote)UTIL.getManager(CARAPHASEDOC_ID);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("CaraUtilsResource failed to start. Could not start the bean manager.");
        }

        if (!projectbeanmanager.ping())
            throw new ServletException("ProjectResource failed to start. Could not ping the bean manager!");
        if (!caraprojectbeanmanager.ping())
            throw new ServletException("CARAProjectResource failed to start. Could not ping the bean manager!");
        if (!caraphasedocbeanmanager.ping())
            throw new ServletException("CARAPhaseDocResource failed to start. Could not ping the bean manager!");
    }



    @GET
    @Path("/findNEPAProject")
    public Response findNEPAProject(
            @QueryParam("name") String name,
            @QueryParam("id") String palsid,
            @QueryParam("unit") String unitliststr,
            @QueryParam("status") String statusliststr,
            @QueryParam("pubflag") Integer pubflagval,
            @QueryParam("archiveflag") Integer archiveflagval,
            @QueryParam("rows") Integer rows,
            @QueryParam("start") Integer start,
            @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        String [] unitlist = {};
        ArrayList<Integer> statuslist = new ArrayList<Integer>();

        if (unitliststr != null) {
            unitlist = unitliststr.trim().split(",");
            for (int i=0; i < unitlist.length; i++) {
                unitlist[i] = fixWildcards(unitlist[i].trim());
            }
        }

        if (statusliststr != null) {
            String [] statuses = statusliststr.trim().split(",");
            for (int i=0; i < statuses.length; i++) {
                try {
                int status = Integer.valueOf(statuses[i]);
                statuslist.add(status);
                } catch (NumberFormatException e) { }
            }
        }

        Integer resultStart = 0;
        if (start != null) {
            resultStart = start;
        }

        Integer resultRows = 100;   // TODO: Make this a config
        if (rows != null) {
            resultRows = rows;
        }

        try {
            Boolean maponly = null; // disregard map settings
            Boolean pubflag = getBooleanFromInt("pubflag", pubflagval);
            Boolean archiveflag = getBooleanFromInt("archiveflag", archiveflagval);

            // Retrieve the specified projects
            Integer statusAr[] = new Integer[statuslist.size()];
            statusAr = statuslist.toArray(statusAr);
            Projectsearchresults results = projectbeanmanager.search(fixWildcards(name), fixWildcards(palsid), unitlist, statusAr, maponly, pubflag, archiveflag, resultStart, resultRows);

            results.getResultmetadata().setRequest(uriInfo.getRequestUri().toString());

            // Get the query results in a JAXBElement object, marshal and return
            us.fed.fs.www.nepa.schema.project.ObjectFactory objectfactory = new us.fed.fs.www.nepa.schema.project.ObjectFactory();
            JAXBElement<Projectsearchresults> jaxbelement = objectfactory.createProjectsearchresults(results);

            JAXBContext jaxbcontext = UTIL.getContext(PROJECT_ID);
            Marshaller marshaller = jaxbcontext.createMarshaller();
            marshaller.marshal(jaxbelement, sw);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    /*
     * Make asterisk the match-all wildcard
     * Replace JPQL wildcards with escaped versions of same
     *
     * Note that literal backslashes need to be paired for Java strings.
     * Also: don't use String.replaceAll unless you desperately want regex substitution
     */
    private String fixWildcards(String src) {
        if (src == null) return src;
        
        String result = src;
        result = result.replace("%", "\\%");
        result = result.replace("_", "\\_");
        result = result.replace("*", "%");

        return result;
    }


    @POST
    @Path("/linkProject")
    public Response linkProject(InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            // NOTE: Must use unmarshaller from DataMartUtil that specifies resource ID as
            // first parameter, because inherited unmarshaller infers package from MY_PROPS
            Caralinkproject linkrequest = (Caralinkproject)unmarshal(UTIL, CARAPROJECT_ID, inputxml, Caralinkproject.class);

            // All clear. Now do the insert.
            caraprojectbeanmanager.linkproject(linkrequest);
        } catch (DataMartException ex) {
            handleException(ex);
        }

        return ok("");
    }


    @POST
    @Path("/unlinkProject")
    public Response unlinkProject(InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            // NOTE: Must use unmarshaller from DataMartUtil that specifies resource ID as
            // first parameter, because inherited unmarshaller infers package from MY_PROPS
            Caraunlinkproject unlinkrequest = (Caraunlinkproject)unmarshal(UTIL, CARAPROJECT_ID, inputxml, Caraunlinkproject.class);

            // All clear. Now do the insert.
            caraprojectbeanmanager.unlinkproject(unlinkrequest);
        } catch (DataMartException ex) {
            handleException(ex);
        }

        return ok("");
    }



    @POST
    @Path("/linkDoc")
    public Response linkDoc(InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            // NOTE: Must use unmarshaller from DataMartUtil that specifies resource ID and UTIL as
            // first two parameters, because inherited unmarshaller infers package from MY_PROPS
            Caralinkdoc linkrequest = (Caralinkdoc)unmarshal(UTIL, CARAPHASEDOC_ID, inputxml, Caralinkdoc.class);

            // All clear. Now do the insert.
            caraphasedocbeanmanager.linkdoc(linkrequest);
        } catch (DataMartException ex) {
            handleException(ex);
        }

        return ok("");
    }



    @POST
    @Path("/unlinkDoc")
    public Response unlinkDoc(InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            // NOTE: Must use unmarshaller from DataMartUtil that specifies resource ID as
            // first parameter, because inherited unmarshaller infers package from MY_PROPS
            Caraunlinkdoc unlinkrequest = (Caraunlinkdoc)unmarshal(UTIL, CARAPHASEDOC_ID, inputxml, Caraunlinkdoc.class);

            // All clear. Now do the insert.
            caraphasedocbeanmanager.unlinkdoc(unlinkrequest);
        } catch (DataMartException ex) {
            handleException(ex);
        }

        return ok("");
    }


    @POST
    @Path("/projectbypalsid/{palsid}/readingroom/setactive")
    public Response activateReadingRoom(@PathParam("palsid") String palsid, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            // Convert palsid to caraid
            Caraproject cp = caraprojectbeanmanager.getByPalsId(palsid);
            int caraid = cp.getCaraid();

            // Create update request
            cp.setReadingroomactive(true);
            caraprojectbeanmanager.update(cp);
        } catch (DataMartException ex) {
            handleException(ex);
        }

        return ok("");
    }


    @POST
    @Path("/projectbypalsid/{palsid}/readingroom/setinactive")
    public Response deactivateReadingRoom(@PathParam("palsid") String palsid, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            // Convert palsid to caraid
            Caraproject cp = caraprojectbeanmanager.getByPalsId(palsid);
            int caraid = cp.getCaraid();

            // Create update request
            cp.setReadingroomactive(false);
            caraprojectbeanmanager.update(cp);
        } catch (DataMartException ex) {
            handleException(ex);
        }

        return ok("");
    }
}
