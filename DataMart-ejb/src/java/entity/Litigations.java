package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "Litigations")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Litigations.findAll", query = "SELECT l FROM Litigations l"),
    @NamedQuery(name = "Litigations.findById", query = "SELECT l FROM Litigations l WHERE l.id = :id"),
    @NamedQuery(name = "Litigations.findByCaseName", query = "SELECT l FROM Litigations l WHERE l.caseName = :caseName"),
    @NamedQuery(name = "Litigations.findByClosedDate", query = "SELECT l FROM Litigations l WHERE l.closedDate = :closedDate")})
public class Litigations implements Serializable {
    @Column(name =     "ClosedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date closedDate;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Column(name = "CaseName")
    private String caseName;
    @JoinColumn(name = "Status", referencedColumnName = "Id")
    @ManyToOne
    private RefLitigationStatuses refLitigationStatuses;
    @JoinColumn(name = "Outcome", referencedColumnName = "Id")
    @ManyToOne
    private RefLitigationOutcomes refLitigationOutcomes;
    @JoinColumn(name = "DecisionId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Decisions decisions;

    public Litigations() {
    }

    public Litigations(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCaseName() {
        return caseName;
    }

    public void setCaseName(String caseName) {
        this.caseName = caseName;
    }

    public RefLitigationStatuses getRefLitigationStatuses() {
        return refLitigationStatuses;
    }

    public void setRefLitigationStatuses(RefLitigationStatuses refLitigationStatuses) {
        this.refLitigationStatuses = refLitigationStatuses;
    }

    public RefLitigationOutcomes getRefLitigationOutcomes() {
        return refLitigationOutcomes;
    }

    public void setRefLitigationOutcomes(RefLitigationOutcomes refLitigationOutcomes) {
        this.refLitigationOutcomes = refLitigationOutcomes;
    }

    public Decisions getDecisions() {
        return decisions;
    }

    public void setDecisions(Decisions decisions) {
        this.decisions = decisions;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Litigations)) {
            return false;
        }
        Litigations other = (Litigations) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Litigations[ id=" + id + " ]";
    }

    public Date getClosedDate() {
        return closedDate;
    }

    public void setClosedDate(Date closedDate) {
        this.closedDate = closedDate;
    }

}
