package stateless;

import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.decisionappealrule.*;
import beanutil.DataMartException;

/*
 *
 * The Manager beans are responsible for:
 *   1) Implementing business logic and
 *   2) Translating front-end resources to back-end database entities.
 *
 * Often the resources will consist of data from numerous database tables. It is the manager's responsibility
 *   to ensure that these front-end resources can be accessed as though they were cut from a single piece of wood.
 *
 */
@Remote
public interface DecisionAppealRuleManagerRemote {
    public void create(Decisionappealrule resource) throws DataMartException;
    public Decisionappealrule get(int decisionid, String id) throws DataMartException;
    public Decisionappealrules getList(int decisionid) throws DataMartException;
    public void delete(int decisionid, String id) throws DataMartException;
    public Boolean ping();
}