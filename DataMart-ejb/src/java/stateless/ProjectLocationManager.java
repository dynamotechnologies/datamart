package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.projectlocation.*;

import beanutil.*;
import entity.RefUnits;
import java.util.ArrayList;
import javax.ejb.Stateless;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless(mappedName="ProjectLocationManagerRemote")
public class ProjectLocationManager implements ProjectLocationManagerRemote
{

    @PersistenceContext
    private EntityManager em;

    private String PROJECT_TYPE = "nepa";

    @Override
    public void merge(Locations resource)
             throws DataMartException
    {
        // Get a reference to the project
        entity.Projects project = DataMartBeanUtil.getProject(em, this.PROJECT_TYPE, resource.getProjectid());
        if (project==null)
            throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION,
                    "Project with type '" + this.PROJECT_TYPE + "' and ID '" + resource.getProjectid() + "' does not exist.");
        
        Double oldLat = project.getLatitude();
        Double oldLon = project.getLongitude();
        
        // nuke old relationships in the entity
        DataMartBeanUtil.removeProjectLocationRelationships(em, project);

        // Set the non-relationship location information
        project.setLocationDesc(resource.getLocationdesc());
        project.setLocationLegalDesc(resource.getLocationlegaldesc());
        project.setLatitude(resource.getLatitude());
        project.setLongitude(resource.getLongitude());

        // extract relationships from the resource and merge them
        mergeProjectRegions(project, resource);
        mergeProjectForests(project, resource);
        mergeProjectDistricts(project, resource);
        mergeProjectStates(project, resource);
        mergeProjectCounties(project, resource);
        
        //only update the KML file after changes have merged successfully
        Double newLat = resource.getLatitude();
        Double newLon = resource.getLongitude();
        if ((oldLat == null ? newLat != null : !oldLat.equals(newLat)) ||
                (oldLon == null ? newLon != null : !oldLon.equals(newLon))) {
            if (newLat == null || newLon == null) {
                try {
                    ServiceHelper.deleteKML(project.getPublicId(),
                                            project.getRefUnits().getId());
                } catch (ServiceException ex) {
                    Logger.getLogger(ProjectLocationManager.class.getName())
                          .log(Level.SEVERE, null, ex);
                }
            } else {
                if (project.getWwwIsPublished()) {
                    String unitId = project.getRefUnits() == null ?
                            "" : project.getRefUnits().getId();
                    String unitUrl = "";
                    if (unitId != null && unitId != "" && unitId.length() > 6){
                        List<RefUnits> result = em.createNamedQuery("RefUnits.findById")
                                .setParameter("id", unitId.substring(0, 6))
                                .getResultList();
                        if (result.size() > 0) {
                            unitUrl = result.get(0).getWwwLink();
                        }
                    }
                    try {
                        ServiceHelper.updateKML(project, unitUrl);
                    } catch (ServiceException ex) {
                        Logger.getLogger(ProjectLocationManager.class.getName())
                          .log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }


    @Override
    public Locations get(String projectid)
            throws DataMartException
    {
        // Get a reference to the project
        entity.Projects project = DataMartBeanUtil.getProject(em, this.PROJECT_TYPE, projectid);
        if (project==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "Project with type '" + this.PROJECT_TYPE + "' and ID '" + projectid + "' does not exist.");

        return createResource(project);
    }


    @Override
    public Locationslist getByUnit(String unitid, Boolean sopaonly)
            throws DataMartException
    {
        // Confirm that this is a valid unit
        entity.RefUnits unit = (entity.RefUnits) DataMartBeanUtil.getEntityById(em,
                "RefUnits", unitid, "Unit");
        if (unit==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        // Get projects for this unit
        /*
         * The sql for this join would be pretty straightforward, but it's a bit more convoluted in jpql.
         * Instead the following performs two queries and combines the results.
         */
        String sopaclause = (sopaonly) ? "p.sopaPublish = true and " : "";
        Query query1 = em.createQuery("SELECT p FROM Projects p JOIN p.projectForestsCollection f JOIN f.refUnits u WHERE " + sopaclause + "u.id LIKE :id");
        Query query2 = em.createQuery("SELECT p FROM Projects p JOIN p.projectDistrictsCollection f JOIN f.refUnits u WHERE " + sopaclause + "u.id LIKE :id");
        query1.setParameter("id", unitid+"%");
        query2.setParameter("id", unitid+"%");
        List entitylist1 =  query1.getResultList();
        List entitylist2 =  query2.getResultList();

        if (!(entitylist1.size()>0 || entitylist2.size()>0))
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        // Loop over the database collection. Each row becomes a new object in the response list
        Locationslist responselist = new Locationslist();
        for (Object e : entitylist1) {
            Locations l = createResource((entity.Projects)e);
            responselist.getLocations().add(l);
        }
        for (Object e : entitylist2) {
            Locations l = createResource((entity.Projects)e);
            // Remove duplicates from the two queries
            Boolean duped = false;
            for (Object dupe : entitylist1) {
                if ( ((entity.Projects)dupe).getId() == ((entity.Projects)e).getId() )
                    duped = true;
            }
            if (!duped) {
                responselist.getLocations().add(l);
            }
        }

        // Get nationwide and regionwide entities for the live sopa
        if (sopaonly) {
            Query query3 = em.createQuery("SELECT p FROM Projects p JOIN p.projectDistrictsCollection f JOIN f.refUnits u WHERE p.sopaPublish = true and " +
                "( u.id LIKE '11000000' OR u.id LIKE :regionwide )");
            String regionwide = unitid.substring(0, 4)+"0000";
            query3.setParameter("regionwide", regionwide);
            List entitylist3 = query3.getResultList();
            for (Object e : entitylist3) {
                // Remove duplicates from the two queries
                Boolean duped = false;
                for (Object dupe : entitylist1) {
                    if ( ((entity.Projects)dupe).getId() == ((entity.Projects)e).getId() )
                        duped = true;
                }
                for (Object dupe : entitylist2) {
                    if ( ((entity.Projects)dupe).getId() == ((entity.Projects)e).getId() )
                        duped = true;
                }
                if (!duped) {
                    responselist.getLocations().add(createResource((entity.Projects)e));
                }
            }
        }

        return responselist;
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     * Extract all region ids from the resource, confirm each one is a valid Unit,
     *     and add them to the relationship table
     */
    private void mergeProjectRegions(entity.Projects p, Locations l)
            throws DataMartException
    {
        Iterator regioniterator = l.getRegions().getRegionid().iterator();
        while (regioniterator.hasNext()) {
            entity.RefUnits ref = (entity.RefUnits) DataMartBeanUtil.getEntityById(em,
                "RefUnits", (String)regioniterator.next(), "Unit Code");
            entity.ProjectRegions r = new entity.ProjectRegions();
            r.setProjects(p);
            r.setRefUnits(ref);
            em.merge(r);
        }
    }

    /*
     * Extract all forest ids from the resource, confirm each one is a valid Unit,
     *     and add them to the relationship table
     */
    private void mergeProjectForests(entity.Projects p, Locations r)
            throws DataMartException
    {
        Iterator forestiterator = r.getForests().getForestid().iterator();
        while (forestiterator.hasNext()) {
            entity.RefUnits ref = (entity.RefUnits) DataMartBeanUtil.getEntityById(em,
                "RefUnits", (String)forestiterator.next(), "Unit Code");
            entity.ProjectForests f = new entity.ProjectForests();
            f.setProjects(p);
            f.setRefUnits(ref);
            em.merge(f);
        }
    }


    /*
     * Extract all district ids from the resource, confirm each one is a valid Unit,
     *     and add them to the relationship table
     */
    private void mergeProjectDistricts(entity.Projects p, Locations r)
            throws DataMartException
    {
        Iterator districtiterator = r.getDistricts().getDistrictid().iterator();
        while (districtiterator.hasNext()) {
            entity.RefUnits ref = (entity.RefUnits) DataMartBeanUtil.getEntityById(em,
                "RefUnits", (String)districtiterator.next(), "Unit Code");
            entity.ProjectDistricts d = new entity.ProjectDistricts();
            d.setProjects(p);
            d.setRefUnits(ref);
            em.merge(d);
        }
    }


    /*
     * Extract all state ids from the resource, confirm each one is a valid State,
     *     and add them to the relationship table
     */
    private void mergeProjectStates(entity.Projects p, Locations r)
            throws DataMartException
    {
        Iterator stateiterator = r.getStates().getStateid().iterator();
        while (stateiterator.hasNext()) {
            entity.RefStates ref = (entity.RefStates) DataMartBeanUtil.getEntityById(em,
                "RefStates", (String)stateiterator.next(), "State Code");
            entity.ProjectStates s = new entity.ProjectStates();
            s.setProjects(p);
            s.setRefStates(ref);
            em.merge(s);
        }
    }

    /*
     * Extract all county ids from the resource, confirm each one is a valid County,
     *     and add them to the relationship table
     */
    private void mergeProjectCounties(entity.Projects p, Locations r)
            throws DataMartException
    {
        Iterator countyiterator = r.getCounties().getCountyid().iterator();
        while (countyiterator.hasNext()) {
            entity.RefCounties ref = (entity.RefCounties) DataMartBeanUtil.getEntityById(em,
                    "RefCounties", (String)countyiterator.next(), "County Code");
            entity.ProjectCounties c = new entity.ProjectCounties();
            c.setProjects(p);
            c.setRefCounties(ref);
            em.merge(c);
        }
    }

    private ArrayList<String> getRegionArray(entity.Projects p)
    {
        Query q = em.createQuery("SELECT p FROM ProjectRegions p WHERE p.projects = :project");
        q.setParameter("project", p);
        List regionlist = q.getResultList();

        ArrayList<String> rval = new ArrayList<String>();
        Iterator it = regionlist.iterator();
        while (it.hasNext())
        {
            entity.ProjectRegions pr = (entity.ProjectRegions) it.next();
            rval.add( pr.getRefUnits().getId() );
        }
        return rval;
    }

    private ArrayList<String> getForestArray(entity.Projects p)
    {
        Query q = em.createQuery("SELECT p FROM ProjectForests p WHERE p.projects = :project");
        q.setParameter("project", p);
        List forestlist = q.getResultList();

        ArrayList<String> rval = new ArrayList<String>();
        Iterator it = forestlist.iterator();
        while (it.hasNext())
        {
            entity.ProjectForests pf = (entity.ProjectForests) it.next();
            rval.add( pf.getRefUnits().getId() );
        }
        return rval;
    }

    private ArrayList<String> getDistrictArray(entity.Projects p)
    {
        Query q = em.createQuery("SELECT p FROM ProjectDistricts p WHERE p.projects = :project");
        q.setParameter("project", p);
        List districtlist = q.getResultList();

        ArrayList<String> rval = new ArrayList<String>();
        Iterator it = districtlist.iterator();
        while (it.hasNext())
        {
            entity.ProjectDistricts pd = (entity.ProjectDistricts) it.next();
            rval.add( pd.getRefUnits().getId() );
        }
        return rval;
    }

    private ArrayList<String> getStateArray(entity.Projects p)
    {
        Query q = em.createQuery("SELECT p FROM ProjectStates p WHERE p.projects = :project");
        q.setParameter("project", p);
        List statelist = q.getResultList();

        ArrayList<String> rval = new ArrayList<String>();
        Iterator it = statelist.iterator();
        while (it.hasNext())
        {
            entity.ProjectStates ps = (entity.ProjectStates) it.next();
            rval.add( ps.getRefStates().getId() );
        }
        return rval;
    }

    private ArrayList<String> getCountyArray(entity.Projects p)
    {
        Query q = em.createQuery("SELECT p FROM ProjectCounties p WHERE p.projects = :project");
        q.setParameter("project", p);
        List countylist = q.getResultList();

        ArrayList<String> rval = new ArrayList<String>();
        Iterator it = countylist.iterator();
        while (it.hasNext())
        {
            entity.ProjectCounties pc = (entity.ProjectCounties) it.next();
            rval.add( pc.getRefCounties().getId() );
        }
        return rval;
    }

    private Locations createResource(entity.Projects p)
    {
        // Location Description is a required field in the schema, but defaults to null in the database
        String desc = (p.getLocationDesc()==null) ? "" : p.getLocationDesc();

        Locations l = new Locations();
        l.setProjectid(p.getPublicId());
        l.setLocationdesc(desc);
        l.setLocationlegaldesc(p.getLocationLegalDesc());
        l.setLatitude(p.getLatitude());
        l.setLongitude(p.getLongitude());

        Regions regions = new Regions();
        regions.getRegionid().addAll( getRegionArray(p) );
        l.setRegions(regions);

        Forests forests = new Forests();
        forests.getForestid().addAll( getForestArray(p) );
        l.setForests(forests);

        Districts districts = new Districts();
        districts.getDistrictid().addAll( getDistrictArray(p) );
        l.setDistricts(districts);

        States states = new States();
        states.getStateid().addAll( getStateArray(p) );
        l.setStates(states);

        Counties counties = new Counties();
        counties.getCountyid().addAll( getCountyArray(p) );
        l.setCounties(counties);

        return l;
    }
}