package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.projectgoal.*;


public class ProjectGoalClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public ProjectGoalClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.projectgoal", "ProjectGoal.xsd");
    }


    public void postProjectGoal(String projecttype, String projectid, Integer goalid, Integer order)
            throws IOException
    {
        Projectgoal projectgoal = new Projectgoal();
        projectgoal.setProjecttype(projecttype);
        projectgoal.setProjectid(projectid);
        projectgoal.setGoalid(goalid);
        projectgoal.setOrder(order);
        postProjectGoal(projectgoal);
    }


    public void postProjectGoal(Projectgoal projectgoal)
            throws IOException
    {
        String projecttype = projectgoal.getProjecttype();
        String projectid = projectgoal.getProjectid();
        post(objectfactory.createProjectgoal(projectgoal), "/projects/"+projecttype+"/"+projectid+"/goals/");
    }


    public Projectgoal getProjectGoal(String projecttype, String projectid, Integer goalid)
            throws IOException
    {
        return get("/projects/"+projecttype+"/"+projectid+"/goals/"+ goalid, Projectgoal.class);
    }

    public Projectgoals getProjectGoalsByProject(String projecttype, String projectid)
            throws IOException
    {
        return get("/projects/"+projecttype+"/"+projectid+"/goals/", Projectgoals.class);
    }
    
    public void putProjectGoal(String projecttype, String projectid, Integer goalid, Integer order)
            throws IOException
    {
        Projectgoal projectgoal = new Projectgoal();
        projectgoal.setProjecttype(projecttype);
        projectgoal.setProjectid(projectid);
        projectgoal.setGoalid(goalid);
        projectgoal.setOrder(order);
        putProjectGoal(projectgoal);
    }


    public void putProjectGoal(Projectgoal projectgoal)
            throws IOException
    {
        String projecttype = projectgoal.getProjecttype();
        String projectid = projectgoal.getProjectid();
        Integer goalid = projectgoal.getGoalid();
        put(objectfactory.createProjectgoal(projectgoal), "/projects/"+projecttype+"/"+projectid+"/goals/"+ goalid);
    }


    public void deleteProjectGoal(String projecttype, String projectid, Integer goalid)
            throws IOException
    {
        delete("/projects/"+projecttype+"/"+projectid+"/goals/" + goalid);
    }

}
