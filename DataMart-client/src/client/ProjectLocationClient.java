package client;

import java.io.IOException;
import java.util.List;
import us.fed.fs.www.nepa.schema.projectlocation.*;


public class ProjectLocationClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public ProjectLocationClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.projectlocation", "ProjectLocation.xsd");
    }


    public Locations getProjectLocations(String projectid)
            throws IOException
    {
        return get("/projects/nepa/" + projectid + "/locations/", Locations.class);
    }


    public Locationslist getByUnit(String unitcode)
            throws IOException
    {
        return get("/units/" + unitcode + "/locations/", Locationslist.class);
    }


    public void putProjectLocations(String projectid,
                                    String description,
                                    String legaldesc,
                                    Double latitude,
                                    Double longitude,
                                    List<String> regionlist,
                                    List<String> forestlist,
                                    List<String> districtlist,
                                    List<String> statelist,
                                    List<String> countylist)
            throws IOException
    {
        Locations locations = new Locations();
        Regions regions = new Regions();
        Forests forests = new Forests();
        Districts districts = new Districts();
        States states = new States();
        Counties counties = new Counties();

        if (regionlist.size()>0)
            regions.getRegionid().addAll(regionlist);
        if (forestlist.size()>0)
            forests.getForestid().addAll(forestlist);
        if (districtlist.size()>0)
            districts.getDistrictid().addAll(districtlist);
        if (statelist.size()>0)
            states.getStateid().addAll(statelist);
        if (countylist.size()>0)
            counties.getCountyid().addAll(countylist);
        
        locations.setProjectid(projectid);
        locations.setLocationdesc(description);
        locations.setLocationlegaldesc(legaldesc);
        locations.setLatitude(latitude);
        locations.setLongitude(longitude);
        locations.setRegions(regions);
        locations.setForests(forests);
        locations.setDistricts(districts);
        locations.setStates(states);
        locations.setCounties(counties);
        putProjectLocations(locations);
    }


    public void putProjectLocations(Locations locations)
            throws IOException
    {
        put(objectfactory.createLocations(locations), "/projects/nepa/" + locations.getProjectid() + "/locations/");
    }
}