package ingress.appeals;

import ingress.LegacyUtil;
import java.util.Date;
import java.util.zip.DataFormatException;
import javax.xml.datatype.XMLGregorianCalendar;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 * This class encapsulates xml ingressed from files validated with the following DTD:
 *
 * <!DOCTYPE appeals [
 * <!ELEMENT appeals (timestamp?, appeal+)>
 * <!ATTLIST appeals xmlns CDATA "http://www.fs.fed.us/pals/xmlns/appeals/">
 * <!ELEMENT timestamp (#PCDATA)>
 * <!ELEMENT appeal (id,projectid?,code,rule,appellant?,outcome,project,response)>
 * <!ELEMENT id (#PCDATA)>
 * <!ELEMENT projectid (#PCDATA)>
 * <!ELEMENT code (#PCDATA)>
 * <!ELEMENT rule (#PCDATA)>
 * <!ELEMENT appellant (#PCDATA)>
 * <!ELEMENT outcome (#PCDATA)>
 * <!ELEMENT project (name,adminunit)>
 * <!ELEMENT name (#PCDATA)>
 * <!ELEMENT adminunit (#PCDATA)>
 * <!ELEMENT response (date,documentid,filesize)>
 * <!ELEMENT date (#PCDATA)>
 * <!ELEMENT documentid (#PCDATA)>
 * <!ELEMENT filesize (#PCDATA)>
 * ]>
 *
 *
 * This class is responsible for encapsulating element `response` as specified above
 */
public class LegacyAppealResponse {
    private XMLGregorianCalendar date;
    private String documentid;
    private String filesize;

    public LegacyAppealResponse(Node n)
            throws DataFormatException
    {
        NodeList mynodes = n.getChildNodes();
        for (int i=0; i<mynodes.getLength(); i++)
        {
            if (mynodes.item(i).getNodeName().equals("date"))
                setDate(mynodes.item(i).getTextContent());
            if (mynodes.item(i).getNodeName().equals("documentid"))
                documentid = mynodes.item(i).getTextContent();
            if (mynodes.item(i).getNodeName().equals("filesize"))
                filesize = mynodes.item(i).getTextContent();
        }
        if (date==null || !(date.toString().length()>0))
            throw new DataFormatException("date not found.");
        if (!(documentid.length()>0))
            throw new DataFormatException("documentid not found.");
        if (!(filesize.length()>0))
            throw new DataFormatException("filesize not found.");
    }

    private void setDate(String d)
            throws DataFormatException
    {
        Date d2 = LegacyUtil.getDateFromString(d, "Response Date");
        date = LegacyUtil.getXMLGregorianFromDateTime(d2);
    }

    public XMLGregorianCalendar getDate()
    {
        return date;
    }

    public String getDocumentId()
    {
        return documentid;
    }

    public String getFileSize()
    {
        return filesize;
    }
}