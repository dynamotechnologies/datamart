package test.reference;

import client.SpecialAuthorityClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.specialauthority.*;


public class SpecialAuthorityTest {

    private SpecialAuthorityClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new SpecialAuthorityClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postSpecialAuthority(TestData.specialauthorities.id1,
                    TestData.specialauthorities.name2,
                    TestData.specialauthorities.description2);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Specialauthority specialauthority = null;
        Specialauthorities specialauthoritylist = null;
        try {
            specialauthority = client.getSpecialAuthority(TestData.specialauthorities.id1);
            specialauthoritylist = client.getSpecialAuthorityList();
        } catch (IOException ex) {
            Logger.getLogger(SpecialAuthorityTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.specialauthorities.id1, specialauthority.getId());
        Assert.assertEquals(TestData.specialauthorities.name1, specialauthority.getName());
        Assert.assertEquals(TestData.specialauthorities.description1, specialauthority.getDescription());
        Assert.assertTrue(specialauthoritylist.getSpecialauthority().get(0).getName().length()>0);
        Assert.assertTrue(specialauthoritylist.getSpecialauthority().get(0).getId().length()>0);

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        Specialauthority specialauthority = null;
        try {
            client.putSpecialAuthority(TestData.specialauthorities.id1,
                    TestData.specialauthorities.name2,
                    TestData.specialauthorities.description2);
            specialauthority = client.getSpecialAuthority(TestData.specialauthorities.id1);
        } catch (Exception ex) {
            Logger.getLogger(SpecialAuthorityTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.specialauthorities.id1, specialauthority.getId());
        Assert.assertEquals(TestData.specialauthorities.name2, specialauthority.getName());
        Assert.assertEquals(TestData.specialauthorities.description2, specialauthority.getDescription());

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getSpecialAuthority(TestData.specialauthorities.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteSpecialAuthority(TestData.specialauthorities.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }

    private void post()
    {
        try {
            client.postSpecialAuthority(TestData.specialauthorities.id1,
                    TestData.specialauthorities.name1,
                    TestData.specialauthorities.description1);
        } catch (IOException ex) {
            Logger.getLogger(SpecialAuthorityTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteSpecialAuthority(TestData.specialauthorities.id1);
        } catch (IOException ex) {
            Logger.getLogger(SpecialAuthorityTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteSpecialAuthority(TestData.specialauthorities.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }
}