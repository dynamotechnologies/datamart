/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author gauri
 */
@Entity
@Table(name = "ProjectMeetingNoticeDocs")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProjectMeetingNoticeDocs.findAll", query = "SELECT p FROM ProjectMeetingNoticeDocs p"),
    @NamedQuery(name = "ProjectMeetingNoticeDocs.findByDocumentOrder", query = "SELECT p FROM ProjectMeetingNoticeDocs p WHERE p.documentOrder = :documentOrder"),
    @NamedQuery(name = "ProjectMeetingNoticeDocs.findByMeetingNoticeDocId", query = "SELECT p FROM ProjectMeetingNoticeDocs p WHERE p.meetingNoticeDocId = :meetingNoticeDocId")})
public class ProjectMeetingNoticeDocs implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "documentOrder")
    private Integer documentOrder;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "meetingNoticeDocId")
    private Integer meetingNoticeDocId;
    @JoinColumn(name = "meetingNoticeId", referencedColumnName = "meetingId")
    @ManyToOne(optional = false)
    private ProjectMeetingNotices meetingNoticeId;
    @JoinColumn(name = "documentId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private ProjectDocuments documentId;

    public ProjectMeetingNoticeDocs() {
    }

    public ProjectMeetingNoticeDocs(Integer meetingNoticeDocId) {
        this.meetingNoticeDocId = meetingNoticeDocId;
    }

    public Integer getDocumentOrder() {
        return documentOrder;
    }

    public void setDocumentOrder(Integer documentOrder) {
        this.documentOrder = documentOrder;
    }

    public Integer getMeetingNoticeDocId() {
        return meetingNoticeDocId;
    }

    public void setMeetingNoticeDocId(Integer meetingNoticeDocId) {
        this.meetingNoticeDocId = meetingNoticeDocId;
    }

    public ProjectMeetingNotices getMeetingNoticeId() {
        return meetingNoticeId;
    }

    public void setMeetingNoticeId(ProjectMeetingNotices meetingNoticeId) {
        this.meetingNoticeId = meetingNoticeId;
    }

    public ProjectDocuments getDocumentId() {
        return documentId;
    }

    public void setDocumentId(ProjectDocuments documentId) {
        this.documentId = documentId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (meetingNoticeDocId != null ? meetingNoticeDocId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProjectMeetingNoticeDocs)) {
            return false;
        }
        ProjectMeetingNoticeDocs other = (ProjectMeetingNoticeDocs) object;
        if ((this.meetingNoticeDocId == null && other.meetingNoticeDocId != null) || (this.meetingNoticeDocId != null && !this.meetingNoticeDocId.equals(other.meetingNoticeDocId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ProjectMeetingNoticeDocs[ meetingNoticeDocId=" + meetingNoticeDocId + " ]";
    }
    
}
