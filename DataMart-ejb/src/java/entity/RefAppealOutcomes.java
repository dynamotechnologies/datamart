package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "RefAppealOutcomes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RefAppealOutcomes.findAll", query = "SELECT r FROM RefAppealOutcomes r"),
    @NamedQuery(name = "RefAppealOutcomes.findById", query = "SELECT r FROM RefAppealOutcomes r WHERE r.id = :id"),
    @NamedQuery(name = "RefAppealOutcomes.findByOutcome", query = "SELECT r FROM RefAppealOutcomes r WHERE r.outcome = :outcome")})
public class RefAppealOutcomes implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "Outcome")
    private String outcome;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refAppealOutcomes")
    private Collection<Appeals> appealsCollection;

    public RefAppealOutcomes() {
    }

    public RefAppealOutcomes(Integer id) {
        this.id = id;
    }

    public RefAppealOutcomes(Integer id, String outcome) {
        this.id = id;
        this.outcome = outcome;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOutcome() {
        return outcome;
    }

    public void setOutcome(String outcome) {
        this.outcome = outcome;
    }

    @XmlTransient
    public Collection<Appeals> getAppealsCollection() {
        return appealsCollection;
    }

    public void setAppealsCollection(Collection<Appeals> appealsCollection) {
        this.appealsCollection = appealsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RefAppealOutcomes)) {
            return false;
        }
        RefAppealOutcomes other = (RefAppealOutcomes) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RefAppealOutcomes[ id=" + id + " ]";
    }

}
