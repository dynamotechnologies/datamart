package test.relationship;

import client.ProjectDistrictClient;
import client.ProjectClient;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.projectlocation.*;


public class ProjectDistrictTest {
    private ProjectDistrictClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new ProjectDistrictClient();
    }


    @Test
    public void testPut()
    {
        put();

        // second PUT should overwrite the first
        try {
            client.putProjectDistrict(TestData.projects.id1, TestData.projectlocations.districtid1);
            client.putProjectDistrict(TestData.projects.id1, TestData.projectlocations.districtid2);
        } catch (Exception ex) {
            Logger.getLogger(ProjectDistrictTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        put();

        Districts district1 = null;
        Districts district2 = null;
        try {
            district1 = client.getProjectDistrict(TestData.projects.id1, TestData.projectlocations.districtid1);
            district2 = client.getProjectDistrict(TestData.projects.id1, TestData.projectlocations.districtid2);
        } catch (Exception ex) {
            Logger.getLogger(ProjectDistrictTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // make sure each GET response is exactly one district, and that it matches the one we PUTted.
        Assert.assertEquals(1, district1.getDistrictid().size());
        Assert.assertEquals(1, district2.getDistrictid().size());
        Assert.assertEquals(TestData.projectlocations.districtid1, district1.getDistrictid().get(0));
        Assert.assertEquals(TestData.projectlocations.districtid2, district2.getDistrictid().get(0));

        delete();
    }

    @Test
    public void testDelete()
    {
        put();
        delete();

        try {
            client.deleteProjectDistrict(TestData.projects.id1, TestData.projectlocations.districtid1);
            // should throw exception, because the district no longer exists
            Assert.fail();
        } catch (Exception ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
        try {
            client.deleteProjectDistrict(TestData.projects.id1, TestData.projectlocations.districtid2);
            // should throw exception, because the district no longer exists
            Assert.fail();
        } catch (Exception ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }


    private void put()
    {
        try {
            client.putProjectDistrict(TestData.projects.id1, TestData.projectlocations.districtid1);
            client.putProjectDistrict(TestData.projects.id1, TestData.projectlocations.districtid2);
        } catch (Exception ex) {
            Logger.getLogger(ProjectDistrictTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteProjectDistrict(TestData.projects.id1, TestData.projectlocations.districtid1);
            client.deleteProjectDistrict(TestData.projects.id1, TestData.projectlocations.districtid2);
        } catch (Exception ex) {
            Logger.getLogger(ProjectDistrictTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteProjectDistrict(TestData.projects.id1, TestData.projectlocations.districtid1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
        try {
            client.deleteProjectDistrict(TestData.projects.id1, TestData.projectlocations.districtid2);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }

    
    @BeforeClass
    public static void  populateDependencies()
    {
        ProjectClient projclient = null;
        try {
            projclient = new ProjectClient();
        } catch (Exception ex) {
            Logger.getLogger(ProjectDistrictTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        // Project Districts rely on Projects. Pre-populate a Project.
        ArrayList<String> purposeids1 = new ArrayList<String>();
        purposeids1.add(TestData.projects.purposeid1);
        purposeids1.add(TestData.projects.purposeid2);
        purposeids1.add(TestData.projects.purposeid3);
        try {
            projclient.postProject(TestData.projects.id1, TestData.projects.type1,
                    TestData.projects.name1, TestData.projects.unitcode1, TestData.projects.description1,
                    TestData.projects.lastupdate1, TestData.projects.commentreg1, TestData.projects.wwwlink1,
                    TestData.projects.wwwsummary1, TestData.projects.wwwpub1, TestData.projects.analysistypeid1,
                    TestData.projects.statusid1, TestData.projects.contactname1, TestData.projects.contactphone1,
                    TestData.projects.contactemail1, purposeids1, TestData.projects.expirationdate1, TestData.projects.projectdocumentid1,
                    TestData.projects.sopapub, TestData.projects.sopanew, TestData.projects.sopaheader, TestData.projects.esd, TestData.projects.cenodecision);
        } catch (IOException ex) {
            Logger.getLogger(ProjectDistrictTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @AfterClass
    public static void  cleanupDependencies()
    {
        ProjectClient projclient = null;
        try {
            projclient = new ProjectClient();
        } catch (Exception ex) {
            Assert.fail();
        }

        // Clean up the Project
        try {
            projclient.deleteProject("nepa", TestData.projects.id1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectDistrictTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }
}