package test.relationship;

import client.ProjectClient;
import client.ProjectGoalClient;
import client.GoalClient;
import client.UnitClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import org.junit.*;
import test.TestData;
import us.fed.fs.www.nepa.schema.project.Project;
import us.fed.fs.www.nepa.schema.projectgoal.*;


public class ProjectGoalTest {
    private ProjectGoalClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new ProjectGoalClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postProjectGoal(
                    TestData.projects.type1,
                    TestData.projects.id1,
                    TestData.goals.id1,
                    TestData.projectgoals.order1);

            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Projectgoal projectGoal = null;
        try {
            projectGoal = client.getProjectGoal(TestData.projects.type1, TestData.projects.id1, TestData.goals.id1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectGoalTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        Assert.assertTrue(TestData.goals.id1 == projectGoal.getGoalid());
        Assert.assertTrue(TestData.projectgoals.order1 == projectGoal.getOrder());

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        try {
            // Update project goal, change order
            client.putProjectGoal(
                    TestData.projects.type1,
                    TestData.projects.id1,
                    TestData.goals.id1,
                    TestData.projectgoals.order2
                    );
        } catch (IOException ex) {
            Logger.getLogger(ProjectGoalTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        Projectgoal projectGoal = null;
        try {
            projectGoal = client.getProjectGoal(TestData.projects.type1, TestData.projects.id1, TestData.goals.id1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectGoalTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        
        Assert.assertTrue(TestData.goals.id1 == projectGoal.getGoalid());
        Assert.assertTrue(TestData.projectgoals.order2 == projectGoal.getOrder());

        delete();
    }

    @Test
    public void testGetAll()
    {
        post();
        
        Projectgoals pgs = null;
        try {
            pgs = client.getProjectGoalsByProject(TestData.projects.type1, TestData.projects.id1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectGoalTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        
        Assert.assertTrue(pgs.getProjectgoal().size() == 1);
        
        Projectgoal pg = (Projectgoal)pgs.getProjectgoal().get(0);
        Assert.assertTrue(TestData.goals.id1 == pg.getGoalid());
        Assert.assertEquals(TestData.projectgoals.order1, pg.getOrder());

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getProjectGoal(TestData.projects.type1, TestData.projects.id1, TestData.goals.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteProjectGoal(TestData.projects.type1, TestData.projects.id1, TestData.goals.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }
    
    private void post()
    {
        try {
            client.postProjectGoal(
                    TestData.projects.type1,
                    TestData.projects.id1,
                    TestData.goals.id1,
                    TestData.projectgoals.order1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectGoalTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteProjectGoal(TestData.projects.type1, TestData.projects.id1, TestData.goals.id1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectGoalTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    @After
    public void cleanFailed()
            throws Exception
    { }

    @BeforeClass
    public static void populateDependencies() throws IOException
    {
        // Create test unit
        UnitClient unitclient = new UnitClient();
        unitclient.postUnit(TestData.goals.testunitid, TestData.goals.testunitname);
        
        // Create test project
        ArrayList<String> purposeids = new ArrayList<String>();
        ProjectClient projectclient = new ProjectClient();
        projectclient.postProject(
                    TestData.projects.id1,
                    TestData.projects.type1,
                    TestData.projects.name1,
                    TestData.goals.testunitid,
                    TestData.projects.description1,
                    TestData.projects.lastupdate1,
                    TestData.projects.commentreg1,
                    TestData.projects.wwwlink1,
                    TestData.projects.wwwsummary1,
                    TestData.projects.wwwpub1,
                    TestData.projects.analysistypeid1,
                    TestData.projects.statusid1,
                    TestData.projects.contactname1,
                    TestData.projects.contactphone1,
                    TestData.projects.contactemail1,
                    purposeids,
                    TestData.projects.expirationdate1,
                    TestData.projects.projectdocumentid1,
                    TestData.projects.sopapub,
                    TestData.projects.sopanew,
                    TestData.projects.sopaheader,
                    TestData.projects.esd,
                    TestData.projects.cenodecision);
         
        // Create test goal
        GoalClient gclient = new GoalClient();
        gclient.postGoal(TestData.goals.id1, TestData.goals.description1);
    }

    @AfterClass
    public static void cleanupDependencies() throws IOException
    {
        GoalClient gclient = new GoalClient();
        try {
            // Delete test goal
            gclient.deleteGoal(TestData.goals.id1);
        } catch (IOException e) {
            if (gclient.getStatus() != 404) {
                throw e;
            }
        }
        
        ProjectClient projectclient = new ProjectClient();
        try {
            // Delete test project
            projectclient.deleteProject(TestData.projects.type1, TestData.projects.id1);
        } catch (IOException e) {
            if (projectclient.getStatus() != 404) {
                throw e;
            }
        }
        
        UnitClient unitclient = new UnitClient();
        try {
            // Delete test unit
            unitclient.deleteUnit(TestData.goals.testunitid);
        } catch (IOException e) {
            if (unitclient.getStatus() != 404) {
                throw e;
            }
        }
    }
}
