package stateless;

import javax.ejb.Stateless;
import beanutil.*;
import java.sql.Date;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import static stateless.ProjectMeetingNoticesManager.fill;

import us.fed.fs.www.nepa.schema.ProjectAdditionalInfo.*;

@Stateless(mappedName="ProjectAdditionalInfoManagerRemote")
public class ProjectAdditionalInfoManager implements ProjectAdditionalInfoManagerRemote {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void create(us.fed.fs.www.nepa.schema.ProjectAdditionalInfo.ProjectAdiitionalInfo resource)
             throws DataMartException
    {
        validate(resource);
        entity.ProjectAdditionalInfo entity = createEntity(resource);

        entity.ProjectAdditionalInfo badentity = createEntity(entity.getAddInfoId());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        entity = em.merge(entity);
    }

    @Override
    public ProjectAdiitionalInfo get(String key) 
             throws DataMartException
    {
        entity.ProjectAdditionalInfo entity = createEntity(new Integer(key));
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return createResource(entity);
    }
    public ProjectAdiitionalInfos getAllByProject(String projecttype, String projectid)
            throws DataMartException {
         
        // Get Project
        entity.Projects project = DataMartBeanUtil.getProject(em, projecttype, projectid);

        // Retrieve project resource area entities from the database
        Query query = em.createQuery("SELECT a FROM ProjectAdditionalInfo a WHERE a.projectId = :project");
        query.setParameter("project", project);
        List entitylist =  query.getResultList();
        Iterator it = entitylist.iterator();

        // Loop over the database collection. Each row becomes a new object in the response list
        ProjectAdiitionalInfos responselist = new ProjectAdiitionalInfos();
        while (it.hasNext()) {
            responselist.getProjectAdiitionalInfo().add(createResource((entity.ProjectAdditionalInfo)it.next()));
        }
        
       
        return responselist;
    }
    @Override
    public void update(ProjectAdiitionalInfo resource)
             throws DataMartException
    {
        validate(resource);
        entity.ProjectAdditionalInfo entity = createEntity(resource);

        if (createEntity(entity.getAddInfoId())==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        entity = em.merge(entity);
    }

    @Override
    public void delete(String key)
             throws DataMartException
    {
        entity.ProjectAdditionalInfo entity = createEntity(new Integer(key));
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
      
        em.remove(em.merge(entity));
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    /*
     *
     * This method does the mapping from jaxb object to entity object and vice versa
     */
    private ProjectAdiitionalInfo createResource(entity.ProjectAdditionalInfo entity)
            throws DataMartException
    {
        ProjectAdiitionalInfo resource = new ProjectAdiitionalInfo();
        resource.setAddInfoId(entity.getAddInfoId().toString());
        resource.setProjectId(entity.getProjectId().getPublicId());
        resource.setDescription(entity.getDescription());
        resource.setLastUpdateBy(entity.getLastUpdateBy());
        Calendar cal = Calendar.getInstance();
        cal.setTime(entity.getLasteUpdateDate());
        String formatedDate = fill('0',2,""+(cal.get(Calendar.MONTH) + 1) )+ "/" + cal.get(Calendar.DATE) + "/" +cal.get(Calendar.YEAR);
        resource.setLasteUpdateDate(formatedDate);      

        return resource;
    }

    private entity.ProjectAdditionalInfo createEntity(ProjectAdiitionalInfo resource)
            throws DataMartException
    {
        entity.ProjectAdditionalInfo entity = new entity.ProjectAdditionalInfo();
        entity.Projects project = DataMartBeanUtil.getProject(em, "nepa", resource.getProjectId());
       // entity..setProjects(project);
        
        //entity.setAddInfoId(Integer.SIZE);
        entity.setAddInfoId(new Integer(resource.getAddInfoId()));
        entity.setDescription(resource.getDescription());
        entity.setLastUpdateBy(resource.getLastUpdateBy());
        entity.setLasteUpdateDate(Date.valueOf(resource.getLasteUpdateDate()));
        entity.setProjectId(project);        
        return entity;
    }

    private entity.ProjectAdditionalInfo createEntity(Integer key)
            throws DataMartException
    {
        return em.find(entity.ProjectAdditionalInfo.class, key);
    }

    private void validate(ProjectAdiitionalInfo resource)
            throws DataMartException
    {
         //Ensure these are not empty strings
        DataMartBeanUtil.confirmNotNull(resource.getAddInfoId(), "addInfoId");
        DataMartBeanUtil.confirmNotNull(resource.getProjectId(), "projectid");
    }

}