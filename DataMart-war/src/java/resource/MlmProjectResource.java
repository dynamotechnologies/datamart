package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.MLMProjectManagerRemote;
import us.fed.fs.www.nepa.schema.mlmproject.Mlmproject;
import us.fed.fs.www.nepa.schema.mlmproject.Mlmprojects;
import us.fed.fs.www.nepa.schema.mlmproject.ObjectFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;

@Path ("/mlm/projects")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class MlmProjectResource  extends AbstractResource {

    private MLMProjectManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public MlmProjectResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "MLMProject.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.mlmproject");
        MY_PROPS.setProperty("bean", "MLMProjectManagerRemote");
        try {
            beanmanager = (MLMProjectManagerRemote) init(MLMProjectManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("MlmProjectResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("MlmProjectResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;
        try {
            Mlmproject mlmproject = (Mlmproject)unmarshal(inputxml, Mlmproject.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+mlmproject.getMlmid());

            // All clear. Now do the insert.
            beanmanager.create(mlmproject);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the newly created entity
        return created(uri);
    }


    @GET
    @Path("/{id}")
    public Response get(@PathParam("id") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            Mlmproject mlmproject = beanmanager.get(id);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Mlmproject> jaxbelement = objectfactory.createMlmproject(mlmproject);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @GET
    public Response getAll(@QueryParam("unit") String unitid,
            @QueryParam("forestmatch") String forestid,
            @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        if ((unitid == null) && (forestid == null)) {
            complainLoudly(new Exception("No match criteria specified"), Response.Status.BAD_REQUEST);
        }

        StringWriter sw = new StringWriter();
        try {

            Mlmprojects mlmprojectlist = null;

            if (forestid != null) {
                if (! forestid.matches("\\d{6}")) {
                    complainLoudly(new Exception("Bad forest ID format"), Response.Status.BAD_REQUEST);
                }
                mlmprojectlist = beanmanager.getAllByForestmatch(forestid);
            }
            else if (unitid != null) {
                if (! unitid.matches("\\d+")) {
                    complainLoudly(new Exception("Bad unit ID format"), Response.Status.BAD_REQUEST);
                }
                mlmprojectlist = beanmanager.getAllByUnit(unitid);
            }

            if (mlmprojectlist == null) {
                complainLoudly(new Exception("Error retrieving records"), Response.Status.INTERNAL_SERVER_ERROR);
            }

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Mlmprojects> jaxbelement = objectfactory.createMlmprojects(mlmprojectlist);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") String id, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(id);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}
