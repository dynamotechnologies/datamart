#!/bin/bash

APPDIR=/home/mhsu/comments-uncat
cd $APPDIR

#
#  Copy list of missing files from Oracle server
#
curl oracleprod2/cgi-bin/getuncat.sh > $APPDIR/uncat.txt

mailinglist='jfreeman@phaseonecg.com,ojas@nsgi-hq.com,scollins@phaseonecg.com,mbroderick@phaseonecg.com'

hostname=$(hostname)

#
#  Send final report
#
cat $APPDIR/uncat.txt | mail "$mailinglist" -s "Uncategorized document report from $hostname"
