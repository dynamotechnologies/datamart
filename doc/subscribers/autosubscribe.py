#!/usr/bin/python -u

#
#	Read new email subscribers from Datamart and
#	create subscriptions at GovDelivery
#
#	Requires /var/www/common/dmUtil and /var/www/config/dmUtil-conf
#

import xml.dom.minidom
import sys, os
fname = os.path.join(os.path.dirname(__file__), "/var/www/common/dmUtil")
sys.path.insert(0, fname)
import dmUtil
from urllib2 import HTTPError
import base64
import time
import ConfigParser

#
#  CONFIG SECTION
#  Set G_DRY_RUN to the desired setting
#
#  We now read configs from /var/www/config/dmUtil-conf
#
G_DRY_RUN = False
G_TOPICS_DOM = None
G_CARA_TO_PALS_IDS = {}

myConf = ConfigParser.SafeConfigParser()
myConf.optionxform = str
myConf.read('/var/www/config/dmUtil-conf')

configs = {}

configs.update(myConf.items("DATAMART"))
DATAMART_URL_PREFIX = configs['PREFIX']

configs.update(myConf.items("GOVDELIVERY"))
GOVDELIVERY_URL_PREFIX = configs['PREFIX']
#
#  END CONFIG SECTION
#

#
#  Fetch the XML resource at URL and return the document
#
def getDoc(URL):
  try:
    rsrc_xml = dmUtil.get(URL)
    return xml.dom.minidom.parseString(rsrc_xml)
  except HTTPError, e:
    raise e

#
#  Get DOM for list of new subscribers from Datamart
#
#  Only process Email subscribers for active topics
#  Limit number of subscribers to 10000 per run
#
#  (Need the whole DOM for modification purposes, it's weird but OK)
#
def DMGetNewSubscribersDOM():
  URL = DATAMART_URL_PREFIX + "/utils/subscribers/unsubscribed?activetopic=1&contactmethod=Email&rows=10000"
  return getDoc(URL)

#
#  Get CARA project linkage record from Datamart
#  (used to map CARA ID to PALS ID)
#
def DMGetCARAProject(caraid):
  URL = DATAMART_URL_PREFIX + "/caraprojects/" + caraid
  return getDoc(URL).documentElement

#
#  Convert CARA ID to PALS ID
#
def getPALSID(caraid):
  global G_CARA_TO_PALS_IDS
  palsid = None
  if (caraid in G_CARA_TO_PALS_IDS):
    palsid = G_CARA_TO_PALS_IDS[caraid]
  else:
    caraproj = DMGetCARAProject(caraid)
    palsidnode = caraproj.getElementsByTagName('palsid')[0]
    palsid = palsidnode.firstChild.data
    G_CARA_TO_PALS_IDS[caraid] = palsid
  return palsid

#
#  Update subscriber record in Datamart
#
def DMUpdateSubscriber(caraid, subscriberid, xml):
  URL = DATAMART_URL_PREFIX + "/caraprojects/" + caraid + "/mailinglist/subscribers/" + subscriberid
  try:
    dmUtil.put(URL, xml)
  except HTTPError, e:
    raise e

#
#  Get full list of topics from GovDelivery
#
def GDGetTopics():
  global G_TOPICS_DOM
  if (G_TOPICS_DOM is None):
    URL = GOVDELIVERY_URL_PREFIX + "/topics.xml"
    G_TOPICS_DOM = getDoc(URL).documentElement
  return G_TOPICS_DOM

#
#  Get list of topics for a subscriber from GovDelivery
#
def GDGetSubscriber(email):
  b64email = base64.b64encode(email)
  URL = GOVDELIVERY_URL_PREFIX + "/subscribers/" + b64email + ".xml"
  return getDoc(URL).documentElement

#
#  Get list of topics for a subscriber from GovDelivery
#
def GDGetSubscriberTopics(email):
  b64email = base64.b64encode(email)
  URL = GOVDELIVERY_URL_PREFIX + "/subscribers/" + b64email + "/topics.xml"
  return getDoc(URL).documentElement

#
#  Replace list of topics for a subscriber in GovDelivery
#  (duplicates not allowed)
#
def GDReplaceSubscriberTopics(email, xml):
  b64email = base64.b64encode(email)
  URL = GOVDELIVERY_URL_PREFIX + "/subscribers/" + b64email + "/topics.xml"
  try:
    dmUtil.put(URL, xml)
  except HTTPError, e:
    raise e

#
#  Create a subscriber record in GovDelivery
#  (no error if subscriber already exists)
#
#  At some point this function may need to be expanded to use the contents
#  of the Datamart subscriber record to create subscriber-question records
#  in GovDelivery
#
def GDCreateSubscriber(email):
  global G_subscribers

  subscriberXML = "					\
  <?xml version=\"1.0\" encoding=\"UTF-8\"?>		\
  <subscriber>						\
    <email>" + email + "</email>			\
    <send-notifications type=\"boolean\">false</send-notifications>	\
    <digest-for>0</digest-for>				\
  </subscriber>"
  try:
    URL = GOVDELIVERY_URL_PREFIX + "/subscribers.xml"
    dmUtil.post(URL, subscriberXML)
    print "User " + email + " created"
    G_subscribers += 1
  except HTTPError, e:
    if (e.code == 422):
      # GD returns a 422 if the data format is bad,
      # or if subscriber has already been created
      #
      # Make sure subscriber does not exist, before throwing an exception
      subscriberRec = GDGetSubscriber(email)
      print "User " + email + " already exists, no change"
    else:
      raise e

#
#  Check if topic already exists in GovDelivery
#  (True/False)
#
def GDTopicExists(topicid):
  topics = GDGetTopics()
  codes = topics.getElementsByTagName('code')
  found_topic = False
  for code in codes:
    if (code.firstChild.data == topicid):
      found_topic = True
      break
  return found_topic

#
#  Add the topic ID to the user's list of subscriptions in GovDelivery
#
def GDAddTopic(email, topicid):
  subscribertopics = GDGetSubscriberTopics(email)
  subscriberTopicsXML = "				\
  <?xml version=\"1.0\" encoding=\"UTF-8\"?>		\
  <subscriber>						\
    <topics type=\"array\">				\
  "
  subscriberTopicsXML += "				\
    <topic>						\
      <code>" + topicid + "</code>			\
    </topic>						\
  "
  for subscribertopicnode in subscribertopics.getElementsByTagName('to-param'):
    code = subscribertopicnode.firstChild.data
    if (code != topicid):
      subscriberTopicsXML += "				\
        <topic>						\
          <code>" + code + "</code>			\
        </topic>						\
      "
  subscriberTopicsXML += "				\
    </topics>						\
  </subscriber>						\
  "
  GDReplaceSubscriberTopics(email, subscriberTopicsXML)

#######
# MAIN
#######

print "=========="

G_errors = 0
G_wrongmethod = 0
G_subscriptions = 0
G_subscribers = 0

print "Creating new mailing list subscriptions"
if (G_DRY_RUN):
  print "Starting DRY RUN, no changes will be made"
else:
  print "Starting LIVE RUN"

timestamp = time.strftime("%Y-%m-%dT%H:%M:%S")
print "Start time: " + timestamp

dmUtil.initOpener()

dom = DMGetNewSubscribersDOM()
newSubscribers = dom.documentElement;

for subscriber in newSubscribers.childNodes:

  # Get subscriber attributes
  caraidnode = subscriber.getElementsByTagName('caraprojectid')[0]
  caraid = caraidnode.firstChild.data

  subscriberidnode = subscriber.getElementsByTagName('subscriberid')[0]
  subscriberid = subscriberidnode.firstChild.data

  contactmethodnode = subscriber.getElementsByTagName('contactmethod')[0]
  contactmethod = contactmethodnode.firstChild.data
  if (contactmethod != 'Email'):
    print "User caraid: " + caraid + ", subscriberid: " + subscriberid + " has contactmethod = " + contactmethod + ", skipping"
    G_wrongmethod += 1
    continue	# Next subscription

  emailnode = subscriber.getElementsByTagName('email')[0]
  email = None
  if (emailnode.firstChild != None):
    email = emailnode.firstChild.data
    print "Processing user " + email
  else:
    print "ERROR User caraid: " + caraid + ", subscriberid: " + subscriberid + " did not provide an email address, skipping"
    G_errors += 1
    continue	# Next subscription

  if (email.strip == ""):
    print "ERROR User caraid: " + caraid + ", subscriberid: " + subscriberid + " did not provide a valid email address, skipping"
    G_errors += 1
    continue	# Next subscription

  # Convert caraid to palsid
  palsid = getPALSID(caraid)

  # Make sure active GovDelivery topic exists
  topicid="NEPA_" + palsid + "_S"
  if (GDTopicExists(topicid) == False):
    print "ERROR Could not create subscription for user " + email + ", topic " + topicid + " is not active"
    G_errors += 1
    continue	# Next subscription

  # Create GovDelivery subscriber
  if (G_DRY_RUN):
    print "Dry run, skipping subscriber-create"
  else:
    try:
      GDCreateSubscriber(email)
    except HTTPError, e:
      print "ERROR Could not create subscriber " + email + ", check format"
      G_errors += 1
      continue	# Next subscription

  # Add subscriber topic
  if (G_DRY_RUN):
    print "Dry run, skipping subscriber topic update"
  else:
    GDAddTopic(email, topicid)
    print "Subscription created for subscriber " + subscriberid + ", CARA project " + caraid + ", topic " + topicid
    G_subscriptions += 1

  # Mark datamart record as subscribed
  timestamp = time.strftime("%Y-%m-%dT%H:%M:%S")
  newelement = dom.createElement("subscribeddate")
  txt = dom.createTextNode(timestamp)
  newelement.appendChild(txt)
  subscriber.appendChild(newelement)
  subscriber.setAttribute('xmlns', 'http://www.fs.fed.us/nepa/schema')

  if (G_DRY_RUN):
    print "Dry run, skipping Datamart subscriber update"
  else:
    # Create new DOM for <subscriber>, instead of <subscribers>
    subdom = xml.dom.minidom.parseString(subscriber.toxml("utf-8"))
    subxml = subdom.toxml("utf-8")
    DMUpdateSubscriber(caraid, subscriberid, subxml)

print "Finished"
if (G_errors > 0):
  print str(G_errors) + " subscriptions could not be processed"
else:
  print "No errors"
print "Added " + str(G_subscribers) + " new subscriber records"
print "Skipped " + str(G_wrongmethod) + " non-email subscribers records"
print "Added " + str(G_subscriptions) + " new topic subscriptions"

if (G_errors > 0):
  sys.exit(1)
else:
  sys.exit(0)
