package stateless;

import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.ProjectImplInfo.*;
import beanutil.DataMartException;

@Remote
public interface ProjectImplInfoManagerRemote {
    public void create(ProjectImplInfo resource) throws DataMartException;
    public ProjectImplInfo get(String ProjectId) throws DataMartException;
    public void update(ProjectImplInfo resource) throws DataMartException;
    public ProjectImplInfos getAllByProject(String projecttype, String projectid) throws DataMartException;
    public void delete(String filename) throws DataMartException;
    public Boolean ping();
}