package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.commentphase.*;


public class CommentPhaseClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public CommentPhaseClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.commentphase", "CommentPhase.xsd");
    }


    public void postCommentPhase(   Integer caraid,
                                    Integer id,
                                    String name,
                                    String startdate,
                                    String finishdate,
                                    Integer lettno,
                                    Integer lettuniq,
                                    Integer lettmst,
                                    Integer lettform,
                                    Integer lettformplus,
                                    Integer lettformdupe)
            throws IOException
    {
        Commentphase cphase = new Commentphase();
        cphase.setPhaseid(id);
        cphase.setName(name);
        cphase.setStartdate(dateStrToXML(startdate));
        cphase.setFinishdate(dateStrToXML(finishdate));
        cphase.setLettno(lettno);
        cphase.setLettuniq(lettuniq);
        cphase.setLettmst(lettmst);
        cphase.setLettform(lettform);
        cphase.setLettformplus(lettformplus);
        cphase.setLettformdupe(lettformdupe);
        postCommentPhase(caraid, cphase);
    }


    public void postCommentPhase(Integer caraid, Commentphase cphase)
            throws IOException
    {
        post(objectfactory.createCommentphase(cphase), "/caraprojects/" + caraid + "/comment/phases");
    }


    public Commentphase getCommentPhase(Integer caraid, Integer id)
            throws IOException
    {
        return get("/caraprojects/" + caraid + "/comment/phases/" + id, Commentphase.class);
    }


    public Commentphases getCommentPhaseList(Integer caraid)
            throws IOException
    {
        return get("/caraprojects/" + caraid + "/comment/phases", Commentphases.class);
    }


    public void putCommentPhase(   Integer caraid,
                                    Integer id,
                                    String name,
                                    String startdate,
                                    String finishdate,
                                    Integer lettno,
                                    Integer lettuniq,
                                    Integer lettmst,
                                    Integer lettform,
                                    Integer lettformplus,
                                    Integer lettformdupe)
            throws IOException
    {
        Commentphase cphase = new Commentphase();
        cphase.setPhaseid(id);
        cphase.setName(name);
        cphase.setStartdate(dateStrToXML(startdate));
        cphase.setFinishdate(dateStrToXML(finishdate));
        cphase.setLettno(lettno);
        cphase.setLettuniq(lettuniq);
        cphase.setLettmst(lettmst);
        cphase.setLettform(lettform);
        cphase.setLettformplus(lettformplus);
        cphase.setLettformdupe(lettformdupe);
        putCommentPhase(caraid, cphase);
    }


    public void putCommentPhase(Integer caraid, Commentphase cphase)
            throws IOException
    {
        put(objectfactory.createCommentphase(cphase),
                "/caraprojects/" + caraid + "/comment/phases/" + cphase.getPhaseid());
    }


    public void deleteCommentPhase(Integer caraid, Integer id)
            throws IOException
    {
        delete("/caraprojects/" + caraid + "/comment/phases/" + id);
    }
}