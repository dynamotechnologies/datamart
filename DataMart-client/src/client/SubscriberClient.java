package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.subscriber.*;


public class SubscriberClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public SubscriberClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.subscriber", "Subscriber.xsd");
    }


    public void postSubscriber( Integer caraid,
                                Integer id,
                                String email,
                                String lastname,
                                String firstname,
                                String title,
                                String org,
                                String orgtype,
                                String subscribertype,
                                String addstreet1,
                                String addstreet2,
                                String city,
                                String state,
                                String zip,
                                String prov,
                                String country,
                                String phone,
                                String contactmethod)
            throws IOException
    {
        Subscriber subscriber = new Subscriber();
        subscriber.setSubscriberid(id);
        subscriber.setEmail(email);
        subscriber.setLastname(lastname);
        subscriber.setFirstname(firstname);
        subscriber.setTitle(title);
        subscriber.setOrg(org);
        subscriber.setOrgtype(orgtype);
        subscriber.setSubscribertype(subscribertype);
        subscriber.setAddstreet1(addstreet1);
        subscriber.setAddstreet2(addstreet2);
        subscriber.setCity(city);
        subscriber.setState(state);
        subscriber.setZip(zip);
        subscriber.setProv(prov);
        subscriber.setCountry(country);
        subscriber.setPhone(phone);
        subscriber.setContactmethod(contactmethod);
        postSubscriber(caraid, subscriber);
    }


    public void postSubscriber(Integer caraid, Subscriber subscriber)
            throws IOException
    {
        post(objectfactory.createSubscriber(subscriber), "/caraprojects/" + caraid + "/mailinglist/subscribers");
    }


    public Subscriber getSubscriber(Integer caraid, Integer id)
            throws IOException
    {
        return get("/caraprojects/" + caraid + "/mailinglist/subscribers/" + id, Subscriber.class);
    }


    public Subscribers getSubscriberList(Integer caraid)
            throws IOException
    {
        return get("/caraprojects/" + caraid + "/mailinglist/subscribers", Subscribers.class);
    }


    public void putSubscriber( Integer caraid,
                                Integer id,
                                String email,
                                String lastname,
                                String firstname,
                                String title,
                                String org,
                                String orgtype,
                                String subscribertype,
                                String addstreet1,
                                String addstreet2,
                                String city,
                                String state,
                                String zip,
                                String prov,
                                String country,
                                String phone,
                                String contactmethod)
            throws IOException
    {
        Subscriber subscriber = new Subscriber();
        subscriber.setSubscriberid(id);
        subscriber.setEmail(email);
        subscriber.setLastname(lastname);
        subscriber.setFirstname(firstname);
        subscriber.setTitle(title);
        subscriber.setOrg(org);
        subscriber.setOrgtype(orgtype);
        subscriber.setSubscribertype(subscribertype);
        subscriber.setAddstreet1(addstreet1);
        subscriber.setAddstreet2(addstreet2);
        subscriber.setCity(city);
        subscriber.setState(state);
        subscriber.setZip(zip);
        subscriber.setProv(prov);
        subscriber.setCountry(country);
        subscriber.setPhone(phone);
        subscriber.setContactmethod(contactmethod);
        putSubscriber(caraid, subscriber);
    }


    public void putSubscriber(Integer caraid, Subscriber subscriber)
            throws IOException
    {
        put(objectfactory.createSubscriber(subscriber),
                "/caraprojects/" + caraid + "/mailinglist/subscribers/" + subscriber.getSubscriberid());
    }


    public void deleteSubscriber(Integer caraid, Integer id)
            throws IOException
    {
        delete("/caraprojects/" + caraid + "/mailinglist/subscribers/" + id);
    }
}