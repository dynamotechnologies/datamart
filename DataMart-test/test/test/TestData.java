package test;

public class TestData {

    public static final int CANT_GET_NOT_FOUND = 404;
    public static final int CANT_POST_ALREADY_EXISTS = 409;
    public static final int CANT_PUT_DONT_EXIST = 404;
    public static final int CANT_DELETE_DONT_EXIST = 404;
    public static final int BAD_PARENTS = 409;

    /*
     * REFERENCE TABLES
     *
     * The reference test data defined here is intentionally orthogonal to actual production
     *     data. This is important to avoid interference.
     *
     * TODO: Review all test data. Confirm that these values will never be used in real life.
     *       Revise the data such that it would be 1) inoffensive and 2) easily recognized as test data
     *       were it ever to find its way into the production database.
     */
    
    public static final class units {
        public static String code1 = "11012345";
        public static String code2 = "110111";
        public static String name1 = "Test National Forest That Does Not Exist";
        public static String name2 = "Test National Grassland That Does Not Exist";
        public static boolean extendeddetails1 = false;
        public static boolean extendeddetails2 = true;
        public static boolean projectmap1 = true;
        public static boolean projectmap2 = false;
        public static String address1_1 = "Original address line 1";
        public static String address1_2 = "New address line 2";
        public static String address2_1 = "Original address line 2";
        public static String address2_2 = "New address line 2";
        public static String city1 = "Original City";
        public static String city2 = "New City";
        public static String state1 = "Original State";
        public static String state2 = "New State";
        public static String zip1 = "Original Zip";
        public static String zip2 = "New Zip";
        public static String phone1 = "Original phone numbr"; // 20 chars
        public static String phone2 = "New phone number";
        public static String wwwlink1 = "Original web link";
        public static String wwwlink2 = "New web link";
        public static String commentemail1 = "Originalemail@mailinator.com";
        public static String commentemail2 = "Newemail@mailinator.com";
        public static String newspaper1 = "Original Newspaper Name";
        public static String newspaper2 = "New Newspaper Name";
        public static String newspaperurl1 = "originalnewspaperurl.test";
        public static String newspaperurl2 = "newnewspaperurl.test";
        public static String boundaryurl1 = "originalboundaryURL";
        public static String boundaryurl2 = "newboundaryURL";
        public static boolean active1 = true;
        public static boolean active2 = false;
        public static String spotlight1_1 = "12345";
        public static String spotlight1_2 = "54321";
        public static String spotlight2_1 = "22222";
        public static String spotlight2_2 = "33333";
        public static String spotlight3_1 = "33333";
        public static String spotlight3_2 = "44444";
        
        
    }

    public static final class cars {
        public static String make1 = "Dodge";
        public static String model1 = "Challenger";
        public static String model2 = "Ram";
    }
    public static final class statuses {
        public static String id1 = "9";
        public static String name1 = "Forgotten";
        public static String name2 = "Remembered";
    }

    public static final class states {
        public static String id1 = "TEST_ST";
        public static String name1 = "Test Old State Name";
        public static String name2 = "Test New State Name";
    }

    public static final class counties {
        public static String id1 = "TEST_CO";
        public static String name1 = "Test Old County Name";
        public static String name2 = "Test New County Name";
    }

    public static final class milestones {
        public static String id1 = "TEST_MS";
        public static String name1 = "Test Old Milestone Name";
        public static String name2 = "Test New Milestone Name";
    }

    public static final class projectmilestones {
        public static String seqnum1 = "87";
        public static String datestring1 = "01/01/2011";
        public static String status1 = "1";
        public static String history1 = "1";
    }

    public static final class specialauthorities {
        public static String id1 = "TEST_SA";
        public static String name1 = "Test Old Special Authority Name";
        public static String name2 = "Test New Special Authority Name";
        public static String description1 = "Test Old Special Authority Description";
        public static String description2 = "Test New Special Authority Description";
    }

    public static final class activities {
        public static String id1 = "TEST_AC";
        public static String name1 = "Test Old Activity Name";
        public static String name2 = "Test New Activity Name";
    }

    public static final class analysistypes {
        public static String id1 = "PBR";
        public static String name1 = "Pabst Blue Ribbon";
        public static String name2 = "Professional Bull Riders, Inc.";
    }

    // appealdismissalreasons
    public static final class adrs {
        public static String id1 = "15";
        public static String name1 = "Insufficient Cowbell";
        public static String name2 = "FS Withdrew Cowbell";
    }

    public static final class appealoutcomes {
        public static String id1 = "54";
        public static String name1 = "Reversed a little, but not that much.";
        public static String name2 = "Reversed a great deal.";
    }

    public static final class appealrules {
        public static String id1 = "test";
        public static String rule1 = "test rule name";
        public static String rule2 = "test rule replacement";
        // description field should have max length of 500 chars.
        public static String desc1 = "test rule long description goes here.   test rule long description goes here.   test rule long description goes here.   test rule long description goes here.   test rule long description goes here.   test rule long description goes here.   test rule long description goes here.   test rule long description goes here.   test rule long description goes here.   test rule long description goes here.   ";
    }

    public static final class appealstatuses {
        public static String id1 = "9";
        public static String name1 = "Appeal is being appealed";
        public static String name2 = "Review delayed pending ingress of strong coffee";
    }

    public static final class commentregulations {
        public static String id1 = "99";
        public static String name1 = "747 (Boeing)";
        public static String name2 = "MD80 (McDonnell Douglas)";
    }

    public static final class purposes {
        public static String id1 = "ZX";
        public static String name1 = "Cheap Entertainment";
        public static String name2 = "Executing terms of a lost wager";
    }

    public static final class applications {
        public static String id1 = "abcd";
        public static String name1 = "ABC-D";
        public static String name2 = "EIEIO";
    }

    public static final class projecttypes {
        public static String id1 = "asdf";
        public static String name1 = "ROAAAR!";
        public static String name2 = "RAAAAAAAR!!";
    }

    public static final class analysisapplicables {
        public static int id1 = 87;
        public static String analysistypeid1 = "EIS";
        public static String analysistypeid2 = "EA";
        public static int applicableregid1 = 1;
        public static int applicableregid2 = 2;
        public static String commentperioddays1 = "30";
        public static String commentperioddays2 = "60";
        public static String objectionperioddays1 = "90";
        public static String objectionperioddays2 = "120";
        public static boolean active1 = false;
        public static boolean active2 = true;
    }

    public static final class catexclusions {
        public static int id1 = 12345;
        public static String name1 = "catexcl1";
        public static String name2 = "catexcl2";
        public static String description1 = "Test reference categorical exclusion 1.";
        public static String description2 = "Test reference categorical exclusion 2.";
    }

    public static final class litstatuses {
        public static int id1 = 23456;
        public static String name1 = "Test reference litigation status 1.";
        public static String name2 = "Test reference litigation status 2.";
    }

    public static final class litoutcomes {
        public static int id1 = 34567;
        public static String name1 = "Test reference litigation outcome 1.";
        public static String name2 = "Test reference litigation outcome 2.";
    }


    /*
     * NON-REFERENCE ENTITIES
     *
     * Some entities are dependent on other entities.
     * All entities are dependent on reference data.
     *
     * It is necessary to pre-populate the database using the Reference Data Ingress tool prior to
     *    testing the non-reference entities.
     * 
     */

    public static final class projects {
        public static String id1 = "9999999";
        public static String type1 = "nepa";
        public static String name1 = "Project to replace all trees";
        public static String unitcode1 = "110305";
        public static String description1 = "This is a silly project";
        public static String lastupdate1 = "2010-01-21T04:31:59";
        public static String commentreg1 = "1";
        public static String wwwlink1 = "http://www.123abc.com";
        public static String wwwsummary1 = "http://www.123abc.com/summary";
        public static String wwwpub1 = "0";
        public static String analysistypeid1 = "CE";
        public static String statusid1 = "1";
        public static String contactname1 = "Asdf Asdfer";
        public static String contactphone1 = "202-123-4567";
        public static String contactemail1 = "asdf@qwerty.com";
        public static String purposeid1 = "VM";
        public static String purposeid2 = "HF";
        public static String purposeid3 = "WM";

        public static String locationdesc1 = "Description for Location 1";
        public static String locationdesc2 = "Description for Location 2";
        public static String locationlegaldesc1 = "Legal description for Location 1";
        public static String locationlegaldesc2 = "Legal description for Location 2";
        public static double latitude1 = 10.0;
        public static double latitude2 = -90.0;
        public static double longitude1 = -10.0;
        public static double longitude2 = 90.0;
        public static String expirationdate1 = "2012-01-21";    // UTC
        public static String expirationdate2 = "2022-12-28";    // YYYY-MM-DD
        public static int projectdocumentid1 = 99999998;
        public static int projectdocumentid2 = 99999999;
        public static Boolean sopapub = true;
        public static Boolean sopanew = true;
        public static String sopaheader = "11080312";
        public static Boolean esd = false;
        public static Boolean cenodecision = false;

        public static String wwwpub2 = "1";
        public static String name2 = "Project to replace all shrubbery";
        public static String contactname2 = "Qwerty McQwerterson";
        public static String purposeid4 = "MG";
        public static String purposeid5 = "LM";
        public static String purposeid6 = "LW";

        public static String badunitcode1 = "ABACADABA";
        public static String badanalysistypeid1 = "ZZ";
        public static String badstatusid1 = "89";
    }

    public static final class appeals {
        public static String id1 = "appeal1";
        public static String appellant1 = "appellant name";
        public static String outcomeid1 = "4";
        public static String dismissalid1 = "1";
        public static String responsedate1 = "2010-01-21";
        public static String ruleid1 = "215";
        public static String statusid1 = "1";

        public static String appellant2 = "appellant two";
        public static String outcomeid2 = "5";
        public static String adminunitcode1 = "110305";
    }

    public static final class objections {
        public static String id1 = "obj1";
        public static String objector1 = "objector name";
        public static String responsedate1 = "1980-07-17";
        
        public static String objector2 = "objector two";
        public static String responsedate2 = "1982-03-14";
    }

    public static final class projectdocuments {
        public static String docid1 = "doc1";
        public static String pubflag1 = "1";
        public static String wwwlink1 = "http://asdfasdfasdf.com";
        public static String docname1 = "this is the document name";
        public static String description1 = "this is the document description";
        public static String pdffilesize1 = "104 bytes";
        public static String sensitiveflag1 = "1";

        public static String docid2 = "doc2";
        public static String pubflag2 = "1";
        public static String wwwlink2 = "http://www.google.com";
        public static String docname2 = "Google";
        public static String description2 = "Google is a search engine";
        public static String pdffilesize2 = "874 bytes";
        public static String sensitiveflag2 = "0";

        public static String docid3 = "doc3";
        public static String pubflag3 = "0";
        public static String wwwlink3 = "http://www.boogle.com";
        public static String docname3 = "Sensitivedoc";
        public static String description3 = "Test sensitive document";
        public static String pdffilesize3 = "99 bytes";
        public static String sensitiveflag3 = "1";

        public static String docid4 = "doc4";
        public static String pubflag4 = "0";
        public static String wwwlink4 = "http://www.doogle.com";
        public static String docname4 = "Unpublisheddoc";
        public static String description4 = "Test unpublished document";
        public static String pdffilesize4 = "100 bytes";
        public static String sensitiveflag4 = "0";
    }

    public static final class containers {
        public static String projectid1 = "99912345";
        
        public static String docid1 = "docID1";
        public static String docid2 = "docID2";
        public static String docid3 = "docID3";
        public static String docid4 = "docID4";
        public static String docid5 = "docID5";

        public static int docorder1 = 1;
        public static int docorder2 = 2;
        public static int docorder3 = 3;
        public static int docorder4 = 4;
        public static int docorder5 = 5;

        public static String contlabel1 = "this is label 1";
        public static String contlabel2 = "this is label 2";
        public static String contlabel3 = "this is label 3";

        public static int contorder1 = 1;
        public static int contorder2 = 2;
        public static int contorder3 = 3;
        public static int contorder4 = 4;
        public static int contorder5 = 5;

        public static int contid1 = 100;
        public static int contid2 = 200;
        public static int contid3 = 300;
        public static int contid4 = 400;

        public static String badcontid1 = "foobar";
    }

    public static final class projectlocations {
        public static String description1 = "behind the barn, past the white fence and to the right of the cows";
        public static String legaldesc1 = "T15S, R10W, S29-30";
        public static String description2 = "past the barn, beyond the beige fence, and to the left of the cows (they moved).";
        public static String legaldesc2 = "T-1000, R01X, S87-40";
        public static double latitude1 = 58.301935;
        public static double longitude1 = -134.419740;
        public static String regionid1 = "1110";
        public static String regionid2 = "1111";
        public static String forestid1 = "110310";
        public static String forestid2 = "110312";
        public static String districtid1 = "11092002";
        public static String districtid2 = "11092003";
        public static String stateid1 = "AK";
        public static String stateid2 = "AZ";
        public static String countyid1 = "21161";
        public static String countyid2 = "21167";
    }

    public static final class users {
        public static String shortname1 = "jsmith";
        public static String firstname1 = "John";
        public static String lastname1 = "Smith";
        public static String phone1 = "(301) 555-1212";
        public static String title1 = "";
        public static String email1 = "john@mailinator.com";

        public static String phone2 = "(202) 555-1234 OR (202) 555-9876";
        public static String title2 = "Mr.";
        public static String email2 = "jsmith@thisisnotmyrealemail.com";
    }

    public static final class caraprojects {
        public static int id1 = 999998;
    }

    public static final class mlmprojects {
        public static String id1 = "999988";
    }

    public static final class subscribers {
        public static int id1 = 999987;
        public static String email1 = "jsmith@smithco.com";
        public static String lastname1 = "smith";
        public static String firstname1 = "jebodiah";
        public static String title1 = "Senor";
        public static String email2 = "asmith@smithco.com";
        public static String lastname2 = "smitherson";
        public static String firstname2 = "jeremiah";
        public static String title2 = "Senorita";
        public static String org1 = "asdf";
        public static String orgtype1 = "asdf";
        public static String subscribertype1 = "asdf";
        public static String addstreet1_1 = "1234 Main St.";
        public static String addstreet2_1 = "";
        public static String city1 = "St. Louis";
        public static String state1 = "UT";
        public static String zip1 = "12345=0090";
        public static String prov1 = "Alberta";
        public static String country1 = "EEUU";
        public static String phone1 = "123-456-7890 x9876";
        public static String contactmethod1 = "Email";
    }

    public static final class commentphases {
        public static int id1 = 999987;
        public static String name1 = "Testing Phase";
        public static String name2 = "QA Phase";
        public static int lettno1 = 1;
        public static int lettuniq1 = 2;
        public static int lettuniq2 = 7;
        public static int lettmst1 = 3;
        public static int lettmst2 = 8;
        public static int lettform1 = 4;
        public static int lettformplus1 = 5;
        public static int lettformdupe1 = 6;
        public static String startdate1 = "2012-01-26";    // UTC
        public static String finishdate1 = "2017-04-01";    // YYYY-MM-DD
    }

    public static final class roles {
        public static int id1 = 9991;
        public static int id2 = 9992;
        public static String name1 = "POOBAH";
        public static String name2 = "GAZOO";
        public static String description1 = "Lord High Exalted Poobah";
        public static String description2 = "The Great Gazoo";
    }

    public static final class unitroles {
        public static int id1 = 9999999;    // AG_ID
    }

    public static final class documentsensitivityrationales {
        public static int id1 = 99;
        public static int sensitivity1 = 99;
        public static String desc1 = "Sensitivity Test 1";
        public static int order1 = 99;

        public static int sensitivity2 = 100;
        public static String desc2 = "Sensitivity Test 2";
        public static int order2 = 100;
    }

    public static final class decisions {
        public static final int id1 = 999999;
        public static final String name1 = "Test National Forest System Land Management Planning: Final Test";
        public static final String constraint1 = "Constrained Due to Cloudy Weather";
        public static final String legalnoticedate1 = "2007-04-08";
        public static final Double areasize1 = 1.23;
        public static final String areaunits1 = "ac";
        public static final String dectype1 = "DM";
        public static final String decdate1 = "2013-09-16";
        public static final String name2 = "Test National Forest System Land Management Planning: Second Final Test";
        public static final String legalnoticedate2 = "2009-01-29";
        public static final String dectype2 = "ROD";
        public static final String decdate2 = "1999-09-13";
    }
    
    public static final class decisionmakers {
        public static final int id1 = 999999;
        public static final String title1 = "TEST Decision Maker Title 999999";
        public static final String name1 = "TEST Decision Maker Name 999999";
        public static final String signeddate1 = "2013-09-17";
        public static final int id2 = 999111;
        public static final String title2 = "TEST Decision Maker Title 999111";
        public static final String name2 = "TEST Decision Maker Name 999111";
        public static final String signeddate2 = "1999-09-13";
    }

    public static final class litigations {
        public static final int id1 = 99998;
        public static final String casename1 = "Citizens for Better Unit Testing v. USDA";
        public static final int status1 = 1;
        public static final int outcome1 = 4;
        public static final String closed1 = "2009-05-03";
        public static final String casename2 = "Foreign Nationals for Better Unit Testing v. USDA";
        public static final int status2 = 2;
        public static final String closed2 = "2010-09-13";

    }

    public static final class decisionappealrules {
        public static final String id1 = "215";
        public static final String id2 = "251";
    }

    public static final class nepacategoricalexclusions {
        public static final int id1 = 14;
        public static final int id2 = 35;
    }

    public static final class configs {
        public static final String key1 = "test_key1";
        public static final String value1 = "value1";
        public static final String key2 = "test_key2";
        public static final String value2 = "value2";
    }

    public static final class projectkmls {
        public static final String filename1 = "test_proj_1.kml";
        public static final String label1 = "testproj label 1";
        public static final String maptype1 = "testproj maptype 1";
        public static final String filename12 = "test_proj_2.kml";
        public static final String label2 = "testproj label 2";
        public static final String maptype2 = "testproj maptype 2";
    }

    public static final class unitkmls {
        public static final String filename1 = "test_unit_1.kml";
        public static final String label1 = "testunit label 1";
        public static final String maptype1 = "testunit maptype 1";
        public static final String filename12 = "test_unit_2.kml";
        public static final String label2 = "testunit label 2";
        public static final String maptype2 = "testunit maptype 2";
    }
    
    public static final class goals {
        public static final Integer id1 = 999991;
        public static final Integer id2 = 999992;
        public static final String description1 = "Test Resource Area 1";
        public static final String description2 = "Test Resource Area 2";
        public static final String testunitid = "99992345";
        public static final String testunitname = "Test National Forest That Does Not Exist";
    }
    
    public static final class resourceareas {
        public static final Integer id1 = 999991;
        public static final Integer id2 = 999992;
        public static final String description1 = "Test Resource Area 1";
        public static final String description2 = "Test Resource Area 2";
        public static final String testunitid = "99992345";
        public static final String testunitname = "Test National Forest That Does Not Exist";
    }
    
    public static final class projectgoals {
        public static final Integer order1 = 10;
        public static final Integer order2 = 20;
    }
    
    public static final class unitgoals {
        public static final Integer order1 = 10;
        public static final Integer order2 = 20;
    }
       
    public static final class projectresourceareas {
        public static final Integer order1 = 10;
        public static final Integer order2 = 20;
    }
    
    public static final class decisiontypes {
        public static final String id1 = "AA";
        public static final String id2 = "BB";
        public static final String desc1 = "Description for test type AA";
        public static final String desc2 = "Description for test type BB";
        public static final int palsid1 = 101;
        public static final int palsid2 = 102;
    }
}