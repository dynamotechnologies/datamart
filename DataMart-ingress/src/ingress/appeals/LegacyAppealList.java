package ingress.appeals;

import java.util.ArrayList;
import java.util.zip.DataFormatException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

/*
 * This class encapsulates xml ingressed from files validated with the following DTD:
 *
 * <!DOCTYPE appeals [
 * <!ELEMENT appeals (timestamp?, appeal+)>
 * <!ATTLIST appeals xmlns CDATA "http://www.fs.fed.us/pals/xmlns/appeals/">
 * <!ELEMENT timestamp (#PCDATA)>
 * <!ELEMENT appeal (id,projectid?,code,rule,appellant?,outcome,project,response)>
 * <!ELEMENT id (#PCDATA)>
 * <!ELEMENT projectid (#PCDATA)>
 * <!ELEMENT code (#PCDATA)>
 * <!ELEMENT rule (#PCDATA)>
 * <!ELEMENT appellant (#PCDATA)>
 * <!ELEMENT outcome (#PCDATA)>
 * <!ELEMENT project (name,adminunit)>
 * <!ELEMENT name (#PCDATA)>
 * <!ELEMENT adminunit (#PCDATA)>
 * <!ELEMENT response (date,documentid,filesize)>
 * <!ELEMENT date (#PCDATA)>
 * <!ELEMENT documentid (#PCDATA)>
 * <!ELEMENT filesize (#PCDATA)>
 * ]>
 *
 *
 * This class is responsible for encapsulating element `appeals` as specified above
 */
public class LegacyAppealList {
    private String timestamp = ""; // optional
    private ArrayList<LegacyAppeal> appeals = new ArrayList<LegacyAppeal>(); // one or many

    public LegacyAppealList(Document doc)
            throws DataFormatException
    {
            putTimeStamp(doc);
            putAppeals(doc);
    }

    private void putTimeStamp(Document d)
            throws DataFormatException
    {
        NodeList nodes = d.getElementsByTagName("timestamp");
        if (nodes.getLength()>0)
            timestamp = nodes.item(0).getTextContent();
    }

    private void putAppeals(Document d)
            throws DataFormatException
    {
        NodeList nodes = d.getElementsByTagName("appeal");
        for (int i=0; i<nodes.getLength(); i++)
        {
            try {
                appeals.add(new LegacyAppeal(nodes.item(i)));
            } catch (Exception ex) {
                System.out.println("Appeal ingress failed.\n"+ex.getMessage()+"\n\n"+nodes.item(i).getTextContent()+"\n\n\n");
            }
        }
        if (!(appeals.size()>0))
            throw new DataFormatException("Document does not contain any appeals.");
    }

    public String getTimeStamp()
    {
        return timestamp;
    }

    public ArrayList<LegacyAppeal> getAppeals()
    {
        return appeals;
    }
}