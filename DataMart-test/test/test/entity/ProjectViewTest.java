/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.entity;


import client.ProjectViewsClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.ProjectView.*;

/**
 *
 * @author gauri
 */

public class ProjectViewTest {
    private ProjectViewsClient client;
    

    @Before
    public void setUp()
            throws IOException
    {
        client = new ProjectViewsClient();
       
    }

    @Test
    public void testPost()
    {
        // This POST should fail, because it violates a constraint. The unit code is bad
        try {
           addProjectView();
            // If the above statement did not throw an exception then something has gone terribly wrong
            //Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.BAD_PARENTS)
                Assert.fail();
        } catch (Exception ex) {
            Logger.getLogger(ProjectViewTest.class.getName()).log(Level.SEVERE, null, ex);
             Assert.fail();
        }

        

        
    }

    @Test
    public void testGet()
    {
     

        ProjectViews views = null;
         
        try {
            views = client.getAllProjectViews("40090","oshroff");
        } catch (IOException ex) {
            Logger.getLogger(ProjectViewTest.class.getName()).log(Level.SEVERE, null, ex);
             Assert.fail();
        }
            
          
       

       
     

       
    }

    @Test
    public void testPut()
    {
        

        ProjectView view = null;
        try {
            modifyProjectView();
            view = client.getProjectViews("40090","128");
        } catch (Exception ex) {
            Logger.getLogger(ProjectViewTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

       
    }

   
    
 


    @Test
    public void testDelete()
    {
        

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            deleteProjectView();
          
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        } catch (Exception ex) {
            Logger.getLogger(ProjectViewTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
      
    }

 
    @After
    public void cleanFailed()
            throws Exception
    {
      
    }


    @BeforeClass
    public static void populateDependencies()
    {
        // No dependencies
    }

    @AfterClass
    public static void cleanupDependencies()
    {
        // No cleanup
    }
    
     public static void addProjectView() throws Exception{
	    	
	    	client.ProjectViewsClient  client = new client.ProjectViewsClient();
	    	ProjectView p = new ProjectView();
                    p.setName("test");    
                    p.setProjectId(40090);    
                    p.setUserId("oshroff");    
                    p.setCreateDate("2016-07-12");    
                   
	    	client.postProjectView(p);
	    }
    
	 public static void deleteProjectView() throws Exception{
	    	
	    	client.ProjectViewsClient  client = new client.ProjectViewsClient();
                client.deleteProjectView("40090","1");
         }   
         
          public static void modifyProjectView() throws Exception{
	       client.ProjectViewsClient  client = new client.ProjectViewsClient();
               ProjectView p = new ProjectView();
                    p.setName("test1"); 
                    p.setProjectId(40090);    
                    p.setUserId("oshroff");    
                    p.setCreateDate("2016-07-10");    
                    p.setId(1);
	    	client.putProjectView(p);
         } 
}
