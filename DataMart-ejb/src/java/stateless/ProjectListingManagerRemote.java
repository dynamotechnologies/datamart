package stateless;

import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.project.*;
import beanutil.DataMartException;
import us.fed.fs.www.nepa.schema.projectlisting.Projectlistings;


@Remote
public interface ProjectListingManagerRemote {

    public Boolean ping();
}