package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.appealrule.*;

import beanutil.*;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import java.util.Iterator;


@Stateless(mappedName="AppealRuleManagerRemote")
public class AppealRuleManager implements AppealRuleManagerRemote {

    @PersistenceContext
    private EntityManager em;


    public void create(Appealrule resource) throws DataMartException
    {
        validate(resource);
        entity.RefAppealRules entity = createEntity(resource);

        entity.RefAppealRules badstatus = em.find(entity.RefAppealRules.class, entity.getId());
        if (badstatus != null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        entity = em.merge(entity);
    }

    public String getRule(String id) throws DataMartException
    {
        entity.RefAppealRules entity = createEntity(id);
        if (entity == null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return entity.getRule();
    }

    public String getDescription(String id) throws DataMartException
    {
        entity.RefAppealRules entity = createEntity(id);
        if (entity == null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        return entity.getDescription();
    }

    public void update(Appealrule resource) throws DataMartException
    {
        validate(resource);
        entity.RefAppealRules entity = createEntity(resource);

        if (em.find(entity.RefAppealRules.class, entity.getId()) == null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        entity = em.merge(entity);
    }

    public void delete(String id) throws DataMartException
    {
        entity.RefAppealRules entity = createEntity(id);
        if (entity == null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));
    }

    public Appealrules getAll() throws DataMartException
    {
        // Retrieve all units from the database
        Query query = em.createQuery("SELECT s FROM RefAppealRules s");
        List statuslist =  query.getResultList();
        Iterator it = statuslist.iterator();

        // Loop over the database collection. Each row becomes a new unit in the response list
        Appealrules responselist = new Appealrules();
        while (it.hasNext()) {
            responselist.getAppealrule().add(createResource( (entity.RefAppealRules)it.next() ));
        }
        return responselist;
    }

    public Boolean ping()
    {
        return true;
    }

    /*
     *
     * This method does the mapping from jaxb AppealRule to entity.AppealRule and vice versa
     */
    private Appealrule createResource(entity.RefAppealRules entity)
            throws DataMartException
    {
        Appealrule resource = new Appealrule();
        resource.setId(entity.getId().toString());
        resource.setRule(entity.getRule());
        resource.setDescription(entity.getDescription());
        return resource;
    }

    private entity.RefAppealRules createEntity(Appealrule resource)
            throws DataMartException
    {
        entity.RefAppealRules entity = new entity.RefAppealRules();
        entity.setId(resource.getId());
        entity.setRule(resource.getRule());
        entity.setDescription(resource.getDescription());
        return entity;
    }

    // This version of overloaded method createEntity() accepts a String ID as a parameter, and returns the
    //   associated entity, or null if it doesn't exist
    private entity.RefAppealRules createEntity(String id)
            throws DataMartException
    {
        return em.find(entity.RefAppealRules.class, id);
    }

    private void validate(Appealrule resource)
            throws DataMartException
    {
        DataMartBeanUtil.confirmNotNull(resource.getRule(), "Rule");
    }
}
