package client;

import java.io.IOException;
import us.fed.fs.www.nepa.schema.role.*;


public class RoleClient extends AbstractClient
{
    ObjectFactory objectfactory = new ObjectFactory();

    public RoleClient()
            throws IOException
    {
        init("us.fed.fs.www.nepa.schema.role", "Role.xsd");
    }


    public void postRole(int id, String name, String description)
            throws IOException
    {
        Role role = new Role();
        role.setId(id);
        role.setName(name);
        role.setDescription(description);
        postRole(role);
    }


    public void postRole(Role role)
            throws IOException
    {
        post(objectfactory.createRole(role), "/ref/roles");
    }


    public Role getRole(int id)
            throws IOException
    {
        return get("/ref/roles/" + id, Role.class);
    }


    public Roles getRoleList()
            throws IOException
    {
        return get("/ref/roles", Roles.class);
    }


    public void putRole(int id, String name, String description)
            throws IOException
    {
        Role role = new Role();
        role.setId(id);
        role.setName(name);
        role.setDescription(description);
        putRole(role);
    }


    public void putRole(Role role)
            throws IOException
    {
        put(objectfactory.createRole(role), "/ref/roles/" + role.getId());
    }


    public void deleteRole(int id)
            throws IOException
    {
        delete("/ref/roles/" + id);
    }
}