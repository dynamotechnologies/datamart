package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "RefAppealRules")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RefAppealRules.findAll", query = "SELECT r FROM RefAppealRules r"),
    @NamedQuery(name = "RefAppealRules.findById", query = "SELECT r FROM RefAppealRules r WHERE r.id = :id"),
    @NamedQuery(name = "RefAppealRules.findByRule", query = "SELECT r FROM RefAppealRules r WHERE r.rule = :rule"),
    @NamedQuery(name = "RefAppealRules.findByDescription", query = "SELECT r FROM RefAppealRules r WHERE r.description = :description")})
public class RefAppealRules implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private String id;
    @Basic(optional = false)
    @Column(name = "Rule")
    private String rule;
    @Basic(optional = false)
    @Column(name = "Description")
    private String description;
    @OneToMany(mappedBy = "refAppealRules")
    private Collection<DecisionAppealRules> decisionAppealRulesCollection;
    @OneToMany(mappedBy = "refAppealRules")
    private Collection<Appeals> appealsCollection;

    public RefAppealRules() {
    }

    public RefAppealRules(String id) {
        this.id = id;
    }

    public RefAppealRules(String id, String rule, String description) {
        this.id = id;
        this.rule = rule;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRule() {
        return rule;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public Collection<DecisionAppealRules> getDecisionAppealRulesCollection() {
        return decisionAppealRulesCollection;
    }

    public void setDecisionAppealRulesCollection(Collection<DecisionAppealRules> decisionAppealRulesCollection) {
        this.decisionAppealRulesCollection = decisionAppealRulesCollection;
    }

    @XmlTransient
    public Collection<Appeals> getAppealsCollection() {
        return appealsCollection;
    }

    public void setAppealsCollection(Collection<Appeals> appealsCollection) {
        this.appealsCollection = appealsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RefAppealRules)) {
            return false;
        }
        RefAppealRules other = (RefAppealRules) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RefAppealRules[ id=" + id + " ]";
    }

}
