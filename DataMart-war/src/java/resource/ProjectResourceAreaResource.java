package resource;

import beanutil.DataMartException;
import lombok.extern.slf4j.Slf4j;
import stateless.ProjectResourceAreaManagerRemote;
import us.fed.fs.www.nepa.schema.projectresourcearea.ObjectFactory;
import us.fed.fs.www.nepa.schema.projectresourcearea.Projectresourcearea;
import us.fed.fs.www.nepa.schema.projectresourcearea.Projectresourceareas;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;

@Path ("/projects/{projtype}/{projid}/resourceareas")
@Consumes(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Produces(DataMartUtil.PRIMARY_MEDIA_TYPE)
@Slf4j
public class ProjectResourceAreaResource extends AbstractResource
{
    private ProjectResourceAreaManagerRemote beanmanager;
    @Context
    private ServletContext servletcontext;
    @Context
    private HttpHeaders headers;

    public ProjectResourceAreaResource()
            throws ServletException
    {
        // MY_PROPS and init() are defined in AbstractResource.java
        MY_PROPS.setProperty("schema", "ProjectResourceArea.xsd");
        MY_PROPS.setProperty("package", "us.fed.fs.www.nepa.schema.projectresourcearea");
        MY_PROPS.setProperty("bean", "ProjectResourceAreaManagerRemote");
        try {
            beanmanager = (ProjectResourceAreaManagerRemote) init(ProjectResourceAreaManagerRemote.class);
        } catch (IOException ex) {
            log.error(ex.toString());
            throw new ServletException("ProjectResourceAreaResource failed to start. Could not start the bean manager.");
        }
        if (!beanmanager.ping())
            throw new ServletException("ProjectResourceAreaResource failed to start. Could not ping the bean manager!");
    }


    @POST
    public Response post(@PathParam("projtype") String projtype, @PathParam("projid") String projid, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        URI uri = null;

        try {
            Projectresourcearea area = (Projectresourcearea)unmarshal(inputxml, Projectresourcearea.class);
            uri = new URI(uriInfo.getAbsolutePath().toString()+"/"+area.getResourceareaid());

            if (projtype.compareTo(area.getProjecttype()) != 0)
                complainLoudly(new Exception("Project type in uri must match type in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            if (projid.compareTo(area.getProjectid()) != 0)
                complainLoudly(new Exception("Project ID in uri must match ID in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the insert.
            beanmanager.create(area);
        } catch (URISyntaxException ex) {
            complainLoudly(ex, Response.Status.INTERNAL_SERVER_ERROR);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the newly created entity
        return created(uri);
    }


    @GET
    @Path("/{areaid}")
    public Response get(@PathParam("projtype") String projtype, @PathParam("projid") String projid, @PathParam("areaid") Integer areaid, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            Projectresourcearea area = beanmanager.get(projtype, projid, areaid);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Projectresourcearea> jaxbelement = objectfactory.createProjectresourcearea(area);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }


    /*
     *  Get list of all project resource areas under this project
     */
    @GET
    public Response getAllByProject(@PathParam("projtype") String projtype, @PathParam("projid") String projid, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        StringWriter sw = new StringWriter();
        try {
            // Get the name of the specified unit
            Projectresourceareas areas = beanmanager.getAllByProject(projtype, projid);

            // Get the query results in a JAXBElement object, marshal and return
            ObjectFactory objectfactory = new ObjectFactory();
            JAXBElement<Projectresourceareas> jaxbelement = objectfactory.createProjectresourceareas(areas);
            sw = marshal(jaxbelement);
        } catch (DataMartException ex) {
            handleException(ex);
        } catch (JAXBException ex2) {
            complainLoudly(ex2, Response.Status.INTERNAL_SERVER_ERROR);
        }
        return ok(sw);
    }

    @PUT
    @Path("/{areaid}")
    public Response put(@PathParam("projtype") String projtype, @PathParam("projid") String projid, @PathParam("areaid") Integer areaid, InputStream inputxml, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        Projectresourcearea area = null;
        try {
            area = (Projectresourcearea) unmarshal(inputxml, Projectresourcearea.class);

            if (projtype.compareTo(area.getProjecttype()) != 0)
                complainLoudly(new Exception("Project type in uri must match type in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            if (projid.compareTo(area.getProjectid()) != 0)
                complainLoudly(new Exception("Project ID in uri must match ID in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            if (areaid != area.getResourceareaid())
                complainLoudly(new Exception("Resource area ID in uri must match ID in xml."), Response.Status.UNSUPPORTED_MEDIA_TYPE);

            // All clear. Now do the update.
            beanmanager.update(area);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        // Return a link to the updated entity
        return ok("<location>"+uriInfo.getAbsolutePath().toString()+"</location>");
    }


    @DELETE
    @Path("/{areaid}")
    public Response delete(@PathParam("projtype") String projtype, @PathParam("projid") String projid, @PathParam("areaid") Integer areaid, @Context UriInfo uriInfo)
            throws WebApplicationException
    {
        try {
            beanmanager.delete(projtype, projid, areaid);
        } catch (DataMartException ex) {
            handleException(ex);
        }
        return deleted();
    }
}
