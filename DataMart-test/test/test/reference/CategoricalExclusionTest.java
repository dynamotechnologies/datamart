package test.reference;

import client.CategoricalExclusionClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.categoricalexclusion.*;


public class CategoricalExclusionTest {
    private CategoricalExclusionClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new CategoricalExclusionClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postCatExclusion(TestData.catexclusions.id1, TestData.catexclusions.name2, TestData.catexclusions.description2);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Categoricalexclusion ce = null;
        Categoricalexclusions celist = null;
        try {
            ce = client.getCatExclusion(TestData.catexclusions.id1);
            celist = client.getCatExclusionList();
        } catch (IOException ex) {
            Logger.getLogger(CategoricalExclusionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.catexclusions.id1, ce.getId());
        Assert.assertEquals(TestData.catexclusions.name1, ce.getName());
        Assert.assertTrue(celist.getCategoricalexclusion().get(0).getId()>0);
        Assert.assertTrue(celist.getCategoricalexclusion().get(0).getName().length()>0);

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        Categoricalexclusion ce = null;
        try {
            client.putCatExclusion(TestData.catexclusions.id1, TestData.catexclusions.name2, TestData.catexclusions.description2);
            ce = client.getCatExclusion(TestData.catexclusions.id1);
        } catch (Exception ex) {
            Logger.getLogger(CategoricalExclusionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.catexclusions.name2, ce.getName());

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getCatExclusion(TestData.catexclusions.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteCatExclusion(TestData.catexclusions.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }

    private void post()
    {
        try {
            client.postCatExclusion(TestData.catexclusions.id1, TestData.catexclusions.name1, TestData.catexclusions.description1);
        } catch (IOException ex) {
            Logger.getLogger(CategoricalExclusionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteCatExclusion(TestData.catexclusions.id1);
        } catch (IOException ex) {
            Logger.getLogger(CategoricalExclusionTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteCatExclusion(TestData.catexclusions.id1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }
}