/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author gauri
 */
@Entity
@Table(name = "ProjectAddInfoDocs")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProjectAddInfoDocs.findAll", query = "SELECT p FROM ProjectAddInfoDocs p"),
    @NamedQuery(name = "ProjectAddInfoDocs.findByDocumentOrder", query = "SELECT p FROM ProjectAddInfoDocs p WHERE p.documentOrder = :documentOrder"),
    @NamedQuery(name = "ProjectAddInfoDocs.findByAddInfoDocId", query = "SELECT p FROM ProjectAddInfoDocs p WHERE p.addInfoDocId = :addInfoDocId")})
public class ProjectAddInfoDocs implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "documentOrder")
    private Integer documentOrder;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "addInfoDocId")
    private Integer addInfoDocId;
    @JoinColumn(name = "addInfoId", referencedColumnName = "AddInfoId")
    @ManyToOne(optional = false)
    private ProjectAdditionalInfo addInfoId;
    @JoinColumn(name = "documentId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private ProjectDocuments documentId;

    public ProjectAddInfoDocs() {
    }

    public ProjectAddInfoDocs(Integer addInfoDocId) {
        this.addInfoDocId = addInfoDocId;
    }

    public Integer getDocumentOrder() {
        return documentOrder;
    }

    public void setDocumentOrder(Integer documentOrder) {
        this.documentOrder = documentOrder;
    }

    public Integer getAddInfoDocId() {
        return addInfoDocId;
    }

    public void setAddInfoDocId(Integer addInfoDocId) {
        this.addInfoDocId = addInfoDocId;
    }

    public ProjectAdditionalInfo getAddInfoId() {
        return addInfoId;
    }

    public void setAddInfoId(ProjectAdditionalInfo addInfoId) {
        this.addInfoId = addInfoId;
    }

    public ProjectDocuments getDocumentId() {
        return documentId;
    }

    public void setDocumentId(ProjectDocuments documentId) {
        this.documentId = documentId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (addInfoDocId != null ? addInfoDocId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProjectAddInfoDocs)) {
            return false;
        }
        ProjectAddInfoDocs other = (ProjectAddInfoDocs) object;
        if ((this.addInfoDocId == null && other.addInfoDocId != null) || (this.addInfoDocId != null && !this.addInfoDocId.equals(other.addInfoDocId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ProjectAddInfoDocs[ addInfoDocId=" + addInfoDocId + " ]";
    }
    
}
