package test.relationship;

import client.ProjectClient;
import client.ProjectResourceAreaClient;
import client.ResourceAreaClient;
import client.UnitClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import org.junit.*;
import test.TestData;
import us.fed.fs.www.nepa.schema.project.Project;
import us.fed.fs.www.nepa.schema.projectresourcearea.Projectresourcearea;
import us.fed.fs.www.nepa.schema.projectresourcearea.Projectresourceareas;


public class ProjectResourceAreaTest {
    private ProjectResourceAreaClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new ProjectResourceAreaClient();
    }

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postProjectResourceArea(
                    TestData.projects.type1,
                    TestData.projects.id1,
                    TestData.resourceareas.id1,
                    TestData.projectresourceareas.order1);

            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)
                Assert.fail();
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Projectresourcearea projectresourcearea = null;
        try {
            projectresourcearea = client.getProjectResourceArea(TestData.projects.type1, TestData.projects.id1, TestData.resourceareas.id1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectResourceAreaTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        Assert.assertTrue(TestData.resourceareas.id1 == projectresourcearea.getResourceareaid());
        Assert.assertTrue(TestData.projectresourceareas.order1 == projectresourcearea.getOrder());

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        try {
            // Update project resource area, change order
            client.putProjectResourceArea(
                    TestData.projects.type1,
                    TestData.projects.id1,
                    TestData.resourceareas.id1,
                    TestData.projectresourceareas.order2
                    );
        } catch (IOException ex) {
            Logger.getLogger(ProjectResourceAreaTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }

        Projectresourcearea projectresourcearea = null;
        try {
            projectresourcearea = client.getProjectResourceArea(TestData.projects.type1, TestData.projects.id1, TestData.resourceareas.id1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectResourceAreaTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        
        Assert.assertTrue(TestData.resourceareas.id1 == projectresourcearea.getResourceareaid());
        Assert.assertTrue(TestData.projectresourceareas.order2 == projectresourcearea.getOrder());

        delete();
    }

    @Test
    public void testGetAll()
    {
        post();
        
        Projectresourceareas pras = null;
        try {
            pras = client.getProjectResourceAreasByProject(TestData.projects.type1, TestData.projects.id1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectResourceAreaTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        
        Assert.assertTrue(pras.getProjectresourcearea().size() == 1);
        
        Projectresourcearea pra = (Projectresourcearea)pras.getProjectresourcearea().get(0);
        Assert.assertTrue(TestData.resourceareas.id1 == pra.getResourceareaid());
        Assert.assertEquals(TestData.projectresourceareas.order1, pra.getOrder());

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getProjectResourceArea(TestData.projects.type1, TestData.projects.id1, TestData.resourceareas.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteProjectResourceArea(TestData.projects.type1, TestData.projects.id1, TestData.resourceareas.id1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }
    
    private void post()
    {
        try {
            client.postProjectResourceArea(
                    TestData.projects.type1,
                    TestData.projects.id1,
                    TestData.resourceareas.id1,
                    TestData.projectresourceareas.order1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectResourceAreaTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteProjectResourceArea(TestData.projects.type1, TestData.projects.id1, TestData.resourceareas.id1);
        } catch (IOException ex) {
            Logger.getLogger(ProjectResourceAreaTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    @After
    public void cleanFailed()
            throws Exception
    { }

    @BeforeClass
    public static void populateDependencies() throws IOException
    {
        // Create test unit
        UnitClient unitclient = new UnitClient();
        unitclient.postUnit(TestData.resourceareas.testunitid, TestData.resourceareas.testunitname);
        
        // Create test project
        ArrayList<String> purposeids = new ArrayList<String>();
        ProjectClient projectclient = new ProjectClient();
        projectclient.postProject(
                    TestData.projects.id1,
                    TestData.projects.type1,
                    TestData.projects.name1,
                    TestData.resourceareas.testunitid,
                    TestData.projects.description1,
                    TestData.projects.lastupdate1,
                    TestData.projects.commentreg1,
                    TestData.projects.wwwlink1,
                    TestData.projects.wwwsummary1,
                    TestData.projects.wwwpub1,
                    TestData.projects.analysistypeid1,
                    TestData.projects.statusid1,
                    TestData.projects.contactname1,
                    TestData.projects.contactphone1,
                    TestData.projects.contactemail1,
                    purposeids,
                    TestData.projects.expirationdate1,
                    TestData.projects.projectdocumentid1,
                    TestData.projects.sopapub,
                    TestData.projects.sopanew,
                    TestData.projects.sopaheader,
                    TestData.projects.esd,
                    TestData.projects.cenodecision);
         
        // Create test resource area
        ResourceAreaClient raclient = new ResourceAreaClient();
        raclient.postResourceArea(TestData.resourceareas.id1, TestData.resourceareas.testunitid, TestData.resourceareas.description1);
    }

    @AfterClass
    public static void cleanupDependencies() throws IOException
    {
        ResourceAreaClient raclient = new ResourceAreaClient();
        try {
            // Delete test resource area
            raclient.deleteResourceArea(TestData.resourceareas.id1);
        } catch (IOException e) {
            if (raclient.getStatus() != 404) {
                throw e;
            }
        }
        
        ProjectClient projectclient = new ProjectClient();
        try {
            // Delete test project
            projectclient.deleteProject(TestData.projects.type1, TestData.projects.id1);
        } catch (IOException e) {
            if (projectclient.getStatus() != 404) {
                throw e;
            }
        }
        
        UnitClient unitclient = new UnitClient();
        try {
            // Delete test unit
            unitclient.deleteUnit(TestData.resourceareas.testunitid);
        } catch (IOException e) {
            if (unitclient.getStatus() != 404) {
                throw e;
            }
        }
    }
}
