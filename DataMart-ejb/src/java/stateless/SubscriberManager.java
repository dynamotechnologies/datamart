package stateless;

// The public-facing (resource) classes are imported and referenced by classname. The entity
//   classes, being more regal, are to be addressed only by their full path.
import us.fed.fs.www.nepa.schema.subscriber.*;

import javax.ejb.Stateless;
import beanutil.*;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.HashMap;
import java.lang.reflect.Field;


@Stateless(mappedName="SubscriberManagerRemote")
public class SubscriberManager implements SubscriberManagerRemote {

    @PersistenceContext
    private EntityManager em;

    private int MAX_EMAIL_LEN          = 250;
    private int MAX_LASTNAME_LEN       = 100;
    private int MAX_FIRSTNAME_LEN      = 100;
    private int MAX_TITLE_LEN          = 50;
    private int MAX_ORG_LEN            = 100;
    private int MAX_ORGTYPE_LEN        = 100;
    private int MAX_SUBSCRIBERTYPE_LEN = 25;
    private int MAX_ADDRESSSTREET1_LEN = 200;
    private int MAX_ADDRESSSTREET2_LEN = 200;
    private int MAX_CITY_LEN           = 100;
    private int MAX_STATE_LEN          = 2;
    private int MAX_ZIP_LEN            = 10;
    private int MAX_PROVINCE_LEN       = 100; 
    private int MAX_COUNTRY_LEN        = 100;
    private int MAX_PHONE_LEN          = 20;
    private int MAX_CONTACTMETHOD_LEN  = 5;           
     
    @Override
    public void create(Subscriber resource)
             throws DataMartException
    {
        resource = validate(resource);
        
        entity.MailingListSubscribers entity = createEntity(resource);

        entity.MailingListSubscribers badentity = retrieveEntity(resource.getCaraprojectid(), resource.getSubscriberid());
        if (badentity!=null)
            throw new DataMartException(DataMartException.ALREADY_EXISTS);

        em.persist(entity);
    }

    // Get all subscribers attached to a specific CARA project ID
    @Override
    public Subscribers getAll(int caraProjectId)
             throws DataMartException
    {
        // Retrieve all entities from the database
        entity.Projects project = DataMartBeanUtil.getCaraProject(em, caraProjectId);
        if (project == null) {
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                "CARA Project with id '" + caraProjectId + "' does not exist.");
        }
        Query query = em.createQuery("SELECT s FROM MailingListSubscribers s WHERE s.projects = :projects");
        query.setParameter("projects", project);
        List entitylist = query.getResultList();
        if (entitylist==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                "No Subscribers found for project '" + caraProjectId + "'.");
        
        Subscribers responselist = new Subscribers();
        Iterator it = entitylist.iterator();
        while (it.hasNext()) {
            responselist.getSubscriber().add(createResource(caraProjectId, (entity.MailingListSubscribers) it.next()));
        }
        return responselist;
    }
    
    /*
     * Retrieve a list of all new subscriptions (subscribedDate is null)
     * 
     * The Subscriber resource has a CARA ID because all subscribers are currently
     * created by CARA, for CARA projects.  However the subscriber record itself
     * is tied to a Project rather than a CARAProject.  This was because, at the
     * time of initial design, it was expected that Subscribers could be attached
     * to projects of any type.
     * 
     * If Subscribers end up being phased out because they were only a temporary
     * solution to the problem of having CARA create GovDelivery subscriptions,
     * then this code will go away and the problem is moot.  However if subscribers
     * wind up being repurposed for an actual non-CARA application, we will need
     * to revisit all of this code, which assumes CARA IDs, and create methods
     * that work without them.
     * 
     * Parameters:
     * contactmethod - if not null, all subscribers must match this contact method
     *                 (usually either Email or Mail)
     * activetopicflag - if true, return only new subscribers for active mailing lists
     *                   if false, return only new subscribers for inactive mailing lists
     *                   if null, do not filter on mailing list status
     *                   (you can subscribe to an inactive mailing list for some reason)
     * start - if not null, start returning the Nth row
     * rows - if not null, return N rows
     */
    @Override
    public Subscribers getAllUnsubscribed(String contactmethod, Boolean activetopicflag, Integer start, Integer rows)
             throws DataMartException
    {
        HashMap<Integer, Integer> caraIDMap = new HashMap<Integer, Integer>();

        // Construct map from (internal) Project IDs to CARA IDs
        Query mapquery = em.createQuery("SELECT DISTINCT c from CARAProjects c, MailingListSubscribers s WHERE c.projects = s.projects AND s.subscribedDate IS NULL");

        @SuppressWarnings("unchecked")
        List<entity.CARAProjects> maplist = mapquery.getResultList();

        for (entity.CARAProjects cproj : maplist) {
            caraIDMap.put(cproj.getProjects().getId(), cproj.getId());
        }

        Subscribers responseList = new Subscribers();

        // Retrieve all unsubscribed subscribers from the database
        String sql = "SELECT s FROM MailingListSubscribers s WHERE s.subscribedDate IS NULL";
        if (activetopicflag != null) {
            if (activetopicflag) {
                sql = "SELECT s FROM MailingListSubscribers s, MLMProjects p WHERE s.projects = p.projects AND s.subscribedDate IS NULL";
            } else {
                sql = "SELECT s FROM MailingListSubscribers s WHERE s.projects.id NOT IN (SELECT p.projects.id from MLMProjects p) AND s.subscribedDate IS NULL";
            }
        }
        
        if (contactmethod != null) {
            sql += " AND s.contactMethod = :contactmethod";
        }

        sql += " ORDER BY s.subscriberId";

        // use sql to create Query object
        Query query = em.createQuery(sql);
        
        // Set Query params based on method params
        if (contactmethod != null)
            query.setParameter("contactmethod", contactmethod);

        if (rows != null) {
            query.setMaxResults(rows);
        }

        if (start != null) {
            query.setFirstResult(start);
        }

        @SuppressWarnings("unchecked")
        List<entity.MailingListSubscribers> entitylist = query.getResultList();

        for (entity.MailingListSubscribers subscriber : entitylist) {
            Integer key = subscriber.getProjects().getId();
            // If the map doesn't contain the key, then this is probably a subscriber to
            // a non-NEPA project, and doesn't have a CARAProject ID
            // This scenario is currently not worth worrying about, although it may change
            // in the future
            if (caraIDMap.containsKey(key)) {
                Integer caraProjectId = caraIDMap.get(key);
                responseList.getSubscriber().add(createResource(caraProjectId, subscriber));
            }
        }
        return responseList;
    }

    @Override
    public Subscriber get(int caraProjectId, Integer id)
            throws DataMartException
    {
        entity.MailingListSubscribers entity = retrieveEntity(caraProjectId, id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                "Mailing list subscriber with id '" + id + " does not exist.");

        return createResource(caraProjectId, entity);
    }

    @Override
    public void update(Subscriber resource)
             throws DataMartException
    {
        resource = validate(resource);
        
        entity.MailingListSubscribers entity = createEntity(resource);

        entity.MailingListSubscribers oldentity = retrieveEntity(resource.getCaraprojectid(), resource.getSubscriberid());
        if (oldentity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);

        // set the internal db identifier for the row so jpa knows this is an update and not an insert
        entity.setId(oldentity.getId());

        em.merge(entity);
    }

    @Override
    public void delete(int caraProjectId, Integer id)
             throws DataMartException
    {
        entity.MailingListSubscribers entity = retrieveEntity(caraProjectId, id);
        if (entity==null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND);
        em.remove(em.merge(entity));
    }


    @Override
    public Boolean ping()
    {
        return true;
    }


    private void validateStringField(String name, String field, Integer maxlen) throws DataMartException {
        // subscriber string fields are all required but check anyway
        if (field != null) {
            if (field.length() > maxlen) {
                throw new DataMartException(DataMartException.CONSTRAINT_VIOLATION, name + " must be shorter than " + maxlen);
            }
        }
    }


    /*
     * Validate fields for an incoming subscription record
     * Trim whitespace from incoming string fields
     * 
     * The JAXB validator ensures that required fields are present and that they
     * are of the correct type, but it doesn't validate field lengths.  Also,
     * there doesn't appear to be a way to capture a SQL error for a field width.
     */
    private Subscriber validate(Subscriber subscriber) throws DataMartException {
        // trim whitespace from fields
        subscriber.setEmail(subscriber.getEmail().trim());
        subscriber.setLastname(subscriber.getLastname().trim());
        subscriber.setFirstname(subscriber.getFirstname().trim());
        subscriber.setTitle(subscriber.getTitle().trim());
        subscriber.setOrg(subscriber.getOrg().trim());
        subscriber.setOrgtype(subscriber.getOrgtype().trim());
        subscriber.setSubscribertype(subscriber.getSubscribertype().trim());
        subscriber.setAddstreet1(subscriber.getAddstreet1().trim());
        subscriber.setAddstreet2(subscriber.getAddstreet2().trim());
        subscriber.setCity(subscriber.getCity().trim());
        subscriber.setState(subscriber.getState().trim());
        subscriber.setZip(subscriber.getZip().trim());
        subscriber.setProv(subscriber.getProv().trim());
        subscriber.setCountry(subscriber.getCountry().trim());
        subscriber.setPhone(subscriber.getPhone().trim());
        subscriber.setContactmethod(subscriber.getContactmethod().trim());
        
        validateStringField("email", subscriber.getEmail(), MAX_EMAIL_LEN);
        validateStringField("lastname", subscriber.getLastname(), MAX_LASTNAME_LEN);
        validateStringField("firstname", subscriber.getFirstname(), MAX_FIRSTNAME_LEN);
        validateStringField("title", subscriber.getTitle(), MAX_TITLE_LEN);
        validateStringField("org", subscriber.getOrg(), MAX_ORG_LEN);
        validateStringField("orgtype", subscriber.getOrgtype(), MAX_ORGTYPE_LEN);
        validateStringField("subscribertype", subscriber.getSubscribertype(), MAX_SUBSCRIBERTYPE_LEN);
        validateStringField("address1", subscriber.getAddstreet1(), MAX_ADDRESSSTREET1_LEN);
        validateStringField("address2", subscriber.getAddstreet2(), MAX_ADDRESSSTREET2_LEN);
        validateStringField("city", subscriber.getCity(), MAX_CITY_LEN);
        validateStringField("state", subscriber.getState(), MAX_STATE_LEN);
        validateStringField("zip", subscriber.getZip(), MAX_ZIP_LEN);
        validateStringField("province", subscriber.getProv(), MAX_PROVINCE_LEN);
        validateStringField("country", subscriber.getCountry(), MAX_COUNTRY_LEN);
        validateStringField("phone", subscriber.getPhone(), MAX_PHONE_LEN);
        validateStringField("contactmethod", subscriber.getContactmethod(), MAX_CONTACTMETHOD_LEN);

	return subscriber;
    }
    
    
    /*
     *
     * This method does the mapping from jaxb object to entity object and vice versa
     *
     * CARA ID is not present in the entity itself. We could get it by joining CaraProjects with Projects(Id), but for
     *   efficiency we accept CARA ID as a parameter instead. It is assumed that the requestor already has this
     *   information and won't mind sending it along.
     */
    private Subscriber createResource(Integer caraId, entity.MailingListSubscribers entity)
            throws DataMartException
    {
        Subscriber resource = new Subscriber();
        resource.setCaraprojectid(caraId);
        resource.setSubscriberid(entity.getSubscriberId());
        resource.setEmail(entity.getEmail());
        resource.setLastname(entity.getLastName());
        resource.setFirstname(entity.getFirstName());
        resource.setTitle(entity.getTitle());
        resource.setOrg(entity.getOrg());
        resource.setOrgtype(entity.getOrgType());
        resource.setSubscribertype(entity.getSubscriberType());
        resource.setAddstreet1(entity.getAddressStreet1());
        resource.setAddstreet2(entity.getAddressStreet2());
        resource.setCity(entity.getCity());
        resource.setState(entity.getState());
        resource.setZip(entity.getZip());
        resource.setProv(entity.getProvince());
        resource.setCountry(entity.getCountry());
        resource.setPhone(entity.getPhone());
        resource.setContactmethod(entity.getContactMethod());
        resource.setSubscribeddate(DataMartBeanUtil.getXMLGregorianFromDateTime(entity.getSubscribedDate()));
        
        return resource;
    }

    private entity.MailingListSubscribers createEntity(Subscriber resource)
            throws DataMartException
    {
        Integer caraProjectId = resource.getCaraprojectid();
        entity.Projects project = DataMartBeanUtil.getCaraProject(em, caraProjectId);
        if (project == null)
            throw new DataMartException(DataMartException.RESOURCE_NOT_FOUND,
                    "CARA Project with id '" + caraProjectId + "' does not exist.");

        entity.MailingListSubscribers entity = new entity.MailingListSubscribers();
        entity.setProjects(project);
        entity.setSubscriberId(resource.getSubscriberid());
        entity.setEmail(resource.getEmail());
        entity.setLastName(resource.getLastname());
        entity.setFirstName(resource.getFirstname());
        entity.setTitle(resource.getTitle());
        entity.setOrg(resource.getOrg());
        entity.setOrgType(resource.getOrgtype());
        entity.setSubscriberType(resource.getSubscribertype());
        entity.setAddressStreet1(resource.getAddstreet1());
        entity.setAddressStreet2(resource.getAddstreet2());
        entity.setCity(resource.getCity());
        entity.setState(resource.getState());
        entity.setZip(resource.getZip());
        entity.setProvince(resource.getProv());
        entity.setCountry(resource.getCountry());
        entity.setPhone(resource.getPhone());
        entity.setContactMethod(resource.getContactmethod());
        entity.setSubscribedDate(DataMartBeanUtil.getDateFromXMLGregorian(resource.getSubscribeddate()));
        return entity;
    }

    /*
     * Retrieve an entity by project/subscriberid
     * return null if project not found
     */
    private entity.MailingListSubscribers retrieveEntity(int caraProjectId, int id)
            throws DataMartException
    {
        entity.Projects project = DataMartBeanUtil.getCaraProject(em, caraProjectId);
        if (project == null) {
            return null;
        }

        Query query = em.createQuery("SELECT s FROM MailingListSubscribers s WHERE s.projects = :project and s.subscriberId = :subscriberId");
        query.setParameter("project", project);
        query.setParameter("subscriberId", id);
        return (entity.MailingListSubscribers) DataMartBeanUtil.getEntityByQueryAllowNull(query,
                "Multiple subscribers found for project '" + project.getId() + "' with id '" + id + "'.");
    }
}
