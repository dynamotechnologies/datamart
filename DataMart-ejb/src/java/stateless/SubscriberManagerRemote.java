package stateless;

import javax.ejb.Remote;
import us.fed.fs.www.nepa.schema.subscriber.*;
import beanutil.DataMartException;

@Remote
public interface SubscriberManagerRemote {
    public void create(Subscriber resource) throws DataMartException;
    public Subscriber get(int caraProjectId, Integer id) throws DataMartException;
    public void update(Subscriber resource) throws DataMartException;
    public void delete(int caraProjectId, Integer id) throws DataMartException;
    public Subscribers getAll(int caraProjectId) throws DataMartException;
    public Subscribers getAllUnsubscribed(String contactmethod, Boolean activetopicflag, Integer start, Integer rows) throws DataMartException;
    public Boolean ping();
}