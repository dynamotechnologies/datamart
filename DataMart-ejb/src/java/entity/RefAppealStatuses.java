package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "RefAppealStatuses")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RefAppealStatuses.findAll", query = "SELECT r FROM RefAppealStatuses r"),
    @NamedQuery(name = "RefAppealStatuses.findById", query = "SELECT r FROM RefAppealStatuses r WHERE r.id = :id"),
    @NamedQuery(name = "RefAppealStatuses.findByName", query = "SELECT r FROM RefAppealStatuses r WHERE r.name = :name")})
public class RefAppealStatuses implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "Name")
    private String name;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refAppealStatuses")
    private Collection<Appeals> appealsCollection;

    public RefAppealStatuses() {
    }

    public RefAppealStatuses(Integer id) {
        this.id = id;
    }

    public RefAppealStatuses(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Collection<Appeals> getAppealsCollection() {
        return appealsCollection;
    }

    public void setAppealsCollection(Collection<Appeals> appealsCollection) {
        this.appealsCollection = appealsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RefAppealStatuses)) {
            return false;
        }
        RefAppealStatuses other = (RefAppealStatuses) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RefAppealStatuses[ id=" + id + " ]";
    }

}
