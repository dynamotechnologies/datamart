/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import entity.RefUnits;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Kevin Sours
 */
@Entity
@Table(name = "ContainerTemplates")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContainerTemplates.findAll", query = "SELECT c FROM ContainerTemplates c"),
    @NamedQuery(name = "ContainerTemplates.findById", query = "SELECT c FROM ContainerTemplates c WHERE c.id = :id"),
    @NamedQuery(name = "ContainerTemplates.findByName", query = "SELECT c FROM ContainerTemplates c WHERE c.name = :name")})
public class ContainerTemplates implements Serializable {
    @Basic(optional = false)
    @Column(name = "Created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "Name")
    private String name;
    @JoinColumn(name = "UnitId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private RefUnits unitId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "templateId")
    private Collection<ContainerTemplateItems> containerTemplateItemsCollection;

    public ContainerTemplates() {
    }

    public ContainerTemplates(Integer id) {
        this.id = id;
    }

    public ContainerTemplates(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RefUnits getUnitId() {
        return unitId;
    }

    public void setUnitId(RefUnits unitId) {
        this.unitId = unitId;
    }

    @XmlTransient
    public Collection<ContainerTemplateItems> getContainerTemplateItemsCollection() {
        return containerTemplateItemsCollection;
    }

    public void setContainerTemplateItemsCollection(Collection<ContainerTemplateItems> containerTemplateItemsCollection) {
        this.containerTemplateItemsCollection = containerTemplateItemsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContainerTemplates)) {
            return false;
        }
        ContainerTemplates other = (ContainerTemplates) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "beanutil.ContainerTemplates[ id=" + id + " ]";
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

}
