package test.reference;

import client.UnitClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import test.TestData;
import us.fed.fs.www.nepa.schema.unit.*;


public class UnitTest {
    private UnitClient client;

    @Before
    public void setUp()
            throws IOException
    {
         client = new UnitClient();
    }

    /*
     * Except in those cases where comments explicitly recommend otherwise, all tests should confirm that:
     * - POST fails on existing resource with code TestData.CANT_POST_ALREADY_EXISTS
     * - GET fails on nonexistent resource with code TestData.CANT_GET_NOT_FOUND
     * - PUT fails on nonexistent resource with code TestData.CANT_PUT_DONT_EXIST
     * - DELETE fails on nonexistent resource with code TestData.CANT_DELETE_DONT_EXIST
     * - Any action fails if specified parent does not exist, e.g. POSTing a projdoc with a bad projectid. This
     *      should return code TestData.BAD_PARENTS
     *
     * TODO: enforce proper http response codes on failure. Add checking for all of the above stated problems in all tests suites.
     *
     * Note: Problems not yet discoverable by this test suite include side effects, and also that odd sort of
     *      defect which can only be made known over the course of several seemingly unrelated requests. But the prospect of finding
     *      peculiarities of this nature through the use of an automated testing framework is more ardently to be hoped
     *      for than seriously to be expected.
     */

    @Test
    public void testPost()
    {
        post();

        // This second POST should fail, because the entity already exists
        try {
            client.postUnit(TestData.units.code1, TestData.units.name2);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_POST_ALREADY_EXISTS)    // TODO: implement this conditional logic in all tests where failure is expected. Make sure
                Assert.fail();              // we're getting the right kind of failure.
        }

        delete();
    }

    @Test
    public void testGet()
    {
        post();

        Unit unit = null;
        Units unitlist = null;
        Units regionlist = null;
        Units subunits = null;
        try {
            unit = client.getUnit(TestData.units.code1);
            unitlist = client.getUnitList();
            regionlist = client.getRegions();
            subunits = client.getSubunits(TestData.units.code2);
        } catch (IOException ex) {
            Logger.getLogger(UnitTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.units.code1, unit.getCode());
        Assert.assertEquals(TestData.units.name1, unit.getName());
        
        // 9/18/2013 - According to comment in UnitManager.java, all units have
        // extended details and the extendeddetails flag is deprecated
        Assert.assertEquals(true, unit.isExtendeddetails());
        
        Assert.assertEquals(TestData.units.projectmap1, unit.isProjectmap());
        Assert.assertEquals(TestData.units.address1_1, unit.getAddress1());
        Assert.assertEquals(TestData.units.address2_1, unit.getAddress2());
        Assert.assertEquals(TestData.units.city1, unit.getCity());
        Assert.assertEquals(TestData.units.state1, unit.getState());
        Assert.assertEquals(TestData.units.zip1, unit.getZip());
        Assert.assertEquals(TestData.units.phone1, unit.getPhonenumber());
        Assert.assertEquals(TestData.units.wwwlink1, unit.getWwwlink());
        Assert.assertEquals(TestData.units.commentemail1, unit.getCommentemail());
        Assert.assertEquals(TestData.units.newspaper1, unit.getNewspaper());
        Assert.assertEquals(TestData.units.newspaperurl1, unit.getNewspaperurl());
        Assert.assertEquals(TestData.units.boundaryurl1, unit.getBoundaryurl());
        Assert.assertEquals(TestData.units.active1, unit.isActive());
        Assert.assertEquals(TestData.units.spotlight1_1, unit.getSpotlightid1());
        Assert.assertEquals(TestData.units.spotlight2_1, unit.getSpotlightid2());
        Assert.assertEquals(TestData.units.spotlight3_1, unit.getSpotlightid3());



        /*
         *
         * It's okay to test the list this way, but only if all fields being tested are required fields.
         *       Otherwise the test may fail simply because an optional field was omitted from the response.
         *       It is extremely important to test the optional fields on the single-entity test, however,
         *       because problems with optional fields would not necessarily cause the jaxb marshal/unmarshal
         *       process to fail. It may be okay to accept on faith that if the single-entity tests all pass
         *       with flying colors that the optional fields are working fine and can be excluded from the getAll() test.
         *
         * TODO: Confirm that all fields tested using the format `getObject().get(0).getElement().length()>0`
         *       are indeed required fields.
         *
         * TODO: Go through all methods in all test classes and confirm that all optional fields are being tested.
         *
         */
        Assert.assertTrue(unitlist.getUnit().get(0).getCode().length()>0);
        Assert.assertTrue(unitlist.getUnit().get(0).getName().length()>0);

        // Make sure the regions special request returns the same number of units as there are
        //     4-digit codes in the standard units list
        int i=0;
        for (Unit u : unitlist.getUnit()) {
            if (u.getCode().length()==4)
                i++;
        }
        Assert.assertEquals(i, regionlist.getUnit().size());
        
        // As of 9/18/2013, unit 110111 has 8 subunits. make sure they're all there.
        // (7 subunits, plus 110111 itself)
        // This test will need to be updated if the reference units change
        Assert.assertEquals(8, subunits.getUnit().size());

        delete();
    }

    @Test
    public void testPut()
    {
        post();

        Unit unit = null;
        try {
            client.putUnit(TestData.units.code1, TestData.units.name2,
                    TestData.units.extendeddetails2,
                    TestData.units.projectmap2,
                    TestData.units.address1_2,
                    TestData.units.address2_2,
                    TestData.units.city2,
                    TestData.units.state2,
                    TestData.units.zip2,
                    TestData.units.phone2,
                    TestData.units.wwwlink2,
                    TestData.units.commentemail2,
                    TestData.units.newspaper2,
                    TestData.units.newspaperurl2,
                    TestData.units.boundaryurl2,
                    TestData.units.active2, 
                    TestData.units.spotlight1_2,
                    TestData.units.spotlight2_2,
                    TestData.units.spotlight3_2
                    );
            unit = client.getUnit(TestData.units.code1);
        } catch (Exception ex) {
            Logger.getLogger(UnitTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
        Assert.assertEquals(TestData.units.name2, unit.getName());
        Assert.assertEquals(TestData.units.extendeddetails2, unit.isExtendeddetails());
        Assert.assertEquals(TestData.units.projectmap2, unit.isProjectmap());
        Assert.assertEquals(TestData.units.address1_2, unit.getAddress1());
        Assert.assertEquals(TestData.units.address2_2, unit.getAddress2());
        Assert.assertEquals(TestData.units.city2, unit.getCity());
        Assert.assertEquals(TestData.units.state2, unit.getState());
        Assert.assertEquals(TestData.units.zip2, unit.getZip());
        Assert.assertEquals(TestData.units.phone2, unit.getPhonenumber());
        Assert.assertEquals(TestData.units.wwwlink2, unit.getWwwlink());
        Assert.assertEquals(TestData.units.commentemail2, unit.getCommentemail());
        Assert.assertEquals(TestData.units.newspaper2, unit.getNewspaper());
        Assert.assertEquals(TestData.units.newspaperurl2, unit.getNewspaperurl());
        Assert.assertEquals(TestData.units.boundaryurl2, unit.getBoundaryurl());
        Assert.assertEquals(TestData.units.active2, unit.isActive());
        Assert.assertEquals(TestData.units.spotlight1_2, unit.getSpotlightid1());
        Assert.assertEquals(TestData.units.spotlight2_2, unit.getSpotlightid2());
        Assert.assertEquals(TestData.units.spotlight3_2, unit.getSpotlightid3());
        

        delete();
    }

    @Test
    public void testDelete()
    {
        post();
        delete();

        // Subsequent GET requests on this deleted entity should now return 404
        try {
            client.getUnit(TestData.units.code1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_GET_NOT_FOUND)
                Assert.fail();
        }
        // Subsequent DELETE's should also fail, because the entity no longer exists
        try {
            client.deleteUnit(TestData.units.code1);
            // If the above statement did not throw an exception then something has gone terribly wrong
            Assert.fail();
        } catch (IOException ex) {
            if (client.getStatus()!=TestData.CANT_DELETE_DONT_EXIST)
                Assert.fail();
        }
    }

    private void post()
    {
        try {
            // client.postUnit(TestData.units.code1, TestData.units.name1);
            client.postUnit(TestData.units.code1, TestData.units.name1,
                    TestData.units.extendeddetails1,
                    TestData.units.projectmap1,
                    TestData.units.address1_1,
                    TestData.units.address2_1,
                    TestData.units.city1,
                    TestData.units.state1,
                    TestData.units.zip1,
                    TestData.units.phone1,
                    TestData.units.wwwlink1,
                    TestData.units.commentemail1,
                    TestData.units.newspaper1,
                    TestData.units.newspaperurl1,
                    TestData.units.boundaryurl1,
                    TestData.units.active1, 
                    TestData.units.spotlight1_1,
                    TestData.units.spotlight2_1,
                    TestData.units.spotlight3_1
                    );

        } catch (IOException ex) {
            Logger.getLogger(UnitTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }

    private void delete()
    {
        try {
            client.deleteUnit(TestData.units.code1);
        } catch (IOException ex) {
            Logger.getLogger(UnitTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.fail();
        }
    }


    @After
    public void cleanFailed()
            throws Exception
    {
        try {
            client.deleteUnit(TestData.units.code1);
            throw new Exception("A test failed to clean up after itself!");
        } catch (IOException ex) {
            // woohoo! the test cleaned up after itself
        }
    }
}